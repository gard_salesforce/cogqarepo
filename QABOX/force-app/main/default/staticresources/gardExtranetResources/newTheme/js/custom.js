jQuery(document).ready( function(){
	
	/* Script to control popup closing feature */
	jQuery('.personal-info-edit').click( function(){
		jQuery('body').css({
			'overflow-y': 'hidden'
		});
		jQuery('.inner-overley, #contact-info-pop-container').show();
	});
	jQuery('.popup-close').click( function(){
		jQuery(this).parent().hide();
		jQuery('.inner-overley').hide();
		jQuery('.overley').hide();
		if (jQuery('.home-fab-sidebar-container').length > 0) {
			jQuery('.home-fab-sidebar-container').css({
				'z-index': 999999
			});
		}
	});

	jQuery('.comp-info-edit').click( function(){
		jQuery('body').css({
			'overflow-y': 'hidden'
		});
		jQuery('.inner-overley, #company-info-pop-container').show();
	});
	jQuery('.popup-close').click( function(){
		jQuery(this).parent().hide();
		jQuery('.inner-overley').hide();
		jQuery('.overley').hide();
		jQuery('body').css({
			'overflow-y': 'scroll'
		});
	});
		/* Controls the feedback popup */
	jQuery('.footer-section-title.feedback a').click( function(){
		jQuery('body').css({
			'overflow-y': 'hidden'
		});
		jQuery('.overley, .home-page-feedback-popup').show();
		if (jQuery('.home-fab-sidebar-container').length > 0) {
			jQuery('.home-fab-sidebar-container').css({
				'z-index': 999998
			});
		}
	});
	/* Controls the feedback popup */

	/* Controls the browser requirement popup */
	jQuery('.footer-section-title.brwreq a').click( function(e){
		e.preventDefault();
		jQuery('body').css({
			'overflow-y': 'hidden'
		});
		jQuery('.overley, .home-page-browser-req-popup').show();
		if (jQuery('.home-fab-sidebar-container').length > 0) {
			jQuery('.home-fab-sidebar-container').css({
				'z-index': 999998
			});
		}
	});
	/* Controls the browser requirement popup */
	
	
	/* Restricts the last breadcrumb to be navigated */
	jQuery('#breadcrumbs .breadcrumbs-wrapper a:last-child').click( function(e){
		e.preventDefault();
	});
	/* Controls the Emergency popup 
	jQuery('.emergency-popup-container').click( function(){
		jQuery('.emergency-popup-holder').show();
	});

	Control for clicking else where in the html except the My Account link popup.
	var emgElement = jQuery('.emergency-popup-holder');
	jQuery(document).bind('mouseup', function (e){
		if (emgElement.has(e.target).length === 0) {
			jQuery(emgElement).hide();
		}
	});
	Controls the Emergency popup */

	/* Controls the tabular view of address book */
	jQuery('.single-tab').click( function(){
		/*if(jQuery(this).hasClass('clients') && jQuery(this).hasClass('deselected')) {
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.gard-cont.selected').removeClass('selected').addClass('deselected');
			jQuery('.tab-details-section .tab-grad-cont-section').hide();
			jQuery('.tab-details-section .tab-grad-client-section').show();
			jQuery('.tab-details-section .tab-section-name').text('Client List');
		}
		if(jQuery(this).hasClass('gard-cont') && jQuery(this).hasClass('deselected')) {
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.clients.selected').removeClass('selected').addClass('deselected');
			jQuery('.tab-details-section .tab-grad-cont-section').show();
			jQuery('.tab-details-section .tab-grad-client-section').hide();
			/*jQuery('.tab-details-section .tab-section-name').text('GARD Contacts');
		}
		if(jQuery(this).hasClass('open-claims') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.single-tab.all-claims').removeClass('reported-claim-active');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .tab-open-claim-section').show();
			jQuery('.tab-details-section .tab-section-name').text('Open Claims');
			jQuery('#breadcrumbs .breadcrumbs-wrapper a:last-child').text('Open Claims');
		}
		if(jQuery(this).hasClass('all-claims') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.single-tab.all-claims').removeClass('reported-claim-active');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .tab-all-claim-section').show();
			jQuery('.tab-details-section .tab-section-name').text('All Claims');
			jQuery('#breadcrumbs .breadcrumbs-wrapper a:last-child').text('All Claims');
		}
		if(jQuery(this).hasClass('reported-claims') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.single-tab.all-claims').addClass('reported-claim-active');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .tab-reported-claim-section').show();
			jQuery('.tab-details-section .tab-section-name').text('Reported Claims');
			jQuery('#breadcrumbs .breadcrumbs-wrapper a:last-child').text('Reported Claims');
		}
		if(jQuery(this).hasClass('cover') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.single-tab.object').removeClass('reported-active');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .tab-open-claim-section').show();
			jQuery('.tab-details-section .tab-section-name').text('My Covers');
			jQuery('#breadcrumbs .breadcrumbs-wrapper a:last-child').text('My Covers');
		}
		if(jQuery(this).hasClass('object') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.single-tab.object').removeClass('reported-active');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .tab-all-claim-section').show();
			jQuery('.tab-details-section .tab-section-name').text('My Objects');
			jQuery('#breadcrumbs .breadcrumbs-wrapper a:last-child').text('My Objects');
		}
		if(jQuery(this).hasClass('portfolio-report') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.single-tab.object').addClass('reported-active');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .tab-reported-claim-section').show();
			jQuery('.tab-details-section .tab-section-name').text('My Portfolio Reports');
			jQuery('#breadcrumbs .breadcrumbs-wrapper a:last-child').text('My portfolio Reports');
		}*/
		if(jQuery(this).hasClass('my-information-tab') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .my-info-container-holder').show();
			jQuery('.create-new-contact').hide();
			var scrollingFooterHeight = jQuery('#footer').offset().top;
			var windowRelTop = jQuery(window).scrollTop();
			var menuPos = scrollingFooterHeight - windowRelTop - 1;
			jQuery('.floating-main-menu-container').height(menuPos);
			jQuery('.table-footnote').show();
		}
		if(jQuery(this).hasClass('my-subscription-tab') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .my-subs-container-holder').show();
			jQuery('.create-new-contact').hide();
			var scrollingFooterHeight = jQuery('#footer').offset().top;
			var windowRelTop = jQuery(window).scrollTop();
			var menuPos = scrollingFooterHeight - windowRelTop - 1;
			jQuery('.floating-main-menu-container').height(menuPos);
			jQuery('.table-footnote').hide();
		}
		if(jQuery(this).hasClass('my-contact-tab') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .my-contacts-container-holder').show();
			jQuery('.create-new-contact').show();
			var scrollingFooterHeight = jQuery('#footer').offset().top;
			var windowRelTop = jQuery(window).scrollTop();
			var menuPos = scrollingFooterHeight - windowRelTop - 1;
			jQuery('.floating-main-menu-container').height(menuPos);
			jQuery('.table-footnote').hide();
		}
		if(jQuery(this).hasClass('share-access-tab') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .req-client-share-acc').show();
		}
		if(jQuery(this).hasClass('already-shared-tab') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .req-client-already-share-acc').show();
		}
	});

	/* Controls the client detail contact popup */
	jQuery('.client-link-details-option').click( function(){
		jQuery('.client-contact-details-popup, .overley').show();
	});

	/* Controls the contact me popup features old 
	jQuery('.acc-info.contact').click( function(){
		if(!jQuery(this).hasClass('selected')) {
			jQuery('.contact-me-container').show();
			jQuery(this).addClass('selected');
		}
	});*/
	/* Controls the contact me popup features */
	var contactMe = jQuery('.acc-info.contact');
	jQuery(document).on('click', function (e){
					var currentTarget = jQuery(e.target);
					if (!currentTarget.hasClass('contact') && (!currentTarget.parents().hasClass('contact-me-container') && !currentTarget.hasClass('contact-me-container'))) {
									contactMe.removeClass('open').addClass('closed');
									jQuery('.contact-me-container').hide();
					}
					else if (currentTarget.hasClass('contact') && currentTarget.hasClass('open') && !currentTarget.parents().hasClass('contact-me-container')) {
									contactMe.removeClass('open').addClass('closed');
									jQuery('.contact-me-container').hide();
					}
					else if (currentTarget.hasClass('contact') && currentTarget.hasClass('closed')) {
									contactMe.removeClass('closed').addClass('open');
									jQuery('.contact-me-container').show();
					}
	});
	/* Controls the contact me popup features */

	
	/* Controls the contact me popup close option by clicking the Cancel button */
	jQuery('.contact-submit-field button.contact-me-cancel').click( function(){
		jQuery('.contact-me-container').hide();
		jQuery('.acc-info.contact').removeClass('selected');
	});

	/* Control for clicking else where in the html except the popup. */
	var element = jQuery('.contact-me-container');
	jQuery(document).bind('mousedown', function (e){
		if (element.has(e.target).length === 0) {
			jQuery(element).hide();
			jQuery('.acc-info.contact').removeClass('selected');
		}
	});

	/* Controls the My Account link popup  old 
	jQuery('.acc-info.my-account').click( function(){
		if(!jQuery(this).hasClass('selected')) {
			jQuery('.my-account-link-popup-container').show();
			jQuery(this).addClass('selected');
		}
	});*/
	/* Controls the My Account link popup */
	var myAcc = jQuery('.acc-info.my-account');
	jQuery(document).on('click', function (e){
					var currentTarget = jQuery(e.target);
					if (!currentTarget.hasClass('my-account') && (!currentTarget.parents().hasClass('my-account-link-popup-wrapper') && !currentTarget.hasClass('my-account-link-popup-wrapper'))) {
									myAcc.removeClass('open').addClass('closed');
									jQuery('.my-account-link-popup-container').hide();
					}
					else if (currentTarget.hasClass('my-account') && currentTarget.hasClass('open') && !currentTarget.parents().hasClass('my-account-link-popup-wrapper')) {
									myAcc.removeClass('open').addClass('closed');
									jQuery('.my-account-link-popup-container').hide();
					}
					else if (currentTarget.hasClass('my-account') && currentTarget.hasClass('closed')) {
									myAcc.removeClass('closed').addClass('open');
									jQuery('.my-account-link-popup-container').show();
					}
	});
	/* Controls the My Account link popup */

	/* Control for clicking else where in the html except the My Account link popup. */
	var accElement = jQuery('.my-account-link-popup-container');
	jQuery(document).bind('mouseup', function (e){
		if (accElement.has(e.target).length === 0) {
			jQuery(accElement).hide();
			jQuery('.acc-info.my-account').removeClass('selected');
		}
	});
	
	/* Control the new request form popup open/close feature */
	var newReq = jQuery('.new-request > a');
	jQuery(document).on('click', function (e){
		var currentTarget = jQuery(e.target);
		if (!currentTarget.hasClass('new-request-popup-button')) {
			newReq.removeClass('open').addClass('closed');
			jQuery('.new-request .manage-claim-dropdown-inner-wrapper').hide();
		}
		else if (currentTarget.hasClass('new-request-popup-button') && currentTarget.hasClass('open')) {
			newReq.removeClass('open').addClass('closed');
			jQuery('.new-request .manage-claim-dropdown-inner-wrapper').hide();
		}
		else if (currentTarget.hasClass('new-request-popup-button') && currentTarget.hasClass('closed')) {
			newReq.removeClass('closed').addClass('open');
			jQuery('.new-request .manage-claim-dropdown-inner-wrapper').show();
		}
	});
	/* Control the new request form popup open/close feature */
	/* Controls the manage clims popup open/close feature in claims details page */
	var mngClaim = jQuery('.manage-claims-popup');
	jQuery(document).on('click', function (e){
		var currentTarget = jQuery(e.target);
		if (!currentTarget.hasClass('manage-claims-popup')) {
			mngClaim.removeClass('open').addClass('closed');
			jQuery('.manage-claim-dropdown-wrapper').hide();
		}
		else if (currentTarget.hasClass('manage-claims-popup') && currentTarget.hasClass('open')) {
			mngClaim.removeClass('open').addClass('closed');
			jQuery('.manage-claim-dropdown-wrapper').hide();
		}
		else if (currentTarget.hasClass('manage-claims-popup') && currentTarget.hasClass('closed')) {
			mngClaim.removeClass('closed').addClass('open');
			jQuery('.manage-claim-dropdown-wrapper').show();
		}
	});
	/* Controls the manage clims popup open/close feature in claims details page */
	/* Controls the comment on claim popup open/close feature in claims details page */
	var comtClaim = jQuery('.comment-on-claims-dropdown');
	jQuery(document).on('click', function (e){
		var currentTarget = jQuery(e.target);
		if (!currentTarget.hasClass('comment-on-claims-dropdown')) {
			if (currentTarget.parents().hasClass('comment-on-claims-dropdown-wrapper')) {
				comtClaim.removeClass('closed').addClass('open');
				jQuery('.comment-on-claims-dropdown-wrapper').show();
			}
			else {
				comtClaim.removeClass('open').addClass('closed');
				jQuery('.comment-on-claims-dropdown-wrapper').hide();
			}
		}
		else if (currentTarget.hasClass('comment-on-claims-dropdown') && currentTarget.hasClass('open')) {
			comtClaim.removeClass('open').addClass('closed');
			jQuery('.comment-on-claims-dropdown-wrapper').hide();
		}
		else if (currentTarget.hasClass('comment-on-claims-dropdown') && currentTarget.hasClass('closed')) {
			comtClaim.removeClass('closed').addClass('open');
			jQuery('.comment-on-claims-dropdown-wrapper').show();
		}
	});
	/* Controls the comment on claim popup open/close feature in claims details page */

	
	/* jQuery('#newClaimPage:pageFrame:reportNewClaimForm:new-claim-upload-doc').change( function(){
		console.log('call');
		jQuery('#newClaimPage:pageFrame:reportNewClaimForm:new-claim-upload-doc').trigger("click");
		jQuery('#newClaimPage:pageFrame:reportNewClaimForm:uploadFile').val(jQuery('#newClaimPage:pageFrame:reportNewClaimForm:new-claim-upload-doc').val());
	});
	jQuery('#newClaimPage:pageFrame:reportNewClaimForm:new-claim-upload-doc').click( function(){
		jQuery('#newClaimPage:pageFrame:reportNewClaimForm:new-claim-upload-doc').trigger("change");
		jQuery('#newClaimPage:pageFrame:reportNewClaimForm:uploadFile').val(jQuery('#newClaimPage:pageFrame:reportNewClaimForm:new-claim-upload-doc').val());
	});
	
	Controls the document upload field in report new claim page 
		
		  jQuery('.new-claim-doc-upld input[type="file"]').MultiFile({
		
		maxfile: 5120,
		maxsize: 25600,
		accept: 'pdf|jpg',
		list: '.selected-files-container',
		STRING: { remove:''}
	
		  });
		*/
	/* Controls the new user access req popup 
	jQuery('.create-user-access a.form-btn').click( function(e){
		e.preventDefault();
		jQuery('.new-user-access-req, .overley').show();
	});*/
	
	/* Control for hover else where in the html except the popup. */
	/* $('.acc-info.contact').bind('mouseover', openSubMenu); 
	$('.acc-info.contact').bind('mouseout', closeSubMenu);  */
	
		jQuery('.contact-me-wrapper .dropdown-menu.open ul.dropdown-menu.selectpicker li').each( function(){ 
		var singleSelOptn = jQuery(this).children('a').children('span.text').text();
		if (singleSelOptn.length > 22) {
			jQuery(this).children('a').addClass('selectpicker-long-option');
			jQuery(this).children('a').attr('alt', singleSelOptn);
			jQuery(this).children('a').children('span.text').html(singleSelOptn.substring(0, 20) + '...');
		}
		else {
			jQuery(this).children('a').children('span.text').html(singleSelOptn);
		}
	});
		
		jQuery('.portfolio-reports .dropdown-menu.open ul.dropdown-menu.selectpicker li').each( function(){
		/*, .report-new-claim-field-section .dropdown-menu.open ul.dropdown-menu.selectpicker li*/
		var singleSelOptn = jQuery(this).children('a').children('span.text').text();
		if (singleSelOptn.length > 30) {
			jQuery(this).children('a').addClass('selectpicker-long-option');
			jQuery(this).children('a').attr('alt', singleSelOptn);
			jQuery(this).children('a').children('span.text').html(singleSelOptn.substring(0, 29) + '...');
		}
		else {
			jQuery(this).children('a').children('span.text').html(singleSelOptn);
		}
	});
	/* jQuery multiselect custom design */
	/* jQuery('.multiselect').multiselect({
		noneSelectedText: false
	}); */

	/* bootstrap multiselect custom design */
	/* new multiselect start 
	jQuery('.gard-contact.claim-tabs-section .multiselect').multiselect({
		includeSelectAllOption: true,
		nonSelectedText: '',
		selectAllText: '<span>Check all</span>',
		enableFiltering: true,
		disableIfEmpty: true,
		filterBehavior: 'text',
		enableCaseInsensitiveFiltering: true,
		label: function(element) {
			var optionStr = jQuery(element).text();
			optionStr = jQuery.trim(optionStr);
			if (jQuery(element).parents('.request-forms').length > 0) {
				if (optionStr.length > 25) {
					return '<span class="select-options" alt="' + optionStr + '">' + optionStr.substring(0, 24) + '...' + '</span>';
				}
				else {
					return '<span class="select-options">' + optionStr + '</span>';
				}
			}
			if (jQuery(element).parents('.claim-details-document-type-filter').length > 0) {
				if (optionStr.length > 25) {
					return '<span class="select-options" alt="' + optionStr + '">' + optionStr.substring(0, 23) + '...' + '</span>';
				}
				else {
					return '<span class="select-options">' + optionStr + '</span>';
				}
			}
			else {
				if (optionStr.length > 19) {
					return '<span class="select-options" alt="' + optionStr + '">' + optionStr.substring(0, 18) + '...' + '</span>';
				}
				else {
					return '<span class="select-options">' + optionStr + '</span>';
				}
			}
		},
		buttonText: function(options, select) {
			if (options.length == 0) {
				if (select.parent().parent().hasClass('global-user-select-wrapper-section')) {
					return ' <span>Select client</span>';
				}
				else {
					return ' <b class="caret"></b>';
				}
			}
			else {
				if (options.length == 1) {
					var labels = [];
					options.each(function() {
						if (jQuery(this).attr('label') !== undefined) {
							labels.push(jQuery(this).attr('label'));
						}
						else {
							var btnstr = jQuery(this).html();
							if (select.parent().parent().hasClass('claim-details-document-type-filter')) {
								if (btnstr.length > 28) {
									labels.push(btnstr.substring(0, 26) + '...');
								}
								else {
									labels.push(btnstr);
								}
							}
							else {
								if (btnstr.length > 18) {
									labels.push(btnstr.substring(0, 18) + '...');
								}
								else {
									labels.push(btnstr);
								}
							}
						}
					});
					return '<span>' + labels + '</span>';
				}
				else {
					if (options.length > 1) {
						
						return '<span>Multi</span> <b class="caret"></b>';
					}
				}
			}
		}
	});*/
	/* new multiselect end */
	/* new multiselect old 
	jQuery('.gard-contact.claim-tabs-section .multiselect').multiselect({
		label: function(element) {
			var optionStr = jQuery(element).html();
			if (optionStr.length > 28) {
				return '<span class="select-options" alt="' + optionStr + '">' + optionStr.substring(0, 26) + '...' + '</span>';
			}
			else {
				return '<span class="select-options">' + optionStr + '</span>';
			}
		},
		includeSelectAllOption: true,
		selectAllText: '<span>Check all</span>',
		buttonText: function(options, select) {
			if (options.length == 0) {
				return ' <b class="caret"></b>';
			}
			else {
				if (options.length == 1) {
					var labels = [];
					options.each(function() {
						if (jQuery(this).attr('label') !== undefined) {
							labels.push(jQuery(this).attr('label'));
						}
						else {
							var btnstr = jQuery(this).html();
							if (btnstr.length > 18) {
								labels.push(btnstr.substring(0, 18) + '...');
							}
							else {
								labels.push(btnstr);
							}
						}
					});
					return '<span>' + labels + '</span>';
				}
				else {
					if (options.length > 1) {						
						return '<span>Multi</span> <b class="caret"></b>';
					}
				}
			}
		}
	});
	*/
	/* jQuery multiselect popup's custom scroll bar */
	jQuery(".gard-contact.claim-tabs-section .select-wrapper .btn-group ul.multiselect-container, .dropdown-menu.open ul").mCustomScrollbar({
		mouseWheelPixels: 20,
		scrollEasing: "linear",
		scrollInertia: 0
	});
	
	jQuery('.mCSB_draggerContainer').click( function(e){
		e.stopPropagation();
	});

	/* Multiselect custom dropdown for the header global drop down */
	jQuery('.global-user-select-wrapper-section .global-multiselect').multiselect({
		includeSelectAllOption: true,
		nonSelectedText: 'Test',
		selectAllText: '<span>Check all</span>',
		enableFiltering: true,
		disableIfEmpty: true,
		filterBehavior: 'text',
		enableCaseInsensitiveFiltering: true,
		templates: {
			li: '<li><a href="javascript:void(0);"><label></label></a><span class="fab-option"></span></li>'
		},
		label: function(element) {
			var optionStr = jQuery(element).html();
			var optionVal = jQuery(element).val();
			if (optionStr.length > 21) {
				return '<span client-id="' + optionVal + '" class="select-options" alt="' + optionStr + '">' + optionStr.substring(0, 20) + '...' + '</span>';
			}
			else {
				return '<span client-id="' + optionVal + '" class="select-options">' + optionStr + '</span>';
			}
		},
		buttonText: function(options, select) {
			if (options.length == 0) {
				return ' <span>Select client</span>';
			}
			else {
				if (options.length == 1) {
					var labels = [];
					options.each(function() {
						if (jQuery(this).attr('label') !== undefined) {
							labels.push($(this).attr('label'));
						}
						else {
							var btnstr = jQuery(this).html();
							if (btnstr.length > 24) {
								labels.push(btnstr.substring(0, 24) + '...');
							}
							else {
								labels.push(btnstr);
							}
						}
					});
					return '<span>' + labels + '</span>';
				}
				else {
					if (options.length > 1) {
						/* return '<span>' + options.length + ' ' + this.nSelectedText + '</span> <b class="caret"></b>'; */
						return '<span>Multi</span> <b class="caret"></b>';
					}
				}
			}
		}
	});
	jQuery('.global-user-select-wrapper-section ul.multiselect-container li').each( function(){
		if (jQuery(this).hasClass('multiselect-all')) {
			jQuery(this).children('span.fab-option').remove();
		}
		else {
			var opClntId = jQuery(this).children().find('.select-options').attr('client-id');
			jQuery(this).children('span.fab-option').attr('clientId', opClntId);
		}
	});
	jQuery('.fav-options-each-fav-pointer .fab-option, .chosen-fav-claim-pointer.fab-option').click( function(){
		jQuery(this).toggleClass('favved');
	});
	jQuery('.global-user-select-wrapper-section .fab-option').click( function(){
		jQuery(this).toggleClass('favved');
		jQuery(this).parent().children('a').children('label').toggleClass('fav-label');
	});
	jQuery('.fav-options-each .fab-option').click( function(e){
		e.preventDefault();
		var lThis = jQuery(this);
		lThis.toggleClass('favved').parent().append('<div class="fav-item-removing">Removing...</div>');
		setTimeout( function(){
			lThis.parent().parent().delay(500).remove();
		}, 800);
	});
	/* Multiselect custom dropdown for the header global drop down */

	/* jQuery multiselect popup's custom scroll bar */
	jQuery(".select-wrapper .btn-group ul.multiselect-container, .dropdown-menu.open ul").mCustomScrollbar({
		mouseWheelPixels: 20,
		scrollEasing: "linear",
		scrollInertia: 0
	});
	jQuery(".hobe-gard-news-wrapper").mCustomScrollbar({
		mouseWheelPixels: 20,
		autoHideScrollbar: true,
		scrollEasing: "linear",
		scrollInertia: 0
	});
	/* scroll of new admin leftside of usermanagement page  */
	jQuery(".req-new-user-list-container").mCustomScrollbar({
		mouseWheelPixels: 20,
		scrollEasing: "linear",
		scrollInertia: 0
	});
	/* scroll of new admin leftside of usermanagement page  */
	/* Stop multi-select dropdown closing event clicking on the custom scroll bar */
	jQuery('.mCSB_draggerContainer').click( function(e){
		e.stopPropagation();
	});
	/* Controls the search box opening feature */
	jQuery('.search-opener').click( function(){
		if (!jQuery(this).hasClass('selected')) {
			jQuery('.search-opener-section .search-wrapper').stop(true,true).animate({
				right: 0,
				width: 257,
				duration: 0
			}, 400);
			setTimeout( function(){
				jQuery('.site-search-box').focus();
			}, 400);
			jQuery(this).addClass('search-opener-selected').removeClass('search-opener');
		}
	});
	var searchBox = jQuery('.search-opener-section .search-wrapper');
	jQuery(document).bind('mousedown', function (e){
		if (e.target.className != 'search-opener-selected') {
			if (searchBox.has(e.target).length === 0) {
				searchBox.stop(true,true).animate({
					right: -217,
					width: 150
				}, 400);
				jQuery('.search-opener-selected').addClass('search-opener').removeClass('search-opener-selected');
			}
		}
	});
	/* Controls the search box opening feature */

	/* Provides the tooltip using jquery UI tooltip plugin */
	jQuery('td, .select-wrapper .btn-group ul.multiselect-container span, .dropdown-menu.open ul.dropdown-menu.selectpicker li a.selectpicker-long-option').mouseenter( function(){
		jQuery('.custom-tooltip').remove();
		if(jQuery(this).attr('alt')) {
			jQuery(this).append('<span class="custom-tooltip">' + jQuery(this).attr('alt') + '</span>');
		}
	});
	jQuery('td, .select-wrapper .btn-group ul.multiselect-container span, .dropdown-menu.open ul.dropdown-menu.selectpicker li a').mouseleave( function(){
		jQuery('.custom-tooltip').remove();
	});
	/* Moving tooltip with the cursor 	
	*/
	jQuery(document).on('mousemove', function(e){
		var windowExitVal =  jQuery('.custom-tooltip').width() + e.pageX + 40;
		if (windowExitVal > jQuery(window).width())  {			
			var lft = windowExitVal - jQuery(window).width();
			jQuery('.custom-tooltip').offset({
			   left:  e.pageX - lft,
			   top:   e.pageY + 22
			});
		}
		else {
			jQuery('.custom-tooltip').offset({
			   left:  e.pageX + 1,
			   top:   e.pageY + 22
			});
			jQuery('.custom-tooltip').css('max-width', '100%');
		}
	}); 

	/* JS for the auto triming of forms attached files' names */
	jQuery('a.form-pdf-downloader').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 69) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 66) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	/* JS for the auto triming of forms attached files' names */

	/* Controls the HOME page's top left block title trimming */
	if (jQuery('.left-news-section .home-page-left-content-container-latest-news .page-title a').text().trim().length > 60) {
		var singleSelOptn = jQuery('.left-news-section .home-page-left-content-container-latest-news .page-title').text().trim();
		jQuery('.left-news-section .home-page-left-content-container-latest-news .page-title a').addClass('selectpicker-long-option');
		jQuery('.left-news-section .home-page-left-content-container-latest-news .page-title a').attr('alt', singleSelOptn);
		jQuery('.left-news-section .home-page-left-content-container-latest-news .page-title a').html(singleSelOptn.substring(0, 58) + '...');
	}
	else if (jQuery('.left-news-section .home-page-left-content-container-latest-news .page-title').text().trim().length > 60) {
		var singleSelOptn = jQuery('.left-news-section .home-page-left-content-container-latest-news .page-title').text().trim();
		jQuery('.left-news-section .home-page-left-content-container-latest-news .page-title').addClass('selectpicker-long-option');
		jQuery('.left-news-section .home-page-left-content-container-latest-news .page-title').attr('alt', singleSelOptn);
		jQuery('.left-news-section .home-page-left-content-container-latest-news .page-title').html(singleSelOptn.substring(0, 58) + '...');
	}
	/* Controls the HOME page's top left block title trimming */
	
	/* Controls the HOME page's top right block title trimming */
	if (jQuery('.right-news-section .home-page-left-content-container-latest-news .page-title a').text().trim().length > 60) {
		var singleSelOptn = jQuery('.right-news-section .home-page-left-content-container-latest-news .page-title').text().trim();
		jQuery('.right-news-section .home-page-left-content-container-latest-news .page-title a').addClass('selectpicker-long-option');
		jQuery('.right-news-section .home-page-left-content-container-latest-news .page-title a').attr('alt', singleSelOptn);
		jQuery('.right-news-section .home-page-left-content-container-latest-news .page-title a').html(singleSelOptn.substring(0, 58) + '...');
	}
	else if (jQuery('.right-news-section .home-page-left-content-container-latest-news .page-title').text().trim().length > 60) {
		var singleSelOptn = jQuery('.right-news-section .home-page-left-content-container-latest-news .page-title').text().trim();
		jQuery('.right-news-section .home-page-left-content-container-latest-news .page-title').addClass('selectpicker-long-option');
		jQuery('.right-news-section .home-page-left-content-container-latest-news .page-title').attr('alt', singleSelOptn);
		jQuery('.right-news-section .home-page-left-content-container-latest-news .page-title').html(singleSelOptn.substring(0, 58) + '...');
	}
	/* Controls the HOME page's top right block title trimming */
	/* Controls the claim details subclaim page table data trimming */
	jQuery('.claim-adition-page td.sub-claim-detail').each( function(){
			var formsName = jQuery.trim(jQuery(this).text());
			if (formsName.length > 22) {
				jQuery(this).addClass('selectpicker-long-option');
				jQuery(this).attr('alt', formsName);
				jQuery(this).html(formsName.substring(0, 20) + '...');
			}
			else {
				jQuery(this).html(formsName);
			}
		});
		jQuery('.claim-adition-page td.sub-claim-desc').each( function(){
			var formsName = jQuery.trim(jQuery(this).text());
			if (formsName.length > 15) {
				jQuery(this).addClass('selectpicker-long-option');
				jQuery(this).attr('alt', formsName);
				jQuery(this).html(formsName.substring(0, 13) + '...');
			}
			else {
				jQuery(this).html(formsName);
			}
		});
	/* Controls the claim details subclaim page table data trimming */
	/* Provides the tooltip using jquery UI tooltip plugin */
	jQuery('.my-addressbook-container .tab-section-name span, .my-addressbook-container .info-email a, td, .select-wrapper .btn-group ul.multiselect-container span, .dropdown-menu.open ul.dropdown-menu.selectpicker li a.selectpicker-long-option, .selected-files-container span.file-name, .download-holder a.form-pdf-downloader.selectpicker-long-option, .right-news-section .home-page-left-content-container-latest-news .page-title a.selectpicker-long-option, .left-news-section .home-page-left-content-container-latest-news .page-title a.selectpicker-long-option, .underwriter-popup .under-popup-section .sec-details a, .broker-view-page-container .home-page-content-wrapper .home-page-recent-claims-container td.claims-handler span.underwrt-popup-link,.home-page-recent-claims-container .home-page-change-table td.gard-reference a,.select-wrapper .btn-group ul.multiselect-container span, .dropdown-menu.open ul.dropdown-menu.selectpicker li a.selectpicker-long-option,.right-news-section .home-page-left-content-container-latest-news .page-title.selectpicker-long-option, .left-news-section .home-page-left-content-container-latest-news .page-title.selectpicker-long-option').mouseenter( function(){
		jQuery('.custom-tooltip').remove();
		if(jQuery(this).attr('alt')) {
			//jQuery('body').append('<span class="custom-tooltip">' + jQuery(this).attr('alt') + '</span>');
			 jQuery('body').append('<span class="custom-tooltip"></span>');        
			 jQuery('body .custom-tooltip').text(jQuery(this).attr('alt'));
		}
	});
	jQuery('.my-addressbook-container .tab-section-name span, .my-addressbook-container .info-email a, td, .select-wrapper .btn-group ul.multiselect-container span, .dropdown-menu.open ul.dropdown-menu.selectpicker li a, .selected-files-container span.file-name, .download-holder a.form-pdf-downloader.selectpicker-long-option, .right-news-section .home-page-left-content-container-latest-news .page-title a.selectpicker-long-option, .left-news-section .home-page-left-content-container-latest-news .page-title a.selectpicker-long-option, .underwriter-popup .under-popup-section .sec-details a, .broker-view-page-container .home-page-content-wrapper .home-page-recent-claims-container td.claims-handler span.underwrt-popup-link,.home-page-recent-claims-container .home-page-change-table td.gard-reference a,.select-wrapper .btn-group ul.multiselect-container span, .dropdown-menu.open ul.dropdown-menu.selectpicker li a.selectpicker-long-option,.right-news-section .home-page-left-content-container-latest-news .page-title.selectpicker-long-option, .left-news-section .home-page-left-content-container-latest-news .page-title.selectpicker-long-option').mouseleave( function(){
		jQuery('.custom-tooltip').remove();
	});
	
	/* Provides the tooltip using jquery UI tooltip plugin */
	/*jQuery('button').mouseenter( function(){
		if (jQuery(this).hasClass('selectpicker-long-option')) {
			if(jQuery(this).attr('alt')) {
				jQuery('body').append('<span class="custom-tooltip">' + jQuery(this).attr('alt') + '</span>');
			}
		}
	})
	jQuery('button').mouseleave( function(){
		jQuery('.custom-tooltip').remove();
	})*/
	
	/* tooltrip for claim-handler-details page start*/
	jQuery('.claim-handler-details-container .info-email a').each( function(){             
                var emailText = jQuery.trim(jQuery(this).text());
                if (emailText.length > 36) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 34) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
                });

	jQuery('.claim-handler-details-container .mobile .text-val, .claim-handler-details-container .tel-num .text-val').each( function(){             
                var emailText = jQuery.trim(jQuery(this).text());
                if (emailText.length > 32) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 31) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
	});


	 jQuery('.claim-handler-details-container .info-email a,.claim-handler-details-container .mobile .text-val, .claim-handler-details-container .tel-num .text-val').mouseenter( function(){
                jQuery('.custom-tooltip').remove();
                if(jQuery(this).attr('alt')) {
                    jQuery('body').append('<span class="custom-tooltip">' + jQuery(this).attr('alt') + '</span>');
                }
	});
	jQuery('.claim-handler-details-container .info-email a,.claim-handler-details-container .mobile .text-val, .claim-handler-details-container .tel-num .text-val').mouseleave( function(){
                jQuery('.custom-tooltip').remove();
	});
	/* tooltrip for claim-handler-details page end*/
	
	/*  %%%%%%%%%%%%%% Home page %%%%%%%%%%%%%%% */
	/* tooltrip for td gard-claim-reference in home page start*/
	jQuery('.home-page-recent-claims-container .home-page-change-table td.gard-reference a').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());		
		if (formsName.length > 30) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 28) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	
	/* tooltrip for td gard-claim-reference in home page end*/
	
	/* tooltrip for client home page start*/
	jQuery('.broker-view-page-container .home-page-content-wrapper td.client').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());		
		if (formsName.length > 43) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 41) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	/* tooltrip for client home page end*/
	/* JS for the auto trimming of claim-handler td  */
	jQuery('.broker-view-page-container .home-page-content-wrapper .home-page-recent-claims-container td.claims-handler span.underwrt-popup-link').each( function(){
                var emailText = jQuery.trim(jQuery(this).text());
                if (emailText.length > 32) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 30) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
});
/* JS for the auto trimming of claim-handler td  */	
	/* JS for the auto trimming of object td  */
	jQuery('.broker-view-page-container .home-page-content-wrapper .home-page-recent-claims-container td.object').each( function(){
                var emailText = jQuery.trim(jQuery.trim(jQuery(this).text()));
                if (emailText.length > 40) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 37) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
});
/* JS for the auto trimming of object td  */	
/* JS for the auto trimming of claim-type td  */
	jQuery('.broker-view-page-container .home-page-content-wrapper .home-page-recent-claims-container td.claim-type').each( function(){
                var emailText = jQuery.trim(jQuery.trim(jQuery(this).text()));
                if (emailText.length > 22) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 20) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
});
/* JS for the auto trimming of claim-type td  */
/* JS for the auto trimming of cover td  */
	jQuery('.broker-view-page-container .home-page-content-wrapper .home-page-recent-claims-container td.cover').each( function(){
                var emailText = jQuery.trim(jQuery.trim(jQuery(this).text()));
                if (emailText.length > 32) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 30) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
});
/* JS for the auto trimming of cover td */	
/* JS for the auto trimming of cover td for link */
	jQuery('.broker-view-page-container .home-page-content-wrapper .home-page-recent-covers-container td.cover a').each( function(){
                var emailText = jQuery.trim(jQuery.trim(jQuery(this).text()));
                if (emailText.length > 32) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 30) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
});
jQuery('.broker-view-page-container .home-page-content-wrapper .home-page-recent-covers-container td.cover a').mouseenter( function(){
                jQuery('.custom-tooltip').remove();
                if(jQuery(this).attr('alt')) {
                    jQuery('body').append('<span class="custom-tooltip">' + jQuery(this).attr('alt') + '</span>');
                }
	});
	jQuery('.broker-view-page-container .home-page-content-wrapper .home-page-recent-covers-container td.cover a').mouseleave( function(){
                jQuery('.custom-tooltip').remove();
	});

/* JS for the auto trimming of cover td for link */	
/* JS for the auto trimming of objact-name and product-area td */
	jQuery('.broker-view-page-container .home-page-content-wrapper .home-page-recent-covers-container td.product-area, .broker-view-page-container .home-page-content-wrapper .home-page-recent-covers-container td.object-name').each( function(){
                var emailText = jQuery.trim(jQuery.trim(jQuery(this).text()));
                if (emailText.length > 32) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 30) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
});	
/* JS for the auto trimming of objact-name and product-area td */

/*########### trimming td data for client view home page table start ################## */
	/* JS for the auto trimming of claim-handler td  */
	jQuery('.client-view-page-container .home-page-content-wrapper .home-page-recent-claims-container td.claims-handler span.underwrt-popup-link').each( function(){
                var emailText = jQuery.trim(jQuery(this).text());
                if (emailText.length > 42) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 40) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
});
/* JS for the auto trimming of claim-handler td  */	
	/* JS for the auto trimming of object td  */
	jQuery('.client-view-page-container .home-page-content-wrapper .home-page-recent-claims-container td.object').each( function(){
                var emailText = jQuery.trim(jQuery.trim(jQuery(this).text()));
                if (emailText.length > 43) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 41) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
});
/* JS for the auto trimming of object td  */	
/* JS for the auto trimming of claim-type td  */
	jQuery('.client-view-page-container .home-page-content-wrapper .home-page-recent-claims-container td.claim-type').each( function(){
                var emailText = jQuery.trim(jQuery.trim(jQuery(this).text()));
                if (emailText.length > 38) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 35) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
});
/* JS for the auto trimming of claim-type td  */
/* JS for the auto trimming of cover td  */
	jQuery('.client-view-page-container .home-page-content-wrapper .home-page-recent-claims-container td.cover').each( function(){
                var emailText = jQuery.trim(jQuery.trim(jQuery(this).text()));
                if (emailText.length > 42) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 40) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
});
/* JS for the auto trimming of cover td */	
/* JS for the auto trimming of cover td for link */
	jQuery('.client-view-page-container .home-page-content-wrapper .home-page-recent-covers-container td.cover a').each( function(){
                var emailText = jQuery.trim(jQuery.trim(jQuery(this).text()));
                if (emailText.length > 50) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 48) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
});
jQuery('.client-view-page-container .home-page-content-wrapper .home-page-recent-covers-container td.cover a, .client-view-page-container .home-page-content-wrapper .home-page-recent-claims-container td.claims-handler span.underwrt-popup-link').mouseenter( function(){
                jQuery('.custom-tooltip').remove();
                if(jQuery(this).attr('alt')) {
                    jQuery('body').append('<span class="custom-tooltip">' + jQuery(this).attr('alt') + '</span>');
                }
	});
	jQuery('.client-view-page-container .home-page-content-wrapper .home-page-recent-covers-container td.cover a, .client-view-page-container .home-page-content-wrapper .home-page-recent-claims-container td.claims-handler span.underwrt-popup-link').mouseleave( function(){
                jQuery('.custom-tooltip').remove();
	});

/* JS for the auto trimming of cover td for link */	
/* JS for the auto trimming of objact-name and product-area td */
	jQuery('.client-view-page-container .home-page-content-wrapper .home-page-recent-covers-container td.product-area, .client-view-page-container .home-page-content-wrapper .home-page-recent-covers-container td.object-name').each( function(){
                var emailText = jQuery.trim(jQuery.trim(jQuery(this).text()));
                if (emailText.length > 42) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 40) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
});	
/* JS for the auto trimming of objact-name and product-area td */
/*################ trimming td data for client view home page table end ############### */
/*  %%%%%%%%%%%%%% Home page %%%%%%%%%%%%%%% */

	/* tooltrip for client-name start*/
	jQuery('td.client-name').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 28) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 26) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	/* tooltrip for client-name end*/
/* %%%%%%%%%%%%%%% Portfolio Module %%%%%%%%%%%%%%%% */
	/* tooltrip for client-name portfolio start*/
	jQuery('.portfolio td.client').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 56) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 53) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	/* tooltrip for client-name portfolio end*/
	/* tooltrip for portfolio cover start*/
	jQuery('.broker-view-page-container .portfolio .cover td.cover, .client-view-page-container .portfolio .cover td.cover').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 33) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 30) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	/* tooltrip for portfolio cover end*/
	/* tooltrip for portfolio product-area start*/
	jQuery('.broker-view-page-container .portfolio .cover td.product-area, .client-view-page-container .portfolio .cover td.product-area').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 18) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 16) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	/* tooltrip for portfolio product-area end*/
	/* tooltrip for portfolio broker-name start*/
	jQuery('.client-view-page-container .portfolio .cover td.broker-name').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 56) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 53) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	/* tooltrip for portfolio broker-name end*/
	/* tooltrip for portfolio underwriter start*/
	jQuery('.portfolio td.underwriter.mycover span.underwrt-popup-link').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 33) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 30) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	jQuery('.portfolio td.underwriter.mycover span.underwrt-popup-link').mouseenter( function(){
                jQuery('.custom-tooltip').remove();
                if(jQuery(this).attr('alt')) {
                    jQuery('body').append('<span class="custom-tooltip">' + jQuery(this).attr('alt') + '</span>');
                }
	});
	jQuery('.portfolio td.underwriter.mycover span.underwrt-popup-link').mouseleave( function(){
                jQuery('.custom-tooltip').remove();
	});
	/* tooltrip for portfolio underwriter end*/
	/* ########## CoverList Page Start ############ */
		/* tooltrip for portfolio underwriter-coverlist start*/
		jQuery('.broker-view-page-container .obj-per-cover td.underwriter.mycoverlist span.underwrt-popup-link, .broker-view-page-container .obj-per-cover td.view-obj a, .broker-view-page-container .obj-per-cover td.view-obj span.no-link').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 22) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 20) + '...');
		}
		else {
			jQuery(this).html(formsName);
			}
		});	
		jQuery('.broker-view-page-container .obj-per-cover td.underwriter.mycoverlist span.underwrt-popup-link, .broker-view-page-container .obj-per-cover td.view-obj a, .broker-view-page-container .obj-per-cover td.view-obj span.no-link').mouseenter( function(){
					jQuery('.custom-tooltip').remove();
					if(jQuery(this).attr('alt')) {
						jQuery('body').append('<span class="custom-tooltip">' + jQuery(this).attr('alt') + '</span>');
					}
		});
		jQuery('.broker-view-page-container .obj-per-cover td.underwriter.mycoverlist span.underwrt-popup-link, .broker-view-page-container .obj-per-cover td.view-obj a, .broker-view-page-container .obj-per-cover td.view-obj span.no-link').mouseleave( function(){
					jQuery('.custom-tooltip').remove();
		});
		
		/* tooltrip for portfolio underwriter-coverlist end*/
		
		jQuery('.broker-view-page-container .obj-per-cover td.obj-cat').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 28) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 26) + '...');
		}
		else {
			jQuery(this).html(formsName);
			}
		});


	jQuery('.broker-view-page-container .obj-per-cover td.product-area').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 22) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 20) + '...');
		}
		else {
			jQuery(this).html(formsName);
			}
		});
	
		/* client view start */
		/* tooltrip for portfolio underwriter-coverlist start*/
		jQuery('.client-view-page-container .obj-per-cover td.underwriter.mycoverlist span.underwrt-popup-link, .client-view-page-container .obj-per-cover td.view-obj a, .client-view-page-container .obj-per-cover td.view-obj span.no-link').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 43) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 41) + '...');
		}
		else {
			jQuery(this).html(formsName);
			}
		});	
		jQuery('.client-view-page-container .obj-per-cover td.underwriter.mycoverlist span.underwrt-popup-link, .client-view-page-container .obj-per-cover td.view-obj a, .client-view-page-container .obj-per-cover td.view-obj span.no-link').mouseenter( function(){
					jQuery('.custom-tooltip').remove();
					if(jQuery(this).attr('alt')) {
						jQuery('body').append('<span class="custom-tooltip">' + jQuery(this).attr('alt') + '</span>');
					}
		});
		jQuery('.client-view-page-container .obj-per-cover td.underwriter.mycoverlist span.underwrt-popup-link, .client-view-page-container .obj-per-cover td.view-obj a, .client-view-page-container .obj-per-cover td.view-obj span.no-link').mouseleave( function(){
					jQuery('.custom-tooltip').remove();
		});
		
		/* tooltrip for portfolio underwriter-coverlist end*/
		
		jQuery('.client-view-page-container .obj-per-cover td.obj-cat').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 41) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 39) + '...');
		}
		else {
			jQuery(this).html(formsName);
			}
		});


	jQuery('.client-view-page-container .obj-per-cover td.product-area').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 33) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 30) + '...');
		}
		else {
			jQuery(this).html(formsName);
			}
		});
		/* client view end */
	/* ########## CoverList Page End ############ */
	/* ########### MyObject Page Start ############# */
	/* tooltrip for portfolio broker start*/
	jQuery('.broker-view-page-container .portfolio .object td.broker').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 33) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 30) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	/* tooltrip for portfolio broker end*/
	/* tooltrip for portfolio object-category start*/
	jQuery('.broker-view-page-container .portfolio .object td.object-category, .client-view-page-container .portfolio .object td.object-category').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 22) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 20) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	/* tooltrip for portfolio object-category end*/
	/* tooltrip for portfolio cover start*/
	jQuery('.broker-view-page-container .portfolio .object td.cover, .client-view-page-container .portfolio .object td.cover').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 28) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 26) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	/* tooltrip for portfolio cover end*/
	/* tooltrip for portfolio product-area start*/
	jQuery('.broker-view-page-container .portfolio .object td.product-area, .client-view-page-container .portfolio .object td.product-area').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 16) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 14) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	/* tooltrip for portfolio product-area start*/
	
		jQuery('.broker-view-page-container .portfolio .object td.underwriter.myobject span.underwrt-popup-link').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 22) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 20) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	
	jQuery('.broker-view-page-container .portfolio .object td.obj-name a, .broker-view-page-container .portfolio .object td.obj-name span.no-link').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 18) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 16) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	
		jQuery('.client-view-page-container .portfolio .object td.underwriter.myobject span.underwrt-popup-link').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 30) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 27) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	
	jQuery('.client-view-page-container .portfolio .object td.obj-name a, .client-view-page-container .portfolio .object td.obj-name span.no-link').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 26) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 23) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});

	jQuery('.broker-view-page-container .portfolio .object td.underwriter.myobject span.underwrt-popup-link, .broker-view-page-container .portfolio .object td.obj-name a, .client-view-page-container .portfolio .object td.underwriter.myobject span.underwrt-popup-link, .client-view-page-container .portfolio .object td.obj-name a, .client-view-page-container .portfolio .object td.obj-name span.no-link, .broker-view-page-container .portfolio .object td.obj-name span.no-link').mouseenter( function(){
					jQuery('.custom-tooltip').remove();
					if(jQuery(this).attr('alt')) {
						jQuery('body').append('<span class="custom-tooltip">' + jQuery(this).attr('alt') + '</span>');
					}
		});
		jQuery('.broker-view-page-container .portfolio .object td.underwriter.myobject span.underwrt-popup-link, .broker-view-page-container .portfolio .object td.obj-name a, .client-view-page-container .portfolio .object td.underwriter.myobject span.underwrt-popup-link, .client-view-page-container .portfolio .object td.obj-name a, .client-view-page-container .portfolio .object td.obj-name span.no-link, .broker-view-page-container .portfolio .object td.obj-name span.no-link').mouseleave( function(){
					jQuery('.custom-tooltip').remove();
		});
	/* ########### MyObject Page End ############# */
	/* ########### ObjectDetails Page ############# */
	jQuery('.covers-for-object-section.details-object-section td.capital').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 42) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 40) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});

	jQuery('.covers-for-object-section.details-object-section td.broker-name').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 44) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 42) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	
	jQuery('.covers-for-object-section.details-object-section td.underwriter.myobjectdetails span.underwrt-popup-link').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 33) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 30) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	
	jQuery('.covers-for-object-section.details-object-section td.underwriter.myobjectdetails span.underwrt-popup-link, .claims-for-object-section.details-object-section td.gard-ref a').mouseenter( function(){
					jQuery('.custom-tooltip').remove();
					if(jQuery(this).attr('alt')) {
						jQuery('body').append('<span class="custom-tooltip">' + jQuery(this).attr('alt') + '</span>');
					}
		});
		jQuery('.covers-for-object-section.details-object-section td.underwriter.myobjectdetails span.underwrt-popup-link, .claims-for-object-section.details-object-section td.gard-ref a').mouseleave( function(){
					jQuery('.custom-tooltip').remove();
		});
	jQuery('.claims-for-object-section.details-object-section td.claim-name').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 46) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 44) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});

	jQuery('.claims-for-object-section.details-object-section td.your-ref').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 28) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 26) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});

	jQuery('.claims-for-object-section.details-object-section td.gard-ref a').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 30) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 27) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	
	jQuery('.claims-for-object-section.details-object-section td.vessel-name').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 33) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 30) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});

	/* ########### ObjectDetails Page ############# */
	
/* %%%%%%%%%%%%%%% Portfolio Module %%%%%%%%%%%%%%%% */	


/* %%%%%%%%%%%%%%% Claims Module %%%%%%%%%%%%%%%% */	
	/* tooltrip for client-type start*/
	jQuery('.broker-view-page-container .my-cliams td.client-type').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 28) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 26) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	/* tooltrip for client-type end*/
	 /* tooltrip for obj-name start*/
	jQuery('.broker-view-page-container .my-cliams td.obj-name').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 22) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 20) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	/* tooltrip for obj-name end*/
	/* tooltrip for cover start*/
	jQuery('.broker-view-page-container .my-cliams td.cover').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 30) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 27) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	/* tooltrip for cover end*/
	/* tooltrip for client-claim-ref start*/
	jQuery('.broker-view-page-container .my-cliams td.client-ref').each( function(){
		var formsName = jQuery.trim(jQuery(this).text());
		if (formsName.length > 38) {
			jQuery(this).addClass('selectpicker-long-option');
			jQuery(this).attr('alt', formsName);
			jQuery(this).html(formsName.substring(0, 36) + '...');
		}
		else {
			jQuery(this).html(formsName);
		}
	});
	/* tooltrip for client-claim-ref end*/
		/* JS for the auto trimming of claim-handler of report-claim td  */
	jQuery('.broker-view-page-container .tab-reported-claim-section td.claims-handler span.underwrt-popup-link').each( function(){
				var emailText = jQuery.trim(jQuery(this).text());
				if (emailText.length > 30) {
								jQuery(this).addClass('selectpicker-long-option');
								jQuery(this).attr('alt', emailText);
								jQuery(this).html(emailText.substring(0, 28) + '...');
				}
				else {
								jQuery(this).html(emailText);
				}
});
jQuery('.broker-view-page-container .tab-reported-claim-section td.claims-handler span.underwrt-popup-link').mouseenter( function(){
	jQuery('.custom-tooltip').remove();
	if(jQuery(this).attr('alt')) {
		jQuery('body').append('<span class="custom-tooltip">' + jQuery(this).attr('alt') + '</span>');
	}
});
jQuery('.broker-view-page-container .tab-reported-claim-section td.claims-handler span.underwrt-popup-link').mouseleave( function(){
			jQuery('.custom-tooltip').remove();
});

/* JS for the auto trimming of claim-handler report-claim td  */	
				
		/*  ######## Client view tool trip start    */
			/* tooltrip for client-claim-ref start*/
	  jQuery('.client-view-page-container .my-cliams td.client-ref').each( function(){
			var formsName = jQuery.trim(jQuery(this).text());
			if (formsName.length > 38) {
				jQuery(this).addClass('selectpicker-long-option');
				jQuery(this).attr('alt', formsName);
				jQuery(this).html(formsName.substring(0, 36) + '...');
			}
			else {
				jQuery(this).html(formsName);
			}
		});
		/* tooltrip for client-claim-ref end*/
		/* tooltrip for client-type start*/
		jQuery('.client-view-page-container .my-cliams td.client-type').each( function(){
			var formsName = jQuery.trim(jQuery(this).text());
			if (formsName.length > 42) {
				jQuery(this).addClass('selectpicker-long-option');
				jQuery(this).attr('alt', formsName);
				jQuery(this).html(formsName.substring(0, 40) + '...');
			}
			else {
				jQuery(this).html(formsName);
			}
		});
		/* tooltrip for client-type end*/
	
		/* tooltrip for obj-name start*/
		jQuery('.client-view-page-container .my-cliams td.obj-name').each( function(){
			var formsName = jQuery.trim(jQuery(this).text());
			if (formsName.length > 30) {
				jQuery(this).addClass('selectpicker-long-option');
				jQuery(this).attr('alt', formsName);
				jQuery(this).html(formsName.substring(0, 28) + '...');
			}
			else {
				jQuery(this).html(formsName);
			}
		});
		/* tooltrip for obj-name end*/
	
		/* tooltrip for cover start*/
		jQuery('.client-view-page-container .my-cliams td.cover').each( function(){
			var formsName = jQuery.trim(jQuery(this).text());
			if (formsName.length > 30) {
				jQuery(this).addClass('selectpicker-long-option');
				jQuery(this).attr('alt', formsName);
				jQuery(this).html(formsName.substring(0, 28) + '...');
			}
			else {
				jQuery(this).html(formsName);
			}
		});
		/* tooltrip for cover end*/
		/* JS for the auto trimming of claim-handler of report-claim td  */
		jQuery('.client-view-page-container .tab-reported-claim-section td.claims-handler span.underwrt-popup-link').each( function(){
					var emailText = jQuery.trim(jQuery(this).text());
					if (emailText.length > 46) {
									jQuery(this).addClass('selectpicker-long-option');
									jQuery(this).attr('alt', emailText);
									jQuery(this).html(emailText.substring(0, 44) + '...');
					}
					else {
									jQuery(this).html(emailText);
					}
	});
	jQuery('.client-view-page-container .tab-reported-claim-section td.claims-handler span.underwrt-popup-link').mouseenter( function(){
		jQuery('.custom-tooltip').remove();
		if(jQuery(this).attr('alt')) {
			jQuery('body').append('<span class="custom-tooltip">' + jQuery(this).attr('alt') + '</span>');
		}
	});
	jQuery('.client-view-page-container .tab-reported-claim-section td.claims-handler span.underwrt-popup-link').mouseleave( function(){
				jQuery('.custom-tooltip').remove();
	});
	
	/* JS for the auto trimming of claim-handler report-claim td  */    
		 /*  ######## Client view tool trip end */
	/* $$$$$$$$$$$$$ tooltrip for claim details page sub-claims start $$$$$$$$$$$$$$$$$$$ */
	jQuery('.sub-claims-section.pi td.clim-type, .sub-claims-section.pi td.claim-details').each( function(){
					var emailText = jQuery.trim(jQuery(this).text());
					if (emailText.length > 38) {
									jQuery(this).addClass('selectpicker-long-option');
									jQuery(this).attr('alt', emailText);
									jQuery(this).html(emailText.substring(0, 36) + '...');
					}
					else {
									jQuery(this).html(emailText);
					}
	});

	jQuery('.sub-claims-section.pi td.claim-handler span.underwrt-popup-link').each( function(){
					var emailText = jQuery.trim(jQuery(this).text());
					if (emailText.length > 44) {
									jQuery(this).addClass('selectpicker-long-option');
									jQuery(this).attr('alt', emailText);
									jQuery(this).html(emailText.substring(0, 42) + '...');
					}
					else {
									jQuery(this).html(emailText);
					}
	});

	jQuery('.sub-claims-section.pi td.client-claim-ref').each( function(){
					var emailText = jQuery.trim(jQuery(this).text());
					if (emailText.length > 30) {
									jQuery(this).addClass('selectpicker-long-option');
									jQuery(this).attr('alt', emailText);
									jQuery(this).html(emailText.substring(0, 28) + '...');
					}
					else {
									jQuery(this).html(emailText);
					}
	});
	jQuery('.sub-claims-section.pi td.claim-handler span.underwrt-popup-link').mouseenter( function(){
		jQuery('.custom-tooltip').remove();
		if(jQuery(this).attr('alt')) {
			jQuery('body').append('<span class="custom-tooltip">' + jQuery(this).attr('alt') + '</span>');
		}
	});
	jQuery('.sub-claims-section.pi td.claim-handler span.underwrt-popup-link').mouseleave( function(){
				jQuery('.custom-tooltip').remove();
	});
	/* $$$$$$$$$$$$$ tooltrip for claim details page sub-claims end $$$$$$$$$$$$$$$$$$$ */
	
/* %%%%%%%%%%%%%%% Claims Module %%%%%%%%%%%%%%%% */
		/* JS for the auto trimming of Address Book page's tab title */
jQuery('.my-addressbook-container .tab-section-name span').each( function(){
                var titleName = jQuery.trim(jQuery(this).text());
                if (titleName.replace(/\s+$/, '').length > 45) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', titleName);
                                jQuery(this).html(titleName.substring(0, 43) + '...');
                }
                else {
                                jQuery(this).html(titleName);
                }
});
/* JS for the auto trimming of Address Book page's tab title */

/* JS for the auto trimming of Address Book page's long email 
jQuery('.my-addressbook-container .tab-grad-client-section .info-email a').each( function(){
                var emailText = jQuery.trim(jQuery(this).text());
                if (emailText.length > 26) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 24) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
});
JS for the auto trimming of Address Book page's long email */

/* JS for the auto trimming of Broker client page's long email 
jQuery('.my-addressbook-container .underwriters .info-email a, .my-addressbook-container .claim-handlers .info-email a, .my-addressbook-container .acc-contacts .info-email a').each( function(){
                var emailText = jQuery.trim(jQuery(this).text());
                if (emailText.length > 26) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 24) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
});
 JS for the auto trimming of Address Book page's long email */

/* JS for the auto trimming of underwriter popup's long email */
jQuery('.underwriter-popup .under-popup-section .sec-details a').each( function(){
                var emailText = jQuery.trim(jQuery(this).text());
                if (emailText.length > 22) {
                                jQuery(this).addClass('selectpicker-long-option');
                                jQuery(this).attr('alt', emailText);
                                jQuery(this).html(emailText.substring(0, 20) + '...');
                }
                else {
                                jQuery(this).html(emailText);
                }
});
/* JS for the auto trimming of underwriter popup's long email */	
	/* Provides the tooltip using jquery UI tooltip plugin */
		/* Controls the underwriter popup 
	jQuery('.underwriter > a').click( function(e){
		e.preventDefault();
		jQuery('.underwriter-popup').remove();
		jQuery(this).append('<div class="underwriter-popup"><div class="und-popup-pointer"></div><div class="und-popup-wrapper"><div class="under-popup-section"><div class="sec-heading">Direct Line : </div><div class="sec-details">+47 37 01 93 47</div></div><div class="under-popup-section"><div class="sec-heading">Mobile Phone : </div><div class="sec-details">+47 97559347</div></div><div class="under-popup-section"><div class="sec-heading">Email: </div><div class="sec-details"><a href="mailto:torgrim.andersen@gard.no">torgrim.andersen@gard.no</a></div></div><div class="under-popup-section"><div class="sec-heading">Office : </div><div class="sec-details">Lillesand</div></div></div></div>');
	});
	var undElement = jQuery('.underwriter > a');
	jQuery(document).bind('mouseup', function (e){
		if (undElement.has(e.target).length === 0) {
			jQuery('.underwriter-popup').remove();
		}
	});*/
	/* Controls the underwriter popup */

	/* Controls the portfolio object detail edit popup */
	jQuery('.details-sections-edit').click( function(){
		jQuery('.overley, .portfolio-object-detail-edit-popup').show();
	});
	/* Controls the portfolio object detail edit popup */
		/* CSS for date picker in report new claimdol-field 
	jQuery('.dol-field').datepicker({
		maxDate: new Date(),
		dateFormat:'dd/mm/yy',
		showOn: "both",
		//buttonImage: "../images/icon_calendar.png",
		dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
		beforeShow: function(input, inst) {
			inst.dpDiv.css({marginTop: '-8px'});
		}
	});
	/* CSS for date picker in report new claimdol-field */
		/* CSS for date picker in report new claimdol-field 
		maxDate: new Date(),	
		dateFormat:'dd/mm/yy',
		showOn: "both",
		//buttonImage: "../images/icon_calendar.png",
		dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
		beforeShow: function(input, inst) {
			inst.dpDiv.css({marginTop: '-8px'});
		}
	});
	/* CSS for date picker in report new claimdol-field */
	/* close error block */
		var undElement = jQuery('.contact-field-holder .contact-submit-error');
	jQuery(document).bind('mouseup', function (e){
		if (undElement.has(e.target).length === 0) {
			jQuery(undElement).hide();
		}
	});
	/* JS for file names in report new claim */
	jQuery('.selected-files-container span.file-name').each( function(){
		var fileName = jQuery.trim(jQuery(this).text());
		if (fileName.length > 13) {
			jQuery(this).text(fileName.substring(0, 13) + '...');
			jQuery(this).attr('alt', fileName);
		}
	});
	/* JS for file names in report new claim */

	/* Controls the title length and the filter button on Address Book page */
	var compressedWidth = jQuery('.add-title-cont-holder').width();
	jQuery('.add-client-tab-title').css('max-width', compressedWidth);
	/* Controls the title length and the filter button on Address Book page */

	/* Restricts the number, arrow, backspace and delete button for the pagination */
	jQuery('.page-no.pager-section > input[type="text"]').keypress( function(event){
		return validatePagination(event);
	});
	/* Restricts the number, arrow, backspace and delete button for the pagination */
	/* Restricts the number, arrow, backspace and delete button for the IMO */
	jQuery('.alternate-imo-no > input[type="text"]').keypress( function(event){
		return validatePagination(event);
	});
	/* Restricts the number, arrow, backspace and delete button for the IMO */
	/* Restricts whitespace */
	jQuery('.no-space-allow > input[type="text"]').keypress( function(event){
		return validateNoSpacePagination(event);
	});
	/* Restricts whitespace */
	
	/* Deselect any text selection while choosing any picklist data */
	jQuery('.select-wrapper .btn-group button.multiselect, .select-wrapper .dropdown-toggle.selectpicker').click( function(){
					document.getSelection().removeAllRanges();
	});
	/* Deselect any text selection while choosing any picklist data */
	/* Autocomplete of text box in chrome has been blocked */
	if(navigator.userAgent.toLowerCase().indexOf("chrome") >= 0){
		jQuery("input").attr('autocomplete','off');
	}
	/* Autocomplete of text box in chrome has been blocked */
	/* darg and drop text restricted */
	jQuery("body").attr('ondragstart','return false;');
	jQuery("body").attr('ondrop','return false;');
	/* darg and drop text restricted */
	/* Restricts the <,> for the text input 
	jQuery('textarea,input[type="text"]').keypress( function(event){
		return validateTagScript(event);
	});
	jQuery('textarea,input[type="text"]').bind('keyup',function() {
		jQuery(this).val(jQuery(this).val().replace(/[<>]+/g, ""));
	});*/
	var incrementer = 1;
	/* ############# Custom search in select picklist single ############# 
	// JS for the normal select custom design 	
	jQuery('.selectpicker').selectpicker({
		noneResultsText: 'No match found!',
		liveSearch: true,
		searchAccentInsensitive: false
	});	

	// Default search by any word selection for the pick-list search option 
	var incrementer = 1;
	jQuery('.search-by-option-container').each( function(){
		var searchOptions = '<span class="search-by-icon-button selectpicker-long-option" alt="Search by"><span class="search-by-pointer"></span><div class="search-by-option"><div class="search-by-text">Search by<span class="close-search-by"></span></div><div class="search-options"><input class="any-word" id="search-by-any-word' + incrementer + '" type="radio" value="0" name="search-option' + incrementer + '" checked><label for="search-by-any-word' + incrementer + '">Any word</label></div><div class="search-options"><input class="exact-phrase" id="search-by-exact-phrase' + incrementer + '" type="radio" value="1" name="search-option' + incrementer + '"><label for="search-by-exact-phrase' + incrementer + '">Exact phrase</label></div></div></span>';
		jQuery(this).html(searchOptions);
		incrementer++;
	});
	// Default search by any word selection for the pick-list search option 

	
	// Controls the opening/closing feature for the search by popup. 
	jQuery('.search-by-icon-button').click( function(){
		jQuery(this).children().show();
	});
	var searchByElement = jQuery('.search-by-icon-button');
	jQuery(document).bind('mouseup', function (e){
		if (searchByElement.has(e.target).length === 0) {
			jQuery(searchByElement).children().hide();
		}
	});
	// Controls the opening/closing feature for the search by popup. 

	// Controls the search by option popup for the single pick-list 
	jQuery('.bs-searchbox').each( function(){
		var searchOptions = '<span class="search-by-icon-button selectpicker-long-option" alt="Search by"><span class="search-by-pointer"></span><div class="search-by-option"><div class="search-by-text">Search by<span class="close-search-by"></span></div><div class="search-options"><input class="any-word" id="search-by-any-word' + incrementer + '" type="radio" value="0" name="search-option' + incrementer + '" checked><label for="search-by-any-word' + incrementer + '">Any word</label></div><div class="search-options"><input class="exact-phrase" id="search-by-exact-phrase' + incrementer + '" type="radio" value="1" name="search-option' + incrementer + '"><label for="search-by-exact-phrase' + incrementer + '">Exact phrase</label></div></div></span>';
		jQuery(this).append(searchOptions);
		incrementer++;
	});
	jQuery('.bs-searchbox .search-by-icon-button').click( function(e){
		e.stopPropagation();
		jQuery(this).children().show();
	});
	var searchByElementSingle = jQuery('.bs-searchbox .search-by-icon-button');
	jQuery(document).bind('mousedown', function (e){
		if (searchByElementSingle.has(e.target).length === 0) {
			jQuery(searchByElementSingle).children().hide();
		}
	});
	// Controls the search by option popup for the single pick-list 
	
	
	############# Custom search in select picklist single ############# */
	
	
	
	/* Controls for the portfolio UW form opener popup old 
	jQuery('.portfolio-forms-category-option label').click( function(e){
		if (e.target.nodeName == 'LABEL' && !jQuery(this).hasClass('chosen')) {
			jQuery('.portfolio-uw-opener-popup-container').hide();
			jQuery('.portfolio-forms-category-option label').each( function(){
				jQuery(this).removeClass('chosen');
			});
			jQuery(this).addClass('chosen');
			jQuery(this).children('.portfolio-uw-opener-popup-container').show();
		}
	});
	jQuery('.portfolio-uw-popup-opener-closer').click( function(e){
		jQuery(this).parent().hide();
		jQuery(this).parent().parent().removeClass('chosen');
	});
	*/
	/* Controls for the portfolio UW form opener popup */
	var portFormAction = jQuery('.form-popup-action');
	jQuery(document).on('click', function (e){
		var currentTarget = jQuery(e.target);
		if (!currentTarget.hasClass('form-popup-action') && (!currentTarget.parents().hasClass('portfolio-uw-opener-popup-container') && !currentTarget.hasClass('portfolio-uw-opener-popup-container'))) {
			portFormAction.removeClass('open').addClass('closed');
			portFormAction.children('.portfolio-uw-opener-popup-container').hide();
		}
		else if (currentTarget.hasClass('form-popup-action') && currentTarget.hasClass('open') && !currentTarget.parents().hasClass('portfolio-uw-opener-popup-container')) {
			portFormAction.removeClass('open').addClass('closed');
			portFormAction.children('.portfolio-uw-opener-popup-container').hide();
		}
		else if (currentTarget.hasClass('form-popup-action') && currentTarget.hasClass('closed')) {
			currentTarget.removeClass('closed').addClass('open');
			jQuery('.portfolio-uw-opener-popup-container').hide();
			currentTarget.children('.portfolio-uw-opener-popup-container').show();
		}
	});
	var formPopupAction = jQuery('.form-popup-action');
	jQuery(document).bind('mouseup', function (e){
		if (formPopupAction.has(e.target).length === 0) {
			formPopupAction.children('.portfolio-uw-opener-popup-container').hide();
			formPopupAction.removeClass('chosen');
		}
	});
	/* Controls the descriptive tooltip popup */
	var descTT = jQuery('.desc-tooltip');
	jQuery(document).on('click', function (e){
		e.stopPropagation();
		var currentTarget = jQuery(e.target);
		if (!currentTarget.hasClass('desc-tooltip') && (!currentTarget.parents().hasClass('desc-tooltip-container') && !currentTarget.hasClass('desc-tooltip-container'))) {
			descTT.removeClass('open').addClass('closed');
			descTT.children('.desc-tooltip-container').hide();
		}
		else if (currentTarget.hasClass('desc-tooltip') && currentTarget.hasClass('open') && !currentTarget.parents().hasClass('desc-tooltip-container')) {
			descTT.removeClass('open').addClass('closed');
			descTT.children('.desc-tooltip-container').hide();
		}
		else if (currentTarget.hasClass('desc-tooltip') && currentTarget.hasClass('closed')) {
			descTT.removeClass('closed').addClass('open');
			currentTarget.children('.desc-tooltip-container').show();
		}
	});
	/* Controls the descriptive tooltip popup */
	var descTTEle = jQuery('.desc-tooltip');
	jQuery(document).bind('mouseup', function (e){
		if (descTTEle.has(e.target).length === 0) {
			jQuery(descTTEle).removeClass('selected');
			jQuery('.desc-tooltip-container').hide();
		}
	});
	/* Controls the descriptive tooltip popup */
	/************************ Flexible content height functionality starts here ***************************/
	/* Controls the flexi height container's height according to the browser window 
	var onLoadWindowHeight = jQuery(window).height();
	var flexiContainerHeight = (onLoadWindowHeight - 250);
	jQuery('.form-field-scrol-container').height(flexiContainerHeight);
	jQuery(window).resize(function(){
		var dynamicWindowHeight = jQuery(window).height();
		var dynamicflexiContainerHeight = (dynamicWindowHeight - 250);
		jQuery('.form-field-scrol-container').height(dynamicflexiContainerHeight);
	});
	Controls the flexi height container's height according to the browser window 

	Controls the flexi container's height according to the browser window in HOME page 
	var onLoadWindowHeightHome = jQuery(window).height();
	var flexiContainerHeightHome = (onLoadWindowHeightHome - 410);
	jQuery('.home-page-content-container .form-field-scrol-container').height(flexiContainerHeightHome);
	jQuery(window).resize(function(){
		var dynamicWindowHeightHome = jQuery(window).height();
		var dynamicflexiContainerHeightHome = (dynamicWindowHeightHome - 410);
		jQuery('.home-page-content-container .form-field-scrol-container').height(dynamicflexiContainerHeightHome);
	});
	 Controls the flexi container's height according to the browser window in HOME page 
	 Free content flexible feature 
	var onLoadWindHeight = jQuery(window).height();
	console.log(onLoadWindHeight);
	var flexiFreeContainerHeight = (onLoadWindHeight - 250);
	jQuery('.free-content-area').css('min-height', flexiFreeContainerHeight);
	jQuery(window).resize(function(){
					var dynamicWindowHeight = jQuery(window).height();
					var dynamicflexiContainerHeight = (dynamicWindowHeight - 250);
					jQuery('.free-content-area').css('min-height', dynamicflexiContainerHeight);
	});
	 Free content flexible feature */
	

	/* Controls the flexi height container's height according to the browser window */
	var onLoadWindowHeight = jQuery(window).height();
	// jQuery('.form-field-scrol-container').height(flexiContainerHeight);
	if (jQuery('.home-page-content-container').length <= 0) {
		if (!jQuery('.form-field-scrol-container').hasClass('free-content-area')) {
			var flexiContainerHeight = (onLoadWindowHeight - 250);
			jQuery('.form-field-scrol-container').height(flexiContainerHeight);
		}
		else {
			var flexiContainerHeight = (onLoadWindowHeight - 350);
			jQuery('.form-field-scrol-container').css('min-height', flexiContainerHeight);
		}
	}
	jQuery(window).resize(function(){
		if (jQuery('.home-page-content-container').length <= 0) {
			//console.log('diff HOME====================>>>>>>>>>');
			var dynamicWindowHeight = jQuery(window).height();
			var dynamicflexiContainerHeight = (dynamicWindowHeight - 350);
			// jQuery('.form-field-scrol-container').height(dynamicflexiContainerHeight);
			jQuery('.form-field-scrol-container').css('min-height', dynamicflexiContainerHeight);
		}
	});
	/* Controls the flexi height container's height according to the browser window */

	/* Free content flexible feature */
	var onLoadWindHeight = jQuery(window).height();
	//console.log(onLoadWindHeight);
	var flexiFreeContainerHeight = (onLoadWindHeight - 250);
	if (!jQuery('.free-content-area').hasClass('form-field-scrol-container')) {
		jQuery('.free-content-area').css('min-height', flexiFreeContainerHeight);
	}
	jQuery(window).resize(function(){
		//console.log('free====================>>>>>>>>>');
		var dynamicWindowHeight = jQuery(window).height();
		var dynamicflexiContainerHeight = (dynamicWindowHeight - 250);
		if (!jQuery('.free-content-area').hasClass('form-field-scrol-container')) {
			jQuery('.free-content-area').css('min-height', dynamicflexiContainerHeight);
		}
	});
	/* Free content flexible feature */

	/* Controls the flexi container's height according to the browser window in HOME page */
	var onLoadWindowHeightHome = jQuery(window).height();
	var flexiContainerHeightHome = (onLoadWindowHeightHome - 550);
	jQuery('.home-page-content-container .form-field-scrol-container').css('min-height', flexiContainerHeightHome);
	jQuery(window).resize(function(){
		var dynamicWindowHeightHome = jQuery(window).height();
		var dynamicflexiContainerHeightHome = (dynamicWindowHeightHome - 550);
		jQuery('.home-page-content-container .form-field-scrol-container').css('min-height', dynamicflexiContainerHeightHome);
	});
	/* Controls the flexi container's height according to the browser window in HOME page */

	/************************ Flexible content height functionality starts here ***************************/ 

	jQuery('.form-field-scrol-container').mCustomScrollbar({
		mouseWheelPixels: 20,
		autoHideScrollbar: true,
		scrollEasing: "linear",
		scrollInertia: 0,
		theme: "minimal"
	});
	/* Controls the browser window zooming usibility issues */
	jQuery(window).on('resize', function() {
		browserZoomCalculation();
		mainMenuHeightAdjust();
	});
	if (window.devicePixelRatio > 1) {
		browserZoomCalculation();
	}
	/* Controls the browser window zooming usibility issues */
	/* Controls for the floating main menu */
	jQuery('.floating-main-menu-container').mouseenter( function(){
		if (!jQuery(this).hasClass('expanded')) {
			jQuery(this).animate({
				width: 160
			}, 200, function(){
				jQuery(this).addClass('expanded');
			});
		}
	});
	jQuery('.floating-main-menu-container').mouseleave( function(){
		jQuery(this).animate({
			width: 47
		}, 200, function(){
			jQuery(this).removeClass('expanded');
		});
	});
	/*setTimeout( function(){
		var staticFooterHeight = jQuery('.footer-container').offset().top;
		console.log('staticFooterHeight---------->'+staticFooterHeight);
		jQuery('.floating-main-menu-container').height(staticFooterHeight);
	}, 1000);*/
	//mainMenuHeightAdjust();
	jQuery(window).scroll( function(){
		mainMenuHeightAdjust();
	});
	
	jQuery(window).load( function(){
		mainMenuHeightAdjust();
	});
	/* Controls for the floating main menu */
	
	/* Custom accordion feature control 
	jQuery('.accordion-opener').click( function(){
		var athis = jQuery(this);
		if (!athis.parent().hasClass('deactivated')) {
			athis.next().children().children('.claim-accordion-closed-data').hide();
			athis.next().children().children('.claim-accordion-container').show();
			var contHeight = athis.next().children('.accordion-container-holder').height();
			if (!athis.hasClass('open')) {
				athis.addClass('open');
				athis.next().animate({
					height: contHeight + 'px'
				}, 250);
			}
			else {
				athis.next().animate({
					height: 18
				}, 250, function() {
					athis.removeClass('open');
					athis.next().children().children('.claim-accordion-closed-data').fadeIn(100);
					athis.next().children().children('.claim-accordion-container').hide();
				});
			}
		}
	});
	jQuery('.continue-reimbursement-button').click( function(e){
		e.preventDefault();
		var athis = jQuery('.reimbursement-each-section .accordion-opener');
		var chosenCurrency = jQuery('.reimbursement-each-section .claim-filter-sections button.selectpicker span.filter-option').text();
		athis.next().animate({
			height: 18
		}, 250, function() {
			athis.removeClass('open').addClass('completed');
			athis.next().children().children('.claim-accordion-closed-data').children().children('.user-chosen-claim-currency').text(chosenCurrency)
			athis.next().children().children('.claim-accordion-closed-data').fadeIn(100);
			athis.next().children().children('.claim-accordion-container').hide();
			jQuery('.claim-accordion.total-calculation-reimbursement-section.deactivated').removeClass('deactivated');
			var nextContHeight = jQuery('.claim-accordion.total-calculation-reimbursement-section .accordion-container-holder').height();
			jQuery('.claim-accordion.total-calculation-reimbursement-section .accordion-opener').addClass('open').next().animate({
				height: nextContHeight
			}, 1000, function(){
				jQuery('.claim-accordion.total-calculation-reimbursement-section').removeClass('closed');
			});
		});
		
	});
	jQuery('.currency-submission button').click( function(e){
		e.preventDefault();
		var athis = jQuery('.claim-currency .accordion-opener');
		var chosenCurrency = jQuery('.claim-currency button.selectpicker span.filter-option').text();
		athis.next().animate({
			height: 18
		}, 250, function() {
			athis.removeClass('open').addClass('completed');
			athis.next().children().children('.claim-accordion-closed-data').children().children('.user-chosen-claim-currency').text(chosenCurrency)
			athis.next().children().children('.claim-accordion-closed-data').fadeIn(100);
			athis.next().children().children('.claim-accordion-container').hide();
			jQuery('.claim-accordion.reimbursement-each-section.deactivated').removeClass('deactivated');
			var nextContHeight = jQuery('.claim-accordion.reimbursement-each-section .accordion-container-holder').height();
			jQuery('.claim-accordion.reimbursement-each-section .accordion-opener').addClass('open').next().animate({
				height: nextContHeight
			}, 1000, function(){
				jQuery('.claim-accordion.reimbursement-each-section').removeClass('closed');
			});
		});
		
	});
	jQuery('.claim-accordion-data-changer a').click( function(e){
		e.preventDefault();
		var athis = jQuery(this).parent().parent().parent().parent().prev();
		athis.next().children().children('.claim-accordion-closed-data').hide();
		athis.next().children().children('.claim-accordion-container').show();
		var contHeight = athis.next().children('.accordion-container-holder').height();
		if (!athis.hasClass('open')) {
			athis.addClass('open');
			athis.next().animate({
				height: contHeight + 'px'
			}, 250);
		}
	});*/
	/* Custom accordion feature control */
	/* HOME page social link hover feature */
	jQuery('.social-link').mouseenter( function(){
		jQuery(this).animate({
			width: 31
		}, 100);
	});
	jQuery('.social-link').mouseleave( function(){
		jQuery(this).animate({
			width: 24
		}, 100);
	});
	/* HOME page social link hover feature */

	/* HOME page update tab controller */
	jQuery('.update-each').click( function(){
		jQuery('.update-each').each( function(){
			jQuery(this).removeClass('active');
		});
		if (jQuery(this).hasClass('claim-updates')) {
			jQuery('.home-page-update-container .tab-sigle-section').hide();
			jQuery(this).addClass('active');
			jQuery('.claim-update-content-area').show();
		}
		else if (jQuery(this).hasClass('cover-updates')) {
			jQuery('.home-page-update-container .tab-sigle-section').hide();
			jQuery(this).addClass('active');
			jQuery('.cover-update-content-area').show();
		}
		else if (jQuery(this).hasClass('register-claim')) {
			jQuery('.home-page-update-container .tab-sigle-section').hide();
			jQuery(this).addClass('active');
			jQuery('.cover-update-content-area').show();
		}
	});
	/* HOME page update tab controller */
	/* HOME page social link hover feature */
	jQuery('.social-link').mouseenter( function(){
		jQuery(this).animate({
			width: 31
		}, 100);
	});
	jQuery('.social-link').mouseleave( function(){
		jQuery(this).animate({
			width: 24
		}, 100);
	});
	/* HOME page social link hover feature */

	/* HOME page update tab controller */
	jQuery('.update-each').click( function(){
		jQuery('.update-each').each( function(){
			jQuery(this).removeClass('active');
		});
		if (jQuery(this).hasClass('claim-updates')) {
			jQuery('.home-page-update-container .tab-sigle-section').hide();
			jQuery(this).addClass('active');
			jQuery('.claim-update-content-area').show();
		}
		else if (jQuery(this).hasClass('cover-updates')) {
			jQuery('.home-page-update-container .tab-sigle-section').hide();
			jQuery(this).addClass('active');
			jQuery('.cover-update-content-area').show();
		}
		else if (jQuery(this).hasClass('register-claim')) {
			jQuery('.home-page-update-container .tab-sigle-section').hide();
			jQuery(this).addClass('active');
			jQuery('.cover-update-content-area').show();
		}
	});
	/* HOME page update tab controller */
	/* JS for the auto trimming of Client in client login name start*/
		jQuery('.emptyPanelStyle').each( function(){          
						var emailText = jQuery.trim(jQuery(this).text());
						if (emailText.length > 28) { 
										jQuery(this).attr('alt', emailText);
										jQuery(this).html(emailText.substring(0, 26) + '...');
						}
						else {
										jQuery(this).html(emailText);
						}
		});
		/* JS for the auto trimming of underwriter popup long email */    
		/* Provides the tooltip using jquery UI tooltip plugin */
		jQuery('.emptyPanelStyle').mouseenter( function(){
			jQuery('.custom-tooltip').remove();
			if(jQuery(this).attr('alt')) {
				jQuery('body').append('<span class="custom-tooltip">' + jQuery(this).attr('alt') + '</span>');
			}
		});
		jQuery('.emptyPanelStyle').mouseleave( function(){
			jQuery('.custom-tooltip').remove();
		});
	/* JS for the auto trimming of Client in client login name  end */
	

	/* HOME page timebar date calculation 
	var todayTimestamp = jQuery('.renewal-timebar-section input[type="hidden"].today-date-holder').val();
	var jsDate = new Date(todayTimestamp*1000);
	var currentDay = jsDate.getDate() ;
	var currentMonth = jsDate.getMonth() + 1;
	var currentYear = jsDate.getFullYear();
	jQuery('.renewal-timebar-today-section .today-date').text(currentDay + '-' + currentMonth + '-' + currentYear);
	 HOME page timebar date calculation */
	
	/* Control for custom datepicker 
	jQuery('.custom-datepicker').Zebra_DatePicker({
	  direction: true,
	  format: 'd-m-Y',
	  first_day_of_week: 0
	});
	jQuery('.custom-datepicker').data('Zebra_DatePicker');
	*/
	/*jQuery('.portfolio-uw-popup-opener-onlinefill').click( function(){
		if (jQuery(this).hasClass('chartered-vessels')) {
			jQuery('.uw-form-for-chartered-vessel').show();
			jQuery('.uw-form-for-chartered-vessel .multistep-content-holder').each( function(){
				jQuery(this).hide();
			});
			jQuery('.uw-form-for-chartered-vessel .multistep-content-holder:first-child').show();
			jQuery('.uw-form-for-chartered-vessel .multistep-specifier-container li').each( function(){
				jQuery(this).removeClass('selected').addClass('deselected');
			});
			jQuery('.uw-form-for-chartered-vessel .multistep-specifier-container li:first-child').addClass('selected').removeClass('deselected');
			jQuery('.overley').show();
		}
		else if (jQuery(this).hasClass('ship-owner')) {
			jQuery('.uw-form-for-ship-owner').show();
			jQuery('.uw-form-for-ship-owner .multistep-content-holder').each( function(){
				jQuery(this).hide();
			});
			jQuery('.uw-form-for-ship-owner .multistep-content-holder:first-child').show();
			jQuery('.uw-form-for-ship-owner .multistep-specifier-container li').each( function(){
				jQuery(this).removeClass('selected').addClass('deselected');
			});
			jQuery('.uw-form-for-ship-owner .multistep-specifier-container li:first-child').addClass('selected').removeClass('deselected');
			jQuery('.overley').show();
		}
		else if (jQuery(this).hasClass('mou')) {
			jQuery('.uw-form-for-mou').show();
			jQuery('.uw-form-for-mou .multistep-content-holder').each( function(){
				jQuery(this).hide();
			});
			jQuery('.uw-form-for-mou .multistep-content-holder:first-child').show();
			jQuery('.uw-form-for-mou .multistep-specifier-container li').each( function(){
				jQuery(this).removeClass('selected').addClass('deselected');
			});
			jQuery('.uw-form-for-mou .multistep-specifier-container li:first-child').addClass('selected').removeClass('deselected');
			jQuery('.overley').show();
		}
		else if (jQuery(this).hasClass('small-craft')) {
			jQuery('.uw-form-for-small-craft').show();
			jQuery('.uw-form-for-small-craft .multistep-content-holder').each( function(){
				jQuery(this).hide();
			});
			jQuery('.uw-form-for-small-craft .multistep-content-holder:first-child').show();
			jQuery('.uw-form-for-small-craft .multistep-specifier-container li').each( function(){
				jQuery(this).removeClass('selected').addClass('deselected');
			});
			jQuery('.uw-form-for-small-craft .multistep-specifier-container li:first-child').addClass('selected').removeClass('deselected');
			jQuery('.overley').show();
		}
		else if (jQuery(this).hasClass('charter-clients')) {
			jQuery('.uw-form-for-charterers').show();
			jQuery('.uw-form-for-charterers .multistep-content-holder').each( function(){
				jQuery(this).hide();
			});
			jQuery('.uw-form-for-charterers .multistep-content-holder:first-child').show();
			jQuery('.uw-form-for-charterers .multistep-specifier-container li').each( function(){
				jQuery(this).removeClass('selected').addClass('deselected');
			});
			jQuery('.uw-form-for-charterers .multistep-specifier-container li:first-child').addClass('selected').removeClass('deselected');
			jQuery('.overley').show();
		}
		else if (jQuery(this).hasClass('offshore-object')) {
			jQuery('.uw-form-for-off-vesl').show();
			jQuery('.uw-form-for-off-vesl .multistep-content-holder').each( function(){
				jQuery(this).hide();
			});
			jQuery('.uw-form-for-off-vesl .multistep-content-holder:first-child').show();
			jQuery('.uw-form-for-off-vesl .multistep-specifier-container li').each( function(){
				jQuery(this).removeClass('selected').addClass('deselected');
			});
			jQuery('.uw-form-for-off-vesl .multistep-specifier-container li:first-child').addClass('selected').removeClass('deselected');
			jQuery('.overley').show();
		}
		else if (jQuery(this).hasClass('ri-declaration')) {
			jQuery('.uw-form-for-ri-declaration').show();
			jQuery('.uw-form-for-ri-declaration .multistep-content-holder').each( function(){
				jQuery(this).hide();
			});
			jQuery('.uw-form-for-ri-declaration .multistep-content-holder:first-child').show();
			jQuery('.uw-form-for-ri-declaration .multistep-specifier-container li').each( function(){
				jQuery(this).removeClass('selected').addClass('deselected');
			});
			jQuery('.uw-form-for-ri-declaration .multistep-specifier-container li:first-child').addClass('selected').removeClass('deselected');
			jQuery('.overley').show();
		}
		else if (jQuery(this).hasClass('itope')) {
			jQuery('.uw-form-for-itope').show();
			jQuery('.uw-form-for-itope .multistep-content-holder').each( function(){
				jQuery(this).hide();
			});
			jQuery('.uw-form-for-itope .multistep-content-holder:first-child').show();
			jQuery('.uw-form-for-itope .multistep-specifier-container li').each( function(){
				jQuery(this).removeClass('selected').addClass('deselected');
			});
			jQuery('.uw-form-for-itope .multistep-specifier-container li:first-child').addClass('selected').removeClass('deselected');
			jQuery('.overley').show();
		}
	});*/
	/* Controls for the portfolio UW form opener popup */
	
		/* Controls the table sort icon chnage 
	jQuery('.sort-asc, .sort-desc').click( function(){
		if (!jQuery(this).parent().hasClass('unsorted')) {
			jQuery(this).toggleClass('sort-desc');
		}
	});
	jQuery('.unsorted .sort-asc').click( function(){
		jQuery(this).next().remove();
		jQuery(this).parent().removeClass('unsorted');
	});
	jQuery('.unsorted .sort-desc').click( function(){
		jQuery(this).addClass('sort-asc');
		jQuery(this).prev().remove();
		jQuery(this).parent().removeClass('unsorted');
	});
	 Controls the table sort icon chnage */
	/* unsaved message for report new claim */	
	jQuery('.dummy-link-unsaved').click( function(){
		if (typeof jQuery('.new-claim-deletion-confirm-unsave') != typeof undefined) {
			jQuery('.new-claim-deletion-confirm-unsave,.overley').show();
		}
	});
	/* unsaved message for report new claim */	
	
	/* hide document tab in claimdetails for bloker login*/
	jQuery('.broker-view-page-container .claim-details-page-main-container .document-claim-specifier.acumulator-specifier').remove();
	/* hide document tab in claimdetails for bloker login*/
	
});


/**
 * Function name: validatePagination()
 * - Restricts the character entry only to numbers, backspace, delete, home, end, right
 *   and left arrow and the enter button.
 *
 * @param:
 * - event(object): Carries the pressed key details.
 *
 * @return:
 * - true if matchs the key codes
 * - false if doesn't match the key codes
 */
function validatePagination(event, enteredVal) {
    var key = window.event ? event.keyCode : event.which;

	// Checks for numbers, backspace, delete, home, end, right and left arrow and the enter button.
	if (event.keyCode == 8 || event.keyCode == 9 ||event.keyCode == 13 || event.keyCode == 35 ||
	event.keyCode == 36 || event.keyCode == 37 || event.keyCode == 46 || event.keyCode == 39) {
		return true;
	}
	else if ( key < 48 || key > 57 ) { // For other key press.
		return false;
	}
	else {
		return true;
	}
}
function validateNoSpacePagination(event, enteredVal) {
    var key = window.event ? event.keyCode : event.which;

	// Checks for white space.
	if (event.keyCode == 32 ) {
		return false;
	}
	else {
		return true;
	}
}
/**
 * Function name: validateTagScript()
 * - Restricts the character entry only to <, >
 *   and left arrow and the enter button.
 *
 * @param:
 * - event(object): Carries the pressed key details.
 *
 * @return:
 * - true if matchs the key codes
 * - false if doesn't match the key codes
 */
function validateTagScript(event, enteredVal) {
    var key = window.event ? event.keyCode : event.which;

	// Checks for <,>.
	var charStr = String.fromCharCode(key);
    if (charStr == "<" || charStr == ">") {		
        return false;
    }	
	else {	
		return true;
	 }
}
/**
* Function name: browserZoomCalculation()
* - Checks the browser zoom level and accordingly sets the page elements.
*
* @param:
* - No parameter is passed.
*
* @return:
* - Nothing returns.
*/
function browserZoomCalculation() {
	// When the browser zoom lavel is more that 105%
	if (window.devicePixelRatio > 1) {
		jQuery('.header-container').css({
			position: 'absolute',
			top: 0
		});
		jQuery('.header-container').css({
			top: jQuery(window).scrollTop()
		})
		jQuery(window).scroll( function(){
			jQuery('.header-container').css({
				top: jQuery(window).scrollTop()
			});
		});
		jQuery('.global-user-select-wrapper-section .select-wrapper .btn-group ul.multiselect-container').removeClass('zommed-out');
		jQuery('.renewal-timebar-container .renewal-timebar-section-container').removeClass('zoomed-out-timebar');
	}
	// When the browser zoom lavel is at it's default level (means 100%)
	else if (window.devicePixelRatio == 1) {
		jQuery('.header-container').css({
			position: 'fixed',
			top: 0
		});
		jQuery(window).scroll( function(){
			jQuery('.header-container').css({
				top: 0
			});
		});
		jQuery('.global-user-select-wrapper-section .select-wrapper .btn-group ul.multiselect-container').removeClass('zommed-out');
		jQuery('.renewal-timebar-container .renewal-timebar-section-container').removeClass('zoomed-out-timebar');
	}
	// When the browser zoom lavel is less than default level (means 100%)
	else if (window.devicePixelRatio < 1) {
		jQuery('.global-user-select-wrapper-section .select-wrapper .btn-group ul.multiselect-container').addClass('zommed-out');
		jQuery('.renewal-timebar-container .renewal-timebar-section-container').addClass('zoomed-out-timebar');
	}
}

/**
 * Function name: mainMenuHeightAdjust()
 * - Adjust the main menu height.
 *
 * @param:
 * - No parameter is passed.
 *
 * @return:
 * - Nothing returns.
 */
function mainMenuHeightAdjust() {
	var scrollingFooterHeight = jQuery('.footer-container').offset().top;
	var windowRelTop = jQuery(window).scrollTop();
	var menuPos = scrollingFooterHeight - windowRelTop;
	jQuery('.floating-main-menu-container').height(menuPos + 1);
}
/* use to test currency format */
function testcurrency(v){
	//var regexcurrency = /^-?(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+)?(\.[0-9]{1,2})?$/;
	var regexcurrency = /^-?([0-9]+)?(\.[0-9]{1,2})?$/;
	return regexcurrency.test(v);
}
/* use to test currency format */
/* use to test uploaded file format */
/*Commented for SF-4811 STARTS
function isvalidfiletype(t){	
	var acceptfiletype = /^.*\.(([dD][oO][cC])|([dD][oO][cC][xX])|([xX][lL][sS])|([xX][lL][sS][xX])|([pP][pP][tT])|([pP][pP][tT][xX])|([jJ][pP][gG])|([pP][dD][fF])|([pP][aA][gG][eE][sS])|([pP][sS][dD])|([dD][xX][fF])|([eE][pP][sS])|([pP][sS])|([xX][pP][sS])|([aA][iI])|([tT][iI][fF][fF])|([sS][vV][gG])|([tT][tT][fF]))$/;
	return acceptfiletype.test(t);
}
Commented for SF-4811 ENDS*/

//Edited for SF-4811
function isvalidfiletype(t){	
	var acceptfiletype = /^.*\.(([dD][oO][cC])|([dD][oO][cC][xX])|([xX][lL][sS])|([xX][lL][sS][xX])|([pP][pP][tT])|([pP][pP][tT][xX])|([jJ][pP][gG])|([pP][dD][fF])|([pP][aA][gG][eE][sS])|([pP][sS][dD])|([dD][xX][fF])|([eE][pP][sS])|([pP][sS])|([xX][pP][sS])|([aA][iI])|([tT][iI][fF][fF])|([sS][vV][gG])|([tT][tT][fF])|([pP][nN][gG]))$/;
	return acceptfiletype.test(t);
}