jQuery(document).ready( function(){
	/* Script to control popup closing feature */
	jQuery('.personal-info-edit').click( function(){
		jQuery('.overley, #contact-info-pop-container').show();
	});
	jQuery('.popup-close').click( function(){
		jQuery(this).parent().hide();
		jQuery('.overley').hide();
	});

	jQuery('.comp-info-edit').click( function(){
		jQuery('.overley, #company-info-pop-container').show();
	});
	jQuery('.popup-close').click( function(){
		jQuery(this).parent().hide();
		jQuery('.overley').hide();
	});

	/* Restricts the last breadcrumb to be navigated */
	jQuery('#breadcrumbs .breadcrumbs-wrapper a:last-child').click( function(e){
		e.preventDefault();
	});

	/* Controls the tabular view of address book */
	jQuery('.single-tab').click( function(){
		if(jQuery(this).hasClass('clients') && jQuery(this).hasClass('deselected')) {
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.gard-cont.selected').removeClass('selected').addClass('deselected');
			jQuery('.tab-details-section .tab-grad-cont-section').hide();
			jQuery('.tab-details-section .tab-grad-client-section').show();
			/*jQuery('.tab-details-section .tab-section-name').text('Client List');*/
		}
		if(jQuery(this).hasClass('gard-cont') && jQuery(this).hasClass('deselected')) {
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.clients.selected').removeClass('selected').addClass('deselected');
			jQuery('.tab-details-section .tab-grad-cont-section').show();
			jQuery('.tab-details-section .tab-grad-client-section').hide();
			/*jQuery('.tab-details-section .tab-section-name').text('GARD Contacts');*/
		}
		if(jQuery(this).hasClass('open-claims') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.single-tab.all-claims').removeClass('reported-claim-active');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .tab-open-claim-section').show();
			jQuery('.tab-details-section .tab-section-name').text('Open Claims');
			jQuery('#breadcrumbs .breadcrumbs-wrapper a:last-child').text('Open Claims');
		}
		if(jQuery(this).hasClass('all-claims') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.single-tab.all-claims').removeClass('reported-claim-active');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .tab-all-claim-section').show();
			jQuery('.tab-details-section .tab-section-name').text('All Claims');
			jQuery('#breadcrumbs .breadcrumbs-wrapper a:last-child').text('All Claims');
		}
		if(jQuery(this).hasClass('reported-claims') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.single-tab.all-claims').addClass('reported-claim-active');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .tab-reported-claim-section').show();
			jQuery('.tab-details-section .tab-section-name').text('Reported Claims');
			jQuery('#breadcrumbs .breadcrumbs-wrapper a:last-child').text('Reported Claims');
		}
		if(jQuery(this).hasClass('cover') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.single-tab.object').removeClass('reported-active');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .tab-open-claim-section').show();
			jQuery('.tab-details-section .tab-section-name').text('My Covers');
			jQuery('#breadcrumbs .breadcrumbs-wrapper a:last-child').text('My Covers');
		}
		if(jQuery(this).hasClass('object') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.single-tab.object').removeClass('reported-active');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .tab-all-claim-section').show();
			jQuery('.tab-details-section .tab-section-name').text('My Objects');
			jQuery('#breadcrumbs .breadcrumbs-wrapper a:last-child').text('My Objects');
		}
		if(jQuery(this).hasClass('portfolio-report') && jQuery(this).hasClass('deselected')) {
			jQuery('.single-tab.selected').removeClass('selected').addClass('deselected');
			jQuery(this).removeClass('deselected').addClass('selected');
			jQuery('.single-tab.object').addClass('reported-active');
			jQuery('.tab-detail-columns .tab-detail-container').hide();
			jQuery('.tab-detail-columns .tab-reported-claim-section').show();
			jQuery('.tab-details-section .tab-section-name').text('My Portfolio Reports');
			jQuery('#breadcrumbs .breadcrumbs-wrapper a:last-child').text('My Portfolio Reports');
		}
	});

	/* Controls the client detail contact popup */
	jQuery('.client-link-details-option').click( function(){
		jQuery('.client-contact-details-popup, .overley').show();
	});

	/* Controls the contact me popup features */
	jQuery('.acc-info.contact').click( function(){
		if(!jQuery(this).hasClass('selected')) {
			jQuery('.contact-me-container').show();
			jQuery(this).addClass('selected');
		}
	});
	
	/* Controls the contact me popup close option by clicking the Cancel button */
	jQuery('.contact-submit-field button.contact-me-cancel').click( function(){
		jQuery('.contact-me-container').hide();
		jQuery('.acc-info.contact').removeClass('selected');
	});

	/* Control for clicking else where in the html except the popup. */
	var element = jQuery('.contact-me-container');
	jQuery(document).bind('mouseup', function (e){
		if (element.has(e.target).length === 0) {
			jQuery(element).hide();
			jQuery('.acc-info.contact').removeClass('selected');
		}
	});

	/* Controls the My Account link popup */
	jQuery('.acc-info.my-account').click( function(){
		if(!jQuery(this).hasClass('selected')) {
			jQuery('.my-account-link-popup-container').show();
			jQuery(this).addClass('selected');
		}
	});

	/* Control for clicking else where in the html except the My Account link popup. */
	var accElement = jQuery('.my-account-link-popup-container');
	jQuery(document).bind('mouseup', function (e){
		if (accElement.has(e.target).length === 0) {
			jQuery(accElement).hide();
			jQuery('.acc-info.my-account').removeClass('selected');
		}
	});

	/* Controls the document upload field in report new claim page */
	jQuery('.new-claim-doc-upld input[type="file"]').change( function(){
		jQuery('.new-claim-upload-doc').trigger("click");
		jQuery('.new-claim-field.doc-upld-field').val(jQuery('.new-claim-doc-upld input[type="file"]').val());
	});
	jQuery('.new-claim-field.doc-upld-field').click( function(){
		jQuery('.new-claim-upload-doc').trigger("change");
		jQuery('.new-claim-field.doc-upld-field').val(jQuery('.new-claim-doc-upld input[type="file"]').val());
	});

	/* Controls the new user access req popup */
	jQuery('.create-user-access a.form-btn').click( function(e){
		e.preventDefault();
		jQuery('.new-user-access-req, .overley').show();
	});

	/* Controls the admin user change req popup */
	jQuery('.request-admin-user-change a.form-btn').click( function(e){
		e.preventDefault();
		jQuery('.admin-user-change-req, .overley').show();
	});

	/* Control for hover else where in the html except the popup. */
	/* $('.acc-info.contact').bind('mouseover', openSubMenu); 
	$('.acc-info.contact').bind('mouseout', closeSubMenu);  */

	/* JS for the multiselect custom design */
	/* $('.multiselect').multiselect({
		enableCaseInsensitiveFiltering: true,
		filterBehavior: 'text',
		dropRight: false,
		disableIfEmpty: true,
		includeSelectAllOption: true,
		includeSelectAllIfMoreThan: 4,
		maxHeight: 200,
		buttonText: function(options) {
			if (options.length == 0) {
				return '<div class="down-arrow"></div>';
			}
			else if (options.length > 2) {
				return options.length + ' selected <div class="down-arrow"></div>';
			}
			else {
				var selected = '';
				options.each(function() {
					selected += $(this).text() + ', ';
				});
				return selected.substr(0, selected.length -2) + '<div class="down-arrow"></div>';
			}
		}
	}); */
	/* jQuery multiselect custom design */
	jQuery('.multiselect').multiselect({
		noneSelectedText: false
	});

	/* jQuery multiselect custom design */
	jQuery(".ui-multiselect-checkboxes").mCustomScrollbar({
		/* autoDraggerLength: false */
	});
	
});

/* function openSubMenu() {
	jQuery(this).addClass('selected');
	$(this).find('.contact-me-container').css('display', 'block');
}; 

function closeSubMenu() { 
	$(this).find('div.contact-me-container').css('display', 'none'); 
	jQuery('.acc-info.contact').removeClass('selected');
}; */