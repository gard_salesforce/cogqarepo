({
	doInit : function(component, event, helper) {        
        helper.getInvitees(component, event, helper);
    },
    viewAllSurveyInvitee : function(component, event, helper) {        
        helper.viewAllSurveyInvitees(component, event, helper);
    }
})