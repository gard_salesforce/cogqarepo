({
	getInvitees  : function(component, event, helper) {
        var evtId =  component.get('v.recordId');        
        var records = [];
        var recordCount = '';
        var action = component.get('c.fetchSurveyMembers');
        action.setParams({
            "eventId":evtId
        });
        action.setCallback(this,function(response){
            if(response.getState()==='SUCCESS'){
                records = JSON.parse(response.getReturnValue());
                recordCount = records.length;
                component.set('v.surveyInvitees',records);
                component.set('v.recordCount',recordCount);
                console.log('surveyInvitees :'+component.get('v.surveyInvitees'));
                console.log('count--'+recordCount);
            }else{
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
	},
    viewAllSurveyInvitees : function(component, event, helper) {
        console.log('Showing all survey invitees');
        var evt = $A.get("e.force:navigateToComponent");       
        evt.setParams({
            componentDef:"c:AllSurveyInvitees",
            componentAttributes: {
                recordId : component.get("v.recordId")
            }
        });
        evt.fire();
    }
})