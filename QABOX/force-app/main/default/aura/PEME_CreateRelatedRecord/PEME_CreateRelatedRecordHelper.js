({    
    //Retrieves, PrePopulates about-to-be-created account/contact's fields with the calling debit_note/manning_agent record's and Opens it in create page
    openPrePopulatedRelatedRecordCreationPage : function(component,event,helper){
        console.log('inside openPrePopulatedRelatedContactCreationPage');
        const strAccount = 'Account';
        const strContact = 'Contact';
        const strDebitNote = 'PEME_Debit_note_detail__c';
        const strManninngAgent = 'PEME_Manning_Agent__c';
        
        const callerRecordId = component.get('v.recordId');
        const sObjectToBeCreated = component.get('v.sObjectToBeCreated');
        const sObjectToBeCreatedRecordTypeId = (sObjectToBeCreated === strAccount ? '012D0000000UV5g' : (sObjectToBeCreated === strContact ? '012D0000000V8ww' : '') );
        
        console.log('values callerRecordId/sObjectToBeCreated/sObjectToBeCreatedRecordTypeId - '+callerRecordId+' / '+sObjectToBeCreated+' / '+sObjectToBeCreatedRecordTypeId);
        
        var getParentRecord = component.get('c.getParentRecord');//returns an object to prepopulated fields in the new record
        getParentRecord.setParams({
            "callerRecordId" : callerRecordId,
            "sObjectToBeCreated" : sObjectToBeCreated
        });
        getParentRecord.setCallback(this,function(res){
            if(res.getState() === 'SUCCESS' || res.getState() === 'DRAFT'){
                var parentObject = res.getReturnValue();
                
                console.log('parentObject(returnValue) - '+parentObject);
                
                var parentRecordType = (callerRecordId.substring(0,3) === 'a2g' ? strDebitNote : (callerRecordId.substring(0,3) === 'a2l' ? strManninngAgent : ''));
                var defaultFieldValuesMap = {}; //to map Parent record's fields with the Accounts' fields
                var createCompanyRecordEvent = $A.get("e.force:createRecord");
                
                console.log('parentRecordType - '+parentRecordType);
                console.log('createCompanyRecordEvent - '+createCompanyRecordEvent);
                console.log('parentObject.Requested_Country__c - '+parentObject.Requested_Country__c);
                
                if(parentRecordType === strDebitNote){
                    if(sObjectToBeCreated == strAccount){
                        defaultFieldValuesMap['Name'] = parentObject.Draft_Company_Name__c;
                        defaultFieldValuesMap['Billing_Street_Address_Line_1__c'] = parentObject.Requested_Address__c;
                        defaultFieldValuesMap['Billing_Street_Address_Line_2__c'] = parentObject.Requested_Address_Line_2__c;
                        defaultFieldValuesMap['Billing_Street_Address_Line_3__c'] = parentObject.Requested_Address_Line_3__c;
                        defaultFieldValuesMap['Billing_Street_Address_Line_4__c'] = parentObject.Requested_Address_Line_4__c;
                        defaultFieldValuesMap['Site'] = parentObject.Requested_City__c;
                        defaultFieldValuesMap['BillingState__c'] = parentObject.Requested_State__c;
                        defaultFieldValuesMap['BillingPostalcode__c'] = parentObject.Requested_Zip_Code__c;
                        defaultFieldValuesMap['Country__c'] = parentObject.Requested_Country__c;
                        defaultFieldValuesMap['Manning_Agent_or_Debit_Note_ID__c'] = parentObject.Id;
                    }
                }else if(parentRecordType === strManninngAgent){
                    if(sObjectToBeCreated == strAccount){
                        defaultFieldValuesMap['Name'] = parentObject.Company_Name__c;
                        defaultFieldValuesMap['Billing_Street_Address_Line_1__c'] = parentObject.Requested_Address__c;
                        defaultFieldValuesMap['Billing_Street_Address_Line_2__c'] = parentObject.Requested_Address_Line_2__c;
                        defaultFieldValuesMap['Billing_Street_Address_Line_3__c'] = parentObject.Requested_Address_Line_3__c;
                        defaultFieldValuesMap['Billing_Street_Address_Line_4__c'] = parentObject.Requested_Address_Line_4__c;
                        defaultFieldValuesMap['Site'] = parentObject.Requested_City__c;
                        defaultFieldValuesMap['BillingState__c'] = parentObject.Requested_State__c;
                        defaultFieldValuesMap['BillingPostalcode__c'] = parentObject.Requested_Zip_Code__c;
                        defaultFieldValuesMap['Country__c'] = parentObject.Requested_Country__c;
                        defaultFieldValuesMap['Corporate_email__c'] = parentObject.Requested_Email__c
                        defaultFieldValuesMap['Phone'] = parentObject.Requested_Phone__c
                        defaultFieldValuesMap['Manning_Agent_or_Debit_Note_ID__c'] = parentObject.Id;
                    }else if(sObjectToBeCreated == strContact){
                        defaultFieldValuesMap['LastName'] = parentObject.Requested_Contact_Person__c;
                        defaultFieldValuesMap['Email'] = parentObject.Requested_Email__c;
                        defaultFieldValuesMap['MobilePhone'] = parentObject.Requested_Phone__c;
                        defaultFieldValuesMap['Manning_Agent_or_Debit_Note_ID__c'] = parentObject.Id;
                    }
                }
                
                console.log('defaultFieldValuesMap - '+defaultFieldValuesMap);
                
                //set parameters for e.force:createRecord event
                createCompanyRecordEvent.setParams({
                    "entityApiName" : sObjectToBeCreated,
                    "recordTypeId" : sObjectToBeCreatedRecordTypeId,
                    "defaultFieldValues" : defaultFieldValuesMap,
                    "panelOnDestroyCallback": function(event){
                        //$A.get("e.force:closeQuickAction").fire(); 
                        //$A.get('e.force:refreshView').fire(); 
                        console.log('Newest Code');
                        
                        var redirectBack = $A.get("e.force:navigateToSObject");	
                        redirectBack.setParams({
                            "recordId" : callerRecordId,
                            "isredirect" : true
                        });
                        $A.get('e.force:refreshView').fire(); 
                        redirectBack.fire();
                        //this.parent.location.reload();
                        
                        
                        //Added To resolve redirection problem
                        //helper.openRedirectionWindow(component,event,helper,callerRecordId,sObjectToBeCreated);
                    }
                });
                try{
                    //fire create Record Event and open new company creation page
                    createCompanyRecordEvent.fire();
                }catch(ex){
                    console.log('Create Record event failed - '+ex);
                }
            }else{
                console.log('Server Call Failed');
            }
        });
        
        $A.enqueueAction(getParentRecord);
    },
    //opens up a new component to create a delay while showing the updated record page to the user
    openRedirectionWindow : function(component,event,helper,recordId,sObjectToBeCreated){
        console.log('recordId / sObjectToBeCreated - '+recordId+' / '+sObjectToBeCreated);
        var title = 'Success!!!';
        var message = ('Your new Manning Agent '+sObjectToBeCreated+' have been successfully created!!!');
        
        var isRedirect = true;
        var slideDevName = 'detail';
        var componentEvent = $A.get("e.force:navigateToComponent");
        console.log('recordId / sObjectToBeCreated /  - '+recordId+' / '+sObjectToBeCreated);
        componentEvent.setParams({
            componentDef : "c:SuccessMessageComponent",
            componentAttributes: {
                isRedirect : isRedirect,
                goToRecord : recordId,
                slideDevName : slideDevName,
                title : title,
                message : message,
                isredirect : true
            }
        });
        console.log('recordId / sObjectToBeCreated /  - '+recordId+' / '+sObjectToBeCreated);
        //setTimeout(function(){
        $A.get("e.force:refreshView").fire();
        componentEvent.fire()
        //},100);
	}
})