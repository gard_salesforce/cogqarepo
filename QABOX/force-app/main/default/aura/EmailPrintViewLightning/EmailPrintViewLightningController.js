({
    doInit : function(component, event, helper){
        var recordId = component.get('v.recordId');
        var strEmailPrintViewURL = "/apex/EmailPrintView?Id="+recordId; 
        console.log('new code' + strEmailPrintViewURL);
        var emailPrintEvent = $A.get("e.force:navigateToURL");
        
        emailPrintEvent.setParams({
            "url": strEmailPrintViewURL
        });
        
        emailPrintEvent.fire();
        
        setTimeout(function(){
            console.log('In function');
            $A.get("e.force:closeQuickAction").fire();
     
        }, 1000);
        
    }
})