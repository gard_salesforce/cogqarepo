({
    openPopUp : function (component, event, helper) {
        component.set("v.btnValue",event.getSource().getLocalId());
        component.set("v.isOpen", true);
        console.log('Record id : '+component.get("v.recordId")+" -btn clicked- "+component.get("v.btnValue"));
    },
    closeModal : function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
     },
    fetchSelectedFiles : function(component, event, helper){
        console.log('Child selected rows : '+JSON.stringify(component.get("v.cmpSelectedRows")));
        if(component.get("v.btnValue") == 'cloneCase'){
            helper.cloneCaseWithSelectedFiles(component, event, helper);//.then(() => helper.closePopupAndShowToast(component, event, helper));
        }
        /*
        if(component.get("v.btnValue") == 'sendFiles'){
        	helper.sendSelectedFiles(component, event, helper);
        }
        */
    }
})