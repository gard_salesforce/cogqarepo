({
    /*
    sendSelectedFiles : function(component, event, helper) {
		var selectedFiles = component.get("v.cmpSelectedRows");
        console.log('selected files -- '+selectedFiles);
        var fileIds = [];
        for(var i = 0;i < selectedFiles.length;i++){
            fileIds.push(selectedFiles[i]['recordId']);
            console.log('--file IDs in loop--'+fileIds);
        }
        console.log('--file IDs--'+fileIds);
        var action = component.get("c.sendCaseFiles");
        action.setParams({
			"caseFileIds": fileIds
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('--successfully sent--');
            }
        });
        $A.enqueueAction(action);
        component.set("v.isOpen", false);
    },*/
    cloneCaseWithSelectedFiles : function(component, event, helper) {
        //return new Promise(
    	//$A.getCallback((resolve, reject) => {
        var recordId = component.get("v.recordId");
        var clonedRecordId;
		var selectedFilesForClonedCase = component.get("v.cmpSelectedRows");
        console.log('selected files in clone-- '+selectedFilesForClonedCase);
        var conDocs = [];
        var emailMsgs = [];
        for(var i = 0;i < selectedFilesForClonedCase.length;i++){
            if(selectedFilesForClonedCase[i]['conDocId'] != null && selectedFilesForClonedCase[i]['conDocId'] != '')
            	conDocs.push(selectedFilesForClonedCase[i]['conDocId']);
            else if(selectedFilesForClonedCase[i]['conDocId'] == null || selectedFilesForClonedCase[i]['conDocId'] == '')
                emailMsgs.push(selectedFilesForClonedCase[i]['recordId']);
            console.log('--file IDs in clone loop--'+conDocs);
            console.log('--email messages id in loop--'+emailMsgs);
        }
        console.log('--file IDs--'+conDocs);
        var action = component.get("c.cloneCase");
        action.setParams({
            "caseId": recordId,
			"conDocumentIdList": conDocs,
            "selectedEmailMsgsIdList": emailMsgs            
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                //clonedRecordId = response.getReturnValue();
                //component.set("v.JSONOut",response.getReturnValue());
                var JSONOut = response.getReturnValue();
                //console.log('--successfully clonedRecordId--'+JSON.parse(JSON.stringify(response.getReturnValue())));
                console.log('--record Id after cloning--'+JSONOut.recordId);
                console.log('--hasError after cloning--'+JSONOut.hasError);
                component.set("v.isOpen", false);
                if(JSONOut.hasError)
                	helper.showtoast(component, event, helper, "Warning", "Cloning successfull!", "Redirected to cloned case. One or more email messages might have crossed character limit and is truncated");
                else
                    helper.showtoast(component, event, helper, "Success", "Cloning successfull!", "Redirected to cloned case");
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": JSONOut.recordId
                    //"recordId": clonedRecordId	//component.get("v.clonedRecId"),
                    //"slideDevName": "detail"
        		});
        		navEvt.fire();
            }
        });
        
       $A.enqueueAction(action);
    },
    showtoast : function(component, event, helper, type, title, message) {
    	var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "title": title, //"Cloning successfull!",
            "message": message
        });
       toastEvent.fire();
    }/*,
    closePopupAndShowToast : function(component, event, helper) {
    	console.log('--in toast event method--'+component.get("v.clonedRecId"));
   		
        
    	toastEvent.fire();
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
        	"recordId": component.get("v.clonedRecId"),
        	"slideDevName": "detail"
        });
        navEvt.fire();   
    }*/
})