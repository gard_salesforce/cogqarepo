({
	doInit : function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
    	workspaceAPI.getFocusedTabInfo().then(function(response) {
        console.log('response: ' + JSON.stringify(response));
        //var focusedTabId = response.subtabs[0].tabId;
        if(typeof response != typeof  undefined && response != null && response.isSubtab){
            var focusedTabId = response.tabId;
        }
        console.log('focusTabId '+focusedTabId);
        workspaceAPI.setTabLabel({
            tabId: focusedTabId,
            label: "Survey Members",
            title: "Survey Members"
        });
        workspaceAPI.setTabIcon({
            tabId: focusedTabId,
            icon: "utility:user_role",
            iconAlt: "Survey Members"
        });
    })
         helper.getColumnName(component, event, helper);
        helper.getInvitees(component, event, helper);
        helper.getEvtName(component, event, helper);
    },
    updateColumnSorting: function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        // assign the latest attribute with the sorted column fieldName and sorted direction
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        console.log('field name :'+fieldName+'-Direction-'+sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);
    },
    showevtName:function(component, event, helper){
        var evId =  component.get('v.recordId'); 
        var navEvt = $A.get("e.force:navigateToSObject");
    	navEvt.setParams({
        	"recordId": evId
    	});
    	navEvt.fire();
    },
})