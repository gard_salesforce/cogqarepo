({
    
    getInvitees  : function(component, event, helper) {
        var evtId =  component.get('v.recordId');
        var records = [];
        var recordCount = '';
        var action = component.get('c.fetchSurveyMembers');
        action.setParams({
            "eventId":evtId
        });
        action.setCallback(this,function(response){
            if(response.getState()==='SUCCESS'){
                records = JSON.parse(response.getReturnValue());
                recordCount = records.length;
                for(let row of records){
                    for(let col of component.get('v.mycolumns')){
                        if(col.type=='Boolean' && row[col.fieldName]==true){
                             row[col.fieldName+'_chk'] ='utility:check';
                        }
                    }
                }
                component.set('v.surveyInvitees',records);
                component.set('v.recordCount',recordCount);
                console.log('surveyInvitees :'+component.get('v.surveyInvitees'));
                console.log('count--'+recordCount);
            }else{
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
	},
    getEvtName  : function(component, event, helper) {
        var evtId =  component.get('v.recordId');
        var eventName = '';
        var action = component.get('c.fetchEventName');
        action.setParams({
            "eventId":evtId
        });
        action.setCallback(this,function(response){
            if(response.getState()==='SUCCESS'){
                eventName = response.getReturnValue();
                component.set('v.evtName',eventName);
                console.log('Event name :'+component.get('v.evtName'));
            }else{
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
    },
    
    getColumnName : function(component, event, helper) {
		//var recordId = component.get("v.recordId");
        component.set('v.mycolumns', [
            {label: 'Invitee Name', fieldName: 'contactName', type: 'text', sortable: true},
             {label: 'KCC survey sent date', fieldName: 'responseTime', type: 'String', sortable: true},
            {label: 'Responded?', fieldName: 'responded', type: 'boolean', sortable: true,
             cellAttributes: {  iconName: { fieldName: 'responded_chk' },
                             iconPosition: 'right'
                             
                             }}
            
           
             
        ]);},
    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.surveyInvitees");
        var reverse = sortDirection !== 'asc';
        console.log('Reverse : '+reverse);
        //sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse));
        cmp.set("v.surveyInvitees", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
        function(x) {return x[field]};
        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    }
})