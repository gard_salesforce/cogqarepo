({
	fetchInviteesHelper : function(component, event, helper) {
		var recordId = component.get("v.recordId");
        component.set('v.mycolumns', [
            {label: 'Name', fieldName: 'contactName', type: 'text'}
        ]);
        var action = component.get("c.fetchInvitees");
        action.setParams({
			"eventId": recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            let errors = response.getError();
            if (state === "SUCCESS") {
                component.set("v.invitees", JSON.parse(response.getReturnValue()));
                helper.sortOnLoad(component, event, helper);
                var inviteeList = [];
                inviteeList = component.get("v.invitees");
                if(inviteeList.length > 0)
                    component.set("v.inviteesAvailable",true);
                else
                    component.set("v.InviteesNotAvailable",true);
            }
            else{
                console.log("--Sorry for the error--"+state+"-message-"+errors);
            }
        });
        console.log('--Survey invitees--'+component.get("v.invitees"));
        $A.enqueueAction(action);
    },
    sortOnLoad: function(cmp, event, helper){
        cmp.set("v.sortedBy", 'contactName');
        cmp.set("v.sortedDirection", 'desc');
        //console.log('field name :'+v.sortedDirection+'-Direction-'+v.sortedBy);
        helper.sortData(cmp, 'contactName', 'desc');
    },
    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.invitees");
        var reverse = sortDirection !== 'asc';
        console.log('Reverse : '+reverse);
        //sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse));
        cmp.set("v.invitees", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
        function(x) {return x[field]};
        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },
    getSelectedRows : function(component, event, helper){
        var selectedRows = event.getParam('selectedRows');
        if(selectedRows.length > 0)
            component.set("v.isDisabled",false);
        else
            component.set("v.isDisabled",true);
        component.set("v.cmpSelectedRows",selectedRows);
        console.log('-selected rows in child-'+component.get("v.cmpSelectedRows"));
    },
    saveSurveyInvitees : function(component, event, helper){
		var inviteeList = component.get("v.cmpSelectedRows");
    	var ConIds = [];
        for(var i = 0;i < inviteeList.length;i++){
        	ConIds.push(inviteeList[i]['contactId']);
        }
 		console.log('Selected contacts :'+ConIds);
 		var action = component.get("c.createSurveyInvitees");
		action.setParams({
			"contactIdList": ConIds,
            "eventId": component.get("v.recordId")
        });
        component.set("v.isDisabled",true);
        action.setCallback(this, function(response){
            var state = response.getState();
            let errors = response.getError();
            if (state === "SUCCESS") {
                console.log('--Invitations sent--');
                helper.showtoast(component, event, helper, "Success", "KCC Survey Invitation sent!","");
                helper.cancelAction(component, event, helper);
            }
            else{
                console.log("--Sorry for the error--"+state+"-message-"+errors);
            }
        });
        $A.enqueueAction(action);
	},
    cancelAction :  function(component, event, helper){
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    showtoast : function(component, event, helper, type, title, message) {
    	var toastEvent = $A.get("e.force:showToast");
        console.log('Toast fired--');
        toastEvent.setParams({
            "type": type,
            "title": title,
            "message": message
        });
       toastEvent.fire();
    }
})