({
    getFollowers : function(component, event, helper){
        var recordId = component.get('v.recordId');
        var thisPageNumber = component.get('v.pageNumber');
        var action = component.get('c.getPageWrappers');
        action.setParams({
            "parentId" : recordId,
            "pageNumber" : thisPageNumber
        });
        action.setCallback(this,function(response){
            if(response.getState() === 'SUCCESS' || response.getState() === 'DRAFT'){
                var thisPage = response.getReturnValue();
                
                component.set('v.hasPrevious',thisPage.hasPrevious);
                component.set('v.hasNext',thisPage.hasNext);
                component.set('v.followers',thisPage.followers);
                setTimeout(function(){
                    if(thisPage.followers.length > 0){
                        component.set('v.hasList',true);
                    }
                    component.set('v.doneProcessing',true);
                },300);
            }
        });
        $A.enqueueAction(action);
    },
    followUnfollowUser : function(component,event,helper,action){//added subjectId for SF-4923 AND replaced by helper.changeFollowing() after SF-4923
        //var selectedIndex = event.currentTarget.id;
        var selectedIndex = event.getSource().get('v.value');
        var followersList = component.get('v.followers');
        var subject = followersList[selectedIndex];
        var subjectId = subject.followerUser.id;
        var followUnfollowAction = component.get('c.followUnFollowUserServer');
        console.log('subjectId/action - '+subjectId+'/'+action);
        followUnfollowAction.setParams({
            "subjectId" : subjectId,
            "action" : action
        });
        followUnfollowAction.setCallback(this,function(res){
            if(res.getState() === 'SUCCESS' || res.getState() === 'DRAFT'){
                helper.getFollowers(component, event, helper);
                console.log(action+' Succeeded');
            }else{
                console.log(action+' Failed');
            }
        });
        $A.enqueueAction(followUnfollowAction);
    },
    goToUser : function(component,event,helper){
        var sobjectId = event.currentTarget.id;
        if (sobjectId.indexOf("005") > -1){ //Note 500 is prefix for Case Record
            var urlEvent = $A.get("e.force:navigateToSObject");
            urlEvent.setParams({
                "recordId" : sobjectId,
                "isredirect" : true
            });
            urlEvent.fire();
        }
    },
    changeFollowing : function(component, event, helper){//added for SF-4923
        console.log('helper.changeFollowing final STARTS');
        //get the subjectId, current user's follow state and "action" that is to be performed
    	var selectedIndex = event.getSource().get('v.accesskey');//temporary jugaad, later this needs to be changed when an appropriate attribute is added to the "lightning:buttonStateful" by salesforce to retain the index of a stateful button when placed in a repeater
    	var followersList = component.get('v.followers');
        var subject = followersList[selectedIndex];//selected user
    	var subjectId = subject.followerUser.id;
        var action = subject.currentUserFollows ? 'unfollow' : 'follow';//if the selected user is to be followed or unfollowed
        
        //server call to follow/unfollow subject User
    	var followUnfollowAction = component.get('c.followUnFollowUserServer');
        console.log('subjectId/action - '+subjectId+'/'+action);
    	followUnfollowAction.setParams({
            "subjectId" : subjectId,
            "action" : action
        });
        followUnfollowAction.setCallback(this,function(res){
            if(res.getState() === 'SUCCESS' || res.getState() === 'DRAFT'){
                    helper.getFollowers(component, event, helper);
                	console.log(action+' Succeeded');
            }else{
                console.log(action+' Failed');
            }
            console.log('helper.changeFollowing final ENDS');
        });
        $A.enqueueAction(followUnfollowAction);
	}
})