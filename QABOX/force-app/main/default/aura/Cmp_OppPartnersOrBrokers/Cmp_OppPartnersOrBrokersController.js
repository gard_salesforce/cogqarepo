({
    isRefreshed : function(component, event, helper) {        
        helper.getPartnersOrBrokers(component, event, helper);        
        if(component.get('v.modalPromise')){
            component.get('v.modalPromise').then(
                function (modal) {                    
                    modal.close();
                }
            ); 
        }        
    },
    doInit : function(component, event, helper) {        
        helper.getPartnersOrBrokers(component, event, helper);
    },
    viewAllPartnerOrBroker:function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");       
        evt.setParams({
            componentDef:"c:AllOppPartnersBrokers",
            componentAttributes: {
                recordId : component.get("v.recordId")
            }
        });
        evt.fire();
    },
    deletePartner:function(component, event, helper){
        var remaningPartnersOrBrokers = [];
        remaningPartnersOrBrokers = component.get('v.partnersOrBrokers');
        var action = component.get('c.deletePartnersOrBrokers');
        action.setParams({
            "partnerOrBrokerId":remaningPartnersOrBrokers[event.target.id].Id
        });
        action.setCallback(this,function(res){
            if(res.getState()==='SUCCESS' ||res.getState()==='DRAFT'){
                remaningPartnersOrBrokers.splice(event.target.id,1);        
                component.set('v.partnersOrBrokers',remaningPartnersOrBrokers); 
                var dataMap = res.getReturnValue();
                console.log('dataMap status:::'+dataMap.status);
                if(dataMap.status=='success'){
                    console.log('Deleted');
                    $A.get('e.force:refreshView').fire();
                }
            }
            else{
                console.log('Error');
            }
        });
        $A.enqueueAction(action); 
    },
    createPartnerOrBroker :function(component, event, helper){
        var oppId =  component.get('v.recordId');
        var modalBody;        
        var modalPromise = $A.createComponent('c:newPartnerOrBroker', {"partnerOrBroker":oppId}, function(content, status){
            modalBody =  content;
            var modalPromise  = component.find('overlayLib').showCustomModal({
                header: "Partners or brokers",
                body: modalBody, 
                showCloseButton: true,
                cssClass: "mymodal",
                closeCallback: function() {
                }
            });
            component.set("v.modalPromise", modalPromise);
        });
    }
})