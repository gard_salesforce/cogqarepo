({
	searchRecordsHelper : function(component, event, helper, selectedRecords) {   
        //Testing
        component.set('v.mycolumns', [
            {label: 'Name', fieldName: 'label', type: 'text'},
            {label: 'Email', fieldName: 'email', type: 'text'}
        ]);
        //Testing
        var recordId = component.get("v.recordId");
        $A.util.removeClass(component.find("Spinner"), "slds-hide");
        component.set("v.message", '');
        component.set("v.recordsList", []);
        var searchString = component.get("v.searchString");
    	var action = component.get("c.fetchRecords");
        action.setParams({
            'searchString' : searchString,
            'values' : JSON.stringify(selectedRecords)
        });
        action.setCallback(this,function(response){
        	var result = JSON.parse(response.getReturnValue());//response.getReturnValue();
        	if(response.getState() === 'SUCCESS') {
    			if(result.length > 0) {
    				// To check if value attribute is prepopulated or not
					if( $A.util.isEmpty(selectedRecords) ) {
                        var selectedRcrds = component.get("v.selectedRecords") || [];
                        for(var i = 0; i < result.length; i++) {
                            if(selectedRcrds.includes(result[i].value))
                                result[i].isSelected = true;
                        }
                        component.set("v.recordsList", result); 
                    } else {
                        component.set("v.selectedDataObj", result);
                    }
                    component.set("v.showTable", true);
    			} else {
    				component.set("v.message", "No Records Found for '" + searchString + '"');
                    component.set("v.showTable", false);
    			}
        	} else {
                // If server throws any error
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set("v.message", errors[0].message);
                }
            }
            // To open the drop down list of records
            if( $A.util.isEmpty(selectedRecords) )
                $A.util.addClass(component.find("resultsDiv"),"slds-is-open");
        	$A.util.addClass(component.find("Spinner"), "slds-hide");
        });
        $A.enqueueAction(action);
	},
    sendInvitationEmailHelper : function(component, event, helper){
        var recordId = component.get("v.recordId");
        var action = component.get("c.sendInvitation");
        var conIds = [];
        var selectedRecs = component.get("v.selectedDataObj");
        for(var i = 0;i < selectedRecs.length;i++){
            conIds.push(selectedRecs[i].value);
        }
        action.setParams({
            "claimId" : recordId,
            "selectedContactIdsList" : conIds
        });
        action.setCallback(this,function(response){
            var result = response.getReturnValue();
            if(response.getState() === 'SUCCESS') {
                helper.showtoast(component, event, helper, "Success", "Survey sent!", "Please check the claim to see the survey sent.");
                component.set("v.showTable", false);
                console.log('---Invitation sent---');
            }else{
                console.log('---Invitation send failed---');
            }
        });
        $A.enqueueAction(action);
    },
    showtoast : function(component, event, helper, type, title, message) {
    	var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "title": title,
            "message": message
        });
       toastEvent.fire();
    }
})