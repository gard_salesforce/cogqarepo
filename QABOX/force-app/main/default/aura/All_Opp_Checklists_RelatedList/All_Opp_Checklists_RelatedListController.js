({
	doInit : function(component, event, helper) {
		helper.getOppChecklists(component, event, helper);
        helper.getOppName(component, event, helper);
        helper.getLoggedInUser(component, event, helper);
	},
    gotoRecentListViewOpp : function(component, event, helper) {
		helper.goToRecentlyViewedOpp(component, event, helper);
	},
    gotoOppDetailRecord : function(component, event, helper) {
		helper.navigateToOpp(component, event, helper);
	},
    gotoOppChecklistDetailRecord : function(component, event, helper) {
		helper.navigateToOppChecklist(component, event, helper);
	},
    //Btn handlers-starts
    createOppList : function(component, event, helper) {
        //helper.navigateToOpp(component, event, helper);
        helper.createOppCheckListHelper(component, event, helper);
        helper.rectypeCall(component, event, helper);   
    },
    closeRecordTypeModal : function(component, event, helper){
        helper.closeModel(component, event, helper);
    } ,  
    onRadiohandler : function(component, event, helper){        
        helper.onRadio(component, event, helper);
    },
    createRec_PnI : function(component, event, helper){                      
        helper.createOppCheckList_PnI_Helper(component, event, helper);
    },
     //Btn handlers-ends
    /* handleSelect : function(component, event, helper){ 
        var menuValue = event.detail.menuItem.get("v.accesskey");
        //console.log('menuValue ====>> '+menuValue);
        switch(menuValue) {
            case "1":  helper.editOppChklist(component, event, helper); break;
            case "2":  helper.deleteOppChklist(component, event, helper); break;
        }
    }*/
    editRec : function(component, event, helper){
        helper.editOppChklist(component, event, helper);
    },
    deleteRec : function(component, event, helper){
        helper.deleteOppChklist(component, event, helper);
    },
    refreshComp : function(component, event, helper){
        //helper.isRefreshed(component, event, helper);
        helper.getOppChecklists(component, event, helper); 
    }
})