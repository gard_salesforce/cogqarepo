({
    getOppChecklists : function(component, event, helper) {//fetching Opp chklist records
        var recordId=component.get("v.recordId");
        var action =component.get("c.getOppChecklists");
        action.setParams({
            "oppId": recordId
        });
        var cm = {};
        action.setCallback(this,function(res){
            var state = res.getState();                     
            if(state ==="SUCCESS"){            
                cm = res.getReturnValue();                
                console.log('AXN>>'+cm);
                component.set("v.Opp_Checklists",cm);
            }
            else{
                console.log("Failed State-->"+state);              
            }
        });
        $A.enqueueAction(action);        
    },
    getOppName : function(component, event, helper){//Showing Opp Name in View All page
        var recordId=component.get("v.recordId");
        var action =component.get("c.getOpportunites");
        action.setParams({
            "recordId": recordId
        });
        var cm = {};
        action.setCallback(this,function(res){
            var state = res.getState();                     
            if(state ==="SUCCESS"){            
                cm = res.getReturnValue();                
                console.log('AXN>>'+cm);
                component.set("v.oppName",cm[cm.length-1].Name);
            }
            else{
                console.log("Failed State getting Opp-->"+state);              
            }
        });
        $A.enqueueAction(action);        
    },
    goToRecentlyViewedOpp:function(component, event, helper){//navigates to Opp Recent List view
        var hostname = window.location.hostname;
        console.log('hostname:::::::'+hostname);
        component.set("v.hostName",hostname);
        var oppRecentlyViewedUrl = 'https://'+hostname+'/lightning/o/Opportunity/list?filterName=Recent';
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": oppRecentlyViewedUrl
        });
        urlEvent.fire();
    },
    navigateToOpp:function(component, event, helper){
        var oppId =  component.get('v.recordId'); 
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": oppId
        });
        navEvt.fire();
    },
    navigateToOppChecklist : function(component, event, helper){
        console.log('==Hi==');
        var allOppChecklistRecs = [];
        allOppChecklistRecs = component.get("v.Opp_Checklists");
        console.log('IDS>>>>'+allOppChecklistRecs[event.target.id].Id);
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": allOppChecklistRecs[event.target.id].Id
        });
        navEvt.fire();
    },
    //Btn helpers-starts
    rectypeCall : function(component, event, helper) { //fetching recordtypes of Opportunity checklst
        console.log('Method 2--->');
        //component.set("v.isOpen", true);
        var action = component.get("c.getRecordTypes");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.lstRecordTypes",response.getReturnValue());
                var res = response.getReturnValue();
                var opt = [];
                response.getReturnValue().forEach(function(data,index){                
                    console.log(index+'>>>>',JSON.stringify(data));
                    opt.push({
                        'label': data.recordTypeLabel,
                        'value': data.recordTypeId
                    });
                    if(data.recordTypeId ==='012D0000000BfGMIA0'){
                        component.set("v.defaultRecType",data.recordTypeId);
                        console.log('Def rec type'+data.recordTypeId);
                    }
                });
                component.set("v.options",opt);
            } 
            else if (state == "ERROR") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Please contact your administrator"
                });
                toastEvent.fire();
            }    
        });
        $A.enqueueAction(action);
    },
    onRadio : function(component,event,helper){ //method called for radio button recordtype selection
        //      console.log(event.target.value);
        //component.find('selectedRecId').get('v.value');
        console.log('value'+component.find('selectedRecId').get('v.value'));
        component.set("v.selectedRecord",component.find('selectedRecId').get('v.value'));
        component.set('v.isDisabled',false);
        
    },  
    createOppCheckListHelper : function(component, event, helper) {  //creating new opp checklist record for Marine Opportunity
        //enabling boolean flags to display record type selection page for P&I Opportunity
        console.log('Method 1--->');
        var recordId=component.get("v.recordId");
        var action =component.get("c.getOpportunites");      
        var createOppCheckList = $A.get("e.force:createRecord");
        
        var cm = {};
        action.setParams({
            "recordId": recordId
        });
        action.setCallback(this,function(res){
            var state = res.getState();                     
            if(state ==="SUCCESS"){
                cm = res.getReturnValue();
                var oppName =cm[cm.length-1].Name;
                var trimmedOppName = oppName.substr(0,68);
                console.log(trimmedOppName);
                if(cm[cm.length-1].RecordTypeName__c ==="Marine" && cm[cm.length-1].Checklist_Count__c===0){
                    if(cm[cm.length-1].Type ==='Renewal'){
                        createOppCheckList.setParams({
                            "entityApiName": "Opportunity_Checklist__c",
                            "defaultFieldValues": {
                                'Opportunity__c' : recordId,
                                'Name':'Checklist - '+trimmedOppName                              	
                            },
                            'recordTypeId':'012D0000000V8dVIAS'
                        });
                        createOppCheckList.fire();
                    }
                    else if(cm[cm.length-1].Type ==='New Business' || cm[cm.length-1].Type ==='Up Sell'){
                        createOppCheckList.setParams({
                            "entityApiName": "Opportunity_Checklist__c",
                            "defaultFieldValues": {
                                'Opportunity__c' : recordId,
                                'Name':'Checklist - '+trimmedOppName                              	
                            },
                            'recordTypeId':'012D0000000V8dVIAS'
                        });
                        createOppCheckList.fire();
                    }
                    
                }
                else if(cm[cm.length-1].RecordTypeName__c ==="P_I"){                    
                    if(cm[cm.length-1].Type ==='New Business' || cm[cm.length-1].Type ==='Up Sell'){
                        alert("No need for checklist. You only need to create a checklist for Renewal P&I opportunities");
                    }else{
                       component.set('v.isPnIOpp',true);
                       component.set("v.isOpen", true);  
                    }
                }
                    else{
                        alert("Only one Opportunity Checklist may be created for each Opportunity");  
                    }
            }
            else{
                console.log("Failed State-->"+state);              
            }
        });
        $A.enqueueAction(action);
        //opening backdrop when recordtype selection page comes up
        var modal = component.find("oppModal");
        var modalBackdrop = component.find("oppModalBackdrop");
        $A.util.addClass(modal,"slds-fade-in-open");
        $A.util.addClass(modalBackdrop,"slds-backdrop_open");
    },
    createOppCheckList_PnI_Helper : function(component, event, helper) {//creating new opp checklist record for P_I Opportunity
        var recordId=component.get("v.recordId");
        var action =component.get("c.getOpportunites");      
        var createOppCheckList = $A.get("e.force:createRecord");
        component.set('v.isPnIOpp',false);//closing the recordtype selection modal after clicking Next btn
        component.set("v.isOpen", false); 
        var cm = {};
        action.setParams({
            "recordId": recordId
        });
        action.setCallback(this,function(res){
            var state = res.getState();                     
            if(state ==="SUCCESS"){
                cm = res.getReturnValue();
                var oppName =cm[cm.length-1].Name;
                var trimmedOppName = oppName.substr(0,68);
                //var rt = cm[cm.length-1].RecordTypeName__c;                           
                if(cm[cm.length-1].RecordTypeName__c ==="P_I"){
                    //alert('In Progress...');                  
                    var selectedrectype = component.get("v.selectedRecord");
                    if(selectedrectype !== null && selectedrectype !== ''){
                        createOppCheckList.setParams({
                            "entityApiName": "Opportunity_Checklist__c",
                            "defaultFieldValues": {
                                'Opportunity__c' : recordId,
                                'Name':'Checklist - '+trimmedOppName                              	
                            },
                            'recordTypeId':selectedrectype
                        });
                        createOppCheckList.fire();
                    }
                }
            }
            else{
                console.log("Failed State-->"+state);
            }
        });
        $A.enqueueAction(action);      
    },
    closeModel: function(component, event, helper) { //disappears the recordtype selection page on hitting close icon
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen",false );
        //disappearing the backdrop when recordtype selection page comes up
        var modal = component.find("oppModal");
        var modalBackdrop = component.find("oppModalBackdrop");
        $A.util.removeClass(modal,"slds-fade-in-open");
        $A.util.removeClass(modalBackdrop,"slds-backdrop_open");           
    },
    navigateToOpp:function(component, event, helper){
        console.log('Method3--->')
        var oppId =  component.get('v.recordId'); 
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": oppId
        });
        navEvt.fire();
    },
    //btn helpers-ends
    editOppChklist : function(component, event, helper){//navigates to record detail on clicking record hyperlink
        console.log('Edit Record Method');
        var allOppChecklistRecs = [];
        allOppChecklistRecs = component.get("v.Opp_Checklists");
        console.log('IDS>>>>'+allOppChecklistRecs[event.target.id].Id);
        var navEvt = $A.get("e.force:editRecord");
        navEvt.setParams({
            "recordId": allOppChecklistRecs[event.target.id].Id
        });
        navEvt.fire();
    },
    getLoggedInUser : function(component, event, helper){ 
        console.log('Inside Profile Id')
        var cm;
        var action =component.get("c.fetchUserInfo");        
        action.setCallback(this,function(response){
            var state= response.getState();
            if(state==="SUCCESS"){
                console.log('In success');
                cm = response.getReturnValue();
                console.log('ProfileId'+cm);
                if(cm != '00e20000001O7HlAAK' && cm != '00e20000001US4eAAG'){
                 component.set("v.showBtnMenu",false);
                }
            }else{
                console.log("Failed State getting ProfileId-->"+state);
            }
        });
      $A.enqueueAction(action);  
    },
    deleteOppChklist:function(component, event, helper){
        var OppChecklistRecsleft = [];
        OppChecklistRecsleft = component.get('v.Opp_Checklists');
        var action = component.get('c.deleteRecord');
        action.setParams({
            "oppchkId":OppChecklistRecsleft[event.target.id].Id
        });
        action.setCallback(this,function(res){
            if(res.getState()==='SUCCESS' ||res.getState()==='DRAFT'){
                OppChecklistRecsleft.splice(event.target.id,1);        
                component.set('v.Opp_Checklists',OppChecklistRecsleft); 
                var dataMap = res.getReturnValue();
                console.log('dataMap status:::'+dataMap.status);
                if(dataMap.status=='success'){
                    console.log('Deleted');
                    this.getOppChecklists(component, event, helper);
                }
            }
            else{
                console.log('Error');
            }
        });
        $A.enqueueAction(action); 
    }
})