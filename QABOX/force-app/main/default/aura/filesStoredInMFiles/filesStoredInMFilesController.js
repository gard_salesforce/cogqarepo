({
	fetchFiles : function(component, event, helper){
        console.log('do init called--'+component.get("v.recordId"));
        helper.fetchFilesHelper(component, event, helper);
        
     },
    updateColumnSorting: function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        // assign the latest attribute with the sorted column fieldName and sorted direction
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        console.log('field name :'+fieldName+'-Direction-'+sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);
    }
})