({
    fetchFilesHelper : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        component.set('v.mycolumns', [
            {label: 'File name', fieldName: 'Title', type: 'text', sortable:true},
            {label: 'File Type', fieldName: 'fileType', type: 'text', sortable:true},
            {label: 'Number of attachment(s)', fieldName: 'fileCount', type: 'text', sortable:true},
            {label: 'From address', fieldName: 'fromAdd', type: 'text', sortable:true},
            {label: 'To address', fieldName: 'toAdd', type: 'text', sortable:true},
            {label: 'Message Date', fieldName: 'msgDate', type: 'text', sortable:true},
            //{label: 'MFiles Document Id', fieldName: 'documentId', type: 'text', sortable:true} //SF-6268 added
            {label: 'MFiles Document Id', fieldName: 'MFilesLink', type: 'url', typeAttributes: {label: { fieldName: 'documentId' }, target: '_blank'}} //Changed for SF-6437
            //{label: 'File source', fieldName: 'fileSource', type: 'text', sortable:true}
        ]);
        var action = component.get("c.fetchCaseFiles");
        action.setParams({
            "caseId": recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            let errors = response.getError();
            if (state === "SUCCESS") {
                component.set("v.filesList", JSON.parse(response.getReturnValue()));
                helper.sortOnLoad(component, event, helper);
                var fileList = [];
                fileList = component.get("v.filesList");
                component.set("v.recordCount",fileList.length);
                
                if(fileList.length > 0)
                    component.set("v.filesAvailable",true);
                else
                    component.set("v.filesNotAvailable",true);
            }
            else{
                console.log("--Sorry for the error--"+state+"-message-"+errors);
            }
        });
        console.log('--Files for this case--'+component.get("v.filesList"));
        $A.enqueueAction(action);
    },
    sortOnLoad: function(cmp, event, helper){
        cmp.set("v.sortedBy", 'msgDate');
        cmp.set("v.sortedDirection", 'desc');
        //console.log('field name :'+v.sortedDirection+'-Direction-'+v.sortedBy);
        helper.sortData(cmp, 'msgDate', 'desc');
    },
    
    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.filesList");
        var reverse = sortDirection !== 'asc';
        console.log('Reverse : '+reverse);
        //sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set("v.filesList", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
        function(x) {return x[field]};
        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    }
})