({
	commentSizeCheck : function(component, event, helper) {
        if (component.isValid()) {
            var box = event.getSource();
            var text = box.get("v.value");
            component.set("v.IdeaCommentRecord.CommentBody", text);
            text = text.replace(/\n/g, "");
            var len = text.length;
            var max = component.get("v.maxlength");
            if(len <= max) {
            component.set("v.size", max-len);
            component.set('v.warningMessage',null);
            }
            else {
                component.set("v.size", 0);
                    component.set('v.warningMessage','Maximum character exceeded');
            }
        }
    },
})