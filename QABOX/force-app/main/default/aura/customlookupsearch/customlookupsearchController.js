({
    handleKeyUp: function (component, event,helper) {
        var searchText = component.find('enter-search').get('v.value');        
        var lookupEvent =  component.getEvent('LookupEvent');
        lookupEvent.setParams({'searchText':searchText});        
        lookupEvent.fire();
    }
})