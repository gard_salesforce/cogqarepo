({
    fetchAccountDetails : function(component, event, helper) {
        helper.fetchAccountDetailsHelper(component, event, helper);
    },
    cancelAction : function(component, event, helper){
        helper.cancelActionHelper(component, event, helper);
    },
    handleOptionChange : function(component, event, helper) {
        var reason = component.find("reasonField").get("v.value");
        console.log('--reason--'+reason);
        if(reason)
            component.set("v.isDisabled",false);
        else
            component.set("v.isDisabled",true);
    },
    handleSubmit: function(component, event, helper) {
        console.log('--HandleSubmit fired-1--');
        event.preventDefault();       // stop the form from submitting
        
        var fields = event.getParam('fields');
        fields.ParentId = null;
        fields.Company_Status__c = 'Inactive';
        fields.Company_Role__c = '';
        fields.Sub_Roles__c = '';
        fields.Deleted__c = true;
        component.find('recordEditForm').submit(fields);
        console.log('--HandleSubmit fired-2--');
    },
    handleSuccess: function(component, event, helper) {
        component.set("v.reloadForm", false);
        $A.get('e.force:refreshView').fire();
        console.log('--Handle Success fired--'+component.get("v.reloadForm"));
    }
})