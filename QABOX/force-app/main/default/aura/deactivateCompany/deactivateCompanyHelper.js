({
	fetchAccountDetailsHelper : function(component, event, helper) {
		var action = component.get("c.setRequestFlagFromLightning");
        var recordId = component.get("v.recordId");
        action.setParams({
			"accId": recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            let errors = response.getError();
            if (state === "SUCCESS") {
                component.set("v.controllerObj", JSON.parse(response.getReturnValue()));
                console.log("--Success--"+component.get("v.controllerObj")['successFlag']);
                console.log("--onRisk--"+component.get("v.controllerObj")['onRisk']);
                console.log("--isESPFlag--"+component.get("v.controllerObj")['isESPFlag']);
                console.log("--reasonBlankFlag--"+component.get("v.controllerObj")['reasonBlankFlag']);
                console.log("--deletedFlagOriginalVal--"+component.get("v.controllerObj")['deletedFlagOriginalVal']);
                if(component.get("v.controllerObj")['isESPFlag'] && component.get("v.controllerObj")['reasonBlankFlag'] && !(component.get("v.controllerObj")['myGardenabled'] || component.get("v.controllerObj")['onRisk'] || component.get("v.controllerObj")['deletedFlagOriginalVal'] || component.get("v.controllerObj")['exceptionOccured'])){
                	component.set("v.showReason",true);
                	//component.set("v.notShowReason",false);
                }else{
                    component.set("v.showReason",false);
                    //component.set("v.notShowReason",true);
                }
                console.log('--show reason--'+component.get("v.showReason"));
                console.log('--not show reason--'+component.get("v.notShowReason"));
                /*
                if(!(component.get("v.controllerObj")['successFlag']))
                    component.set('v.openPopup',true);
                else{
                    console.log('--Deactivated-1--');
                    helper.showToast(component, event, helper, "Success", "Company has been successfully deactivated!", "");
                    //helper.refreshFocusedTabHelper(component, event, helper);
                    //$A.get('e.force:refreshView').fire();
                    //helper.cancelActionHelper(component, event, helper);
                    console.log('--Deactivated-2--');
                }*/
                console.log("--Deleted--"+component.get("v.controllerObj")['deletedFlagOriginalVal']);
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
	},
    showToast : function(component, event, helper, type, title, message){
        console.log('--Show toast--');
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "title": title,
            "message": message
        });	
        toastEvent.fire();
    },
    cancelActionHelper : function(component, event, helper){
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    
    refreshFocusedTabHelper : function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {

            workspaceAPI.refreshTab({
                      tabId: focusedTabId,
                      includeAllSubtabs: true
             });
        })
        .catch(function(error) {
            console.log(error);
        });
    }
})