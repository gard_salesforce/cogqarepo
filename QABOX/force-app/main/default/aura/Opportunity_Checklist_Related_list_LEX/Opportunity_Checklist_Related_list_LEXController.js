({
    doInit : function(component, event, helper) {
        helper.compDestroy(component, event, helper);
        helper.getOppChecklists(component, event, helper);
        helper.getLoggedInUser(component, event, helper);
    },
    viewAll : function(component, event, helper) {
        helper.viewAllOppChecklists(component, event, helper);
    },
    /*callBtn : function(component, event, helper) {
		helper.executeOppChecklistBtn(component, event, helper);
	},*/
    gotoOppChecklistDetailRecord : function(component, event, helper) {
        helper.navigateToOppChecklist(component, event, helper);
    },
    //Btn handlers-starts
    createOppList : function(component, event, helper) {
        helper.createOppCheckListHelper(component, event, helper);
        helper.rectypeCall(component, event, helper);   
    },
    closeRecordTypeModal : function(component, event, helper){
        helper.closeModel(component, event, helper);
    } ,  
    onRadiohandler : function(component, event, helper){        
        helper.onRadio(component, event, helper);
    },
    createRec_PnI : function(component, event, helper){                      
        helper.createOppCheckList_PnI_Helper(component, event, helper);
    },
    //Btn handlers-ends
    handleSelect : function(component, event, helper){ 
        var menuValue = event.detail.menuItem.get("v.accesskey");
        //console.log('menuValue ====>> '+menuValue);
        switch(menuValue) {
            case "1":  helper.editOppChklist(component, event, helper); break;
            case "2":  helper.deleteOppChklist(component, event, helper); break;
        }
    },
    refreshComp : function(component, event, helper){
        //helper.isRefreshed(component, event, helper);
        helper.getOppChecklists(component, event, helper); 
    }
    
    
})