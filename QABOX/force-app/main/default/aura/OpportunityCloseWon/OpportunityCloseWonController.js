({
    doInit : function(component, event, helper) {
        helper.fetchRecordType(component);
        var recordId=component.get("v.recordId");
        console.log('recordId'+recordId);
        var oppRecord = {};
        var action = component.get("c.getOpportunityRecord");
        action.setParams({
            "recordId": recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS") {
                oppRecord = JSON.parse(response.getReturnValue());                
                console.log('oppRecord id>>'+oppRecord.Opp.Id);
                console.log('test values--'+oppRecord.Opp.RecordTypeName__c+'--'+oppRecord.Opp.Checklist_Count__c+'--'+oppRecord.Opp.Type);
                if(oppRecord.Opp.RecordTypeName__c === 'P_I' && oppRecord.Opp.Checklist_Count__c === 0 && oppRecord.Opp.Type === 'Renewal'){
                    component.set('v.isPnIOpp',true);
                    component.set('v.isOpen',true);
                }
                else if(oppRecord.Opp.RecordTypeName__c === 'Marine' && oppRecord.Opp.Checklist_Count__c === 0){
                    component.set('v.isPnIOpp',false);
                    var createAction = component.get('c.createOppChecklist');
                    $A.enqueueAction(createAction);
                }
                    else{
                        helper.updateOpportunity(component, event, helper);
                    }
            }
            else{
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    selectRecordTypeOp:function(component, event, helper){
        //component.set('v.isDisabled',false);
        component.set("v.selectedRecordType",event.getParam("value"));
        console.log('--selected record type--'+component.get("v.selectedRecordType"));
        console.log('--available record types--'+component.get("v.options"));
        component.set("v.selectedRecord",component.find('selectedRecId').get('v.value'));
    },
    createOppChecklist:function(component, event, helper){
        var recordId=component.get("v.recordId");
        console.log('createOppChecklist got clicked--'+recordId);
        var oppRecord = {};
        var action = component.get("c.getOpportunityRecord");
        var createOppCheckList = $A.get("e.force:createRecord");
        action.setParams({
            "recordId": recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS") {
                {
                    oppRecord = JSON.parse(response.getReturnValue());
                    if(oppRecord.Opp.RecordTypeName__c ==="P_I"){
                        console.log('Opp.RecordTypeName__c--59--'+oppRecord.Opp.RecordTypeName__c);
                        createOppCheckList.setParams({
                            "entityApiName": "Opportunity_Checklist__c",
                            "defaultFieldValues": {
                                'Opportunity__c' : recordId,
                                'Name':'Checklist - '+oppRecord.Opp.Name.substr(0,68)
                            },
                            'recordTypeId': component.get('v.selectedRecordType')
                        });
                        console.log('component.get(v.selectedRecordType)--68--'+component.get('v.selectedRecordType'));
                        createOppCheckList.fire();
                    }
                    else if(oppRecord.Opp.RecordTypeName__c ==="Marine" && oppRecord.Opp.Checklist_Count__c===0){
                        //if(oppRecord.Opp.Type ==='Renewal'){
                        createOppCheckList.setParams({
                            "entityApiName": "Opportunity_Checklist__c",
                            "defaultFieldValues": {
                                'Opportunity__c' : recordId,
                                'Name':'Checklist - '+oppRecord.Opp.Name.substr(0,68)
                            },
                            'recordTypeId':'012D0000000V8dVIAS'	
                        });
                        createOppCheckList.fire();
                    }
                }
                 var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
            }
            else{
                alert('Error occured. Please reload the page and try again!');
            }
        });
        $A.enqueueAction(action);
    },
    cancelAction:function(component, event, helper){
        helper.cancelAction(component, event, helper);
    }
})