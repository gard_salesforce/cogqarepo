({
    /*
    fetchRecordType: function(component){
        var recordtypelist = [];
            recordtypelist.push(
            {
            'key':'Owners checklist',
            'value':'012D0000000BfGMIA0'
            },
             {
                 'key':'MOU checklist',
                 'value':'012D0000000BfGLIA0'
             },
             
             {
                 'key':'Charterers checklist',
                 'value':'012D0000000BfGKIA0'
             } 
            );
        component.set('v.recordtypelist',recordtypelist);
    } commented for testing
    */
    fetchRecordType: function(component){
        var recordtypelist = [];
            recordtypelist.push(
            {
            'label':'Owners checklist',
            'value':'012D0000000BfGMIA0'
            },
             {
                 'label':'MOU checklist',
                 'value':'012D0000000BfGLIA0'
             },
             
             {
                 'label':'Charterers checklist',
                 'value':'012D0000000BfGKIA0'
             } 
            );
        component.set('v.options',recordtypelist);
    }
    
    ,
    updateOpportunity : function(component, event, helper){
    	var recordId = component.get("v.recordId");
        var oppRecord = {};
        var action = component.get("c.updateOpportunities");
        action.setParams({
            "recordId" : recordId
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var res = response.getReturnValue();
                if(res.isSuccess){
                	console.log('--Opportunity closed--');
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                }
                else{
                    //alert(res.errorMsg);
                    component.set('v.showError',true);
                    component.set('v.errorMessage',res.errorMsg);
                }
            }
            else{
                alert("Error occured!");
            }
        });
        //var dismissActionPanel = $A.get("e.force:closeQuickAction");
        //dismissActionPanel.fire();
        $A.enqueueAction(action);
	},
    cancelAction : function(component, event, helper){
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    }
})