({
    doInit : function(component, event, helper){
        var id = component.get('v.recordId');
        console.log('new code 8');
        
        //downloads the Gard Contact list
        var strForceDownloadURL = "/apex/FetchGardContactsInCRM?id="+id;
        var forceDownloadEvent = $A.get("e.force:navigateToURL");
        forceDownloadEvent.setParams({
            "url": strForceDownloadURL
        });
        
        forceDownloadEvent.fire();
        
        setTimeout(function(){
            $A.get("e.force:closeQuickAction").fire();
            
            //Redirects back to the original page
            var holdingBackEvent = $A.get("e.force:navigateToSObject");
            holdingBackEvent.setParams({
                "recordId" : id,
                "slideDevName" : "detail"
            });
            holdingBackEvent.fire();
        }, 8000);
        
    }
})