({
    closeModal : function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
     },
    openPopUp : function (component, event, helper) {
        component.set("v.isOpen", true);
    },
	convertEmails : function(component, event, helper) {
        helper.convertEmailsHelper(component, event, helper);
	}
})