({
    convertEmailsHelper : function(component, event, helper){
        component.set("v.isOpen", false);
        component.set("v.isDisabled",true);
        var recordId = component.get("v.recordId");
		var selectedEmails = component.get("v.cmpSelectedRows");
        var emailMsgs = [];
        
        for(var i = 0;i < selectedEmails.length;i++){
			emailMsgs.push(selectedEmails[i]['recordId']);
        }
        console.log('--email messages id in loop--'+emailMsgs);
        var action = component.get("c.convertEmailToFile");
        action.setParams({
            "caseId": recordId,
            "emailIdListToBeConverted":emailMsgs
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var title,message;
            if (state === "SUCCESS") {
                console.log('--processing--');
                helper.showToast(component, event, helper, "Success", "Your request is processing!", "Please wait for sometime and refresh the page to access the converted email Messages");
            }
            else if (state === "ERROR") {
                helper.showToast(component, event, helper, "Error", "Sorry!", "Unable process your request. Please contact your admin.");
            }
            
        })
        $A.enqueueAction(action);
	},
	showToast : function(component, event, helper, type, title, message){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "title": title,
            "message": message
        });	
        toastEvent.fire();
    }
})