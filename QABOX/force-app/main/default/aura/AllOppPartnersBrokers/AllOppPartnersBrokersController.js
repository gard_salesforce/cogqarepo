({	
    isRefreshed : function(component, event, helper) {        
        helper.getPartnersOrBrokers(component, event, helper);
        
        if(component.get('v.modalPromise')){
             component.get('v.modalPromise').then(
                function (modal) {                    
                    modal.close();
                }
         ); 
        }
 
    },
	viewAllRecords: function(component, event, helper) { 
        /* var workspaceAPI = component.find("workspace");
    	 workspaceAPI.getFocusedTabInfo().then(function(response) {
        	var focusedTabId = response.tabId;
            workspaceAPI.setTabLabel({
                tabId: focusedTabId,
                label: "Partners or Brokers" 
            });
            workspaceAPI.setTabIcon({
                tabId: focusedTabId,
                icon: "utility:user_role", 
                iconAlt: "Partners or Brokers" 
            });
    	}) */
       var workspaceAPI = component.find("workspace");
    workspaceAPI.getFocusedTabInfo().then(function(response) {
       console.log('response: ' + JSON.stringify(response));
        //var focusedTabId = response.subtabs[0].tabId;
        if(typeof response != typeof  undefined && response != null && response.isSubtab){
            var focusedTabId = response.tabId;
        }
        console.log('focusTabId '+focusedTabId);
        workspaceAPI.setTabLabel({
            tabId: focusedTabId,
            label: "Partners or Brokers",
            title: "Partners or Brokers"
        });
        workspaceAPI.setTabIcon({
            tabId: focusedTabId,
            icon: "utility:user_role",
            iconAlt: "Partners or Brokers"
        });
    })
    	helper.getPartnersOrBrokers(component, event, helper);
        helper.getOppName(component, event, helper);
	},
    deletePartner:function(component, event, helper){
	var remaningPartnersOrBrokers = [];
	remaningPartnersOrBrokers = component.get('v.partnersOrBrokers');
	var action = component.get('c.deletePartnersOrBrokers');
	action.setParams({
		"partnerOrBrokerId":remaningPartnersOrBrokers[event.target.id].Id
	});
	action.setCallback(this,function(res){
		if(res.getState()==='SUCCESS' ||res.getState()==='DRAFT'){
			remaningPartnersOrBrokers.splice(event.target.id,1);        
			component.set('v.partnersOrBrokers',remaningPartnersOrBrokers); 
			var dataMap = res.getReturnValue();
            console.log('dataMap status:::'+dataMap.status);
            if(dataMap.status=='success'){
                 console.log('Deleted');
                 $A.get('e.force:refreshView').fire();
			//window.location.reload();
            }
		}
            else{
                console.log('Error');
            }
	});
	$A.enqueueAction(action); 
},
    showOppName:function(component, event, helper){
        var oppId =  component.get('v.recordId'); 
        /* component.find("navigation").navigate({
        "type" : "standard__recordPage",
        "attributes": {
            "recordId"      : oppId,
            "actionName"    : actionName ? actionName : "view"   //clone, edit, view
        }
    }, true); */
        var navEvt = $A.get("e.force:navigateToSObject");
    	navEvt.setParams({
        	"recordId": oppId
    	});
    	navEvt.fire();
    },
    goToRecentlyViewedOPP:function(component, event, helper){
        var hostname = window.location.hostname;
		//var arr = hostname.split(".");
		//var instance = arr[0];
		//console.log('instance:::::::'+instance);
		console.log('hostname:::::::'+hostname);
        var oppRecentlyViewedUrl = 'https://'+hostname+'/lightning/o/Opportunity/list?filterName=Recent';
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": oppRecentlyViewedUrl
        });
        urlEvent.fire();
    },
    createPartnerOrBroker :function(component, event, helper){
      	var oppId =  component.get('v.recordId');
        var modalBody;        
        var modalPromise = $A.createComponent('c:newPartnerOrBroker', {"partnerOrBroker":oppId}, function(content, status){
            modalBody =  content;
            var modalPromise  = component.find('overlayLib').showCustomModal({
                       header: "Partners or brokers",
                       body: modalBody, 
                       showCloseButton: true,
                       cssClass: "mymodal",
                       closeCallback: function() {
                           //alert('You closed the alert!');                           
                       }
                   });
            component.set("v.modalPromise", modalPromise);
        });
    }
})