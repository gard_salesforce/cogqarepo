({
	selectedRecordReg : function(component, event, helper) {
        var selectedIndex = event.currentTarget.id;        
        var selectedRecord = component.get('v.recordlist')[selectedIndex];        
        var selectedEvent = component.getEvent('selectedEvent');
        selectedEvent.setParams({
            selectedRecord:selectedRecord
        });
        console.log('@@selectedEvent',selectedEvent);
        selectedEvent.fire();
        component.set('v.recordlist',null);
	},
    handleRemove : function(component, event, helper) {
    }    
})