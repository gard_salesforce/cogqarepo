({
	doInit : function(component, event, helper) {
        var isReady = false;
        var recordId = component.get("v.recordId");
		var action = component.get("c.checkMadatoryFields");
        action.setParams({
            "caseId":recordId
		});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                isReady = response.getReturnValue();
                console.log('--isReady-returned-'+response.getReturnValue());
                component.set("v.isReady", response.getReturnValue());
                if(isReady === "true")
                    component.set("v.isReady",true);
                else
                    component.set("v.isReady",false);
                console.log('--Is ready?'+component.get("v.isReady"));
            }
        });
        $A.enqueueAction(action);
	},
    doSubmit : function(component, event, helper) {
		helper.sendSelectedFiles(component, event, helper);
	}
})