({
	sendSelectedFiles : function(component, event, helper) {
		var selectedFiles = component.get("v.cmpSelectedRows");
        console.log('selected files -- '+selectedFiles);
        var fileIds = [];
        var recordId = component.get("v.recordId");
        for(var i = 0;i < selectedFiles.length;i++){
            fileIds.push(selectedFiles[i]['recordId']);
            console.log('--file IDs in loop--'+fileIds);
        }
        console.log('--file IDs--'+fileIds);
        var action = component.get("c.sendCaseFiles");
        action.setParams({
            "caseId":recordId,
			"caseFileIds": fileIds
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('--successfully sent--');
                helper.showtoast(component, event, helper, "Success", "Your request is processing!", "Wait for sometime to check the document id.");
            }
        });
        $A.enqueueAction(action);
        component.set("v.isOpen", false);
    },
    showtoast : function(component, event, helper, type, title, message) {
    	var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "title": title, //"Cloning successfull!",
            "message": message
        });
       toastEvent.fire();
    }
})