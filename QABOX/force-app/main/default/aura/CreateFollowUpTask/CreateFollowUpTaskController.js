({
    doInit : function(component, event, helper) {
        var recordId=component.get("v.recordId");
        console.log('recordId'+recordId);
        var entobject={};
        var action =component.get("c.getEventRecord");
        action.setParams({
            "recordId": recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                entobject= response.getReturnValue();
              helper.createFollowUpTaskHelper(component,entobject);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    }
})