({
    createFollowUpTaskHelper : function(cmp, entObject) {
        var createTask = $A.get("e.force:createRecord");
        console.log('helper'+entObject);
        createTask.setParams({
            "entityApiName": "Task",
            "defaultFieldValues": {
                'WhatId':entObject.WhatId,
                'Type': 'Other',
                'Status':'Not Started',
                'Subject':entObject.Subject,
                'Priority':'Normal'
            }
        });
        createTask.fire(); 
        $A.get("e.force:closeQuickAction").fire();
    }
})