({
	openPopUp : function (component, event, helper) {
        component.set("v.isOpen", true);
    },
    handleClose : function (component, event, helper) {
        component.set("v.isOpen", false);
    }
})