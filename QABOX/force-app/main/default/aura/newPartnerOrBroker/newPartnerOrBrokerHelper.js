({
    helperdoInit : function(component, event, helper) {
        var frmElement = component.get('c.getPrePopulatedForm');
        frmElement.setCallback(this,function(res){
            var state =  res.getState();
            if(state === 'DRAFT' || state === 'SUCCESS'){                
                component.set('v.wrapperPartnerListRet',JSON.parse(res.getReturnValue()));
                var defaultAccount = [];
                component.get('v.wrapperPartnerListRet').forEach(function(item,index){
                    item.acct=null;
                    defaultAccount.push(item);
                });
                component.set('v.wrapperPartnerListRet',defaultAccount);
            }
            else{
                console.log('Error');                           
            }
        });
        $A.enqueueAction(frmElement);
        var frmRole = component.get('c.getRoles');
        frmRole.setCallback(this,function(res){
            var state =  res.getState();
            if(state === 'DRAFT' || state === 'SUCCESS'){                
                var defaultRoles = [];
                var results = res.getReturnValue();
                console.log('getReturnValue>>>'+results.length);                
                for(var index=0;index<=results.length-1;index++){                    
                    defaultRoles.push({'value':results[index],
                                       'label':results[index]
                                      });                        
                }
                console.log('defaultRoles>>>'+defaultRoles.length);  
                component.set('v.roles',defaultRoles);
                console.log('roles>>>>>>>>>>>>>>'+component.get('v.roles').length);
            }
            else{
                console.log('Error');                           
            }                            	
            
        });
        $A.enqueueAction(frmRole);
    },
    fetchofCustomerDetails: function(component, event, helper){
         var customerDetails = component.get('c.getCustomerDetails');
        customerDetails.setParams({
            'opportunityId':component.get('v.partnerOrBroker')
        });
        customerDetails.setCallback(this,function(res){
            component.set('v.customerDetails',res.getReturnValue());
        });
        $A.enqueueAction(customerDetails);
    },    
    createNewPartner : function(component, event, helper) {       		
        var partnerListaftersave = component.get('v.wrapperPartnerListRet');
        var partnerListtobecreated = [];
        var isaction=true;
        partnerListaftersave.forEach(function(data,index){            
            if(data.acct != null && data.acct.Id == component.get('v.customerDetails').AccountId){                
                component.set('v.showError',true);
                component.set('v.errorMessage','Self Account ('+component.get('v.customerDetails').Account.Name+') is not allowed');
                isaction = false;
            }else if(data.acct != null){
                partnerListtobecreated.push(data);    
            }
        });        
        console.log('partnerListaftersave'+JSON.stringify(partnerListaftersave));       
        if(partnerListtobecreated.length > 0 && isaction){
            var controleNewPartnerAction = component.get('c.createNewPartnersOrBrokers');             
            controleNewPartnerAction.setParams({
                "wrapPartnerjson": JSON.stringify(partnerListtobecreated),
                "opportunityID"  : component.get('v.partnerOrBroker')
            });
            controleNewPartnerAction.setCallback(this,function(res){
                var state =  res.getState();
                if(state === 'DRAFT' || state === 'SUCCESS'){
                    console.log('SUCCESS');
                    $A.get('e.force:refreshView').fire();
                }
                else{
                    console.log('Error');
                    component.set('v.showError',true);
                    component.set('v.errorMessage','Error');
                }                            	
            });
            $A.enqueueAction(controleNewPartnerAction);
        }            
        
    }
})