({
    fetchEmailsHelper : function(component, event, helper) {
        console.log("--showEmailsToBeConvertedHelper called--");
        component.set('v.mycolumns', [
            {label: 'Subject', fieldName: 'Subject', type: 'text', sortable:true},
            {label: 'From address', fieldName: 'fromAdd', type: 'text', sortable:true},
            {label: 'To address', fieldName: 'toAdd', type: 'text', sortable:true},
            {label: 'Message date', fieldName: 'msgDate', type: 'text', sortable:true}
        ]);
            var recordId = component.get("v.recordId");
            var action = component.get("c.FetchEmailsFromCase");
            action.setParams({
                "caseId": recordId
            });
            console.log("--Before callback--"+recordId);
            action.setCallback(this, function(response){
            	console.log("--setCallback called--");
                var state = response.getState();
                if (state === "SUCCESS") {
            		component.set("v.emailsList", JSON.parse(response.getReturnValue()));
            		console.log("Emails : "+component.get("v.emailsList"));
                	helper.sortOnLoad(component, event, helper);
                	var emailsLst = []; 
                	emailsLst = component.get("v.emailsList");
                	if(emailsLst.length > 0)
                    	component.set("v.emailsAvailable",true);
                    else
                        component.set("v.emailsNotAvailable",true);
                    component.set("v.isOpen", false);
                }else{
                	console.log("--Sorry for the error--"+state+"-message-"+errors);
            	}
            });   
		$A.enqueueAction(action);
	},
	
    getSelectedRows : function(component, event, helper){
        var selectedRows = event.getParam('selectedRows');
        if(selectedRows.length > 0)
            component.set("v.isDisabled",false);
        else
            component.set("v.isDisabled",true);
        component.set("v.cmpSelectedRows",selectedRows);
        console.log('-selected rows in child-'+component.get("v.cmpSelectedRows"));
    },
    sortOnLoad: function(cmp, event, helper){
        cmp.set("v.sortedBy", 'msgDate');
        cmp.set("v.sortedDirection", 'desc');
        //console.log('field name :'+v.sortedDirection+'-Direction-'+v.sortedBy);
        helper.sortData(cmp, 'msgDate', 'desc');
    },

    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.emailsList");
        var reverse = sortDirection !== 'asc';
        console.log('Reverse : '+reverse);
        //sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set("v.emailsList", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
        function(x) {return x[field]};
        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    }
})