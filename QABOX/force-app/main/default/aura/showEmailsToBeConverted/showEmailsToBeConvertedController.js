({
    fetchEmails : function(component, event, helper) {
        var recordId = component.get("v.recordId");
       //component.set("v.reportURL","https://gardas--qabox.lightning.force.com/lightning/r/Report/00O3z0000094247EAA/view?fv2="+recordId);
        var baseLink = $A.get("$Label.c.showEmailsBaseLink");
        component.set("v.reportURL",baseLink+recordId);
        helper.fetchEmailsHelper(component, event, helper);
    },
    updateColumnSorting: function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        // assign the latest attribute with the sorted column fieldName and sorted direction
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        console.log('field name :'+fieldName+'-Direction-'+sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);
    },
    getSelectedRows : function(component, event, helper){
        helper.getSelectedRows(component, event);
    }
})