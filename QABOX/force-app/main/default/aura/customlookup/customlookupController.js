({
	doHandlerSearchEvent : function(component, event, helper) {
        var searchParam = event.getParam('searchText');        
        var action = component.get('c.getRecordList');
        action.setParams({
            ObjectName:component.get('v.objName'),
            searchText:searchParam,
            fieldInsearch:component.get('v.fieldInsearch')
        });
        action.setCallback(this,function(res){
            var status = res.getState();
            if(status == 'SUCCESS' || 'DRAFT'){
             	var responseValue = res.getReturnValue();                
                component.set('v.recordlist',responseValue);
            }else{
                conosle.log('error'+error);
            }    
        });
        $A.enqueueAction(action);		
	},
    doHandlerSelected : function(component, event, helper) {        
        var selectedRecord = event.getParam('selectedRecord');
        component.set('v.selectedRecord',selectedRecord);        
    },
	handleRemove : function(component, event, helper) {
        component.set('v.selectedRecord',null);  
    }    
})