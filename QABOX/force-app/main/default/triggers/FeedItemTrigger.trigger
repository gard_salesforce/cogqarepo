trigger FeedItemTrigger on FeedItem (after insert){
    
    List<FeedItem> caseFeedItems = new List<FeedItem>();
    for(FeedItem fi : trigger.new){
        if(fi.parentId.getSObjectType() == Schema.Case.SObjectType){
            caseFeedItems.add(fi);
        }
    }
    if(!caseFeedItems.isEmpty()) FeedItemTriggerHelper.processCaseFeedItems(caseFeedItems);//SF-5466
}