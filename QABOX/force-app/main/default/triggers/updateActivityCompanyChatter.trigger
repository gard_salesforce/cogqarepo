trigger updateActivityCompanyChatter on Event (after insert) {
    
    List<FeedItem> feedItems = new List<FeedItem>();
    /*
    //We want to show the User name as assignedTo. The only way to get to that is by querying the user table.
    Set<ID> ownerIds = new Set<ID>();
    for (Event e : Trigger.new) {
        ownerIds.add(e.ownerId);
    }
    Map<ID,User> userMap = new Map<ID,User>([SELECT ID, Name FROM User WHERE ID IN :ownerIds]); //This is our user map

    //Now loop though the new/updated tasks and create the feed posts
    for (Event e : Trigger.new) {
        if (e.WhatId != null) {
            if (e.Type == 'Meeting') {
                FeedItem fitem = new FeedItem();
                fitem.type = 'LinkPost';
                fitem.ParentId = e.WhatId;
                fitem.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + e.id;
                fitem.Title = 'View Meeting';
    
                //Get the user by checking the userMap we created earlier
                User assignedTo = userMap.get(e.ownerId);
    
                fitem.Body = 'New Meeting' + ((e.Location__c) != null ? ', ' + e.Location__c : '') 
                            + '\nStart:' + e.StartDateTime.format() + ',  End:' + e.EndDateTime.format()
                            + '\nSubject: ' + e.Subject;
                            
                feedItems.add(fitem);
            }
        }
    }

    //Save the FeedItems all at once.
    if (feedItems.size() > 0) {
        Database.insert(feedItems,false); //notice the false value. This will allow some to fail if Chatter isn't available on that object
    }
    */
}