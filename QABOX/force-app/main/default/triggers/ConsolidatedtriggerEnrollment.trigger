trigger ConsolidatedtriggerEnrollment on PEME_Enrollment_Form__c (before insert,before update,after insert,after update,before delete,after delete) {

    Recent_Updates__c ru =new Recent_Updates__c();  
    Account acc = new account();
    List<Account> tcl=[select name,company_id__c from Account where company_id__c='22384'];        //SF-4176
    Boolean testCompanyFoundFlag=false;
    String tcid='';
    if(tcl!=null&&tcl.size()==1){
            tcid=tcl[0].company_id__c;
            testCompanyFoundFlag=true;
    }

    if(Trigger.isInsert && Trigger.isBefore){
        
        for(PEME_Enrollment_Form__c obj:Trigger.new){
            if(obj.Enrollment_Status__c == null) obj.Enrollment_Status__c = 'Draft'; //Added for SF-4871
            obj.Last_Status_Change_Date__c = Datetime.NOW();
        }
        PEMEEnrollmentHelper.PEMEEnrollmentBeforeInsert(trigger.new); //SF-4232 
        PEMEEnrollmentHelper.underApprovalStatusEligibiltyCheck(Trigger.oldMap,Trigger.newMap);//added for SF-4871
    }
    
    if(Trigger.isUpdate && Trigger.isBefore){
        PEMEEnrollmentHelper.underApprovalStatusEligibiltyCheck(Trigger.oldMap,Trigger.newMap);//added for SF-4871
        PEMEEnrollmentHelper.approvedStatusEligibilityCheck(Trigger.oldMap,Trigger.newMap);//added for SF-4871
        for(PEME_Enrollment_Form__c obj:Trigger.new){
            if(obj.Enrollment_Status__c != Trigger.oldMap.get(obj.Id).Enrollment_Status__c){
                obj.Last_Status_Change_Date__c = Datetime.NOW();
            }
        }
    }
    
    if((Trigger.isUpdate||Trigger.isInsert) && Trigger.isAfter){
           List<PEME_Enrollment_Form__c> enrollmentList=new List<PEME_Enrollment_Form__c>();
         for(PEME_Enrollment_Form__c enrollment:trigger.new){
            if(!enrollment.Enrollment_Status__c.equalsIgnoreCase('Draft')){
              enrollmentList.add(enrollment);
            }
         }
         if(!enrollmentList.isEmpty()){
             //PEMEEnrollmentHelper.PEMEEnrollmentAfterInsert(enrollmentList);commented for SF-4871
         }
            if(Trigger.isUpdate && Trigger.new[0].Enrollment_Status__c=='Enrolled' && Trigger.Old[0].Enrollment_Status__c=='Under Approval'){
                PEMELandingPageCtrl.sendEnrollmentApprovalEmails(Trigger.new[0]);
            }
            if((Trigger.isInsert && Trigger.new[0].Enrollment_Status__c=='Under Approval') || (Trigger.isUpdate && Trigger.old[0].Enrollment_Status__c!='Under Approval' && Trigger.new[0].Enrollment_Status__c=='Under Approval')){ //&& (objProf.Id==UserInfo.getProfileId() || objProf1.Id==UserInfo.getProfileId())){
                GardUtils.sendEnrollmentSubmissionEmails(Trigger.new[0].id);
            }
            if(Trigger.isUpdate)
            {
            if(Trigger.new[0].OwnerId !=Trigger.oldMap.get(Trigger.new[0].Id).OwnerId) // for owner change
            {              
             for(ProcessInstanceWorkitem piw : [SELECT Id,ActorId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =: Trigger.new[0].Id AND ProcessInstance.Status = 'Pending' LIMIT 1])
             {
                  if(piw != null)
                  {
                      piw.ActorId = Trigger.new[0].OwnerId;
                      
                      try {
                          update piw;
                      }
                      catch(DMLException ex){
                          system.debug('Exception found'+ex);
                      }
                  }
              }
              
            }
            }//end for owner
        }
       
    if((Trigger.isUpdate||Trigger.isInsert) && (Trigger.isAfter ||Trigger.isBefore)){
       set<String> setAccIds = new set<String>();
       String status;
        list<Account> lstAccsToUpdt = new list<Account>();
         for(PEME_Enrollment_Form__c obj:Trigger.new){
              setAccIds.add(obj.ClientName__c);   
              status=obj.Enrollment_Status__c;        
         }
      if(setAccIds.size()>0){
             for(Account acc:[Select Id,PEME_Enrollment_Status__c from Account where Id IN:setAccIds]){
                 acc.PEME_Enrollment_Status__c=status;
                 lstAccsToUpdt.add(acc);
             }
             if(lstAccsToUpdt.size()>0){
                 try{
                     update lstAccsToUpdt;
                 }catch(DMLException ex){
                 
                 }
             }
         }
     
    }
    
    if(Trigger.isUpdate && Trigger.isAfter){
        for(PEME_Enrollment_Form__c obj:Trigger.new){
            if(obj.Enrollment_Status__c=='Enrolled' && Trigger.oldMap.get(obj.Id).Enrollment_Status__c=='Under Approval'){
                acc = [Select name,company_id__c from account where id =:obj.ClientName__c];
                if((!testCompanyFoundFlag)||(acc.company_id__c!=tcid)){              //SF-4176
                ru.Update_Value__c = '<span class="text-bold">'+ acc.name + '</span> is enrolled in the PEME program effective since <span class="text-bold">' + obj.Last_Status_Change_Date__c.format('dd-MM-yyyy') + '</span>';
                insert ru;
                }
            }
        }
        for(PEME_Enrollment_Form__c obj:Trigger.new){
            if(obj.Enrollment_Status__c=='Disenrolled' && Trigger.oldMap.get(obj.Id).Enrollment_Status__c=='Enrolled'){
                acc = [Select name,company_id__c from account where id =:obj.ClientName__c];
                if((!testCompanyFoundFlag)||(acc.company_id__c!=tcid)){           //SF-4176
                ru.Update_Value__c = '<span class="text-bold">' + acc.name + '</span> has disenrolled from the PEME program effective since <span class="text-bold">' + obj.Last_Status_Change_Date__c.format('dd-MM-yyyy') + '</span>';
                insert ru;
                
                }
            }
        }
    }
    
    
    if(Trigger.isDelete&& Trigger.isBefore){
        set<String> setAccIds = new set<String>();
        list<Account> lstAccsToUpdt = new list<Account>();
         for(PEME_Enrollment_Form__c obj:Trigger.old){
              setAccIds.add(obj.ClientName__c);           
         }
         if(setAccIds.size()>0){
             for(Account acc:[Select Id,PEME_Enrollment_Status__c from Account where Id IN:setAccIds]){
                 acc.PEME_Enrollment_Status__c=null;
                 lstAccsToUpdt.add(acc);
             }
             if(lstAccsToUpdt.size()>0){
                 try{
                     update lstAccsToUpdt;
                 }catch(DMLException ex){
                 
                 }
             }
         }
    }
   
    if(trigger.isbefore && trigger.isInsert)
    {
        GardUtils.updateGUID(Trigger.new);        
    }
    if(Trigger.isDelete && Trigger.isAfter){//SF-1902
        PEMEEnrollmentHelper.afterDelete(Trigger.old);
    }
}