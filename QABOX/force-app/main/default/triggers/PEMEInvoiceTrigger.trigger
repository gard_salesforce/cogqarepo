trigger PEMEInvoiceTrigger on PEME_Invoice__c (before insert,after insert,after update,before update) {
    
    if(trigger.isbefore && trigger.isInsert)
    {
        GardUtils.updateGUID(Trigger.new);        
    }    
    if(trigger.isupdate && trigger.isAfter && trigger.new.size() == 1){
        //System.debug('peme Trigger ran');
        if(
            (
                trigger.new[0].Status__c.equalsIgnoreCase('Approved') && !trigger.old[0].Status__c.equalsIgnoreCase('Approved')
            )
            || (
                trigger.new[0].Status__c.equalsIgnoreCase('Partially Approved') && !trigger.old[0].Status__c.equalsIgnoreCase('Partially Approved')
            )
        ){
            System.enqueueJob(new PemeWsJobs(trigger.new[0].id));
        }else if(Test.isRunningTest()){
            System.enqueueJob(new PemeWsJobs(trigger.new[0].id));
        }
    }
}