trigger CompanyUnapprovedRolesUpdateTrigger on Account (before update) {
    if(trigger.isUpdate && trigger.isBefore)
    {   
        List<Account> acc = new List<Account>(); 
        for (Account anAccount : Trigger.new) {
        Account oldAccount = Trigger.oldMap.get(anAccount.Id);
           if(anAccount.IsRejected__c == true && oldAccount.IsRejected__c <> anAccount.IsRejected__c && anAccount.Unapproved_Company_Roles__c !=null){
               anAccount.Unapproved_Company_Roles__c = '';
               anAccount.Unapproved_Company_Roles_to_Add__c = '';
               anAccount.Unapproved_Company_Roles_to_Remove__c = '';
               anAccount.Sub_Roles_to_Remove__c = '';
               anAccount.Sub_Roles_Unapproved__c = '';
               anAccount.Sub_Roles_to_Add__c = '';
            } 
            
            acc.add(anAccount);
        }
    }   
}