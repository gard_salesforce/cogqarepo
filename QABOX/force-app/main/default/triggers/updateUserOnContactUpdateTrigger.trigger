trigger updateUserOnContactUpdateTrigger on Contact (after update) {
    Set<Id>  changedConId= new Set<Id>();
    List<User> userName=  new List<User>();
    
    for(Contact newCon: Trigger.new){
        if(newCon.FirstName!=Trigger.oldMap.get(newCon.Id).FirstName ||
           newCon.LastName !=Trigger.oldMap.get(newCon.Id).LastName ) {
               changedConId.add(newCon.Id);
           }
    }
    
    if(changedConId.size() > 0){
        userName= [Select Id,ContactId from user where ContactId in:changedConId];
        
        List<Contact> contactDetail = [Select FirstName, LastName, Phone, Fax, MobilePhone, OtherPhone, MailingStreet, 
                                       MailingCity, MailingState, MailingPostalCode,
                                       MailingCountry, MailingLatitude, MailingLongitude 
                                       from contact where id in: changedConId ];
        for(User updatedUser: userName ){
            for(Contact conList :contactDetail){
                if(updatedUser.ContactId==conList.id){
                    String nickname='';
                    if((conList.FirstName + conList.LastName).length()>39){
                        nickname = (conList.FirstName + conList.LastName).substring(0,39);
                    }else{
                        nickname=conList.FirstName + conList.LastName;}
                    updatedUser.FirstName = conList.FirstName;
                    updatedUser.LastName = conList.LastName;
                    updatedUser.CommunityNickname = nickname;
                    updatedUser.Phone =conList.OtherPhone;
                    updatedUser.Fax = conList.Fax;
                    updatedUser.MobilePhone = conList.MobilePhone;
                    updatedUser.Street = conList.MailingStreet;
                    updatedUser.City = conList.MailingCity;
                    updatedUser.State = conList.MailingState; 
                    updatedUser.PostalCode = conList.MailingPostalCode;
                    updatedUser.Country = conList.MailingCountry;
                    updatedUser.Latitude = conList.MailingLatitude; 
                    updatedUser.Longitude = conList.MailingLongitude;
                }
            }
        }   
    }
    
    if(userName.size() > 0){
        update userName ;
    }
}