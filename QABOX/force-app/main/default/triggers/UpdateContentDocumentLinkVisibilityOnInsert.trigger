trigger UpdateContentDocumentLinkVisibilityOnInsert on ContentDocumentLink (before insert,after insert) {

    if(Trigger.isInsert && Trigger.isBefore){
        for(ContentDocumentLink l:Trigger.new){
            l.Visibility='AllUsers'; 
            //l.ShareType='V';
        }
    }
    /*Commented for Arpan's code migration
    if(trigger.isInsert && trigger.isAfter){
        integer i=0;
         Map<Id,ContentDocumentLink> linkedEntityMap = new Map<Id,ContentDocumentLink>();
        Map<Id, List<ContentDocumentLink>> origContentDocumentLinksByorigEmailIds = new Map<Id, List<ContentDocumentLink>>();
         
        for(ContentDocumentLink cdl : trigger.new){
              if(string.valueof(cdl.LinkedEntityId).startsWith('02s')){
                 linkedEntityMap.put(cdl.LinkedEntityId,cdl);
              }
          }
         Map<Id, EmailMessage> emailMessagesByCaseIds = new Map<Id, EmailMessage>();
         Map<id,EmailMessage>ClonedEmailMap=new Map<Id, EmailMessage>();
         set<Id>ClonedCaseSet=new set<Id>();
         Map<Id, List<EmailMessage>> clonedEmailMessagesByorigEmailMessageIds = new Map<Id, List<EmailMessage>>();
         List<EmailMessage>emailMessageList=new List<EmailMessage>([Select id,clonedParent__c,ccaddress,ToAddress, subject, HtmlBody, TextBody,incoming,parentId,hasAttachment
                                                                                                           from EmailMessage WHERE
                                                                                                           id IN: linkedEntityMap.keySet()
                                                                                                           OR clonedParent__c IN: linkedEntityMap.keySet() ]);
        
           // for(emailMessage email:[SELECT id,parentid,clonedParent__c from EmailMessage where clonedParent__c IN :linkedEntityMap.keySet()]){
           for(emailMessage email:emailMessageList){
            	if(linkedEntityMap.containsKey(email.clonedParent__c) && email.ParentId.getSObjectType() == Case.sObjectType){
            	   ClonedEmailMap.put(email.ClonedParent__c,email);
            	   ClonedCaseSet.add(email.parentId);
            	}
            }
          //  for(EmailMessage email:[SELECT ccaddress,ToAddress, subject, HtmlBody, TextBody,incoming,parentId,hasAttachment FROM EmailMessage where id  IN : linkedEntityMap.keySet()]){
                 for(EmailMessage email:emailMessageList){
                 if( linkedEntityMap.containsKey(email.id) &&email.incoming && email.hasattachment && email.ParentId.getSObjectType() == Case.sObjectType && !ClonedEmailMap.containsKey(email.id)){
                      emailMessagesByCaseIds.put(email.ParentId, email);
            }
           else if(ClonedEmailMap.containsKey(email.id)){
           	     if(origContentDocumentLinksByorigEmailIds.containsKey(email.id)){
           	     	origContentDocumentLinksByorigEmailIds.get(email.id).add(linkedEntityMap.get(email.id));
           	     }
           	     else{
           	     	origContentDocumentLinksByorigEmailIds.put(email.id,new List<ContentDocumentLink>{linkedEntityMap.get(email.id)});
           	     }
            	 if(clonedEmailMessagesByorigEmailMessageIds.containsKey(email.id))
                  {
                    clonedEmailMessagesByorigEmailMessageIds.get(email.Id).add(ClonedEmailMap.get(email.id));
                  }
                  else
                  {
                     clonedEmailMessagesByorigEmailMessageIds.put(email.Id,new List<EmailMessage>{ ClonedEmailMap.get(email.id)});
                   }
            }
     }
     if(!emailMessagesByCaseIds.isEmpty()){ 
                //caseCloneController.cloneCaseforIncomingEmails(emailMessagesByCaseIds); //commented for Arpan's code migration
      }
        
    if(!clonedEmailMessagesByorigEmailMessageIds.isEmpty()){
      	 Map<id,List<Case>>clonedCaseMap=new Map<id,List<Case>>();
      	for(Case clonedCase:[Select id,parentId,CaseNumber from Case where Id IN:ClonedCaseSet]){
      		if(clonedCaseMap.containsKey(clonedCase.parentId))
                   {
                      clonedCaseMap.get(clonedCase.parentId).add(clonedCase);
                   }
             else
              {
                  clonedCaseMap.put(clonedCase.parentId,new List<case>{clonedCase});
              }                  
      	}
      	caseCloneController.cloneAndInsertAttachment(origContentDocumentLinksByorigEmailIds,clonedEmailMessagesByorigEmailMessageIds,clonedCaseMap);
      }
 //}
    }
	*/
}