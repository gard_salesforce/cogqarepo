trigger createContentDistributionAfterContentVersion on ContentVersion (after insert) {
    
        ContentDistribution cd;
        List<ContentDistribution> contentDistributionList = new List<ContentDistribution>();
        for(ContentVersion contentV : Trigger.new){
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.allowFieldTruncation = true;
            cd = new ContentDistribution ();
            system.debug('ContentVersion title:::::::::::::::'+contentV.Title);
            system.debug('ContentVersion id:::::::::::::::'+contentV.Id);
            cd.Name = contentV.Title;
            cd.ContentVersionId = contentV.Id;
            cd.PreferencesAllowViewInBrowser= true;
            cd.PreferencesLinkLatestVersion=true;
            cd.PreferencesNotifyOnVisit=false;
            cd.PreferencesPasswordRequired=false;
            cd.PreferencesAllowOriginalDownload= true;
            cd.setOptions(dmo);
            contentDistributionList.add(cd);
        }
        system.debug('ContentDistribution:::::::::::::::'+cd);
        if(contentDistributionList != null && contentDistributionList.size() > 0){
            try{
                insert contentDistributionList;
            }catch(DmlException e){system.debug('Exception caused by --'+e);}
        }
}