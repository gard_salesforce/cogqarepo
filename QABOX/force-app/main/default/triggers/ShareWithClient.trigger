/*
*This trigger works on 'after update' of contract record. 
*This deletes all the mycoverlist records related to the userid of the updated contract record and reclaculate the Asset records 
*for that user id and inserts the new set of asset records into Mycoverlist.
*
*/
trigger ShareWithClient on Contract (before insert,after update) {
    Set<Id> setAccId = new Set<Id>();
    
    List<Contact> lstContact= new List<Contact>();
    Set<Id> setConId = new Set<Id>();
    List<User> lstUser= new List<User>();
    Boolean blBrokerView;
    List<MyCoverList__c> lstMyCover = new List<MyCoverList__c>();
    List<MyCoverList__c> delLstMyCover;
    List<MyCoverList__c> delLstMyCoverAll = new List<MyCoverList__c>();
    
    
    set<Id> CompanyIds = new set<Id>();
    if(Trigger.isInsert  && Trigger.isBefore){
        for(Contract aContract : Trigger.new){
            System.debug('aContract Broker' + aContract.broker__c);
            System.debug('aContract Client ' + aContract.client__c);
            
            List<Contract> singleContracts = [SELECT ID, Shared_With_Client__c FROM CONTRACT WHERE client__c = :aContract.client__c and Broker__c = :aContract.broker__c LIMIT 1];
            if(singleContracts.size() > 0){
                System.Debug('**** Shared_With_Client__c: ' + singleContracts.get(0).Shared_With_Client__c);
                System.Debug('**** Agrrement id: ' + singleContracts.get(0).ID);
                aContract.Shared_With_Client__c = singleContracts.get(0).Shared_With_Client__c;
            }
        }
    }   
      
    for(Contract contr : Trigger.New){
        if(Trigger.oldMap != null && contr.Shared_With_Client__c!=Trigger.oldMap.get(contr.Id).Shared_With_Client__c && contr.Shared_With_Client__c == true){
            setAccId.add(contr.Client__c);
        }else if(Trigger.oldMap != null && contr.Shared_With_Client__c!=Trigger.oldMap.get(contr.Id).Shared_With_Client__c && contr.Shared_With_Client__c == false){
            setAccId.add(contr.Client__c);
        }
    }
    if(setAccId != null){
        lstContact = [Select Id From Contact Where AccountId IN : setAccId];
    }
    for(Contact con : lstContact){
        setConId.add(con.Id);
    }
    
    if(setConId  != null){
        lstUser = [Select  Id, contactID From User Where ContactId IN : setConId ];
    }
    
    for(User usr:lstUser){
                Contact myContact;
                    if(usr!= null && usr.contactID != null){
                        myContact = [Select AccountId from Contact Where id=: usr.contactID];
                    }
                    Account acc;
                    List<Object__c> lstObject = new List<Object__c>();
                    List<Contract> lstContract = new List<Contract>();
                    List<Contract> lstContractClient = new List<Contract>();        
                    
                    if(myContact != null && myContact.AccountId != null){
                        acc = [Select ID,Company_Role__c from Account Where id =: myContact.AccountId ];        
                        if(acc != null && acc.Company_Role__c != null &&  acc.Company_Role__c.contains('Broker')){
                            blBrokerView = true;
                           /* lstContract = [Select Id, Client__c from Contract Where Broker__c =: acc.id];
                            Set<ID> setClientID = new Set<ID>();
                            for(Contract contract: lstContract){
                                setClientID.add(contract.Client__c);
                            }*/
                          //  lstContractClient = [Select id from Contract Where Client__c IN : setClientID];
                           lstContractClient = [Select Id, Client__c from Contract Where Broker__c =: acc.id];
                        }else{
                            blBrokerView = false;          
                            lstContractClient = [Select id from Contract Where Client__c =: myContact.AccountId AND (broker__c=null or Shared_With_Client__c=true)];
                        }
                    }
                    
                    Set<ID> setCoverID = new Set<ID>();
                    for(Contract obj : lstContractClient){
                        setCoverID.add(obj.ID);
                    }
                    
                    //showMyCover =true;
                    string strforClient = '';
                    string strforClientGroupby = '';
                    
                    if(blBrokerView == true){
                        //strforClient = 'Object__r.Client__r.Name clientname,';
                        //strforClientGroupby = 'Object__r.Client__r.Name,';
                        strforClient = 'Agreement__r.Client__r.Name clientname, Agreement__r.Client__r.Id clientId,';
                        strforClientGroupby = 'Agreement__r.Client__r.Name,Agreement__r.Client__r.Id,';
                    }                    
                    
                    String sSoqlQuery = 'SELECT MAX(Expiration_Date__c) expdate, '+strforClient+' UnderwriterNameTemp__c underwriter, UnderwriterId__c underwriterId,Product_Name__c prod,Agreement__r.Business_Area__c busarea,'+ 
                                        'Agreement__r.Broker__r.Name AggName,Agreement__r.Broker__r.Id AggId, Agreement__r.Policy_Year__c plcyyr, '+
                                        'Claims_Lead__c claim, Gard_Share__c grdshr, On_risk_indicator__c rskind,'+
                                         'Count(id) FROM Asset Where Agreement__c IN : setCoverID '+
                                        'GROUP BY '+strforClientGroupby+' UnderwriterNameTemp__c, UnderwriterId__c, Product_Name__c, '+
                                                 'Agreement__r.Business_Area__c,'+
                                                 'Agreement__r.Broker__r.Name,Agreement__r.Broker__r.Id,'+
                                                 'Agreement__r.Policy_Year__c,'+
                                                 'Claims_Lead__c, Gard_Share__c, On_risk_indicator__c';
                                                 
                    AggregateResult[] groupedResults = database.query(sSoqlQuery);
                    
                   
                    delLstMyCover = [Select id from MyCoverList__c Where userId__c =: usr.Id];
                    for(MyCoverList__c mcl : delLstMyCover){
                        delLstMyCoverAll.add(mcl);
                    }
                    
                          
                    for (AggregateResult ar : groupedResults)  {
                        if(blBrokerView == false){
                            lstMyCover.add(new MyCoverList__c(
                                                 client__c='',
                                                 client_id__c='',
                                                 underwriter__c=string.valueof(ar.get('underwriter')),
                                                 UnderwriterId__c=string.valueof(ar.get('underwriterId')),
                                                 product__c=string.valueof(ar.get('prod')),
                                                 productarea__c=string.valueof(ar.get('busarea')),
                                                 broker__c=string.valueof(ar.get('AggName')),
                                                 broker_id__c=string.valueof(ar.get('AggId')), 
                                                 expirydate__c=string.valueof(ar.get('expdate')),
                                                 policyyear__c=Integer.valueof(ar.get('plcyyr')),                                           
                                                 claimslead__c=string.valueof(ar.get('claim')),
                                                 gardshare__c=Double.valueof(ar.get('grdshr')),
                                                 onrisk__c=Boolean.valueof(ar.get('rskind')),
                                                 viewobject__c=Integer.valueOf(ar.get('expr0')),
                                                 userId__c = usr.Id
                                             ));
                        }else{
                            lstMyCover.add(new MyCoverList__c(
                                                            client__c=string.valueof(ar.get('clientname')),
                                                            client_id__c=string.valueof(ar.get('clientId')),
                                                             underwriter__c=string.valueof(ar.get('underwriter')),
                                                             UnderwriterId__c=string.valueof(ar.get('underwriterId')),
                                                             product__c=string.valueof(ar.get('prod')),
                                                             productarea__c=string.valueof(ar.get('busarea')),
                                                             broker__c=string.valueof(ar.get('AggName')),
                                                             broker_id__c=string.valueof(ar.get('AggId')),
                                                             expirydate__c=string.valueof(ar.get('expdate')),
                                                             policyyear__c=Integer.valueof(ar.get('plcyyr')),                                           
                                                             claimslead__c=string.valueof(ar.get('claim')),
                                                             gardshare__c=Double.valueof(ar.get('grdshr')),
                                                             onrisk__c=Boolean.valueof(ar.get('rskind')),
                                                             viewobject__c=Integer.valueOf(ar.get('expr0')),
                                                             userId__c = usr.Id
                                                           
                                              ));
                        }
                     }
                    
            
            
            
            
            }          
            Database.delete( delLstMyCoverAll, false);
            Database.insert( lstMyCover, false);  
    
    
    
    
}