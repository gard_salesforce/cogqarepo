trigger UpdateCompanyName on Account(after update) {
    Map<ID, String> accountIdWithString = new Map<Id, String>();
    Map<ID, String> contactIdWithString = new Map<Id, String>();
    List<User> userToBeUpdated = new List<User>();
    Boolean accountNameChanged = false;
    String accName;
    
    if(Trigger.isUpdate && Trigger.isAfter){
        /** Start MYGP-31 **/
        for(Account eachAccount : Trigger.new){
            accName = '';
            Account oldAccount = Trigger.oldMap.get(eachAccount.Id);
            if(!eachAccount.Name.equals(oldAccount.Name)){
                accountNameChanged = true;
                accName = eachAccount.Name.length() > 80 ? eachAccount.Name.subString(0,80) : eachAccount.Name;
                accountIdWithString.put(eachAccount.ID, accName);
            }
        }
        
        if(accountNameChanged){
            List<Contact> allContacts = [SELECT ID,AccountId FROM CONTACT WHERE AccountId IN: accountIdWithString.keySet()];
            for(Contact aContact : allContacts){
                contactIdWithString.put(aContact.ID,accountIdWithString.get(aContact.AccountId));
            }
            
            List<User> allUsers = [SELECT ID,CompanyName,ContactId  FROM USER WHERE ContactId IN: contactIdWithString.keySet()];
            for(User anUser : allUsers){
                anUser.CompanyName = contactIdWithString.get(anUser.ContactId);
                userToBeUpdated.add(anUser);
            }
            
            if(userToBeUpdated.size() > 0){
                update userToBeUpdated;
            }
        }
        /** End MYGP-31 **/
    }
}