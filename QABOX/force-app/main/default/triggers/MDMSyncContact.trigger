trigger MDMSyncContact on Contact bulk (before insert,after insert,before update,after update,before delete,after delete) {

    // MM 28-11-14: added contact account address syncing when contact account is updated, not MDM related but adding it here saved another trigger, 
    //              and helps manage risks around conflicting functionality, trigger execution sequence etc.
    if (!System.isBatch() && trigger.isBefore && (trigger.isUpdate || trigger.isInsert)) {
        for(Contact con : trigger.new) {
            if(trigger.isInsert || (trigger.isUpdate && con.AccountId != trigger.oldMap.get(con.Id).get('AccountId'))) {
                processContacts pc = new processContacts();
                pc.syncConAccAddress(trigger.new);
            }
        }
    }        
        
    // MDM-specific functionality
    MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
    
    if(mdmSettings.Triggers_Enabled__c && mdmSettings.Contact_Trigger_Enabled__c){
        if (trigger.isBefore) {
            if (trigger.isInsert) {
                MDMSyncContactHandler.BeforeInsert(trigger.new, trigger.newMap);
            } else if (trigger.isUpdate) {
                MDMSyncContactHandler.BeforeUpdate(trigger.new, trigger.newMap, trigger.old, trigger.oldMap);
            } else if (trigger.isDelete) {
                MDMSyncContactHandler.BeforeDelete(trigger.old, trigger.oldMap);
            }
            
        } else if (trigger.isAfter) {
            if (trigger.isInsert) {
                MDMSyncContactHandler.AfterInsert(trigger.new, trigger.newMap);
            } else if (trigger.isUpdate) {
                MDMSyncContactHandler.AfterUpdate(trigger.new, trigger.newMap, trigger.old, trigger.oldMap);
            } else if (trigger.isDelete) {
                MDMSyncContactHandler.AfterDelete(trigger.old, trigger.oldMap);
            }
        }
    }

}