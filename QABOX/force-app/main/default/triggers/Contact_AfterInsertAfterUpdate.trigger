/*
*
* PCI-000113 - cDecisions Ltd - Steve O'Connell - 18/9/2013
*
* Trigger to remove contacts from active events/campaigns if the
* No Longer Employed by Company flag is set to true. 
*
*/


trigger Contact_AfterInsertAfterUpdate on Contact (before insert,after insert, after update) {

    Set<Id> contactsNoLongerWorkingIds = new Set<Id>();
    String delErrMsg = System.Label.Delete_Event_Member_Error;
    //Added For SF-3927
    if(trigger.isInsert && trigger.isBefore){
          ManningAgentID__c manningAgent=ManningAgentID__c.getInstance(userInfo.getUserId());
          if(manningAgent != null && (Trigger.new[0].Manning_Agent_or_Debit_Note_ID__c == null || Trigger.new[0].Manning_Agent_or_Debit_Note_ID__c =='')){
            Trigger.new[0].Manning_Agent_or_Debit_Note_ID__c=manningAgent.ManningAgentID__c;
          }
    }
    //Added For SF-3927
    if(trigger.isInsert && trigger.isAfter){
          ManningAgentID__c manningAgent=ManningAgentID__c.getInstance(userInfo.getUserId());
          if(manningAgent != null){
              try{
                    delete manningAgent;
              }
              catch(Exception e){system.debug('Exception Occured GG--'+e);}
          }
    }
    if(trigger.isUpdate)
    {
        for(Contact c:trigger.new)
        {
        
            if((c.No_Longer_Employed_by_Company__c == true) && (trigger.oldMap.get(c.Id).No_Longer_Employed_by_Company__c == false))
            {       
                //Flags has changed from false to true
                //Add contact Id to list of contacts to remove from events & campaigns
                
                contactsNoLongerWorkingIds.add(c.Id);
            }           
        }
        //GardUtils.updateUser(trigger.newMap.keySet()); //commenting because it is hampering other functionalities.
          
    }
    
    if(contactsNoLongerWorkingIds.size()>0)
    {
        //Get Event invitations for these contacts
        List<String> activeEventStatuses = new List<String>();
        activeEventStatuses.add('Draft');
        activeEventStatuses.add('Open');
        activeEventStatuses.add('Published');
        
        Map<Id, fluidoconnect__Invitation__c> eventInvitations = new Map<Id, fluidoconnect__Invitation__c>([SELECT Id,fluidoconnect__Contact__c FROM fluidoconnect__Invitation__c WHERE fluidoconnect__Contact__c IN :contactsNoLongerWorkingIds AND fluidoconnect__Survey__r.fluidoconnect__Status__c IN :activeEventStatuses AND fluidoconnect__Invite_Sent__c = false]);
        List<CampaignMember> campaignMembers = [SELECT Id FROM CampaignMember WHERE ContactId IN :contactsNoLongerWorkingIds AND Campaign.isActive = true];
        
        //delete eventInvitations;
        Database.DeleteResult[] drList = Database.delete(eventInvitations.values(), false); 

        for (Database.DeleteResult dr : drList) {
            if (dr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully deletes Invitation. Invitation ID: ' + dr.getId());
            }
            else {
                // Operation failed, so get all errors                
                /*for(Database.Error err : dr.getErrors()) {
                    System.debug('The following error has occurred.'); 
                    if(err.getStatusCode()==StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION && err.getMessage()=='Deleting member not allowed'){
                        System.debug(System.Label.Delete_Event_Member_Error);
                        //add an error to the contact record that generated the error.
                        Trigger.newMap.get(eventInvitations.get(dr.getId()).fluidoconnect__Contact__c).addError(System.Label.Delete_Event_Member_Error);
                    }else{                   
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        Trigger.newMap.get(eventInvitations.get(dr.getId()).fluidoconnect__Contact__c).addError(err.getStatusCode() + ': ' + err.getMessage());
                    }
                    System.debug('Fields that affected this error: ' + err.getFields());
                }*/
            }
        }
        
        delete campaignMembers;
    }
    //added for SF-272      
    if(Trigger.isInsert && Trigger.isAfter &&(Trigger.new[0].Manning_Agent_or_Debit_Note_ID__c != null && Trigger.new[0].Manning_Agent_or_Debit_Note_ID__c !=  '' && Trigger.new[0].Manning_Agent_or_Debit_Note_ID__c.startsWithIgnoreCase('a2l'))){        
        PEME_Manning_Agent__c manningAgent = new PEME_Manning_Agent__c();       
        manningAgent = [SELECT Id,Contact_Point__c FROM PEME_Manning_Agent__c WHERE Id =: Trigger.new[0].Manning_Agent_or_Debit_Note_ID__c];        
        manningAgent.Contact_Point__c = Trigger.new[0].Id;      
        try{        
            update manningAgent ;       
        }       
        catch(exception e){system.debug('Cannot update-->'+e.getMessage());}        
   }
   
}