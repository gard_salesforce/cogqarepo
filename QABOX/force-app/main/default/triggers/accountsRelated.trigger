trigger accountsRelated on Cover_and_Objects_for_case__c (after insert,after update,after delete) {

    if(Trigger.IsInsert && Trigger.IsAfter){
        Set<Id>coids = new Set<Id>();
        Set<Id>caseids = new Set<Id>();
        for(Cover_and_Objects_for_case__c co: Trigger.new){
            coids.add(co.id);
            caseids.add(co.Case__c);
        }
        delAttach.updateAccforCOB(coids,caseids);
    
    }
    
     if(Trigger.IsDelete && Trigger.IsAfter){
         System.debug('Inside del--->');
        Set<string>coids = new Set<string>();
        Set<Id>caseids = new Set<Id>();
        for(Cover_and_Objects_for_case__c co: Trigger.old){
        System.debug('co--->'+co);
            coids.add(co.Asset_Company_Id__c);
            caseids.add(co.Case__c);
        }
        System.debug('Accountids--->'+coids);
        delAttach.delforCOB(coids,caseids);
     }

}