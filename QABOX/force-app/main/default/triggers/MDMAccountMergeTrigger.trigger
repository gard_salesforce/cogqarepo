trigger MDMAccountMergeTrigger on MDM_Account_Merge__c (before insert) {
    List<ID> toCompanyIds = new List<Id>();
    for(MDM_Account_Merge__c m : Trigger.new){
        toCompanyIds.add(m.To_Account__c);
    }
    
    List<Account> allAccounts = [SELECT ID, Company_ID__c from ACCOUNT WHERE ID IN: toCompanyIds];
    //Create the Map
    Map<ID, String> accountMap = new Map<ID, String>();
    
    for(Account anAccount: allAccounts){
        accountMap.put(anAccount.ID,anAccount.Company_ID__c);
    }
    
    for(MDM_Account_Merge__c m : Trigger.new){
        m.To_Company_ID__c = accountMap.get(m.To_Account__c);
    }
}