trigger PreventDeactivateESPCompany on Account (before update) 
{
    //prevent me from deactivating any ESP company with related contacts that have ESP skills. SF-6207 
    List<Id> aid = new List<ID>();
    Map<Id, Account> AccMap = Trigger.NewMap;
    List<Contact> ctct = [select id, Accountid, name, No_Longer_Employed_by_Company__c,
                          (Select Id, Name From ESP_Contact_Skills__r where Id != null)
                          from Contact where No_Longer_Employed_by_Company__c = false AND accountId IN:trigger.new];
    for(Account acc: trigger.new){
        if(acc.Company_Status__c=='Inactive' && trigger.newMap.get(acc.id).Company_Status__c !=trigger.oldMap.get(acc.id).Company_Status__c){
            
            for(Contact con:ctct)
            {
                
                for(esp_Contact_skills__c esp: con.esp_contact_skills__r)
                {
                    if(esp.id!=null )
                    {
                        AccMap.get(con.Accountid).addError('You cannot deactivate a company where contacts have assigned an ESP Skill!');
                    }
                }
            }
        }
    }
}