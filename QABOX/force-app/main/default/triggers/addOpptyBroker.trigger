trigger addOpptyBroker on Opportunity (after insert, after update) {

//If the broker contact has changed, add them to the related list

    List<Partner> brokersToAdd = new List<Partner>();
            
    if(trigger.isInsert)
    {
        for(Opportunity opp:trigger.new)
        {
            if(opp.Broker_Contact__c != null)
            {
                brokersToAdd.add(new Partner(OpportunityId = opp.Id, AccountToId = opp.Broker_Id__c, Role='Broker'));
            }
        }
    }
    else if(trigger.isUpdate)
    {
        Set<Id> oppIds = new Set<Id>();
        
        for(Opportunity opp:trigger.new)
        {
            oppIds.add(opp.Id);
        }
        
        Boolean brokerAlreadyInList = false;
        Map<Id, Opportunity> existingRelBrokers = new Map<Id, Opportunity>([SELECT Id, (SELECT Id, AccountToId FROM OpportunityPartnersFrom WHERE Role='Broker') FROM Opportunity WHERE Id In :oppIds]);
        
        for(Opportunity opp:trigger.new)
        {           
            if(trigger.oldMap.get(opp.Id).Broker_Contact__c != opp.Broker_Contact__c && opp.Broker_Contact__c != null)
            {
                //Contact has changed, add to list
                brokerAlreadyInList = false;
                system.debug('About to check if partner already in list. List size: ' + opp.OpportunityPartnersFrom.size());
                for(OpportunityPartner broker: existingRelBrokers.get(opp.Id).OpportunityPartnersFrom)
                {
                    system.debug('Checking OpportunityPartner record: ' + broker.Id);
                    if(broker.AccountToId == opp.Broker_Id__c)
                    {
                        brokerAlreadyInList = true;
                    }
                }
                if(brokerAlreadyInList == false)
                {
                    System.debug('opp.Broker_Id__c-->' + opp.Broker_Id__c);
                    System.debug('opp.accountId-->' + opp.accountId);
                    
                    String brokersAccountId = opp.Broker_Id__c;
                    String partnersAccountId = opp.accountId;
                    
                    if(partnersAccountId != null && brokersAccountId.substring(0,15) != partnersAccountId.substring(0,15)){    //CRM 142
                        System.debug('brokersAccountId.substring(0,15)-->' + brokersAccountId.substring(0,15));
                        System.debug('partnersAccountId.substring(0,15)-->' + partnersAccountId.substring(0,15));
                        
                        brokersToAdd.add(new Partner(OpportunityId = opp.Id, AccountToId = opp.Broker_Id__c, Role='Broker',IsPrimary = true));
                    }
                }
            }   
        }
    }
    
    insert brokersToAdd;
    
}