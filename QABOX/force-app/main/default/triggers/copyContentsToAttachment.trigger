trigger copyContentsToAttachment on ContentDocumentLink (after insert,after update) {
    String parentid;
    contentVersion cv = new ContentVersion();
    attachment att = new attachment();
    system.debug('trigger.new[0].LinkedEntityId---'+trigger.new[0]);
    for(ContentDocumentLink cdl:trigger.new){
        system.debug('trigger.new[0].LinkedEntityId---'+cdl.LinkedEntityId);
        if(!string.valueOf(cdl.LinkedEntityId).startsWith('005')){
        cv = [select Title, FileType, VersionData from ContentVersion where ContentDocumentId =: trigger.new[0].ContentDocumentId];
            parentid = cdl.LinkedEntityId;
            system.debug('parentid---'+parentid+'--cv--'+cv);
            break;
        }
    }
    Blob body = cv.VersionData;
    att.parentId = parentid;
    att.Body = body;
    att.Name = cv.Title + '.' +cv.FileType;
    att.ContentType = cv.FileType;
    try{
        insert att;
    }catch(DmlException e){}
}