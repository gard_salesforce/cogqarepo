//Test Class - TestGardTeamTrigger
//trigger Trigger_CreateUpdateRelatedGardContact on Gard_Team__c (after insert, after update, after delete) {
trigger GardTeamTrigger on Gard_Team__c (before insert,before update,after delete) {//"before update,before insert,after delete" added for SF-4662
    
    //added for SF-4662
    if(Trigger.isBefore && Trigger.isInsert) GardTeamHelper.beforeInsert(Trigger.new);
    if(Trigger.isBefore && Trigger.isUpdate) GardTeamHelper.beforeUpdate(Trigger.new);
    if(Trigger.isafter && Trigger.isDelete) GardTeamHelper.afterDelete(Trigger.old);
    
    //Commenting whole code for SF-4662, to revert it back, remove all "SF-4662 Comment STARTS" lines
    /*SF-4662 Comment STARTS 
     //Trigger for update
    if(Trigger.isafter && Trigger.isinsert){
        List<Gard_Contacts__c> listGardContacts = new List<Gard_Contacts__c>();
        for(Gard_team__c gt : Trigger.new){
            Gard_contacts__c gc = new Gard_Contacts__c();
            gc.FirstName__c = gt.name;
            gc.phone__c = gt.phone__c;
            gc.Office_city__c = gt.office__c;
            gc.email__c = gt.P_I_email__c;
            gc.marine_email__c = gt.Marine_Email__c;
            gc.isActive__c = true;
            gc.Is_Gard_Team__c = true;
            
            listGardContacts.add(gc);
        }
        if (listGardContacts != null && listGardContacts.size() > 0){
            insert listGardContacts;
            //populateGardcontactIdInGardTeam(Trigger.new, listGardContacts);
        }
        /*
        @future
        populateGardcontactIdInGardTeam(List<Gard_Team__c> gardteamlist, List<Gard_Contacts__c> gardcontactlist){
            //Do something
        }
        */
    /*SF-4662 Comment STARTS
    }
    
    //Trigger for update
    if(Trigger.isafter && Trigger.isupdate){
        List<String> related_gardcontacts_id_list = new List<String>();
        for(Gard_team__c gt : Trigger.new){
            related_gardcontacts_id_list.add(gt.contactid__c);
        }
        List<Gard_Contacts__c> related_gardcontacts_list = [SELECT id from Gard_Contacts__c where id in :related_gardcontacts_id_list];
        Map<id, Gard_Contacts__c> mapGC = new Map<id, Gard_Contacts__c>();
        for (Gard_Contacts__c gc : related_gardcontacts_list){
            mapGC.put(gc.id, gc);
        }
        
        List<Gard_Contacts__c> tobeUdpatedList = new List<Gard_Contacts__c>();
        
        for(Gard_Team__c gt : Trigger.new){
            Gard_Contacts__c gc = mapGC.get(gt.contactId__c);
            gc.FirstName__c = gt.name;
            gc.phone__c = gt.phone__c;
            gc.isActive__c = gt.active__c;
            gc.Office_city__c = gt.office__c;
            gc.email__c = gt.P_I_email__c;
            gc.marine_email__c = gt.Marine_Email__c;
            gc.Is_Gard_Team__c = true;
            
            tobeUdpatedList.add(gc);
        }
        
        if(tobeUdpatedList!= null && tobeUdpatedList.size() > 0)
            update related_gardcontacts_list ;
    }
    
    //Trigger for delete
    /*
    if(Trigger.isafter && Trigger.isdelete){
        List<Gard_Contacts__c> listGardContacts = [SELECT id from Gard_Contacts__c where gard_team__c in Trigger.new];
        if(listGardContacts != null && listGardContacts.size() > 0){
            for(Gard_contacts__c g : listGardContacts){
                g.isActive__c = false;
            }
        }
        update listGardContacts;
    }
    */        
}