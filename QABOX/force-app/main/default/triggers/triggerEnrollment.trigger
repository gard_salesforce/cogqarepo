trigger triggerEnrollment on PEME_Enrollment_Form__c (before insert,before update,after insert,after update,before delete, after delete) {

    if(Trigger.isInsert && Trigger.isBefore){
        for(PEME_Enrollment_Form__c obj:Trigger.new){
            obj.Last_Status_Change_Date__c = Datetime.NOW();
        }
    }
    if(Trigger.isUpdate && Trigger.isBefore){
        for(PEME_Enrollment_Form__c obj:Trigger.new){
            if(obj.Enrollment_Status__c!=Trigger.oldMap.get(obj.Id).Enrollment_Status__c){
                obj.Last_Status_Change_Date__c = Datetime.NOW();
            }
        }
    }
    /*if(trigger.isInsert && trigger.isAfter){
           List<PEME_Enrollment_Form__c> enrollmentList=new List<PEME_Enrollment_Form__c>();
           for(PEME_Enrollment_Form__c enrollment:trigger.new){
             if(!enrollment.Enrollment_Status__c.equalsIgnoreCase('Draft')){
                enrollmentList.add(enrollment);
             }
           }
           if(!enrollmentList.isEmpty()){
                PEMEEnrollmentHelper.PEMEEnrollmentAfterInsert(enrollmentList);
           }
    }*/
    if((Trigger.isUpdate||Trigger.isInsert) && Trigger.isAfter){
             List<PEME_Enrollment_Form__c> enrollmentList=new List<PEME_Enrollment_Form__c>();
           for(PEME_Enrollment_Form__c enrollment:trigger.new){
             if(!enrollment.Enrollment_Status__c.equalsIgnoreCase('Draft')){
                enrollmentList.add(enrollment);
             }
           }
           if(!enrollmentList.isEmpty()){
                PEMEEnrollmentHelper.PEMEEnrollmentAfterInsert(enrollmentList);
           }
        //Profile objProf = [Select Id,Name From Profile where Name='Partner Community Login User Custom' LIMIT 1]; 
        //Profile objProf1 = [Select Id,Name From Profile where Name='Partner Community Pooled (copy from Login) User Custom' LIMIT 1];
        //for(PEME_Enrollment_Form__c obj:Trigger.new){
            //if(Trigger.new[0].Enrollment_Status__c != null && Trigger.Old[0].Enrollment_Status__c != null && Trigger.new[0].Enrollment_Status__c=='Enrolled' && Trigger.Old[0].Enrollment_Status__c=='Under Approval'){// && Trigger.oldMap.get(obj.Id).Enrollment_Status__c=='Under Approval'){
            //Code Modified by Tushar : 01/09/2017
            if(Trigger.isUpdate && Trigger.new[0].Enrollment_Status__c=='Enrolled' && Trigger.Old[0].Enrollment_Status__c=='Under Approval'){
                PEMELandingPageCtrl.sendEnrollmentApprovalEmails(Trigger.new[0]);
            }
            if(Trigger.new[0].Enrollment_Status__c=='Under Approval'){ //&& (objProf.Id==UserInfo.getProfileId() || objProf1.Id==UserInfo.getProfileId())){
                GardUtils.sendEnrollmentSubmissionEmails(Trigger.new[0].id);
            }
            if(Trigger.isUpdate)
            {
            if(Trigger.new[0].OwnerId !=Trigger.oldMap.get(Trigger.new[0].Id).OwnerId) // for owner change
            {              
             for(ProcessInstanceWorkitem piw : [SELECT Id,ActorId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =: Trigger.new[0].Id AND ProcessInstance.Status = 'Pending' LIMIT 1])
             {
                  if(piw != null)
                  {
                      piw.ActorId = Trigger.new[0].OwnerId;
                      
                      try {
                          update piw;
                      }
                      catch(DMLException ex){
                          system.debug('Exception found'+ex);
                      }
                  }
              }
              
            }
            }//end for owner
        }
    //}   
    if((Trigger.isUpdate||Trigger.isInsert) && (Trigger.isAfter ||Trigger.isBefore)){
       set<String> setAccIds = new set<String>();
       String status;
        list<Account> lstAccsToUpdt = new list<Account>();
         for(PEME_Enrollment_Form__c obj:Trigger.new){
              setAccIds.add(obj.ClientName__c);   
              status=obj.Enrollment_Status__c;        
         }
      if(setAccIds.size()>0){
             for(Account acc:[Select Id,PEME_Enrollment_Status__c from Account where Id IN:setAccIds]){
                 acc.PEME_Enrollment_Status__c=status;
                 lstAccsToUpdt.add(acc);
             }
             if(lstAccsToUpdt.size()>0){
                 try{
                     update lstAccsToUpdt;
                 }catch(DMLException ex){
                 
                 }
             }
         }
 
    }
    if(Trigger.isDelete&& Trigger.isBefore){
        set<String> setAccIds = new set<String>();
        list<Account> lstAccsToUpdt = new list<Account>();
         for(PEME_Enrollment_Form__c obj:Trigger.old){
              setAccIds.add(obj.ClientName__c);           
         }
         if(setAccIds.size()>0){
             for(Account acc:[Select Id,PEME_Enrollment_Status__c from Account where Id IN:setAccIds]){
                 acc.PEME_Enrollment_Status__c=null;
                 lstAccsToUpdt.add(acc);
             }
             if(lstAccsToUpdt.size()>0){
                 try{
                     update lstAccsToUpdt;
                 }catch(DMLException ex){
                 
                 }
             }
         }
    }
   /*  if((Trigger.isUpdate||Trigger.isInsert) && (Trigger.isAfter||Trigger.isBefore)){
       set<String> enrollId = new set<String>();
       String status;
        list<PEME_Manning_Agent__c> lstManAgent = new list<PEME_Manning_Agent__c>();
        list<PEME_Debit_note_detail__c> lstDebitNote = new list<PEME_Debit_note_detail__c>();
         for(PEME_Enrollment_Form__c obj:Trigger.new){
              enrollId.add(obj.id);   
              status=obj.Enrollment_Status__c;        
         }
         if(enrollId.size()>0){
             for(PEME_Manning_Agent__c acc:[Select Id,Status__c from PEME_Manning_Agent__c where PEME_Enrollment_Form__c IN:enrollId]){
                 if(status == 'Enrolled')
                 {
                     acc.Status__c = 'Approved';
                     lstManAgent.add(acc);
                 }
                 else if(status == 'Not Enrolled')
                 {
                     acc.Status__c = 'Rejected';
                     lstManAgent.add(acc);
                 }
             }
             if(lstManAgent.size()>0){
                 try{
                     update lstManAgent;
                 }catch(DMLException ex){
                 
                 }
             }
         }
         if(enrollId.size()>0){
             for(PEME_Debit_note_detail__c acc1:[Select Id,Status__c from PEME_Debit_note_detail__c where PEME_Enrollment_Form__c IN:enrollId]){
                 if(status == 'Enrolled')
                 {
                     acc1.Status__c = 'Approved';
                     lstDebitNote.add(acc1);
                 }
                 else if(status == 'Not Enrolled')
                 {
                     acc1.Status__c = 'Rejected';
                     lstDebitNote.add(acc1);
                 }
             }
             if(lstDebitNote.size()>0){
                 try{
                     update lstDebitNote;
                 }catch(DMLException ex){
                 
                 }
             }
         }
    }*/
    if(trigger.isbefore && trigger.isInsert)
    {
        GardUtils.updateGUID(Trigger.new);        
    }
    if(Trigger.isDelete && Trigger.isAfter){//SF-1902
        //PEMEEnrollmentHelper.afterDelete(Trigger.old);
    }
}