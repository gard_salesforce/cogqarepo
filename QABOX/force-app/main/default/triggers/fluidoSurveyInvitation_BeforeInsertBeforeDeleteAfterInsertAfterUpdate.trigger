//Trigger created for sending Marketing Cloud Emails from Salesforce
// Workflows 'Event Member Attending' and 'Event Member Cancelled' will not be used anymore 
// Email templated 'Event Decline Template' and 'Event Confirmation Template' will not be used any more
//previously created 2 triggers on 'Before insert' and 'Before Delete' also combined in this one trigger
trigger fluidoSurveyInvitation_BeforeInsertBeforeDeleteAfterInsertAfterUpdate on fluidoconnect__Invitation__c (before insert, before Delete, after insert, after update) {
    
    //before insert trigger part
    if((Trigger.isBefore) && (Trigger.isInsert))
    {
    
        //cDecisions Ltd - 9/9/13 - Check survey status before adding invitee
        
        //Build map of parent surveys and status
        
        Set<Id> flSurveyIds = new Set<Id>();
        
        for(fluidoconnect__Invitation__c flInvitation : trigger.new)
        {
            flSurveyIds.add(flInvitation.fluidoconnect__Survey__c);
        }
        
        Map<Id, fluidoconnect__Survey__c> flSurveys = new Map<Id, fluidoconnect__Survey__c>([SELECT Id, Invitations_Locked__c, fluidoconnect__Status__c, fluidoconnect__Event_Owner__c, fluidoconnect__Event_Organiser__c FROM fluidoconnect__Survey__c WHERE Id IN :flSurveyIds]);
        
        for(fluidoconnect__Invitation__c flInvitation : trigger.new)
        {
            if(flSurveys.get(flInvitation.fluidoconnect__Survey__c).Invitations_Locked__c == true)
            {
                if((UserInfo.getUserId() != flSurveys.get(flInvitation.fluidoconnect__Survey__c).fluidoconnect__Event_Owner__c) && (UserInfo.getUserId() != flSurveys.get(flInvitation.fluidoconnect__Survey__c).fluidoconnect__Event_Organiser__c))
                {
                    flInvitation.addError(Label.SurveyOnlyEventOrganisersCanAdd);
                }
            }
        }
        
    }
    // before delete trigger part
    
    if((Trigger.isBefore) && (Trigger.isDelete))
    {
        Set<Id> surveyIds = new Set<Id>();
        for(fluidoconnect__Invitation__c inv : Trigger.old) surveyIds.add(inv.fluidoconnect__Survey__c);
        
        Map<Id, fluidoconnect__Survey__c> mapSurveys = new Map<Id, fluidoconnect__Survey__c>();
        for(fluidoconnect__Survey__c survey : [SELECT Id, Invitations_Locked__c, fluidoconnect__Event_Owner__c,
                                               fluidoconnect__Event_Organiser__c, fluidoconnect__Status__c FROM fluidoconnect__Survey__c 
                                               WHERE Id IN :surveyIds]) {
                                                   mapSurveys.put(survey.Id, survey);
                                               }
        
        Id usrId = Userinfo.getUserId();
        for(fluidoconnect__Invitation__c inv : Trigger.old) {
            fluidoconnect__Survey__c survey = mapSurveys.get(inv.fluidoconnect__Survey__c);
            if(survey == null) continue;
            
            if(survey.Invitations_Locked__c) { // invitations already sent
                // only event owner or event organiser can delete the event member
                if(survey.fluidoconnect__Event_Owner__c != usrId && survey.fluidoconnect__Event_Organiser__c != usrId) {
                    inv.addError('You need to be the Event Owner or Organizer to delete an event member from an event where invitations are already sent!');
                } 
            }
        }
    }
    
    //after insert and after update part
    if(Trigger.isAfter)
    {
        List<Id> lstIds;
        map<Id, fluidoconnect__Survey__c> mapEvents;
        
        
        //collect event id
        lstIds = new List<Id>();
        for(fluidoconnect__Invitation__c s: Trigger.New)
        {       
            lstIds.add(s.fluidoconnect__Survey__c);
        }
        
        //collect event details
        if(lstIds != null || lstIds.size()>0)
        {
            mapEvents = new map<Id, fluidoconnect__Survey__c>();
            for(fluidoconnect__Survey__c c:[Select Id, Add_to_Calendar__c,Name, Event_Start_Date__c, fluidoconnect__Survey_Title_English__c, fluidoconnect__Event_Organiser__r.Name, fluidoconnect__Event_Organiser__r.Email from fluidoconnect__Survey__c where Id IN: lstIds])
            {
                mapEvents.put(c.Id, c);
            }
        }    
        
        //getCustomSettings Details
        List<MarketingCloudAPICall__c> lstVal = [SELECT Id, ClientId__c , Confirmation_TSDExternalKey__c, AccessToken__c , ClientSecret__c,EndPoint_SendEmail__c, EndPoint__c FROM MarketingCloudAPICall__c WHERE Name = 'APICallDetail'];
        system.debug('lstVal:' +lstVal);
        
        for(fluidoconnect__Invitation__c s: Trigger.New)
        {
            String organiserName, organiserEmail;  
            system.debug('Record:'+s);
            String eventDate  = String.valueOf(mapEvents.get(s.fluidoconnect__Survey__c).Event_Start_Date__c);
            String calenderEventUrl = String.valueOf(mapEvents.get(s.fluidoconnect__Survey__c).Add_to_Calendar__c);
            System.debug('calenderEventUrl :::::::::::::::'+calenderEventUrl);
            if(calenderEventUrl == null || calenderEventUrl == ''){
                calenderEventUrl = '';
        }
            if(eventDate != NULL && eventDate != '')
                eventDate = (mapEvents.get(s.fluidoconnect__Survey__c).Event_Start_Date__c).format();
            
            String organiserId = mapEvents.get(s.fluidoconnect__Survey__c).fluidoconnect__Event_Organiser__c;
            if(organiserId != NULL && organiserId != '')
            {
                organiserId = mapEvents.get(s.fluidoconnect__Survey__c).fluidoconnect__Event_Organiser__c;
                organiserName = mapEvents.get(s.fluidoconnect__Survey__c).fluidoconnect__Event_Organiser__r.Name;
                organiserEmail = mapEvents.get(s.fluidoconnect__Survey__c).fluidoconnect__Event_Organiser__r.Email;
                
            }           
           
            String title = string.valueOf(mapEvents.get(s.fluidoconnect__Survey__c));
            if(title != NULL && title != '')
            {
                //title = mapEvents.get(s.fluidoconnect__Survey__c).Name;
                title = mapEvents.get(s.fluidoconnect__Survey__c).fluidoconnect__Survey_Title_English__c; //SF-4201: Use Event Title (English) field
                if(title == NULL || title == '')
                    title = '';
            }
            system.debug('Current User id:'+UserInfo.getUserId());  
            //system.debug('Event Organiser Id:'+event.fluidoconnect__Event_Organiser__c);  
            
            //in case of update 
            if((organiserName != null || organiserName != '') && (organiserEmail != null || organiserEmail != '' ))
            {   
                system.debug('organiserName :::::::::::::'+organiserName+' '+'email::::::: '+organiserEmail);
                if((Trigger.isUpdate) && (UserInfo.getUserId() != organiserId) && ( s.fluidoconnect__Status__c != trigger.oldMap.get(s.id).fluidoconnect__Status__c )) //check if value in status field has changed
                { 
                    //system.debug('Event id:'+s.fluidoconnect__Survey__c);            
                    //system.debug('Event english title:'+event.fluidoconnect__Survey_Title_English__c);            
                    
                    //Attending event
                    // added member contact id as subscriber key for SF-4359
                    if(s.fluidoconnect__Status__c == 'Attending')
                    {
                        
                        MCSendAPIEmail.sendEmail(s.fluidoconnect__Contact__c,s.fluidoconnect__Member_email__c, lstVal[0].Confirmation_TSDExternalKey__c, title, s.fluidoconnect__Survey_Link_text__c, organiserName, organiserEmail, eventDate, 'Attending',calenderEventUrl);
                        system.debug('insert event ---> mail is sent::: attending ::::::::::::::');
                    }
                //Cancelled event
                 // added member contact id as subscriber key for SF-4359
                    else if(s.fluidoconnect__Status__c == 'Cancelled'){
                        MCSendAPIEmail.sendEmail(s.fluidoconnect__Contact__c,s.fluidoconnect__Member_email__c, lstVal[0].Confirmation_TSDExternalKey__c, title, s.fluidoconnect__Survey_Link_text__c, organiserName, organiserEmail, eventDate, 'Cancellation','');
                        system.debug('insert event ---> mail is sent::: Cancelling::::::::::::::');
                    }
                }
                
                //in case of insert   
                else if((Trigger.isInsert) && (UserInfo.getUserId() != organiserId)) 
                {
                    //Attending event
                     // added member contact id as subscriber key for SF-4359
                    if(s.fluidoconnect__Status__c == 'Attending'){
                    
                        MCSendAPIEmail.sendEmail(s.fluidoconnect__Contact__c,s.fluidoconnect__Member_email__c, lstVal[0].Confirmation_TSDExternalKey__c, title, s.fluidoconnect__Survey_Link_text__c, organiserName, organiserEmail, eventDate, 'Attending',calenderEventUrl);
                         system.debug('update event ---> mail is sent::: attending ::::::::::::::');
                    }
                        
                    
                    //Cancelled event
                     // added member contact id as subscriber key for SF-4359
                    else if(s.fluidoconnect__Status__c == 'Cancelled')
                        MCSendAPIEmail.sendEmail(s.fluidoconnect__Contact__c,s.fluidoconnect__Member_email__c, lstVal[0].Confirmation_TSDExternalKey__c, title, s.fluidoconnect__Survey_Link_text__c, organiserName, organiserEmail, eventDate, 'Cancellation','');
                        system.debug('update event ---> mail is sent::: CANCELLING ::::::::::::::');
                }
            }
        }
    }
    
}