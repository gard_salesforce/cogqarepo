trigger AgreementSetTrigger on agreement_set__c (before Insert) 
{
    if(Trigger.isbefore && Trigger.isInsert)
    { 
        BrokerShrRecordTriggerHelperCtrl.populateAgreementSetGuid(Trigger.New);
    }
}