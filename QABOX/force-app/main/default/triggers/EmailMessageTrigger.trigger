/**
* Created by ohuuse on 26/09/2019.
*/

trigger EmailMessageTrigger on EmailMessage(before insert, before update, after insert, after update){
    List<String> debugStrings = new List<String>();
    debugStrings.add('EmailMessageTrigger LOGS : ');
    debugStrings.add('Trigger.operationType - '+Trigger.operationType);
    debugStrings.add('TriggerStart EMs : ');
    for(EmailMessage em : Trigger.new) debugStrings.add(em.id+' : '+em.subject);
    if(Trigger.isInsert && Trigger.isBefore){
     
        EmailMessageService.newIncomingEmail(Trigger.New);
    }
    //Added by Arpan for SF-5440
    
    if((Trigger.isUpdate || Trigger.isInsert) && Trigger.isBefore && !(Trigger.isUpdate && (Trigger.old[0].Converted__c != Trigger.new[0].Converted__c))){
        List<id> emIds = new List<Id>();
        for(EmailMessage em:trigger.new){
            emIds.add(em.parentId);
        }
        Map<Id, Case> casesByIds = new Map<Id, Case>([SELECT Id, CaseNumber, RecordType.Name, Case_Ref_Id__c FROM Case WHERE Id IN :emIds]);
        debugStrings.add('casesByIds.size() - '+casesByIds.size());
        for(EmailMessage em:trigger.new){
            //system.debug('If variables 1: '+em.ParentId);
            //system.debug('If variables 2: '+casesByIds.get(em.ParentId).RecordType.Name);
            //system.debug('If variables 3: '+em.subject);
            try{
                if(!em.Incoming && casesByIds.get(em.ParentId) != null && !(em.subject.contains('[ ref:_')) && !Gard_RecursiveBlocker.blocker){//casesByIds.get(em.ParentId).RecordType.Name == 'Customer Transactions'){
                    if(!em.subject.Contains('Case Number:')){
                        em.subject = em.subject + ' [Case Number: '+casesByIds.get(em.ParentId).CaseNumber+' ]';
                    }else{               
                        em.subject = em.subject.substringBefore('[Case Number:') + ' [Case Number: '+casesByIds.get(em.ParentId).CaseNumber+' ]';
                    }
                    if(em.TextBody != null && em.TextBody.contains('ref:_00D')){
                        //em.TextBody = em.TextBody.replace('ref:_00D',casesByIds.get(em.ParentId).Case_Ref_Id__c +'; ref:_00D' );
                        em.TextBody = em.TextBody.substringBefore('ref:_00D')+casesByIds.get(em.ParentId).Case_Ref_Id__c.replace(' ]','').replace('[ ','')+ em.TextBody.subString(em.TextBody.indexOf('ref:_00D') + 30);
                        system.debug('i am here in text body-else-');
                    }
                    if(em.HtmlBody != null && em.HtmlBody.contains('ref:_00D')){
                        //em.HtmlBody = em.HtmlBody.replace('ref:_00D',casesByIds.get(em.ParentId).Case_Ref_Id__c +'; ref:_00D' );
                        em.HtmlBody = em.HtmlBody.substringBefore('ref:_00D')+casesByIds.get(em.ParentId).Case_Ref_Id__c.replace(' ]','').replace('[ ','')+ em.HtmlBody.subString(em.HtmlBody.indexOf('ref:_00D') + 30);
                        system.debug('i am here in html body-else-');
                    }
                    system.debug('Subject--'+em.subject);
                    debugStrings.add('em.subject - '+em.subject);
                }
            }catch(Exception ex){
                debugStrings.add('Exception - '+ex.getCause());
                debugStrings.add('Exception - '+ex.getStackTraceString());                
            }
        }
    }
    
    /*// commented for EmailReplyJob
if(trigger.isInsert && Trigger.isAfter){
EmailMessageService.newReplyOnClosedCase(Trigger.new);
//Added for storing incoming emails as EML Files
//StoreEmailsAsFilesHandler.afterInsert(Trigger.New[0].Id);
}
*/
    
    if((trigger.isInsert || trigger.isUpdate) && Trigger.isAfter ){  
        List<EmailMessage> incomingCaseEmails = new List<EmailMessage>();
        List<EmailMessage> outgoingCaseEmails = new List<EmailMessage>();
        for(EmailMessage em : Trigger.new){
            if(em.parentId != null && em.parentId.getSObjectType() == Case.SObjectType){
                debugStrings.add('Line 77 is true');
                if(em.incoming){
                    if(em.status.equals('0')) incomingCaseEmails.add(em);
                }else{
                    if(em.status.equals('3')) outgoingCaseEmails.add(em);
                }
            }
        }
        
        if(!incomingCaseEmails.isEmpty()) EmailMessageTriggerHelper.updateCaseActivity(incomingCaseEmails,true);//SF-5466
        if(!outgoingCaseEmails.isEmpty()) EmailMessageTriggerHelper.updateCaseActivity(outgoingCaseEmails,false);//SF-5466
        
        EmailMessageService.checkOutboundEmailSize(trigger.newMap); //SF-5437 added by Anju
    }
    
    //Pulkit's code STARTS
    if(trigger.isInsert && trigger.isAfter){
        if(Trigger.new.size() == 1 && Trigger.new[0].incoming && Trigger.new[0].parentId != null && Trigger.new[0].parentId.getSObjectType() == Case.sObjectType){
            debugStrings.add('Line 67 is true and EmailReparenting job was called');
            EmailMessageJobQueueingBatchScheduler.startDelayedQueue(Trigger.new[0].parentId,Trigger.new[0].fromAddress,(Trigger.new[0].messageDate+''));//SF-5381 & SF-5382
        }
    }
    debugStrings.add('TriggerEnd EMs : ');
    for(EmailMessage em : Trigger.new) debugStrings.add(em.id+' : '+em.subject);
    
    //Handle Out Of Office emails : SF-5820

    /*if(trigger.isInsert && trigger.isbefore && trigger.new[0].Incoming && trigger.new[0].subject.contains('Automatic reply')){
      system.debug('inside OOO******'+trigger.new[0].status);
        String csNum = '';
        String refNum = '';
        id oooCaseid = trigger.new[0].parentId;
         system.debug('case number in oooCaseid :'+oooCaseid );
        csNum = trigger.new[0].subject.substringAfter('Case Number: ').substringBefore(' ]');
        system.debug('case number in OOO:'+csNum);
        case cs = new case();
        cs = [SELECT id,Case_Ref_Id__c,New_incoming_email__c FROM Case WHERE CaseNumber =: csNum];
        trigger.new[0].parentId = cs.Id;
        if(trigger.new[0].TextBody != null)
            trigger.new[0].TextBody = trigger.new[0].TextBody+'\n'+cs.Case_Ref_Id__c.replace(' ]','').replace('[ ','');
        if(trigger.new[0].HtmlBody != null)
            trigger.new[0].HtmlBody = trigger.new[0].HtmlBody+'\n'+cs.Case_Ref_Id__c.replace(' ]','').replace('[ ','');
            //trigger.new[0].status= 'New';
        delete [SELECT Id FROM Case where Id =: oooCaseid];
    }*/
}