trigger updatePIHullCorrespFlag on CorrespondentPort__c (after update, after insert) {
    system.debug('----Corresp trigger fired ----');
    map<account,list<CorrespondentPort__c>> accsCorrepPortMap = new map<account,list<CorrespondentPort__c>>();
    map<id,account> accountMap = new map<id,account>();
    list<CorrespondentPort__c> accsCorrepPort ;
    set<CorrespondentPort__c> setAccsCorrepPort ;
    list<account> listToBeUpdated = new list<account>();
    //stores all correspondent ports
    list<CorrespondentPort__c> allCorresPorts = [SELECT Id,Company__c,P_I__c,Stop_Date_P_I__c,Hull__c,Stop_Date_Hull__c FROM CorrespondentPort__c];
    //to collect all the companies associated with correspondent ports
    Set<Id> corresAccIds = new Set<Id>();
    for(CorrespondentPort__c c:allCorresPorts){
        corresAccIds.add(c.Company__c);
    }
    List<account> listOfAllAcc = [SELECT Id FROM Account WHERE Id IN :corresAccIds];
    Set<account> setOfAllAcc = new Set<account>();
    setOfAllAcc.addAll(listOfAllAcc);
    for(Account acc:setOfAllAcc){
        accountMap.put(acc.Id,acc);
        system.debug('first all ccount map -- > '+accountMap);
    }
    if(trigger.isInsert || trigger.isUpdate){
        for(CorrespondentPort__c corr:trigger.new){
            accsCorrepPort = new list<CorrespondentPort__c>();
            setAccsCorrepPort = new set<CorrespondentPort__c>();
            //For loop to collect all correspondent ports of the account
            for(CorrespondentPort__c cmpCorr:allCorresPorts){
                if(corr.Company__c == cmpCorr.Company__c){
                    setAccsCorrepPort.add(cmpCorr);
                }
            }
            accsCorrepPort.addAll(setAccsCorrepPort);
            if(accsCorrepPortMap.get(accountMap.get(corr.Company__c)) == null){
                accsCorrepPortMap.put(accountMap.get(corr.Company__c),accsCorrepPort);
                system.debug('map of account and corr ports inside loop if --> '+accsCorrepPortMap);
            }
        }
        system.debug('map of account and corr ports--> '+accsCorrepPortMap);
        //Iterating over each accounts in the map
        for(account acc:accsCorrepPortMap.KeySet()){
            boolean PIFlag = false , HULLFlag = false; //variables to count number of pi and hull correspondents with no stop dates
            for(CorrespondentPort__c corr:accsCorrepPortMap.get(acc)){
                //to check whether all the correspondent ports has stop dates
                system.debug('----- Correspondent ports--- '+corr);
                system.debug('--Dates--> '+corr.Stop_Date_P_I__c+'--today-- '+date.today());
                if(corr.P_I__c == true && (corr.Stop_Date_P_I__c == null || corr.Stop_Date_P_I__c >= date.today())){
                    PIFlag = true;
                }
                if(corr.Hull__c == true && (corr.Stop_Date_Hull__c == null || corr.Stop_Date_Hull__c >= date.today())){
                    HULLFlag = true;    
                }
            }
            system.debug('----- PI HULL Count --- '+PIFlag + HULLFlag);
            if(PIFlag){
                acc.P_I_Correspondent__c = true;
            }
            else {
                acc.P_I_Correspondent__c = false; 
            }
            if(HULLFlag){
                acc.Hull_Correspondent__c  = true;
            }
            else {
                acc.Hull_Correspondent__c  = false; 
            }
            listToBeUpdated.add(acc);
        }
        system.debug('from trigger. list to be updated--> '+listToBeUpdated);
        try{
            update listToBeUpdated;
        }catch(Exception e){system.debug('Exception found--> '+e.getMessage());}
    }
}