trigger BrokerShrRecordTrigger on Contract (after insert, after update, after delete) 
{
    if(Trigger.isAfter && Trigger.isInsert)
    {
        BrokerShrRecordTriggerHelperCtrl.AfterInsert(Trigger.New);
    }
    if(Trigger.isAfter && Trigger.isUpdate)
    {
        BrokerShrRecordTriggerHelperCtrl.AfterUpdate(Trigger.OldMap, Trigger.NewMap);
    }
    if(Trigger.isAfter && Trigger.isDelete)
    {
        BrokerShrRecordTriggerHelperCtrl.AfterDelete(Trigger.oldMap.keySet());
    }
}