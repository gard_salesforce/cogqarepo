trigger MDMSyncMergedAccount on MDM_Account_Merge__c bulk (before insert,after insert,before update,after update,before delete,after delete) {

	MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
	
	if(mdmSettings.Triggers_Enabled__c && mdmSettings.MDM_Account_Merge_Trigger_Enabled__c){

	    if (trigger.isBefore) {
	        if (trigger.isInsert) {
				MDMSyncMergeAccountHandler.BeforeInsert(trigger.new, trigger.newMap);
	        } else if (trigger.isUpdate) {
	        	MDMSyncMergeAccountHandler.BeforeUpdate(trigger.new, trigger.newMap, trigger.old, trigger.oldMap);
	        } else if (trigger.isDelete) {
	            MDMSyncMergeAccountHandler.BeforeDelete(trigger.old, trigger.oldMap);
	        }
	    } else if (trigger.isAfter) {
			if (trigger.isInsert) {               
			    MDMSyncMergeAccountHandler.AfterInsert(trigger.new, trigger.newMap);
			} else if (trigger.isUpdate) {
				MDMSyncMergeAccountHandler.AfterUpdate(trigger.new, trigger.newMap, trigger.old, trigger.oldMap);
			} else if (trigger.isDelete) {
			    MDMSyncMergeAccountHandler.AfterDelete(trigger.old, trigger.oldMap);
			}
	    }
	}

}