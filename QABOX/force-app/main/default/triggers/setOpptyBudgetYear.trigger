trigger setOpptyBudgetYear on Opportunity (before insert, before update) {

    if(trigger.isInsert || trigger.isUpdate)
    {
        for(Opportunity opp: trigger.new)
        {
            //logic of workflow is implemented in trigger. //SF-4369
            if(opp.Expiry_Date__c == null || opp.StageName != 'Closed Won')
                opp.Expiry_Date__c = opp.CloseDate;
            //End - SF-4369  
            if(opp.Expiry_Date__c != null && (trigger.isInsert || opp.Expiry_Date__c != trigger.oldMap.get(opp.Id).Expiry_Date__c))
            {
                if((opp.Expiry_Date__c.month() == 1) || (opp.Expiry_Date__c.month() == 2 && opp.Expiry_Date__c.day()<20))
                {
                    //Date is before Feb 20th of the year. Set budget year as last year.
                    opp.Budget_Year__c = string.valueof(opp.Expiry_Date__c.year() - 1);
                }
                else
                {
                    //After 20th Feb, set budget year as this year
                    opp.Budget_Year__c = string.valueof(opp.Expiry_Date__c.year());
                }               
            }
            
            //SF-3744
            if(Trigger.isBefore && trigger.isUpdate){
                if(Trigger.oldMap.get(opp.id).StageName == 'Closed Lost' && opp.StageName == 'Risk Evaluation'){
                    opp.Reason_Lost_Declined__c = null;
                    opp.Reason_Lost_Declined_Other__c = null;  
                }
            }
            //End 
        }
          
    }
}