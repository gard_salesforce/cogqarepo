// ****Same logic is now maintained in Multi-company junction object's 
//*****trigger using GardUtils's UpdateAccount method

//New Version
trigger UserTriggerToUpdateAccount on User (after insert,after update) {
    /*Set<Id> accountIdsForActiveUsers = new Set<Id>();
    Set<Id> accountIdsForInactiveUsers = new Set<Id>();
    
    if(Trigger.isInsert && GardUtils.inUpdateAccountRecordFirst){
        for(User anUser : [Select id,IsActive,FirstName,ContactId,Contact.AccountId,Contact.Account.MyGard_enabled__c from User Where Id IN: Trigger.newMap.KeySet()]){
            System.debug('anUser from UserTriggerToUpdateAccount '+anUser );
            if(anUser.FirstName!=null && !anUser.FirstName.startsWithIgnoreCase('MyGard') && anUser.ContactId != null){
                // for inactive user
                if(!anUser.IsActive){
                    accountIdsForInactiveUsers.add(anUser.Contact.AccountId);
                }// for active user
                else if(anUser.IsActive && !anUser.Contact.Account.MyGard_enabled__c){
                    accountIdsForActiveUsers.add(anUser.Contact.AccountId);
                }
            }
        }
    }else if(Trigger.isUpdate && GardUtils.inUpdateAccountRecordFirst){
        for(User anUser : [Select id,IsActive,FirstName,ContactId,Contact.AccountId,Contact.Account.name,Contact.Account.MyGard_enabled__c from User Where Id IN: Trigger.newMap.KeySet()]){
            System.debug('anUser from UserTriggerToUpdateAccount '+anUser );
            if(anUser.FirstName!=null && !anUser.FirstName.startsWithIgnoreCase('MyGard') && anUser.ContactId != null){
                System.debug('2.anUser.Contact.AccountId -->'+anUser.Contact.AccountId);
                //Fetching the oldUser
                User oldUser = Trigger.oldMap.get(anUser.Id);
                //This is to check whether an active user become inactive due to Update operation in Salesforce
                if(oldUser.IsActive && !anUser.IsActive){
                        System.debug('3.anUser.Contact.AccountId -->'+anUser.Contact.AccountId);
                        accountIdsForInactiveUsers.add(anUser.Contact.AccountId);
                    }
                else if((!oldUser.IsActive && anUser.IsActive) && !anUser.Contact.Account.MyGard_enabled__c){
                    System.debug('4.anUser.Contact.AccountId -->'+anUser.Contact.AccountId);
                    accountIdsForActiveUsers.add(anUser.Contact.AccountId);
                }
            }
        }
    }
    if(accountIdsForActiveUsers.size() > 0){
        System.debug('accountIdsForActiveUsers --->'+accountIdsForActiveUsers);
        if(System.isFuture()||System.isBatch())
            GardUtils.updateAccountRecord(accountIdsForActiveUsers,true);
        else
            GardUtils.updateAccountRecordFuture(accountIdsForActiveUsers,true);
    }
    if(accountIdsForInactiveUsers.size() > 0){
        System.debug('accountIdsForInactiveUsers --->'+accountIdsForInactiveUsers);
        if(System.isFuture()||System.isBatch())
            GardUtils.updateAccountRecord(accountIdsForInactiveUsers,false);
        else
            GardUtils.updateAccountRecordFuture(accountIdsForInactiveUsers,false);
    }*/
}


//Old version
/*trigger UserTriggerToUpdateAccount on User (after insert,after update) {
    Set<Id> accountIds = new Set<Id>();
    Set<Id> accountIdsForInactiveUsers = new Set<Id>();
    if(!Test.isRunningTest())
    {
        for(User anUser : Trigger.new){
            if(anUser.FirstName!=null && !anUser.FirstName.startsWithIgnoreCase('MyGard') && anUser.ContactId != null){
                Contact associatedContact = [SELECT ID,AccountId  FROM CONTACT WHERE ID =: anUser.ContactId];
                Account associatedAccount = [SELECT ID,MyGard_enabled__c FROM ACCOUNT WHERE ID =: associatedContact.AccountId];
                
                if(!anUser.IsActive){
                    System.Debug('**** Inactive user ***');
                    List<User> associatedActiveUsers = [SELECT ID FROM USER WHERE IsActive = true AND (NOT FirstName LIKE 'MyGard%') AND ContactId IN 
                                                        (SELECT ID FROM CONTACT WHERE AccountId =: associatedAccount.ID )];
                    if(associatedActiveUsers.size() == 0){ //
                        accountIdsForInactiveUsers.add(associatedAccount.ID);
                    }
                }else if(anUser.IsActive && !associatedAccount.MyGard_enabled__c){
                    System.Debug('*** Active user ***');
                    accountIds.add(associatedAccount.ID);
                }
            }
        }
        if(accountIds.size() > 0){
             if(System.isFuture()||System.isBatch()){
            GardUtils.updateAccountRecord(accountIds,true);
             }else{
             GardUtils.updateAccountRecordFuture(accountIds,true);
            }  
        }
        
         if(accountIdsForInactiveUsers.size() > 0){
             if(System.isFuture()||System.isBatch()){
            GardUtils.updateAccountRecord(accountIdsForInactiveUsers,false);
              }else{
             GardUtils.updateAccountRecordFuture(accountIdsForInactiveUsers,false);
            }  
        }
        
    }
}*/