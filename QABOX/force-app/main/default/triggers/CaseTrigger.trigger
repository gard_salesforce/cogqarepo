//Test Class : TestCaseTrigger
trigger CaseTrigger on Case (before update, after insert, before insert, after update/*, after delete*/) 
{    
    Case objectCase = new Case();
    Id profileId=userinfo.getProfileId();
  //  String profileName = [Select Id,Name from Profile where Id=:profileId].Name;
  //  system.debug('ProfileName'+profileName);
    
    // for contract review
    // type changed as per mail
   /* commented for SF - 5273
    if(Trigger.isUpdate && Trigger.isAfter && Trigger.New[0].Type != null && Trigger.New[0].Type.equalsIgnoreCase('Crew Contract Review'))
    {        
        System.debug('update contract review object  &&&&&&&&&&&&&&&&&&&');
        // update contract review object
        if( Trigger.New[0].id != null)
            GardUtils.updateContractReview(Trigger.New[0]);            
        // Webservice call for owner change 
        if( Trigger.Old[0].ownerId != null && Trigger.New[0].ownerId != null && (Trigger.New[0].ownerId != Trigger.Old[0].ownerId) && Trigger.New[0].DM_Contract_Review_Case_ID__c != null)
        {
            // restrict for first update from ContractReviewLOCCtrl controller
            if(ContractReviewLOCCtrl.chkboolean){
                System.debug('Calling changeOwner_WS from trigger for owner change');
                ContractReviewLOCCtrl.changeOwner_WS(String.valueOf(Trigger.New[0].ownerId),Trigger.New[0].DM_Contract_Review_Case_ID__c);    
                
                // task creation on owner change
                //NOTE to reviewer : Explicit query is required since owner.name returns null in SF ;)
                System.debug('prev user name '+Trigger.Old[0].owner.Name);
                String prevUserName = [SELECT name from User where id = :Trigger.Old[0].ownerId limit 1].name; 
                
                ContractReviewLOCCtrl.createTaskOnOwnerChange(Trigger.New[0].Name_Of_Contract__c, prevUserName, Trigger.New[0].ownerId, Trigger.New[0].id);
            }
        }
        // Webservice call for Reviewer change
        if( Trigger.Old[0].Reviewer__c != null && Trigger.New[0].Reviewer__c != null && Trigger.New[0].Reviewer__c != Trigger.Old[0].Reviewer__c && Trigger.New[0].DM_Contract_Review_Case_ID__c != null)
        {
            // restrict for first update from ContractReviewLOCCtrl controller
            if(ContractReviewLOCCtrl.chkboolean){
                System.debug('Calling changeOwner_WS from trigger for reviewer change');
                // insert manual apex sharing record on reviewer change           
                ContractReviewLOCCtrl.changeOwner_WS(String.valueOf(Trigger.New[0].Reviewer__c),Trigger.New[0].DM_Contract_Review_Case_ID__c);
            }
        }        
    }    
    
    commented for SF - 5273 */
    Boolean isThisFromReportAClaim = false;
    if(trigger.isUpdate && (!GardUtils.isUpdateForAssignRule)){
        
        for(Case c : Trigger.old){
            if(c.status == 'Draft')
                isThisFromReportAClaim = true;
        }
        
    }
    if(trigger.isbefore && trigger.isInsert)
    {
        GardUtils.updateGUID(Trigger.new);
        
        //SF 1866
        GardUtils.populateClaimPicklistFieldsForCase(Trigger.isInsert, Trigger.new);
        system.debug('trigger execution start*******');
        //GardUtils.setAssignmentRule(Trigger.new); //SF-5255
                
    }    
    
    if(!isThisFromReportAClaim){
        if(trigger.isBefore)
        {
            //Update_Status_Change_Date whenever the status is changed
            /*List<Case> oldValues=Trigger.Old;
            List<Case> newValues=Trigger.New;
            Map<Id,Case> mapOldCase=new Map<Id,Case>();
            mapOldCase.putAll(oldValues);
            for(Case aCase:newValues)
            {
                if(mapOldCase.containsKey(aCase.Id))
                {
                    Case oldCase =mapOldCase.get(aCase.Id);
                    if(oldCase.Status_Change__c!=aCase.Status_Change__c)
                    {
                      aCase.Status_Change_Date__c=DateTime.Now();   
                    }  
                }
            }*/
        }
        
        if(trigger.isAfter && trigger.isInsert)
        {    // change case owner for after insert on case
            system.debug('**************trigger.newMap  '+trigger.newMap);    
            GardUtils.changeCaseOwner(trigger.newMap);
            //GardUtils.setAssignmentRule(Trigger.new); //SF-5255
            if(!test.isrunningtest()){
                GardUtils.insertContractReview(Trigger.new[0].id); 
            }
            Map<Id,Set<Id>> caseids= new Map<Id,Set<Id>>();
            for(Case cs:Trigger.new){
                 if((cs.Submitter_company__c!= null ) || (cs.AccountId!= null)){
                     if(!caseids.keyset().contains(cs.id)){
                         set<id> acids= new set<id>();
                         if(cs.Submitter_company__c!= null )
                         acids.add(cs.Submitter_company__c);
                         if(cs.AccountId!= null )
                         acids.add(cs.AccountId);
                         if(cs.Account_Id__c!= null )
                         acids.add(cs.Account_Id__c);
                         caseids.put(cs.id,acids);        
                     }
                 }   
            
            }
            System.debug('CaseIDs--->'+caseids);
            if(caseids.size()>0)
            delAttach.updateAccforCase(caseids);
        }
        
        //sf-3511- start
        if(trigger.isAfter && ((trigger.isInsert) || (trigger.isUpdate && (!GardUtils.isUpdateForAssignRule))))
        {
            system.debug('test sf-3511');
            List<Case> lstCasestoUpdate = new List<Case>();
            List<RecordType> lstRecordType = [SELECT Id, Name FROM RecordType WHERE Name = 'Claim' and sObjectType = 'Case'];
            String strId = lstRecordType[0].Id;
            Map<Id,Case> lstCase =Trigger.newMap;
            map<Id,Case> caseAccPA = new map<Id,Case>([ Select Id, RecordTypeId, Claims_handler__c, Claims_handler__r.IsActive, OwnerId from Case where id In: lstCase.keySet()]);
            if(caseAccPA != null){
                for(Case c:caseAccPA.values())
                {
                    //system.debug('c.RecordTypeId:'+c.RecordTypeId);
                    //system.debug('strId:'+strId);
                    //system.debug('c.Claims_handler__c:'+c.Claims_handler__c);
                    //system.debug('c.OwnerId:'+c.OwnerId);
                    //system.debug('CH Active/Inactive:'+c.Claims_handler__r.IsActive);
                    if((c.RecordTypeId == strId) && (c.Claims_handler__c != null) && (c.Claims_handler__c != c.OwnerId))
                    {
                        if(c.Claims_handler__r.IsActive){
                            c.OwnerId  = c.Claims_handler__c;
                            lstCasestoUpdate.add(c);                    
                        }
                    }
                }
            }
            system.debug('lstCasestoUpdate size:'+lstCasestoUpdate.size());
            if(lstCasestoUpdate.size()>0)
            {
                if(!test.isRunningTest())
                    GardUtils.updateClaimOwners(lstCasestoUpdate);
            }
        }
        //sf-3511 - end
        // Used by time based workflow for deleting cases which are in delete status
        if(trigger.isAfter || trigger.isBefore)
        {
            Set<Id> setCaseId = new Set<Id>();
            Map<Id,Case> updatedCase =Trigger.newMap;
            if(updatedCase != null)
            {
                for(Case caseObj :updatedCase.values())
                {
                    if(caseObj.status ==  'Delete')
                        setCaseId.add(caseObj.id);
                }
                
                if(setCaseId.size() >0)
                    GardUtils.deleteCase(setCaseId); 
            }
        }
    }
    if(trigger.isAfter && trigger.isUpdate && (!GardUtils.isUpdateForAssignRule)){
        GardUtils.updateCaseStatusForCrewContractLOC(trigger.oldMap, trigger.newMap);
        GardUtils.setCaseOwnerAsFileOwner(Trigger.oldMap,Trigger.newMap);//added for SF-5376
        //Added for 5545
        Map<Id,Set<Id>> caseidsupdate= new Map<Id,Set<Id>>();
        Set<String> accids= new Set<String>();
        set<id> caseids= new set<id>();
        //Set<String> accidsdel= new Set<String>();//1
        //Set<id> caseidsdel= new set<id>();//2
        String action='';
            for(Case cs:Trigger.new){
            System.debug('sc--->'+cs.Submitter_company__c);
            System.debug('ac--->'+cs.AccountId);
                 if((cs.Account_Id__c != Trigger.oldMap.get(cs.id).Account_Id__c && cs.Account_Id__c != null) || (cs.Submitter_company__c != Trigger.oldMap.get(cs.id).Submitter_company__c && cs.Submitter_company__c!= null) || (cs.AccountId != Trigger.oldMap.get(cs.id).AccountId && cs.AccountId != null)){
                     //caseidsdel.add(cs.id);
                     action='Update';
                     if(!caseidsupdate.keyset().contains(cs.id)){
                         set<id> acids= new set<id>();
                         if((cs.Submitter_company__c != null ) && cs.Submitter_company__c != Trigger.oldMap.get(cs.id).Submitter_company__c ){
                            acids.add(cs.Submitter_company__c);
                            //accidsdel.add(Trigger.oldMap.get(cs.id).Submitter_company__c);
                         }
                         if(cs.AccountId!=Trigger.oldMap.get(cs.id).AccountId && (cs.AccountId != null )){
                            acids.add(cs.AccountId);
                            //accidsdel.add(Trigger.oldMap.get(cs.id).AccountId);
                         }
                         
                         if(cs.Account_Id__c!=Trigger.oldMap.get(cs.id).Account_Id__c && (cs.Account_Id__c != null )){
                            acids.add(cs.Account_Id__c);
                            //accidsdel.add(Trigger.oldMap.get(cs.id).Account_Id__c);
                         }caseidsupdate.put(cs.id,acids);        
                     }
                 }
                  else if((cs.Account_Id__c != Trigger.oldMap.get(cs.id).Account_Id__c && cs.Account_Id__c == null) || (cs.Submitter_company__c != Trigger.oldMap.get(cs.id).Submitter_company__c && cs.Submitter_company__c== null) || (cs.AccountId != Trigger.oldMap.get(cs.id).AccountId && cs.AccountId == null)){
                     System.debug('Inside else if--->');
                     action='Delete';
                         caseids.add(cs.id);
                         if((cs.Submitter_company__c == null ) && cs.Submitter_company__c != Trigger.oldMap.get(cs.id).Submitter_company__c )
                         accids.add(Trigger.oldMap.get(cs.id).Submitter_company__c);
                         if(cs.AccountId!=Trigger.oldMap.get(cs.id).AccountId && (cs.AccountId == null ))
                         accids.add(Trigger.oldMap.get(cs.id).AccountId);
                         if(cs.Account_Id__c!=Trigger.oldMap.get(cs.id).Account_Id__c && (cs.Account_Id__c == null ))
                         accids.add(Trigger.oldMap.get(cs.id).Account_Id__c);
                         //caseidsupdate.put(cs.id,acids);        
                   
                 }   
            
            }
            System.debug('CaseIDs--->'+caseids);
            if(caseidsupdate.keyset().size()>0 && action=='Update'){
                delAttach.updateAccforCase(caseidsupdate);
                //delAttach.delAccforCase(accidsdel,caseidsdel);    
            }
            delAttach.updateAccforCase(caseidsupdate);
            if(accids.size()>0 && action=='Delete')
            delAttach.delAccforCase(accids,caseids);
 
    }
    
    if(trigger.isBefore && trigger.isUpdate && (!GardUtils.isUpdateForAssignRule)){
        GardUtils.restrictUpdate(trigger.New[0]);
        //SF 3660
        System.debug('From trigger-->' + Trigger.new);
        if(!test.isrunningtest()){
            GardUtils.populateClaimPicklistFieldsForCase(Trigger.isInsert, Trigger.new);
        }
        
       // GardUtils.newCaseUserAssignment(Trigger.oldMap,Trigger.newMap);//added for SF-5015, modified for SF-5114
    }
    /*
    if(trigger.isAfter && trigger.isDelete){
        System.debug('Inside After delete Trigger');
        Set<Id> caseids= new Set<Id>();
        for(Case cs : trigger.old) {
             if(cs.MasterRecordId != null) {
                 caseids.add(cs.MasterRecordId);
             }
        }
        System.debug('Caseids--->'+caseids);
        if(caseids.size()>0){
           // delAttach.deletemergecase(caseids);     //added as part of 5567
        }
    }*/
    
    if(trigger.isAfter && trigger.isInsert){
        GardUtils.setAssignmentRule(Trigger.new); //SF-5255
    
    }
}