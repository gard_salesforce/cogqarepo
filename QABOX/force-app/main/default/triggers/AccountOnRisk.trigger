trigger AccountOnRisk on Account (after insert, after update) {
    MDM_Settings__c mdmSettings;
    if(Test.isRunningTest()){
        mdmSettings = MDM_Settings__c.getInstance();
        mdmSettings.Triggers_Enabled__c = true;
        mdmSettings.AccountOnRisk_Trigger_Enabled__c = true;
    }else{
        mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
    }
    
    if(mdmSettings.Triggers_Enabled__c){// SF-3966 Commented because it needs to run for everyone && mdmSettings.AccountOnRisk_Trigger_Enabled__c){
        if(ObjectTree.TestTrigger){
            if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){	
                ObjectTree.updateParentRisk(Trigger.newMap.keySet());
            }
        }
    }
}