trigger EditCorrespondentContactSubscriptionFields on Contact (before update,before insert) {
    List<String> contactNotEditableFields = new List<String>();
    List<String> testFields = new List<String>();
    boolean showError=false;
    String errorMsg = Label.CorrespondingContactErrorMsg;
    Map<String, Schema.SObjectField> contactFields = Schema.SObjectType.Contact.fields.getMap();
    //Fetching Subscription field names from custom settings
    ContactSubscriptionFields__c conSubFields = ContactSubscriptionFields__c.getInstance('Contact Subscription Fields');
    ContactSubscriptionFields__c conSubFields2 = ContactSubscriptionFields__c.getInstance('Contact Subscription Fields 2');
    ContactSubscriptionFields__c conSubFields3 = ContactSubscriptionFields__c.getInstance('Contact Subscription Fields 3');
    String subscriptionFieldsNames;
    //concatinating all the field names into one string
    if(conSubFields != null && conSubFields2 != null && conSubFields3 != null){
        subscriptionFieldsNames = conSubFields.Subscription_Fields__c + conSubFields2.Subscription_Fields__c + conSubFields3.Subscription_Fields__c ;
    }
    if(subscriptionFieldsNames != null && subscriptionFieldsNames != ''){
        system.debug('test class TESTING : '+subscriptionFieldsNames);
        for(String str : contactFields.keyset()) {
            system.debug('test class TESTING FROM FOR: '+subscriptionFieldsNames);
            if(!subscriptionFieldsNames.containsIgnoreCase(str)){
                contactNotEditableFields.add(str);
            }
        }
    }
    DQ__c dqSetting = DQ__c.getOrgDefaults();
    Set<Id> permissionSetIds = new Set<Id>();
    for(SetupEntityAccess access : [SELECT ParentId FROM SetupEntityAccess WHERE SetupEntityId IN (SELECT Id FROM CustomPermission WHERE DeveloperName = 'Edit_Correspondent_Contacts')]){
    permissionSetIds.add(access.ParentId);
    }
    List<PermissionSetAssignment> assigneeList = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId IN :permissionSetIds AND AssigneeId =: userInfo.getUserId()] ;
    for(contact NewContact : trigger.new){
        //Checks whether the logged in user can edit the contact or not
        if((dqSetting.Enable_Validation__c) && (NewContact.recordTypeId == '012D0000000UV5lIAG') && (assigneeList.size() == 0) && trigger.isUpdate){
            contact oldContact = trigger.oldMap.get(NewContact.Id);
            for(String fields : contactNotEditableFields){
                if(NewContact.get(fields) != oldContact.get(fields)){
                    NewContact.addError(errorMsg);
                    break;
                }
            }
        }
    }
}