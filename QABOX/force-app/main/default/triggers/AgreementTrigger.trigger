//Test Class : Any Test run that creates or updates contract would work
trigger AgreementTrigger on Contract (before insert,before update){
    if(Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)){//added for SF-6784
        for(Contract agreement : Trigger.new){
            agreement.Shared_with_Client__c = true;
        }
    }
}