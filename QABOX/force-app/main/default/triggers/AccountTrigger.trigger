trigger AccountTrigger on Account (before update, after update, before insert, after insert) {
    
    static String triggerContext = ((Trigger.isBefore ? 'Before' : 'After') + (Trigger.isInsert ? ' Insert' : ' Update') + (System.isFuture() ? ' Future' : (System.isBatch() ? ' Batch' : '')));
    //System.debug('Trigger run Context - '+triggerContext);
    //System.debug('Account Trigger Run Context - SOQL used/Total - '+triggerContext+' - '+Limits.getQueries()+'/'+Limits.getLimitQueries());
    //System.debug('beforeUpdateRunOnce/afterUpdateRunOnce/beforeInsertRunOnce/afterInsertRunOnce - '+AccountHelper.beforeUpdateRunOnce+'/'+AccountHelper.afterUpdateRunOnce+'/'+AccountHelper.beforeInsertRunOnce+'/'+AccountHelper.afterInsertRunOnce);
    if(trigger.isBefore && trigger.isInsert)
    {
        //System.debug('SF-4462 AccountTrigger.beforeInsertTrigger accList[0].Manning_Agent_or_Debit_Note_ID__c - '+Trigger.new[0].Manning_Agent_or_Debit_Note_ID__c);
        if(!AccountHelper.beforeInsertRunOnce){
            AccountHelper.beforeInsertRunOnce = true;
            AccountHelper.AccountBeforeInsert(trigger.new);
        }
        //System.debug('SF-4462 AccountTrigger.beforeInsertTrigger accList[0].Manning_Agent_or_Debit_Note_ID__c - '+Trigger.new[0].Manning_Agent_or_Debit_Note_ID__c);
    }
    if(trigger.isAfter && Trigger.isInsert)
    {
        //System.debug('SF-4462 AccountTrigger.afterInsertTrigger accList[0].Manning_Agent_or_Debit_Note_ID__c - '+Trigger.new[0].Manning_Agent_or_Debit_Note_ID__c);
        if(!AccountHelper.afterInsertRunOnce){
            AccountHelper.afterInsertRunOnce = true;
            AccountHelper.AccountAfterInsert(trigger.new);
        }
        //System.debug('SF-4462 AccountTrigger.afterInsertTrigger accList[0].Manning_Agent_or_Debit_Note_ID__c - '+Trigger.new[0].Manning_Agent_or_Debit_Note_ID__c);
    }
    if(trigger.isBefore && trigger.isUpdate)
    {
        if(!AccountHelper.beforeUpdateRunOnce){
            AccountHelper.beforeUpdateRunOnce = true;
            AccountHelper.accountBeforeUpdate(trigger.new,trigger.oldMap);
        }
    }
    if(trigger.isAfter && trigger.isUpdate)
    {
        if(!AccountHelper.afterUpdateRunOnce){
            AccountHelper.afterUpdateRunOnce = true;
            AccountHelper.accountAfterUpdate(trigger.new,trigger.oldMap);
        }
    }
    
    //Added For SF-3927
    if(trigger.isInsert && trigger.isBefore){
          ManningAgentID__c manningAgent=ManningAgentID__c.getInstance(userInfo.getUserId());
          if(manningAgent != null && (Trigger.new[0].Manning_Agent_or_Debit_Note_ID__c == null || Trigger.new[0].Manning_Agent_or_Debit_Note_ID__c == '')){
            Trigger.new[0].Manning_Agent_or_Debit_Note_ID__c=manningAgent.ManningAgentID__c;
            System.debug('---Executed--');
          }
    }
    //Added For SF-3927
    if(trigger.isInsert && trigger.isAfter){
          ManningAgentID__c manningAgent=ManningAgentID__c.getInstance(userInfo.getUserId());
          if(manningAgent != null){
              try{
                    delete manningAgent;
                    system.debug('---Deleted--');
              }
              catch(Exception e){system.debug('Exception Occured GG--'+e);}
          }
    }
    
 /*   if(Trigger.isUpdate && Trigger.isbefore)
    {
        system.debug('Trigger.new ---->'+Trigger.new);
        if(Trigger.new[0].Company_Status__c == 'Inactive' && Trigger.old[0].Company_Status__c == 'Active')
        {
            Trigger.new[0].ParentId = null;
            system.debug('Trigger.new -asdasdasdasdasd--->'+Trigger.new);
        }
        system.debug('Trigger.new ---->'+Trigger.new);
        system.debug('Trigger.new ---->'+Trigger.new[0].Company_Status__c);
    }
    if(Trigger.isUpdate && Trigger.isAfter)
    {    
        List<Account> failedAccounts = new List<Account>();
        List<Id> accountIdsToSetNotifyAdminFalse = new List<Id>();
        List<Id> accountIdsToSetNotifyAdminTrue = new List<Id>();
        if(Trigger.isUpdate && Trigger.isAfter){
            List<Contact> allContactsToBeUpdated = new List<Contact>();
            for (Account anAccount : Trigger.new) {
                //START -- Code for Account Sync Failed Notification
                if(anAccount.Account_Sync_Status__c == 'Synchronised' && anAccount.notifyAdmin__c){
                    //Update Account notify admin flag to false
                    accountIdsToSetNotifyAdminFalse.add(anAccount.Id);
                }else if(anAccount.Account_Sync_Status__c == 'Sync Failed' && !anAccount.notifyAdmin__c){
                    //Update account notify admin flag to true
                    accountIdsToSetNotifyAdminTrue.add(anAccount.Id);
                    failedAccounts.add(anAccount);
                }
                //END -- Code for Account Sync Failed Notification
                
                Account oldAccount = Trigger.oldMap.get(anAccount.Id);
                if ((oldAccount.P_I_Member_Flag__c != anAccount.P_I_Member_Flag__c) && anAccount.Company_Role_Text__c.contains('Client')) {
                  for(Contact con : [Select Id, Name, Publication_Member_circulars__c, Publications_Gard_Rules__c, Publication_Guidance_to_Master__c,
                                            Publication_Rules_Number_of_copies__c,Publication_GtM_Number_of_Copies__c
                                         From Contact Where Accountid =: anAccount.ID AND Primary_Contact__c = true] ){
                        if(anAccount.P_I_Member_Flag__c == true){
                            con.Publication_Member_circulars__c = true;
                            con.Publications_Gard_Rules__c = true;
                            con.Publication_Guidance_to_Master__c = true;
                            if(con.Publication_Rules_Number_of_copies__c ==null || con.Publication_Rules_Number_of_copies__c ==''){
                                con.Publication_Rules_Number_of_copies__c='1';
                            }
                            if(con.Publication_GtM_Number_of_Copies__c ==null || con.Publication_GtM_Number_of_Copies__c ==''){
                                con.Publication_GtM_Number_of_Copies__c='1';
                            }
                        }else{
                            con.Publications_Gard_Rules__c = false;
                            con.Publication_Guidance_to_Master__c = false;
                        }
                        allContactsToBeUpdated.add(con);
                    }
                }
            }
            if(allContactsToBeUpdated.size() > 0){
                update allContactsToBeUpdated;
            }
            if(accountIdsToSetNotifyAdminFalse.size() > 0 && !test.isRunningTest() && !System.isFuture() && !System.isBatch()){
                AccountHelper.updateAccountRecordForNotifyAdmin(false, accountIdsToSetNotifyAdminFalse);
            }
            if(accountIdsToSetNotifyAdminTrue.size() > 0 && !test.isRunningTest() && !System.isFuture() && !System.isBatch()){
                AccountHelper.updateAccountRecordForNotifyAdmin(true, accountIdsToSetNotifyAdminTrue);
            }
            
            //START -- Code for Account Sync Failed Notification - Send Email
            if(failedAccounts.size() > 0){
                Sys_Admin_Email_Synch_Failed__c accountSyncFailedEmailNotification = Sys_Admin_Email_Synch_Failed__c.getValues('Account Sync Failed');
                if(Test.isRunningTest()){
                    MailUtil.sendAccountSyncFailureMail(failedAccounts);
                }else if(accountSyncFailedEmailNotification.Should_Send_Email__c){
                    MailUtil.sendAccountSyncFailureMail(failedAccounts);
                }
            }
            //END -- Code for Account Sync Failed Notification - Send Email
        }
    }
     
    //CRM - 144 - Code has been merged from Accounthelper class
    //CRM 167
    if((Trigger.isInsert || Trigger.isUpdate) && Trigger.isAfter){// && !GardUtils.approvalalreadysubmitted){
        try{
            GardUtils.submitApproval(Trigger.new);
        }
        catch(Exception e){    //catch for any issue for debugging
            System.debug('Error occurredto during GardUtils.submitApproval-->' + e.getMessage());
        }
    }
    //CRM 167
    //END CRM 144 
     //added for SF-272
     if(Trigger.isInsert && Trigger.isAfter && (trigger.new[0].Manning_Agent_or_Debit_Note_ID__c != null && trigger.new[0].Manning_Agent_or_Debit_Note_ID__c != '')){
        String objType = '';
        if(trigger.new[0].Manning_Agent_or_Debit_Note_ID__c != null && trigger.new[0].Manning_Agent_or_Debit_Note_ID__c != ''){
            objType = trigger.new[0].Manning_Agent_or_Debit_Note_ID__c.substring(0,3);
            if(objType == 'a2l'){
                PEME_Manning_Agent__c manningAgent = new PEME_Manning_Agent__c();
                manningAgent = [SELECT Id,Client__c FROM PEME_Manning_Agent__c WHERE Id =: Trigger.new[0].Manning_Agent_or_Debit_Note_ID__c];
                manningAgent.Client__c = Trigger.new[0].Id;
                try{
                    update manningAgent ;
                }
                catch(exception e){system.debug('Cannot update-->'+e.getMessage());}
            }
            else if(objType == 'a2g'){
                PEME_Debit_note_detail__c debitNote = new PEME_Debit_note_detail__c();
                debitNote = [SELECT Id,Company_Name__c FROM PEME_Debit_note_detail__c WHERE Id =: Trigger.new[0].Manning_Agent_or_Debit_Note_ID__c];
                debitNote.Company_Name__c = Trigger.new[0].Id;
                try{
                    update debitNote ;
                }
                catch(exception e){system.debug('Cannot update-->'+e.getMessage());}
            }
        }
    } 
    //end of SF-272
    
    //GUID Generation - start
     if(Trigger.isInsert && Trigger.isBefore)
     {        
         for(Account aObject: Trigger.New)
         {            
             Blob b = Crypto.GenerateAESKey(128);            
             String h = EncodingUtil.ConvertTohex(b);            
             String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20); 
             aObject.guid__c = guid;        
         }    
     }
     //GUID Generation end
    
    // added for SF- 3867
    /* Set<String> account_role_set = new Set<String>();
    account_role_set.add('Broker');
    account_role_set.add('Client');
    account_role_set.add('External Service Provider');
    account_role_set.add('Correspondent'); */
    
  /*  String account_role = '';
    String[] account_role_kyc_arr; 
    Integer account_role_kyc_size;
    String account_role_kyc_str = '';
    Integer count;
    Boolean isOtherRole;
    String accountBvDID = '';
    String accountKycExceptionDmsRef = '';
    
    
    //if((Trigger.isInsert || Trigger.isUpdate ) && Trigger.isBefore) //commented out aritra so that the code fires only for Update
    
    if(Trigger.isUpdate && Trigger.isBefore) //added aritra
    {
        for(Account aObject: Trigger.new)
        
        if(aObject.Company_Status__c == 'Active'){    //added aritra
        
            {
                count = 0;
                isOtherRole = false;
                accountBvDID = aObject.BvD_ID__c;
                accountKycExceptionDmsRef = aObject.KYC_exception_DMS_ref__c;
                
                /* commented out aritra so that it does not fire for insert
                if(Trigger.isInsert)
                {
                    System.debug('########### from Trigger.isInsert ##############');
                    account_role = aObject.Unapproved_Company_Roles__c;
                    System.debug('########### from Trigger.isInsert account_role  ##############'+account_role );
                } 
                */
                
            /*    if(Trigger.isUpdate && aObject.Company_Role__c !=null)
                {
                    System.debug('########### from Trigger.isUpdate ##############');
                    account_role = aObject.Company_Role__c;
                    System.debug('########### from Trigger.isUpdate account_role ##############'+account_role );
                }
                if(account_role != null && account_role !='')
                {
                   
                    System.debug('######### account_role ##############'+account_role);
                    // for single role , append ';'
                    if(!account_role.contains(';'))
                    {
                        account_role = account_role + ';';
                        System.debug('######### single account_role after append ;##############'+account_role);
                    }
                    if(account_role.contains(';'))
                    {
                        account_role_kyc_arr = new String[]{account_role};
                        account_role_kyc_arr = account_role.split(';');
                        System.debug('######### account_role_kyc_arr ##############'+account_role_kyc_arr);
                        account_role_kyc_size = account_role_kyc_arr.size();
                        System.debug('######### account_role_kyc_size ##############'+account_role_kyc_size);
                    }
                    if(account_role_kyc_size > 0)
                    {
                        for(String str : account_role_kyc_arr)
                        {
                           // account_role_kyc_str = account_role_kyc_str + ' ' +str;
                            System.debug('#################### str #########################'+str);
                            if(str.contains('Other') || str.contains('Investments') || str.contains('Insurance Company') || str.contains('Bank'))
                            {
                                isOtherRole = true;
                            }
                            else
                            {
                                isOtherRole = false;
                                break;
                            }
                        }
                        System.debug('#################### isOtherRole #########################'+isOtherRole); 
                    }
                    
                    if(isOtherRole)
                    {
                        System.debug('########### from isOtherRole if ##############');
                        if(accountBvDID == null && accountKycExceptionDmsRef == null)
                        {
                            System.debug('########### from accountKycExceptionDmsRef if ##############');
                            accountKycExceptionDmsRef = 'ONE';
                            aObject.KYC_exception_DMS_ref__c = accountKycExceptionDmsRef;
                            System.debug('########### accountKycExceptionDmsRef updated #############');
                            System.debug('########### updated accountKycExceptionDmsRef #############'+accountKycExceptionDmsRef);
                            System.debug('########### updated aObject.KYC_exception_DMS_ref__c #############'+aObject.KYC_exception_DMS_ref__c);
                        }
                        /* if((Trigger.isUpdate) && (aObject.KYC_exception_DMS_ref__c  != null && aObject.KYC_exception_DMS_ref__c  != 'ONE'))
                        {
                            System.debug('########## accountKycExceptionDmsRef == other value #################'+aObject.KYC_exception_DMS_ref__c);
                            aObject.addError('account Kyc Exception DmsRef cannot be added');
                           
                        } */
                  /*  } 
                    else
                    {
                        System.debug('########### from isOtherRole else ##############'+isOtherRole);
                        if((Trigger.isUpdate) && (aObject.KYC_exception_DMS_ref__c  != null && aObject.KYC_exception_DMS_ref__c  == 'ONE'))
                        {
                            aObject.KYC_exception_DMS_ref__c = '';
                            System.debug('########### kyc not required :::::::aObject.KYC_exception_DMS_ref__c #############'+aObject.KYC_exception_DMS_ref__c);
                        }
                    }
                }
            }
       }//added aritra
    }   
    // end of SF-3867
        //SF - 4081, SF - 4226
    
    //Before update set the flag create_marine_client__c to TRUE if the role is broker and membership status is Approved
    if(Trigger.Isbefore && Trigger.IsUpdate && Trigger.new[0].Membership_Status__c == 'Approved' && Trigger.new[0].Role_Broker__c == TRUE)
        Trigger.new[0].create_marine_client__c = True;
        
    //added by Aritra
    
     // ########### SF-4037 starts ################
    List<String> tmpListNew_str_arr = new List<String>();
    List<String> account_role_arr = new List<String>();
    Boolean broker_Onrisk = false;
    Boolean client_Onrisk = false;
    Boolean isFollowerList_configured = false;
    List<AccountFollwer__c> follower_list_availablity = AccountFollwer__c.getall().values();
    if(follower_list_availablity != null && follower_list_availablity.size() >0) //Added by Arindam 
    {
    isFollowerList_configured = follower_list_availablity[0].check_account_follwer__c;
    }
    system.debug('############ isFollowerList_configured #######'+isFollowerList_configured);
    if(Trigger.isUpdate && Trigger.isAfter && isFollowerList_configured == true){
  
        Set<ID> ids = Trigger.newMap.keySet();
        System.debug(' ::::::::::: ids :::::::::::::::::::::'+ids);
        List<EntitySubscription> old_followEntityList =  new List<EntitySubscription>();
        old_followEntityList = [SELECT SubscriberId FROM EntitySubscription where ParentId = :ids];
        List <Account> account_followerList = new List<Account>();
        account_followerList = [SELECT Underwriter_main_contact__c,Key_Claims_Contact__c,Claim_handler_Defence__c,Claim_handler_Cargo_Dry__c,Claim_handler_Cargo_Liquid__c,Claim_handler_CEP__c,Claim_handler_Crew__c,Claim_handler_Energy__c,Claim_handler_Marine__c,Claims_handler_Builders_Risk__c,Claim_Adjuster_Marine_lk__c,Claim_handler_Cargo_Dry_2_lk__c,Claim_handler_Cargo_Liquid_2_lk__c,Claim_handler_CEP_2_lk__c,Claim_handler_Crew_2_lk__c,Claim_handler_Defence_2_lk__c,Claim_handler_Energy_2_lk__c,Claim_handler_Marine_2_lk__c,Claims_handler_Builders_Risk_2_lk__c FROM Account where id = :ids];

        System.debug(' ::::::::::: account_followerList :::::::::::::::::::::'+account_followerList);
        System.debug(' ::::::::::: old_followEntityList :::::::::::::::::::::'+old_followEntityList);
        System.debug(' ::::::::::: old_followEntityList :::::::::::::::::::::'+old_followEntityList.size());
        List<EntitySubscription> updated_old_followEntityList =  new List<EntitySubscription>();
      
        Boolean isfound;
        List<EntitySubscription> deleted_followEntityList =  new List<EntitySubscription>();
        Boolean isOnRisk = false;
        for(Account aObject: Trigger.new)
        {    
            if(aObject.Company_Status__c == 'Active')
            {   
                // ########### checking company role and onrisk section starts ###########
                broker_Onrisk = aObject.Broker_On_Risk_Flag__c;
                client_Onrisk = aObject.Client_On_Risk_Flag__c;
                if(String.isNotBlank(aObject.Company_Role__c) && String.isNotEmpty(aObject.Company_Role__c))
                    {
                        account_role_arr.add(aObject.Company_Role__c);  
                        
                    }
                    System.debug('######### broker onrisk ###########'+aObject.Broker_On_Risk_Flag__c);
                    System.debug('########### account_role_arr ##############'+account_role_arr);
                    System.debug('########### account_role_arr size ##############'+account_role_arr.size());
                    if(account_role_arr.size() > 0){
                        for(String compRole : account_role_arr){
                            if((compRole.contains('Broker') && broker_Onrisk == true) || (compRole.contains('Client') && client_Onrisk == true))
                            {
                                isOnRisk = true;
                                break;
                            }
                            else
                            {
                                isOnRisk = false;   
                            }
                            
                        }
                    }
                    system.debug(' ############## isOnRisk ############'+isOnRisk);
            // ########### company role and onrisk checking section ends #############
            // ########### null/empty checking of all fieds and making string list section starts ##############
                    for(Integer i=0;i<account_followerList.size();i++)
                    {
                        if(String.isNotBlank(account_followerList[i].Underwriter_main_contact__c) && String.isNotEmpty(account_followerList[i].Underwriter_main_contact__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Underwriter_main_contact__c);                                                                        
                        }
                        if(String.isNotBlank(account_followerList[i].Claim_handler_Defence__c) && String.isNotEmpty(account_followerList[i].Claim_handler_Defence__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claim_handler_Defence__c);
                        } 
                        if(String.isNotBlank(account_followerList[i].Claim_handler_Cargo_Dry__c) && String.isNotEmpty(account_followerList[i].Claim_handler_Cargo_Dry__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claim_handler_Cargo_Dry__c);
                        }
                        if(String.isNotBlank(account_followerList[i].Claim_handler_Cargo_Liquid__c) && String.isNotEmpty(account_followerList[i].Claim_handler_Cargo_Liquid__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claim_handler_Cargo_Liquid__c);
                        }
                        if(String.isNotBlank(account_followerList[i].Claim_handler_CEP__c) && String.isNotEmpty(account_followerList[i].Claim_handler_CEP__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claim_handler_CEP__c);
                        }
                        if(String.isNotBlank(account_followerList[i].Claim_handler_Crew__c) && String.isNotEmpty(account_followerList[i].Claim_handler_Crew__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claim_handler_Crew__c);
                        }
                        if(String.isNotBlank(account_followerList[i].Claim_handler_Energy__c) && String.isNotEmpty(account_followerList[i].Claim_handler_Energy__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claim_handler_Energy__c);
                        }
                        if(String.isNotBlank(account_followerList[i].Claim_handler_Marine__c) && String.isNotEmpty(account_followerList[i].Claim_handler_Marine__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claim_handler_Marine__c);
                        }
                        if(String.isNotBlank(account_followerList[i].Claims_handler_Builders_Risk__c) && String.isNotEmpty(account_followerList[i].Claims_handler_Builders_Risk__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claims_handler_Builders_Risk__c);
                        }
                        if(String.isNotBlank(account_followerList[i].Claim_Adjuster_Marine_lk__c) && String.isNotEmpty(account_followerList[i].Claim_Adjuster_Marine_lk__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claim_Adjuster_Marine_lk__c);
                        }
                        if(String.isNotBlank(account_followerList[i].Claim_handler_Cargo_Dry_2_lk__c) && String.isNotEmpty(account_followerList[i].Claim_handler_Cargo_Dry_2_lk__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claim_handler_Cargo_Dry_2_lk__c);
                        }
                        if(String.isNotBlank(account_followerList[i].Claim_handler_Cargo_Liquid_2_lk__c) && String.isNotEmpty(account_followerList[i].Claim_handler_Cargo_Liquid_2_lk__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claim_handler_Cargo_Liquid_2_lk__c);
                        }
                        if(String.isNotBlank(account_followerList[i].Claim_handler_CEP_2_lk__c) && String.isNotEmpty(account_followerList[i].Claim_handler_CEP_2_lk__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claim_handler_CEP_2_lk__c);
                        }
                        if(String.isNotBlank(account_followerList[i].Claim_handler_Crew_2_lk__c) && String.isNotEmpty(account_followerList[i].Claim_handler_Crew_2_lk__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claim_handler_Crew_2_lk__c);
                        }
                        if(String.isNotBlank(account_followerList[i].Claim_handler_Defence_2_lk__c) && String.isNotEmpty(account_followerList[i].Claim_handler_Defence_2_lk__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claim_handler_Defence_2_lk__c);
                        }
                        if(String.isNotBlank(account_followerList[i].Claim_handler_Energy_2_lk__c) && String.isNotEmpty(account_followerList[i].Claim_handler_Energy_2_lk__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claim_handler_Energy_2_lk__c);
                        }
                        if(String.isNotBlank(account_followerList[i].Claim_handler_Marine_2_lk__c) && String.isNotEmpty(account_followerList[i].Claim_handler_Marine_2_lk__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claim_handler_Marine_2_lk__c);
                        }
                        if(String.isNotBlank(account_followerList[i].Claims_handler_Builders_Risk_2_lk__c) && String.isNotEmpty(account_followerList[i].Claims_handler_Builders_Risk_2_lk__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Claims_handler_Builders_Risk_2_lk__c);
                        }
                        if(String.isNotBlank(account_followerList[i].Key_Claims_Contact__c) && String.isNotEmpty(account_followerList[i].Key_Claims_Contact__c))
                        {
                            tmpListNew_str_arr.add(account_followerList[i].Key_Claims_Contact__c);
                        }
                    }
                    System.debug(' ::::::::::: tmpListNew_str_arr :::::::::::::::::::::'+tmpListNew_str_arr);
                    System.debug(' ::::::::::: tmpListNew_str_arr.size() :::::::::::::::::::::'+tmpListNew_str_arr.size());
                    isfound = false;
                    Set<String> setString = new Set<String>(tmpListNew_str_arr);
                    System.debug(' ::::::::::: setString :::::::::::::::::::::'+setString.size());
                    List<String> listStrings = new List<String>(setString);
                    System.debug(' ::::::::::: listStrings :::::::::::::::::::::'+listStrings.size());
                    System.debug(' ::::::::::: listStrings :::::::::::::::::::::'+listStrings);
                // ########### null/empty checking of all fieds and making string list section ends ##############   
                    //############ insert new follers sections strats ############## 
                if(isOnRisk)
                {
                    if(listStrings.size() > 0)
                    {  
                        updated_old_followEntityList.clear();
                        for(String str : listStrings)
                        {
                            isfound = false;
                            for(EntitySubscription entity : old_followEntityList)
                            {
                                system.debug('updated_old_followEntityList size ##############'+updated_old_followEntityList);
                                system.debug('updated_old_followEntityList size ##############'+updated_old_followEntityList.size());
                                String str_entity = entity.SubscriberId;
                                if(str_entity.contains(str))
                                {
                                    isfound = true;        
                                } 
                        
                            }
                            If(isfound == false)
                            {
                                EntitySubscription updatedEntity = new EntitySubscription();
                                System.debug('new follower is :::::::'+str);              
                                updatedEntity.SubscriberId = str;
                                updatedEntity.ParentId = aObject.id;
                                updated_old_followEntityList.add(updatedEntity);
                            }               
                        }   
                    }
                }
                else{
                    System.debug('######## other than cleint broker or onrisk is false ############');
                }
                // ############ insert new follers sections ends ############## 
                
                // ############delete unfollwer sections strats ##############
                if(isOnRisk)
                {
                    if(old_followEntityList.size() > 0){
                        
                        for(EntitySubscription old_subscriberId : old_followEntityList)
                        {
                            if(!listStrings.contains(old_subscriberId.SubscriberId))
                            {
                                deleted_followEntityList.add(old_subscriberId);
                            }
                        }
                    }
                }
                else{
                    System.debug('######## other than cleint broker or onrisk is false ############');
                }
                // ############delete unfollwer sections ends ############## 
            }    
        }
        if(updated_old_followEntityList != null && updated_old_followEntityList.size() > 0 && isOnRisk == true){
            insert updated_old_followEntityList;
        }
        system.debug('deleted_followEntityList ##################'+deleted_followEntityList);
        if(deleted_followEntityList != null && deleted_followEntityList.size() > 0 && isOnRisk == true){
            delete deleted_followEntityList;
        }
    }
     // ########### SF-4037 ends ##################
      //added SF-3949 (owner update for inactive companies)
     if(trigger.IsBefore && Trigger.IsUpdate && trigger.old[0].Company_Role__c != null){
         if(trigger.new[0].Company_Status__c == 'Inactive' && trigger.old[0].Company_Role__c.contains('Client') && !(trigger.old[0].Company_Role__c.contains('Correspondent'))){
             trigger.new[0].ownerId = trigger.new[0].Underwriter_main_contact__c;
         }
     }
     //SF-3949 - Ends.*/
}