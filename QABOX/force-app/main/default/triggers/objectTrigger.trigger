trigger objectTrigger on Object__c (before insert) {
   //Updating the GUID
    if(Trigger.isInsert && Trigger.isBefore){
        for(Object__c aObject: Trigger.New){
            Blob b = Crypto.GenerateAESKey(128);
            String h = EncodingUtil.ConvertTohex(b);
            String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20);
            System.Debug('*****guid '+guid);
            aObject.guid__c = guid;
        }
    }
}