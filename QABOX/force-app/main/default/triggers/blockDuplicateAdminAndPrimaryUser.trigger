trigger blockDuplicateAdminAndPrimaryUser on Contact(before update, after update){
        
   if(Trigger.isAfter){
    if(!Gard_RecursiveBlocker.blocker){  
        
        List<Contact> toBeUpdatedContactList = new List<Contact>();
        //to ensure at least one contact will be primary contact
        List<Contact> primeContactList = new List<Contact>([SELECT id,primary_contact__c  from Contact where
                                                primary_contact__c = true AND
                                                accountId = :Trigger.new[0].accountId AND                                          
                                                id != :Trigger.new[0].id]);
                                                
        if(Trigger.old[0].primary_contact__c == false && Trigger.new[0].primary_contact__c == true && primeContactList.size() > 0){
                 for(Contact c : primeContactList){
                     c.primary_contact__c = false;                 
                 }
                 toBeUpdatedContactList.addAll(primeContactList); 
            }
            
        if(toBeUpdatedContactList.size() > 0 )
        {
            update toBeUpdatedContactList;                  
        } 
        
        /*  Now these things are maintained in Acc-Contact junction object only        
        
        List<Contact> adminContactList = new List<Contact>([SELECT id,name,administrator__c,primary_contact__c  from Contact where
                                                administrator__c = true AND
                                                accountId = :Trigger.new[0].accountId AND                                          
                                                id != :Trigger.new[0].id]);
                                                
                                                //Error block to ensure at least one contact must be admin and 
        List<Contact> primeContactList = new List<Contact>([SELECT id,administrator__c,primary_contact__c  from Contact where
                                                primary_contact__c = true AND
                                                accountId = :Trigger.new[0].accountId AND                                          
                                                id != :Trigger.new[0].id]);
        
        // New admin contact list used for MYG2A-80
        List<Contact> adminContactPeopleClaimAccess = new List<Contact>([SELECT id,name,administrator__c,primary_contact__c  from Contact where                                                                                                                     
                                                                            id = :Trigger.new[0].id and 
                                                                            administrator__c = true]);
                                                
        List<Contact> toBeUpdatedContactList = new List<Contact>();
        Set<Id> removeAdmin_ContactIdSet = new Set<Id>();
        Set<Id> makeAdmin_ContactIdSet = new Set<Id>();
        
        if(Trigger.old[0].administrator__c == false && Trigger.new[0].administrator__c == true && adminContactList.size() > 0){
             
             for(Contact c : adminContactList){
                 c.administrator__c = false;
                 removeAdmin_ContactIdSet.add(c.id); // for account contact junction object update
             }
             toBeUpdatedContactList.addAll(adminContactList); 
        }
        if(Trigger.old[0].primary_contact__c == false && Trigger.new[0].primary_contact__c == true && primeContactList.size() > 0){
             for(Contact c : primeContactList){
                 c.primary_contact__c = false;                 
             }
             toBeUpdatedContactList.addAll(primeContactList); 
        }
        
        // MYG2A-80: make admin contact People Claim User by default.
            if(Trigger.old[0].administrator__c == false && Trigger.new[0].administrator__c == true && adminContactPeopleClaimAccess != null && adminContactPeopleClaimAccess.size()>0 && adminContactPeopleClaimAccess[0].administrator__c == true)
            {
                adminContactPeopleClaimAccess[0].IsPeopleLineUser__c = true;                
                toBeUpdatedContactList.add(adminContactPeopleClaimAccess[0]);
                makeAdmin_ContactIdSet.add(adminContactPeopleClaimAccess[0].id); // for account contact junction object update                
            }
        
        system.debug('**********toBeUpdatedContactList '+toBeUpdatedContactList);
        //Update the list
                
        if(toBeUpdatedContactList.size() > 0 )
        {
            update toBeUpdatedContactList;                  
        } 
        */
        
        
        Gard_RecursiveBlocker.blocker = true;
        //Synchronizing Users
        //MYG-1336
        if(Trigger.isBefore){
         if(Trigger.New[0].email != Trigger.old[0].email && Trigger.New[0].email != '' && Trigger.New[0].email != null){
            if(GardUtils.inUpdateUserFirst)
                GardUtils.UpdateUser(Trigger.New[0].id, Trigger.New[0].email);
         }
        }
                
        //Used for Account Contact Mapping Juction Object PLEASE DONT DELETE IT. WILL LEAD TO ISSUE NO myg 2278 AGAIN    
        GardUtils.updateAccountContactMappingForNoLongerEmployed(Trigger.newMap, Trigger.OldMap);//Synchronizing Users  
        //MYGP-134 --> No automation to junction object on account change in contact      
        //GardUtils.updateAccountContactMappingForAccountChange(Trigger.newMap, Trigger.OldMap);
     }
    }
    
}