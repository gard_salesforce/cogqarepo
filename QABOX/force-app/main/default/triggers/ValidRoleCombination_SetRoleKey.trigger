trigger ValidRoleCombination_SetRoleKey on Valid_Role_Combination__c (before Insert, before update) {
    for (Valid_Role_Combination__c vr : trigger.new) {
        vr.role_key__c = vr.role__c + '_' + vr.sub_role__c;
    }
}