trigger CaseCommentTrigger on CaseComment(after insert){
	List<CaseComment> caseComments = new List<CaseComment>();
    List<Id> parentCaseIds = new List<Id>();
    for(CaseComment caseComm : trigger.new){
        if(caseComm.parentId.getSObjectType() == Schema.Case.SObjectType){
            caseComments.add(caseComm);
            parentCaseIds.add(caseComm.parentId);
        }
    }
    
    if(!parentCaseIds.isEmpty()) CaseCommentTriggerHelper.processCaseComments(parentCaseIds);//SF-5466
}