trigger triggerPEMEExams on PEME_Exam_Detail__c (after insert,after update,before update) {
//Added as part of ticket SF-1917   
    if(trigger.isafter && trigger.isUpdate){
            try{
                List<Approval.ProcessSubmitRequest> requestList = new List<Approval.ProcessSubmitRequest>();
                 for(PEME_Exam_Detail__c exam:trigger.new){
                     if(exam.status__c.equalsIgnoreCase('Under Approval') /*&& trigger.oldMap.get(exam.id).status__c.equalsIgnoreCase('Draft')*/ ){
                            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                            req.setProcessDefinitionNameOrId('PEMExaminationApproval');
                            req.setObjectId(exam.Id);
                            if(trigger.oldMap.get(exam.id).status__c.equalsIgnoreCase('Rejected-Need Input')){
                                 req.setComments(exam.Comments__c);
                            }
                            requestList.add(req);
                            //Approval.ProcessResult result = Approval.process(req);
                     }
                 }
                 List<Approval.ProcessResult> result = Approval.process(requestList);  //Added for SF-4514
            }catch(exception ex){
                
            }
        }
      //Code ends for ticket SF-1917 
      
   // set<String> setInvoiceIds = new set<String>();
  //  set<String> setInvoiceIdsForAmt = new set<String>();
  //  set<String> setInvoiceIdsForDOFV= new set<String>();
  //  set<String> setInvoiceIdsForBlankDOFV= new set<String>();
 //   List<PEME_Invoice__c > invoicesToUpdate = new List<PEME_Invoice__c >();
    Map<Id, PEME_Exam_Detail__c> rejectedExams = new Map<Id, PEME_Exam_Detail__c>{};
   
 //   Integer flag=0;
    for(PEME_Exam_Detail__c objNew:trigger.new){
       /* System.debug('objNew.Status__c---'+objNew.Status__c);
        System.debug('Trigger.oldmap.get(objNew.Id).Status__c---------------'+Trigger.oldmap.get(objNew.Id).Status__c);
        System.debug('Trigger.isAfter-----'+Trigger.isAfter);
        System.debug('Result--' + (Trigger.isAfter && Trigger.isupdate && (objNew.Status__c != Trigger.oldmap.get(objNew.Id).Status__c)));
        if((Trigger.isAfter && Trigger.isupdate && (objNew.Status__c != Trigger.oldmap.get(objNew.Id).Status__c))||Trigger.isinsert){
            System.debug('setInvoiceIds22222222---------------'+setInvoiceIds);
            setInvoiceIds.add(objNew.Invoice__c);
        }
        if(Trigger.isAfter && Trigger.isupdate && (objNew.Status__c!=Trigger.oldmap.get(objNew.Id).Status__c) && objNew.Status__c=='Approved'){
            setInvoiceIdsForAmt.add(objNew.Invoice__c);
        }
        if(Trigger.isAfter && Trigger.isupdate && (objNew.Status__c!=Trigger.oldmap.get(objNew.Id).Status__c) && ((objNew.Status__c=='Approved')/*||(objNew.Status__c=='Rejected-Need Input')*//*))/{
            setInvoiceIdsForDOFV.add(objNew.Invoice__c);
        }
        if(Trigger.isAfter && Trigger.isupdate && (objNew.Status__c!=Trigger.oldmap.get(objNew.Id).Status__c) && ((objNew.Status__c=='Rejected-Need Input')/*||(objNew.Status__c=='Rejected-Need Input')*//*)){
            setInvoiceIdsForBlankDOFV.add(objNew.Invoice__c);
        }*/
        if(Trigger.isBefore && Trigger.isupdate){
            if (Trigger.oldmap.get(objNew.Id).Status__c != 'Rejected-Closed' && objNew.Status__c == 'Rejected-Need Input'){
                rejectedExams.put(objNew.Id,objNew);    
            }   
        }
    }
 /*   System.debug('setInvoiceIds---------------'+setInvoiceIds);
    if(setInvoiceIds.size()>0)
    {
        flag = 1;
        PEMEExamApproveRejectCtrl.updateTrigger(flag,setInvoiceIds);
    }
    if(setInvoiceIdsForAmt.size()>0)
    {
        flag = 2;
        PEMEExamApproveRejectCtrl.updateTrigger(flag,setInvoiceIdsForAmt);
    }
    if(setInvoiceIdsForDOFV.size()>0)
    {
        flag = 3;
        PEMEExamApproveRejectCtrl.updateTrigger(flag,setInvoiceIdsForDOFV);
    }
    if(setInvoiceIdsForBlankDOFV.size()>0)
    {
        flag = 4;
        PEMEExamApproveRejectCtrl.updateTrigger(flag,setInvoiceIdsForBlankDOFV);
    }*/
   /* if(setInvoiceIds.size()>0){
        Boolean isPending=false;
        Boolean isRejected=false;
        Boolean isApproved=false;
        String invoiceStatus = '';
        for(PEME_Exam_Detail__c objExam:[select Id,Name,Status__c,Invoice__c from PEME_Exam_Detail__c where Invoice__c IN:setInvoiceIds]){
            if(objExam.Status__c=='Under Approval'){
                isPending=true;
            }else if(objExam.Status__c=='Rejected-Need Input' || objExam.Status__c=='Rejected-Closed'){
                isRejected=true;
            }else if(objExam.Status__c=='Approved'){
                isApproved=true;
            }
        }
        if(isRejected && !isPending && !isApproved){
            invoiceStatus = 'Rejected';    
        }
        if(!isRejected && !isPending && isApproved){
            invoiceStatus = 'Approved';    
        }
        if(!isRejected && isPending && !isApproved){
            invoiceStatus = 'Under Approval';    
        }
        if(isApproved && isPending && !isRejected){
            invoiceStatus = 'Partially Approved';    
        }
        if(isRejected && isPending && !isApproved){
            invoiceStatus = 'Partially Approved';    
        }
        if(isRejected && isApproved && !isPending){
            invoiceStatus = 'Partially Approved';    
        }
        if(isRejected && isApproved && isPending){
            invoiceStatus = 'Partially Approved';    
        }
        if(!isRejected && !isPending && !isApproved){
            invoiceStatus = 'Draft';    
        }
        PEME_Invoice__c objInvoice = [select Id,Name,Status__c from PEME_Invoice__c where Id IN:setInvoiceIds][0];
        objInvoice.Status__c = invoiceStatus;
        update objInvoice;
    }
    if(setInvoiceIdsForAmt.size()>0){
        Decimal totalAmt = 0;
        for(PEME_Exam_Detail__c objExam:[select Id,Name,Status__c,Amount__c,Invoice__c from PEME_Exam_Detail__c where Invoice__c IN:setInvoiceIdsForAmt AND Status__c='Approved']){
            totalAmt = totalAmt + objExam.Amount__c;
        }     
        PEME_Invoice__c objInvoice = [select Id,Name,Status__c,Total_Amount__c from PEME_Invoice__c where Id IN:setInvoiceIdsForAmt][0];
        objInvoice.Total_Amount__c= totalAmt;
        update objInvoice;
    }
    if(setInvoiceIdsForDOFV.size()>0){
      //  List<PEME_Invoice__c > invoicesToUpdate = new List<PEME_Invoice__c >();
        for(PEME_Invoice__c objInvoice:[select Id,Name,Date_of_Final_Verification__c from PEME_Invoice__c where Id IN:setInvoiceIdsForDOFV]){
            objInvoice.Date_of_Final_Verification__c = Date.valueOf(Datetime.now());
            invoicesToUpdate.add(objInvoice);
        }
      //  update invoicesToUpdate;
    }
       if(setInvoiceIdsForBlankDOFV.size()>0){
          //  List<PEME_Invoice__c > invoicesToUpdate = new List<PEME_Invoice__c >();
            for(PEME_Invoice__c objInvoice:[select Id,Name,Date_of_Final_Verification__c from PEME_Invoice__c where Id IN:setInvoiceIdsForBlankDOFV]){
            objInvoice.Date_of_Final_Verification__c = null;
            invoicesToUpdate.add(objInvoice);
            
        } 
    }         
    update invoicesToUpdate; */
    if (!rejectedExams.isEmpty()){    
           
        List<Id> processInstanceIds = new List<Id>{};
        try{
            for (PEME_Exam_Detail__c exams : [SELECT (SELECT ID
                                                  FROM ProcessInstances
                                                  ORDER BY CreatedDate DESC
                                                  LIMIT 1)
                                          FROM PEME_Exam_Detail__c
                                          WHERE ID IN :rejectedExams.keySet()]){
                processInstanceIds.add(exams.ProcessInstances[0].Id);
            }
        }catch(Exception e){}
       /* for (ProcessInstanceStep piStep : [SELECT Comments,Id,ProcessInstanceId,StepNodeId FROM ProcessInstanceStep where ProcessInstanceId in:processInstanceIds and StepStatus='Rejected']){
         If(piStep.Comments == null || piStep.Comments.trim().length() == 0) {
             piStep.addError('Operation Cancelled: Please provide a rejection reason!');
             return;
         }
        }*/
        try{
            for (ProcessInstance pi : [SELECT TargetObjectId,
                                           (SELECT Id, StepStatus, Comments 
                                            FROM Steps
                                            ORDER BY CreatedDate DESC
                                            LIMIT 1 )
                                       FROM ProcessInstance
                                       WHERE Id IN :processInstanceIds
                                       ORDER BY CreatedDate DESC]){                   
                 if((pi.Steps[0].Comments == null || pi.Steps[0].Comments.trim().length() == 0)){
                    rejectedExams.get(pi.TargetObjectId).addError('Operation Cancelled: Please provide a rejection reason!');
                    //pi.Steps[0].addError('Operation Cancelled: Please provide a rejection reason!');
                 }
            }
        }catch(Exception e){}
   }
   // Send mail when any record gets rejected --> MYG-2840
   if(Trigger.isUpdate && (Trigger.isAfter || Trigger.isBefore))
   {
    /*   PEME_Exam_Detail__c objExam = new PEME_Exam_Detail__c();
       boolean hasRejected = false;
       List<PEME_Exam_Detail__c> examRecord = new List<PEME_Exam_Detail__c>();
       examRecord =[SELECT Id,Status__c FROM PEME_Exam_Detail__c where Invoice__c=:Trigger.New[0].Invoice__c and status__c='Rejected-Need Input'];
       if(examRecord != null && examRecord.size()>0)
       {
           hasRejected = true;
       }
       system.debug('hasRejected-------->'+hasRejected);
       if(Trigger.New[0].status__c == 'Rejected-Need Input' && !(hasRejected))
       {
           GardUtils.sendPEMERejectMail(Trigger.New[0].Invoice__c, 'MyGard-PEME examination details rejection - Notification to clinics');
       }
      /* if(Trigger.isBefore)
       {
       List<Id> processInstanceIds = new List<Id>{};
       List<PEME_Exam_Detail__c> ex1 = new List<PEME_Exam_Detail__c>();
       //ex1 = [SELECT (SELECT ID FROM ProcessInstances ORDER BY CreatedDate DESC LIMIT 1) FROM PEME_Exam_Detail__c where id =: Trigger.New[0].id];
       //if(ex1.size()>0)
      // {
        for (PEME_Exam_Detail__c exams : [SELECT (SELECT ID FROM ProcessInstances ORDER BY CreatedDate DESC LIMIT 1) FROM PEME_Exam_Detail__c where id =: Trigger.New[0].id]){
              if(exams.ProcessInstances != null && exams.ProcessInstances.size() >0)
              {
                system.debug('exams--------->'+exams);
                processInstanceIds.add(exams.ProcessInstances[0].Id);
              }
            }  
        //}  
        system.debug('processInstanceIds---->'+processInstanceIds);  
        if(processInstanceIds!=null)
        {
             for (ProcessInstance pi : [SELECT TargetObjectId,
                                           (SELECT Id, StepStatus, Comments FROM Steps where StepStatus in('Rejected','Approved')
                                            ORDER BY CreatedDate DESC
                                            LIMIT 1 )
                                       FROM ProcessInstance
                                       WHERE Id IN :processInstanceIds
                                       ORDER BY CreatedDate DESC])
                                       {
                                             objExam = [select Id,Name,Status__c,Invoice__c,GARD_Comments__c from PEME_Exam_Detail__c where id =: Trigger.New[0].id];
                                             trigger.new[0].GARD_Comments__c = pi.Steps[0].Comments;
                                       }
                  }//end update
      }*/

   }
   if(trigger.isbefore && trigger.isInsert)
    {
        GardUtils.updateGUIDExam(Trigger.new);        
    }
    if(trigger.isafter && trigger.isUpdate)
    {
        if(Trigger.New[0].status__c == 'Rejected-Closed')
       {
           GardUtils.sendPEMEDeleteMail(Trigger.New[0].Invoice__c, 'PEME-Send mail to PEME HK when clinic deletes an invoice line');
       }
    }
}