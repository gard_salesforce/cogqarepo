trigger setOpptyApprovers on Opportunity (before insert,before update) {
    
    //Start SF-3894 : Set SVP to Responsible CUO
    Set<Id> accountIdSet = new Set<Id>();
    List<Opportunity> opportunityListSVP = new List<Opportunity>();
    //List<Opportunity> opportunityListCountry = new List<Opportunity>();
    Map<Id,Id> accountIdAccountResponsibleCUOMap = new Map<Id,Id>();
    //Map<Id,Id> accountIdAccountCountryMap = new Map<Id,Id>();
    
    for (Opportunity opp: Trigger.new) {
        if(trigger.IsInsert || trigger.IsUpdate) {
            if( (opp.Senior_Approver__c == null)) {
                opportunityListSVP.add(opp);        
                accountIdSet.add(opp.AccountId);          
            }
            //SF4398
            /*if ((opp.Country_v2__c == null)) { 
                opportunityListCountry.add(opp);        
                accountIdSet.add(opp.AccountId);
            }*/         
        } 
    }
    
    if (accountIdSet.size() > 0 && accountIdSet != null) {
        for(Account acc : [Select id,Responsible_CUO__c,Country__c From Account Where id in : accountIdSet]) {
            accountIdAccountResponsibleCUOMap.put(acc.id,acc.Responsible_CUO__c);
            //accountIdAccountCountryMap.put(acc.id,acc.Country__c); //SF-4398
        }    
    }
    
    if(!accountIdAccountResponsibleCUOMap.IsEmpty() && accountIdAccountResponsibleCUOMap.keyset() != null && opportunityListSVP != null) {
        for (Opportunity opp: opportunityListSVP) {
            if(opp.Senior_Approver__c == null && accountIdAccountResponsibleCUOMap.get(opp.AccountId) != null) {
                opp.Senior_Approver__c = accountIdAccountResponsibleCUOMap.get(opp.AccountId);
            }                
        }    
    }
    
    //SF-3998
    /*if(!accountIdAccountCountryMap.IsEmpty() && accountIdAccountCountryMap.keyset() != null && opportunityListCountry != null) {
        for (Opportunity opp: opportunityListCountry) {
            if (opp.Country_v2__c == null && accountIdAccountCountryMap.get(opp.AccountId) != null) {
                opp.Country_v2__c = accountIdAccountCountryMap.get(opp.AccountId);
            }
        }
    } */    
    
    for (Opportunity opp: Trigger.new) {
        if(trigger.IsInsert || trigger.IsUpdate ) {
            //set the area manager IF opportunity area manager not already set
            ID strAreaMgr = opp.Account_Area_Manager__c;
            if (strAreaMgr!=null && opp.Area_Manager__c == null) {
                opp.Area_Manager__c = strAreaMgr;
            }
         }
    }
}