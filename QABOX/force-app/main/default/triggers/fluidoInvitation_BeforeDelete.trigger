trigger fluidoInvitation_BeforeDelete on fluidoconnect__Invitation__c (before delete) {
    Set<Id> surveyIds = new Set<Id>();
    for(fluidoconnect__Invitation__c inv : Trigger.old) surveyIds.add(inv.fluidoconnect__Survey__c);
    
    Map<Id, fluidoconnect__Survey__c> mapSurveys = new Map<Id, fluidoconnect__Survey__c>();
    for(fluidoconnect__Survey__c survey : [SELECT Id, Invitations_Locked__c, fluidoconnect__Event_Owner__c,
                    fluidoconnect__Event_Organiser__c, fluidoconnect__Status__c FROM fluidoconnect__Survey__c 
                    WHERE Id IN :surveyIds]) {
        mapSurveys.put(survey.Id, survey);
    }
    
    Id usrId = Userinfo.getUserId();
    for(fluidoconnect__Invitation__c inv : Trigger.old) {
        fluidoconnect__Survey__c survey = mapSurveys.get(inv.fluidoconnect__Survey__c);
        if(survey == null) continue;

        if(survey.Invitations_Locked__c) { // invitations already sent
            // only event owner or event organiser can delete the event member
            if(survey.fluidoconnect__Event_Owner__c != usrId && survey.fluidoconnect__Event_Organiser__c != usrId) {
                inv.addError('You need to be the Event Owner or Organizer to delete an event member from an event where invitations are already sent!');
            } 
        }
    }
}