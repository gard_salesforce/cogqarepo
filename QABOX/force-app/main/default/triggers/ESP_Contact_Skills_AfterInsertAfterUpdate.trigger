trigger ESP_Contact_Skills_AfterInsertAfterUpdate on ESP_Contact_Skills__c (after insert, after update) {

    List<FeedItem> newPosts = new List<FeedItem>();
    List<Id> contactIds = new List<Id>();
    Map<Id,Contact> contacts;

    //Need to know more information about the parent contact for each skill
    for(ESP_Contact_Skills__c contactSkill : Trigger.New)
    {
        contactIds.add(contactSkill.ESP_Contact_skill__c);
    }
    
    contacts = new Map<Id,Contact>([SELECT Id, FirstName, LastName, OwnerId FROM Contact WHERE Id IN :contactIds]);

    for(ESP_Contact_Skills__c contactSkill : Trigger.New)
    {
        FeedItem contactPost = new FeedItem();
        contactPost.parentId = contactSkill.ESP_Contact_skill__c;//ESP_Contact_skill_c is the lookup to Contact
                
        //Construct feed contactPost 
        if(Trigger.isUpdate)
        {
            //Updating existing skill
            contactPost.Body = 'Contact skill updated for ' + blankIfNull(contacts.get(contactSkill.ESP_Contact_Skill__c).FirstName + ' ') + contacts.get(contactSkill.ESP_Contact_Skill__c).LastName + ':';
            contactPost.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + contacts.get(contactSkill.ESP_Contact_Skill__c).Id;
            
            //Check which fields have been updated
            if(contactSkill.Main_skill__c != Trigger.oldMap.get(contactSkill.Id).Main_skill__c)
            {
                contactPost.Body += '\nPrevious main skill value: ' + blankIfNull(Trigger.oldMap.get(contactSkill.Id).Main_skill__c);
                contactPost.Body += '\nNew main skill value: '      + blankIfNull(contactSkill.Main_skill__c);
            }
            
            if(contactSkill.Specialised_skill__c != Trigger.oldMap.get(contactSkill.Id).Specialised_skill__c)
            {
                contactPost.Body += '\nPrevious specialised skill value: ' + blankIfNull(Trigger.oldMap.get(contactSkill.Id).Specialised_skill__c);
                contactPost.Body += '\nNew specialised skill value: '      + blankIfNull(contactSkill.Specialised_skill__c);
            }
            
            if(contactSkill.Skill_comments__c != Trigger.oldMap.get(contactSkill.Id).Skill_comments__c)
            {
                contactPost.Body += '\nPrevious skill comments: ' + blankIfNull(Trigger.oldMap.get(contactSkill.Id).Skill_comments__c);
                contactPost.Body += '\nNew skill comments: '      + blankIfNull(contactSkill.Skill_comments__c);
            }
            
        }
        else
        {
            //New contact skill
            contactPost.Body = 'New contact skill added for ' + blankIfNull(contacts.get(contactSkill.ESP_Contact_Skill__c).FirstName + ' ') + contacts.get(contactSkill.ESP_Contact_Skill__c).LastName + ':';
            contactPost.Body += blankIfNull(contactSkill.Main_skill__c)!=''        ? '\nMain skill: ' + contactSkill.Main_skill__c : '';
            contactPost.Body += blankIfNull(contactSkill.Specialised_skill__c)!='' ? '\nSpecialised skill: ' + contactSkill.Specialised_skill__c : '';
            contactPost.Body += blankIfNull(contactSkill.Skill_comments__c)!=''    ? '\nSkill comments: ' + contactSkill.Skill_comments__c : '';
        }
        
        FeedItem ownerPost = contactPost.clone();
        ownerPost.ParentId = contacts.get(contactSkill.ESP_Contact_skill__c).OwnerId;
        
        newPosts.add(contactPost);
        newPosts.add(ownerPost);
    }
    
    insert newPosts;

    private string blankIfNull(string inVal) {
        if(inVal==null || inVal.trim()=='null' || inVal.trim()=='')
            return '';
        else
            return inVal;
    }
}