trigger Event_AfterInsertAfterUpdate on Event (after insert, after update, after delete) {
    
    Event e_new, e_old;
    /*
    if(trigger.isInsert || trigger.isUpdate)
    {
         e_new = Trigger.new[0];
    if(!Trigger.isInsert && !Trigger.old.isEmpty()) e_old = Trigger.old[0];
    String UserId = UserInfo.getUserId();   
    }*/
//---- Insert Starts-----
   /* if (trigger.isInsert){
        for(Event e_new:trigger.new){
            if(e_new.WhatId != null && (e_new.Type!= null && e_new.Type.equalsignoreCase('meeting'))){
                //Create task to fill up meeting minutes
                Task t = new Task();
                t.whatId = e_new.WhatId;
                t.meeting_id__c = e_new.id;
                t.meeting_name__c = e_new.subject;
                t.OwnerId = e_new.OwnerId;
                t.Subject = 'Complete Meeting minutes - ' + e_new.subject;
                t.Priority = 'Normal';
                t.ActivityDate = date.newinstance(e_new.EndDateTime.year(), e_new.EndDateTime.month(), e_new.EndDateTime.day()); //SF 3863
                //t.ActivityDate = e_new.EndDateTime; //SF 3863
                t.Status = 'Not Started';
                // added for SF-3853
                t.isComplete__c= true;
                // end of SF-3853
                //if(e_new.Minutes_of_Meeting__c != null && e_new.Minutes_of_Meeting__c != '')
                if(e_new.Description != null && e_new.Description != '') 
                {
                    t.Status = 'Completed';
                } //This is to ensure to take scenario, where the minutes are filled while creating meetings
                insert t;
                
                //publish minutes of meeting in company chatter feed
                //commented for 3837 - Aritra
                //if(e_new.Minutes_of_Meeting__c != null && e_new.Minutes_of_Meeting__c != ''){
               
               //************Added this if condition for 4072.. Date - 19/01/2018************/        
      /*          if(e_new.WhatId != null && (e_new.WhatId.getsObjectType() == Opportunity.sObjectType)){
                    if(e_new.Type != null && e_new.Type.equalsignoreCase('meeting')){
                        FeedItem fitem = new FeedItem(); 
                        //******If, event is linked with an oppty then it will link the chatter post to opportunity's Company******4072.. Date - 19/01/2018
                        Id accId = [select id,AccountId from Opportunity where id=:e_new.WhatId].AccountId;
                        fitem.type = 'LinkPost';
                        fitem.ParentId = accId;
                        fitem.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + e_new.id;
                        fitem.Title = 'View Meeting'; 
                        fitem.Body = 'New Meeting' + ((e_new.Location__c) != null ? ', ' + e_new.Location__c : '') 
                                + '\nStart:' + e_new.StartDateTime.format() + ',  End:' + e_new.EndDateTime.format()
                                + '\nSubject: ' + e_new.Subject;
                                
                        insert fitem;
                    }               
                }
            }
        }
    }
//----insert ends----
//--- Update Starts -----
    if(Trigger.isUpdate){
       
            /* SF-3936-- Starts */
     /*       List<Event> evntlst = new List<Event>();
            set<ID> EventWhatIDS = new set<ID>();
           if(e_new.WhatId != null && e_new.IsDuplicate__c == false)
            {
                
                String strTemp = String.valueOf(e_new.id).substring(0,15).trim();
               /* System.debug('The MeetingId is***'+strTemp +'****'+String.valueOf(e_new.id).substring(0, 15));
                evntlst = [select Meeting_ID__c,CurrencyIsoCode,IsAllDayEvent,OwnerId,ActivityDate,Purpose__c,Description,DurationInMinutes,EndDateTime,RecordTypeId,Location,Location__c,WhoId,IsPrivate,IsReminderSet,ShowAs,StartDateTime,Subject,ActivityDateTime,Type,Hospitality_Gift_accepted_or_offered__c,Gift_given__c,Gift__c,Hospitality__c,Hospitality_given__c,Estimated_value__c,Description_hospitality__c FROM Event where Meeting_ID__c =:strTemp];
                
                EventRelationHelper.FetchChildRecord(e_new,strTemp);
                system.debug('child events: '+evntlst);
               /* for(Integer i=0;i<evntlst.size();i++)
                {
                    evntlst[i].CurrencyIsoCode = e_new.CurrencyIsoCode;
                    evntlst[i].IsAllDayEvent = e_new.IsAllDayEvent;
                    evntlst[i].ActivityDate = e_new.ActivityDate;
                    evntlst[i].Purpose__c = e_new.Purpose__c;
                    evntlst[i].DurationInMinutes = e_new.DurationInMinutes;
                    evntlst[i].EndDateTime = e_new.EndDateTime;
                    evntlst[i].Location = e_new.Location;
                    evntlst[i].Location__c = e_new.Location__c;
                    evntlst[i].type = e_new.Type;
                    evntlst[i].WhoId = e_new.WhoId;
                    evntlst[i].IsPrivate = e_new.IsPrivate;
                    evntlst[i].IsReminderSet = e_new.IsReminderSet;
                    evntlst[i].ShowAs = e_new.ShowAs;
                    evntlst[i].StartDateTime = e_new.StartDateTime;
                    evntlst[i].Subject = e_new.Subject;
                    evntlst[i].ActivityDateTime = e_new.ActivityDateTime;
                    evntlst[i].Description = e_new.Description;
                    evntlst[i].RecordTypeId = System.Label.Related_Event_RecordType;//SF-4310
                }
                try{
                    system.debug('child events size: '+evntlst.size());
                    if(evntlst.size() > 0)
                        update evntlst;
                }
                catch(Exception e){
                    system.debug('Exception Occured '+e);
                }
             String str = String.valueOf(e_new.id).substring(0, 15);
             //String EventOwnerId = String.valueOf(e_new.ownerId);
            //EventRelationHelper.UpdateChildRecord(e_new,str);
            //EventRelationHelper.UpdateChildRecordInsert(EventOwnerId ,str);
            EventRelationHelper.UpdateChildRecord(trigger.new);
            EventRelationHelper.UpdateChildRecordInsert(str);

            }
            
            
            /* SF-3936-- Ends */
            //String str = String.valueOf(e_new.id).substring(0, 15);
           // EventRelationHelper.UpdateEventFeedTask(str,e_new,e_old);
   /*         For(Event e_new:trigger.new){ 
            //commented for 3837 - Aritra
            //if(e_new.Type.equalsignoreCase('meeting') && e_new.Minutes_of_Meeting__c != e_old.Minutes_of_Meeting__c)      
            Boolean completeTask = false;
            List<Task> taskList;
            //aritra if(e_new.Type != null && e_new.Type != '' && e_old.Purpose__c != null && e_old.Purpose__c != ''){
            if(e_new.WhatId != null && (e_new.WhatId.getsObjectType() == Opportunity.sObjectType)){
               if(e_new.Type != null && e_new.Type.equalsignoreCase('meeting') && e_new.Description != null && e_new.Description != e_old.Description){            
                    //******SF - 4072: If, event is linked with an oppty then it will link the chatter post to opportunity's Company*****Date - 19/01/2018
                            Id accId = [select id,AccountId from Opportunity where id=:e_new.WhatId].AccountId;
                            List<FeedItem> fItemList = new List<FeedItem>();
                            FeedItem fitemAcc = new FeedItem();
                            //Opportunity's account feed
                            fitemAcc.type = 'LinkPost';
                            fitemAcc.ParentId = accId;
                           //SOC 27/3/13 - 2013R1 Changed URL to include base URL so that links on email Chatter notifications work
                            fitemAcc.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + e_new.id;
                            fitemAcc.Title = 'View Meeting'; 
                            fitemAcc.Body = 'Meeting Update' + ((e_new.Location__c) != null ? ', ' + e_new.Location__c : '') 
                                    + '\nStart:' + e_new.StartDateTime.format() + ',  End:' + e_new.EndDateTime.format()
                                    + '\nSubject: ' + e_new.Subject;
                                    
                            fItemList.add(fitemAcc);
                            //Opportunity feed
                            FeedItem fitemOpp = new FeedItem();
                            //Opportunity's account feed
                            fitemOpp.type = 'LinkPost';
                            fitemOpp.ParentId = e_new.WhatId;
                           //SOC 27/3/13 - 2013R1 Changed URL to include base URL so that links on email Chatter notifications work
                            fitemOpp.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + e_new.id;
                            fitemOpp.Title = 'View Meeting'; 
                            fitemOpp.Body = 'Meeting Update' + ((e_new.Location__c) != null ? ', ' + e_new.Location__c : '') 
                                    + '\nStart:' + e_new.StartDateTime.format() + ',  End:' + e_new.EndDateTime.format()
                                    + '\nSubject: ' + e_new.Subject;
                                    
                            fItemList.add(fitemOpp);
                            insert fItemList;
                    completeTask = true;
                    
                }
            }
            
            else{
                    if(e_new.WhatId != null && (e_new.Type != null && e_new.Type.equalsignoreCase('meeting') && e_new.Description != null && e_new.Description != e_old.Description)){
                        FeedItem fitem = new FeedItem();                       
                            fitem.type = 'LinkPost';
                            fitem.ParentId = e_new.WhatId;
                           //SOC 27/3/13 - 2013R1 Changed URL to include base URL so that links on email Chatter notifications work
                            fitem.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + e_new.id;
                            fitem.Title = 'View Meeting'; 
                            fitem.Body = 'Meeting Update' + ((e_new.Location__c) != null ? ', ' + e_new.Location__c : '') 
                                    + '\nStart:' + e_new.StartDateTime.format() + ',  End:' + e_new.EndDateTime.format()
                                    + '\nSubject: ' + e_new.Subject;
                                    
                    insert fitem;
                    system.debug('updated---'+fitem.Body);
                    completeTask = true;
                   }
                
                
                }
                //if(completeTask || (e_new.EndDateTime != e_old.EndDateTime))
                
                if(completeTask || (e_new.EndDateTime != e_old.EndDateTime)){
                    taskList= [SELECT id, subject, status, activityDate from Task where Meeting_ID__c = :e_new.id AND status != 'Completed' limit 1];
                   
                    if(taskList != null && taskList.size() > 0){
                         // added for SF-3853
                        //isComplete__c = true -> denotes the task should not be shown in the activity timeline
                        taskList[0].isComplete__c= true;
                        // end of SF-3853
                        if(completeTask){
                            taskList[0].status = 'Completed';
                        }
                        if(e_new.EndDateTime != e_old.EndDateTime){
                            taskList[0].ActivityDate = Date.newinstance(e_new.EndDateTime.year(), e_new.EndDateTime.month(), e_new.EndDateTime.day()); //SF 3863
                        }
                    }
                }
                
                //added for 3899
                //if event's subject is changed it must do cascade change 
                //to task's subject and task's meeting name text field
                System.debug('e_new.subject--' + e_new.subject+'e_new.id->'+e_new.id);
                System.debug('e_old.subject--' + e_old.subject+'e_old.id->'+e_old.id);
                if(e_new.Type != null && e_new.Type.equalsignoreCase('meeting')){// added for SF-4007
                    if(e_new.subject != e_old.subject){
                        System.debug('I am here1');
                        Task t;
                        if(taskList != null && taskList.size() > 0){
                            Tasklist[0].Subject = 'Complete Meeting minutes - ' + e_new.subject;
                            Tasklist[0].meeting_name__c = e_new.subject;
                            
                        }
                        else{
                            taskList= [SELECT id, subject from Task where Meeting_ID__c = :e_new.id limit 1];
                            if(taskList != null && taskList.size() > 0){// add for SF-4007 null check
                            System.debug('I am in Task Query on update');
                                Tasklist[0].Subject = 'Complete Meeting minutes - ' + e_new.subject;
                                Tasklist[0].meeting_name__c = e_new.subject;
                               
                            }// add for SF-4007 null check
                         // add new task on update if Task is not there SF-4007
                         else {
                         System.debug('I am in Task create on update');
                             Task t_new = new Task();
                            if(e_new.WhatId != null){
                                t_new.whatId = e_new.WhatId;
                                t_new.meeting_id__c = e_new.id;
                                t_new.meeting_name__c = e_new.subject;
                                t_new.OwnerId = e_new.OwnerId;
                                t_new.Subject = 'Complete Meeting minutes - ' + e_new.subject;
                                t_new.Priority = 'Normal';
                                t_new.ActivityDate = date.newinstance(e_new.EndDateTime.year(), e_new.EndDateTime.month(), e_new.EndDateTime.day()); //SF 3863
                                //t.ActivityDate = e_new.EndDateTime; //SF 3863
                                t_new.Status = 'Not Started';
                                // added for SF-3853
                                t_new.isComplete__c= true;
                                // end of SF-3853
                                //if(e_new.Minutes_of_Meeting__c != null && e_new.Minutes_of_Meeting__c != '')
                                if(e_new.Description != null && e_new.Description != '') 
                                {
                                    t_new.Status = 'Completed';                        
                                } //This is to ensure to take scenario, where the minutes are filled while creating meetings
                                insert t_new;
                            }
                         }
                         // add new task on update if Task is not there SF-4007   
                        
                        }
                    }
                    //added 3899
                
                    //Finally update the task/tasklist
       /*             if(taskList != null && taskList.size() > 0){
                        System.debug('I am here2');
                        update taskList;
                    }
               
                }// added for SF-4007
            //aritra }
        }
    } */
//--- Update Ends ---
   // Added for SF-3936  - Geetham
    //SF-3936 -- starts 
  /*  if(Trigger.isDelete)
    {
       e_old = Trigger.old[0];
       String str = String.valueOf(e_old.id).substring(0,15);
       EventRelationHelper.DeleteChildRecord(str);
       List<Event> AccountId = new List<Event>();
       List<Event> evntlst_update = new List<Event>();
       List<Event> evntlst = new List<Event>();
       List<FeedItem> FeedItemlst=new List<FeedItem>();
       set<ID> EventWhatIDS = new set<ID>();
       set<ID> EventWhatIDS1 = new set<ID>();
       AccountId=[select WhatId from Event where Meeting_Id__c=:String.valueOf(e_old.id).substring(0,15)];
       system.debug('The Account Ids are'+AccountId);
       for(Event EventItreator:AccountId)        
       {       
            EventWhatIDS.add(EventItreator.WhatId);     
       }  
       if(EventWhatIDS.size()>0 && EventWhatIDS!=null)
       {
           evntlst = [select Meeting_ID__c,CurrencyIsoCode,IsAllDayEvent,OwnerId,ActivityDate,Purpose__c,Description,DurationInMinutes,EndDateTime,RecordTypeId,Location,Location__c,WhoId,IsPrivate,IsReminderSet,ShowAs,StartDateTime,Subject,ActivityDateTime,Type FROM Event where WhatId IN :EventWhatIDS and Meeting_ID__c=:String.valueOf(e_old.id).substring(0, 15)];        
           try
           {
                delete evntlst;
           }
            catch(Exception e){
                system.debug('Exception Occured '+e);
            }
        }
        
        List<task> tasklst=[select id from task where whatId IN :EventWhatIDS and Meeting_ID__c=:e_old.id];
            if(tasklst!=null && tasklst.size()>0)
            {
                delete tasklst;
            }
            
         List<Event> evntOldTrigger = Trigger.old;
         for(Event AccountIdIterator:evntOldTrigger)        
         {       
            EventWhatIDS1.add(AccountIdIterator.WhatId);     
         }     
          List<FeedItem> fitemlst =[SELECT id,LinkUrl FROM FeedItem where ParentId IN :EventWhatIDS1];
          for(FeedItem f:fitemlst)
          { 
               if(f.LinkUrl!=null)
               {
                   for(Event e1:evntOldTrigger)
                   {
                       if(f.LinkUrl.right(18).substring(0,15) == e1.id || f.LinkUrl.right(18)== e1.id)
                       {
                            FeedItemlst.add(f);
                       }
                   }
               }
           } 
            system.debug('The feeditemlist***'+FeedItemlst);
            delete FeedItemlst;         
    } */
        //SF-3936 -- Ends
}