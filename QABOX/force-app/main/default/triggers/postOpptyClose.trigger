trigger postOpptyClose on Opportunity (after insert,after update, before update) {

    //PCI-000140 Post to account when opp closed won
    
    List<FeedItem> feedItems = new List<FeedItem>();
    String renewalIndex;
    List<Opportunity> oppLst = (List<Opportunity>)Trigger.New; // SF - 5054
    List<OpportunityLineItem> prodList = new List<OpportunityLineItem>(); // SF - 5054
    Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Marine').getRecordTypeId(); // SF-5054
    //Check if opp is newly closed won
    for(Opportunity o:trigger.new)
    {
        renewalIndex = '';
        Boolean oppIsNewlyClosedWon = false;
        if(trigger.isInsert && o.isClosed == true && o.isWon == true)
        {
            oppIsNewlyClosedWon = true;
        }
        else if(trigger.isUpdate)
        {
            if(o.isClosed == true && trigger.oldMap.get(o.Id).isClosed == false)
            {
                if(o.isWon == true && trigger.oldMap.get(o.Id).isWon == false)
                {
                    oppIsNewlyClosedWon = true;
                }
            }
        }
        
        if(oppIsNewlyClosedWon == true && !Gard_RecursiveBlocker.blocker)
        {   
            Gard_RecursiveBlocker.blocker = true;
            FeedItem fitem = new FeedItem();
            if(o.Type == 'Renewal'){
                renewalIndex =  '\n' + Schema.Opportunity.fields.Opportunity_Renewal_Index__c.getDescribe().getLabel() + ': ' + o.Opportunity_Renewal_Index__c;
            }
            fitem.type = 'LinkPost';
            fitem.ParentId = o.AccountId;
            fitem.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + o.Id;
            fitem.Title = 'View Opportunity';
            
            fitem.Body = 'Opportunity won: ' + o.Name
                        + '\n' + Schema.Opportunity.fields.Amount.getDescribe().getLabel() + ': ' + o.CurrencyIsoCode + ' ' + o.Amount +
                        renewalIndex +//+ '\n' + Schema.Opportunity.fields.Opportunity_Renewal_Index__c.getDescribe().getLabel() + ': ' + o.Opportunity_Renewal_Index__c
                        + '\n' + Schema.Opportunity.fields.Budget_Year__c.getDescribe().getLabel() + ': ' + o.Budget_Year__c
                        + '\n' + Schema.Opportunity.fields.Type.getDescribe().getLabel() + ': ' + o.Type;
                                                  
            feedItems.add(fitem);
        }
    }
    
    //Save the FeedItems all at once.
    if (feedItems.size() > 0) {
        Database.insert(feedItems,false); //notice the false value. This will allow some to fail if Chatter isn't available on that object
    }
    
    // added for SF - 5054
    if(trigger.isUpdate && trigger.isBefore)
    {
      prodList = [SELECT Id, OpportunityId FROM OpportunityLineItem WHERE OpportunityId IN : oppLst AND (Product2.name = 'Hull and Machinery' OR Product2.name ='Loss of Hire (LOH)' OR Product2.name ='Increased Value (IV)') AND (RA__C = NULL OR MA__C= NULL)];
        if(prodList.size() > 0){
            for(Opportunity opp : oppLst){
                if(Trigger.oldMap.get(Opp.id).StageName == 'Quote' && (Opp.StageName == 'Closed Won' || opp.StageName == 'Closed Lost' || opp.StageName == 'Closed Declined' || opp.StageName == 'Closed Other') && opp.RecordTypeId == RecordTypeId ){
                     Opp.addError('Please make sure that the RA% and MA% fields are filled in for the products(Hull and Machinery,Loss of Hire (LOH),Increased Value (IV)) , under the Products related list. If not applicable enter 0');
                }
            }
        }
    }
    

}