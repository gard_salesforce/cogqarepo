trigger MDMSyncRole on MDM_Role__c bulk (before insert,after insert,before update,after update,before delete,after delete) {

    MDM_Settings__c mdmSettings;
    if(!test.isRunningTest()){
        mdmSettings= MDMSettingSupport.GetSetting(UserInfo.getUserId());
    }
    else{
        mdmSettings = new MDM_Settings__c(AccountOnRisk_Trigger_Enabled__c = true, Account_Trigger_Enabled__c = true,
                                          Contact_Trigger_Enabled__c = true, Edit_Sync_Failure_Records__c = false,
                                          Triggers_Enabled__c = true , Web_Services_Enabled__c = true, MDM_Role_Trigger_Enabled__c = true
                                          );  
        //insert MDMsettings ;
    }
    if(mdmSettings.Triggers_Enabled__c && mdmSettings.MDM_Role_Trigger_Enabled__c){
        if (trigger.isBefore) {
            if (trigger.isInsert) {
                MDMSyncRoleHandler.BeforeInsert(trigger.new, trigger.newMap);
            } else if (trigger.isUpdate) {
                MDMSyncRoleHandler.BeforeUpdate(trigger.new, trigger.newMap, trigger.old, trigger.oldMap);
            } else if (trigger.isDelete) {
                MDMSyncRoleHandler.BeforeDelete(trigger.old, trigger.oldMap);
            }
            
        } else if (trigger.isAfter) {
        /*
            if (trigger.isInsert && Gard_RecursiveBlocker.blocker) {
                system.debug('Gard_RecursiveBlocker.blocker---'+Gard_RecursiveBlocker.blocker);
                MDMSyncRoleHandler.AfterInsert(trigger.new, trigger.newMap);
            } else if (trigger.isUpdate) {
                system.debug('<----After Update roles fired---->');
                MDMSyncRoleHandler.AfterUpdate(trigger.new, trigger.newMap, trigger.old, trigger.oldMap);
            } else if (trigger.isDelete) {
                MDMSyncRoleHandler.AfterDelete(trigger.old, trigger.oldMap);
            }
         */
         Boolean modified = false;
            if (trigger.isUpdate){
                for(MDM_Role__c roles:trigger.new){
                    if(roles.Role_Modified__c == true){
                        modified = true;
                    }
                    else{modified = false;break;}
                }
                system.debug('trigger.new[0]-> '+trigger.new[0].Role_Modified__c);
            }
            if (trigger.isUpdate  && modified) {
                system.debug('role modified- > '+modified+' -- Synchronisation_Status__c -- '+trigger.new[0].Synchronisation_Status__c);
                MDMSyncRoleHandlerNew.AfterUpdate(trigger.new, trigger.newMap, trigger.old, trigger.oldMap);
            }
            else if (trigger.isDelete) {
                MDMSyncRoleHandler.AfterDelete(trigger.old, trigger.oldMap);
            }
        }
    }

}