import { LightningElement, api, track } from "lwc";

export default class SearchBarTile extends LightningElement {
  @api contactName;
  @api contactEmail;
  @api tileId;
  @api companyName;
 
  @track focus = false;

  /**
   * Handles focus class on tile
   *
   * @returns {String} class for tile component
   */
  get tileClass() {
    const basecls =
      "slds-media slds-listbox__option slds-listbox__option_entity slds-listbox__option_has-meta";
    return this.focus ? basecls + " slds-has-focus" : basecls;
  }

  /**
   * Dispatches tileselect event 
   *
   * @fires tileselect
   */
  tileSelect() {
    this.dispatchEvent(
      new CustomEvent("tileselect", {
        detail: {
          tileId: this.tileId,
          contactName: this.contactName,
          contactEmail: this.contactEmail //added for testing
        }
      })
    );
  }

	/**
	 * Adds functionality for selecting tile by pressing enter
	 * 
	 * @param {object} event - keyup event
	 */
  handleKeyUp(event) {
    const isEnterKey = event.keyCode === 13;
    if (isEnterKey) {
      this.tileSelect();
    }
  }

	/**
	 * Makes tile selecteable by clicking it
	 */
  handleClick() {
    this.tileSelect();
  }

	/**
	 * Updates focus property to true when tile is focused
	 */
  handleFocus() {
    this.focus = true;
	}
	
	/**
	 * Updates focus property to false when tile is blurred
	 */
  handleBlur() {
    this.focus = false;
  }
}