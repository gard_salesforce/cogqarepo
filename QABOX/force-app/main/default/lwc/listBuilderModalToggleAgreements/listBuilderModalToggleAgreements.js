import { LightningElement, track, api } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";

export default class ListBuilderModalToggleAgreements extends LightningElement {
  @api recordId;

  @track toggleModal = false;
  /**
   * Toggles the modal on.
   */
  handleClick() {
    this.toggleModal = true;
  }

  /**
   * Toggles the modal off.
   */
  handleClose() {
    this.toggleModal = false;
  }

  /**
   * Closes the modal and displays success or error message.
   *
   * Fires a refresh event to tell the wrapper component to refresh the view if saving
   * was successful.
   *
   * @fires refresh
   *
   * @param {Object} event   - save event
   */
  handleSaveAndClose(event) {
    this.toggleModal = false;
    if (event.detail.status === "success") {
      this.dispatchEvent(new CustomEvent("refresh"));

      this.dispatchEvent(
        new ShowToastEvent({
          title: "Success",
          message: event.detail.message,
          variant: "success"
        })
      );
    } else if (event.detail.status === "error") {
      this.dispatchEvent(
        new ShowToastEvent({
          title: "Error",
          message: event.detail.message,
          variant: "error"
        })
      );
    }
  }
}