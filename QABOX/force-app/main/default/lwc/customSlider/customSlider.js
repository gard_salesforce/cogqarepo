import { LightningElement, api } from "lwc";

export default class CustomSlider extends LightningElement {
  @api min;
  @api max;
  @api value;
  @api disabled;
  @api step = 1;
  @api labelHidden = false;

  /**
   * Updates value of slider and dispatches event with value.
   * 
   * Delays event firing if new event is fired within 500 ms.
   * 
   * @fires slide 
   * 
   * @param {object} event - input event
   */
  handleInput(event) {
    this.value = event.target.value;
    window.clearTimeout(this.delaySlider);
    // eslint-disable-next-line @lwc/lwc/no-async-operation
    this.delaySlider = setTimeout(() => {
      this.dispatchEvent(
        new CustomEvent("slide", {
          detail: this.value
        })
      );
    }, 500);
  }
}