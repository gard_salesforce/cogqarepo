/* eslint-disable @lwc/lwc/no-async-operation */
import { LightningElement, api, track, wire } from "lwc";
import getAccounts from "@salesforce/apex/listBuilderModalController.getRelevantAccounts";
// import { compareInArray } from "c/jsUtils";

export default class ListBuilderSearchBar extends LightningElement {
  @api recordId;
  @track tiles = [];
  @track pillMap = new Map([]);
  @track searchTerm;

  /**
   * Tracks slider state
   *
   * @param {boolean} disabled      - Whether the slider is disabled or not, determined by the handleYearFilter
   * @param {Integer} min           - Minimum value for the slider
   * @param {Integer} max           - Maximum value for the slider
   * @param {Integer} value         - current value for the slider
   */
  @track slider = {
    disabled: true,
    min: 2000,
    max: new Date().getFullYear().toString(),
    value: new Date().getFullYear().toString()
  };

  /**
   * Tracks view state
   *
   * @param {boolean} searchFlag      - Is true when typing in searchbar, toggles the dropdown
   * @param {boolean} loading         - Is true when loading results
   * @param {boolean} noResults       - True if no results are returned, displays message in dropdown
   * @param {Integer} itemsSelected   - Keeps track of number of pills
   */
  @track state = {
    searchFlag: false,
    loading: false,
    noResults: false,
    itemsSelected: this.pillMap.size
  };

  /**
   * Wires the searchterm to backend and updates the list of this.tiles which are displayed
   *
   * @param {object} results - {error, data}
   */
  @wire(getAccounts, { companyString: "$searchTerm" })
  getAccounts(result) {
    var data = result.data;
    this.tiles = [];
    if (data) {
      let accounts = JSON.parse(data);
      this.tiles = accounts.map(account => {
        let tile = {};
        tile.id = account.recordId;
        tile.name = account.name;
        tile.companyId = account.companyId;
        tile.subinfo = account.billingCity;
        return tile;
      });
    }
    this.setState({ loading: false });
  }

  /**
   * Returns the pill object containing data to view
   */
  get pills() {
    let pills = [];
    this.pillMap.forEach((value, key) => {
      pills.push({ id: key, label: value });
    });
    return pills;
  }

  /**
   * Triggers the dropdown displaying tiles
   *
   * @return {String} The class which displays and hides the dropdown.
   */
  get comboBoxClass() {
    const basecls =
      "slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click";
    return this.state.searchFlag ? basecls + " slds-is-open" : basecls;
  }

  /**
   * Updates this.searchTerm based on the value from the lightning-input component
   *
   * Ignores whitespace and the onchange event is debounced, i.e. the searchTerm is not updated
   * if a new event is fired within 500 ms, to reduce the number of calls to the backend and hence
   * SOQL queries.
   *
   * @param {object} event - onchange event fired by the lightning-input component
   */
  handleSearch(event) {
    const input = event.detail.value;

    if (!input || input.length === 0 || /^\s*$/.test(input)) {
      this.setState({ searchFlag: false });
    } else {
      this.setState({ searchFlag: true, loading: true });

      window.clearTimeout(this.delayTimeout);
      this.delayTimeout = setTimeout(() => {
        this.searchTerm = input;
      }, 500);
    }
  }

  /**
   * Handles a tileselect event.
   *
   * Adds a pill to this.pills corresponding to the tile selected, if it is not already added,
   * and fires a pilladd event.
   *
   * @fires pilladd
   *
   * @param {object} event - tileselect event
   */
  handleTileSelect(event) {
    var tile = event.detail;

    this.pillMap.set(tile.tileId, tile.name);

    this.dispatchEvent(
      new CustomEvent("pilladd", {
        detail: tile.tileId
      })
    );
    this.resetSearch();
  }

  /**
   * Handles pillremove event
   *
   * Removes the pill from this.pills and sends a new pillremove event to parent component.
   *
   * @fires pillremove
   *
   * @param {object} event - remove event
   */
  handlePillRemove(event) {
    let pillId = event.detail;
    this.pillMap.delete(pillId);
    this.updateState();

    this.dispatchEvent(
      new CustomEvent("pillremove", {
        detail: pillId
      })
    );
  }

  /**
   * Handles checkbox change on Risk Indicator checkbox
   *
   * @fires filter
   *
   * @param {object} event - change event from checkbox
   */
  handleRiskFilter(event) {
    this.dispatchEvent(this.filterEvent("riskIndicator", event.detail.checked));
  }

  /**
   * Handles checkbox change on Policy Year checkbox
   *
   * @fires filter
   *
   * @param {object} event - change event from checkbox
   */
  handleYearFilter(event) {
    this.slider.disabled = !event.detail.checked;
    if (this.slider.disabled) {
      this.dispatchEvent(this.filterEvent("policyYear", null));
    } else {
      this.dispatchYearFilter();
    }
  }

  /**
   * Handles slider change
   *
   * @fires filter
   *
   * @param {object} event - onslide event from slider
   */
  handleSlide(event) {
    this.slider.value = event.detail;
    this.dispatchYearFilter();
  }

  /**
   * Handles Agreement Number Filter
   *
   * Takes in onchange event from lightning-input and fires filter event with input value.
   * Debounces onchange event to prevent too many filter events firing.
   *
   * @fires filter
   *
   * @param {object} event - onchange event from lightning-input component
   */
  handleAgreementFilter(event) {
    window.clearTimeout(this.delayFilter);
    let value = event.detail.value === '' ? null : event.detail.value;
    this.delayFilter = setTimeout(() => {
      this.dispatchEvent(
        this.filterEvent("agreementReference", value)
      );
    }, 500);
  }

  /**
   * Handles Cover Filter
   *
   * Takes in onchange event from lightning-input and fires filter event with input value.
   * Debounces onchange event to prevent too many filter events firing.
   *
   * @fires filter
   *
   * @param {object} event - onchange event from lightning-input component
   */
  handleCoverFilter(event) {
    window.clearTimeout(this.delayFilter);
    let value = event.detail.value === '' ? null : event.detail.value;
    this.delayFilter = setTimeout(() => {
      this.dispatchEvent(this.filterEvent("coverName", value));
    }, 500);
  }

  /**
   * Handles Object Filter
   *
   * Takes in onchange event from lightning-input and fires filter event with input value.
   * Debounces onchange event to prevent too many filter events firing.
   *
   * @fires filter
   *
   * @param {object} event - onchange event from lightning-input component
   */
  handleObjectFilter(event) {
    window.clearTimeout(this.delayFilter);
    let value = event.detail.value === '' ? null : event.detail.value;
    this.delayFilter = setTimeout(() => {
      this.dispatchEvent(this.filterEvent("objectName", value));
    }, 500);
  }

  /**
   * Dispatches filter event with policyYear
   *
   * @fires filter
   */
  dispatchYearFilter() {
    this.dispatchEvent(this.filterEvent("policyYear", this.slider.value));
  }

  filterEvent(filter, value) {
    return new CustomEvent("filter", {
      detail: {
        filter: filter,
        value: value
      }
    });
  }

  /**
   * Resets searchTerm and sets searchFlag false
   */
  resetSearch() {
    this.searchTerm = "";
    this.setState({ searchFlag: false });
  }

  /**
   * Sets the state and updating related states
   *
   * @param {object} state    - An object where the property is a property of the
   *                            tracked this.state and value is the value to be assigned
   */
  setState(state) {
    for (const key in state) {
      if (this.state.hasOwnProperty(key)) {
        this.state[key] = state[key];
      }
    }
    this.updateState();
  }

  /**
   * Updates other properties based on the new state set.
   */
  updateState() {
    this.state.itemsSelected = this.pillMap.size;
    this.state.noResults =
      this.state.searchFlag && !this.state.loading && this.tiles.length === 0;
  }
}