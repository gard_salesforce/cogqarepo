import { LightningElement, api } from "lwc";

export default class SearchBarPill extends LightningElement {
  @api label;
  @api pillId;

  /**
   * Handles pill remove
   * 
   * @fires remove
   */
  handleRemove() {
    this.dispatchEvent(new CustomEvent("remove", {
      detail: this.pillId,
    }));
  }
}