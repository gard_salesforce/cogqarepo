/* eslint-disable @lwc/lwc/no-async-operation */
import { LightningElement, api, track, wire } from "lwc";
import getContacts from "@salesforce/apex/ClaimSurveyController.getRelevantContacts";
// import { compareInArray } from "c/jsUtils";

export default class ListBuilderSearchBar extends LightningElement {
  @api recordId;
  @track tiles = [];
  @track pillMap = new Map([]);
  @track searchTerm;

 
  /**
   * Tracks view state
   *
   * @param {boolean} searchFlag      - Is true when typing in searchbar, toggles the dropdown
   * @param {boolean} loading         - Is true when loading results
   * @param {boolean} noResults       - True if no results are returned, displays message in dropdown
   * @param {Integer} itemsSelected   - Keeps track of number of pills
   */
  @track state = {
    searchFlag: false,
    loading: false,
    noResults: false,
    itemsSelected: this.pillMap.size
  };

  /**
   * Wires the searchterm to backend and updates the list of this.tiles which are displayed
   *
   * @param {object} results - {error, data}
   */
  @wire(getContacts, { contactString: "$searchTerm" })
  getContacts(result) {
    var data = result.data;
    this.tiles = [];
    if (data) {
      let contacts = JSON.parse(data);
      this.tiles = contacts.map(contact => {
        let tile = {};
        tile.id = contact.value;
        tile.name = contact.label;
        tile.companyName = contact.companyName;
        tile.email = contact.email;
        return tile;
      });
    }
    this.setState({ loading: false });
  }

  /**
   * Returns the pill object containing data to view
   */
  get pills() {
    let pills = [];
    this.pillMap.forEach((value, key) => {
      pills.push({ id: key, label: value });
    });
    return pills;
  }

  /**
   * Triggers the dropdown displaying tiles
   *
   * @return {String} The class which displays and hides the dropdown.
   */
  get comboBoxClass() {
    const basecls =
      "slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click";
    return this.state.searchFlag ? basecls + " slds-is-open" : basecls;
  }

  /**
   * Updates this.searchTerm based on the value from the lightning-input component
   *
   * Ignores whitespace and the onchange event is debounced, i.e. the searchTerm is not updated
   * if a new event is fired within 500 ms, to reduce the number of calls to the backend and hence
   * SOQL queries.
   *
   * @param {object} event - onchange event fired by the lightning-input component
   */
  handleSearch(event) {
    const input = event.detail.value;

    if (!input || input.length === 0 || /^\s*$/.test(input)) {
      this.setState({ searchFlag: false });
    } else {
      this.setState({ searchFlag: true, loading: true });

      window.clearTimeout(this.delayTimeout);
      this.delayTimeout = setTimeout(() => {
        this.searchTerm = input;
      }, 500);
    }
  }

  /**
   * Handles a tileselect event.
   *
   * Adds a pill to this.pills corresponding to the tile selected, if it is not already added,
   * and fires a pilladd event.
   *
   * @fires pilladd
   *
   * @param {object} event - tileselect event
   */
  handleTileSelect(event) {
    var tile = event.detail;

    this.pillMap.set(tile.tileId, tile.contactName);
    //console.log("tile onselect:"+tile.tileId+"-"+tile.contactName+"-"+tile.contactEmail);
    /*this.pillMap.forEach((value, key) => {
      console.log("Pill Map-"+value+" - "+key);
    });
    */
    this.dispatchEvent(/*
      new CustomEvent("pilladd", {
        detail: tile.tileId
      }*/
      new CustomEvent("pilladd", {
        detail: {
          tileId: tile.tileId,
          contactName: tile.contactName,
          contactEmail: tile.contactEmail
          //pills : this.pills  //Added for testing
        }
      })
    );
    this.resetSearch();
  }

  /**
   * Handles pillremove event
   *
   * Removes the pill from this.pills and sends a new pillremove event to parent component.
   *
   * @fires pillremove
   *
   * @param {object} event - remove event
   */
  handlePillRemove(event) {
    let pillId = event.detail;
    //console.log("pillId in searchbar component : "+pillId);
    this.pillMap.delete(pillId);
    this.updateState();

    this.dispatchEvent(
      new CustomEvent("pillremove", {
        /*detail: {
          tileId: pillId.tileId,
          contactName: pillId.contactName,
          contactEmail: pillId.contactEmail,
          pills : this.pills  //Added for testing
        }*/
        detail: pillId
      })
    );
  }
  /**
   * Resets searchTerm and sets searchFlag false
   */
  resetSearch() {
    this.searchTerm = "";
    this.setState({ searchFlag: false });
  }

  /**
   * Sets the state and updating related states
   *
   * @param {object} state    - An object where the property is a property of the
   *                            tracked this.state and value is the value to be assigned
   */
  setState(state) {
    for (const key in state) {
      if (this.state.hasOwnProperty(key)) {
        this.state[key] = state[key];
      }
    }
    this.updateState();
  }

  /**
   * Updates other properties based on the new state set.
   */
  updateState() {
    this.state.itemsSelected = this.pillMap.size;
    this.state.noResults =
      this.state.searchFlag && !this.state.loading && this.tiles.length === 0;
  }
}