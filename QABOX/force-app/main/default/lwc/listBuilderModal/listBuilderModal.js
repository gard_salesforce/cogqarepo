/* eslint-disable default-case */
import { LightningElement, api, track } from "lwc";
import getRelatedCovers from "@salesforce/apex/listBuilderModalController.getRelatedRelevantCovers";
import getMoreCovers from "@salesforce/apex/listBuilderModalController.getMoreRelevantCovers";
import updateCovers from "@salesforce/apex/listBuilderModalController.deleteCreateJunctionCovers";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
// import { isEmpty } from "c/jsUtils";

/**
 * Columns in the data table.
 */
const columns = [
  {
    label: "Cover Name",
    fieldName: "coverName",
    type: "text",
    sortable: true
  },
  {
    label: "Object",
    fieldName: "objectName",
    type: "text",
    sortable: true
  },
  {
    label: "Agreement",
    fieldName: "agreementReference",
    type: "text",
    sortable: true
  },
  {
    label: "Company",
    fieldName: "companyName",
    type: "text",
    sortable: true
  },
  {
    label: "Policy Year",
    fieldName: "policyYear",
    type: "text",
    sortable: true
  },
  {
    label: "Risk Indicator",
    fieldName: "riskIndicator",
    type: "boolean"
  }
];

/**
 * Contains the functions and properties for the ListBuilderModal.
 *
 * @constructs LightningElement
 */
export default class ListBuilderModal extends LightningElement {
  @api recordId;
  @api loadLimit;
  rows = [];
  @track filteredRows = [];
  @track columns = columns;
  @track initialRender = true;
  earlierCheckedValues = []; 
  checked = [];

  useFilter = false;

  /**
   * State tracking object.
   *
   * @property {list} checkedRows                   - The rows currently checked
   * @property {boolean} loading                    - If the view is loading records from backend
   * @property {object} filters                     - The filters currently applied
   * @property {boolean} filters.riskIndicator      - riskIndicator filter
   * @property {Integer} filters.policyYear         - The policy year to filter rows by
   * @property {String} filters.agreementReference  - The string to filter agreement reference by
   */
  @track state = {
    checkedRows: [],
    loading: false,
    filters: {
      riskIndicator: true,
      policyYear: null,
      agreementReference: null,
      coverName: null,
      objectName: null
    }
  };

  /**
   * Sort state tracking object
   *
   * @property {String} by            - fieldName of column to sort by
   * @property {String} direction     - Direction to sort by, either "asc" or "desc"
   */
  @track sorting = {
    by: null,
    direction: "asc"
  };

  // Keeps track of accountIds to load records from
  pillIds = new Set([]);
  // Keeps checked rows in memory while filtered away
  checkedRowsShadow = new Set([]);

  /**
   * Loads related covers on initial render.
   *
   */
  renderedCallback() {
    if (this.initialRender === true) {
      getRelatedCovers({ caseId: this.recordId })
        .then(checkedCovers => {
          this.rows.push(...JSON.parse(checkedCovers));

          this.initialChecked = new Set([]);

          this.rows.forEach(row => {
            this.checkedRowsShadow.add(row.recordId);
            this.initialChecked.add(row.recordId);
          });

          this.setState({ checkedRows: Array.from(this.checkedRowsShadow) });

          this.updateFilters();
          // console.log(JSON.stringify(this.state.checkedRows));
        })
        .catch(() => {
          this.setState({ loading: false });
        });

      this.initialRender = false;
    }
  }

  /**
   * Handles record adding when a pill is added/tile is clicked.
   *
   * @param {object} event - pilladd event
   */
  handlePillAdd(event) {
    //this.setState({ loading: true });
    let target = this.template.querySelector("lightning-datatable");
    // console.log(target);

    target.enableInfiniteLoading = true;
    // console.log("Pill add, getting more covers");

    const pillId = event.detail;
    // console.log(pillId);
    this.pillIds.add(pillId);
    this.loadMoreData(target);
  }

  /**
   * handles load more event
   *
   * @param {object} event
   */
  handleLoadMore(event) {
    // console.log("loads more");
    this.loadMoreData(event.target);
  }

  /**
   * Loads more rows
   *
   * @param {object} target - the lightning-datatable element
   */
  loadMoreData(target) {
    target.isLoading = true;
    // eslint-disable-next-line radix
    const limit = parseInt(this.loadLimit);

    getMoreCovers({
      accountIds: [...Array.from(this.pillIds)],
      lim: limit,
      notWantedCovers: this.rows.map(row => row.recordId),
      filterValueMap: this.state.filters,
      sortMap: this.sorting
    })
      .then(result => {
        let newRows = JSON.parse(result);
        if (newRows.length !== 0) {
          this.rows.push(...newRows);
          this.updateFilters();
          let newChecked = newRows
            .filter(row => this.initialChecked.has(row.recordId))
            .map(row => row.recordId);
          // console.log(newChecked);

          this.setState({
            checkedRows: this.state.checkedRows.concat(newChecked)
          });
        }

        if (newRows.length < limit) {
          target.enableInfiniteLoading = false;
        }

        target.isLoading = false;
        // console.log(JSON.stringify(this.state.checkedRows));
        //this.setState({loading: false});
      })
      .catch(() => {
        // console.log("error loading more");
        target.enableInfiniteLoading = false;
        target.isLoading = false;
      });
  }

  /**
   * Handles row removal when a pill is removed.
   *
   * @param {object} event - pillremove event
   */

  handlePillRemove(event) {
    //console.log('In pill remove');
    this.setState({ loading: true });
    const pillId = event.detail;
    this.pillIds.delete(pillId);

    if (this.pillIds.size === 0) {
      this.template.querySelector(
        "lightning-datatable"
      ).enableInfiniteLoading = false;
    }

    this.rows = this.rows.filter(row => {
      return row.accountId !== pillId;
    });
    this.updateFilters();
    this.setState({ loading: false });
  }

  /**
   * Handles row selection and updates checkedRows.
   *
   * @param {object} event - rowselection event
   */
  handleRowSelection(event) {
    this.updateChecked();
    //console.log('In handle row selection');
    let checkedIds = event.detail.selectedRows.map(row => row.recordId);
    //let totalIds = event.detail.earlierCheckedValues.map(row => row.recordId);
    //let finalIds = new Set([...checkedIds, ...totalIds]);
    //let earlierId = this.earlierCheckedValues;
    //checkedIds.push(...earlierId);
    // let checkedIds = new Set([]);
    // checkedIds.add(...checked.map(row => row.recordId));
    // checkedIds.add(...this.initialChecked);
    this.setState({ checkedRows: checkedIds });
  }

  /**
   * Handles the sort event and sorts the rows correspondingly.
   *
   * @param {object} event - sorting event
   */
  updateColumnSorting(event) {
    var fieldName = event.detail.fieldName;
    var sortDirection = event.detail.sortDirection;

    this.sorting.by = fieldName;
    this.sorting.direction = sortDirection;
    this.sortRows(this.sorting.by, this.sorting.direction);
    this.filteredRows = JSON.parse(JSON.stringify(this.filteredRows)); // workaround to update UI
  }

  /**
   * Sorts the rows of filteredRows.
   *
   * @param {String} fieldName        - Name of the field to sort by
   * @param {String} sortDirection    - The direction to sort, either "asc" or "desc"
   */
  sortRows(fieldName, sortDirection) {
    this.filteredRows.sort((a, b) => a[fieldName].localeCompare(b[fieldName]));
    if (sortDirection === "asc") {
      this.filteredRows.reverse();
    }
  }

  /**
   * Handles filter event and updates the filter status. Also
   *
   * @param {oject} event - filter event
   */
  handleFilters(event) {
    //console.log('In handle filters test1');
    //console.log('In handle filters test2');
    //this.earlierCheckedValues = this.state.checkedRows;
    //this.setState({ loading: true });
    let target = this.template.querySelector("lightning-datatable");
    target.enableInfiniteLoading = true;

    let filterName = event.detail.filter;
    let filterValue = event.detail.value;

    this.state.filters[filterName] = filterValue;

    this.updateFilters();

    if (this.filteredRows.length === 0) {
      this.loadMoreData(target);
    }
    this.useFilter = true;
    this.updateChecked();
    /*this.setState({
      loading: false
    });*/
  }

  /**
   * Dispatches close event.
   *
   * @fires close
   */
  handleClose() {
    this.dispatchEvent(new CustomEvent("close"));
  }

  /**
   * Saves the changes to the database without closing the modal.
   */
  handleSave() {
    this.saveWithClose(false);
  }

  /**
   * Saves the changes to the database and closes the modal.
   */
  handleSaveAndClose() {
    //console.log('In Save and CLose');
    this.saveWithClose(true);
  }

  /**
   * Saves changes to database and notifying user of success or error.
   *
   * If the close parameter is true, the method will fire a save event. If the close
   * parameter is false it will fire a ShowToastEvent to notify user that changes
   * has been made.
   *
   * @param {boolean} close - Determines if the close event should be fired or not.
   *
   * @fires save
   * @fires ShowToastEvent
   */
  saveWithClose(close) {
    let saveObjs = [];
    var saveEvent;
    var errorEvent;

    for (let i = 0; i < this.rows.length; i++) {
      let row = this.rows[i];

      let saveObj = {};

      saveObj.recordId = row.recordId;
      if (
        !this.initialChecked.has(row.recordId) &&
        this.checked.has(row.recordId)
      ) {
        //console.log('record Id in create'+row.recordId);
        saveObj.dml = "create";
        saveObjs.push(saveObj);
      } /*else if (
        this.initialChecked.has(row.recordId) &&
        !this.state.checkedRows.includes(row.recordId) &&
        this.checked == ''
      ) {
        console.log('record Id in delete1'+row.recordId);
        saveObj.dml = "delete";
        saveObjs.push(saveObj);
      }*/
      else if (
        this.initialChecked.has(row.recordId) &&
        !this.state.checkedRows.includes(row.recordId) &&
        !this.checked.has(row.recordId)
      ) {
        //console.log('record Id in delete2'+row.recordId);
        saveObj.dml = "delete";
        saveObjs.push(saveObj);
      }
    }

    let jsonOut = JSON.stringify(saveObjs);
    //console.log('jsonOut'+jsonOut);
    if (close) {
      //console.log('saveEvent'+saveEvent);
      saveEvent = new CustomEvent("save", {
        detail: {
          status: "success",
          message: "Covers were updated"
        }
      });
      //console.log('saveEvent'+saveEvent);
      errorEvent = error =>
        new CustomEvent("save", {
          detail: {
            status: "error",
            message: error.message
          }
        });
        //console.log('errorEvent'+errorEvent);
    } else {
      saveEvent = new ShowToastEvent({
        title: "Success",
        message: "Covers were updated",
        variant: "success"
      });

      errorEvent = error =>
        new ShowToastEvent({
          title: "Error",
          message: "A save error occured",
          variant: error.message
        });
    }

    updateCovers({ caseId: this.recordId, jsonString: jsonOut })
      .then(() => {
        this.dispatchEvent(saveEvent);

        if (!close) {
          // Update initialChecked
          getRelatedCovers({ caseId: this.recordId }).then(checkedCovers => {
            
            let rows = JSON.parse(checkedCovers);
            this.initialChecked = new Set([]);
            rows.forEach(row => this.initialChecked.add(row.recordId));
            
});
          //.catch(console.log("Error updating initial checked"));
        }
      })
      .catch(error => {
        this.dispatchEvent(errorEvent(error));
      });
  }

  /**
   * Updates displayed rows based on filters.
   */
  updateFilters() {
    this.filteredRows = JSON.parse(JSON.stringify(this.rows)); // Create copy

    for (const filter in this.state.filters) {
      if (this.state.filters.hasOwnProperty(filter)) {
        const filterValue = this.state.filters[filter];

        if (filter === "riskIndicator") {
          this.filteredRows = this.filteredRows.filter(
            row => row.riskIndicator === filterValue
          );
        } else if (filter === "policyYear" && filterValue) {
          this.filteredRows = this.filteredRows.filter(
            row => row.policyYear === filterValue
          );
        } else if (filter === "agreementReference" && filterValue) {
          this.filteredRows = this.filteredRows.filter(row => {
            if (row.agreementReference != null) {
              // Must be lowercase since Apex is case insensitive
              return row.agreementReference
                .toLowerCase()
                .includes(filterValue.toLowerCase());
            }
            return false;
          });
        } else if (filter === "coverName" && filterValue) {
          this.filteredRows = this.filteredRows.filter(row =>
            // Must be lowercase since Apex is case insensitive
            row.coverName.toLowerCase().includes(filterValue.toLowerCase())
          );
        } else if (filter === "objectName" && filterValue) {
          this.filteredRows = this.filteredRows.filter(row =>
            // Must be lowercase since Apex is case insensitive
            row.objectName.toLowerCase().includes(filterValue.toLowerCase())
          );
        }
      }
    }
  }

  /**
   * Ensures that the initially checked rows remains checked when changing filters etc.
   */
  updateChecked() {
    let currentSelected = new Set(
      this.template
        .querySelector("lightning-datatable")
        .getSelectedRows()
        .map(row => row.recordId)
    );
    var initialSelectedFinalValues = [];
    var initialCheckedvalues = new Set([...this.initialChecked]);  
    //console.log('inside for initialCheckedvalues array'+Array.from(initialCheckedvalues));
    for (var i=0;i<Array.from(initialCheckedvalues).length;i++){
      // console.log('inside for with Array item'+Array.from(initialCheckedvalues)[i]);
      //console.log('inside for currentSelected'+currentSelected);
      //console.log('inside for currentSelected with array'+Array.from(currentSelected));
      //console.log('inside for currentSelected with array node'+Array.from(currentSelected)[i]);
      for (var j=0;j<Array.from(currentSelected).length;j++){
        //console.log('inside for'+Array.from(currentSelected));
        if(Array.from(currentSelected)[j].includes(Array.from(initialCheckedvalues)[i])){
        //console.log('inside if'+Array.from(initialCheckedvalues));
        initialSelectedFinalValues.push(...Array.from(initialCheckedvalues)[i]);
      }
    }
    }
    //console.log('inside for initialSelectedFinalValues'+initialSelectedFinalValues);
    this.earlierCheckedValues.push(...currentSelected);
    //console.log("this.earlierCheckedValues : " + this.earlierCheckedValues);
    if( this.useFilter ==  true){
      //console.log('inside usefilter if');
      this.checked = new Set([...this.earlierCheckedValues, ...initialSelectedFinalValues]);
    }
    else{
      //console.log('inside usefilter else');
      this.checked = new Set([...currentSelected]);
    }
    //console.log('checked rows'+this.checked) ;
    this.setState({ checkedRows: Array.from(this.checked) });
    //console.log('checked rows'+Array.from(this.checked) ) ;
  }

  /**
   * Sets the state and updating related states
   *
   * @param {object} state    - An object where the property is a property of the
   *                            tracked this.state and value is the value to be assigned
   */
  setState(state) {
    for (const key in state) {
      if (this.state.hasOwnProperty(key)) {
        this.state[key] = state[key];
      }
    }
    this.updateState();
  }

  /**
   * Updates other properties based on the new state set.
   *
   * @return undefined.
   */
  updateState() {
    return undefined;
  }

  /**
   * navigates to case record
   *
   */
  handleNavigate(){
    let sfdcBaseURL = window.location.origin;
    console.log('sfdcBaseURL after modification----'+sfdcBaseURL);
    console.log('record id---'+this.recordId);
    window.open(sfdcBaseURL+'/'+this.recordId);
    
  }
}