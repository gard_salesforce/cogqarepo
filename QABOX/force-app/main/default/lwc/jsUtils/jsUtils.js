/**
 * Checks through array if there is an element which matches the comparing function.
 * 
 * @param {Array} array         - The array to search through
 * @param {Object} element      - The object to check against array contents
 * @param {Function} comparer   - The comparing function
 */
const compareInArray = function(array, element, comparer) {
  for (let i = 0; i < array.length; i++) {
    if (comparer(array[i], element)) {
      return true;
    } 
  }
  return false;
}

const isEmpty = function(str) {
  return (!str || 0 === str.length);
}


export {compareInArray, isEmpty};