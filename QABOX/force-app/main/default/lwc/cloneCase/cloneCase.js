/**
 * Created by ohuuse on 30/10/2019.
 */

import {LightningElement, api, track, wire} from 'lwc';
import cloneIt from '@salesforce/apex/CaseCloneController.cloneCaseWithRelatedRecords';
import {ShowToastEvent} from 'lightning/platformShowToastEvent'; // import toast message event .
import {NavigationMixin} from 'lightning/navigation';

export default class CloneCase extends NavigationMixin(LightningElement) {
    @api recordId;
    @track newCaseId;
    @track error;
    @track toggleModal = false;

    handleClose() {
        this.toggleModal = false;
    }

    handleClick() {
        this.toggleModal = true;
        cloneIt({caseId: this.recordId})
            .then(result => {
                this.newCaseId = result;
                this.error = undefined;
                this.toggleModal = false;
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Cloning successfull',
                        message: 'Cloned Case now Loaded',
                        variant: 'success',
                    }),
                    this[NavigationMixin.Navigate]({
                        type: 'standard__recordPage',
                        attributes: {
                            recordId: result,
                            objectApiName: 'Case',
                            actionName: 'view'
                        }
                    }),
                );
                // console.log(JSON.stringify(result));
            })
            .catch(error => {
                this.error = error;
                this.toggleModal = false;
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error while cloning Case',
                        message: error.message,
                        variant: 'error',
                    }),
                );
            });
    }


}