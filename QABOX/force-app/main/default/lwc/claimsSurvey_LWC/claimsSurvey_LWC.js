/* eslint-disable default-case */
import { LightningElement, api, track } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
// import { isEmpty } from "c/jsUtils";

/**
 * Columns in the data table.
 */
const columns = [
  {
    label: "Name",
    fieldName: "contactName", //"id",
    type: "text"
  },
  {
    label: "Email",
    fieldName: "contactEmail",  //"label",
    type: "text"
  }
];

/**
 * Contains the functions and properties for the ListBuilderModal.
 *
 * @constructs LightningElement
 */
export default class ListBuilderModal extends LightningElement {
  @api recordId;
  @api loadLimit;
  rows = [];
  @track filteredRows = [];
  @track columns = columns;
  @track initialRender = true;
  checked = [];

  useFilter = false;

  /**
   * State tracking object.
   *
   * @property {list} checkedRows                   - The rows currently checked
   * @property {boolean} loading                    - If the view is loading records from backend
   * @property {object} filters                     - The filters currently applied
   * @property {boolean} filters.riskIndicator      - riskIndicator filter
   * @property {Integer} filters.policyYear         - The policy year to filter rows by
   * @property {String} filters.agreementReference  - The string to filter agreement reference by
   */
  @track state = {
    checkedRows: [],
    loading: false,
    filters: {
      riskIndicator: true,
      policyYear: null,
      agreementReference: null,
      coverName: null,
      objectName: null
    }
  };

  // Keeps track of Contact Ids to load records selected from the dropdwon or /deleted
  @track pillMap = new Map([]);
 
  /**
   * Loads related covers on initial render.
   *
   */
  renderedCallback() {
    
  }

  /**
   * Handles record adding when a pill is added/tile is clicked.
   *
   * @param {object} event - pilladd event
   */
  handlePillAdd(event) {
    let target = this.template.querySelector("lightning-datatable");
    target.isLoading = true;
    this.setState({ loading: true });
    const pillId = event.detail;
    this.pillMap.set(pillId.tileId, pillId);
    this.updateFilters();
    this.setState({ loading: false });
    target.isLoading = false;
  }
  
  /**
   * Handles row removal when a pill is removed.
   *
   * @param {object} event - pillremove event
   */

  handlePillRemove(event) {
    this.setState({ loading: true });
    const pillId = event.detail;
    this.pillMap.delete(pillId);
    this.updateFilters();
    this.setState({ loading: false });
  }

  updateFilters() {
    let tableValues = [];
    this.pillMap.forEach((value, key) => {
      tableValues.push(value);
    });
    
    this.filteredRows = JSON.parse(JSON.stringify(tableValues));
    console.log("filtered rows:"+this.filteredRows);
  }
  /**
   * Dispatches close event.
   *
   * @fires close
   */
  handleClose() {
    this.dispatchEvent(new CustomEvent("close"));
  }

  /**
   * Saves the changes to the database without closing the modal.
   */
  handleSave() {
    this.saveWithClose(false);
  }

  /**
   * Saves the changes to the database and closes the modal.
   */
  handleSaveAndClose() {
    //console.log('In Save and CLose');
    this.saveWithClose(true);
  }

  /**
   * Saves changes to database and notifying user of success or error.
   *
   * If the close parameter is true, the method will fire a save event. If the close
   * parameter is false it will fire a ShowToastEvent to notify user that changes
   * has been made.
   *
   * @param {boolean} close - Determines if the close event should be fired or not.
   *
   * @fires save
   * @fires ShowToastEvent
   */
  saveWithClose(close) {
    let saveObjs = [];
    var saveEvent;
    var errorEvent;

    for (let i = 0; i < this.rows.length; i++) {
      let row = this.rows[i];

      let saveObj = {};

      saveObj.recordId = row.recordId;
      if (
        !this.initialChecked.has(row.recordId) &&
        this.checked.has(row.recordId)
      ) {
        //console.log('record Id in create'+row.recordId);
        saveObj.dml = "create";
        saveObjs.push(saveObj);
      } /*else if (
        this.initialChecked.has(row.recordId) &&
        !this.state.checkedRows.includes(row.recordId) &&
        this.checked == ''
      ) {
        console.log('record Id in delete1'+row.recordId);
        saveObj.dml = "delete";
        saveObjs.push(saveObj);
      }*/
      else if (
        this.initialChecked.has(row.recordId) &&
        !this.state.checkedRows.includes(row.recordId) &&
        !this.checked.has(row.recordId)
      ) {
        //console.log('record Id in delete2'+row.recordId);
        saveObj.dml = "delete";
        saveObjs.push(saveObj);
      }
    }

    let jsonOut = JSON.stringify(saveObjs);
    //console.log('jsonOut'+jsonOut);
    if (close) {
      //console.log('saveEvent'+saveEvent);
      saveEvent = new CustomEvent("save", {
        detail: {
          status: "success",
          message: "Covers were updated"
        }
      });
      //console.log('saveEvent'+saveEvent);
      errorEvent = error =>
        new CustomEvent("save", {
          detail: {
            status: "error",
            message: error.message
          }
        });
        //console.log('errorEvent'+errorEvent);
    } else {
      saveEvent = new ShowToastEvent({
        title: "Success",
        message: "Covers were updated",
        variant: "success"
      });

      errorEvent = error =>
        new ShowToastEvent({
          title: "Error",
          message: "A save error occured",
          variant: error.message
        });
    }

    updateCovers({ caseId: this.recordId, jsonString: jsonOut })
      .then(() => {
        this.dispatchEvent(saveEvent);

        if (!close) {
          // Update initialChecked
          getRelatedCovers({ caseId: this.recordId }).then(checkedCovers => {
            
            let rows = JSON.parse(checkedCovers);
            this.initialChecked = new Set([]);
            rows.forEach(row => this.initialChecked.add(row.recordId));
            
});
          //.catch(console.log("Error updating initial checked"));
        }
      })
      .catch(error => {
        this.dispatchEvent(errorEvent(error));
      });
  }

  /**
   * Sets the state and updating related states
   *
   * @param {object} state    - An object where the property is a property of the
   *                            tracked this.state and value is the value to be assigned
   */
  setState(state) {
    for (const key in state) {
      if (this.state.hasOwnProperty(key)) {
        this.state[key] = state[key];
      }
    }
    this.updateState();
  }

  /**
   * Updates other properties based on the new state set.
   *
   * @return undefined.
   */
  updateState() {
    return undefined;
  }

  /**
   * navigates to case record
   *
   */
  handleNavigate(){
    let sfdcBaseURL = window.location.origin;
    console.log('sfdcBaseURL after modification----'+sfdcBaseURL);
    console.log('record id---'+this.recordId);
    window.open(sfdcBaseURL+'/'+this.recordId);
    
  }
}