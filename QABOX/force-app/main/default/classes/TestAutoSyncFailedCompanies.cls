@isTest(SeeAllData = True)
Public Class TestAutoSyncFailedCompanies{
    
    static testMethod void autoSyncMethod(){
    GardTestData testData = new GardTestData();
    testData.commonRecord();
    /*
    Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
    insert Markt;  
     
    Gard_Contacts__c grdobj  =    new  Gard_Contacts__c(
                                            FirstName__c = 'Testreqchng',
                                            LastName__c = 'Baann',
                                            //Company_Type__c = 'Grd', 
                                            Email__c = 'WR_gards.12@Test.com',
                                            //MailingCity__c = 'Arendal', 
                                            //MailingCountry__c = 'Norway', 
                                           // MailingPostalCode__c =  '487155',
                                           // MailingState__c = 'Oslo', 
                                           // MailingStreet__c = 'Rd Road', 
                                            //Mobile__c = '98323322', 
                                            MobilePhone__c = '548645', 
                                            Nick_Name__c  = 'NilsPeter', 
                                            Office_city__c = 'Arendal', 
                                            Phone__c = '5454454',
                                            Portal_Image__c ='<img alt="User-added image" src="https://c.cs8.content.force.com/servlet/rtaImage?eid=a1hL0000000kSyZ&amp;feoid=00NL0000003OySC&amp;refid=0EML00000008Zjs"></img>', 
                                            Title__c = GardTestData.testStr 
                                           );
      insert grdobj ;
    
      User salesforceLicUser = new User(
                                    Alias = GardTestData.standtStr , 
                                    profileId = GardTestData.salesforceLicenseId ,
                                    Email = GardTestData.stdUsrMailStr ,
                                    EmailEncodingKey = GardTestData.utfStr ,
                                    CommunityNickname = GardTestData.test13Str ,
                                    LastName = GardTestData.testingStr ,
                                    LanguageLocaleKey= GardTestData.enUsrStr ,
                                    LocaleSidKey= GardTestData.enUsrStr , 
                                    //contactID = brokercontact.id, 
                                    TimeZoneSidKey= GardTestData.tymZnStr ,
                                    UserName='mygardtest008@testorg.com.mygard',
                                    contactId__c = grdobj.id
                               );
     insert salesforceLicUser;
     
    
    
     Account brokerAccAutoSync = new Account(   Name='testAutoSync',
                                BillingCity = 'Southampton',
                                BillingCountry =  GardTestData.bilCntryStr ,
                                BillingPostalCode = 'BS2 AD!',
                                BillingState = 'Avon LAng' ,
                                BillingStreet = '1 Mangrove Road',
                                recordTypeId=System.Label.Broker_Contact_Record_Type,
                                Site = '_www.tcs.se',
                                Type =  GardTestData.typeBrStr ,
                                company_Role__c =  GardTestData.typeBrStr ,
                                //Company_ID__c = '64143',
                                guid__c = '8ce8ad89-a6ed-1836-9e17',
                                Market_Area__c = Markt.id,
                                Product_Area_UWR_2__c= GardTestData.piStr ,
                                Product_Area_UWR_4__c= GardTestData.piStr ,
                                Product_Area_UWR_3__c= GardTestData.marineStr ,
                                Claim_handler_Marine_2_lk__c = salesforceLicUser.id ,
                                Claim_handler_Cargo_Liquid__c = salesforceLicUser.id ,
                                Role_Broker__c = true,
                                Claim_handler_Charterers__c = salesforceLicUser.id ,
                                Claim_handler_Defence__c = salesforceLicUser.id ,
                                Claim_handler_Energy__c = salesforceLicUser.id ,
                                Claim_handler_Energy_2_lk__c= salesforceLicUser.id ,
                                Claims_handler_Builders_Risk_2_lk__c= salesforceLicUser.id ,
                                Claim_handler_Charterers_2_lk__c = salesforceLicUser.id ,
                                Claim_handler_Cargo_Liquid_2_lk__c = salesforceLicUser.id ,
                                Claims_handler_Builders_Risk__c = salesforceLicUser.id ,
                                Company_Role_Text__c = 'Broker',
                                //Claim_handler_Charterers__c = salesforceLicUser.id ,
                                Area_Manager__c = salesforceLicUser.id ,
                                X2nd_UWR__c=salesforceLicUser.id,
                                X3rd_UWR__c=salesforceLicUser.id,
                                Underwriter_4__c=salesforceLicUser.id,
                                Underwriter_main_contact__c=salesforceLicUser.id,
                                UW_Assistant_1__c=salesforceLicUser.id,
                                U_W_Assistant_2__c=salesforceLicUser.id,
                                Claim_handler_Crew__c = salesforceLicUser.id,
                                Enable_share_data__c = true,
                                Company_Status__c='Active',
                                GIC_Office_ID__c = null,
                                Key_Claims_Contact__c = salesforceLicUser.id,
                                Claim_Adjuster_Marine_lk__c = salesforceLicUser.id,
                                Claim_handler_Cargo_Dry__c = salesforceLicUser.id,
                                Claim_handler_CEP__c = salesforceLicUser.id,
                                Claim_handler_Marine__c = salesforceLicUser.id,
                                Claim_handler_Cargo_Dry_2_lk__c = salesforceLicUser.id,
                                Claim_handler_CEP_2_lk__c = salesforceLicUser.id,
                                Claim_handler_Crew_2_lk__c =  salesforceLicUser.id,
                                Claim_handler_Defence_2_lk__c = salesforceLicUser.id,
                                Accounting_P_I__c = salesforceLicUser.id,
                                Accounting__c = salesforceLicUser.id,
                                
                                Confirm_not_on_sanction_lists__c = true,
                                License_description__c  = 'some desc',
                                Description = 'some desc',
                                Licensed__c = 'Pending'

                                //Owner = salesforceLicUser
                                //Ownerid=salesforceLicUser.id
                                                                                                  
                           );
    insert brokerAccAutoSync;
    */
        system.runAs(GardTestData.brokerUser) {
            test.starttest();
            AutoSyncFailedCompanies ASfailedC = new AutoSyncFailedCompanies();
            ASfailedC.syncFailedCompanies();
            test.stoptest();
        }
    }
}