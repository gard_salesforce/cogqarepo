public with sharing class PrognosisConfigSupport {
	private static Prognosis_Config__c testconfig = null;
	
	public static Prognosis_Config__c GetSetting(String UserId){
		if(Test.isRunningTest() && testconfig != null ){return testconfig;			
		}else{
			Prognosis_Config__c theobject = Prognosis_Config__c.getInstance(UserId);
			
			if(theobject==null || Test.isRunningTest()){
				theobject = new Prognosis_Config__c();
				theobject.Enable_Diagnostics__c = Test.isRunningTest();
				theobject.Enable_User_History__c = true;
				theobject.Prognosis_Year__c = '2014';
			}
			return theobject;
		}
	}
	
	public static Prognosis_Config__c GetSetting(){
		if(Test.isRunningTest() && testconfig != null ){return testconfig;			
		}else{
			Prognosis_Config__c theobject = Prognosis_Config__c.getInstance();
			
			if(theobject==null || Test.isRunningTest()){
				theobject = new Prognosis_Config__c();
				theobject.Enable_Diagnostics__c = Test.isRunningTest();
				theobject.Enable_User_History__c = true;
				theobject.Prognosis_Year__c = '2014';
			}
			return theobject;
		}		
	} 
}