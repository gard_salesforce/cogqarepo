@isTest
public class TestMailUtil {
	public static Account clientAcc;
	public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
	public static Market_Area__c Markt;
	public static User salesforceLicUser;
	public static Contact clientContact;
	
	private static void createUsers(){
		salesforceLicUser = new User(
                                Alias = 'standt', 
                                profileId = salesforceLicenseId ,
                                Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8',
                                CommunityNickname = 'test13',
                                LastName='Testing',
                                LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US',  
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName='test008@testorg.com'
                                     );
        
        insert salesforceLicUser; 
	}
	
	private static void createClientCompany(){
		Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt; 
        createUsers();        
        
        clientAcc = new Account( Name = 'Test_1', 
                                Site = '_www.test_1.se', 
                                Type = 'Client', 
                                BillingStreet = 'Gatan 1',
                                BillingCity = 'Stockholm', 
                                BillingCountry = 'SWE', 
                                BillingPostalCode = 'BS1 1AD',
                                recordTypeId=System.Label.Client_Contact_Record_Type,
                                Company_Role_Text__c='Client',
                                Market_Area__c = Markt.id,
                                Area_Manager__c = salesforceLicUser.id,
                                OwnerId = salesforceLicUser.id
                               );
        insert clientAcc;
	}
	
	private static void createClientContact(){
		createClientCompany();
		clientContact= new Contact( FirstName='primary_1',
                              LastName='chak',
                              MailingCity = 'London',
                              MailingCountry = 'United Kingdom',
                              MailingPostalCode = 'SE1 1AE',
                              MailingState = 'London',
                              MailingStreet = '4 London Road',
                              AccountId = clientAcc.Id,
                              Email = 'test321@gmail.com',
                              Primary_Contact__c = true,
                              Publication_Member_circulars__c=false,
                              Publications_Gard_Rules__c=false,
                              Publication_Guidance_to_Master__c=false
                               ); 
	}
	
	static testMethod void testAccountSyncFailureMail(){
		TestMailUtil.createClientCompany();
		List<Account> listOfAccounts = new List<Account>();
		listOfAccounts.add(clientAcc);
		
		Messaging.SendEmailResult[] emailResult = MailUtil.sendAccountSyncFailureMail(listOfAccounts);
		
		System.assert(emailResult.get(0).isSuccess());
	}
	
	static testMethod void testContactSyncFailureMail(){
		TestMailUtil.createClientContact();
		List<Contact> listOfContacts = new List<Contact>();
		listOfContacts.add(clientContact);
		
		Messaging.SendEmailResult[] emailResult = MailUtil.sendContactSyncFailureMail(listOfContacts);
		
		System.assert(emailResult.get(0).isSuccess());
	}
}