@IsTest
public class TestAsyncWsdlGardNoV10{
    
    private static testMethod void TestAsyncexecute_pt(){
        new AsyncWsdlGardNoV10Reportsservice.Asyncexecute_pt();
    }
    
    private static testMethod void TestbeginExecute(){
         new AsyncWsdlGardNoV10Reportsservice.Asyncexecute_pt().beginExecute(null,null,null,null,null,null,null);
    }
    
    private static testMethod void TestbeginExecuteMarine(){
         new AsyncWsdlGardNoV10Reportsservice.Asyncexecute_pt().beginExecuteMarine(null,null,null,null,null,null);
    }
    private static testMethod void TestAsyncClaimReceiverPort(){
        new AsyncMessagesGardNoV10Claimreceiver.AsyncClaimReceiverPort();
    }
    private static testMethod void TestbeginCreateClaimRequest(){
         new AsyncMessagesGardNoV10Claimreceiver.AsyncClaimReceiverPort().beginCreateClaimRequest(null,null,null,null);
    }
    private static testMethod void TestbeginUpdateClaimRequest(){
        new AsyncMessagesGardNoV10Claimreceiver.AsyncClaimReceiverPort().beginUpdateClaimRequest(null,null,null,null);
    }
     private static testMethod void testGetDocumentResponse_elementFutureGetValue(){
        System.assertEquals('Test Response', new AsyncMessagesGardNoV10Contractlocpreview.ContractLOCPreviewResponseBaseFuture().getValue());
    } 
    private static testMethod void testContractLOConfirmationResponseBaseFuture(){
        new AsyncMessagesGardNoV10Contractloconfirma.ContractLOConfirmationResponseBaseFuture().getValue();
    } 
    private static testMethod void testContractReassignResponseBaseFuture (){
        new AsyncMessagesGardNoV10Contractreassignre.ContractReassignResponseBaseFuture().getValue();
    } 
    private static testMethod void TestAsyncContractReviewPort(){
        new AsyncMessagesGardNoV10Contractreviewrece.AsyncContractReviewPort();
    } 
   private static testMethod void TestContractLOConfirmationResponse(){
        new AsyncMessagesGardNoV10Contractreviewrece.AsyncContractReviewPort().beginContractLOConfirmationRequest(null,null);
    } 
    private static testMethod void TestLOCSendEmailRequest(){
        new AsyncMessagesGardNoV10Contractreviewrece.AsyncContractReviewPort().beginLOCSendEmailRequest(null,null);
    } 
    private static testMethod void testContractreviewresp(){
        new AsyncMessagesGardNoV10Contractreviewresp.ContractReviewResponseBaseFuture().getValue();
    } 
    private static testMethod void testLOCSendEmailResponse(){
        new AsyncMessagesGardNoV10Locsendemailrespon.LOCSendEmailResponseBaseFuture().getValue();
    }
    private static testMethod void TestContractLOCPreviewRequest(){
        new AsyncMessagesGardNoV10Contractreviewrece.AsyncContractReviewPort().beginContractLOCPreviewRequest(null,null);
    } 
     private static testMethod void testGetDocumentResponse_elementFuture(){
        System.assertEquals('Test Response', new AsyncMessagesGardNoV10Reportresponse.ReportResponseBaseFuture().getValue());
    }
    private static testMethod void testgetReportResponse_element(){
       new servicesGardNoMygardreports_SH.getReportResponse_element();
    }
    private static testMethod void testgetReportWithParamsResponse_element(){
       new servicesGardNoMygardreports_SH.getReportWithParamsResponse_element();
    }
    private static testMethod void testgetReport_element(){
       new servicesGardNoMygardreports_SH.getReport_element();
    }
    private static testMethod void testgetReportWithParams_element(){
       new servicesGardNoMygardreports_SH.getReportWithParams_element();
    }
    private static testMethod void testgetReportWithParams(){
       new servicesGardNoMygardreports_SH.BasicHttpBinding_ImyGardReportsQSPort().getReportWithParams(null,null);
    }
    private static testMethod void testgetReport(){
       new servicesGardNoMygardreports_SH.BasicHttpBinding_ImyGardReportsQSPort().getReport(null);
    }
    private static testMethod void testContractReviewRequestBase(){
       new messagesGardNoV10Contractreviewrece.ContractReviewRequestBase();
    }
    private static testMethod void testContactPreference_element(){
       new messagesGardNoV10Contractreview.ContactPreference_element();
    }
     private static testMethod void testClientType(){
       new messagesGardNoV10Contractreview.ClientType();
    }
    private static testMethod void testCoverType(){
       new messagesGardNoV10Contractreview.CoverType();
    }
    private static testMethod void testContractReviewBase(){
       new messagesGardNoV10Contractreview.ContractReviewBase();
    }
    private static testMethod void testClaimTitleType(){
       new messagesGardNoV10Contractreview.ClaimTitleType();
    }
    private static testMethod void ContractLOCPreviewRequest(){
       new AsyncMessagesGardNoV10Contractreviewrece.AsyncContractReviewPort().beginContractReviewRequest(null,null);
    }
    private static testMethod void testClaimReferenceType(){
       new messagesGardNoV10Contractreview.ClaimReferenceType();
    } 
    private static testMethod void testClaimReceiverRequestBase(){
       new messagesGardNoV10Claimreceiver.ClaimReceiverRequestBase();
    }
  /*  private static testMethod void Testprocess(){
         new xmlnsOracleComContractreviewreceivera.ReassignContractBPEL_pt().process(null,null);
    } */
  /*  private static testMethod void testClaimReceiverPort(){
       new messagesGardNoV10Claimreceiver.ClaimReceiverPort();
    }
    private static testMethod void testCreateClaimRequest(){
       new messagesGardNoV10Claimreceiver.ClaimReceiverPort().CreateClaimRequest(null,null,null);
    } 
    private static testMethod void testUpdateClaimRequest(){
       new messagesGardNoV10Claimreceiver.ClaimReceiverPort().UpdateClaimRequest(null,null,null);
    } */
}