//This test class is for BlueCardReqCtrl
    
    @isTest(seeAllData=false)
    public class TestBlueCardReqCtrl{
            private static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
            private static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
            private static Organization_Wide_Email__c cV;
            private static GardTestData testRec;
            private static ClientActivityEvents__c clientActs;
        
        private static void createTestData(){
            //Creating Test Data
            conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
            conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
            conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
            conFieldsList.add(conSubFields);
            conFieldsList.add(conSubFields2);
            conFieldsList.add(conSubFields3); 
            insert conFieldsList; 
            
            ClientActivityEvents__c clientActivityEvent1 = new ClientActivityEvents__c(name = 'Request new Blue Card',Event_Type__c='Request new Blue Card');
            insert clientActivityEvent1;
            //ClientActivityEvents__c clientActivityEvent2 = new ClientActivityEvents__c(name = 'Request renewal of Blue Card',Event_Type__c='Request renewal of Blue Card');
            //insert clientActivityEvent2; 
            
            testRec = new GardTestData();
            testRec.commonrecord();  
            testRec.customsettings_rec();
             cV = new Organization_Wide_Email__c(name = 'test', value__c ='no-reply@gard.no');
            insert cV;
            clientActs = new ClientActivityEvents__c(name = 'Request renewal of Blue Card',Event_Type__c='Renewal of Blue Card');
            insert clientActs;
        }
    	//Test Method 1 
        private static testmethod void BlueCardReqCtrl1(){
            /*
            //Creating Test Data
            conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fieldsabc' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
            conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields pqr' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
            conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields xyz' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
            conFieldsList.add(conSubFields);
            conFieldsList.add(conSubFields2);
            conFieldsList.add(conSubFields3);
            insert conFieldsList; 
            ClientActivityEvents__c clientActs = new ClientActivityEvents__c(name = 'Request renewal of Blue Card',Event_Type__c='Renewal of Blue Card');
            insert clientActs;
            GardTestData testRec = new GardTestData();
            testRec.commonrecord();  
            testRec.customsettings_rec();
            Organization_Wide_Email__c cV = new Organization_Wide_Email__c(name = 'test', value__c ='no-reply@gard.no');
            insert cV;
           */
            createTestData();
            //Invoking methods of BlueCardReqCtrl for broker client users....
            System.runAs(GardTestData.brokerUser){ 
                Test.startTest();
                List<EmailTemplate> Ack_TempId = new List<EmailTemplate>([SELECT Id, body, subject FROM EmailTemplate WHERE Name ='MyGard-Request Blue Card for new object-Acknowledgement']);
                BlueCardReqCtrl testBroker=new BlueCardReqCtrl();
                // List<SelectOption> selOpts=testBroker.blueCardOption;
                List<SelectOption> flag=testBroker.getFlagOption();
                List<SelectOption> port=testBroker.getPort();
                //List<SelectOption> objOp=testBroker.objectOption;
                //System.assertEquals(selOpts.size()>0,true,true);
                System.assertEquals(flag.size()>0,true,true);
                System.assertEquals(port.size()>0,true,true);
                //System.assertEquals(objOp.size()>0,true,true);
                testBroker.regOwnerChng=true;
                testBroker.flagChng=true;
                testBroker.flag='Norway';
                testBroker.callSign='NewSign';
                testBroker.vesselNameChng=true;
                testBroker.vesselName='NewVessel';
                testBroker.dateEffective = '12-12-2014';
                testBroker.regOwnerName='testname';
                testBroker.comments='This is test';
                testBroker.objMode=true;
                testBroker.selectedPort='Arendal';
                String objId = GardTestData.test_object_1st.id;
                testBroker.prvUrl='https://cs8.salesforce.com/setup/build/viewApexClass.apexp?objid='+objId+'';
                testBroker.ursFaithfully='Test';
                testBroker.LOUConfirmation=true;
              
                testBroker.populateClient();
                testBroker.selectedObj=new List<String>();
                testBroker.selectedObj.add(GardTestData.test_object_1st.id);
                testBroker.selectedObj.add(GardTestData.test_object_2nd.id);
                testBroker.selectedObj.add(GardTestData.test_object_3rd.id);
                System.assertEquals(testBroker.selectedObj.size()>0,true,true);
                testBroker.SelectedClient=GardTestData.brokerContact.AccountId; 
                testBroker.loggedInAccId=GardTestData.brokerContact.AccountId;
                testBroker.selectedBlueCardType='Request new Blue Card';
                testBroker.saveCase();
                Case singleCase = [SELECT ID FROM CASE WHERE Claim_Reference_Number__c='kol23232' LIMIT 1];
                testBroker.caseTestId = singleCase.Id;
                testBroker.submit();
                //testBroker.resetChkboxFields();
                testBroker.selectedClient=GardTestData.brokerAcc.id;
                testBroker.emailBody = '[HEADER]Thank you for contacting Gard. Your request renewal of Blue Card has been successfully registered.'+              
                'Below is a summary of the details submitted on MyGard:Client: [Client]Objects: [Objects]Date Effective: [DATE]Comments: [COMMENTS]You have also acknowledged that you have read and agreed to the terms and conditions.'+
                'Preferred method of contact:'+
                'Phone: [PHONEPREF]'+
                'Email: [EMAILPREF]'+
                'If you have any queries, please contact us at mygard.support@gard.no.'+
                '[SIGNATURE]'+
                '[FOOTER]';
                testBroker.dt = datetime.newInstance(1960, 02,17);
                System.assertNotEquals(null,testBroker.selectedClient);
                //testBroker.sendMails();
                testBroker.sendType();
                testBroker.prvUrl = 'https://cs8.salesforce.com/setup/build/viewApexClass.apexp?objid=MyObjectDetails';
                testBroker.selectedBlueCardType='Request renewal of Blue Card';
                
                Test.stopTest(); 
                //testBroker.submit();
                //testBroker.selectedBlueCardType='New Blue Card';
                //testBroker.submit();
                //testBroker.sendType();
                testBroker.regOwnerChng=false;
                testBroker.vesselNameChng=false;
                testBroker.flagChng=false;
                testBroker.gTonnage='test';
                testBroker.disableBCChange=true;
                testBroker.requiredBCChange=true;
                testBroker.checkSelectedBlueCard();
                testBroker.resetChkboxFields();
                testBroker.resetGTBox();
                //testBroker.objectMode();
                testBroker.cancel();
                
               // testBroker.resetGTBox();
                testBroker.resetGTField();
                
            } 
        }
        //Test Method 2 
        private static testmethod void BlueCardReqCtrl2(){
            createTestData();
           
           /* System.runAs(GardTestData.clientUser){
                BlueCardReqCtrl test_client=new BlueCardReqCtrl();
                test_client.objMode=true;
                test_client.selectedObj=new List<String>();
                test_client.selectedObj.add(GardTestData.test_object_1st.id);
                test_client.selectedObj.add(GardTestData.test_object_2nd.id);
                test_client.selectedObj.add(GardTestData.test_object_3rd.id);
                test_client.objectMode();
            }*/
            //Invoking methods of BlueCardReqCtrl for broker client users....
            System.runAs(GardTestData.brokerUser){ 
                Test.startTest();
                List<EmailTemplate> Ack_TempId = new List<EmailTemplate>([SELECT Id, body, subject FROM EmailTemplate WHERE Name ='MyGard-Request Blue Card for new object-Acknowledgement']);
                BlueCardReqCtrl testBroker=new BlueCardReqCtrl();
                // List<SelectOption> selOpts=testBroker.blueCardOption;
                List<SelectOption> flag=testBroker.getFlagOption();
                List<SelectOption> port=testBroker.getPort();
                //List<SelectOption> objOp=testBroker.objectOption;
                //System.assertEquals(selOpts.size()>0,true,true);
                System.assertEquals(flag.size()>0,true,true);
                System.assertEquals(port.size()>0,true,true);
                //System.assertEquals(objOp.size()>0,true,true);
                testBroker.regOwnerChng=true;
                testBroker.flagChng=true;
                testBroker.flag='Norway';
                testBroker.callSign='NewSign';
                testBroker.vesselNameChng=true;
                testBroker.vesselName='NewVessel';
                testBroker.dateEffective = '12-12-2014';
                testBroker.regOwnerName='testname';
                testBroker.comments='This is test';
                testBroker.objMode=true;
                testBroker.selectedPort='Arendal';
                String objId = GardTestData.test_object_1st.id;
                testBroker.prvUrl='https://cs8.salesforce.com/setup/build/viewApexClass.apexp?objid='+objId+'';
                testBroker.ursFaithfully='Test';
                testBroker.LOUConfirmation=true;
              
                testBroker.populateClient();
                testBroker.selectedObj=new List<String>();
                testBroker.selectedObj.add(GardTestData.test_object_1st.id);
                testBroker.selectedObj.add(GardTestData.test_object_2nd.id);
                testBroker.selectedObj.add(GardTestData.test_object_3rd.id);
                System.assertEquals(testBroker.selectedObj.size()>0,true,true);
                testBroker.SelectedClient=GardTestData.brokerContact.AccountId; 
                testBroker.loggedInAccId=GardTestData.brokerContact.AccountId;
                //testBroker.selectedBlueCardType='Request new Blue Card';
                testBroker.selectedBlueCardType='New Blue Card';
                /*
                testBroker.objLst.add(new testBroker.RequestObjectChange(GardTestData.ObjectList[0].id,GardTestData.ObjectList[0].Flag__c,GardTestData.ObjectList[0].Port__c,GardTestData.ObjectList[0].Imo_Lloyds_No__c,GardTestData.ObjectList[0].Signal_Letters_Call_sign__c,GardTestData.ObjectList[0].Name,String.valueOf(GardTestData.ObjectList[0].Gross_tonnage__c),'newRegdOwnerName','newRegdOwnerAdd'));
                testBroker.objLst.add(new testBroker.RequestObjectChange(GardTestData.ObjectList[1].id,GardTestData.ObjectList[1].Flag__c,GardTestData.ObjectList[1].Port__c,GardTestData.ObjectList[1].Imo_Lloyds_No__c,GardTestData.ObjectList[1].Signal_Letters_Call_sign__c,GardTestData.ObjectList[1].Name,String.valueOf(GardTestData.ObjectList[1].Gross_tonnage__c),'newRegdOwnerName','newRegdOwnerAdd'));
                testBroker.objLst.add(new testBroker.RequestObjectChange(GardTestData.ObjectList[2].id,GardTestData.ObjectList[2].Flag__c,GardTestData.ObjectList[2].Port__c,GardTestData.ObjectList[2].Imo_Lloyds_No__c,GardTestData.ObjectList[2].Signal_Letters_Call_sign__c,GardTestData.ObjectList[2].Name,String.valueOf(GardTestData.ObjectList[2].Gross_tonnage__c),'newRegdOwnerName','newRegdOwnerAdd'));
                testBroker.objLst.add(new testBroker.RequestObjectChange(GardTestData.ObjectList[3].id,GardTestData.ObjectList[3].Flag__c,GardTestData.ObjectList[3].Port__c,GardTestData.ObjectList[3].Imo_Lloyds_No__c,GardTestData.ObjectList[3].Signal_Letters_Call_sign__c,GardTestData.ObjectList[3].Name,String.valueOf(GardTestData.ObjectList[3].Gross_tonnage__c),'newRegdOwnerName','newRegdOwnerAdd'));
                */
                testBroker.saveCase();
                Case singleCase = [SELECT ID FROM CASE WHERE Claim_Reference_Number__c='kol23232' LIMIT 1];
                testBroker.caseTestId = singleCase.Id;
                testBroker.submit();
                //testBroker.resetChkboxFields();
                
            } 
            Test.stopTest(); 
        }
        
        private static testmethod void BlueCardReqCtrl3(){
            createTestData();
            System.runAs(GardTestData.brokerUser){
                Test.startTest();
                List<EmailTemplate> Ack_TempId = new List<EmailTemplate>([SELECT Id, body, subject FROM EmailTemplate WHERE Name ='MyGard-Request Blue Card for new object-Acknowledgement']);
                BlueCardReqCtrl testBroker=new BlueCardReqCtrl();
                // List<SelectOption> selOpts=testBroker.blueCardOption;
                List<SelectOption> flag=testBroker.getFlagOption();
                List<SelectOption> port=testBroker.getPort();
                //List<SelectOption> objOp=testBroker.objectOption;
                //System.assertEquals(selOpts.size()>0,true,true);
                System.assertEquals(flag.size()>0,true,true);
                System.assertEquals(port.size()>0,true,true);
                //System.assertEquals(objOp.size()>0,true,true);
                testBroker.regOwnerChng=true;
                testBroker.flagChng=true;
                testBroker.flag='Norway';
                testBroker.callSign='NewSign';
                testBroker.vesselNameChng=true;
                testBroker.vesselName='NewVessel';
                testBroker.dateEffective = '12-12-2014';
                testBroker.regOwnerName='testname';
                testBroker.comments='This is test';
                testBroker.objMode=true;
                testBroker.selectedPort='Arendal';
                String objId = GardTestData.test_object_1st.id;
                testBroker.prvUrl='https://cs8.salesforce.com/setup/build/viewApexClass.apexp?objid='+objId+'';
                testBroker.ursFaithfully='Test';
                testBroker.LOUConfirmation=true;
              
                testBroker.populateClient();
                testBroker.selectedObj=new List<String>();
                testBroker.selectedObj.add(GardTestData.test_object_1st.id);
                testBroker.selectedObj.add(GardTestData.test_object_2nd.id);
                testBroker.selectedObj.add(GardTestData.test_object_3rd.id);
                System.assertEquals(testBroker.selectedObj.size()>0,true,true);
                testBroker.SelectedClient=GardTestData.brokerContact.AccountId; 
                testBroker.loggedInAccId=GardTestData.brokerContact.AccountId;
                //testBroker.selectedBlueCardType='Request new Blue Card';
                testBroker.selectedBlueCardType='New Blue Card';
                testBroker.selectedClient=GardTestData.brokerAcc.id;
                testBroker.emailBody = '[HEADER]Thank you for contacting Gard. Your request renewal of Blue Card has been successfully registered.'+              
                'Below is a summary of the details submitted on MyGard:Client: [Client]Objects: [Objects]Date Effective: [DATE]Comments: [COMMENTS]You have also acknowledged that you have read and agreed to the terms and conditions.'+
                'Preferred method of contact:'+
                'Phone: [PHONEPREF]'+
                'Email: [EMAILPREF]'+
                'If you have any queries, please contact us at mygard.support@gard.no.'+
                '[SIGNATURE]'+
                '[FOOTER]';
                testBroker.dt = datetime.newInstance(1960, 02,17);
                System.assertNotEquals(null,testBroker.selectedClient);
                //testBroker.sendMails();
                testBroker.prvUrl = 'https://cs8.salesforce.com/setup/build/viewApexClass.apexp?objid=MyObjectDetails';
                //testBroker.selectedBlueCardType='Request renewal of Blue Card';
                //testBroker.submit();
                testBroker.selectedBlueCardType='New Blue Card';
                testBroker.submit();
                testBroker.selectedBlueCard.add('New Blue Card');
                testBroker.selectedBlueCard.add('Request renewal of Blue Card');
                //testBroker.sendType();
                testBroker.regOwnerChng=false;
                testBroker.vesselNameChng=false;
                testBroker.flagChng=false;
                testBroker.gTonnage='test';
                testBroker.disableBCChange=true;
                testBroker.requiredBCChange=true;
                testBroker.selectedBlueCard.add('New Blue Card');
                testBroker.selectedBlueCard.add('WRC Blue Card');
                testBroker.selectedBlueCard.add('Wreck Removal Blue Card (WRBC)');
                testBroker.checkSelectedBlueCard();
                testBroker.resetChkboxFields();
                testBroker.resetGTBox();
                testBroker.objectMode();
                testBroker.selectedBlueCardType='Request renewal of Blue Card';
                //testBroker.sendType();
                testBroker.selectedObj = new List<String>();
                testBroker.selectedObj.add(GardTestData.ObjectList[0].id+'');
                testBroker.selectedObj.add(GardTestData.ObjectList[1].id+'');
                testBroker.selectedObj.add(GardTestData.ObjectList[2].id+'');
                testBroker.selectedObj.add(GardTestData.ObjectList[3].id+'');
                testBroker.populateBlueCardType();
                //testBroker.populateBlueCardType();    commented on 15th March,2019 for too many soql
                testBroker.cancel();
                testBroker.resetGTField();
                Test.stopTest();
            }
        }
    }