@isTest
Class TestMyObjectDetailsAllClaimsCtrl1
{
     
    public static testmethod void cover()
    { 
		  AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
          insert numberofusers;
          GardTestData test_rec = new GardTestData();
      	  test_rec.commonRecord();
          test_rec.customsettings_rec();
          System.assert(GardTestData.grdobj!= null , true);
          GardTestData.test_object_2nd.guid__c = '8ce8ac89-a6fd-1836-9e07';
          update GardTestData.test_object_2nd;
          System.assertEquals(GardTestData.test_object_2nd.guid__c , '8ce8ac89-a6fd-1836-9e07');
          
        System.runAs(GardTestData.clientUser)
        {
            test.startTest();
            ApexPages.StandardController scontroller = new ApexPages.StandardController(GardTestData.clientAcc);
            //test_rec.obj_1st.guid__c = '8ce8ac89-a6fd-1836-9e07';
            //update test_rec.obj_1st;
            Pagereference pge_1 = page.MyObjectDetailsAllClaims;
            pge_1.getparameters().put('objid',GardTestData.test_object_2nd.guid__c);
            test.setcurrentpage(pge_1);
            MyObjectDetailsAllClaimsCtrl MyObject=new MyObjectDetailsAllClaimsCtrl(scontroller);
            MyObjectDetailsAllClaimsCtrl MyObjects=new MyObjectDetailsAllClaimsCtrl();
            MyObject.jointSortingParam = 'ASC##Claim_Type__c';
            MyObject.ViewData();
            MyObject.setSortDirection('Claim_Type__c');
            MyObject.setpageNumber();
            MyObject.navigate();
            MyObject.first();
            MyObject.next();
            MyObject.last();
            MyObject.previous();
            MyObject.exportToExcel();
            MyObject.obj = new object__c();
            MyObject.obj.id = GardTestData.test_object_2nd.Id;
            MyObject.isAuthorized = true;
            //MyObject.createFavourites();
            //MyObject.deleteFavourites(); 
            test.stopTest();
        }              
    }
}