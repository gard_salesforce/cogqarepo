public without sharing class MyCoverListCtrl
{
  //used for the Pagination purpose.
    
    private static final Integer SMARTQUERY_PAGE_SIZE   = 10;
    private static final String ONRISK_Y   = 'Y';
    private static final String ONRISK_N   = 'N';
    public boolean portfolioDocumentTab {get;set;}
    //pagination variables
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    public String sSoqlQuery{get;set;}
    private String nameOfMethod;
    Public Integer intStartrecord { get ; set ;}
    Public Integer intEndrecord { get ; set ;}
    
    Set<String> Account_ID_Set;
    public Boolean hasMultipleClients{get;set;}
    public Integer pageNum{get;set;}
    
    public List<Account> lstAccount{get;set;}
    public string loggedInContactId;
    public string loggedInAccountId;
    private String underwriter;
    private String product;
    private String productarea;
    private String broker;    
    private String policyyear;
    private String claimslead;
    private String gardshare;    
    private Date expirydate;
    private Boolean blOnrisk;
    private String onrisk;
    private String client;
    String strASC;
    public String selectedclientid{get;set;}
    
    List<Object__c> lstObject = new List<Object__c>();
    List<Contract> lstContract = new List<Contract>();
    List<Contract> lstContractClient = new List<Contract>();
    
    MyCoverList__c myCoverObj;
    
    public String base64Photo{get;set;}//added for Vcard 
    
    private Set<ID> setRiskID;
    private Set<String> setCoverID;
    private Boolean isQueried = false;
    public List<coverwrapper> coverLst  { get; set; }
    public List<coverwrapper> coverObjectLst  { get; set; }
    public String strParams  { get; set; }
    public Boolean showMyCover { get; set; }
    public String strMyCover { get; set; } 
    public Boolean blBrokerView { get; set; }
    public String jointSortingParam { get;set; }
    private String sortDirection;
    private String sortExp;
    private string sortFullExp;
    private string strforClient;
    public List<Asset> lstRiskCover{get;set;}
    public List<Asset> lstAssetfilter;
    public String underwrId { get;set; }
    private List<Gard_Contacts__c> lstUnderWriterPopup;
    public Gard_Contacts__c UnderWriterInfo{get;set;}
    public String pointingId{get;set;}
    public Integer noOfData{get;set;}
    public Set<String> lstObjectTypeOptions;
    public List<String> SelectedObjectType {get;set;}
    Public String hidePemeObject=''; // for 4687
    Public String hidePemeObj='';  // for 4687
    String agreementId;
    //added for SF-3606
    public String shareClientName{get;set;}
    public String shareClientId{get;set;}
    public List<SelectOption> getObjectTypeOptions() 
    {
        List<SelectOption> Options = new List<SelectOption>();
        if(lstObjectTypeOptions!= null && lstObjectTypeOptions.size()>0)
        {
            for(String strOp:lstObjectTypeOptions)
            {
                if(strOp != null )
                Options.add(new SelectOption(strOp,strOp));
            }
            Options.sort();
        }
        return Options;
    } 
    
    public String sortExpression
    {
        get
        {
            return sortExp;
        }
        set
        {
            //if the column is clicked on then switch between Ascending and Descending modes
            if (value == sortExp)
                sortDirection = (sortDirection == strASC)? 'DESC' : strASC;
            else
                sortDirection = strASC;
            sortExp = value;
        }
    }
    
     public String getSortDirection()
     {
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
          return strASC;
        else
         return sortDirection;
     }
    
     public void setSortDirection(String value)
     {  
       sortDirection = value;
     }
     
    public MyCoverListCtrl()
    {
        pageNum = 1;
        //strParams = ApexPages.currentPage().getParameters().get('id');
        strParams = ApexPages.currentPage().getParameters().get('enid');
        selectedclientid = '';
        system.debug('******strParams**************'+strParams);
        // decrypt id
        
        MyGardDocumentAvailability__c CS  = MyGardDocumentAvailability__c.getValues('My Portfolio Document Tab');
        if(CS.IsAvailable__c){
            portfolioDocumentTab = true;
            }
        else{
            portfolioDocumentTab = false;
        }
        
        String ssuid;
        if(!test.isrunningtest())
        {
            if(strParams != null)
            {
                Blob cryptoKey = Crypto.generateAesKey(128);
                ssuid = EncodingUtil.urlDecode(strParams, 'UTF-8');    
                Blob afterblob = EncodingUtil.base64Decode(ssuid);
                system.debug('after urldec done@@@@@@@@@@@@@@'+ssuid);    
                ssuid = (afterblob).toString();        
               //ssuid = (Crypto.decryptWithManagedIV('AES128', cryptoKey, afterblob)).toString();
                strParams = ssuid;
                system.debug('******strParams**************'+strParams);            
            }
        }
        if(!test.isRunningTest())
        {
        myCoverObj = [SELECT AgreementId__c , client__c, underwriter__c,product__c,productarea__c ,broker__c, expirydate__c, policyyear__c, 
                                claimslead__c, gardshare__c, onrisk__c, viewobject__c FROM MyCoverList__c Where guid__c =: strParams];
        System.debug('*********myCoverObj '+myCoverObj);
        }
        sSoqlQuery ='';
        blBrokerView = false;
        sortExp = 'Object__r.Name';
        strASC = 'ASC';
        sortDirection = strASC;
        setRiskID = new Set<ID>();
        strforClient = '';
        shareClientName = '';
        shareClientId = '';
        lstRiskCover = new List<Asset>();
        coverLst = new  List<coverwrapper>();  
        lstAssetfilter = new List<Asset>();
        lstObjectTypeOptions = new Set<String>();
        lstUnderWriterPopup = new List<Gard_Contacts__c>();
        UnderWriterInfo = new Gard_Contacts__c();
        
        //calling helperclass method
        MyGardHelperCtrl.CreateCommonData();
        //getting User object and other details from helper class
        User usr = MyGardHelperCtrl.usr;
        loggedInContactId = MyGardHelperCtrl.LOGGED_IN_CONTACT_ID;
        loggedInAccountId = MyGardHelperCtrl.LOGGED_IN_ACCOUNT_ID;
        //acmJuncObj = MyGardHelperCtrl.ACM_JUNC_OBJ;
        Account_ID_Set = MyGardHelperCtrl.ACCOUNT_ID_SET;
        shareClientName = MyGardHelperCtrl.SHARED_CLIENTS_NAME;
        shareClientId = MyGardHelperCtrl.ONLY_SHARED_CLIENTS;
        setCoverID = new Set<String>();
        setCoverID = MyGardHelperCtrl.TOTAL_CONTRACT_ID_SET;
        hasMultipleClients = MyGardHelperCtrl.hasMultipleClients;
        
        if(MyGardHelperCtrl.USER_TYPE == 'Broker')
        {
            blBrokerView = true;
            sortExpression = 'Agreement__r.Client__r.name';
        }
        else
        {
            blBrokerView = false;
            if(hasMultipleClients){
                sortExpression = 'Agreement__r.Client__r.name';
            }else{
                sortExpression = 'Object__r.Name';
            }
        }
    }
    
     public PageReference underwriterPopup() 
     {
        system.debug('******underwriterId@@@@@@@@@@@@@**************'+underwrId );
        system.debug('******pointingId@@@@@@@@@@@@@**************'+pointingId);
        base64Photo = null;//reset base64 data
        if(underwrId != null)
        {
            String[] arrStr = underwrId .split('#');
            underwrId = arrStr[0];
            pointingId = arrStr[1];   
                  
            lstUnderWriterPopup = new List<Gard_Contacts__c>();
            lstUnderWriterPopup = [select id, phone__c, FirstName__c, LastName__c, MobilePhone__c, email__c, Office_City__c, Portal_Image__c,Title__c 
                                       from Gard_Contacts__c 
                                       where id=:underwrId];         
            UnderWriterInfo = new Gard_Contacts__c();
            if(lstUnderWriterPopup.size()>0)
                UnderWriterInfo = lstUnderWriterPopup[0];
       }       
       return null; 
   }
    
    public PageReference viewObject() 
    {    
        system.debug('******objid**************'+strParams);
        String guid = [Select guid__c from Object__c Where id =: strParams].guid__c;
        system.debug('******guid**************'+guid);
        PageReference objectPage = new PageReference('/apex/MyObjectDetails?objid='+guid+'&clientId='+selectedclientid);
        objectPage.setRedirect(true);
        return objectPage;
    }
      
    public List<coverwrapper> getCovers() 
    {
        return coverLst  ;
    }
    public PageReference ViewData() 
    {
        isQueried = true;
        String strCondition ='';
        String strFilterCondition = '';
        hidePemeObject = MyGardHelperCtrl.hidePrepemeObject(); // for 4687
        hidePemeObj = '%' + hidePemeObject + '%';  // for 4687
        
        //system.debug('*****setCoverID.size*****'+setCoverID);
        system.debug('****** jointSortingParam**************'+jointSortingParam);
        if(jointSortingParam != null)
        {
            String[] arrStr = jointSortingParam .split('##');
            sortDirection = arrStr[0];
            sortExp = arrStr[1];
        }
        system.debug('******jointSortingParam **************'+jointSortingParam);
        strParams = ApexPages.currentPage().getParameters().get('underwriter');
        List<Asset> lstRiskCover;
        
        if(myCoverObj != null)
        {
            System.debug('*************blOnrisk '+blOnrisk);
            System.debug('*************blOnrisk '+myCoverObj.onrisk__c);
            agreementId = myCoverObj.AgreementId__c ;
            blOnrisk = myCoverObj.onrisk__c; //false;
            underwriter = myCoverObj.underwriter__c;
            product = myCoverObj.product__c;
            productarea = myCoverObj.productarea__c;
            broker = myCoverObj.broker__c;
            expirydate= Date.valueOf(myCoverObj.expirydate__c);
            policyyear  = String.valueOf(myCoverObj.policyyear__c);
            claimslead  = myCoverObj.claimslead__c;
            gardshare  = String.valueOf(myCoverObj.gardshare__c);
            
            System.debug('*************gardshare '+gardshare);            
            blOnrisk= myCoverObj.onrisk__c ;
            client = myCoverObj.client__c;
        }
        String param ;
        //productarea = 'P&I';
                    
        if(product != null)
        {
             product= product.replace('|', '&');
             product= product.replace('@', ',');
             strMyCover = product;
        }
        if(productarea != null)
        {
             productarea = productarea.replace('|', '&');
             productarea = productarea.replace('@', ',');
        }                    
                              
        system.debug('******underwriter**************'+underwriter);
        system.debug('******product **************'+product );
        system.debug('******productarea **************'+productarea );
        system.debug('******broker **************'+broker );
        system.debug('******expirydate **************'+expirydate );
        system.debug('******policyyear **************'+policyyear );
        system.debug('******claimslead **************'+claimslead );
        system.debug('******gardshare **************'+gardshare );
        system.debug('******blOnrisk **************'+blOnrisk);
        system.debug('******client **************'+client );
        string sortFullExp = sortExpression  + ' ' + sortDirection;
            
        if(underwriter != '')
        {
            strCondition = ' AND UnderwriterNameTemp__c =:  underwriter ';
        }
        if((client != '' || client != null ) && blBrokerView == true)
        { //added for MYGP-216
            strCondition = strCondition + ' AND Agreement__r.Client__r.Name=: client ';
        }
        if(product != '')
        {
            strCondition = strCondition + ' AND Product_Name__c =: product ';
        }
        if(productarea != '')
        {
            strCondition = strCondition + ' AND Agreement__r.Business_Area__c =: productarea ';
        }
        //if(blBrokerView)
        //{ //added for MYGP-216
            //strCondition = strCondition + ' AND Agreement__r.Broker__r.Name=: broker  ';
            //strCondition = strCondition + ' AND Agreement__r.Broker__c =: loggedInAccountId ';
        //}
        
        if(policyyear != '')
        {
            strCondition = strCondition + ' AND Agreement__r.Policy_Year__c=: policyyear ';
        }
        if(claimslead != '')
        {
            strCondition = strCondition + ' AND Claims_Lead__c=:claimslead ';
        }
        if(gardshare != '')
        {             
            strCondition = strCondition + ' AND Gard_Share__c=:gardshare ';
        }
        /*if(expirydate != '' )
        {
            strCondition = strCondition + ' AND Expiration_Date__c =: dtExpirydate ';
        }*/
        if(blOnrisk != null)
        {
            strCondition = strCondition + ' AND On_risk_indicator__c =: blOnrisk ';
        }
        
        if( SelectedObjectType != null && SelectedObjectType.size()>0)
        {
                  strFilterCondition = ' AND Object__r.Object_Type__c IN : SelectedObjectType ';
        }               
        system.debug('************SelectedObjectType************'+SelectedObjectType); 
        system.debug('************strCondition************'+strCondition);          
            
        //if(blBrokerView == true) 4302 Commented for Client Acquisition
        {            
            strforClient = 'Agreement__r.Client__r.Name ,';
        }
        sSoqlQuery = 'SELECT '+strforClient+'Object__r.Id,Object__r.Name , Agreement__r.Client__r.GUID__c, Object__r.Object_Type__c, UnderwriterNameTemp__c, UnderwriterId__c, Product_Name__c,Agreement__r.Business_Area__c, '+
                    'Agreement__r.Broker__r.Name , Expiration_Date__c , Agreement__r.Policy_Year__c , '+
                    'Claims_Lead__c , Gard_Share__c , On_risk_indicator__c,Object__r.Dummy_Object_flag__c FROM Asset Where Agreement__c =: agreementId '+ strCondition + strFilterCondition + 'AND (NOT(Object__r.name LIKE : hidePemeObj))' +
                    ' ORDER BY '+sortFullExp;
       if(setCoverID.contains(agreementId))
       {
           lstAssetfilter=database.query('SELECT Object__r.Object_Type__c FROM Asset Where Agreement__c IN : setCoverID '+ strCondition);
           if(lstAssetfilter != null)
           {
                for (Asset ast : lstAssetfilter)  
                {
                    lstObjectTypeOptions.add(ast.Object__r.Object_Type__c);  
                } 
           }    
            searchAssets();
       }
       else
       {
           PageReference pg = new PageReference('/apex/HomePage');
           system.debug('redirected to homepage' );
           return pg; 
       }
        return null;
    }
    
    public void searchAssets()
    {
        if(sSoqlQuery.length()>0)
        {
            system.debug('****setCon**********'+setCon);
            system.debug('****setCon.getResultSize()**********'+setCon.getResultSize());
            if(!Test.isRunningTest())
            {
                lstRiskCover=(List<Asset>)setCon.getRecords();
            }
            if(setCon.getPageNumber() == 1)
                intStartrecord = 1;
            else
                intStartrecord  = ((setCon.getPageNumber() - 1) * SMARTQUERY_PAGE_SIZE) + 1;
          
            intEndrecord = setCon.getPageNumber() * SMARTQUERY_PAGE_SIZE;
            if( intEndrecord > noOfRecords )
            {
                intEndrecord = noOfRecords ;
            }
        }
              
        system.debug('********lstRiskCover.size()**************'+lstRiskCover.size());
        //showMyCover  = true;
        //coverObjectLst= new List<coverwrapper>();
        noOfData = 0;
        coverLst  = new List<coverwrapper>();
        for(Asset rsk:lstRiskCover)
        {
            if(blBrokerView == false)
            {
                coverLst.add(new coverwrapper(string.valueof(rsk.Agreement__r.Client__r.Name),
                              string.valueof(rsk.Object__r.Id),
                              string.valueof(rsk.Object__r.Name),
                              string.valueof(rsk.Object__r.Object_Type__c),
                              string.valueof(rsk.UnderwriterNameTemp__c),
                              string.valueof(rsk.Product_Name__c),
                              string.valueof(rsk.Agreement__r.Business_Area__c),
                              string.valueof(rsk.Agreement__r.Broker__r.Name), 
                              Date.valueof(rsk.Expiration_Date__c),
                              string.valueof(rsk.Agreement__r.Policy_Year__c),                                           
                              string.valueof(rsk.Claims_Lead__c),
                              string.valueof(rsk.Gard_Share__c),
                              string.valueof(rsk.On_risk_indicator__c),
                              rsk.UnderwriterId__c,
                              string.valueof(rsk.Object__r.Dummy_Object_flag__c),
                              string.valueof(rsk.Agreement__r.Client__r.GUID__c)
                              ));
            }else
            {
                 coverLst.add(new coverwrapper(
                              string.valueof(rsk.Agreement__r.Client__r.Name),
                              string.valueof(rsk.Object__r.Id),
                              string.valueof(rsk.Object__r.Name),
                              string.valueof(rsk.Object__r.Object_Type__c),
                              string.valueof(rsk.UnderwriterNameTemp__c),
                              string.valueof(rsk.Product_Name__c),
                              string.valueof(rsk.Agreement__r.Business_Area__c),
                              string.valueof(rsk.Agreement__r.Broker__r.Name), 
                              Date.valueof(rsk.Expiration_Date__c),
                              string.valueof(rsk.Agreement__r.Policy_Year__c),                                           
                              string.valueof(rsk.Claims_Lead__c),
                              string.valueof(rsk.Gard_Share__c),
                              string.valueof(rsk.On_risk_indicator__c),
                              rsk.UnderwriterId__c,
                              string.valueof(rsk.Object__r.Dummy_Object_flag__c),
                              string.valueof(rsk.Agreement__r.Client__r.GUID__c)
                              ));
            }
            noOfData++; 
         }
    }
    
    public ApexPages.StandardSetController setCon 
    {
        get{
            if(setCon == null || isQueried){
                isQueried = false;
                size = SMARTQUERY_PAGE_SIZE;
                System.debug('Soql Query--->'+sSoqlQuery);
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery+' LIMIT 10000'));
                setCon.setPageSize(size);
                noOfRecords = setCon.getResultSize();
            }
            return setCon;
        }set;
    }
    
   public void setpageNumber()
    {
        setCon.setpageNumber(pageNum);
        searchAssets();
    }
    
    public Boolean hasNext 
    {
        get 
        {
            return setCon.getHasNext();
        }
        set;
    }
    public Boolean hasPrevious 
    {
        get 
        {
            return setCon.getHasPrevious();
        }
        set;
    }
    
    public Integer pageNumber 
    {
        get 
        {
            return setCon.getPageNumber();
        }
        set;
    }
    
    public void navigate()
    {
        if(nameOfMethod=='previous')
        {
             setCon.previous();
             pageNum=setCon.getpageNumber();
             searchAssets();
        }
        else if(nameOfMethod=='last')
        {
             setCon.last();
             pageNum=setCon.getpageNumber();
             searchAssets();
        }
        else if(nameOfMethod=='next')
        {
             setCon.next();
             pageNum=setCon.getpageNumber();
             searchAssets();
        }
        else if(nameOfMethod=='first')
        {
             setCon.first();
             pageNum=setCon.getpageNumber();
             searchAssets();
        }
    }
    public void first() 
    {
        nameOfMethod='first';
        navigate();
    }
    
    public void next()
    {
        nameOfMethod='next';
        pageNum = setCon.getPageNumber();
        navigate();
    }
   
    public void last()
    {
        nameOfMethod='last';
        navigate();
    }
  
    public void previous()
    {
        nameOfMethod='previous';
         pageNum = setCon.getPageNumber();
        navigate();
    }

    public class coverwrapper
    {
        public Asset riskCover{get; set;}
        public String client {get; set;}
        public String objectId {get; set;}
        public String objectname {get; set;}
        public String objectcategory {get; set;}
        public String underwriter {get; set;}
        public String underwriterID {get; set;}
        public String product {get; set;}
        public String productarea {get; set;}
        public String broker {get; set;}
        public Date expirydate {get; set;}
        public String policyyear {get; set;}
        public String claimslead {get; set;}
        public String gardshare {get; set;}
        public String onrisk {get; set;}
        public String param {get; set;}
        public String dummyObject{get; set;}
         public String clientId{get;set;}
        public coverwrapper(String cltn,String objId, String objname, String objtype,String unwrt,String prod, String prodarea,String brk,Date exdt,String plcyr, String clmld, String grdsh,String rsk,String underID, String dummy,String clientId)
        {
           client = cltn;
           objectId  = objId;
           objectname = objname;
           objectcategory = objtype;
           underwriter =  unwrt;
           underwriterID = underID;
           product = prod;
           productarea = prodarea;
           broker = brk;
           expirydate = exdt;
           policyyear = plcyr;
           claimslead = clmld;
           gardshare = grdsh;
           if(rsk == 'true')
           {
               onrisk = ONRISK_Y;
           }else
           {
               onrisk = ONRISK_N;
           }
           dummyObject = dummy;
           this.clientId = clientId;
           //param = unwrt + strSeparator + prod + strSeparator +prodarea+ strSeparator +brk+ strSeparator +exdt+strSeparator +plcyr+strSeparator +clmld+strSeparator +grdsh+strSeparator +rsk;
        }
    }
    public PageReference exportToExcel()  
    {
        setCon.setPagesize(1000);
        searchAssets();
        return page.GenerateExcelForMyCoverList;
    }
    public Static String getGenerateTimeStamp()
    {
        Datetime myDT = Datetime.now();
        String formatted = myDT.formatGMT('d/MMM/yy HH:mm a');
        return formatted ;
    }
    public Static String getGenerateTime()
    {
        Datetime myDT = Datetime.now();
        String formatted = myDT.formatGMT('EEE MMM d HH:mm:ss yyyy');
        return formatted ;
    }
    public Static String getUserName()
    {
        String name;
        User usr = [Select Id, contactID from User Where Id =: UserInfo.getUserId()]  ;      
        Contact myContact;
        if(usr!= null && usr.contactID != null)
        {
            myContact = [Select AccountId,Name from Contact Where id=: usr.contactID];
            name=myContact.Name;
        }
        return name;
    } 
    public PageReference clear()
    {
        pageNum = 1;
        SelectedObjectType.clear();
        setCon.setpageNumber(pageNum);
        return page.MyCoverList;
    }
    public PageReference print()  
    {
        setCon.setPagesize(1000);
        searchAssets();
        return page.PrintforMyCoverList;
    }
    public PageReference downloadVcardCover()
    {
        return page.VcardForCoverListPage;
    }
}