@isTest
private class TestContactTrigger {
    /*
    static testMethod void testContactValuesWithPartnerUsers(){
        test.startTest();
        
        //Create Account
        Account anAccount = new Account(  
            Name = 'Test Account ',
            BillingCity = 'Bristol',
            BillingCountry = 'United Kingdom',
            BillingPostalCode = 'BS1 1AD',
            BillingState = 'Avon' ,
            BillingStreet = '1 Elmgrove Road',
            Company_Role__c = 'Client',
            Area_Manager__c = UserInfo.getUserId()
        );
        insert anAccount;
        
        //Fetch the account
        Account fetchedAccount = [SELECT ID,BillingCity from ACCOUNT WHERE Name = 'Test Account' LIMIT 1];
        System.assertEquals('Bristol',fetchedAccount.BillingCity);
        
        //build a list of record type ids
        List<Id> RecIds = new List<Id>();
        for(RecordType aRecType:ContactToolkit.ContactRecordTypes.values()){
            RecIds.add(aRecType.Id);
        }
        //Create contact
        Contact aContact = new Contact(  FirstName='Test',
                                       LastName = 'Contact ',
                                       RecordTypeId = RecIds[0],
                                       //If the record type is normal use the client type otherwise 
                                       //use the record type name which we expect will be Correspondent
                                       Type__c = ContactToolkit.ContactRecordTypes.get(RecIds[0]).Name == 'Normal' ? 'Client' : ContactToolkit.ContactRecordTypes.get(RecIds[0]).Name,
                                       AccountId = fetchedAccount.Id);
        insert aContact;
        //Fetch the contact 
        Contact fetchedContact = [SELECT ID, FirstName from CONTACT where AccountId =: fetchedAccount.Id LIMIT 1];
        System.assertEquals('Test',fetchedContact.FirstName);
        
        //Create User
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community Login User Custom'];
        String userNameToBeApplied = String.valueof(DateTime.now().getTime()) + 'testing@testorg.com.mygard';
        User u = new User(Alias = 'standt', 
                          Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', 
                          FirstName='TestFirstName',
                          LastName='Testing', 
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', 
                          ProfileId = p.Id, 
                          IsActive =true,
                          TimeZoneSidKey='America/Los_Angeles', 
                          UserName=userNameToBeApplied,
                          ContactId = fetchedContact.Id
                         );
        insert u;
        
        test.stopTest();
        
        update fetchedContact;
        
        Contact fetchedContact1 = [SELECT ID, MyGard_user_type__c FROM CONTACT WHERE AccountId =: fetchedAccount.Id LIMIT 1];
        System.assertEquals('Admin', fetchedContact1.MyGard_user_type__c);
        
        
        update fetchedContact;
        
        fetchedContact1 = [SELECT ID, MyGard_user_type__c FROM CONTACT WHERE AccountId =: fetchedAccount.Id LIMIT 1];
        System.assertEquals('Normal', fetchedContact1.MyGard_user_type__c);
    }
    */
    
    static testMethod void testContactSync(){
         //correspondant contacts Custom Settings
        List<ContactSubscriptionFields__c> conFieldsList = new List<ContactSubscriptionFields__c>();
        ContactSubscriptionFields__c conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        ContactSubscriptionFields__c conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        ContactSubscriptionFields__c conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList;
        //Create Account
       string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
        List<Valid_Role_Combination__c> vrcList = new List<Valid_Role_Combination__c>();
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
        vrcList.add(vrcClient);
        Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
        vrcList.add(vrcBroker);
        Valid_Role_Combination__c vrcClinic = new Valid_Role_Combination__c(Role__c = 'External Service Provider',Sub_Role__c='ESP - Agent');
        vrcList.add(vrcClinic);
        insert vrcList;
        Account anAccount = new Account(  
            Name = 'Test Account ',
            BillingCity = 'Bristol',
            BillingCountry = 'United Kingdom',
            BillingPostalCode = 'BS1 1AD',
            BillingState = 'Avon' ,
            BillingStreet = '1 Elmgrove Road',
            Company_Role__c = 'Client',
            Sub_Roles__c ='',
            Area_Manager__c = UserInfo.getUserId()
        );
        insert anAccount;
        
        
         Gard_Contacts__c gc = new Gard_Contacts__c(FirstName__c ='test1',lastName__c = 'test1');
        insert gc;
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        
        
        User salesforceLicUser = new User(
            Alias = 'standt', 
            profileId = salesforceLicenseId ,
            Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8',
            CommunityNickname = 'test13',
            LastName='Testing',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',  
            TimeZoneSidKey='America/Los_Angeles',
            UserName='test008@testorg.com',
            isActive = true ,
            ContactId__c = gc.id,
            City= 'Arendal'
        );
        insert salesforceLicUser;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'abc120000');
        insert Markt;  
        
        Country__c country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA');
        insert country;
        
        Account clinicAcc = new Account( Name='testClinic',
                                        BillingCity = 'Bristol',
                                        BillingCountry = 'United Kingdom',
                                        BillingPostalCode = 'BS1 1AD',
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.ESP_Contact_Record_Type,
                                        company_Role__c='External Service Provider',
                                        Sub_Roles__c = 'ESP - Agent',
                                        Site = '_www.ctsclinic.se',
                                        Type = 'Surveyor' ,
                                        Area_Manager__c = salesforceLicUser.id,      
                                        Market_Area__c = Markt.id,
                                        PEME_Enrollment_Status__c='Enrolled',
                                        Country__c = country.id                                                                  
                                       );
        insert clinicAcc;
        
        Account clientAcc= New Account( Name = 'Testt',
                                       recordTypeId = System.Label.Client_Contact_Record_Type,
                                       Site = '_www.test.se',
                                       Type = 'Customer',
                                       BillingStreet = 'Gatan 1',
                                       BillingCity = 'Stockholm',
                                       BillingCountry = 'SWE',
                                       Area_Manager__c = salesforceLicUser.id,        
                                       Market_Area__c = Markt.id, 
                                       PEME_Enrollment_Status__c='Enrolled',
                                       Company_Role__c = 'Client',        
                                       Sub_Roles__c = '',
                                       Country__c = country.id
                                      );
        insert clientAcc; 
        
        Contact clinicContact = new Contact( 
            FirstName='clinicContact',
            LastName='clinicContactLN',
            MailingCity = 'London',
            MailingCountry = 'United Kingdom',
            MailingPostalCode = 'SE1 1AD',
            MailingState = 'London',
            MailingStreet = '1 London Road',
            AccountId = clinicAcc.Id,
            Email = 'test5554@noSite.abc'
        );
        insert clinicContact;
        //Fetch the account
        Account fetchedAccount = [SELECT ID,BillingCity from ACCOUNT WHERE Name = 'Test Account' LIMIT 1];
        System.assertEquals('Bristol',fetchedAccount.BillingCity);
        
        //build a list of record type ids
        List<Id> RecIds = new List<Id>();
        for(RecordType aRecType:ContactToolkit.ContactRecordTypes.values()){
            RecIds.add(aRecType.Id);
            
        }
        
        PEME_Enrollment_Form__c pef = new PEME_Enrollment_Form__c ();
        pef.ClientName__c=clientAcc.ID;
        pef.Comments__c='Report analyzed';
        pef.Enrollment_Status__c= 'Draft';
        pef.Gard_PEME_References__c='Test References,  ';
        pef.Last_Status_Change_Date__c=Date.valueOf('2016-01-27');
        pef.Submitter__c=clinicContact.ID;
        pef.DisEnrolled_Expire_Date__c=System.today().adddays(3);
        insert pef;
        
        PEME_Manning_Agent__c pmg=new PEME_Manning_Agent__c  ();
        pmg.Client__c=clinicAcc.ID;
        pmg.PEME_Enrollment_Form__c=pef.ID;
        pmg.Company_Name__c='Test Company';
        pmg.Contact_Point__c=clinicContact.Id;
        pmg.Status__c='Approved';
        insert pmg;
        
        Contact clinicContact1 = new Contact( 
            FirstName='clinicContact',
            LastName='clinicContactLN1',
            MailingCity = 'London',
            MailingCountry = 'United Kingdom',
            MailingPostalCode = 'SE1 1AD',
            MailingState = 'London',
            MailingStreet = '1 London Road',
            AccountId = clinicAcc.Id,
            Email = 'test5554@noSite.abc',
            Manning_Agent_or_Debit_Note_ID__c = pmg.id
        );
        insert clinicContact1;
        //Create contact
        Contact aContact = new Contact(  FirstName='Test',
                                       LastName = 'Contact ',
                                       RecordTypeId = RecIds[0],
                                       //If the record type is normal use the client type otherwise 
                                       //use the record type name which we expect will be Correspondent
                                       Type__c = ContactToolkit.ContactRecordTypes.get(RecIds[0]).Name == 'Normal' ? 'Client' : ContactToolkit.ContactRecordTypes.get(RecIds[0]).Name,
                                       AccountId = fetchedAccount.Id,
                                       No_Longer_Employed_by_Company__c= false,
                                       Manning_Agent_or_Debit_Note_ID__c = pmg.id);
        insert aContact;
        
        Test.startTest();
        Contact fetchedContact = [SELECT ID, Synchronisation_Status__c, Notify_Admin__c,No_Longer_Employed_by_Company__c FROM CONTACT WHERE ID =: aContact.Id];
        fetchedContact.Synchronisation_Status__c='Sync Failed';
        fetchedContact.No_Longer_Employed_by_Company__c= true;
        update fetchedContact;
        Test.stopTest();
        
        fetchedContact = [SELECT ID, Synchronisation_Status__c, Notify_Admin__c FROM CONTACT WHERE ID =: aContact.Id];
        //System.assertEquals(true,fetchedContact.Notify_Admin__c);
    }
    
}