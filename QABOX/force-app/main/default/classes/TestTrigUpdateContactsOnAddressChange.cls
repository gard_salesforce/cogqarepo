@isTest
private class TestTrigUpdateContactsOnAddressChange {
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    
    static testMethod void contactMailingAddressUpdatedWhenCompanyBillingAddressChanges() {
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
        insert vrcClient;
        //Gard_Team__c gt = new Gard_Team__c();
        Gard_Team__c gt = new Gard_Team__c(Active__c = true,office__c = 'test',name = 'gTeam',region_Code__c = 'Code');
        insert gt;
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        Country__c country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISR');
        insert country;
        
        Account account = TestDataGenerator.getClientAccount();
        
        account.Billing_Street_Address_Line_1__c = 'InitialStreetLine1';
        account.Billing_Street_Address_Line_2__c = 'InitialStreetLine2';
        account.Billing_Street_Address_Line_3__c = 'InitialStreetLine3';
        account.Billing_Street_Address_Line_4__c = 'InitialStreetLine4';
        account.BillingCity__c = 'InitialBillingCity';
        account.BillingState__c = 'InitialBillingState';
        account.BillingPostalCode__c = '11111';
        account.BillingCountry__c = 'Norway';
        account.Country__c = country.id;
        update account;
        
        Contact contact = TestDataGenerator.createContactFor(account);          
    
        account.Billing_Street_Address_Line_1__c = 'NewStreetLine1';
        account.Billing_Street_Address_Line_2__c = 'NewStreetLine2';
        account.Billing_Street_Address_Line_3__c = 'NewStreetLine3';
        account.Billing_Street_Address_Line_4__c = 'NewStreetLine4';
        account.BillingCity__c = 'NewBillingCity';
        account.BillingState__c = 'NewBillingState';
        account.BillingPostalCode__c = '22222';
        account.BillingCountry__c = 'Denmark';
        
        update account;
        
        contact = [select Id, LastName, MailingStreet, MailingCity, MailingPostalCode, MailingState, MailingCountry from contact where Id = :contact.id limit 1];   
        
        System.assert(contact.MailingStreet == 'NewStreetLine1\nNewStreetLine2\nNewStreetLine3\nNewStreetLine4', 'Contact\'s initial street address incorrect. Actual value: ' + contact.MailingStreet);
        System.assert(contact.MailingCity == 'NewBillingCity', 'Contact\'s initial city incorrect. Actual value: ' + contact.MailingCity);
        System.assert(contact.MailingCountry == 'Denmark', 'Contact\'s initial country incorrect. Actual value: ' + contact.MailingCountry);
        System.assert(contact.MailingPostalCode == '22222', 'Contact\'s initial postal code incorrect. Actual value: ' + contact.MailingPostalCode);
        System.assert(contact.MailingState == 'NewBillingState', 'Contact\'s initial state incorrect. Actual value: ' + contact.MailingState);
    }
}