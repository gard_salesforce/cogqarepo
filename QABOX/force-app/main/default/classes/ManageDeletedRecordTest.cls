/*************************************************************************************************
    Author      : Martin Gardner
    Company     : cDecisions
    Date Created: 13/09/2013
    Description : This class tests that the creation and update of Deleted_Record__c records
    			  occurs correctly
    Modified    :   
***************************************************************************************************/
@IsTest (seeAllData = true) 
private class ManageDeletedRecordTest {

    static testMethod void testOpportunityDelete() {
    	
    
       
  	   Test.startTest();     
	       //Get the name of the object
	       SetupTestData.setupTestData();
	       String objectname = Schema.SObjectType.Opportunity.getName();
	       
	       //call the delete method
	       ManageDeletedRecord.RememberDeletedRecords(SetupTestData.oppIds, objectname);
	       
	       //call the undelete method
	       ManageDeletedRecord.UpdateUnDeletedRecords(SetupTestData.oppIds, objectname);
	       
	       //Test exception handling
	       SetupTestData.dwhIdPrefix = 'Test2';
	       SetupTestData.setupTestData();
	       
	       ManageDeletedRecord.testFailure = true;
	       //call the delete method 
	       try{
	       		ManageDeletedRecord.RememberDeletedRecords(SetupTestData.oppIds, objectname);
	       }catch (SetupTestData.TestException e) { 
	       		System.Assert(e.getMessage() == ManageDeletedRecord.TestExceptionMessage);
	       }
	       //call the undelete method
	        ManageDeletedRecord.testFailure = true;
	       try{
	       		ManageDeletedRecord.UpdateUnDeletedRecords(SetupTestData.oppIds, objectname);    
	       }catch (SetupTestData.TestException e) {
	       		System.Assert(e.getMessage() == ManageDeletedRecord.TestExceptionMessage);
	       }
	       
	       //Test the delete method with a map of id to dwh id
	       //Prepare map
	       SetupTestData.dwhIdPrefix = 'Test3';
	       SetupTestData.setupTestData();
	       Map<Id, String> recMap = new Map<Id, String>();
	       for(Opportunity anOpp: SetupTestData.testOpps){
	       		recMap.put(anOpp.Id, anOpp.DWH_Opportunity_Id__c);
	       }
	       ManageDeletedRecord.RememberDeletedRecords(recMap, objectName);
	       
	       //Test the map method for exceptions
	       SetupTestData.dwhIdPrefix = 'Test4';
	       SetupTestData.setupTestData();
	       ManageDeletedRecord.testFailure = true;
	       Map<Id, String> recMap2 = new Map<Id, String>();
	       for(Opportunity anOpp: SetupTestData.testOpps){
	       		recMap2.put(anOpp.Id, anOpp.DWH_Opportunity_Id__c);
	       }
	        ManageDeletedRecord.RememberDeletedRecords(recMap2, objectName);
	       
	    Test.stopTest();   
    }
    
    static testMethod void testOpportunityDeleteUnDeleteDeleteSet() {
    	
  	   Test.startTest();     
	       	//Get the name of the object
	       	SetupTestData.setupTestData();
	       	String objectname = Schema.SObjectType.Opportunity.getName();
	       
	       	//call the delete method
	       	ManageDeletedRecord.RememberDeletedRecords(SetupTestData.oppIds, objectname);
	       
	       	//call the undelete method
	       	ManageDeletedRecord.UpdateUnDeletedRecords(SetupTestData.oppIds, objectname);
	       
	       	//call the delete method
	       	ManageDeletedRecord.RememberDeletedRecords(SetupTestData.oppIds, objectname);
	       
	       	//Check that the record history has the undelete flag false
	       	List<Deleted_Record__c> delrecList = [SELECT Id, RecordId__c, Object__c, Deleted_Date__c, UnDeleted__c FROM Deleted_Record__c WHERE RecordId__c in : SetupTestData.oppIds];      
			System.assert(delrecList != null);
			System.assert(delrecList.Size()>0);
			
			for(Deleted_Record__c aRec: delrecList){
				System.AssertEquals(false, aRec.UnDeleted__c);
				
			}
	       
	    Test.stopTest();   
    }
    
    static testMethod void testOpportunityDeleteUnDeleteDeleteMap() {
    	
  	   Test.startTest();     
	       //Get the name of the object
	       SetupTestData.setupTestData();
	       String objectname = Schema.SObjectType.Opportunity.getName();
	       //Test the delete method with a map of id to dwh id
	       //Prepare map
	       SetupTestData.dwhIdPrefix = 'Test3';
	       SetupTestData.setupTestData();
	       Map<Id, String> recMap = new Map<Id, String>();
	       for(Opportunity anOpp: SetupTestData.testOpps){
	       		recMap.put(anOpp.Id, anOpp.DWH_Opportunity_Id__c);
	       }
	       ManageDeletedRecord.RememberDeletedRecords(recMap, objectName);
	       	       
	       	       
	       //call the undelete method
	       ManageDeletedRecord.UpdateUnDeletedRecords(SetupTestData.oppIds, objectname);
	       
	       //call the delete method
	        ManageDeletedRecord.RememberDeletedRecords(recMap, objectName);
	       

	       

	       
	    Test.stopTest();   
    }          
}