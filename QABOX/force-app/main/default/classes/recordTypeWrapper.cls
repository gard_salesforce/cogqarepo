/*
Requirement : Enable the New Opportunity Checklist button in Opportunity Checklist related list of Opportunity in Lightning view
JIRA : SF-4484
Author : Abhirup Banik
Revision History
Intial version : 04/03/2019 : (Descp : Wrapper Class for New_OppChecklist_btn_Ctrl.apxc)
*/
public class recordTypeWrapper {
 /*******************************************/
        /* Wrapper class to display Record Types      */
        /*****************************************/
        
        public class DisplayRecordTypes {
            
            @AuraEnabled public boolean isDefault; 
            @AuraEnabled public string recordTypeLabel;
            @AuraEnabled public string recordTypeId;
            
            public DisplayRecordTypes(){
                isDefault = false;
                recordTypeLabel = '';
                recordTypeId = '';
            }
        }
}