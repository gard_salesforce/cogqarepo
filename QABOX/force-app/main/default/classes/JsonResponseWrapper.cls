//TestClass : TestPemeWebService
public class JsonResponseWrapper {
    
    public class PemeInvoiceFileAuth{
        public String access_token;
        public String refresh_token;
        public String scope;
        public String token_type;
        public String expires_in;
    }
    
	public class PemeInvoiceFile{
        public String status;
        public String message;
        public String traceId;
        public PemeInvoiceFile(String status,String message,String traceId){
            this.status = status;
            this.message = message;
            this.traceId = traceId;
        }
    }
}