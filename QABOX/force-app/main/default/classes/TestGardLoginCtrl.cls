@isTest public class TestGardLoginCtrl{
    private static GardTestData test_rec;
    private static LogInDownTime__c downTime;
    private static Browser_Worning__c browWarn;
    
    private static void createData(){
        test_rec = new GardTestData();
        test_rec.commonrecord();
        downTime = new LogInDownTime__c (
                                                             name = 'DetailMessage',
                                                             Message__c = 'MyGard is currently down for maintenance and is expected to be back in normal operation by 20th April 08:00 Central European Time. We apologize for the inconvenience.'
                                                          );
        insert downTime;
        browWarn = new Browser_Worning__c (
                                                                  name = 'DetailMessage',
                                                                  Message__c = 'MyGard has detected an incompatible browser. Please refer to the user manual for a list of supported browsers.'
                                                              );
        insert browWarn;
        insert new List<MFA_Login__c>{
            new MFA_Login__c(name = 'brokerUserMFA',Entity_Id__c = GardTestData.brokerUser.Id)
                };

    }
    
    @isTest private static void brokerTest(){
        createData();
        System.runAs(GardTestData.brokerUser){
            Test.startTest();
            //System.assertEquals('DetailMessage',downTime.name);
            GardLoginCtrl glcBroker = new GardLoginCtrl();
            PageReference pfbroker1 = glcBroker.forwardToCustomAuthPage();
            PageReference pfbroker2 = glcBroker.forgotPassword();
            ApexPages.currentPage().getParameters().put('startURL','www.gard.com');
            //PageReference pfbroker3 = glcBroker.login();
            glcBroker.isRemember = true;
            glcBroker.userName = GardTestData.brokerUser.username;
            glcBroker.password = 'randomPassword';
            glcBroker.form1Next();
            glcBroker.form2Next();
            glcBroker.login();
            glcBroker.resetForm();
            GardLoginCtrl.getVerCodeMessage(GardTestData.brokerUser.Id);
            //PageReference pfbroker3 = glcBroker.login();
            glcBroker.topMessage = 'message on top';
            glcBroker.detailMessage = 'message in detail';
            glcBroker.BrowsertopMessage = 'browser top message';
            glcBroker.BrowserDetailMessage = 'browser detail message';
            Test.stopTest();
        }
    }
}