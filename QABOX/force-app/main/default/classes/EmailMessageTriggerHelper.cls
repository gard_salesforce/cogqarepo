public class EmailMessageTriggerHelper{

    public static void updateCaseActivity(List<EmailMessage> incomingCaseEmails,Boolean incoming){
        String activityString = incoming ? 'An email was received.' : 'An email was sent.';
        List<Case> parentCases = Database.query('SELECT Id,Type,recordTypeId,recordType.Name,ownerId FROM Case WHERE Id IN (SELECT parentId FROM EmailMessage WHERE Id IN :incomingCaseEmails)');
        for(Case parentCase : parentCases){
            parentCase.Last_Activity__c = activityString;
            parentCase.Last_Activity_DateTime__c = System.now();
        }
        if(!parentCases.isEmpty()) update parentCases;
    }
}