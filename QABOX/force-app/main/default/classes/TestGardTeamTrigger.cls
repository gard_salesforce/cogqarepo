//tests GardTeamTrigger and GardTeamHelper
@isTest public class TestGardTeamTrigger {
	private static Gard_Team__c gt;
    
    private static void createTestData(){
    	gt = new Gard_Team__c(
        	name = 'gardTeamFN',
            region_code__c = 'TSET',
            phone__c = '9876543211',
            office__c = 'officeCity',
            P_I_email__c = 'emailPNI@gardTeam.abc',
            Marine_Email__c = 'emailMarine@gardTeam.abc',
            Active__c = true
        );
    }
    
    @isTest private static void CreateGardTeam(){
        createTestData();
        insert gt;
        update gt;
        delete gt;
    }
    
}