global class Scheduler_class implements Schedulable{

    public static String sched = '0 00 01 * * ?';  //Every Day at Midnight 

    global static String scheduleMe() 
    {
        Scheduler_class  SC = new Scheduler_class (); 
        return System.schedule('My batch Job', sched, SC);
    }

    global void execute(SchedulableContext sc) {

        BatchUpdateGardContacts ugc = new BatchUpdateGardContacts();
         if(!Test.isRunningTest())       
         {
             ID batchprocessid = Database.executeBatch(ugc,50);           
         }
             //BatchDuplicateClaimProcessed  cd = new BatchDuplicateClaimProcessed  ();
             BatchDuplicateClaimProcessed2 cd = new BatchDuplicateClaimProcessed2();
             ID batchprocessid1 = Database.executeBatch(cd,50);           
         //}
    }
}