@isTest
public class TestOppurtunityChecklistRecordTypeSelect{
    private static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    private List<Opportunity> allOpp = [SELECT ID FROM Opportunity WHERE RecordTypeName__c = 'P&I'];
    private static String strAccId;
    private static String strOppId;
    private static User salesforceUser;
    private static Opportunity_Checklist__c testOppChecklists;
    private static String recordTypeId;
    private static Opportunity opp;
 //Create a user
    private static void createUser(){
        salesforceUser = new User(
        Alias = 'standt', 
        profileId = salesforceLicenseId ,
        Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8',
        CommunityNickname = 'test13',
        LastName='Testing',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',  
        TimeZoneSidKey='America/Los_Angeles',
        UserName='test008@testorg.com'
        );
        insert salesforceUser; 
    }

 //create an account record
    private static void createAccount(){
        //CustomSetting added by Abhirup
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
        insert vrcBroker ;
        Account acc = new Account(  Name='APEXTESTACC001',
        BillingCity = 'Bristol',
        company_Role__c =  'Broker',
        Sub_Roles__c =  'Broker - Reinsurance Broker',
        BillingCountry = 'United Kingdom',
        BillingPostalCode = 'BS1 1AD',
        BillingState = 'Avon' ,
        BillingStreet = '1 Elmgrove Road',
        Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id,
        Market_Area__c = TestDataGenerator.getMarketArea().Id,
        Responsible_CUO__c= salesforceUser.Id,
        OwnerId = salesforceUser.id
        );
        insert acc;
        strAccId = acc.Id; 
    }
 //create opportunity
        private static void createOpp(){
        Id strPIRecId =[SELECT Id FROM RecordType WHERE Name='P&I' AND sObjectType='Opportunity' ].id;
        opp = new Opportunity();
        opp.RecordTypeId = strPIRecId ;
        opp.Name = 'APEXTESTOPP001';
        opp.AccountId = strAccId;
        opp.StageName = 'Risk Evaluation';//Renewable Opportunity
        opp.Type = 'New Business';        
        opp.Business_Type__c = 'MOUs';
        opp.Approval_Criteria__c = 'Self Approval';
        opp.Sales_Channel__c = 'Direct';
        opp.CloseDate = Date.Today();
        opp.Confirm_not_on_sanction_list__c = true;
        opp.Amount = 100;
        insert opp;
        strOppId = opp.Id;
        recordTypeId = strPIRecId ;
    }
  
    //Create Opportunity checklist
        private static void createOppCList(){
        recordTypeId = [SELECT Id FROM RecordType WHERE Name='Charterers' AND sObjectType='Opportunity_Checklist__c'].id;
        testOppChecklists = new Opportunity_Checklist__c (Opportunity__c = strOppId ,
                                       Checklist_Completed__c = true,No_change__c = true ,
                                       Capital_Provider1__c = true,Capital_Provider_Comments__c='test',
                                       Broker__c=true,Broker_details_including_contact__c='test',
                                       Broker_Commission__c=true,Commission_Comments__c='test',
                                       RecordTypeId = recordTypeId );
                                       
        insert testOppChecklists ;
    }
    //Test Method
    @isTest public static void TestOTM(){
        createUser();
        createAccount();
        createOpp();
        createOppCList();
        createOppCList();
        PageReference pageRef = Page.OppChecklistRecordTypeSelection;
        Test.setCurrentPage(pageRef);
        Opportunity_Checklist__c  oc = new Opportunity_Checklist__c ();
        ApexPages.StandardController sc = new ApexPages.standardController(oc);
        ApexPages.currentPage().getParameters().put('masterId',strOppId);
        OppurtunityChecklistRecordTypeSelect OCRTypeSelect = new OppurtunityChecklistRecordTypeSelect(sc);
        
        Test.startTest();
        OCRTypeSelect.getRecordTypes();
        OCRTypeSelect.newOppChecklist() ;
        OppurtunityChecklistRecordTypeSelect.isLocked(opp.Id);
        Test.stopTest();
    }
}