@isTest(seealldata = false)
Class TestBrokerClientController
{
        public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
        public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
        public Static testMethod void brokerclientcontrol(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        
        Gardtestdata test_rec = new Gardtestdata();
        test_rec.commonrecord();
        
        ////***************************Inserting custom settings for seealldata=false*********************////
        
        SelectedClientId_AddBook__c addBookCS = new SelectedClientId_AddBook__c();
        addBookCS.Value__c = gardtestdata.ClientAcc.id;
        addBookCS.name = gardtestdata.BrokerContact.id;
        insert addBookCS;
        SelectedClientId_AddBook__c addBookCS1 = new SelectedClientId_AddBook__c();
        addBookCS1.Value__c = gardtestdata.ClientAcc.id;
        addBookCS1.name = gardtestdata.ClientContact.id;
        insert addBookCS1;
        CommonVariables__c Cvar2 = new CommonVariables__c(Name = 'DefaultProfilePic', value__c ='/mygard/profilephoto/005/T' );
        insert Cvar2;
            
          ///////****************************end************************************************////// 
        test.starttest(); 
        System.runAs(gardtestdata.brokerUser)
        {
           
            BrokerClientController brokerClientCnt = new BrokerClientController();
            System.debug('***setClientId '+brokerClientCnt.setClientId);
            //system.debug('********************** user contactid : '+user1.contactid__c);
            brokerClientCnt.strGlobalClient = gardtestdata.clientAcc.id;
            brokerClientCnt.setUWContactId.add(gardtestdata.clientcontact.id);
            brokerClientCnt.selectedGlobalClient = new list<string>();
            brokerClientCnt.selectedGlobalClient.add(gardtestdata.clientAcc.Name);
            List<SelectOption> bclst = brokerClientCnt.getItems();
            brokerClientCnt.setClientId.add(gardtestdata.brokerAcc.id);
            brokerClientCnt.addAddressList();
            brokerClientCnt.refreshList();
            brokerClientCnt.setUWContactId = new set<id>();
            brokerClientCnt.setUWContactId.add(gardtestdata.grdobj.id);            
            //brokerClientCnt.orderByUnderwriter();
            //brokerClientCnt.orderByUnderwriter();
            brokerClientCnt.setClaimContactId = new set<id>();
            brokerClientCnt.setClaimContactId.add(gardtestdata.grdobj.id);
            //brokerClientCnt.orderByClaimsHandler();
            //brokerClientCnt.orderByAccContact();
            //brokerClientCnt.orderByAccContact();
            /*brokerClientCnt.hasNextUW = true ;
            brokerClientCnt.hasPreviousUW = true;
            brokerClientCnt.pageNumberUW = 100;*/
           // brokerClientCnt.previousUW();
            //brokerClientCnt.nextUW();
            //brokerClientCnt.setpageNumberUW();
            //brokerClientCnt.hasNext = true;
            //brokerClientCnt.hasPrevious = true;
           // brokerClientCnt.pageNumber = 100;
            //brokerClientCnt.previous();
            //brokerClientCnt.next();
           // brokerClientCnt.setpageNumber();
            brokerClientCnt.section = 'UWR';
            brokerClientCnt.printData();
            brokerClientCnt.section = 'Claim';
            brokerClientCnt.printData();
            brokerClientCnt.section = 'AccCont';
            brokerClientCnt.printData();
            //brokerClientCnt.last(); 
            //brokerClientCnt.first();
            brokerClientCnt.updateAccountClientIds();
            brokerClientCnt.vcardId = GardTestData.grdobj.Id ;
            brokerClientCnt.downloadVcardBroker();
            brokerClientCnt.downloadGardContactsPDF();      
            brokerClientCnt.gotoNextTab();
            //brokerClientCnt.sorePhotoData();
            
        } 
      
        System.runAs(gardtestdata.clientUser)
        {  
           // test.starttest();
            BrokerClientController brokerClientCnt1 = new BrokerClientController();
            brokerClientCnt1.setClientId.clear();
            
            System.debug('***setClientId '+brokerClientCnt1.setClientId);
            brokerClientCnt1.strGlobalClient = gardtestdata.clientAcc.id;
            brokerClientCnt1.selectedGlobalClient = new list<string>();
            brokerClientCnt1.selectedGlobalClient.add(gardtestdata.clientAcc.Name);
            //brokerClientCnt1.getItems();
            //brokerClientCnt1.getImageUrl('');
            brokerClientCnt1.setClientId.add(gardtestdata.clientAcc.id);
            brokerClientCnt1.addAddressList();
            brokerClientCnt1.refreshList();
            //brokerClientCnt1.orderByUnderwriter();
            //brokerClientCnt1.orderByUnderwriter();
            //brokerClientCnt1.orderByClaimsHandler();
            ////brokerClientCnt1.orderByClaimsHandler();
            //brokerClientCnt1.orderByAccContact();
            //brokerClientCnt1.orderByAccContact();
            brokerClientCnt1.option = gardtestdata.clientAcc.Id;
            brokerClientCnt1.section = 'UWR';
            brokerClientCnt1.printData();
            brokerClientCnt1.section = 'Claim';
            brokerClientCnt1.printData();
            brokerClientCnt1.section = 'AccCont';
            brokerClientCnt1.printData();
            System.assert(brokerClientCnt1.lstgrdContactUWR.size() > 0 , true);
            brokerClientCnt1.strselected = gardtestdata.clientAcc.id+';'+gardtestdata.clientAcc.id;
            pageReference pg = brokerClientCnt1.fetchSelectedClients();
            ApexPages.StandardController scontroller = new ApexPages.StandardController(gardtestdata.ClientAcc);
            ApexPages.currentPage().getParameters().put('id',gardtestdata.ClientAcc.Id);
            FetchGardContactsInCRMCtrl fetchGCINCRM = new FetchGardContactsInCRMCtrl(scontroller);
        }
        Test.stopTest();
    }
}