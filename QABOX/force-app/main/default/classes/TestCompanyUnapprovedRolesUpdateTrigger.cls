@isTest
private class TestCompanyUnapprovedRolesUpdateTrigger  {
    
    static testMethod void testCompanyUnapprovedRoles(){
        //Create Account
        Account anAccount = new Account(  
            Name = 'Test Account',
            BillingCity = 'Bristol',
            BillingCountry = 'United Kingdom',
            BillingPostalCode = 'BS1 1AD',
            BillingState = 'Avon' ,
            BillingStreet = '1 Elmgrove Road',
            Company_Role__c = 'Broker',
            Unapproved_Company_Roles__c = 'External Service Provider',
            Unapproved_Company_Roles_to_Add__c = 'External Service Provider',
            Unapproved_Company_Roles_to_Remove__c = 'Broker',
            Sub_Roles_to_Remove__c = 'Broker - Broker',
            Sub_Roles_Unapproved__c = '',
            Sub_Roles_to_Add__c = 'ESP - Agent',
            IsRejected__c = false ,
            Area_Manager__c = UserInfo.getUserId()
            
        );
        insert anAccount;
        
        //Fetch the account
        Account fetchedAccount = [SELECT ID , Name , IsRejected__c from ACCOUNT WHERE Name = 'Test Account' LIMIT 1];
        fetchedAccount.IsRejected__c = true ;
        update fetchedAccount;
        
        
        
        
        Account fetchedAccountNew = [SELECT ID, Name, Unapproved_Company_Roles__c,Unapproved_Company_Roles_to_Add__c ,Unapproved_Company_Roles_to_Remove__c ,
                                        Sub_Roles_to_Remove__c ,
                                        Sub_Roles_Unapproved__c ,
                                        Sub_Roles_to_Add__c from ACCOUNT WHERE Name = 'Test Account' LIMIT 1];
        
        System.assertEquals(null,fetchedAccountNew.Unapproved_Company_Roles__c);
        System.assertEquals(null,fetchedAccountNew.Unapproved_Company_Roles_to_Add__c);
        System.assertEquals(null,fetchedAccountNew.Unapproved_Company_Roles_to_Remove__c);
        System.assertEquals(null,fetchedAccountNew.Sub_Roles_to_Remove__c);
        System.assertEquals(null,fetchedAccountNew.Sub_Roles_to_Add__c);
        
    }
    
}