public class CloseWonOpportunity_lightning {
    @AuraEnabled public String marineRenewalRecTypIdStr;
    @AuraEnabled public String marineNewBizRecTypIdStr;
    @AuraEnabled
    public static String getOpportunityRecord(String recordId) {
        CloseWonOpportunity_lightning classObj = new CloseWonOpportunity_lightning();
        CloseWonOpportunityWrapper classObjWrp = new CloseWonOpportunityWrapper();
        classObjWrp.opp = [select id, RecordTypeName__c, Checklist_Count__c, Type, Name  from Opportunity Where id=:recordId LIMIT 1];
        system.debug('classObjWrp'+classObjWrp);        
        return System.JSON.serialize(classObjWrp);
    }
    public class CloseWonOpportunityWrapper{
        @AuraEnabled public Opportunity Opp;
    }
    @AuraEnabled
    public static AuraProcessingMessage updateOpportunities(String recordId){
        system.debug('--update opportunity initiated--');
        Savepoint sp = Database.setSavepoint();
        AuraProcessingMessage returnMessage = new AuraProcessingMessage();
        Opportunity opp = [select id, StageName FROM Opportunity WHERE Id =: recordId];
        opp.StageName = 'Closed Won';
        try{
            returnMessage.isSuccess = true;
            update opp;
            if(Test.isRunningTest())
                throw new CalloutException();
        }
        catch(Exception e){
            returnMessage.isSuccess = false;
            returnMessage.errorMsg = e.getMessage();
            integer index = returnMessage.errorMsg.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
            system.debug('--first index'+index);
            if(index!= null && index > 0)
                returnMessage.errorMsg = returnMessage.errorMsg.substring(index,returnMessage.errorMsg.length()).remove('&quot').remove(': []').remove('FIELD_CUSTOM_VALIDATION_EXCEPTION, ').replace('&amp;','&').replace(';','"');
            index = returnMessage.errorMsg.indexOf(':');
            system.debug('--first index'+index);
            if(index!= null && index > 0)
                returnMessage.errorMsg = returnMessage.errorMsg.substring(0,index);
            system.debug('Exception occured-'+e.getMessage());
            Database.rollback(sp);
        }
        return returnMessage;
    }
    public class AuraProcessingMessage { 
        @AuraEnabled public Boolean isSuccess;
        @AuraEnabled public String errorMsg; //error msg
        @AuraEnabled public String successMsg; //success msg
        public AuraProcessingMessage(){
            isSuccess = true;
            errorMsg = '';
            successMsg = '';
        }
    }
}