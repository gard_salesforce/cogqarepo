/*************************************************************************************************
Author      : Rich Callear
Company     : cDecisions
Date Created: 
Description : This class provides support to the Logical Account Deletion vf page
Modified    : Martin Gardner  - Added the set Request flag method to direct the deletion via
an approval process. Also added the account record method to redirect back to the
account record.
***************************************************************************************************/
public with sharing class LogicalAccountDeletionExt {
    
    public account acc {get; set;}
    
    private ApexPages.StandardController stdCtrl {get; set;}
    public Boolean DisplaySection{get;set;}
    public Boolean success{get;set;}//SF-4414 to give out errors
    public Boolean deletedFlagOriginalValue{get;set;} // SF-4414 used to save the older state for Deleted__c flag which would be chagned even if some error occurs when Updating, in some other trigger
    public Boolean isESP{get;set;}
    public Boolean reasonBlank{get;set;}
    
    public logicalAccountDeletionExt(ApexPages.StandardController std) 
    {
        //acc = (account)std.getRecord();
        isESP = false;
        acc = [select id, Deleted__c, On_Risk__c, Broker_On_Risk__c, Client_On_Risk__c, Company_Status__c, Company_Role__c, Sub_Roles__c,MyGard_enabled__c,Reason_for_deactivation__c from Account where Id = :std.getId()];   
        stdctrl = std;
        DisplaySection = false;
        success = false;
        deletedFlagOriginalValue = acc.deleted__c;
        reasonBlank = false;
        if(acc.Company_Role__c != null && acc.Company_Role__c.contains('External Service Provider')) isESP = true;
    }
    
    //Not In Use
    public pagereference setFlag()
    {
        if (!acc.Deleted__c){
            acc.deleted__C = true;
            stdctrl.save();
        }
        return null;
    }
    
    public pageReference setRequestFlag()
    {
        try{
            if (!acc.Deleted__c && !acc.Broker_On_Risk__c && !acc.Client_On_Risk__c && !acc.On_Risk__c && !acc.MyGard_enabled__c && !isESP) {
                //governance disabled
                //if (!acc.Deleted__c && !acc.Broker_On_Risk__c && !acc.Client_On_Risk__c && !acc.MyGard_enabled__c) {
                System.Debug('LogicalAccountDeletion*** Governance Disabled ****');
                acc.ParentId = null; //SF-4414
                acc.Company_Status__c = 'Inactive';
                acc.Company_Role__c = '';
                acc.Sub_Roles__c = '';
                acc.Deleted__c = true;
                acc.Sales_Target_v2__c = '';	//SF-6613	
                acc.Upsell_Target_Old__c = false;	//SF-6613	
                acc.Target_claims_lead__c = false;	//SF-6613
                update acc;
                success = true;
                
            }else if(isESP){
                if((!acc.Deleted__c && !acc.Broker_On_Risk__c && !acc.Client_On_Risk__c && !acc.On_Risk__c && !acc.MyGard_enabled__c) && (acc.Reason_for_deactivation__c != null && acc.Reason_for_deactivation__c != '')){
                    acc.ParentId = null; //SF-4414
                    acc.Company_Status__c = 'Inactive';
                    acc.Company_Role__c = '';
                    acc.Sub_Roles__c = '';
                    acc.Deleted__c = true;
                    acc.Sales_Target_v2__c = '';	//SF-6613	
                    acc.Upsell_Target_Old__c = false;	//SF-6613	
                    acc.Target_claims_lead__c = false;	//SF-6613
                    update acc;
                    success = true;
                }else if(acc.Reason_for_deactivation__c == null || acc.Reason_for_deactivation__c == ''){
                    success = false;
                    reasonBlank = true;
                }
            }
        }Catch(Exception ex){
            success = false;
            System.debug('LogicalAccountDeletion Exception Occurred - '+ex.getMessage());
            System.debug('LogicalAccountDeletion Exception Occurred - '+ex);
            //DisplaySection = true;
            //apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You do not have permissions to edit Correspondents.'));
            //apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage()));
        }
        System.debug('LogicalAccountDeletion acc.Deleted__c - '+acc.Deleted__c);
        System.debug('LogicalAccountDeletion deletedFlagOriginalValue - '+deletedFlagOriginalValue);
        System.debug('LogicalAccountDeletion success - '+this.success);
        return null;
    }
    
    public pagereference accountsHome()
    {
        pagereference home = new pagereference('/001/o');
        return home;
    }
    public pagereference accountRecord()
    {
        pagereference act = new pagereference('/' + acc.Id);
        return act;
    }
    //Method used in lightning component available in account detail page
    @auraEnabled
    public static String setRequestFlagFromLightning(id accId){
        outputJSON opJSON = new outputJSON();
        
        Account acc = [SELECT 
                       id,Deleted__c,On_Risk__c,Broker_On_Risk__c,Client_On_Risk__c,Company_Status__c,
                       Company_Role__c,Sub_Roles__c,MyGard_enabled__c,Reason_for_deactivation__c,Account_Sync_Status__c
                       FROM Account
                       WHERE Id = :accId];
        opJSON.isESPFlag = (acc.Company_Role__c != null && acc.Company_Role__c.contains('External Service Provider'));
        
        Boolean haltDeletion = false;//to set precedence of errors,arrange the following if statements
        if(!haltDeletion && acc.Deleted__c){opJSON.deletedFlagOriginalVal = true;haltDeletion = true;}
        if(!haltDeletion && (acc.Broker_On_Risk__c || acc.Client_On_Risk__c || acc.On_Risk__c)){opJSON.onRisk = true;haltDeletion = true;}
        if(!haltDeletion && acc.MyGard_enabled__c){opJSON.myGardenabled = true;haltDeletion = true;}
        if(!haltDeletion && opJSON.isESPFlag && ([SELECT count() FROM ESP_Contact_Skills__c WHERE ESP_Contact_skill__r.accountid = :acc.id AND ESP_Contact_skill__r.No_Longer_Employed_by_Company__c = false] > 0)){opJSON.hasEspContactSkills = true;haltDeletion = true;}
        if(!haltDeletion && (opJSON.isESPFlag && String.isBlank(acc.Reason_for_deactivation__c))){opJSON.reasonBlankFlag = true;haltDeletion = true;}
        if(!haltDeletion && (acc.Account_Sync_Status__c != 'Synchronised')){opJSON.exceptionOccured = true;haltDeletion = true;}
        
        if(!haltDeletion){//The company isnt deleted already, is neither on risk and nor MyGardEnabled{
            acc.ParentId = null; //SF-4414
            acc.Company_Status__c = 'Inactive';
            acc.Company_Role__c = '';
            acc.Sub_Roles__c = '';
            acc.Deleted__c = true;
            acc.Sales_Target_v2__c = '';	//SF-6613	
            acc.Upsell_Target_Old__c = false;	//SF-6613	
            acc.Target_claims_lead__c = false;	//SF-6613
            try{
                update acc;
                opJSON.successFlag = true;    
            }Catch(DmlException dmlEx){
                System.debug('LogicalAccountDeletion Exception Occurred - '+dmlEx.getMessage());
                if(dmlEx.getMessage().containsIgnoreCase('INSUFFICIENT_ACCESS_OR_READONLY')){
                    opJSON.insufficientAccessException = true;
                }
                throw dmlEx;
            }Catch(Exception ex){
                opJSON.exceptionOccured = true;
                System.debug('LogicalAccountDeletion Exception Occurred - '+ex);
            }
        }
        
        
        System.debug('LogicalAccountDeletion return accId - '+accId);
        System.debug('LogicalAccountDeletion return opJSON - '+opJSON);
        System.debug('LogicalAccountDeletion return opJSON.deletedFlagOriginalVal - '+opJSON.deletedFlagOriginalVal);
        System.debug('LogicalAccountDeletion return opJSON.isESPFlag - '+opJSON.isESPFlag);
        
        return JSON.serialize(opJSON);
    }
    public class outputJSON{
        Boolean deletedFlagOriginalVal = false;
        Boolean onRisk = false;
        Boolean myGardenabled = false;
        Boolean isESPFlag = false;
        Boolean reasonBlankFlag = false;
        Boolean successFlag = false;
        Boolean exceptionOccured = false;
        Boolean hasEspContactSkills = false;//added for SF-6207
        Boolean insufficientAccessException = false;//added for SF-6207
    }
}