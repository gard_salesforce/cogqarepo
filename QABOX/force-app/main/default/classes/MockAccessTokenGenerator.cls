@isTest

global class MockAccessTokenGenerator implements HttpCalloutMock {
  global HTTPResponse respond(HTTPRequest req) {
    HttpResponse res = new HttpResponse();
       
        res.setbody('{'
    +'"access_token": "Jvdbt4n2HfzT_euMnQmJfSksRE1Kwd8du605JPwBYGfkdmQqgGEvC8HzntRdMnrxXfxZgBqaSd2SLLBPlEgVLQ",'
    +'"refresh_token": "gjY81jCY2O-1YsSBaspV3eXDyv-IhasTE8tjVrAEMGfdjtyE89D08WuBBTUn0IUN_jXBwIO_e68sjI259DkvUg",'
    +'"scope": "READ WRITE",'
    +'"token_type": "Bearer",'
    +'"expires_in": 3600}');
    res.setStatusCode(200);
     return res;
        
  }
  }