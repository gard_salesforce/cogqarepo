public class CompanyOnRiskInfoUpdateCtrl{
    @future(callout=true)
    public static void fetchOnRiskInfo(){//(List<String> existingOnRiskPartnerIdList){
        List<CompanyOnRiskInfoResponseWrapper> companyOnRiskInfoList = new List<CompanyOnRiskInfoResponseWrapper>();
        List<String> newPartnerIdList = new List<String>();     //Holds the Partner Ids that are coming from PARIS
        List<String> accountIdsToReset = new List<String>();    //Holds the Partner Ids that are present in SF, but not coming from PARIS
        Map<Integer,ID> companyIdAccMap = new Map<Integer,ID>();
        try{
            AccessTokenResponseWrapper.AccessTokenResponse accessToken = AccessTokenGenerator.getAccessTokenResponse('Retreive access token');
            system.debug('Access token:'+accessToken);
            Mule_Web_Service__c onRiskEPInfo = Mule_Web_Service__c.getInstance('Company On Risk Information');
            Http sendRequest = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(onRiskEPInfo.Endpoint_Url__c+String.valueOf(Date.today()).removeEnd(' 00:00:00')+System.Label.Mule_OnRisk_query_Param);    //String.valueOf(Date.today().addDays(-1)).removeEnd(' 00:00:00'));'2021-02-08'
            request.setheader('Content-Type','application/json');
            request.setheader('Authorization',accessToken.token_type + ' '+ accessToken.access_token);
            request.setTimeout(20000);
            request.setMethod('GET');
            HttpResponse response = sendRequest.send(request);
            if(response.getStatusCode() == 200){
                companyOnRiskInfoList = (List<CompanyOnRiskInfoResponseWrapper>)JSON.deserialize(response.getBody(),List<CompanyOnRiskInfoResponseWrapper>.class);
                if(companyOnRiskInfoList.size() == 0){CompanyOnRiskInfoUpdateCtrl.logErrorResponse('CompanyOnRiskInfoUpdateCtrl (@future)','Response contains no value.','Empty result set received','');}
                else{
                    //Check delta data
                    for(CompanyOnRiskInfoResponseWrapper onRiskInfo:companyOnRiskInfoList){
                        newPartnerIdList.add(String.valueOf(onRiskInfo.crmId));
                    }
                    for(Account acc:[SELECT Id,Company_id__c FROM Account WHERE Company_id__c IN:newPartnerIdList]){
                        if(acc != null)
                        companyIdAccMap.put(Integer.valueOf(acc.Company_id__c),acc.Id);
                    }
                    List<String> existingOnRiskPartnerIdList = new List<String>();
                    for(Company_on_risk_information__c existingOnRiskInfo:[Select CRM_Id__c FROM Company_on_risk_information__c]){
                        existingOnRiskPartnerIdList.add(existingOnRiskInfo.CRM_Id__c);
                        if(!newPartnerIdList.contains(existingOnRiskInfo.CRM_Id__c))
                            accountIdsToReset.add(existingOnRiskInfo.CRM_Id__c);     //Companies that need to be reset.
                    }
                    CompanyOnRiskInfoUpdateCtrl.resetAccounts(accountIdsToReset);
                    /*
                    for(String existingPartenerId:existingOnRiskPartnerIdList){
                    if(!(newPartnerIdList.contains(existingPartenerId)))
                    accountIdsToReset.add(existingPartenerId);     //Companies that need to be reset.
                    }*/
                    //Insert data
                    List<String> incomingParterIdList = new List<String>();
                    List<Company_on_risk_information__c> companyOnRiskResponseList = new List<Company_on_risk_information__c>();
                    
                    integer nullPartnerIdCount = 0;
                    //Account relatedAcc;
                    for(CompanyOnRiskInfoResponseWrapper onRiskInfo:companyOnRiskInfoList){
                        Company_on_risk_information__c comOnRisks = new Company_on_risk_information__c(
                            Assured_On_Risk__c = onRiskInfo.assuredOnRisk ,
                            Broker_On_Risk__c = onRiskInfo.brokerOnRisk ,
                            Builders_On_Risk__c = onRiskInfo.buildersOnRisk ,
                            Client_On_Risk__c = onRiskInfo.clientOnRisk ,
                            Coassured_On_Risk__c = onRiskInfo.coassuredOnRisk ,
                            Company_On_Risk__c = onRiskInfo.companyOnRisk ,
                            Energy_On_Risk__c = onRiskInfo.energyOnRisk ,
                            Insurance_Company_On_Risk__c = onRiskInfo.insurancecompanyOnRisk ,
                            Marine_On_Risk__c = onRiskInfo.marineOnRisk ,
                            Original_Insured_Mutual_On_Risk__c = onRiskInfo.originalInsuredMutualOnRisk ,
                            Original_Insured_Small_Craft_On_Risk__c = onRiskInfo.originalInsuredSmallCraftOnRisk ,
                            P_I_Charterers_On_Risk__c = onRiskInfo.pandiCharterersOnRisk ,
                            P_I_MOUOnRisk__c = onRiskInfo.pandiMOUOnRisk ,
                            P_I_Owners_On_Risk__c = onRiskInfo.pandiOwnersOnRisk ,
                            //Parent_Company__c = onRiskInfo. ,
                            Partner_Id__c = String.valueOf(onRiskInfo.partnerId) ,
                            Name = onRiskInfo.partnerName,
                            Partner_Type_Code__c = onRiskInfo.partnerTypeCode ,
                            Partner_Type_Description__c = onRiskInfo.partnerTypeDescription ,
                            Protected_Coassured_On_Risk__c = onRiskInfo.protectedCoassuredOnRisk ,
                            Stop_Use__c = onRiskInfo.stopUse,
                            CRM_Id__c = String.valueOf(onRiskInfo.crmId)
                        );
                        if(onRiskInfo.crmId != null){
                            //relatedAcc = companyIdAccMap.get(onRiskInfo.crmId);
                            //system.debug('Account in map:'+relatedAcc);
                            //if(relatedAcc != null)
                                comOnRisks.Related_company__c = companyIdAccMap.get(onRiskInfo.crmId);//relatedAcc.Id;
                        }
                        companyOnRiskResponseList.add(comOnRisks);
                        if(comOnRisks.CRM_Id__c == null || comOnRisks.CRM_Id__c == '') {nullPartnerIdCount++;}
                    }
                    upsert companyOnRiskResponseList Partner_Id__c;
                    
                    if(nullPartnerIdCount > 0){CompanyOnRiskInfoUpdateCtrl.logErrorResponse('CompanyOnRiskInfoUpdateCtrl (@future)','Number of null Partner Ids received :'+String.ValueOf(nullPartnerIdCount),'Null PartnerId in response','');}
                    system.debug('Upsert successful. Record count:'+companyOnRiskResponseList.size());
                }
            }else{
                system.debug('Exception occured with status code: '+response.getStatusCode()+' - '+response.getStatus());
                String responseMsg = 'Error in response: \n Status code:'+response.getStatusCode()+'\n Status : '+response.getStatus();
                CompanyOnRiskInfoUpdateCtrl.logErrorResponse('CompanyOnRiskInfoUpdateCtrl (@future)',responseMsg,'Error in response','');
            }
            
        }catch(Exception ex){Logger.LogException('CompanyOnRiskInfoUpdateCtrl (@Future, ex)',ex,'');}
    }
    public static void resetAccounts(List<String> accountIdsToReset){
        system.debug('Account id count in parameter:'+accountIdsToReset.size());
        List<Account> accountsToReset = new List<Account>();
        //Reset accounts in accountIdsToReset variable
        for(Account acc:[SELECT id,P_I_Member_Flag__c,M_E_Client_Flag__c,Client_On_Risk_Flag__c,Broker_On_Risk_Flag__c,On_Risk__c FROM Account WHERE Company_id__c IN: accountIdsToReset]){
            acc.P_I_Member_Flag__c = false;
            acc.M_E_Client_Flag__c = false;
            acc.Client_On_Risk_Flag__c = false;
            acc.Broker_On_Risk_Flag__c = false;
            acc.On_Risk__c = false;
            //acc.Small_craft_client__c = false;    //Commented as it is populated based on last 5years' opportunity.
            acc.Assured_On_Risk__c = false;
            acc.Builders_On_Risk__c = false;
            acc.Coassured_On_Risk__c = false;
            acc.Energy_On_Risk__c = false;
            acc.Insurance_Company_On_Risk__c = false;
            acc.Marine_On_Risk__c = false;
            acc.Original_Insured_Mutual_On_Risk__c = false;
            acc.P_I_Charterers_On_Risk__c = false;
            acc.P_I_MOUOnRisk__c = false;
            acc.P_I_Owners_On_Risk__c = false;
            acc.Protected_Coassured_On_Risk__c = false;
            accountsToReset.add(acc);
        }
        system.debug('Number of accounts to reset:'+accountsToReset.size());
        if(accountsToReset != null && accountsToReset.size() > 0)
            update accountsToReset;
        //Delete the onRisk records that are not sent from PARIS
        if(accountIdsToReset != null && accountIdsToReset.size() > 0){
            List<Company_on_risk_information__c> onRiskRecordsToDelete = new List<Company_on_risk_information__c>();
            onRiskRecordsToDelete = [SELECT id FROM Company_on_risk_information__c WHERE CRM_Id__c IN: accountIdsToReset];
            if(onRiskRecordsToDelete != null && onRiskRecordsToDelete.size() > 0)
                delete onRiskRecordsToDelete;
        }
    }
    public static void logErrorResponse(String source, String msg, String Type, String additionalDetails){
        Exception_Log__c exLog = new Exception_Log__c();
        exLog.source__c = source;
        exLog.exception_Message__c = msg;
        exLog.Type_Name__c = Type;
        exLog.additional_Details__c = additionalDetails;
        insert exLog;
    }
}