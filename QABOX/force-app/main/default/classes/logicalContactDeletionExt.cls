/*************************************************************************************************
    Author      : Martin Gardner
    Company     : cDecisions
    Date Created: 
    Description : This class provides support for overriding the delete function in certain circumstances
    Modified    :   
***************************************************************************************************/
public with sharing class logicalContactDeletionExt {
	
	public boolean WasDeleted {get;set;}
	public boolean deleteOk {get;set;}
    public contact con {get; set;}
    private ApexPages.StandardController stdCtrl {get; set;}

    public logicalContactDeletionExt(ApexPages.StandardController std) {
      con = (contact)std.getRecord();
      if(con.document_addressee__c){
      	deleteOk = false;
      }else{
      	deleteOk = true;
      }
	  WasDeleted = con.Deleted__c;
      stdctrl = std;
    }
    public void setFlag()
    {
		if (!con.Deleted__c) {
	        if(deleteOk){
	        	 con.deleted__C = true;
	            stdctrl.save();
	        }          
		}
    }
    
    public pagereference contactsHome()
    {
            pagereference home = new pagereference('/003/o');
            return home;
    }   
    public pagereference contactsRecord()
    {
            pagereference home = new pagereference('/' + con.Id);
            return home;
    }   
    public pagereference deleteAddressee(){
    	deleteOk = true;
    	setFlag();
    	return(contactsRecord());
    }

}