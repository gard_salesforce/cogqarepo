//created by Pulkit for SF-5197
//To be used as extension to all 3 UWR forms to store dynamic number of assureds/co-assureds,etc.
public class UwrAssuredsExtension{
    //instance variables
    public String parentUWRFormId{get;set;}
    public List<UwrAssuredsWrapper> uAWs{get;set;}
    public Integer rowIndexExt{get;set;} //to delete a row
    public List<UWRAssured__c> uwrAssureds{get;set;}
    public Boolean doneAssuredsProcessing{get;set;} //to show confirmation message
    
    //prepopulated drop-down select options
    public List<SelectOption> allMemberTypesSO{get;set;}
    public List<SelectOption> allCountriesSO{get;set;}
    public List<SelectOption> allCapacitiesSO{get;set;}
    
    //static values of lists
    private static List<String> memberTypes;
    private static List<String> countries;
    private static List<String> capacities;
    
    //later, maybe fetched from somewhere than to be hardcoded
    static{
        memberTypes = new List<String>{
            'Assured',
            'Co-Assured',
            'Protective co-insurance'
        };
                    
		countries = new List<String>();
        for(Country__c country : [SELECT Country_Name__c FROM Country__c ORDER BY Country_Name__c]){
            countries.add(country.Country_Name__c);
        }
        
        capacities = new List<String>{
            'Bareboat Charterers',
			'Beneficial Owners',
			'Registered Owners',
			'Managers',
			'Technical Managers',
			'Commercial Managers',
			'Crew Agents',
			'Crew Managers',
			'Pool Managers',
			'Affiliated/Associated Charterers',
            'Other'
        };
        capacities.sort();
     }
    
    //constructor
    //when using this as Controller
    public UwrAssuredsExtension(){
        doneAssuredsProcessing = false;
        populateMemberTypes();
        populateCountries();
        populateCapacities();
        addRow();
	}
	
	//when using this as Extension
    public UwrAssuredsExtension(UWRForShipOwnersCtrl uwrFormCtrl){//shipOwers Form Extension
        doneAssuredsProcessing = false;
        populateMemberTypes();
        populateCountries();
        populateCapacities();
        addRow();
    }
    
    public UwrAssuredsExtension(UWRMOUCtrl uwrFormCtrl){//MOU Form Extension
        doneAssuredsProcessing = false;
        populateMemberTypes();
        populateCountries();
        populateCapacities();
        addRow();
    }
    
    public UwrAssuredsExtension(UWRforOffshoreVesselCtrl uwrFormCtrl){//offshoreVessel Form Extension
        doneAssuredsProcessing = false;
        populateMemberTypes();
        populateCountries();
        populateCapacities();
        addRow();
    }
    
    public UwrAssuredsExtension(UWRForEntryFormforCharterClientsCtrl uwrFormCtrl){//EntryFormForCharteredVessel Form Extension
        doneAssuredsProcessing = false;
        populateMemberTypes();
        populateCountries();
        populateCapacities();
        addRow();
    }
    
    public UwrAssuredsExtension(UWREntryFormCtrl uwrFormCtrl){//EntryFormForCharteredVessel Form Extension
        doneAssuredsProcessing = false;
        populateMemberTypes();
        populateCountries();
        populateCapacities();
        addRow();
    }
    
    //methods for pre-populating lists for drop-downs
    private void populateMemberTypes(){
        if(allMemberTypesSO == null){
            allMemberTypesSO = new List<SelectOption>();
            for(String memberType : memberTypes){
                allMemberTypesSO.add(new SelectOption(memberType,memberType));
            }
        }
    }
    
    private void populateCountries(){
        if(allCountriesSO == null){
            allCountriesSO = new List<SelectOption>();
            allCountriesSO.add(new SelectOption('',''));
            for(String country : countries){
                allCountriesSO.add(new SelectOption(country,country));
            }
        }
    }

    private void populateCapacities(){
        if(allCapacitiesSO == null){
            allCapacitiesSO = new List<SelectOption>();
            allCapacitiesSO.add(new SelectOption('',''));
            for(String capacity : capacities){
                allCapacitiesSO.add(new SelectOption(capacity,capacity));
            }
        }
    }
    
    public PageReference addRow(){
        System.debug('addRow run uAWs - '+uAWs);
        uAWs = (uAWs == null ? new List<UwrAssuredsWrapper>() : uAWs);
        System.debug('addRow run uAWs.size() - '+uAWs.size());
        uAWs.add(new UwrAssuredsWrapper());
        return null;
    }

    public PageReference deleteRow(){
        System.debug('deleteRow rowIndexExt -'+rowIndexExt);
        if(rowIndexExt != null){
            uAWs.remove(rowIndexExt);
            rowIndexExt = null;
        }
        return null;
    }
    
    //inserts/creates new UWRAssured__c records
    public void saveAssureds(){
        System.debug('saveAssureds parentUWRFormId - '+parentUWRFormId);
        this.doneAssuredsProcessing = true;
        if(uAWs == null || uAWs.size() < 1 || parentUWRFormId == null) return;
        
        this.parentUWRFormId = parentUWRFormId;//to link assureds to UWF form
        wrappersToRecords(); //convert wrappers to Object's records
        
        //if(Test.isRunningTest()) return;
        
        List<Database.SaveResult> dbSRs = Database.insert(uwrAssureds,false);//dml
    }
    
    //Wrapper for each Assured
    public class UwrAssuredsWrapper{
        public String companyName{get;set;}//ft
        public String memberType{get;set;}//dd
        public String country{get;set;}//dd
        public String city{get;set;}//ft
        public String capacity{get;set;}//dd
        public String capacityText{get;set;}//ft
        public String natRegNo{get;set;}//ft
        public Boolean isMember{get;set;}//cb
        @TestVisible private Id parentUWRForm{get;set;}//N/A
        
        public UwrAssuredsWrapper(){
            this.companyName = '';
            this.memberType = '';
            this.country = '';
            this.city = '';
            this.capacity = '';
            this.capacityText = '';
            this.natRegNo = '';
            this.isMember = false;
            this.parentUWRForm = null;
        }
        
    }
    
    //convert wrapper instances to object-record instances
    //returns a list of UWRAssured__c
    public void wrappersToRecords(){
        System.debug('wrappersToRecords parentUWRFormId - '+parentUWRFormId);
        if(uAWs == null || parentUWRFormId == null) return;
        uwrAssureds = new List<UWRAssured__c>();
        for(UwrAssuredsWrapper uAW : uAWs){
            if(!String.isBlank(uAW.companyName)){
                uwrAssureds.add(
                    new UWRAssured__c(
                        Name = uAW.companyName,
                        Member_Type__c = uAW.memberType,
                        Country__c = uAW.country,
                        City__c = uAW.city,
                        Capacity__c = ((!uAW.capacity.equalsIgnoreCase('other') || String.isBlank(uAW.capacityText)) ? (uAW.capacity) : (uAW.capacity+' - '+uAW.capacityText)), //If Other was selected on the page, append Capcity text along with the "Other" text
                        National_Registration_Number__c = uAw.natRegNo,
                        isMember__c = uAW.isMember,
                        UWR_Form__c = parentUWRFormId
                    )
                );
            }
        }
    }
    
    public static List<SelectOption> getAllCapacitiesSO(){
        List<SelectOption> allCapacitiesSO = new List<SelectOption>();
        allCapacitiesSO.add(new SelectOption('',''));
        for(String capacity : capacities){
            allCapacitiesSO.add(new SelectOption(capacity,capacity));
        }
        return allCapacitiesSO;
    }
}