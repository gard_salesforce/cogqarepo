@isTest(seealldata = false)
public class TestRequestPageCtrl{
    private static Gardtestdata gtd;
    private static final String urlParam = 'MyObjectDetails';
    
    //Test Data Setup Method
    private static void createTestData(){
        gtd = new Gardtestdata();
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        MLC_Certificate_availability__c MLC_Certificate_CS_Obj = new MLC_Certificate_availability__c(Name = 'MLC availability', value__c = true);       
        insert MLC_Certificate_CS_Obj ; 
        Blue_Card_renewal_availability__c BlueCardAvailability = new Blue_Card_renewal_availability__c(Name = 'value' , BlueCardRenewalAvailability__c = false);        
        insert BlueCardAvailability ;
        gtd.commonrecord();
    }
    
    //Test Methods
    @isTest public static void testRequestPageCtrl(){
        createTestData();
        
        Test.startTest();
        System.runAs(Gardtestdata.brokerUser){
            RequestPageCtrl test= new RequestPageCtrl();
            test.selectedRequest=Gardtestdata.formtype_1;
            test.requestChnage();
            test.selectedRequest = Gardtestdata.formtype_2;
            test.prvUrl = urlParam;
            test.requestChnage();
            test.selectedRequest=Gardtestdata.formtype_3;
            test.requestChnage();
            test.selectedRequest=Gardtestdata.formtype_4;
            test.requestChnage();
            test.selectedRequest=Gardtestdata.formtype_5;
            test.requestChnage();
            test.selectedRequest=Gardtestdata.formtype_6;
            test.requestChnage();
            test.selectedRequest=Gardtestdata.formtype_7;
            test.requestChnage();
            test.selectedRequest=Gardtestdata.formtype_8;
            test.requestChnage(); 
            test.selectedRequest=Gardtestdata.formtype_9;
            test.requestChnage();
            test.selectedRequest=Gardtestdata.formtype_10;
            //test.prvUrl='MyDetails';
            test.requestChnage();
            test.selectedRequest=Gardtestdata.formtype_11;
            test.requestChnage();
            test.selectedRequest=Gardtestdata.formtype_12;
            test.requestChnage();
            test.selectedRequest=Gardtestdata.formtype_13;
            test.requestChnage();
            
            
            System.assertEquals(test.marine , true);
            test.redirectToLoc();
            test.chkProdArea();
        }
        Test.stopTest();
        //invoking methods for RequestChangeCtrl for broker and client users......
    }
}