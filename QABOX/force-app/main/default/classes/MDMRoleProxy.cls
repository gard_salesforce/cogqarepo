/**************************************************************************
* Author  : Phil Spenceley
* Company : cDecisions
* Date    : 21/10/2013
***************************************************************************/
    
/// <summary>
///  This proxy class provides methods to access the MDM Roles Service
/// </summary>
public without sharing class MDMRoleProxy extends MDMProxy {

	private Set<String> getMDMRoleFields(String requestName) {
		Set<String> fields = new Set<String>();
		for (MDM_Service_Fields__c field : MDM_Service_Fields__c.getAll().values()) {
			if (requestName.equalsIgnoreCase(field.Request_Name__c) && 'MDM_Role__c'.equalsIgnoreCase(field.Source_Object__c) && 'Field'.equalsIgnoreCase(field.Type__c)) {
				fields.add(field.Field_Name__c.toLowerCase());
			}
		}
		return fields;
	}
	private MDM_Role__c getMDMRoleById(id MDMRoleId, String requestName) {
		return (MDM_Role__c)Database.query(buildSoqlRequest(getMDMRoleFields(requestName), 'MDM_Role__c', MDMRoleId));
    }
    private List<MDM_Role__c> getMDMRolesByIds(List<id> MDMRoleIds, String requestName) {
        return (List<MDM_Role__c>)Database.query(buildSoqlRequest(getMDMRoleFields(requestName), 'MDM_Role__c', MDMRoleIds));
    }
	
	private MDMUpsertRoleResponse MDMUpsertRoles(MDMUpsertRoleRequest request) {
        DOM.Document responseDoc = sendRequest(request);
		MDMUpsertRoleResponse response = new MDMUpsertRoleResponse();
		try {
			response.Deserialize(
	            responseDoc
	                .getRootElement()
	                .getChildElement(SOAP_Body, SOAPNS)
	                .getChildElement(MDMChangeResponse_Label, MDMChangeResponse_Namespace)
	        );
		} catch (Exception ex) {
			throw new MDMDeserializationException(responseDoc, 'Failed to deserialize MDMRoleProxy.MDMUpsertRoleResponse', ex);
		}
		return response;
    }
	private MDMDeleteRoleResponse MDMDeleteRoles(MDMDeleteRoleRequest request) {
        DOM.Document responseDoc = sendRequest(request);
		MDMDeleteRoleResponse response = new MDMDeleteRoleResponse();
		try {
			response.Deserialize(
	            responseDoc
	                .getRootElement()
	                .getChildElement(SOAP_Body, SOAPNS)
	                .getChildElement(MDMChangeResponse_Label, MDMChangeResponse_Namespace)
	        );
		} catch (Exception ex) {
			throw new MDMDeserializationException(responseDoc, 'Failed to deserialize MDMRoleProxy.MDMDeleteRoleResponse', ex);
		}
		return response;
    }
	
	public void MDMUpsertRole(Map<id, MDM_Role__c> upsertRoles) {
		MDMUpsertRoleRequest request = new MDMUpsertRoleRequest();
		Map<id, MDM_Role__c> mdmRoles = new Map<id, MDM_Role__c>(getMDMRolesByIds(new List<id> (upsertRoles.keySet()), request.getName()));
		
		try {
			request.MDMRoles = mdmRoles.values();
            MDMUpsertRoleResponse response = MDMUpsertRoles(request);
			for (MDMResponse mdmResponse : response.MDMResponses) {
				System.debug('*** MDMResponse:[ id: + ' + mdmResponse.id + ', success: ' + string.valueOf(mdmResponse.success) + ']');
				if (mdmResponse.Success) {
	                mdmRoles.get(mdmResponse.Id).Synchronisation_Status__c = 'Synchronised';
	            } else {
	                mdmRoles.get(mdmResponse.Id).Synchronisation_Status__c = 'Sync Failed';
	            }	
			}
			
			update mdmRoles.values(); 
			
        } catch (MDMDeserializationException ex) {
			Logger.LogException('MDMRoleProxy.MDMUpsertRoleAsync (@Future, ex)', ex, ex.xmlString);
			if (mdmRoles != null) {
				SetRolesSyncFailed(mdmRoles.values(), 'MDMRoleProxy.MDMUpsertRoleAsync (@Future, innerEx)');
			}
			
        } catch (Exception ex) {
			Logger.LogException('MDMRoleProxy.MDMUpsertRoleAsync (@Future, ex)', ex);
			if (mdmRoles != null) {
				SetRolesSyncFailed(mdmRoles.values(), 'MDMRoleProxy.MDMUpsertRoleAsync (@Future, innerEx)');
			}
        }
	}
	
	public void MDMDeleteRole(Map<id, MDM_Role__c> deleteRoles) {
		MDMDeleteRoleRequest request = new MDMDeleteRoleRequest();
		Map<id, MDM_Role__c> mdmRoles = new Map<id, MDM_Role__c>(getMDMRolesByIds(new List<id> (deleteRoles.keySet()), request.getName()));
		
        try {
			request.MDMRoles = mdmRoles.values();
            MDMDeleteRoleResponse response = MDMDeleteRoles(request);
            //do something with the response...
            for (MDMResponse mdmResponse : response.MDMResponses) {
				System.debug('*** MDMResponse:[ id: + ' + mdmResponse.id + ', success: ' + string.valueOf(mdmResponse.success) + ']');
				if (mdmResponse.Success) {
	                mdmRoles.get(mdmResponse.Id).Synchronisation_Status__c = 'Synchronised';
	            } else {
	                mdmRoles.get(mdmResponse.Id).Synchronisation_Status__c = 'Sync Failed';
	            }	
			}
			
			update mdmRoles.values(); 
        } catch (MDMDeserializationException ex) {
			Logger.LogException('MDMRoleProxy.MDMDeleteRoleAsync (@Future, ex)', ex, ex.xmlString);
			if (mdmRoles != null) {
				SetRolesSyncFailed(mdmRoles.values(), 'MDMRoleProxy.MDMDeleteRoleAsync (@Future, innerEx)');
			}
			
        } catch (Exception ex) {
			Logger.LogException('MDMRoleProxy.MDMDeleteRoleAsync (@Future, ex)', ex);
			if (mdmRoles != null) {
				SetRolesSyncFailed(mdmRoles.values(), 'MDMRoleProxy.MDMDeleteRoleAsync (@Future, innerEx)');
			}
        }
	}
	
	@future (callout = true)
    public static void MDMUpsertRoleAsync(list<id> mdmRoleIds) {
		MDMRoleProxy proxy = new MDMRoleProxy();
		MDMUpsertRoleRequest request = new MDMUpsertRoleRequest();
        Map<id, MDM_Role__c> mdmRoles = new Map<id, MDM_Role__c>(proxy.getMDMRolesByIds(mdmRoleIds, request.getName()));
		proxy.MDMUpsertRole(mdmRoles);
	}
	
	@future (callout = true)
    public static void MDMDeleteRoleAsync(list<id> mdmRoleIds) {
		MDMRoleProxy proxy = new MDMRoleProxy();
		MDMDeleteRoleRequest request = new MDMDeleteRoleRequest();
        Map<id, MDM_Role__c> mdmRoles = new Map<id, MDM_Role__c>(proxy.getMDMRolesByIds(mdmRoleIds, request.getName()));
		proxy.MDMDeleteRole(mdmRoles);
	}
	
	//Called from an exception to set all sync status to failed
	private void SetRolesSyncFailed(List<MDM_Role__c> roles, String source) {
		try {
			if (roles != null) {
				for (MDM_Role__c r : roles) {
					r.Synchronisation_Status__c = 'Sync Failed';
				}
				update roles; 
			}
		} catch (Exception ex) {
			//Simple update, hopefully shouldn't get here.
			Logger.LogException(source, ex);
		}
	}
	
	
/// ----------------------------------------------------------------------------
///  -- Sub classes for the request and response messages called above
/// ----------------------------------------------------------------------------
	public virtual class MDMRoleRequest extends MDMRequest {
        //TODO: move to custom setting...
		protected String MDMChangeRolesRequest_Label {
			get {
				return 'changeRolesRequest';
			}
		}
		protected String MDMChangeRolesRequest_Namespace {
			get {
				return 'http://www.gard.no/mdm/v1_0/mdmmessage';
			}
		}
		protected String MDMChangeRolesRequest_Prefix {
			get {
				return 'mdm';
			}
		}
		
		public virtual override String getName() { return 'changeRolesRequest'; }
		public List<MDM_Role__c> MDMRoles { get; set; }
		
        public virtual override DOM.Document getSoapBody() {
            return getSoapBody(this.getName(), MDMChangeRolesRequest_Namespace, MDMChangeRolesRequest_Prefix);
        }
		
        public DOM.Document getSoapBody(string method, String namespace, String prefix) {
			system.debug('*** method: ' + method);
            DOM.Document body = new DOM.Document();
			DOM.XMLNode root = body.createRootElement(MDMChangeRolesRequest_Label, MDMChangeRolesRequest_Namespace, MDMChangeRolesRequest_Prefix);		

			//Get XML element list from Custom Setting
			List<MDM_Service_Fields__c> serviceFields = [
				SELECT Name, Request_Name__c, Sequence__c, Namespace_Prefix__c, Parent_Node__c, Type__c, XmlLabel__c, Source_Object__c, Field_Name__c, Address_Type__c, Omit_Node_If_Null__c, Strip_Special_Characters__c, Strip_Whitespaces__c
				FROM MDM_Service_Fields__c 
				WHERE Request_Name__c = :method
				ORDER BY Sequence__c];
			
            for(MDM_Role__c r : this.MDMRoles) {
				DOM.XmlNode contactXml = root.addChildElement(method, namespace, prefix);
				appendSObjectXml(contactXml, r, serviceFields);
            }
            return body;
        }
    }
    
	public class MDMUpsertRoleRequest extends MDMRoleRequest {
		public override String getName() { return 'upsertRoleRequest'; }
    }
	
	public virtual class MDMChangeResponse { 
		public List<MDMResponse> mdmResponses { get; set; }
		
		public MDMChangeResponse() {
			mdmResponses = new List<MDMResponse>();
		}
		
		protected void Deserialize(Dom.XmlNode root, String parentNode) {
			try {
				for  (Dom.XmlNode node : root.getChildElements()) {
					try {
						XmlSerializer s = new XmlSerializer();
			            object o = s.Deserialize(node, 'MDMRoleProxy.MDMResponse');
			            mdmResponses.add((MDMResponse)o);
					} catch (Exception ex) {
						//TODO: handle exceptions better...
						//Don't let one failure failt he whole batch...?
					}
				}
			} catch (Exception ex) {
				//TODO: handle exceptions better...
				throw ex;
			}
		}
	}
	
	public class MDMResponse implements XmlSerializable {
		public Boolean Success { get; set; }
		public String Id { get; set; }
		public String Error { get; set; }
		
		public object get(string fieldName) {
	        Map<string, object> thisobjectmap = new Map<string, object> 
	        { 
	            'id' => this.Id, 
	            'success' => this.Success,
				'error' => this.Error
	        };
	        return thisobjectmap.get(fieldname);
	    }
	    
	    public boolean put(string fieldName, object value) {
	        if (fieldName == 'id') {
	            this.Id = String.valueOf(value);
	        } else if (fieldName == 'success') {
	            this.Success = Boolean.valueOf(value);  
			} else if (fieldName == 'error') {
	            this.Error = String.valueOf(value);
	        } else {
	            return false;
	        }
	        return true;
	    }
	    
	    public Set<string> getFields() {
	        Set<string> fields = new Set<string> 
	        { 
	            'success',
				'id',
				'error'
	        };
	        
	        return fields;
	    }
	}
	
	public class MDMUpsertRoleResponse extends MDMChangeResponse {
		public void Deserialize(Dom.XmlNode root) {
			Deserialize(root, 'MDMRoleProxy.MDMChangeResponse');
		}
	}
	
	public class MDMDeleteRoleRequest extends MDMRoleRequest {
		public override String getName() { return 'deleteRoleRequest'; }
    }
	
	public class MDMDeleteRoleResponse extends MDMChangeResponse {
		public void Deserialize(Dom.XmlNode root) {
			Deserialize(root, 'MDMRoleProxy.MDMChangeResponse');
		}
	}
}