@isTest(SeeAllData = true)
private class TesttriggerPEMEExams  {
    
    Static testMethod void  triggerPEMEExamsTest1()
    {
        Account acc = [Select id From Account Limit 1];
        
        Contact con = new Contact();
        con.LastName = 'TestContact';
        con.AccountId= acc.id;
        insert con;
        
        PEME_Enrollment_Form__c pef = new PEME_Enrollment_Form__c();
        pef.Enrollment_Status__c = 'Draft';
        pef.ClientName__c = acc.id;
        pef.Gard_PEME_References__c = 'test';
        pef.Submitter__c = con.id;
        insert pef;
    
        PEME_Invoice__c pi = new PEME_Invoice__c();
        pi.CurrencyIsoCode = 'USD';
        pi.PEME_Enrollment_Detail__c = pef.id;
        insert pi;
        
        
        
        Test.startTest();
        
        PEME_Exam_Detail__c pm = new PEME_Exam_Detail__c();
            pm.Invoice__c = pi.id;    
            //pm.Status__c = 'Under Approval';
            insert pm;
        PEME_Exam_Detail__c pm2 = new PEME_Exam_Detail__c();
            pm2.Invoice__c = pi.id;    
            //pm.Status__c = 'Under Approval';
            insert pm2;
        try
        {
            pm.Status__c = 'Under Approval';
            update pm;
            pm.Status__c = 'Rejected-Need Input';
            update pm;
            //pm.Status__c = 'Under Approval';
            //update pm;
            pm.Status__c = 'Approved';
            update pm;
            pef.Enrollment_Status__c = 'Enrolled';
            update pef;
            pm2.status__c = 'Rejected-Closed';
            update pm2;
            
            //throw new Exception('Operation Cancelled: Please provide a rejection reason!');
        }     
        catch(Exception e)
        {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Operation Cancelled: Please provide a rejection reason!') ? true : false;
            //System.AssertEquals(expectedExceptionThrown, false);
        } 
 
            
        Test.StopTest();
        
    }
    Static testMethod void  triggerPEMEExamsTest2()
    {
        Account acc = [Select id From Account Limit 1];
        
        Contact con = new Contact();
        con.LastName = 'TestContact';
        con.AccountId= acc.id;
        insert con;
        
        PEME_Enrollment_Form__c pef = new PEME_Enrollment_Form__c();
        pef.Enrollment_Status__c = 'Draft';
        pef.ClientName__c = acc.id;
        pef.Gard_PEME_References__c = 'test';
        pef.Submitter__c = con.id;
        insert pef;
    
        PEME_Invoice__c pi = new PEME_Invoice__c();
        pi.CurrencyIsoCode = 'USD';
        pi.PEME_Enrollment_Detail__c = pef.id;
        insert pi;
        
        
        
        Test.startTest();
        
        PEME_Exam_Detail__c pm = new PEME_Exam_Detail__c();
            pm.Invoice__c = pi.id;    
            //pm.Status__c = 'Under Approval';
            insert pm;
        PEME_Exam_Detail__c pm2 = new PEME_Exam_Detail__c();
            pm2.Invoice__c = pi.id;    
            //pm.Status__c = 'Under Approval';
            insert pm2;
        try
        {
            pm.Status__c = 'Under Approval';
            update pm;
        }catch(Exception e)
        {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Operation Cancelled: Please provide a rejection reason!') ? true : false;
            //System.AssertEquals(expectedExceptionThrown, false);
        }
        Test.StopTest();
    }
}