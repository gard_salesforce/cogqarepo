/*************************************************************************************************
    Author      : Martin Gardner
    Company     : cDecisions
    Date Created: 
    Description : This class provides support for overriding the edit function in certain circumstances
    Modified    :   
***************************************************************************************************/
public with sharing class CheckEditContactExt {

    public contact con{get;set;}
    private ApexPages.StandardController stdCtrl {get; set;}

    public checkEditContactExt(ApexPages.StandardController std) {
		if (!Test.isRunningTest()) {
			std.addFields(new String[]{
				'Deleted__c',
				'Synchronisation_Status__c'
			});
		}
        con = (Contact)std.getRecord();
    }
	
	public Boolean EditSyncFailuresEnabled { 
		get { 
			MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
			return (mdmSettings.Edit_Sync_Failure_Records__c && con.Synchronisation_Status__c == 'Sync Failed' && !con.Deleted__c);
		} 
	}
    
    public pagereference checkAndRedirect(){
		if (!con.Deleted__c && (EditSyncFailuresEnabled || con.Synchronisation_Status__c == 'Synchronised')) {
			editLink = new pageReference('/003/e');//?id=' + acc.Id + '&retURL=' + acc.id);
            editLink.getParameters().putAll(ApexPages.currentPage().getParameters());
            //Check the parameters
            System.Debug('*** stdEdit.getParameters = ' + editLink.getParameters());
			if (con.Synchronisation_Status__c == 'Synchronised') {
				return editLink;
			}
		} 
		return null;
		
    }
	
	private pagereference editLink;
	
	public pagereference ignoreWarningAndEdit() {
        return editLink;
    }
	
    public pagereference goBack() {
		Map<String, String> params = ApexPages.currentPage().getParameters();
    	String retURL;
    	if(params.containsKey('retURL')){
    		retURL = params.get('retURL');
    	}else{
    		retURL = '/';
    	}
        pagereference stdDetail = new pageReference(retURL);
        return stdDetail;
    }
    

}