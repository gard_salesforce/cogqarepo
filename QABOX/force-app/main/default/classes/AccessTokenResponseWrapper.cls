public class AccessTokenResponseWrapper {
    
    public class AccessTokenResponse{
        public String access_token;
        public String refresh_token;
        public String scope;
        public String token_type;
        public String expires_in;
    }
    
 
}