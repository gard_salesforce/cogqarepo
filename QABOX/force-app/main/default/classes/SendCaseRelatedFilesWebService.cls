/*****************************************************************************************************
* Class Name: SendCaseRelatedFilesWebService
* Created By: Cognizant
* Date: 08-09-2020
* Description: To send selected case related files from Salesforce to M-Files
*******************************************************************************************************
*  Version		Developer			Date			Description
=======================================================================================================
*  1.0          Anju				08/09/2020  	Created 
*******************************************************************************************************/
public class SendCaseRelatedFilesWebService{
    
    public static String initiateRestCallOut(Id caseId, List<Id> conDocIdList){
        String documentId = '';
        //Access token Generation : Starts
        Mule_Web_Service__c accessTokenDetails = Mule_Web_Service__c.getInstance('Retreive access token');
        TokenResponseWrapper tokenResponseWrapper = new TokenResponseWrapper();
        
        Http sendTokenReq = new Http();
        Httprequest tokenReq = new Httprequest();
        tokenReq.setEndpoint(accessTokenDetails.Endpoint_Url__c);
        tokenReq.setheader('scope','READ WRITE');
        tokenReq.setheader('client_id',accessTokenDetails.Client_Id__c);
        tokenReq.setheader('client_secret',accessTokenDetails.Client_secret__c);
        tokenReq.setheader('grant_type','client_credentials');
        tokenReq.setMethod('POST');
        HttpResponse response = sendTokenReq.send(tokenReq);
        //system.debug('Token received : '+response.getBody());
        tokenResponseWrapper = (TokenResponseWrapper)System.JSON.deserialize(response.getBody(), TokenResponseWrapper.class);
        system.debug('Access token : '+tokenResponseWrapper.access_token);
        String authToken = 'Bearer '+tokenResponseWrapper.access_token;
        //Access token Generation : Ends
        //Sending Documents : Starts
        //Fetch details for case metadata tag - STARTS
        List<String> agreementIds = new List<String>();
        //List<String> objUniqueIds = new List<String>();
        Set<String> objUniqueIds = new Set<String>(); //SF-5422
        
        Case targetCase = new Case();
        targetCase = [SELECT id,Account.company_id__c, Policy_Year__c, Subject_description__c, Product_Areas__c, Category__c, Sub_Category__c,  (SELECT Agreement_Reference__c, Object__r.Source_System_Object_ID__c FROM CaseAssociation__r) from Case Where Id =: caseId Limit 1];
        //documentId = targetCase.Document_Id__c;
        String objId;
        String agreementId;
        String agreementYr;
        String mFilesAgrId;
        for(Cover_and_Objects_for_case__c coversAndObj:targetCase.CaseAssociation__r){
            objId = '';
            agreementId = '';
            agreementYr = '';
            mFilesAgrId = '';
            String[] agrIdArray;
            if(coversAndObj.Agreement_Reference__c != null && coversAndObj.Agreement_Reference__c != ''){
                if(coversAndObj.Agreement_Reference__c.containsIgnoreCase('M')){
                    agrIdArray = coversAndObj.Agreement_Reference__c.split('M');
                    agreementYr = agrIdArray[0].substring(2);
                    agreementId = agrIdArray[1];
                    if(agreementId.startsWith('0'))
                        agreementId = agreementId.substring(1);
                    mFilesAgrId = agreementYr + agreementId;    //Added modified agreement id
                }else If(coversAndObj.Agreement_Reference__c.containsIgnoreCase('P')){
                    agrIdArray = coversAndObj.Agreement_Reference__c.split('P');
                    agreementId = agrIdArray[1];
                    if(agreementId.startsWith('0'))
                        agreementId = agreementId.substring(1);
                    mFilesAgrId = 'G'+agreementId;
                    //agreementYr = agrIdArray[0].substring(2);agreementId
                }
            }
            if(!agreementIds.contains(mFilesAgrId))
                agreementIds.add(mFilesAgrId);
            if(coversAndObj.Object__r.Source_System_Object_ID__c != null && coversAndObj.Object__r.Source_System_Object_ID__c != '' && coversAndObj.Object__r.Source_System_Object_ID__c.startsWithIgnoreCase('P')){
                objId = coversAndObj.Object__r.Source_System_Object_ID__c.replace('P','');
                if(!objUniqueIds.contains(objId))
                    objUniqueIds.add(objId);    //Add modified object id
            }
            else{
                if(!objUniqueIds.contains(coversAndObj.Object__r.Source_System_Object_ID__c))
                    objUniqueIds.add(coversAndObj.Object__r.Source_System_Object_ID__c);
            }
        }
    
        //create case metadata part
        List<String> uwrTopic = new List<String>();
        List<UWRTopicMapping__c> topic = new List<UWRTopicMapping__c>();
        //String[] subCategory;
        List<String> subCategory = new List<String>();
        if(targetCase.Sub_Category__c != null){
            if(targetCase.Sub_Category__c.contains(';'))
                subCategory = targetCase.Sub_Category__c.Split(';');
            else
                subCategory.add(targetCase.Sub_Category__c);
            if(targetCase.Category__c != null){
                for(UWRTopicMapping__c uwrTpc :[SELECT Topic_code__c FROM UWRTopicMapping__c WHERE Category__c =: targetCase.Category__c AND Product_area__c =: targetCase.Product_Areas__c AND Sub_Category__c IN: subCategory]){
                    /*
                    if(uwrTopic != null && uwrTopic.size() > 0 && uwrTpc.Topic_code__c != null && uwrTpc.Topic_code__c != '' && !(uwrTopic.contains(uwrTpc.Topic_code__c)))
                        uwrTopic = uwrTopic + ';';
                    */
                    if(!(uwrTopic.contains(uwrTpc.Topic_code__c)))
                        uwrTopic.add(uwrTpc.Topic_code__c);
                }
            }
        }
        system.debug('uwrTopic--'+uwrTopic);
        system.debug('objUniqueIds--'+objUniqueIds);
        system.debug('agreementIds--'+agreementIds);
        system.debug('heap size initiate callout method :'+Limits.getHeapSize());
        //SF-5422
        System.debug('conDocIdList before: '+conDocIdList);
        BatchSendDocumentsToMFiles bMFiles = new BatchSendDocumentsToMFiles(caseId, conDocIdList, authToken, targetCase.Subject_description__c, uwrTopic,agreementIds, objUniqueIds,targetCase.Account.company_id__c, targetCase.Policy_Year__c, targetCase.Product_Areas__c);
        Database.executeBatch(bMFiles);
        
        /*for(Id fileId:conDocIdList){
            restCallout(caseId,authToken,targetCase.Subject_description__c,uwrTopic,agreementIds,objUniqueIds,targetCase.Account.company_id__c,targetCase.Policy_Year__c,targetCase.Product_Areas__c,fileId,documentId);
        }*/
        return null;
    }

    Public class TokenResponseWrapper{
        public String access_token;
        public String refresh_token;
        public String scope;
        public String token_type;
        public String expires_in;
    }
}