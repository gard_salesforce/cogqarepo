//Test Class : TestPemeWebService
public class PemeWsJobs implements Queueable, Database.AllowsCallouts{
    
    public Integer jobNumber;
    public Id pemeInvoiceId;
    public Id cvId;
    public PemeWsJobs(Id pemeInvoiceId){
        this.jobNumber = 1;
        this.pemeInvoiceId = pemeInvoiceId;
    }
    public pemeWsJobs(PemeWsJobs pwj){
        this.jobNumber = pwj.jobNumber+1;
        this.pemeInvoiceId =  pwj.pemeInvoiceId;
        this.cvId = pwj.cvId;
    } 
  
    public void execute(QueueableContext qc){
        switch on this.jobNumber{
            when 1{
                this.cvId = SaveInvoiceAsCsvFile.saveInvoice(this.pemeInvoiceId);
                if(!Test.isRunningTest()) System.enqueueJob(new PemeWsJobs(this));
            }
            when 2{
                WebServiceCaller.callPemeWs(this.pemeInvoiceId,this.cvId);
            }
        }
    }
}