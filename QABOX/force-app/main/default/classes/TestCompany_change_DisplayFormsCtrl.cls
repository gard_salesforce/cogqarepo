@isTest()
Class TestCompany_change_DisplayFormsCtrl
{
        Public static List<Account> AccountList=new List<Account>();
        Public static List<Contact> ContactList=new List<Contact>();
        Public static List<Contract> ContractList=new List<Contract>();
        Public static List<User> UserList=new List<User>();
        Public static List<Asset> AssetList=new List<Asset>();
        Public static List<Case> caseList =new List<Case>();
        Public static List<Market_Area__c> Market_AreaList=new List<Market_Area__c>();
        Public static List<Object__c> ObjectList=new List<Object__c>();
        Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Partner Community' and name='Partner Community Login User Custom' limit 1].id;
        Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
        public static Account clientAcc;
        public static Account brokerAcc;
        public static Contact brokerContact;
        public static Contact clientContact;
        public static User brokerUser;
        public static User clientUser;
        public static Contract clientContract;
        public static Contract brokerContract;
        public static Object__c obj;
        public static Asset brokerAsset;
        public static Asset clientAsset;
        public static Market_Area__c Markt;
        public static User salesforceLicUser;
        public static Case Client_Case;
        public static Case ClientCase_new;
        public static Case clientCase;
        public static Case clientCase_1;
        public static Case clientCase_2;
        public static Gard_Contacts__c grdobj;
        Public static Account_Contact_Mapping__c Accmap_1st, Accmap_2nd, Accmap_3rd ;
        Public static list<Account_Contact_Mapping__c> Accmap_list = new list<Account_Contact_Mapping__c>();  
        Public static AccountToContactMap__c map_broker, map_client;
        ///////*******Variables for removing common literals********************//////////////
        public static string enUsrStr =  'en_US';
        public static string mailngStateStr =   'London';
        /////**********************end**************************/////
    public static void cover()
    { 
         //correspondant contacts Custom Settings
        List<ContactSubscriptionFields__c> conFieldsList = new List<ContactSubscriptionFields__c>();
        ContactSubscriptionFields__c conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        ContactSubscriptionFields__c conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        ContactSubscriptionFields__c conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList;
        AdminUsers__c adminUsersCS = new AdminUsers__c();
        adminUsersCS.Name = 'Number of users';  
        adminUsersCS.Value__c = 3;
        insert adminUsersCS;
          Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
          Market_AreaList.add(Markt);
          insert Market_AreaList;  
          
          Forms_List__c Forms=new Forms_List__c(Type__c='Claim forms');
          insert Forms;
          
          
          
          salesforceLicUser = new User(
                                            Alias = 'standt', 
                                            profileId = salesforceLicenseId ,
                                            Email='standarduser@testorg.com',
                                            EmailEncodingKey='UTF-8',
                                            CommunityNickname = 'test13',
                                            LastName='Testing',
                                            LanguageLocaleKey= enUsrStr ,
                                            LocaleSidKey= enUsrStr ,  
                                            TimeZoneSidKey='America/Los_Angeles',
                                            UserName='test008@testorg.com.mygard'
                                           );
        insert salesforceLicUser;
       //Accounts............
       brokerAcc = new Account(  Name='testre',
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',
                                    recordTypeId=System.Label.Broker_Contact_Record_Type,
                                    Site = '_www.cts.se',
                                    Type = 'Broker'
                                   // Market_Area__c = Markt.id,
                                   // Area_Manager__c = salesforceLicUser.id                                                                       
                                 );
       AccountList.add(brokerAcc);
    
      
       clientAcc = new Account( Name = 'Test_1', 
                                        Site = '_www.test_1.se', 
                                        Type = 'Client', 
                                        BillingStreet = 'Gatan 1',
                                        BillingCity = 'Stockholm', 
                                        BillingCountry = 'SWE', 
                                        BillingPostalCode = 'BS1 1AD',
                                        recordTypeId=System.Label.Client_Contact_Record_Type
                                       // Market_Area__c = Markt.id,
                                       // Area_Manager__c = salesforceLicUser.id 
                                       );
                                    
        AccountList.add(clientAcc); 
        insert AccountList; 
        
        Attachment att=new Attachment(Name='abc',
                                        ContentType='abcd',
                                        Body = blob.toPDF('pdfContent'),
                                        ParentId = brokerAcc.id
                                       );
          insert att;
        
        //Contacts.................
        brokerContact = new Contact( 
                                             FirstName='Raan',
                                             LastName='Baan',
                                             MailingCity =  mailngStateStr ,
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState =  mailngStateStr ,
                                             MailingStreet = '1 London Road',
                                             AccountId = brokerAcc.Id,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(brokerContact) ;

        clientContact= new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity =  mailngStateStr ,
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState =  mailngStateStr ,
                                              MailingStreet = '4 London Road',
                                              AccountId = clientAcc.Id,
                                              Email = 'test321@gmail.com'
                                          );
        ContactList.add(clientContact);
        insert ContactList;
        
        //Users..................
        brokerUser = new User(
                                    Alias = 'standt', 
                                    profileId = partnerLicenseId,
                                    Email='mdjawedm@gmail.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='estTing',
                                    LanguageLocaleKey= enUsrStr ,
                                    CommunityNickname = 'brokerUser',
                                    LocaleSidKey= enUsrStr ,  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testbrokerUser@testorg.com.mygard',
                                    ContactId = BrokerContact.Id
                                   // ContactId__c = grdobj.id
                                   );
        UserList.add(brokerUser);
                                   
        clientUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'clientUser',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey= enUsrStr ,
                                    LocaleSidKey= enUsrStr ,
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testclientUser@testorg.com.mygard',
                                    ContactId = clientContact.id
                                   // ContactId__c = grdobj.id
                                   );
                
         UserList.add(clientUser) ;
         insert UserList;
         
         map_broker = new AccountToContactMap__c(AccountId__c=brokerAcc.id,Name=brokerContact.id);
         map_client = new AccountToContactMap__c(AccountId__c= clientAcc.id,Name=clientContact.id);
         
         Insert map_broker;
         Insert map_client;
         
         Accmap_1st = new Account_Contact_Mapping__c(Account__c = brokerAcc.id,
                                                   Active__c = true,
                                                   Administrator__c = 'Admin',
                                                   Contact__c = brokerContact.id,
                                                   IsPeopleClaimUser__c = false,
                                                   Marine_access__c = true,
                                                   PI_Access__c = true
                                                   //Show_Claims__c = false,
                                                   //Show_Portfolio__c = false 
                                                   );
           
                                 
          
         Accmap_2nd = new Account_Contact_Mapping__c(Account__c =  clientAcc.id,
                                                   Active__c = true,
                                                   Administrator__c = 'Normal',
                                                   Contact__c = clientContact.id,
                                                   IsPeopleClaimUser__c = false,
                                                   Marine_access__c = true,
                                                   PI_Access__c = true
                                                   //Show_Claims__c = false,
                                                   //Show_Portfolio__c = false 
                                                   );
        Accmap_list.add(Accmap_1st);
        Accmap_list.add(Accmap_2nd);
        
        Insert Accmap_list;
         
        Blob b = Blob.valueOf('Brokers Data');  
        Attachment attachment1 = new Attachment();  
        attachment1.ParentId = Forms.Id;
        attachment1.Name = 'Test Attachment for Color line';  
        attachment1.Body = b;  
        insert(attachment1);  

    }
        public static testmethod void CompanyChangeApproval()
    { 
         TestCompany_change_DisplayFormsCtrl.cover();  
         System.runAs(brokerUser)
        {
             CompanyChangeApprovalCtrl.approveRequest(brokerAcc.id);
             System.assertEquals(brokerAcc.BillingState_1__c , null);
             CompanyChangeApprovalCtrl.rejectRequest(brokerAcc.id,'cancel');
             
        }
    }
        public static testmethod void DisplayForms()
    { 
         TestCompany_change_DisplayFormsCtrl.cover();  
         System.runAs(brokerUser)
        {  
            DisplayFormsCtrl disforctr=new DisplayFormsCtrl();
           //  disforctr.onLoad();
             Attachment att = new Attachment();
             string attachmentName= 'abc'; 
             string origin_name = 'test'; 
             //DisplayFormsCtrl.AttachmentWrapper temp= new DisplayFormsCtrl.AttachmentWrapper(attachmentName,origin_name,att );
             //disforctr.forceDownloadPDF();
        }
        System.runAs(clientUser)
        {  
            DisplayFormsCtrl disforctr=new DisplayFormsCtrl();
            System.assertEquals(disforctr.PIAccess , true);
          //   disforctr.onLoad();
        }
    }  
}