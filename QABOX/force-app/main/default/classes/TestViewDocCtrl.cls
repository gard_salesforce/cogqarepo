@isTest(seealldata=false)
public class TestViewDocCtrl{
    //public static commonvariables__c com_var;
    Public static Object__c test_object;
    @isTest
    private static void testViewDocs(){
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        GardTestData gtdInstance = new GardTestData();
        gtdInstance.commonrecord();
        String clientCompanyId=[select company_id__c from account where id=:gardTestData.clientAcc.id].company_id__c;
        gtdInstance.customsettings_rec();
        gtdInstance.ViewDoc_testrecord(); 
        GardTestData.junc_object_broker.Broker__c = GardTestData.brokerAcc.ID;
        
        update GardTestData.junc_object_broker;
        
        GardTestData.brokerDoc.Document_Metadata_External_ID__c = '2asas123';
        update GardTestData.brokerDoc;
        System.runAs(GardTestData.brokerUser)        
        {
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            Test.startTest();
            //update gardTestData
            ViewDocCtrl test_broker= new ViewDocCtrl();
            test_broker.selectedClient1=clientCompanyId;
            system.debug(' test_broker.selectedClient1  ******'+ test_broker.selectedClient1);
            test_broker.selectedPolicyYr1='2019';  
            test_broker.customQuerySearch();
            test_broker.customQuerySearch1();
            test_broker.redirectDocHome();
            test.stopTest();
        }
    }
    @isTest
    public static void ViewDocumentMethod(){
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        AccessTokenResponseWrapper.AccessTokenResponse token=new AccessTokenResponseWrapper.AccessTokenResponse();
        insert numberofusers;
        GardTestData gtdInstance = new GardTestData();
        gtdInstance.commonrecord();
        gtdInstance.customsettings_rec();
        system.debug('***********Accmap_1 '+GardTestData.Accmap_1 );
        system.debug('***********Accmap_2 '+GardTestData.Accmap_2 );
        gtdInstance.ViewDoc_testrecord(); 
        Account client=[select id,company_id__c,guid__c from account where id=:gardTestData.clientAcc.id];
        Account brokerAcc=[select company_id__c,guid__c from account where id=:gardTestData.brokerAcc.id];
        
        String clientCompanyId=client.company_id__c;
        String clientGuid=client.guid__c;
        GardtestData.test_object_1st.guid__c = '8ce8ac89-a6fd-1836-9e07';
        update GardtestData.test_object_1st; 
        test_object = New Object__c(     Dead_Weight__c= 20,
                                    Object_Unique_ID__c = '1234lkoko',
                                    Name='hennessey',
                                    Object_Type__c='Tanker',
                                    Object_Sub_Type__c='Accommodation Ship',
                                    No_of_passenger__c=100,
                                    No_of_crew__c=70,
                                    Gross_tonnage__c=1000,
                                    Length__c=20,
                                    Width__c=20,
                                    Depth__c=30,
                                    Port__c='Alabama',
                                    Signal_Letters_Call_sign__c='hellooo',
                                    Classification_society__c='CS 1',
                                    Flag__c='Albania',
                                    Rebuilt__c='2014-01-01',
                                    Year_built__c='2013-01-01',
                                    guid__c='8ce8ac89-a6fd-1836-9e07',
                                    Dummy_Object_flag__c=0);
        insert test_object;        
        //Invoking methods for ViewDocCtrl for broker and client users......
        GardTestData.junc_object_broker.Broker__c = GardTestData.brokerAcc.ID;
        update GardTestData.junc_object_broker;
        
        GardTestData.brokerDoc.Document_Metadata_External_ID__c = '2asas123';
        update GardTestData.brokerDoc;
        
        BlueCard_webservice_endpoint__c bcTest = new BlueCard_webservice_endpoint__c(
            Name = 'BlueCard_WS_endpoint',
            Endpoint__c = 'https://soa-test.gard.no/soa-infra/services/Documents/DocumentFetcher/GetDocumentService_ep'
        );
        
        insert bcTest;                                              
        
        
        System.runAs(GardTestData.brokerUser)        
        {
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            
            Test.startTest();
            ApexPages.CurrentPage().getParameters().put('objid','8ce8ac89-a6fd-1836-9e07');
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            Pagereference pge = page.ViewDocument;             
            pge.getParameters().put('objid',GardtestData.test_object_1st.guid__c);
            pge.getparameters().put('clientid',clientGuid);
            test.setcurrentpage(pge);
            ViewDocCtrl test_broker= new ViewDocCtrl();
            ApexPages.currentPage().getParameters().put('favId',GardTestData.test_fav_for_doc.id);   
            test_broker.prvUrl = 'ABC';
            test_broker.selectedClient1=clientCompanyId;
            test_broker.newstrSelected = GardTestData.brokerAcc.id;
            test_broker.strId=GardTestData.test_object_1st.guid__c;        
            test_broker.newFilterName = 'abc';        
            
            ApexPages.currentPage().getParameters().put('objid',test_broker.strId);
            
            test_broker.selectedDocType.add(GardTestData.brokerDoc.Document_Type__c);                       
            test_broker.selectedPolicyYr.add('2019');            
            test_broker.selectedRisk.add('Y');            
            test_broker.selectedRisk.add('N');            
            test_broker.selectedObj.add('Curia');
            apexpages.currentpage().getparameters().put('objid',GardTestData.test_object_1st.guid__c);
            test_broker.strSelected = GardTestData.brokerAcc.id+';'+GardTestData.brokerAcc.id;
            system.assertequals(test_broker.selectedDocType.size()>0,true,true);
            system.assertequals(test_broker.selectedRisk.size()>0,true,true);
            system.assertequals(test_broker.selectedObj.size()>0,true,true);
            test_broker.isQueried = true;
            test_broker.isfav = true;
            test_broker.customQuerySearch();
            test_broker.customQuerySearch1();
            test_broker.sortCol = 'name';
            test_broker.sortOrder = true;
            test_broker.initializeColToOrder();
            test_broker.sortTable();
            
            test_broker.fetchSelectedClients();
            apexpages.currentpage().getparameters().put('objid',test_object.guid__c);
            test_broker.isAuthorized = true;
            test_broker.objMode = TRUE;
            test_broker.imoNo = null;
            test_broker.reset();
            Test.stopTest();
        }
        
        
    }
    @isTest
    public static void ViewDocumentMethod1(){
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        GardTestData gtdInstance = new GardTestData();
        gtdInstance.commonrecord();
        gtdInstance.customsettings_rec();
        system.debug('***********Accmap_1 '+GardTestData.Accmap_1 );
        system.debug('***********Accmap_2 '+GardTestData.Accmap_2 );
        gtdInstance.ViewDoc_testrecord(); 
        Account client=[select id,company_id__c,guid__c from account where id=:gardTestData.clientAcc.id];
        Account brokerAcc=[select company_id__c,guid__c from account where id=:gardTestData.brokerAcc.id];
        
        String clientCompanyId=client.company_id__c;
        String clientGuid=client.guid__c;
        GardtestData.test_object_1st.guid__c = '8ce8ac89-a6fd-1836-9e07';
        update GardtestData.test_object_1st; 
        test_object = New Object__c(     Dead_Weight__c= 20,
                                    Object_Unique_ID__c = '1234lkoko',
                                    Name='hennessey',
                                    Object_Type__c='Tanker',
                                    Object_Sub_Type__c='Accommodation Ship',
                                    No_of_passenger__c=100,
                                    No_of_crew__c=70,
                                    Gross_tonnage__c=1000,
                                    Length__c=20,
                                    Width__c=20,
                                    Depth__c=30,
                                    Port__c='Alabama',
                                    Signal_Letters_Call_sign__c='hellooo',
                                    Classification_society__c='CS 1',
                                    Flag__c='Albania',
                                    Rebuilt__c='2014-01-01',
                                    Year_built__c='2013-01-01',
                                    guid__c='8ce8ac89-a6fd-1836-9e07',
                                    Dummy_Object_flag__c=0);
        insert test_object;        
        //Invoking methods for ViewDocCtrl for broker and client users......
        GardTestData.junc_object_broker.Broker__c = GardTestData.brokerAcc.ID;
        update GardTestData.junc_object_broker;
        
        GardTestData.brokerDoc.Document_Metadata_External_ID__c = '2asas123';
        update GardTestData.brokerDoc;
        
        BlueCard_webservice_endpoint__c bcTest = new BlueCard_webservice_endpoint__c(
            Name = 'BlueCard_WS_endpoint',
            Endpoint__c = 'https://soa-test.gard.no/soa-infra/services/Documents/DocumentFetcher/GetDocumentService_ep'
        );
        
        insert bcTest;                                              
        
        
        System.runAs(GardTestData.brokerUser)        
        {
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            
            Test.startTest();
            ApexPages.CurrentPage().getParameters().put('objid','8ce8ac89-a6fd-1836-9e07');
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            Pagereference pge = page.ViewDocument;             
            pge.getParameters().put('objid',GardtestData.test_object_1st.guid__c);
            pge.getparameters().put('clientid',clientGuid);
            test.setcurrentpage(pge);
            ViewDocCtrl test_broker= new ViewDocCtrl();
            ApexPages.currentPage().getParameters().put('favId',GardTestData.test_fav_for_doc.id);   
            test_broker.prvUrl = 'ABC';
            test_broker.selectedClient1=clientCompanyId;
            test_broker.newstrSelected = GardTestData.brokerAcc.id;
            test_broker.strId=GardTestData.test_object_1st.guid__c;        
            test_broker.newFilterName = 'abc';        
            ApexPages.currentPage().getParameters().put('objid',test_broker.strId);
            test_broker.selectedDocType.add(GardTestData.brokerDoc.Document_Type__c);                        
            test_broker.selectedPolicyYr.add('2019');            
            test_broker.selectedRisk.add('Y');            
            test_broker.selectedRisk.add('N');            
            test_broker.selectedObj.add('Curia');
            apexpages.currentpage().getparameters().put('objid',GardTestData.test_object_1st.guid__c);
            test_broker.strSelected = GardTestData.brokerAcc.id+';'+GardTestData.brokerAcc.id;
            system.assertequals(test_broker.selectedDocType.size()>0,true,true);
            system.assertequals(test_broker.selectedRisk.size()>0,true,true);
            system.assertequals(test_broker.selectedObj.size()>0,true,true);
            test_broker.isQueried = true;
            test_broker.isfav = true;
            
            HttpResponse res = new HttpResponse();
            
            res.setHeader('Content-Type', 'application/json');
            
            res.setBody('{ "Result": true,  "MoreResultsAvailable": false,  "Count": 1,  "ObjectType": 0,  "ObjectTypeAlias": [],'
                        + '"ResultObjects": [{"ObjectID": 2196681, "ExternalID": "2196681", "Name": "248000 - JUMBO JAVELIN (SCC)",'
                        +'"ObjectProperties": { "pd_partner": "Fagioli Spa - Marghera - 48187", "pd_years": "2019", "pd_policydocumenttype": "SCCF Comprehensive Charterer\'s cover - certificate of entry",'
                        +'"documentClass": "Policy document", "name": "248000 - JUMBO JAVELIN (SCC)","pd_creator": "Lin Anett Frøshaug", "lastModified": "15.05.2020 04.52",'
                        +'"pd_validfrom": "04.03.2019 00.00","pd_validto": "31.03.2019 00.00","pd_objectnameondocument": "JUMBO JAVELIN"}}]}');
            
            
            test_broker.documentResponseWrapper= (DocumentResponseWrapper) System.JSON.deserialize(res.getBody(), DocumentResponseWrapper.class);
            system.debug('documentResponseWrapper ****'+test_broker.documentResponseWrapper);
            test_broker.exportToExcel();
            test_broker.pdf ='abc';          
            PageReference pageRef = Page.ViewDocument;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('objid',GardTestData.test_object_1st.guid__c);
            
            test_broker.createFavourites();
            
            test_broker.obj=new object__c();
            test_broker.obj.id=GardTestData.test_object_1st.id;
            Case singleCase = [SELECT ID FROM CASE WHERE Claim_Reference_Number__c='kol23232' LIMIT 1];
            test_broker.caseTestId = singleCase.ID;
            test_broker.sendMailforEditObject();
            test_broker.closeconfirmPopUp();
            test_broker.displayEditPopUp();
            test_broker.docJuncObjLst = new List<DocumentMetadata_Junction_object__c>();
            test_broker.docJuncObjLst.add(GardTestData.junc_object_broker);
            test_broker.deleteFavouritesObject();
            test_broker.fetchFavouritesObject();
            test_broker.deleteFavourites();
            ViewDocCtrl.getUserName();    
            test_broker.currentDocumentId = '2asas123'; 
            test_broker.displayDoc();
            test_broker.currentCompanyId = 'Abc';    
            test_broker.isObjectFav = true;
            test_broker.objMode = false;
            Test.stopTest();
        }
    }
}