public with sharing class surveyInviteeListController {
    @AuraEnabled
    public static String fetchInvitees(Id eventId) {
        List<EventRelation> evRelList = new List<EventRelation>();
        evRelList = [SELECT Relation.name, RelationId FROM EventRelation WHERE EventId =: eventId];// and IsParent = false];
        List<JSONOutput> JSONOutputList = new List<JSONOutput>();
        for(EventRelation er:evRelList){
            if(String.ValueOf(er.RelationId).startsWith('003')){
                JSONOutput JSONOutputs = new JSONOutput();
                JSONOutputs.contactId = er.RelationId;
                JSONOutputs.contactName = er.Relation.name;
                JSONOutputs.parentId = eventId;
                JSONOutputList.add(JSONOutputs);
            }
        }
        return JSON.serialize(JSONOutputList);
    }
    @AuraEnabled
    public static String createSurveyInvitees(List<Id> contactIdList, Id eventId){
        system.debug('contact ids:'+contactIdList);
        List<Survey_Member__c> smList = new List<Survey_Member__c>();
        String evtSubject = [SELECT Subject FROM Event WHERE Id =: eventId limit 1].Subject;
        for(contact con:[SELECT Id,Name FROM Contact WHERE ID IN: contactIdList]){
            Survey_Member__c sm = new Survey_Member__c();
            sm.Name = con.Name;
            sm.Contact__c = con.Id;
            sm.Parent_event__c = eventId;
            sm.Event_title__c = evtSubject;
            smList.add(sm);
        }
        insert smList;
        //populate the KCC Survey Link
        for(Survey_Member__c survMem: smList){
            survMem.KCC_Survey_Link__c = Label.KCC_Survey_Link +'?EventID='+ survMem.Parent_event__c + '&ContactID=' + survMem.Contact__c + '&MemberID=' + survMem.Id + 
                                    '&EventTile=' + survMem.Event_title__c.replace(' ','%20');
        }
        update smList;
        return null;
    }
    @AuraEnabled
    public static String fetchSurveyMembers(Id eventId){
        List<JSONOutput> JSONOutputList = new List<JSONOutput>();
        for(Survey_Member__c sm:[SELECT Contact__r.Name,Parent_event__c,createdDate,Responded__c FROM Survey_Member__c WHERE Parent_event__c LIKE: eventId]){
            JSONOutput JSONOutputs = new JSONOutput();
            JSONOutputs.contactName = sm.Contact__r.Name;
            JSONOutputs.responded = sm.Responded__c;
            JSONOutputs.responseTime = String.ValueOf(sm.createdDate);
            JSONOutputList.add(JSONOutputs);
        }
        return JSON.serialize(JSONOutputList);
    }
     @AuraEnabled
    public static String fetchEventName(Id eventId){
        String evtName = [SELECT Subject FROM Event where Id =: eventId].Subject;
        return evtName;
    }
    public class JSONOutput{
        Id contactId;
        String contactName;
        Id parentId;
        Boolean responded;
        String responseTime;
    }
    //Method to insert emails send to the invitees
    @InvocableMethod
    public static void insertEmailMsg(List<Id> contactId){
        system.debug('Contact Ids from PB: '+contactId);
        
        EmailTemplate emTemp = new EmailTemplate();
        emTemp = [SELECT subject, Body FROM EmailTemplate WHERE DeveloperName = 'KCC_survey'];
        
        List<EmailMessage> emList = new List<EmailMessage>();
        List<Survey_Member__c> smList = new List<Survey_Member__c>([SELECT Id, Contact__c, Contact_name__c, Contact__r.accountId, Contact__r.Email, KCC_Survey_Link__c  FROM Survey_Member__c WHERE Id IN: contactId]);
        for(Survey_Member__c surveyMem : smList){
            system.debug('survey member :'+surveyMem);
            EmailMessage em = new EmailMessage();
            //em.ParentId = contactId[0];
            em.Subject = emTemp.subject;
            em.FromAddress = userinfo.getUserEmail();
            em.ToAddress = surveyMem.Contact__r.Email;
            em.Status = '3';
            em.RelatedToId = surveyMem.Contact__r.accountId;
            em.fromName = userInfo.getUserName();
            em.TextBody = emTemp.Body.replace('{!Survey_Member__c.Contact_name__c}',surveyMem.Contact_name__c).replace('{!$User.FirstName} {!$User.LastName}',userInfo.getName()).replace('{!Survey_Member__c.KCC_Survey_Link__c}',surveyMem.KCC_Survey_Link__c);
            emList.add(em);
            
            /*
             em.TextBody = 'Dear '+con.Name+','+'\n'+'Thank you for attending the event.\n\nPlease click the link below to provide your feedback.'+
                    ' The survey will only take 1-2 minutes to complete.\n\nKind regards,\n'+userInfo.getName()+'\n\nThis email has been sent for and on behalf of an entity of the Gard Group comprised of,'+
                    ' inter alia; Gard P. & I. (Bermuda) Ltd., Assuranceforeningen Gard - gjensidig -, Gard Marine & Energy Limited and Gard Marine & Energy Insurance (Europe) AS.'+
                    ' Gard AS is registered as an insurance intermediary by the Norwegian Financial Supervisory Authority.\n'+'All external service providers for or on behalf of an entity of the Gard Group or its customers'+
                        ' are required to act in accordance with Gard\'s Anti-Bribery Requirements.\n\n\n'+
                        '- CONFIDENTIALITY NOTICE –\n'+
                        'This message is confidential and may be protected by legal privilege. Access to this message by any person other than the addressee is unauthorised and any disclosure or copying of or reliance upon the information contained in this message or any attachments is prohibited. If you receive this message in error you must preserve its confidentiality, advise the sender and delete the material from your computer. www.gard.no';
            */
        }
        insert emList;
        
        List<EmailMessageRelation> emrList = new List<EmailMessageRelation>();
        for(Survey_Member__c surveyMem : smList){
            for(EmailMessage eml : emList){
                if(eml.ToAddress == surveyMem.Contact__r.Email){
                    EmailMessageRelation emrCon = new EmailMessageRelation();
                    emrCon.EmailMessageId = eml.Id;
                    emrCon.RelationId = surveyMem.Contact__c;
                    emrCon.RelationType = 'ToAddress';
                    emrList.add(emrCon);
                    break;
                }
            }
            system.debug('--Breaking Bad--');
        }
        system.debug('Email Msg relations: '+emrList);
        insert emrList;
    }
}