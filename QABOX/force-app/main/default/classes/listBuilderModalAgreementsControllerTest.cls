@isTest (seeAllData = true) //
public with sharing class listBuilderModalAgreementsControllerTest{
    public static Integer testInteger = 1;
    public static String testURL = 'www.randomURL.com';
    public static String testPhone = '12345678';
    public static Date testDate = Date.today();


    Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Transactions').getRecordTypeId();
    private static User salesforceUser;

    @isTest
    public static void listBuilderModalAgreementsControllerTest(){
        List<Asset> assets = new List<Asset>([SELECT Id, AccountId, Account.Name,agreement__c FROM Asset WHERE AccountId != null LIMIT 2]);
        Case caseOne = createCases(1, false, true)[0];
        String companyString = assets[0].Account.Name;
        Cover_and_Objects_for_case__c junctionCover = new Cover_and_Objects_for_case__c();
        junctionCover.Case__c = caseOne.Id;
        junctionCover.Cover__c = assets[0].Id;
        insert junctionCover;
        caseOne.AccountId = assets[0].AccountId;
        update caseOne;
        List<Id> notWantedCovers = new List<Id>();
        List<Id> accountIds = new List<Id>();
        accountIds.add(assets[0].AccountId);

        Map<String, Object> filterValueMap = new Map<String, Object>{
                'policyYear' => null,
                'agreementReference' => null,
                'riskIndicator' => True
        };
        Map<String, Object> sortMap = new Map<String, Object>();

        List<listBuilderModalAgreementsController.jsonIn> jsoninTestList = new List<listBuilderModalAgreementsController.jsonIn>();
        for (Asset a : assets) {
            listBuilderModalAgreementsController.jsonIn jsoninTest = new listBuilderModalAgreementsController.jsonIn();
            jsoninTest.recordId = a.agreement__c;
            jsoninTest.dml = 'create';
            jsoninTestList.add(jsoninTest);
        }

        listBuilderModalAgreementsController.deleteCreateJunctionAgreements(caseOne.Id, JSON.serialize(jsoninTestList));

        for (listBuilderModalAgreementsController.jsonIn jsInMod : jsoninTestList) {
            jsInMod.dml = 'delete';
        }
        listBuilderModalAgreementsController.deleteCreateJunctionAgreements(caseOne.Id, JSON.serialize(jsoninTestList));
        Set<Id> caseid= new Set<id>();
        caseid.add(caseOne.Id);
        Set<Id> cobids= new Set<id>();
        cobids.add(junctioncover.id);
        delAttach.updateAccforCOB(cobids,caseid);
        
        Set<String>ids=new Set<string>();
        ids.add(caseone.Submitter_company__c);
        caseOne.Submitter_Company__c= null;
        caseOne.Account_Id__c= null;
        caseOne.AccountId= null;
        update caseOne;
        
        delAttach.delAccforCase(ids,caseid);
        caseOne.Submitter_Company__c= assets[1].AccountId;
        update caseOne;
    }
    
    @isTest
    public static void listBuilderModalAgreementsControllerTest2(){
        List<Asset> assets = new List<Asset>([SELECT Id, AccountId, Account.Name,agreement__c FROM Asset WHERE AccountId != null LIMIT 2]);
        Case caseOne = createCases(1, false, true)[0];
        String companyString = assets[0].Account.Name;
        Cover_and_Objects_for_case__c junctionCover = new Cover_and_Objects_for_case__c();
        junctionCover.Case__c = caseOne.Id;
        junctionCover.Agreement__c = assets[0].agreement__c;
        insert junctionCover;
        
        listBuilderModalAgreementsController.getRelatedRelevantAgreements(caseOne.Id);
        listBuilderModalAgreementsController.getRelevantAccounts(companyString);
        listBuilderModalAgreementsController.getMoreRelevantAgreements(new List<Id>{assets[0].Account.Id},2, new List<Id>(), new Map<String, Object>(), new Map<String, Object>());
        delete junctionCover;
        delete caseOne;
    }

    public static List<Case> createCases(Integer numberOfRecords, Boolean allFields, Boolean doInsert) {
        return createCases(numberOfRecords, allFields, doInsert, null);
    }

    public static List<Case> createCases(Integer numberOfRecords, Boolean allFields, Boolean doInsert, Id recordTypeId) {
        List<Case> newCases = (List<Case>) createSObjects(Case.Class, numberOfRecords, allFields);
        //GardTestData testRecs = new GardTestData();
        //testRecs.commonRecord();
        if (recordTypeId != null) {
            for (Case c : newCases) {
                c.RecordTypeId = recordTypeId;
                /*c.AccountId = GardTestData.brokerAcc.Id;
                c.Submitter_Company__c = GardTestData.brokerAcc.Id;*/
                
            }
        }
        if (doInsert) insert newCases;
        //to cover caseTrigger
        List<Account> accList = new List<Account>([SELECT Id FROM Account WHERE Role_Broker__c = true LIMIT 1]);
        case cs = newCases[0].clone();
        cs.AccountId = accList[0].Id;
        cs.Submitter_Company__c = accList[0].ID;
        insert cs;
        return newCases;
    }


    public static List<SObject> createSObjects(System.Type objectType, Integer numberOfRecords, Boolean allFields) {
        Map<Schema.SObjectField, DescribeFieldResult> describeFieldResultsByField = getObjectFields(objectType, allFields);
        List<SObject> records = new List<SObject>();
        for (Integer i = 0; i < numberOfRecords; i++) {
            SObject record = (SObject) objectType.newInstance();
            records.add(autoPopulateFields(record, describeFieldResultsByField));
        }
        return records;
    }


    public static Map<Schema.SObjectField, DescribeFieldResult> getObjectFields(System.Type objectType, Boolean allFields) {
        Map<String, Schema.SObjectField> fieldMap = Schema.describeSObjects(new List<String>{objectType.toString()})[0].fields.getMap();
        Map<Schema.SObjectField, DescribeFieldResult> fields = new Map<Schema.SObjectField, DescribeFieldResult>();

        for(Schema.SObjectField field : fieldMap.values()) {
            if (allFields) {
                if (field.getDescribe().isUpdateable() && field.getDescribe().isCreateable() && !field.getDescribe().isIdLookup()) {
                    fields.put(field, field.getDescribe());
                }
            } else {
                if (field.getDescribe().isUpdateable() && field.getDescribe().isCreateable() && !field.getDescribe().isNillable() && !field.getDescribe().isDefaultedOnCreate()) {
                    fields.put(field, field.getDescribe());
                }
            }
        }
        return fields;
    }

    private static SObject autoPopulateFields(SObject record, Map<Schema.SObjectField, DescribeFieldResult> describeFieldResultsByField) {
        for (Schema.SObjectField field : describeFieldResultsByField.keySet())
            if (!describeFieldResultsByField.get(field).isDefaultedOnCreate() && field.getDescribe().getName() != 'RecordTypeId')
                record.put(String.valueOf(field), assignValue(describeFieldResultsByField.get(field)));
        return record;
    }


    private static Object assignValue(DescribeFieldResult result) {
        if (String.valueOf(result.getType()) == 'BOOLEAN') return result.getDefaultValue();
        if (String.valueOf(result.getType()) == 'STRING' || String.valueOf(result.getType()) == 'TEXTAREA') return generateRandomString((result.getLength() < 15 ? result.getLength() : 15));
        if (String.valueOf(result.getType()) == 'DOUBLE') return testInteger;
        if (String.valueOf(result.getType()) == 'PHONE') return testPhone;
        if (String.valueOf(result.getType()) == 'URL') return testURL;
        if (String.valueOf(result.getType()) == 'DATE') return testDate;
        if (String.valueOf(result.getType()) == 'PICKLIST' || String.valueOf(result.getType()) == 'MULITPICKLIST') {
            return String.valueOf(result.getPicklistValues()[0].getValue());
        }
        return null;
    }

    public static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx+1);
        }
        return randStr;
    }


    // #### CONTENTDOCUMENT / CONTENTVERSION #### //
    public static List<Id> createContentDocumentsLinkedToRecord(Integer numberOfContentDocuments, Id recordId) {
        List<ContentVersion> contentVersions             = createContentVersions(numberOfContentDocuments, False, null);
        Map<Id, ContentVersion> contentVersionById       = new Map<Id, ContentVersion>(contentVersions);
        List<Id> contentDocumentIds                      = getDocumentIdsFromContentVersionIds(contentVersionById.keySet());
        List<ContentDocumentLink> contentDocumentLinkIds = insertContentDocumentLinks(recordId, contentDocumentIds);
        return contentDocumentIds;
    }

    public static List<ContentVersion> createContentVersions(Integer numberOfContentDocuments, Boolean allFields, Id firstPublishLocationId) {
        List<ContentVersion> newContentVersions = (List<ContentVersion>) createSObjects(ContentVersion.Class, numberOfContentDocuments, allFields);
        for (ContentVersion cv : newContentVersions) {
            completeContentVersion(cv, firstPublishLocationId);
        }
        insert newContentVersions;
        return newContentVersions;
    }

    public static void completeContentVersion(ContentVersion contentVersion, Id firstPublishLocationId) {
        contentVersion.Title = generateRandomString(15);
        contentVersion.VersionData = EncodingUtil.base64Decode(generateRandomString(15));
        contentVersion.PathOnClient = contentVersion.Title;
        contentVersion.FirstPublishLocationId = firstPublishLocationId;
        contentVersion.ContentUrl = '';
    }

    public static List<Id> getDocumentIdsFromContentVersionIds(Set<Id> contentVersionIds) {
        List<Id> contentDocumentIds = new List<Id>();
        for (ContentVersion contentVersion : [SELECT ContentDocumentId FROM ContentVersion WHERE Id IN : contentVersionIds])
            contentDocumentIds.add(contentVersion.ContentDocumentId);
        return contentDocumentIds;
    }

    public static List<ContentDocumentLink> insertContentDocumentLinks(Id recordId, List<Id> contentDocumentIds) {
        List<ContentDocumentLink> newContentDocumentLinks = new List<ContentDocumentLink>();
        for (Id contentDocumentId : contentDocumentIds) {
            newContentDocumentLinks.add(createContentDocumentLinkAllUsers(recordId, contentDocumentId));
        }
        insert newContentDocumentLinks;
        return newContentDocumentLinks;
    }

    public static ContentDocumentLink createContentDocumentLinkAllUsers(Id linkedEntityId, Id contentDocumentId) {
        return new ContentDocumentLink(ContentDocumentId = contentDocumentId, LinkedEntityId = linkedEntityId, ShareType = 'V', Visibility = 'AllUsers');
    }


}