@isTest
private class MDMSyncRoleRetryExtensionTests {
	private static List<Valid_Role_Combination__c> validRoles;
	
	private static Account setupTestAccount() {
		return new Account (Name = 'Unit Test Account', Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
	}
	
	private static void setupValidRoles() {
		////For some reason this seems to be throwing an internal Salesforce error /
		////System.UnexpectedException: Salesforce System Error: 1583762872-13077 (1716107030) (1716107030)
		//validRoles = (List<Valid_Role_Combination__c>)Test.loadData(Valid_Role_Combination__c.sObjectType, 'MDMValidRoles');
		////END...
		validRoles = new List<Valid_Role_Combination__c>();
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 1', Sub_Role__c = 'Sub Role 11'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 1', Sub_Role__c = 'Sub Role 12'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 2', Sub_Role__c = 'Sub Role 21'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 2', Sub_Role__c = 'Sub Role 22'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Other', Sub_Role__c = 'Other Sub Role'));
		insert validRoles;
	}
	
	static testMethod void MDMSyncAccountRetryExtension_Coverage() {
		///ARRANGE...
		setupValidRoles();
		Account a = setupTestAccount();
		a.Synchronisation_Status__c = 'Sync Failed';
		a.Billing_Address_Sync_Status__c = 'Sync Failed';
		a.Shipping_Address_Sync_Status__c = 'Sync Failed';
		insert a;
		
		MDM_Role__c r = new MDM_Role__c(account__c = a.Id, Valid_Role_Combination__c = validRoles[0].id, Synchronisation_Status__c = 'Sync Failed');
		insert r;
				
		Apexpages.currentPage().getParameters().put('id',r.Id);     
		ApexPages.StandardController sc = new ApexPages.standardController(r);
        MDMSyncRoleRetryExtension retryExt = new MDMSyncRoleRetryExtension(sc);
		
		///ACT...
		Test.startTest();
		boolean b = retryExt.IsAdmin;
		string s = retryExt.redirectUrl;
		boolean b2 = retryExt.shouldRedirect;
		retryExt.RetryRoleSync();
		Test.stopTest();
		
		///ASSERT...
		r = [SELECT id, Synchronisation_Status__c FROM MDM_Role__c WHERE id = :r.id];
		System.assertEquals('Sync In Progress', r.Synchronisation_Status__c, 'Role sync status should be sync in progress');
	}
}