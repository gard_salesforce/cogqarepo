/****************************************************************************************************************************
*    Deveployed By   :    Cognizant Technology Solution
*    Created Date    :    06/06/2019 
*    Descriptions    :    This class is used to schedule claim reminder notification email
*    Modification Log
*    --------------------------------------------------------------------------------------------------------
*    Developer                                Date                        Description
*    --------------------------------------------------------------------------------------------------------
*    Arnab Sarkar                             06/06/2019        SF-5013  Initial version
******************************************************************************************************************************/

global class Scheduler_BatchSendClaimNotification implements Schedulable{
  global void execute(SchedulableContext sc) {
        BatchSendNotificationForClaims batchSendNotificationForClaims = new BatchSendNotificationForClaims();
        Database.executeBatch(batchSendNotificationForClaims,10);           
    }
}