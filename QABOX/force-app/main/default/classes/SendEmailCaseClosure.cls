// Test class - TestEmailSender
public class SendEmailCaseClosure{

    @InvocableMethod
    public static void sendEmail2CasePB(List<Id> caseId){
        
        //Variable to store the Developer Name of the Email Template
        String EmailTemplateDevNameClosed = 'Notification_Case_status_change_to_Closed';
        String EmailTemplateDevNameWFC = 'Notification_Case_status_change_to_Waiting_for_Customer';
       
        //Working as bulk
        List<Case> caseRetList = [SELECT Id, Status, ContactId, OwnerId FROM Case WHERE Id IN :caseId];
        
        //Query to retrieve the Id of the Email Template for Closed and Waiting for Customer
        EmailTemplate etclosed,etwfc;
        etclosed = [SELECT Id FROM EmailTemplate WHERE DeveloperName = :EmailTemplateDevNameClosed];
        etwfc = [SELECT Id FROM EmailTemplate WHERE DeveloperName = :EmailTemplateDevNameWFC];
               
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();
        
        //Variable to store the DisplayName of the OrgWideEmailAddress
        String orgAddressName = 'no-reply@gard.no';
        //Search OrgWideEmailAddress
        OrgWideEmailAddress owea = [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress WHERE DisplayName = :orgAddressName];
        
        //For every case that fires the process builder, create an email and add the record to a list
        for(Case c: caseRetList)
        {
                      
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            //Set Org Address to send the email from
            mail.setOrgWideEmailAddressId(owea.Id);            
            //set the template Id retrieved in the previous query
            if(c.status == 'Closed'){
            mail.setTemplateId(etclosed.Id);
            }
            if(c.status == 'Waiting for Customer'){
            mail.setTemplateId(etwfc.Id);
            }            
            //Set the Id to search the email to be sent
            mail.setTargetObjectId(c.ContactId);
            //Id of the related record. Case in this example
            mail.setWhatId(c.Id);
            //SetSaveAsActivity it is works only if the targetObjectId is not a user. Email is not attached to the
            //case if this option is set to false.
            mail.setSaveAsActivity(true);
            
            messages.add(mail);
                
        }
        
        //Send all the emails
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        //Save the results
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }
    }
}