@isTest
public class testBatchNewOppChecklistCreation{
    public static List<Opportunity> allOpp = [SELECT ID FROM Opportunity WHERE RecordTypeName__c = 'P_I'];
    @isTest static void TestBatchNewOppChrtrsOLD(){
        Database.BatchableContext BC;
        Database.QueryLocator QL;
        BatchNewOppChecklistCreation batchNew = new BatchNewOppChecklistCreation();
        ID batchprocessid = Database.executeBatch(batchNew);
        QL = batchNew.start(BC);
        Database.executeBatch(batchNew);
        batchNew.execute(BC,allOpp);
        batchNew.finish(BC);
    }
/*
    public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static String allOppStr = 'SELECT ID FROM Opportunity WHERE RecordTypeName__c = \'P_I\' ';
    public static List<Opportunity> allOpp = [SELECT ID FROM Opportunity WHERE RecordTypeName__c = 'P_I'];
    public static String strAccId;
    public static String strOppId;
    public static User salesforceUser;
    public static Id oldPIRecId =[SELECT Id FROM RecordType WHERE Name='Old P&I' AND sObjectType='Opportunity_Checklist__c'].id;
    public static Id newPIRecId =[SELECT Id FROM RecordType WHERE Name='P&I' AND sObjectType='Opportunity_Checklist__c'].id;
    public static Opportunity_Checklist__c  testOppChecklistsOLD ,testOppChecklistsNEW ,testOppChecklistsChartrOld,testOppChecklistsChartrNew ;
 //Create a user
    public static void createUser(){
        salesforceUser = new User(
        Alias = 'standt', 
        profileId = salesforceLicenseId ,
        Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8',
        CommunityNickname = 'test13',
        LastName='Testing',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',  
        TimeZoneSidKey='America/Los_Angeles',
        UserName='test008@testorg.com'
        );
        insert salesforceUser; 
    }

 //create an account record
    public static void createAccount(){

        Account acc = new Account(  Name='APEXTESTACC001',
        BillingCity = 'Bristol',
        BillingCountry = 'United Kingdom',
        BillingPostalCode = 'BS1 1AD',
        BillingState = 'Avon' ,
        BillingStreet = '1 Elmgrove Road',
        Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id,
        Market_Area__c = TestDataGenerator.getMarketArea().Id,
        OwnerId = salesforceUser.id
        );
        insert acc;
        strAccId = acc.Id; 
    }
 //create opportunity
        public static void createOpp(){
        Id strRecId =[SELECT Id FROM RecordType WHERE Name='P&I' AND sObjectType='Opportunity' ].id;
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = strRecId ;
        opp.Name = 'APEXTESTOPP001';
        opp.AccountId = strAccId;
        opp.StageName = 'Risk Evaluation';//Renewable Opportunity
        opp.Type = 'New Business';        
        opp.Business_Type__c = 'MOUs';
        opp.Approval_Criteria__c = 'Self Approval';
        opp.Sales_Channel__c = 'Direct';
        opp.CloseDate = Date.Today();
        opp.Confirm_not_on_sanction_list__c = true;
        opp.Amount = 100;
        insert opp;
        strOppId = opp.Id;
    }
    //For Owners Create Opportunity checklist (OLD)
        public static void createOppCListOwnersOLD(){
        testOppChecklistsOLD = new Opportunity_Checklist__c (Agreement_type__c = 'Owners',Owners_P_I_Completed__c = true,Charterers_Completed__c = true,Opportunity__c = strOppId ,
                                       Checklist_Completed__c = true,No_change__c = true ,
                                       Capital_Provider1__c = true,Capital_Provider_Comments__c='test',
                                       Broker__c=true,Broker_details_including_contact__c='test',
                                       Broker_Commission__c=true,Commission_Comments__c='test',
                                       Charterers_Deductiblesv2__c= true, Charterers_Deductibles_Commentsv2__c='test',
                                       RecordTypeId=oldPIRecId);
                                       
        insert testOppChecklistsOLD ;
    }      
       //For Owners Create Opportunity checklist (NEW)
        public static void createOppCListOwnersNEW(){
        
        testOppChecklistsNEW = new Opportunity_Checklist__c (Agreement_type__c = 'Owners',Owners_P_I_Completed__c = true,Charterers_Completed__c = true,Opportunity__c = strOppId ,
                                       Checklist_Completed__c = true,No_change__c = true ,
                                       Capital_Provider1__c = true,Capital_Provider_Comments__c='test',
                                       Broker__c=true,Broker_details_including_contact__c='test',
                                       Broker_Commission__c=true,Commission_Comments__c='test',
                                       Charterers_Deductiblesv2__c= true, Charterers_Deductibles_Commentsv2__c='test',
                                       RecordTypeId=newPIRecId );
                                       
        insert testOppChecklistsNEW ;
    } 
    //For Charterers Create Opportunity checklist (OLD)
     public static void createOppCListCharterersOLD(){
            testOppChecklistsChartrOld = new Opportunity_Checklist__c(Agreement_type__c = 'Charterers',Charterers_Completed__c = true,Opportunity__c = strOppId ,
                                       Checklist_Completed__c = true,No_change__c = true ,
                                       Capital_Provider1__c = true,Capital_Provider_Comments__c='test',
                                       Broker__c=true,Broker_details_including_contact__c='test',
                                       Broker_Commission__c=true,Commission_Comments__c='test',
                                       Charterers_Deductiblesv2__c= true, Charterers_Deductibles_Commentsv2__c='test',
                                       RecordTypeId=oldPIRecId);
        insert testOppChecklistsChartrOld;
    }
     //For Charterers Create Opportunity checklist (NEW)
     public static void createOppCListCharterersNEW(){
            testOppChecklistsChartrNew = new Opportunity_Checklist__c(Agreement_type__c = 'Charterers',Charterers_Completed__c = true,Opportunity__c = strOppId ,
                                       Checklist_Completed__c = true,No_change__c = true ,
                                       Capital_Provider1__c = true,Capital_Provider_Comments__c='test',
                                       Broker__c=true,Broker_details_including_contact__c='test',
                                       Broker_Commission__c=true,Commission_Comments__c='test',
                                       Charterers_Deductiblesv2__c= true, Charterers_Deductibles_Commentsv2__c='test',
                                       RecordTypeId=newPIRecId);
        insert testOppChecklistsChartrNew;
    }
     //Test Method
     
    @isTest static void TestBatchNewOppOLD(){
        Database.BatchableContext BC;
        Database.QueryLocator QL;
        createUser();
        createAccount();
        createOpp();
        Test.startTest();
        BatchNewOppChecklistCreation batchNew = new BatchNewOppChecklistCreation();
        createOppCListOwnersOLD();
        ID batchprocessid = Database.executeBatch(batchNew);
        QL = batchNew.start(BC);
        Database.executeBatch(batchNew);
        batchNew.execute(BC,allOpp);
        batchNew.finish(BC);
        Test.stopTest();
    }
     @isTest static void TestBatchNewOppNEW(){
        Database.BatchableContext BC;
        Database.QueryLocator QL;
        createUser();
        createAccount();
        createOpp();
        Test.startTest();
        BatchNewOppChecklistCreation batchNew = new BatchNewOppChecklistCreation();
        createOppCListOwnersNEW();
        ID batchprocessid = Database.executeBatch(batchNew);
        QL = batchNew.start(BC);
        Database.executeBatch(batchNew);
        batchNew.execute(BC,allOpp);
        batchNew.finish(BC);
        Test.stopTest();
    }
    @isTest static void TestBatchNewOppChrtrsOLD(){
        Database.BatchableContext BC;
        Database.QueryLocator QL;
        createUser();
        createAccount();
        createOpp();
        Test.startTest();
        BatchNewOppChecklistCreation batchNew = new BatchNewOppChecklistCreation();
        createOppCListCharterersOLD();
        ID batchprocessid = Database.executeBatch(batchNew);
        QL = batchNew.start(BC);
        Database.executeBatch(batchNew);
        batchNew.execute(BC,allOpp);
        batchNew.finish(BC);
        Test.stopTest();
    }
    @isTest static void TestBatchNewOppChrtrsNEW(){
        Database.BatchableContext BC;
        Database.QueryLocator QL;
        createUser();
        createAccount();
        createOpp();
        Test.startTest();
        BatchNewOppChecklistCreation batchNew = new BatchNewOppChecklistCreation();
        createOppCListCharterersNEW();
        ID batchprocessid = Database.executeBatch(batchNew);
        QL = batchNew.start(BC);
        Database.executeBatch(batchNew);
        batchNew.execute(BC,allOpp);
        batchNew.finish(BC);
        Test.stopTest();
    }
    */
}