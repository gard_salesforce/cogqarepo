@isTest
private class CheckNewContactExtTests {
	private static List<Valid_Role_Combination__c> validRoles;
	
	private static Account setupTestAccount() {
		return new Account (Name = 'Unit Test Account', Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
	}
	
	private static Contact setupTestContact(id AccountId) {
		return new Contact (FirstName = 'Bill', LastName = 'Smith', AccountId = AccountId);
	}
	
	private static void setupValidRoles() {
		////For some reason this seems to be throwing an internal Salesforce error /
		////System.UnexpectedException: Salesforce System Error: 1583762872-13077 (1716107030) (1716107030)
		//validRoles = (List<Valid_Role_Combination__c>)Test.loadData(Valid_Role_Combination__c.sObjectType, 'MDMValidRoles');
		////END...
		validRoles = new List<Valid_Role_Combination__c>();
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 1', Sub_Role__c = 'Sub Role 11'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 1', Sub_Role__c = 'Sub Role 12'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 2', Sub_Role__c = 'Sub Role 21'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 2', Sub_Role__c = 'Sub Role 22'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Other', Sub_Role__c = 'Other Sub Role'));
		insert validRoles;
	}
	
	static testMethod void CheckAndRedirect_AccountIdNull() {
		///ARRANGE...
		setupValidRoles();
		Account a = setupTestAccount();
		insert a;
		
		Contact c = setupTestContact(null);
		
		Apexpages.currentPage().getParameters().put('id',c.Id);     
		ApexPages.StandardController sc = new ApexPages.standardController(c);
        CheckNewContactExt checkContact = new CheckNewContactExt(sc);
		
		///ACT...
		Test.startTest();
		PageReference pr = checkContact.checkAndRedirect();
		Test.stopTest();
		
		///ASSERT...
		System.assertNotEquals(null, pr, 'Check New should return not null when AccountId is null');
	}
	
	static testMethod void CheckAndRedirect_AccountIdNotNull() {
		///ARRANGE...
		setupValidRoles();
		Account a = setupTestAccount();
		insert a;
		
		Contact c = setupTestContact(a.id);
		
		Apexpages.currentPage().getParameters().put('id',c.Id);     
		ApexPages.StandardController sc = new ApexPages.standardController(c);
        CheckNewContactExt checkContact = new CheckNewContactExt(sc);
		
		///ACT...
		Test.startTest();
		PageReference pr = checkContact.checkAndRedirect();
		Test.stopTest();
		
		///ASSERT...
		System.assertEquals(null, pr, 'Check New should return Contact and Account roles don\'t match');
	}
	
	static testMethod void CheckAndRedirect_GoBack() {
		///ARRANGE...
		setupValidRoles();
		Account a = setupTestAccount();
		insert a;
		
		Contact c = setupTestContact(a.id);
		
		Apexpages.currentPage().getParameters().put('id',c.Id);     
		ApexPages.StandardController sc = new ApexPages.standardController(c);
        CheckNewContactExt checkContact = new CheckNewContactExt(sc);
		
		///ACT...
		Test.startTest();
		PageReference pr = checkContact.goback();
		Test.stopTest();
		
		///ASSERT...
		System.assertNotEquals(null, pr, 'Go Back should not return null');
	}
}