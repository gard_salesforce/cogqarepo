global class BatchDuplicateClaimProcessed2 implements Database.Batchable<sObject> {
    
    global String query;
    global Id claimRecordTypeId = Schema.SObjectType.claim__c.getRecordTypeInfosByName().get('Claim').getRecordTypeId();
    global Id MyGardclaimRecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('MyGardClaim').getRecordTypeId();
    
    global BatchDuplicateClaimProcessed2(){
        query =  'SELECT MyGard_Claim_ID__c,Claims_handler_on_member__c,Reported_Claim__c FROM claim__c WHERE Claim_Reference_Number__c <> NULL AND (NOT  SFDC_Claim_Ref_ID__c LIKE \'E%\') AND MyGard_Claim_ID__c LIKE \'E%\'  AND parent__c=null AND RecordTypeId = : claimRecordTypeId AND createdby.name=\'integration_mule\'';  
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<claim__c> scope){
    
        system.debug('scope'+scope.size());

        //List<string> myGardClaimId=new List<string>() ;
        //SF-1957
        Set<String> myGardClaimId = new Set<String>() ; 
        List<case> updateProcessedClaimList = new List<case>();
        List<claim__c> claimListToBeUpdated = new List<claim__c>();
        Map<String,case> myGardClaimIdMap  = new Map<String,case> ();

        try {
            for (claim__c claim : scope) {
            system.debug('MyGard_Claim_ID__c '+claim.MyGard_Claim_ID__c);
            //Start Issue SF-1957 
            myGardClaimId.add(claim.MyGard_Claim_ID__c);
            claimListToBeUpdated.add(claim);
            //myGardClaimIdMap.put(claim.MyGard_Claim_ID__c,claim.id);

            system.debug('*****myGardClaimId'+myGardClaimId);

            }
        }catch (exception ex){}
        
        //SF-1957
        system.debug('MyGardclaimRecordTypeId>>>>'+MyGardclaimRecordTypeId);
        system.debug('id>>>>>>'+[SELECT ID,Status,MyGard_Claim_ID__c,Claims_handler_on_member__c FROM CASE WHERE RecordTypeId = : MyGardclaimRecordTypeId AND Claim_Reference_Number__c = NULL AND SFDC_Claim_Ref_ID__c like 'E%' AND MyGard_Claim_ID__c like 'E%'  AND parentId = null  AND createdby.name<>'integration' AND createdby.name <> 'integration_mule'][0].MyGard_Claim_ID__c);
         //system.debug('id>>>>>>'+[SELECT ID,Status,MyGard_Claim_ID__c,Claims_handler_on_member__c FROM CASE WHERE RecordTypeId = : MyGardclaimRecordTypeId AND Claim_Reference_Number__c = NULL AND SFDC_Claim_Ref_ID__c like 'E%' AND MyGard_Claim_ID__c like 'E%'  AND parentId = null  AND createdby.name<>'integration' AND createdby.name <> 'integration_mule'][1].MyGard_Claim_ID__c);
        //system.debug();
        //updateProcessedClaimlist =[SELECT ID,Status,MyGard_Claim_ID__c,Claims_handler_on_member__c FROM CASE WHERE RecordTypeId = : MyGardclaimRecordTypeId AND MyGard_Claim_ID__c in :mygardclaimid AND Claim_Reference_Number__c = NULL AND SFDC_Claim_Ref_ID__c like 'E%' AND MyGard_Claim_ID__c like 'E%'  AND parentId = null  AND createdby.name<>'integration' AND createdby.name <> 'integration_mule'];
        system.debug('MyGard_Claim_ID__c--->>' + mygardclaimid);
        
        if(!test.isRunningTest())
        updateProcessedClaimlist =[SELECT ID,Status,MyGard_Claim_ID__c,Claims_handler_on_member__c FROM CASE WHERE RecordTypeId = : MyGardclaimRecordTypeId AND MyGard_Claim_ID__c = :mygardclaimid AND Claim_Reference_Number__c = NULL AND SFDC_Claim_Ref_ID__c like 'E%' AND MyGard_Claim_ID__c like 'E%'  AND parentId = null  AND createdby.name<>'integration' AND createdby.name <> 'integration_mule'];
        else
        updateProcessedClaimlist =[SELECT ID,Status,MyGard_Claim_ID__c,Claims_handler_on_member__c FROM CASE WHERE RecordTypeId = : MyGardclaimRecordTypeId AND Claim_Reference_Number__c = NULL AND SFDC_Claim_Ref_ID__c like 'E%' AND MyGard_Claim_ID__c like 'E%'  AND parentId = null  AND createdby.name<>'integration' AND createdby.name <> 'integration_mule'];    
        system.debug('*****updateProcessedClaimlist'+updateProcessedClaimlist);
        
        for(Case claim : updateProcessedClaimlist){
            if(claim.Status == 'Pending'){//status
                claim.Status = 'Processed';//status
                myGardClaimIdMap.put(claim.MyGard_Claim_ID__c,claim);
            }
        }
        
        try{
            update updateProcessedClaimlist;
            System.debug('updateProcessedClaimlist::::'+updateProcessedClaimlist);
            System.debug('myGardClaimIdMap::::'+myGardClaimIdMap);
            if(!myGardClaimIdMap.Isempty()) {
                System.debug('myGardClaimIdMap::::'+myGardClaimIdMap);
                for(Claim__c claim : claimListToBeUpdated) {
                    System.debug('claim.MyGard_Claim_ID__c::::'+claim.MyGard_Claim_ID__c);
                    System.debug('myGardClaimIdMap.get(claim.MyGard_Claim_ID__c)::::'+myGardClaimIdMap.get(claim.MyGard_Claim_ID__c));
                    if(myGardClaimIdMap.containskey(claim.MyGard_Claim_ID__c) && myGardClaimIdMap.get(claim.MyGard_Claim_ID__c) != null){
                        System.debug('myGardClaimIdMap::::'+myGardClaimIdMap);
                        claim.Reported_Claim__c = myGardClaimIdMap.get(claim.MyGard_Claim_ID__c).Id;
                        claim.Claims_handler_on_member__c = myGardClaimIdMap.get(claim.MyGard_Claim_ID__c).Claims_handler_on_member__c ;
                    }
                }
            }
            update claimListToBeUpdated;
        }
        catch (exception ex){}
        //SF-1957
        //delete delclmlst;
        // DataBase.emptyRecycleBin(dellst);
        
    }

    global void finish(Database.BatchableContext BC){}
}