@IsTest public class TestCustomerFeedbackFromAutResponses{
    private static GardTestData gtd;
    private static Event dummyEvent{
        get{
            return new Event(Subject = 'Dummy Event',DurationInMinutes = 10,ActivityDateTime = System.now());
        }
    }
    
    private static void createTestData(){
        gtd = new GardTestData();
        gtd.commonRecord();
    }
    
    @IsTest private static void TestForClaimSurvey(){
        createTestData();
        Test.startTest();
        GetFeedback_Aut__Survey__c survey = new GetFeedback_Aut__Survey__c(
            GetFeedback_Aut__GetFeedbackId__c = Decimal.valueOf(111111),
            Name = 'Claim Survey of This Year',
            Survey_Type__c = 'Claim'
        );
        insert survey;
        
        GetFeedback_Aut__Response__c response = new GetFeedback_Aut__Response__c(
            GetFeedback_Aut__Survey__c = survey.id,
            GetFeedback_Aut__Contact__c = GardTestData.clientContact.Id,
            Claim__c = GardTestData.brokerClaim.Id,
            //Event_Id__c = dummyEvent.Id,
            GetFeedback_Aut__GetFeedbackId__c = Decimal.valueOf(11111),
            GetFeedback_Aut__LanguageCode__c = 'en',
            GetFeedback_Aut__Language__c = 'English',
            GetFeedback_Aut__Url__c = 'some@url.com'
        );
        insert response;
        
        List<GetFeedback_Aut__Answer__c> answers = new List<GetFeedback_Aut__Answer__c>();        
        answers.add(new GetFeedback_Aut__Answer__c(
            GetFeedback_Aut__Choice1__c = 'xyz',
            GetFeedback_Aut__Choices__c = 'Agree',
            GetFeedback_Aut__Contact__c = GardTestData.clientContact.Id,
            GetFeedback_Aut__DisplayValue__c = 'DisplayValueAnswer',
            GetFeedback_Aut__GetFeedbackId__c = '1111',
            GetFeedback_Aut__QuestionTitle__c = 'Is this the Question?',
            GetFeedback_Aut__QuestionType__c = 'NetPromoter',
            GetFeedback_Aut__Response__c = response.Id,
            GetFeedback_Aut__Text__c = 'Some Text'
        ));
        answers.add(new GetFeedback_Aut__Answer__c(
            GetFeedback_Aut__Choice1__c = 'xyz',
            GetFeedback_Aut__Choices__c = 'Disagree',
            GetFeedback_Aut__Contact__c = GardTestData.clientContact.Id,
            GetFeedback_Aut__DisplayValue__c = 'DisplayValueAnswer',
            GetFeedback_Aut__GetFeedbackId__c = '2222',
            GetFeedback_Aut__QuestionTitle__c = 'Is this the other Question?',
            GetFeedback_Aut__QuestionType__c = 'NonNetPromoter',
            GetFeedback_Aut__Response__c = response.Id,
            GetFeedback_Aut__Text__c = 'Some Text'
        ));
        
        insert answers;
        Test.stopTest();
        
        CustomerFeedbackFromAutResponses.dispatcher(new List<Id>{response.id});
    }
    
    @IsTest private static void TestForFeedSurvey(){
        createTestData();
        
        Test.startTest();
        GetFeedback_Aut__Survey__c survey = new GetFeedback_Aut__Survey__c(
            GetFeedback_Aut__GetFeedbackId__c = Decimal.valueOf(111111),
            Name = 'Event Survey of This Year',
            Survey_Type__c = 'Event'
        );
        insert survey;
        
        GetFeedback_Aut__Response__c response = new GetFeedback_Aut__Response__c(
            GetFeedback_Aut__Survey__c = survey.id,
            GetFeedback_Aut__Contact__c = GardTestData.clientContact.Id,
            Claim__c = GardTestData.brokerClaim.Id,
            //Event_Id__c = dummyEvent.Id,
            GetFeedback_Aut__GetFeedbackId__c = Decimal.valueOf(11111),
            GetFeedback_Aut__LanguageCode__c = 'en',
            GetFeedback_Aut__Language__c = 'English',
            GetFeedback_Aut__Url__c = 'some@url.com'
        );
        insert response;
        
        List<GetFeedback_Aut__Answer__c> answers = new List<GetFeedback_Aut__Answer__c>();        
        answers.add(new GetFeedback_Aut__Answer__c(
            GetFeedback_Aut__Choice1__c = 'xyz',
            GetFeedback_Aut__Choices__c = 'Agree',
            GetFeedback_Aut__Contact__c = GardTestData.clientContact.Id,
            GetFeedback_Aut__DisplayValue__c = 'DisplayValueAnswer',
            GetFeedback_Aut__GetFeedbackId__c = '1111',
            GetFeedback_Aut__QuestionTitle__c = 'Is this the Question?',
            GetFeedback_Aut__QuestionType__c = 'NetPromoter',
            GetFeedback_Aut__Response__c = response.Id,
            GetFeedback_Aut__Text__c = 'Some Text'
        ));
        answers.add(new GetFeedback_Aut__Answer__c(
            GetFeedback_Aut__Choice1__c = 'xyz',
            GetFeedback_Aut__Choices__c = 'Disagree',
            GetFeedback_Aut__Contact__c = GardTestData.clientContact.Id,
            GetFeedback_Aut__DisplayValue__c = 'DisplayValueAnswer',
            GetFeedback_Aut__GetFeedbackId__c = '2222',
            GetFeedback_Aut__QuestionTitle__c = 'Is this the other Question?',
            GetFeedback_Aut__QuestionType__c = 'NonNetPromoter',
            GetFeedback_Aut__Response__c = response.Id,
            GetFeedback_Aut__Text__c = 'Some Text'
        ));
        
        insert answers;
        Test.stopTest();
        
        CustomerFeedbackFromAutResponses.dispatcher(new List<Id>{response.id});
    }
    
}