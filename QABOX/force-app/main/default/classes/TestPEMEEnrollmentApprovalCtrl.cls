@isTest(seeAllData=false)
public class TestPEMEEnrollmentApprovalCtrl
{
    Public static List<Account> AccountList=new List<Account>();
    Public static List<Contact> ContactList=new List<Contact>();
    Public static List<User> UserList=new List<User>();
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
    Public static string partnerLicenseId1 = [SELECT Id FROM profile WHERE Name='System Administrator' limit 1].id;
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Gard_Contacts__c gc;
    public static List<PEME_Enrollment_Form__c> pefList=new List<PEME_Enrollment_Form__c>();
    public static List<PEME_Manning_Agent__c>  pmgList=new List<PEME_Manning_Agent__c>(); 
    public static List<PEME_Debit_note_detail__c> pdnList=new List<PEME_Debit_note_detail__c>();
    
    Static testMethod void cover()
    {
        //Test.startTest();
        gc = new Gard_Contacts__c(FirstName__c ='test',lastName__c = 'test');
        insert gc;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt;
        Gard_Team__c gt= new Gard_Team__c(Name='Claims Support America',Region_code__c='ABCD');
        insert gt;
        Country__c country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA',Claims_Support_Team__c=gt.Id);
        insert country;
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        //Valid Role Combination----
        List<Valid_Role_Combination__c> vrcList = new List<Valid_Role_Combination__c>();
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
        vrcList.add(vrcClient);
        Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
        vrcList.add(vrcBroker);
        insert vrcList;
        CommonVariables__c cvar = new CommonVariables__c(name = 'ReplyToMailId', value__c ='Test123@gmail.com');
        insert cvar;
        User user = new User(
                            Alias = 'standt', 
                            profileId = salesforceLicenseId ,
                            Email='standarduser@testorg.com',
                            EmailEncodingKey='UTF-8',
                            CommunityNickname = 'test13',
                            LastName='Testing',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US',  
                            TimeZoneSidKey='America/Los_Angeles',
                            UserName='test008@testorg.com',
                            ContactId__c = gc.id,
                            City= 'Arendal'
                             );
        insert user ;
         User usersa = new User(
                            Alias = 'standtsa', 
                            profileId = partnerLicenseId1 ,
                            Email='standardsauser@testorg.com',
                            EmailEncodingKey='UTF-8',
                            CommunityNickname = 'testsa13',
                            LastName='Testingsa',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US',  
                            TimeZoneSidKey='America/Los_Angeles',
                            UserName='testsa008@testorg.com',
                            ContactId__c = gc.id,
                            City= 'Arendal'
                             );
        insert usersa ;
        
        User user1 =new User();
        user1 = [select id, contactid__c from user where id=:user.id];
        Account brokerAcc = new Account( Name='testre',
                                        BillingCity = 'Bristol',
                                        BillingCountry = 'United Kingdom',
                                        BillingPostalCode = 'BS1 1AD',
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.Broker_Contact_Record_Type,
                                        Site = '_www.cts.se',
                                        Type = 'Broker' ,
                                        Area_Manager__c = user1.id,      
                                        Market_Area__c = Markt.id ,
                                        Confirm_not_on_sanction_lists__c = true,
                                        License_description__c  = 'some desc',
                                        Description = 'some desc',
                                        Licensed__c = 'Pending',
                                        Sub_Roles__c ='Broker - Reinsurance Broker',
                                        country__c=country.id
                                                                   
                                     );
        AccountList.add(brokerAcc);
        Account clientAcc= New Account();
        clientAcc.Name = 'Testt';
        clientAcc.recordTypeId = System.Label.Client_Contact_Record_Type;
        clientAcc.Site = '_www.test.se';
        clientAcc.Type = 'Customer';
        clientAcc.BillingStreet = 'Gatan 1';
        clientAcc.BillingCity = 'Stockholm';
        clientAcc.BillingCountry = 'SWE';    
        clientAcc.Area_Manager__c = user1.id;        
        clientAcc.Market_Area__c = Markt.id;  
        clientAcc.country__c=country.id; 
        AccountList.add(clientAcc);
        insert AccountList; 
        Contact brokerContact = new Contact( 
                                             FirstName='Raan',
                                             LastName='Baan',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState = 'London',
                                             MailingStreet = '1 London Road',
                                             AccountId = brokerAcc.Id,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(brokerContact) ;
        Contact clientContact= new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity = 'London',
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState = 'London',
                                              MailingStreet = '4 London Road',
                                              AccountId = clientAcc.Id,
                                              Email = 'test321@gmail.com'
                                          );
        ContactList.add(clientContact);
        insert ContactList;
        User brokerUser = new User(
                                    Alias = 'standt', 
                                    profileId = partnerLicenseId,
                                    Email='test120@test.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='estTing',
                                    LanguageLocaleKey='en_US',
                                    CommunityNickname = 'brokerUser',
                                    LocaleSidKey='en_US',  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testbrokerUser@testorg.com.mygard',
                                    ContactId = BrokerContact.Id
                                   );
        UserList.add(brokerUser);
        User clientUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'clientUser',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey='en_US',
                                    LocaleSidKey='en_US',
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testclientUser@testorg.com.mygard',
                                    ContactId = clientContact.id
                                   );
                
        UserList.add(clientUser) ;
        insert UserList;
        
        
        
        PEME_Enrollment_Form__c pefBroker=new PEME_Enrollment_Form__c();
        pefBroker.ClientName__c=brokerAcc.ID;
        pefBroker.Enrollment_Status__c='Draft';
        pefBroker.Submitter__c=brokerContact.Id;
        pefList.add(pefBroker);
        PEME_Enrollment_Form__c pefClient=new PEME_Enrollment_Form__c();
        pefClient.ClientName__c=clientAcc.ID;
        pefClient.Enrollment_Status__c='Draft';
        pefClient.Submitter__c=clientContact.Id;
        pefList.add(pefClient);
        insert pefList;
        
        PEME_Manning_Agent__c pmgBroker=new PEME_Manning_Agent__c();
        //pmg.Name='testpmg';
        pmgBroker.Client__c=brokerAcc.ID;
        pmgBroker.PEME_Enrollment_Form__c=pefBroker.ID;
        pmgBroker.Company_Name__c='Testc Company';
       // pmgBroker.Contact_Person__c='Testc Person';
        pmgBroker.Contact_Point__c=brokerContact.id;
        pmgBroker.Status__c='Approved';
        pmgBroker.Requested_Address__c='testcaddr1';
        pmgBroker.Requested_Address_Line_2__c='testcaddr2';
        pmgBroker.Requested_Address_Line_3__c='testcaddr3';
        pmgBroker.Requested_Address_Line_4__c='testcaddr4';
        pmgBroker.Requested_City__c='testcCity1';
        pmgBroker.Requested_Contact_Person__c='testc person1';
        pmgBroker.Requested_Country__c='testccountry1';
        pmgBroker.Requested_Email__c='testc1@mail.com';
      //  pmgBroker.Requested_Fax__c=1124332211;
        pmgBroker.Requested_Phone__c='1154228855';
        pmgBroker.Requested_State__c='testcstate1';
        pmgBroker.Requested_Zip_Code__c='testczip';
        pmgList.add(pmgBroker);
        
        PEME_Manning_Agent__c pmgClient=new PEME_Manning_Agent__c();
        //pmg.Name='testpmg';
        pmgClient.Client__c=clientAcc.ID;
        pmgClient.PEME_Enrollment_Form__c=pefClient.ID;
        pmgClient.Company_Name__c='Testc Company';
      //  pmgClient.Contact_Person__c='Testc Person';
        pmgClient.Contact_Point__c=clientContact.Id;
        pmgClient.Status__c='Under Approval';// 'Approved'
        pmgClient.Requested_Address__c='testcaddr1';
        pmgClient.Requested_Address_Line_2__c='testcaddr2';
        pmgClient.Requested_Address_Line_3__c='testcaddr3';
        pmgClient.Requested_Address_Line_4__c='testcaddr4';
        pmgClient.Requested_City__c='testcCity1';
        pmgClient.Requested_Contact_Person__c='testc person1';
        pmgClient.Requested_Country__c='testccountry1';
        pmgClient.Requested_Email__c='testc1@mail.com';
     //   pmgClient.Requested_Fax__c=1124332211;
        pmgClient.Requested_Phone__c='1154228855';
        pmgClient.Requested_State__c='testcstate1';
        pmgClient.Requested_Zip_Code__c='testczip';
        pmgList.add(pmgClient);
        
        insert pmgList;
        
        PEME_Debit_note_detail__c pdnBroker=new PEME_Debit_note_detail__c();
        //pdn.Name='testpdn';
        pdnBroker.PEME_Enrollment_Form__c=pefBroker.ID;
        pdnBroker.Contact_person_Email__c='pdnc@test.com';
        pdnBroker.Contact_person_Name__c='conpdncPerson';
      //  pdnBroker.Related_to_manning_agents__c='testclistman';
      //  pdnBroker.Related_to_objects__c='objclisttest';
        //pdn.PEME_Enrollment_Form__c=pef.Id;
        pdnBroker.Requested_Contact_person_Email__c='pdnc@test1.com';
        pdnBroker.Requested_Contact_person_Name__c='conpdncPerson1';
        pdnBroker.Requested_City__c='testpdnccity1';
        pdnBroker.Requested_Country__c='testccountry1';
        pdnBroker.Requested_State__c='testcstate1';
        pdnBroker.Requested_Zip_Code__c='222755';
        pdnBroker.Requested_Address__c='testcaddr1';
        pdnBroker.Requested_Address_Line_2__c='testcaddr2';
        pdnBroker.Requested_Address_Line_3__c='testcaddr3';
        pdnBroker.Requested_Address_Line_4__c='testcaddr4';
        pdnBroker.Status__c= 'Approved' ; //'under approval' 
        pdnBroker.Comments__c='testc comment';
        pdnBroker.RequestedComments__c='testc comment1';
        pdnList.add(pdnBroker);
        
        PEME_Debit_note_detail__c pdnClient=new PEME_Debit_note_detail__c();
        //pdn.Name='testpdn';
        pdnClient.PEME_Enrollment_Form__c=pefClient.ID;
        //pdnClient.Country__c='testpdnccountry';
        pdnClient.Contact_person_Email__c='pdnc@test.com';
        pdnClient.Contact_person_Name__c='conpdncPerson';
      //  pdnClient.Related_to_manning_agents__c='testclistman';
      //  pdnClient.Related_to_objects__c='objclisttest';
        //pdn.PEME_Enrollment_Form__c=pef.Id;
        pdnClient.Requested_Contact_person_Email__c='pdnc@test1.com';
        pdnClient.Requested_Contact_person_Name__c='conpdncPerson1';
        pdnClient.Requested_City__c='testpdnccity1';
        pdnClient.Requested_Country__c='testccountry1';
        pdnClient.Requested_State__c='testcstate1';
        pdnClient.Requested_Zip_Code__c='222755';
        pdnClient.Requested_Address__c='testcaddr1';
        pdnClient.Requested_Address_Line_2__c='testcaddr2';
        pdnClient.Requested_Address_Line_3__c='testcaddr3';
        pdnClient.Requested_Address_Line_4__c='testcaddr4';
        pdnClient.Status__c='under approval'; //'Approved'; 
        pdnClient.Comments__c='testc comment';
        pdnClient.RequestedComments__c='testc comment1';
        pdnList.add(pdnClient);
        
        insert pdnList;
        PEME_Manning_Agent__c pmgClient1=new PEME_Manning_Agent__c();
        //pmg.Name='testpmg';
        pmgClient1.Client__c=clientAcc.ID;
        pmgClient1.PEME_Enrollment_Form__c=pefClient.ID;
        pmgClient1.Company_Name__c='Testc Company';
      //  pmgClient.Contact_Person__c='Testc Person';
        pmgClient1.Contact_Point__c=clientContact.Id;
        pmgClient1.Status__c='Under Approval';
        pmgClient1.Requested_Address__c='testcaddr1';
        pmgClient1.Requested_Address_Line_2__c='testcaddr2';
        pmgClient1.Requested_Address_Line_3__c='testcaddr3';
        pmgClient1.Requested_Address_Line_4__c='testcaddr4';
        pmgClient1.Requested_City__c='testcCity1';
        pmgClient1.Requested_Contact_Person__c='testc person1';
        pmgClient1.Requested_Country__c='testccountry1';
        pmgClient1.Requested_Email__c='testc1@mail.com';
        //pmgClient.Requested_Fax__c=1124332211;
        pmgClient1.Requested_Phone__c='1154228855';
        pmgClient1.Requested_State__c='testcstate1';
        pmgClient1.Requested_Zip_Code__c='testczip';
        pmgList.add(pmgClient1);
        //insert pmgList;
        //System.assertEquals(pdnClient.Address_Line_4__c , 'testc4');
        Test.startTest();
        System.runAs(usersa)
        {
            //PageReference pageRef = page.Enrollment_Approval;
            //Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id' ,pefBroker.Id);
            PEME_Manning_Agent__c PMA = new PEME_Manning_Agent__c();
            ApexPages.StandardController sc = new ApexPages.standardController(PMA);
            PEMEEnrollmentApprovalCtrl PEMEEAppCtrl = new PEMEEnrollmentApprovalCtrl(sc);
            pefBroker.Enrollment_Status__c='Under Approval';
            PEMEEnrollmentApprovalCtrl PEMEEnrollmentApprovalCtrlBroker=new PEMEEnrollmentApprovalCtrl();
            PEMEEnrollmentApprovalCtrlBroker.fetchLatestApprovals(pefBroker.Id);
            system.debug('----->pefbrokerid<-----'+pefBroker.Id);
            PEMEEnrollmentApprovalCtrlBroker.ManAge = new List<PEME_Manning_Agent__c>();
            PEMEEnrollmentApprovalCtrlBroker.debitNote = new List<PEME_Debit_note_detail__c>();
           
            PEMEEnrollmentApprovalCtrlBroker.debitNote.add(pdnBroker);
            PEMEEnrollmentApprovalCtrlBroker.approve();
            PEMEEnrollmentApprovalCtrlBroker.ManAge.add(pmgBroker);
           
            PEMEEnrollmentApprovalCtrlBroker.redirect();
           
            PEMEEnrollmentApprovalCtrlBroker.approvalComments = 'Abc';
          
            PEMEEnrollmentApprovalCtrlBroker.debitNote = new List<PEME_Debit_note_detail__c>();
           
            PageReference pageRef = page.Enrollment_Approval;
            Test.setCurrentPage(pageRef);
             //PEMEEnrollmentApprovalCtrlBroker.reject();
             // PEMEEnrollmentApprovalCtrlBroker.approve();
            //PEMEEnrollmentApprovalCtrlBroker.reject();
            //ProcessInstance pi = [SELECT TargetObjectId, CreatedDate FROM ProcessInstance WHERE TargetObjectId = :pdnBroker.Id];
             // PEMEEnrollmentApprovalCtrlBroker.debNot = new List<PEME_Debit_note_detail__c>();
            //PEMEEnrollmentApprovalCtrlBroker.approve();  
            //PEMEEnrollmentApprovalCtrlBroker.ManAge=pmgList;
            //PEMEEnrollmentApprovalCtrlBroker.cancel();
            //PEMEEnrollmentApprovalCtrlBroker.performApprovals('Approve');
            //PEMEEnrollmentApprovalCtrlBroker.approveRejectExams('Approved');
            PEMEEnrollmentApprovalCtrlBroker.reject();
            //PEMEEnrollmentApprovalCtrlBroker.performApprovals('Approved');
            
            //----added by arpan
            pmgBroker.Status__c='Approved';
            update pmgBroker;
            pdnBroker.Status__c='Approved';
            update pdnBroker;
            pefBroker.Enrollment_Status__c='Under Approval';
            update pefBroker;
            System.currentPageReference().getParameters().put('id' ,pefBroker.Id);
            PEME_Manning_Agent__c PMABroker = new PEME_Manning_Agent__c();
            ApexPages.StandardController scBroker = new ApexPages.standardController(PMABroker);
            PEMEEnrollmentApprovalCtrl PEMEEAppCtrBr = new PEMEEnrollmentApprovalCtrl(scBroker);
            
          Test.stopTest();
   
        }
   
      
    }
     
        

}