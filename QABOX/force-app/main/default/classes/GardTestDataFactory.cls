public class GardTestDataFactory{
 public List<User> userList{get;set;}
           List<Market_Area__c>marketList{get;set;}
           List<Country__c>countryList{get;set;}
           List<Gard_Contacts__c> gardConList{get;set;}
           List<object__c>objectList{get;set;}
           public GardTestDataFactory(){
           gardConList=getGardContact(5);
            userList=getCrmUser(5);
           marketList=getMarketArea();
            countryList=getCountry();
            objectList=getObjects(10);
}
 public List<Valid_Role_Combination__c> getValidClientRoleCombination(){
         List<Valid_Role_Combination__c> vrcList=new List<Valid_Role_Combination__c> ();
      Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
      vrcList.add(vrcClient);
      Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
      vrcList.add(vrcBroker);
      Valid_Role_Combination__c vrcClinic = new Valid_Role_Combination__c(Role__c = 'External Service Provider',Sub_Role__c='ESP - Agent');
      vrcList.add(vrcClinic);
      insert vrcList;
      return vrcList;
    
 }
  public AdminUsers__c getAdminUsersSetting(){
           AdminUsers__c adminUser= new AdminUsers__c();
            adminUser.Name = 'Number of users';
            adminUser.Value__c = 3;
            insert adminUser;
            return adminUser;
  }
  public List<Market_Area__c> getMarketArea(){
      List<Market_Area__c> marketAreaList=new List<Market_Area__c>();
       marketAreaList.add(new Market_Area__c(name='Charterers & Traders', Market_Area_Code__c ='CHATR'));
       marketAreaList.add(new Market_Area__c(name='Energy', Market_Area_Code__c ='ENERGY'));
       marketAreaList.add(new Market_Area__c(name='Marine Builders Risks', Market_Area_Code__c ='MBR'));
        marketAreaList.add(new Market_Area__c(name='Offshore', Market_Area_Code__c ='OFCHA'));
       marketAreaList.add(new Market_Area__c(name='Shipowners', Market_Area_Code__c ='OWN'));
       insert marketAreaList;
       return marketAreaList;
 

  }
   public List<Country__c> getCountry(){
      List<Country__c> countryList = new List<Country__c>();
       countryList.add(new Country__c(name='Angola (AGO)', Country_ISO_Code__c='AGO'));
       countryList.add(new Country__c(name='Belgium (BEL)', Country_ISO_Code__c='BEL'));
       countryList.add(new Country__c(name='Canada (CAN)', Country_ISO_Code__c='CAN'));
        countryList.add(new Country__c(name='Norway (NOR)', Country_ISO_Code__c='NOR'));
             insert countryList;
       return countryList;
 

  }
 
  public List<Gard_Contacts__c> getGardContact(integer count){
  
    List<Gard_Contacts__c> gardConList=new List<Gard_Contacts__c>();
    for(integer i=0;i<10;i++){
       gardConList.add(new Gard_Contacts__c(
                       FirstName__c = 'TestFirstName'+i,
                       LastName__c ='TestLastName'+i,
                       Email__c ='testemail@gard.no' 
                       ));
                       
    }
    insert gardConList;
    return gardConList;
  }
  public List<User> getCrmUser(integer count){
     
     
     List<User>userList=new List<User>();
     Id profileId= [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
     for(integer i=0;i<count;i++){
         
       userList.add(new User(
                 Alias = 'standt'+i, 
                profileId = profileId,
                Email='testuser.'+i+'@testorg.com',
                EmailEncodingKey='UTF-8',
                CommunityNickname = 'test13',
                LastName='Testing',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',  
                TimeZoneSidKey='America/Los_Angeles',
                UserName='test008.' + System.currentTimeMillis() + '@testorg.com',
                isActive = true ,
                
                ContactId__c = gardConList.size()> i?gardConList[i].id:gardConList[0].id,
                City= 'Arendal'

                     ));
                     
     }
     insert userList;
     return userList;
  }
   public List<User> getGardUser(integer count){
     
     
     List<User>userList=new List<User>();
     Id profileId= [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Partner Community' and name='Partner Community Login User Custom' limit 1].id;
     for(integer i=0;i<count;i++){
         
       userList.add(new User(
                 Alias = 'standt'+i, 
                profileId = profileId,
                Email='testuser.'+i+'@testorg.com',
                EmailEncodingKey='UTF-8',
                CommunityNickname = 'gardUser',
                LastName='Testing',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',  
                TimeZoneSidKey='America/Los_Angeles',
                UserName='test008.' + System.currentTimeMillis() + '@testorg.com',
                isActive = true ,
                
                ContactId__c = gardConList.size()> i?gardConList[i].id:gardConList[0].id,
                City= 'Arendal'

                     ));
                     
     }
     insert userList;
     return userList;
  }
 
  public List<Account> getClientCompany(integer count){
          
            Blob b1 = Crypto.GenerateAESKey(128);            
            String h1 = EncodingUtil.ConvertTohex(b1);            
            String guid1 = h1.SubString(0,8)+ '-' + h1.SubString(8,12) + '-' + h1.SubString(12,16) + '-' + h1.SubString(16,20);
             
            List<Account> clientComList=new List<Account>();
            for(integer i=0;i<count;i++){
              clientComList.add( new Account(
                  Name = 'clientCompany'+i,
                                   recordTypeId = System.Label.Client_Contact_Record_Type,
                                   Site = '_www.test.se',
                                   Type = 'Customer',
                                   GUID__c=guid1,
                                   BillingStreet = 'Gatan 1'+i,
                                   BillingCity = 'Stockholm'+i,
                                   BillingCountry = 'SWE'+i,
                                   Area_Manager__c = userList.size()> i?userList[i].id: userList[0].id,            
                                   Market_Area__c = marketList.size()> i?marketList[i].id:marketList[0].id, 
                                   PEME_Enrollment_Status__c='Enrolled',
                                   Company_Role__c = 'Client',        
                                   Sub_Roles__c = '',
                                   Country__c = countryList.size()> i?countryList[i].id:countryList[0].id

                     ));
                               
                 }
                 insert clientComList;
               return clientComList; 
         }
      public List<Account> getESP(integer count){
       Blob b1 = Crypto.GenerateAESKey(128);            
            String h1 = EncodingUtil.ConvertTohex(b1);            
            String guid1 = h1.SubString(0,8)+ '-' + h1.SubString(8,12) + '-' + h1.SubString(12,16) + '-' + h1.SubString(16,20);
             
            List<Account> espComList=new List<Account>();
            for(integer i=0;i<count;i++){
              espComList.add( new Account(
                  Name = 'espCompany'+i,
                                   recordTypeId=System.Label.ESP_Contact_Record_Type,
                                   Site = '_www.test.se',
                                   Type = 'Customer',
                                   GUID__c=guid1,
                                   BillingStreet = 'Gatan 1'+i,
                                   BillingCity = 'Stockholm'+i,
                                   BillingCountry = 'SWE'+i,
                                   Area_Manager__c = userList.size()> i?userList[i].id: userList[0].id,    
                                   Market_Area__c = marketList.size()> i?marketList[i].id:marketList[0].id, 
                                   PEME_Enrollment_Status__c='Enrolled',
                                   Company_Role__c = 'External Service Provider',        
                                   Sub_Roles__c = 'ESP - Agent',
                                   Country__c = countryList.size()> i?countryList[i].id:countryList[0].id

                     ));
                               
                 }
                 insert espComList;
               return espComList;
      }
      public List<Contact> getContact(integer count){
        List<Contact>contList=new List<contact>();
        List<Account>clientList=getClientCompany(count);
        for(integer i=0;i<count;i++){
         contList.add(new Contact(FirstName='firstName'+i,
                                       LastName='lastName'+i,
                                       MailingCity = 'London',
                                       MailingCountry = 'United Kingdom',
                                       MailingPostalCode = 'SE1 1AE',
                                       MailingState = 'London',
                                       MailingStreet = '4 London Road',
                                       AccountId = clientList[i].Id,
                                       Email = 'test321.'+i+'@gmail.com'));
        }
        insert contList;
        return contList;
      }
      
      public List<object__c>getObjects(integer count){
         List<object__c>objectList=new List<Object__c>();
        for(integer i=0;i<count;i++){
          
          objectList.add(new object__c(
                       name='testObject'+i,
                       flag__c='Hong Kong',
                       Object_Unique_ID__c  ='10000'+i,
                       Year_built__c='2020',
                       Imo_Lloyds_No__c ='000000'+i
                    ));
        }
        insert objectList;
      return objectList;
      }
      public List<PEME_Enrollment_Form__c>getPEMEEnrollmentForm(integer count){
        List<Contact>contactList=getContact(count);
        List<PEME_Enrollment_Form__c> pemeEnformList=new List<PEME_Enrollment_Form__c>();
        for(integer i=0;i<count;i++){
        pemeEnFormList.add(new PEME_Enrollment_Form__c(
           ClientName__c=contactList[i].accountId,
           Comments__c='Report analyzed',
           Enrollment_Status__c= 'Draft',
           Gard_PEME_References__c='Test References'+i,
           Last_Status_Change_Date__c=Date.valueOf(System.today().adddays(i)),
           Submitter__c=contactList[i].id,
           DisEnrolled_Expire_Date__c=System.today().adddays(i+1)
        ));
        }
        insert pemeEnFormList;
        return pemeEnFormList;
      }
       public List<PEME_Manning_Agent__c>getPEMEmanningAgent(integer count){
        List<PEME_Enrollment_Form__c>pemeEnFormList=getPEMEEnrollmentForm(count);
        List<PEME_Manning_Agent__c> pemeManningAgentList=new List<PEME_Manning_Agent__c>();
        for(integer i=0;i<count;i++){
        pemeManningAgentList.add(new PEME_Manning_Agent__c(
           Client__c=pemeEnFormList[i].ClientName__c,
           PEME_Enrollment_Form__c=pemeEnFormList[i].id,
           Company_Name__c='Test Company'+i,
           Contact_Point__c=pemeEnFormList[i].Submitter__c,
           Status__c='Approved'

        ));
        }
        insert pemeManningAgentList;
        return pemeManningAgentList;
      }

}