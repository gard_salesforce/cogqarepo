@isTest(seeAllData=false)
Public class TestUWRForShipOwnersCtrl1
{
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    
    
    Public static testmethod void ShipOwnersCtrl(){     
        test.StartTest();   
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        GardTestData test_rec = new GardTestData();
        test_rec.commonrecord(); 
        test_rec.customsettings_rec();
        //Invoking methods of UWRForShipOwnersCtrl for broker and client users.....           
        
        Country_code__c countryCode = new Country_code__c(display_value__c='Test');
        insert countryCode;
        Country__c country = new Country__c(Name='India');   
        insert country;  
        Port__c port1 = new Port__c(Name='Test', Country__c=country.id);
        insert port1;
        Nationality__c nationality = new Nationality__c(Description__c='Test');
        insert nationality;
        
           
        test.StopTest();
        System.runAs(GardTestData.clientUser)  
        {
            UWRForShipOwnersCtrl test_client=new UWRForShipOwnersCtrl();
            
            List<SelectOption> clientOptions =  test_client.getClientLst();
            List<SelectOption> flag=test_client.getFlagLst();
            List<SelectOption> port=test_client.getport();
            List<SelectOption> ClassificationList=test_client.getClassificationList();
            //List<SelectOption> classification=test_client.getclassS();
            test_client.selectedCollLiabilities = '0 RDC'; 
            test_client.selectedIncludingFFO = 'no';
            
            system.assertequals(flag.size()>0,true,true);
            system.assertequals(clientOptions .size()>0,true,true);
            system.assertequals(port.size()>0,true,true);
            system.assertequals(ClassificationList.size()>0,true,true);
            test_client.assetFinal = null;
            test_client.selectedObj=GardTestData.test_object_1st.id;
            test_client.selectedClient = GardTestData.clientAcc.id;
            test_client.ObjName = string.valueof(GardTestData.test_object_1st.Imo_Lloyds_No__c); 
            test_client.FindBy='imo';       
            test_client.searchByImoObjectName();
            test_client.assetFinal = null;
            test_client.selectedObj=GardTestData.test_object_1st.id;
            test_client.selectedClient = GardTestData.clientAcc.id;
            test_client.ObjName = string.valueof(GardTestData.test_object_1st.name); 
            test_client.FindBy='objname';
            test_client.searchByImoObjectName();   
            test_client.selectedCountryCode = 'IN - India';
            test_client.riskAttachmentDate='05.02.2015';
            test_client.dateOutputForReview='05-02-2015';
            test_client.setObjectSelection();
            // test_client.saveRecordForReview();
            test_client.Classification='test3';
            test_client.uwf = new Case();
            test_client.chkAccess();
            test_client.uwf.tonnage__c=integer.valueof('123456');
            test_client.saveRecord();
            test_client.clearRecord();
            test_client.getCName();
            test_client.grossTonnage=343434; 
            
            //test_client.saveRecordForAddlInfo();
            test_client.sendToForm();
            // test_client.callSendEmail();
            test_client.clearRecordForAddlInfo();
            test_client.goPrevForAddlInfo();
            test_client.cancelRecord();
            test_client.goPrevForReview();
            test_client.goNextForAddlInfo();
            test_client.uwf.id=GardTestData.brokerCase.id;
            test_client.uwf = new Case();
            test_client.uwf.tonnage__c=integer.valueof('123456');
            test_client.loggedInAccountId = GardTestData.clientAcc.Id;
            test_client.selectedCollLiabilities = '1/4 RDC'; 
            test_client.selectedIncludingFFO = 'yes';
            test_client.saveRecord();
        }
    }
}