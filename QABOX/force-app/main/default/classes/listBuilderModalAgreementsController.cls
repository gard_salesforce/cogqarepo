/**
 * Created by ohuuse on 22/10/2019.
 * Cloned by Pulkit on 11/02/2021 for SF-5892.
 */

public with sharing class listBuilderModalAgreementsController {

    // Input: Id from Case
    // Return: jsonstring with covers (Assets) marked with checked = true
    // Cover_and_Objects_for_case__c relation
    @AuraEnabled //(Cacheable = True)
    public static String getRelatedRelevantAgreements(Id caseId) {
        // list that will be serialized and returned to the lwc
        List<jsonOut> jsonOuts = new List<jsonOut>();
        List<Id> agreementsGot = new List<Id>();
        // loops through the map and adds them to new instances of the class jsonOut
        for (Cover_and_Objects_for_case__c jc : [SELECT Id, Name, Agreement__c,Agreement__r.Agreement_Reference__c,
                                                 Agreement__r.Policy_Year__c,Agreement__r.Account.Name,Agreement__r.AccountId,
                                                 /*Cover__r.Name, Cover__r.Object__r.Name,Cover__r.On_risk_indicator__c,*/
                                                 Agreement__r.Business_Area__c,Agreement__r.Expiration_Date__c,
                                                 Agreement__r.Agreement_Type__c,Agreement__r.Inception_date__c                                 
                                                 FROM Cover_and_Objects_for_case__c WHERE Case__c = :caseId AND Agreement__c != null]
            ){
                if(agreementsGot.contains(jc.Agreement__c)) continue;
                agreementsGot.add(jc.Agreement__c);                       
                jsonOut js = new jsonOut();
                //js.recordId = jc.Cover__c;
                js.recordId = jc.Agreement__c;
                js.checked = True;
                //js.agreementReference = jc.Cover__r.Agreement__r.Agreement_Reference__c;
                js.companyName = jc.Agreement__r.Account.Name;
                //js.coverName = jc.Cover__r.Name;
                //js.objectName = jc.Cover__r.Object__r.Name;
                js.policyYear = jc.Agreement__r.Policy_Year__c;
                //js.riskIndicator = jc.Cover__r.On_risk_indicator__c;
                js.accountId = jc.Agreement__r.AccountId;
                
                js.agreementNumber = jc.Agreement__r.Agreement_Reference__c;
                js.businessArea = jc.Agreement__r.Business_Area__c;
                js.agreementType = jc.Agreement__r.Agreement_Type__c;
                js.inceptionDate = jc.Agreement__r.Inception_date__c;
                js.expirationDate = jc.Agreement__r.Expiration_Date__c;
                
                jsonOuts.add(js);
            }
        
        return JSON.serialize(jsonOuts);
    }

    // Will SOQL account name based on a input string and return a json string
    @AuraEnabled (Cacheable = True)
    public static String getRelevantAccounts(String companyString) {
        //System.debug('getRelevantAccounts - '+companyString);
        // make the query open for symbols before and after the input string
        String searchString = '%' + companyString + '%';
        String searchStringCId = companyString + '%';

        // list that will be serialized and returned to the lwc
        List<jsonAccountOut> jsonAccountOuts = new List<jsonAccountOut>();
        // loops through the 10 first results from the soql query and adds
        // them to new instances of the class jsonAccountOut
        for(Account acc : [SELECT Id, Name, BillingCity, Company_ID__c, Company_Role__c FROM Account WHERE (Name LIKE :searchString OR Company_ID__c LIKE :searchStringCId) AND Active__c = true AND Company_Role__c IN ('broker','client') ORDER BY Name,Company_ID__c LIMIT 10]){
            jsonAccountOut jsacc = new jsonAccountOut();
            jsacc.recordId = acc.Id;
            jsacc.name = acc.Name;
            jsacc.billingCity = acc.BillingCity;
            jsacc.companyId = acc.Company_ID__c;
            jsacc.role = acc.Company_Role__c;
            jsonAccountOuts.add(jsacc);
        }
        //System.debug('jsonAccountOuts.size() - '+jsonAccountOuts.size());

        if (jsonAccountOuts.size() > 0) {
            //System.debug('jsonAccountOuts - '+jsonAccountOuts);
            return JSON.serialize(jsonAccountOuts);
        } else {
            return '';
        }

    }

    // This method will take in the caseId and a json string with related covers that are to be junctioned to the the
    // case, or the junction is to be deleted
    @AuraEnabled
    public static void deleteCreateJunctionAgreements(Id caseId, string jsonString) {
        //System.debug('jsonString - '+jsonString);
        // deserialize the jsonString to the class jsonsIn
        List<jsonIn> agreementsToModify = (List<jsonIn>) JSON.deserialize(jsonString, List<jsonIn>.class);

        // lists used to separate between deleting and deleting new junction records (Cover_and_Objects_for_case__c)
        List<Id> agreementJunctionsToCreate = new List<Id>();
        List<Id> agreementJunctionsToDelete = new List<Id>();

        // lists used to gather records for dml action at the end
        List<Cover_and_Objects_for_case__c> deleteAgreementJunctionsList = new List<Cover_and_Objects_for_case__c>();
        List<Cover_and_Objects_for_case__c> newAgreementJunctionsList = new List<Cover_and_Objects_for_case__c>();

        // sorts Cover_and_Objects_for_case__c into to lists based on their "dml tag"
        for (jsonIn agreementToModify : agreementsToModify) {
            if (agreementToModify.dml == 'delete') {
                agreementJunctionsToDelete.add(agreementToModify.recordId);
            } else if (agreementToModify.dml == 'create') {
                agreementJunctionsToCreate.add(agreementToModify.recordId);
            }
        }
        //System.debug('coverJunctionsToDelete - '+agreementJunctionsToCreate);
        //System.debug('coverJunctionsToDelete - '+agreementJunctionsToDelete);
        //System.debug('agreementIdsToCreate - '+agreementIdsToCreate);
        /*
        //List<Id> realtedCoverIdsToDelete = [SELECT Id FROM Asset WHERE Agreement__c IN :agreementIdsToDelete];
        //List<Id> realtedCoverIdsToCreate = [SELECT Id FROM Asset WHERE Agreement__c IN :agreementIdsToCreate];
        for(Asset dmlAsset : [SELECT Id,Agreement__c FROM Asset WHERE Agreement__c IN :agreementIdsToCreate]){
            System.debug('dmlAsset - '+dmlAsset);
            if (agreementIdsToCreate.contains(dmlAsset.Agreement__c)) {
                coverJunctionsToCreate.add(dmlAsset.Id);
            }
        }*/
        //System.debug('agreementJunctionsToDelete - '+agreementJunctionsToDelete);
        //System.debug('agreementJunctionsToCreate - '+agreementJunctionsToCreate);
		
        // finds Cover_and_Objects_for_case__c to delete
        if (agreementJunctionsToDelete.size() > 0) {
            //deleteAgreementJunctionsList.addAll([SELECT Id FROM Cover_and_Objects_for_case__c WHERE Cover__c IN :coverJunctionsToDelete AND Case__c = :caseId]);
            deleteAgreementJunctionsList.addAll([SELECT Id FROM Cover_and_Objects_for_case__c WHERE Agreement__c IN :agreementJunctionsToDelete AND Case__c = :caseId]);
        }

        // create (not commit) new Cover_and_Objects_for_case__c
        if (agreementJunctionsToCreate.size() > 0) {
            for (Id agreementId : agreementJunctionsToCreate) {
                Cover_and_Objects_for_case__c newAgreementJunction = new Cover_and_Objects_for_case__c(
                    Case__c = caseId,
                    Agreement__c = agreementId
                    //Cover__c = coverId    
                );
                newAgreementJunctionsList.add(newAgreementJunction);
            }
        }

        // delete old Cover_and_Objects_for_case__c
        if (deleteAgreementJunctionsList.size() > 0) {
            try {
                //System.debug('in delete try');
                delete deleteAgreementJunctionsList;

            } catch (Exception e) {
                //System.debug(e.getMessage());
                throw new AuraHandledException('Something went wrong deleting the old coverJunctions: '
                        + e.getMessage());
            }
        }

        // insert new Cover_and_Objects_for_case__c
        if (newAgreementJunctionsList.size() > 0) {
            try {
                //System.debug('in insert try');
                insert newAgreementJunctionsList;

            } catch (Exception e) {
                //System.debug(e.getMessage());
                throw new AuraHandledException('Something went wrong creating the new coverJunctions: '
                        + e.getMessage());
            }
        }
    }

    // used to send information to the lwc listBuilderModal
    class jsonOut {
        Id recordId;
        Boolean checked;
        String companyName;
        String policyYear;
        String accountId;
        String agreementNumber;
        String businessArea;
        String agreementType;
        Date inceptionDate;
        Date expirationDate;
        //String agreementReference;
        //String coverName;
        //String objectName;
        //Boolean riskIndicator;
    }

    // used to retriev information from the lwc listBuilderModal
    public class jsonIn {
        public Id recordId;
        public String dml;

    }

    // used to send account information to the lwc listBuilderModal
    class jsonAccountOut {
        Id recordId;
        String name;
        String billingCity;
        String companyId;
        String role;
    }

    // Dynamic soql that supplies additional covers. a lwc can send in filters in order to reduce the stress on the front end tabel functionality
    @AuraEnabled
    public static String getMoreRelevantAgreements(List<Id> accountIds, Integer lim, List<Id> notWantedAgreements,/*List<Id> notWantedCovers,*/ Map<String, Object> filterValueMap, Map<String, Object> sortMap) {
        //System.debug('getMoreRelevantAgreements');
        // filters
        Map<String, String> filterFieldMap = new Map<String, String>{
            'agreementNumber' => 'Agreement_Reference__c',
                'companyName' => 'Account.Name',
                'businessArea' => 'Business_Area__c',
                'policyYear' => 'Policy_Year__c',
                'inceptionDate' => 'Inception_date__c',
                'expirationDate' => 'Expiration_Date__c',
                'agreementType' => 'Agreement_Type__c'
                
                //'riskIndicator' => 'On_risk_indicator__c',//obsolete
                //'coverName' => 'Name',//obsolete
                //'objectName' => 'Object__r.Name'//obsolete
                
        };
        //System.debug('filterValueMap - '+filterValueMap);for(String key : filterValueMap.keySet())System.debug(key+' -> '+filterValueMap.get(key));
        //System.debug('filterFieldMap - ');for(String key : filterFieldMap.keySet())System.debug(key+' -> '+filterFieldMap.get(key));
        //System.debug('sortMap - '+sortMap);for(String key : sortMap.keySet())System.debug(key+' -> '+sortMap.get(key));

        //String filterString = 'SELECT Id, Agreement__r.Agreement_Reference__c, Agreement__c, Agreement__r.Policy_Year__c, Account.Name, Name, Object__r.Name, On_risk_indicator__c, AccountId FROM Asset WHERE AccountId IN :accountIds AND Id NOT IN :notWantedCovers ';
        String filterString = 'SELECT Id, Agreement_Reference__c,Policy_Year__c,Account.Name,Name,AccountId,Business_Area__c,Agreement_Type__c,Inception_date__c,Expiration_Date__c FROM Contract WHERE AccountId IN :accountIds AND Id NOT IN :notWantedAgreements ';//notWantedCovers//, /*Object__r.Name,*/ /*On_risk_indicator__c,*/

        if (filterValueMap.get('policyYear') != null) {
            filterString += ' AND ' + filterFieldMap.get('policyYear') + '=' + '\''+ String.valueOf(filterValueMap.get('policyYear')) +'\'';
        }

        if (filterValueMap.get('agreementNumber') != null) {
            filterString += ' AND ' + filterFieldMap.get('agreementNumber') + ' LIKE \'%' + filterValueMap.get('agreementNumber') + '%\'';
        }

        /*if (filterValueMap.get('coverName') != null) {
            filterString += ' AND ' + filterFieldMap.get('coverName') + ' LIKE \'%' + filterValueMap.get('coverName') + '%\'';
        }*/

        /*if (filterValueMap.get('objectName') != null) {
            filterString += ' AND ' + filterFieldMap.get('objectName') + ' LIKE \'%' + filterValueMap.get('objectName') + '%\'';
        }*/
        
        if (filterValueMap.get('companyName') != null) {
            filterString += ' AND ' + filterFieldMap.get('companyName') + '=' + '\''+ String.valueOf(filterValueMap.get('companyName')) +'\'';
        }
        
        if (filterValueMap.get('businessArea') != null) {
            filterString += ' AND ' + filterFieldMap.get('businessArea') + '=' + '\''+ String.valueOf(filterValueMap.get('businessArea')) +'\'';
        }
        
        if (filterValueMap.get('inceptionDate') != null) {
            filterString += ' AND ' + filterFieldMap.get('inceptionDate') + '=' + '\''+ String.valueOf(filterValueMap.get('inceptionDate')) +'\'';
        }
        
        if (filterValueMap.get('expirationDate') != null) {
            filterString += ' AND ' + filterFieldMap.get('expirationDate') + '=' + '\''+ String.valueOf(filterValueMap.get('expirationDate')) +'\'';
        }
        
        if (filterValueMap.get('agreementType') != null) {
             filterString += ' AND ' + filterFieldMap.get('agreementType') + '=' + '\''+ String.valueOf(filterValueMap.get('agreementType')) +'\'';
        }

        /*filterString += ' AND ' + filterFieldMap.get('riskIndicator') + ' = ' + filterValueMap.get('riskIndicator');*/

        if (sortMap.get('by') != null) {
            filterString += ' ORDER BY ' + filterFieldMap.get(String.valueOf(sortMap.get('by'))) + ' ' + sortMap.get('direction') + ' LIMIT :lim';
        } else {
            filterString += ' ORDER BY Name DESC LIMIT :lim';
        }
        //System.debug(filterString);

        List<jsonOut> jsonOuts = new List<jsonOut>();

        //List<Asset> additionalCovers = Database.query(filterString);
        //for (Asset jc : additionalCovers) {
        //System.debug('filterString - '+filterString);
        List<Contract> additionalAgreements = Database.query(filterString);
        for (Contract jc : additionalAgreements) {
            jsonOut js = new jsonOut();
            js.recordId = jc.Id;
            js.checked = False;
            //js.agreementReference = jc.Agreement__r.Agreement_Reference__c;
            js.companyName = jc.Account.Name;
            //js.coverName = jc.Name;
            //js.objectName = jc.Object__r.Name;
            js.policyYear = jc.Policy_Year__c;
            //js.riskIndicator = jc.On_risk_indicator__c;
            js.accountId = jc.AccountId;
            js.agreementNumber = jc.Agreement_Reference__c;
            js.businessArea = jc.Business_Area__c;
            js.agreementType = jc.Agreement_Type__c;
            js.inceptionDate = jc.Inception_date__c;
            js.expirationDate = jc.Expiration_Date__c;
            jsonOuts.add(js);
        }
        //System.debug(jsonOuts.size());
        return JSON.serialize(jsonOuts);
    }
}