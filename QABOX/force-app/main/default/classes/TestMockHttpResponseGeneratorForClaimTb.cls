@isTest
global class TestMockHttpResponseGeneratorForClaimTb implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
		response.setBody('[{"claimId":"G136886","clientId":"45516","eventName":"Converted timebar for CaseNo 92785/2012","eventDate":"2015-05-22T10:00:00","reminderDate":"24.06.2017","sDescription":"","eventType":"","claimCaseNo":"92785/12","eventGuid":"7597fd35-4b9d-415c-b4dd-8bb9c224db15","subscribers":"PHN1YnNjcmliZXJzPgogIDxzdWJzY3JpYmVyIHR5cGU9IiJjbGFpbXNoYW5kbGVyIiI+U0VUQVJOPC9zdWJzY3JpYmVyPgo8L3N1YnNjcmliZXJzPg==","sstatus":"LIVE","systemsource":"3","creator":"CONVER","claiming":-1,"resolution":"","resolvedBy":"","resolveDate":"24.08.2018"}]');
        response.setStatusCode(200);
        return response;
    }
}