//Created for SF-6306
global class SurveyCustomerFeedbackCreator implements Database.Batchable<SObject>,Database.Stateful{
	global List<Id> surveyIds;
    global Id responsibleUserId;// 005D0000001klD9;
    global String title;
    global String typeOfFeedback;
    global String customerFeedbackType;
    global String status;
    global Integer batchSize;
    
    //later, confirm following fields
    global String feedbackForGardDepartment;
    global String source;
    global String keyFeedbackIssueArea;
    
    public void runBatch(){
        if(this.surveyIds == null || this.surveyIds.isEmpty()) return;
        Database.executeBatch(this,this.batchSize);
    }
    
    public SurveyCustomerFeedbackCreator(List<Id> surveyIds){
        this.surveyIds = surveyIds;
        this.responsibleUserId = '00520000001K5hG';//Olga default
        this.title = 'Annual Legal Cost Committee/Feedback on performance';
        //this.typeOfFeedback = 'Not assessed';
        this.typeOfFeedback = 'Internal feedback';
        this.customerFeedbackType = 'Open';
        this.feedbackForGardDepartment = 'Claims';
        this.source = 'Surveys';
        this.keyFeedbackIssueArea = 'Service';
        this.status = 'closed';
        this.batchSize = 50;
    }
    
    global Database.QueryLocator start(Database.BatchableContext dbBc){
        return Database.getQueryLocator('SELECT '
                                        +'id,fluidoconnect__Survey__c,fluidoconnect__Survey__r.ALC_Company__c,CreatedDate,'
                                        +'(SELECT '
                                        +'Id,Question_Text_English__c,fluidoconnect__Response_Bool__c,fluidoconnect__Response__c,fluidoconnect__Summary_del__c,fluidoconnect__Question__r.fluidoconnect__Sort_Order__c,fluidoconnect__Question__r.fluidoconnect__Question_Description__c '
                                        +'FROM fluidoconnect__Responses__r '
                                        +'ORDER BY fluidoconnect__Question__r.fluidoconnect__Sort_Order__c) '
                                        +'FROM fluidoconnect__Invitation__c '
                                        +'WHERE fluidoconnect__Survey__c IN :surveyIds AND fluidoconnect__Survey__r.ALC_Company__c != null');
    }
    
    global void execute(Database.BatchableContext dbBc,List<fluidoconnect__Invitation__c> surveyMembers){
        List<Customer_Feedback__c> customerFeedbacks = new List<Customer_Feedback__c>();
        for(fluidoconnect__Invitation__c surveyMember : surveyMembers){
            String description;
            Map<Integer,String> numberToResponse;
            if(surveyMember.fluidoconnect__Responses__r.isEmpty()) continue;
            for(fluidoconnect__Response__c surveyResponse : surveyMember.fluidoconnect__Responses__r){
                description = (description == null) ? 
                    surveyResponse.fluidoconnect__Question__r.fluidoconnect__Question_Description__c+' : '+(surveyResponse.fluidoconnect__Response_Bool__c ? 'Yes' : 'No')+(!String.isBlank(surveyResponse.fluidoconnect__Response__c) ? ' - '+surveyResponse.fluidoconnect__Response__c : (!String.isBlank(surveyResponse.fluidoconnect__Summary_del__c) ? ' - '+surveyResponse.fluidoconnect__Summary_del__c : '' ) )
                    : description+'\n'+(surveyResponse.fluidoconnect__Question__r.fluidoconnect__Question_Description__c+' : '+(surveyResponse.fluidoconnect__Response_Bool__c ? 'Yes' : 'No')+(!String.isBlank(surveyResponse.fluidoconnect__Response__c) ? ' - '+surveyResponse.fluidoconnect__Response__c : (!String.isBlank(surveyResponse.fluidoconnect__Summary_del__c) ? ' - '+surveyResponse.fluidoconnect__Summary_del__c : '' ) ));
            }
            customerFeedbacks.add(
                new Customer_Feedback__c(
                    Title__c = this.title,
                    Type_of_feedback__c = this.typeOfFeedback,
                    Customer_feedback_type__c = this.customerFeedbackType,
                    Feedback_for_Gard_Department__c = this.feedbackForGardDepartment,
                    Source__c = this.source,
                    Key_feedback_issue_area__c = this.keyFeedbackIssueArea,
                    Responsible__c = this.responsibleUserId,
                    Date_feedback_received__c = surveyMember.CreatedDate.date(),
                    Status__c = this.status,
                    Company_to__c = surveyMember.fluidoconnect__Survey__r.ALC_Company__c,
                    Description__c = description
                    //,Source_Survey_Invitation__c = surveyMember.id//later, check for this field
                )
            );
        }
        Database.insert(customerFeedbacks,false);
    }
    
    global void finish(Database.BatchableContext dbBc){}
    
}

/*
CODE TO RUN THIS CLASS : 
List<Id> sourceSurveyIds = new List<Id>{'a0d3z00002J4E4CAAV'};//surveyIds
SurveyCustomerFeedbackCreator sCFC = new SurveyCustomerFeedbackCreator(sourceSurveyIds);
sCFC.responsibleUserId = '005D0000001klD9';
sCFC.title = 'Annual Legal Cost Committee/Feedback on performance 2020/21';
sCFC.typeOfFeedback = 'Not assessed';
sCFC.customerFeedbackType = 'Open';
sCFC.feedbackForGardDepartment = 'Claims';
sCFC.source = 'Surveys';
sCFC.keyFeedbackIssueArea = 'Service';
sCFC.status = 'Closed';
sCFC.batchSize = 50;
sCFC.runBatch();
*/