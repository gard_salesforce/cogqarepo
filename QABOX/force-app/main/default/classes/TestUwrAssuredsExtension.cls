@isTest private class TestUwrAssuredsExtension{
    
    @isTest private static void assuredsExtension(){
            UwrAssuredsExtension uae = new UwrAssuredsExtension();
    }
    
    @isTest private static void assuredsFromShipOwners(){     
        test.StartTest();
        createDataForTest();
        test.StopTest();
        System.runAs(GardTestData.clientUser){
            UWRForShipOwnersCtrl shipOwnersClient = new UWRForShipOwnersCtrl();
            UwrAssuredsExtension uae = new UwrAssuredsExtension(shipOwnersClient);
            UwrAssuredsExtension.UwrAssuredsWrapper uaw = uae.uAWs.get(0);
            uaw.companyName = GardTestData.clientAcc.name;
            uaw.memberType = uae.allMemberTypesSO.get(0).getValue();
            uaw.country = uae.allCountriesSO.get(0).getValue();
            uaw.city = 'Gotham';
            uaw.capacity = uae.allCapacitiesSO.get(0).getValue();
            uaw.capacityText = 'otherCapText';
            uaw.natRegNo = '123456';
            uaw.isMember = true;
            uaw.parentUWRForm = GardTestData.clientAcc.id;
            uae.parentUWRFormId = GardTestData.clientAcc.id;
            uae.saveAssureds();
            uae.rowIndexExt = uae.uAWs.size()-1;
            uae.deleteRow();
            UwrAssuredsExtension.getAllCapacitiesSO();
        }
    }
    
    @isTest private static void assuredsFromMOU(){     
        test.StartTest();
        createDataForTest();
        test.StopTest();
        System.runAs(GardTestData.clientUser){
            UWRMOUCtrl mouClient = new UWRMOUCtrl();
            UwrAssuredsExtension uae = new UwrAssuredsExtension(mouClient);
            UwrAssuredsExtension.UwrAssuredsWrapper uaw = uae.uAWs.get(0);
            uaw.companyName = GardTestData.clientAcc.name;
            uaw.memberType = uae.allMemberTypesSO.get(0).getValue();
            uaw.country = uae.allCountriesSO.get(0).getValue();
            uaw.city = 'Gotham';
            uaw.capacity = uae.allCapacitiesSO.get(0).getValue();
            uaw.capacityText = 'otherCapText';
            uaw.natRegNo = '123456';
            uaw.isMember = true;
            uaw.parentUWRForm = GardTestData.clientAcc.id;
            uae.parentUWRFormId = GardTestData.clientAcc.id;
            uae.saveAssureds();
            uae.rowIndexExt = uae.uAWs.size()-1;
            uae.deleteRow();
            UwrAssuredsExtension.getAllCapacitiesSO();
        }
    }
    
    @isTest private static void assuredsFromOffshoreVessels(){     
        test.StartTest();
        createDataForTest();
        test.StopTest();
        System.runAs(GardTestData.clientUser){
            UWRforOffshoreVesselCtrl offshoreVesselsClient = new UWRforOffshoreVesselCtrl();
            UwrAssuredsExtension uae = new UwrAssuredsExtension(offshoreVesselsClient);
            UwrAssuredsExtension.UwrAssuredsWrapper uaw = uae.uAWs.get(0);
            uaw.companyName = GardTestData.clientAcc.name;
            uaw.memberType = uae.allMemberTypesSO.get(0).getValue();
            uaw.country = uae.allCountriesSO.get(0).getValue();
            uaw.city = 'Gotham';
            uaw.capacity = uae.allCapacitiesSO.get(0).getValue();
            uaw.capacityText = 'otherCapText';
            uaw.natRegNo = '123456';
            uaw.isMember = true;
            uaw.parentUWRForm = GardTestData.clientAcc.id;
            uae.parentUWRFormId = GardTestData.clientAcc.id;
            uae.saveAssureds();
            uae.rowIndexExt = uae.uAWs.size()-1;
            uae.deleteRow();
            UwrAssuredsExtension.getAllCapacitiesSO();
        }
    }
    
    @isTest private static void assuredsFromComprehensive(){     
        test.StartTest();
        createDataForTest();
        test.StopTest();
        System.runAs(GardTestData.clientUser){
            UWRForEntryFormforCharterClientsCtrl comprehensiveClient = new UWRForEntryFormforCharterClientsCtrl();
            UwrAssuredsExtension uae = new UwrAssuredsExtension(comprehensiveClient);
            UwrAssuredsExtension.UwrAssuredsWrapper uaw = uae.uAWs.get(0);
            uaw.companyName = GardTestData.clientAcc.name;
            uaw.memberType = uae.allMemberTypesSO.get(0).getValue();
            uaw.country = uae.allCountriesSO.get(0).getValue();
            uaw.city = 'Gotham';
            uaw.capacity = uae.allCapacitiesSO.get(0).getValue();
            uaw.capacityText = 'otherCapText';
            uaw.natRegNo = '123456';
            uaw.isMember = true;
            uaw.parentUWRForm = GardTestData.clientAcc.id;
            uae.parentUWRFormId = GardTestData.clientAcc.id;
            uae.saveAssureds();
            uae.rowIndexExt = uae.uAWs.size()-1;
            uae.deleteRow();
            UwrAssuredsExtension.getAllCapacitiesSO();
        }
    }
    
    @isTest private static void assuredsFromEntryForm(){     
        test.StartTest();
        createDataForTest();
        test.StopTest();
        System.runAs(GardTestData.clientUser){
            UWREntryFormCtrl entryFormClient = new UWREntryFormCtrl();
            UwrAssuredsExtension uae = new UwrAssuredsExtension(entryFormClient);
            UwrAssuredsExtension.UwrAssuredsWrapper uaw = uae.uAWs.get(0);
            uaw.companyName = GardTestData.clientAcc.name;
            uaw.memberType = uae.allMemberTypesSO.get(0).getValue();
            uaw.country = uae.allCountriesSO.get(0).getValue();
            uaw.city = 'Gotham';
            uaw.capacity = uae.allCapacitiesSO.get(0).getValue();
            uaw.capacityText = 'otherCapText';
            uaw.natRegNo = '123456';
            uaw.isMember = true;
            uaw.parentUWRForm = GardTestData.clientAcc.id;
            uae.parentUWRFormId = GardTestData.clientAcc.id;
            uae.saveAssureds();
            uae.rowIndexExt = uae.uAWs.size()-1;
            uae.deleteRow();
            UwrAssuredsExtension.getAllCapacitiesSO();
        }
    }
    
    private static void createDataForTest(){
        List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
        ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        /*Country__c country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA');
        //insert country;
        Country_code__c con_code = new Country_code__c (display_value__c = 'IN - India');
        insert  con_code; 
        Port__c port1 = new Port__c(Name='Test', Country__c=country.id);
        insert port1;
        Nationality__c nationality = new Nationality__c(Description__c='Test');
        insert nationality;
        */
        GardTestData test_rec = new GardTestData();
        test_rec.commonrecord(); 
        test_rec.customsettings_rec();        
    }
    
}