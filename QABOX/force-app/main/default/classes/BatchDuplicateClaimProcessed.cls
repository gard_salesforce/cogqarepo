global class BatchDuplicateClaimProcessed implements Database.Batchable<sObject> {
    
    global String query;
    global Id claimRecordTypeId   = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Claim').getRecordTypeId();
    global Id MyGardclaimRecordTypeId   = Schema.SObjectType.Case.getRecordTypeInfosByName().get('MyGardClaim').getRecordTypeId();
    
    global BatchDuplicateClaimProcessed () {
        query=  'SELECT  MyGard_Claim_ID__c,Claims_handler_on_member__c,Reported_Claim__c  FROM Case where RecordTypeId = : claimRecordTypeId and Claim_Reference_Number__c <> NULL and (NOT  SFDC_Claim_Ref_ID__c like \'E%\') and MyGard_Claim_ID__c like\'E%\'  and parentid=null  and createdby.name=\'integration\''; 
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<case> scope){
        system.debug('scope'+scope.size());

        //list<string> mygardclaimid=new list<string>() ;
        //SF-1957
        Set<String> myGardClaimid=new Set<String>() ; 
        list<case> updateProcessedClaimlist=new list<case>();
        list<case> caselistToBeUpdated=new list<case>();
        Map<String,Case> mygardclaimIdMap  = new Map<String,Case> ();

        try {
            for (case s : scope) {
            system.debug('MyGard_Claim_ID__c '+s.MyGard_Claim_ID__c);
            //Start Issue SF-1957 
            mygardclaimid.add(s.MyGard_Claim_ID__c);
            caselistToBeUpdated.add(s);
            //mygardclaimIdMap.put(s.MyGard_Claim_ID__c,s.id);

            system.debug('*****mygardclaimid'+mygardclaimid);

            }
        }catch (exception ex){}
        //SF-1957
        updateProcessedClaimlist =[SELECT ID,Status,MyGard_Claim_ID__c,Claims_handler_on_member__c FROM CASE WHERE RecordTypeId = : MyGardclaimRecordTypeId and MyGard_Claim_ID__c in :mygardclaimid and Claim_Reference_Number__c =NULL and SFDC_Claim_Ref_ID__c like 'E%' and MyGard_Claim_ID__c like'E%'  and parentid=null  and createdby.name<>'integration'];
        system.debug('*****updateProcessedClaimlist'+updateProcessedClaimlist);
        
        for(Case ca : updateProcessedClaimlist) {
            if(ca.Status == 'Pending') {
                ca.Status = 'Processed';    
                mygardclaimIdMap.put(ca.MyGard_Claim_ID__c,ca);
            }
        }
        try {
            update updateProcessedClaimlist;
            System.debug('updateProcessedClaimlist::::'+updateProcessedClaimlist);
            System.debug('mygardclaimIdMap::::'+mygardclaimIdMap);
            if(!mygardclaimIdMap.Isempty()) {
                System.debug('mygardclaimIdMap::::'+mygardclaimIdMap);
                for(Case cas : caselistToBeUpdated) {
                    System.debug('cas.MyGard_Claim_ID__c::::'+cas.MyGard_Claim_ID__c);
                    System.debug('mygardclaimIdMap.get(cas.MyGard_Claim_ID__c)::::'+mygardclaimIdMap.get(cas.MyGard_Claim_ID__c));
                    if(mygardclaimIdMap.containskey(cas.MyGard_Claim_ID__c) && mygardclaimIdMap.get(cas.MyGard_Claim_ID__c) != null)
                    {
                        System.debug('mygardclaimIdMap::::'+mygardclaimIdMap);
                        cas.Reported_Claim__c = mygardclaimIdMap.get(cas.MyGard_Claim_ID__c).Id;
                        cas.Claims_handler_on_member__c = mygardclaimIdMap.get(cas.MyGard_Claim_ID__c).Claims_handler_on_member__c ;
                    }
                }
            }
            update caselistToBeUpdated;
        }
        catch (exception ex){}
        //SF-1957
        //delete delclmlst;
        // DataBase.emptyRecycleBin(dellst);
    }

    global void finish(Database.BatchableContext BC){
    }
}