@isTest
Public class CreateCustomerFeedbackCntrlTest {
Private static Event testdata = new Event();
    
    Public static void CreateTestDataBroker(){
         GardTestData test_rec = new GardTestData();
         test_rec.commonrecord();  
        
        testdata.OwnerId=UserInfo.getUserId();
        testdata.Subject='meeting';
        testdata.Location__c='Arendal';
        testdata.CurrencyIsoCode='USD';
        testdata.EndDateTime=System.today().addDays(1);
        testdata.StartDateTime=System.today();
        testdata.DurationInMinutes=1440;
        testdata.WhatId=GardTestData.brokerAcc.Id;
        Insert testdata;
        
        }
        
        Public static void CreateTestDataClient(){
         GardTestData test_rec = new GardTestData();
         test_rec.commonrecord();  
        
        testdata.OwnerId=UserInfo.getUserId();
        testdata.Subject='meeting';
        testdata.Location__c='Arendal';
        testdata.CurrencyIsoCode='USD';
        testdata.EndDateTime=System.today().addDays(1);
        testdata.StartDateTime=System.today();
        testdata.DurationInMinutes=1440;
        testdata.WhatId=GardTestData.clientAcc.Id;
        Insert testdata;
        
        }
   
       @isTest public static void testMethod1(){
        CreateTestDataBroker();
        CreatCustomerFeedbackCntrl.getEventRecord(testdata.Id);
    }  
       
       @isTest public static void testMethod2(){
        CreateTestDataClient();
        CreatCustomerFeedbackCntrl.getEventRecord(testdata.Id);
    }      
 }