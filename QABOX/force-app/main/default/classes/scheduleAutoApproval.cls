global class scheduleAutoApproval implements Schedulable {
    global void execute (SchedulableContext SC) {
        String strQuery = 'SELECT Id, Name '
                         +'FROM Opportunity '
                         +'WHERE StageName = \'Under Approval\' AND RecordType.Name = \'Marine\' '
                         +'AND (Business_Type__c = \'Ship Owners\''
                         +'AND Approval_criteria__c = \'Silent Approval\' )'
                         +'AND Approval_Duration__c > 0';
        BatchAutoApprove app = new BatchAutoApprove();
        app.query = strQuery;
        Database.executeBatch(app, 100);
    }
    
    //### TEST METHODS ###
    public static TestMethod void testSchedAutoApprove() {
        
        // Just need to test scheduling here as actual processing tested in BatchAutoApproval class
        test.StartTest();
        
        // schedule a new job
        String jobId = System.schedule('testSchedAutoApprove',
            '0 0 0 3 9 ? 2022', 
            new scheduleAutoApproval());
        
        // get the newly created trigger record
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, 
            NextFireTime
            FROM CronTrigger WHERE id = :jobId];
        
        // assert that the values in the job are correct
        System.assertEquals('0 0 0 3 9 ? 2022', ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
        
        test.StopTest();
    }
}