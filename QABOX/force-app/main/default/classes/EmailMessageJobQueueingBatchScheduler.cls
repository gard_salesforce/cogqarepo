//Test Class : TestEmailMessageReparenting
global class EmailMessageJobQueueingBatchScheduler implements Database.Batchable<SObject>,Database.Stateful{

    //instance
    //--stateful
    global String jobName;
    global  Id parentCaseId;
    static List<String> debugStrings = new List<String>();
    //static
    private static String caseFields;
    private static String caseQuery;
    private static final List<Integer> delays = new List<Integer>{1,2,2,5,10,10,10,10,10};
    private static final String delimiter = EmailMessageReparentingJobs.delimiter;
    //--initialization
    static{
        caseFields = 'id,Team__c,ownerId,status';
        caseQuery = 'SELECT '+caseFields+' FROM Case WHERE Id = :parentCaseId';
    }
    
    //BATCH INITIATOR 
    public static void startDelayedQueue(Id parentCaseId,String fromAddress,String messageDate){
        EmailMessageJobQueueingBatchScheduler.startDelayedQueue(parentCaseId,fromAddress,messageDate,1);
    }
    
    public static void startDelayedQueue(Id parentCaseId,String fromAddress,String messageDate,Integer trialNumber){
        if(trialNumber > delays.size()) return;
        List<String> debugStrings = new List<String>();
        Boolean jobAlreadyQueued = false;
        
        String jobName = parentCaseId+delimiter+fromAddress+delimiter+messageDate+delimiter+trialNumber;
        //recordId##franky@example.com##2021-05-26T09:42:00##1
        debugStrings.add('startDelayedQueue() LOGS : ');
        debugStrings.add('jobName() - '+jobName);
        
        for(cronTrigger ct : Database.query('SELECT Id, OwnerId,CronJobDetail.name FROM CronTrigger')){
            if(ct.CronJobDetail.name.equalsIgnoreCase(jobName)){
                jobAlreadyQueued = true;
            }
        }
        
        Integer childEmCount = [SELECT count() FROM EmailMessage WHERE ParentId = :parentCaseId];
        Boolean isMergeOperation = [SELECT status FROM Case WHERE Id = :parentCaseId].status.equalsIgnoreCase('Closed as Duplicate');
        debugStrings.add('JobEnqueuing - '+((jobAlreadyQueued) ? 'Job Already Scheduled' : ((childEmCount != 1) ? 'Scheduling Job Now' : 'Recursive Job Call' )));
        try{
            if(!isMergeOperation && childEmCount != 1 && !jobAlreadyQueued){
                Integer delay = delays[trialNumber-1];
                if(childEmCount > 1) System.scheduleBatch(new EmailMessageJobQueueingBatchScheduler(jobName),jobName,delay);//later, see if we can prevent Reader/Writer using jobName as similarity condition
            }
        }catch(Exception ex){
            debugStrings.add('\ngetStackTraceString()'+ex.getStackTraceString());
            debugStrings.add('\ngetMessage() - '+ex.getMessage());
            debugStrings.add('\ngetTypeName() - '+ex.getTypeName());	
        }
        
        debugStrings.add('startDelayedQueue() LOGS END');
        //final logging code
        debugStrings.add('Database.Batchable execute() LOGS END');
        String debugDescription = String.join(debugStrings,'. \n');
        Logger__c logger = new Logger__c();
        logger.Log_Description__c = debugDescription;
        insert logger;
    }
    
    //BATCHABLE CONSTRUCTORS
    @TestVisible private EmailMessageJobQueueingBatchScheduler(String jobName){//Batch initiation
        this.jobName = jobName;
    }
    
    //BATCHABLE METHODS
    global Database.QueryLocator start(Database.BatchableContext bCon){
        Id parentCaseId = Id.valueOf(jobName.substring(0,jobName.indexOf(delimiter)));
        return Database.getQueryLocator(caseQuery);//redundant query, can't pass parent case to QUEUEABLE Jobs
    }
    
    global void execute(Database.BatchableContext bCon,List<Case> parentCases){
        
        try{
            if(!parentCases.isEmpty()){
                 System.enqueueJob(new EmailMessageReparentingJobs(jobName));
            }else{
                debugStrings.add('if Not Entered : ParentCases Not Found');
            }
        }catch(Exception ex){
            debugStrings.add('\ngetStackTraceString()'+ex.getStackTraceString());
            debugStrings.add('\ngetMessage() - '+ex.getMessage());
            debugStrings.add('\ngetTypeName() - '+ex.getTypeName());	
        }        
        //final logging code
        debugStrings.add('Database.Batchable execute() LOGS END');
        String debugDescription = String.join(debugStrings,'. \n');
        Logger__c logger = new Logger__c();
        logger.Log_Description__c = debugDescription;
        insert logger;
    }
    
    global void finish(Database.BatchableContext bCon){}
    
}