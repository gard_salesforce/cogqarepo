@isTest
private class TestMCSendAPIEmailCtrl {
    
    @isTest 
    static void testCallout() {

         //create test data-start
         //1. Creating 2 users
         String orgId = userInfo.getOrganizationId();
         String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
         Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
         String uniqueName = orgId + dateString + randomInt;
         String userName1 = uniqueName + '@test' + orgId + '.org';
         Profile prof = [Select Id, Name from Profile where Name='System Administrator'];
         List<User> tuser = new List<User>();
         tuser.add(new User(  firstname = 'tMasterUserFname',
                               lastName = 'tMasterUserLName',
                               email = uniqueName + '@test' + orgId + '.org',
                               Username = uniqueName + '@test' + orgId + '.org',
                               EmailEncodingKey = 'ISO-8859-1',
                               Alias = uniqueName.substring(18, 23),
                               TimeZoneSidKey = 'America/Los_Angeles',
                               LocaleSidKey = 'en_US',
                               LanguageLocaleKey = 'en_US',
                               ProfileId = prof.Id
                               ));
         Integer randomInt2 = Integer.valueOf(math.rint(math.random()*1000000));
         String uniqueName2 = orgId + dateString + randomInt2;
         String userName2 = uniqueName2 + '@test' + orgId + '.org';
         tuser.add(new User(  firstname = 'tChildUserFname',
                               lastName = 'tChildUserLName',
                               email = uniqueName2 + '@test' + orgId + '.org',
                               Username = uniqueName2 + '@test' + orgId + '.org',
                               EmailEncodingKey = 'ISO-8859-1',
                               Alias = uniqueName.substring(18, 23),
                               TimeZoneSidKey = 'America/Los_Angeles',
                               LocaleSidKey = 'en_US',
                               LanguageLocaleKey = 'en_US',
                               ProfileId = prof.Id
                               ));
           insert tuser;
         
         //2. creating a test event
         fluidoconnect__Survey__c newEvent = new fluidoconnect__Survey__c();
         newEvent.Event_Start_Date__c =  system.today();
         newEvent.fluidoconnect__Event_Type__c = 'Survey';
         newEvent.fluidoconnect__Status__c = 'Open';
         newEvent.fluidoconnect__Survey_Title_Local__c = 'TestClassEvent';
         List<User> lstUser1 = [Select Id, Name from User where Username=:userName1];
         newEvent.fluidoconnect__Event_Organiser__c = lstUser1[0].Id;
         insert newEvent;
         
         //3. creating custom setting data
         MarketingCloudAPICall__c cs = new MarketingCloudAPICall__c();
         cs.AccessToken__c = '';
         //cs.Cancellation_TSDExternalKey__c = '12346';
         cs.ClientId__c = 'b0v57dyunzfi599teocb2vso';
         cs.ClientSecret__c = 'c64fpZUyFKxiRWtRZtpcSoak';
         //cs.Confirmation_FromAddress__c = 'code@exacttarget.com';
         //cs.Confirmation_fromName__c = 'Rahul Gupta';
         cs.Confirmation_TSDExternalKey__c ='12345';
         cs.EndPoint_SendEmail__c = 'https://www.exacttargetapis.com/messaging/v1/messageDefinitionSends/';
         cs.EndPoint__c = 'https://auth-test.exacttargetapis.com/v1/requestToken';
         cs.Name = 'APICallDetail';
         insert cs;
        
       
        //4. creating a test event member        
        fluidoconnect__Invitation__c newEventMember = new fluidoconnect__Invitation__c();
        List<fluidoconnect__Survey__c> lstEvent = [Select Id, Name from fluidoconnect__Survey__c where fluidoconnect__Survey_Title_Local__c = 'TestClassEvent'];
        newEventMember.fluidoconnect__Survey__c = lstEvent[0].Id;
        List<User> lstUser2 = [Select Id, Name from User where Username=:userName2];
        newEventMember.fluidoconnect__User__c = lstUser2[0].Id;
        newEventMember.fluidoconnect__Status__c = 'Attending';        
        insert newEventMember;
         // create test data for AdminUsers__c 
        AdminUsers__c adminCs = new AdminUsers__c();
        adminCs.name = 'Number of users';
        adminCs.value__c = 3;
        insert adminCs;
        //Test Start
        Test.startTest(); 
        List<MarketingCloudAPICall__c> lstVal = [SELECT Id, ClientSecret__c,EndPoint_SendEmail__c, EndPoint__c FROM MarketingCloudAPICall__c WHERE Name = 'APICallDetail'];
        Test.setMock(HttpCalloutMock.class, new MockHttlResponseMCAPICall());
        
       
        //Request 1
        Http h = new Http();        
        HttpRequest req1 = new HttpRequest();
        req1.setEndpoint(lstVal[0].EndPoint__c);
        req1.setMethod('POST');
        req1.setBody('"clientId": "YOUR_CLIENT_ID_FROM_APP_CENTER","clientSecret": "YOUR_CLIENT_SECRET_FROM_APP_CENTER"}');
        HttpResponse res1 = h.send(req1);  
        
        //Request 2
        Http h2 = new Http();        
        HttpRequest req2 = new HttpRequest();
        req2.setEndpoint(lstVal[0].EndPoint_SendEmail__c);
        req2.setMethod('POST');
        req2.setBody('{"To": {"Address": "a@c.com","SubscriberKey": "a@c.com"},"ContactAttributes": {"SubscriberAttributes": {"fromAddress": "b@a.com", "fromName": "test","Title": "testTitle", "LinkText": "testLinkText", "Organiser": "testName", "date": "testDate", "EventType": "testType", "CalenderLink": "calenderUrl"}}},"OPTIONS": {"RequestType": "SYNC"}}');
        HttpResponse res2 = h2.send(req2);  
        MCSendAPIEmail mc = new MCSendAPIEmail();
        MCSendAPIEmail.sendEmail('a@b.com','123','testtitle','testlink', 'testName','test@test.com','test', 'test', 'test', 'testurl');
        Test.stopTest();
        
        //Test Stop
        
        //to cover constructor 
       
        fluidoconnect__Invitation__c delNewEventMem = [SELECT Id from fluidoconnect__Invitation__c Where Id =: newEventMember.ID];  
        delete delNewEventMem;
    }
}