/***
SeeAllData equal true is required for chatter connect api calls used in FollowService class
Prerequisite:-chatter free user with username[test.follower.chatter] will be present.
userName[test.follower.chatter] has been created in UAT/Prod.
**/

@isTest(SeeAllData = true)
public class TestFollowerService{
    private static TestData testDataobj;
    private static void createTestData(){
        testDataobj = new TestData();
    }
    
    @isTest private static void testFollowerService(){
        createTestData();
        FollowerService.getPageWrappers(testDataobj.acct.id,0);
        FollowerService.followUnFollowUserServer(testDataobj.followingUsers[0].id+'','follow');
        FollowerService.followUnFollowUserServer(testDataobj.followingUsers[0].id+'','unfollow');
        
        /*//Before the SF-4923 changes
        FollowerService.setfollowToUser(testDataobj.acct.Id);
        FollowerService.getFollowerWrappers((testDataobj.acct.Id+'').substring(0,15));            
        FollowerService.setunfollow(testDataobj.acct.Id);
		*/
    }
    class TestData{
        list<User> followingUsers;
        Account acct;
        TestData(){
            followingUsers = [Select Id,Alias ,profileId ,Email ,EmailEncodingKey ,CommunityNickname,LastName ,LanguageLocaleKey ,LocaleSidKey ,TimeZoneSidKey ,UserName,contactId__c,City from User where UserName like '%test.follower.chatter%' limit 1 ];        
            acct = [select id from account where company_status__c='Active' limit 1];
            list<EntitySubscription> ess = [Select id from EntitySubscription where subscriberId=:followingUsers[0].Id and parentid=:acct.Id];
            if(ess.size()>0){
                delete ess;
            }
            EntitySubscription es = new EntitySubscription(
                subscriberId=followingUsers[0].Id , parentid=acct.Id
            );
            insert es;
        }
    }	
}