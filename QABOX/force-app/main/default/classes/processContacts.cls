public with sharing class processContacts {
    /************************************************************************
    * Date: 28/1/2014
    * Author: Matt Morgan
    * Company: cDecisions Ltd.
    * Description: Contact processing methods, located here so they can be called from a batch class or a trigger
    * Modifications: [ENTER MODS HERE]
    ************************************************************************/

    public void syncConAccAddress(List<Contact> contacts) {
        // Build a list of Ids so we can requery, saving having to include all fields in the call to the batch job
        Set<Id> conIds = new Set<Id>();
        Set<Id> accIds = new Set<Id>();
        for (Contact c: contacts) {
            conIds.add(c.Id);
            accIds.add(c.AccountId);
        }
        Map<Id,Account> mapAccs = new Map<Id,Account>([SELECT Id, Name, BillingStreet, BillingCity, BillingPostalCode, BillingState, BillingCountry FROM Account WHERE Id in :accIds]);
        // Retrieve all relevant contacts, with all fields we need to read/update
        List<Contact> toUpdate = new List<Contact>();
        List<Contact> lstCon = [SELECT Id, MailingStreet, MailingCity, MailingPostalCode, MailingState, MailingCountry,
                                AccountId, Account.BillingStreet, Account.BillingCity, Account.BillingPostalCode, Account.BillingState, Account.BillingCountry
                                FROM Contact WHERE Id IN :conIds];
        for(Contact con : contacts) {
            boolean updated = false;
            if(mapAccs.get(con.AccountId)!=null) {
                if(con.MailingStreet     !=((Account)mapAccs.get(con.AccountId)).BillingStreet)     {con.MailingStreet     =((Account)mapAccs.get(con.AccountId)).BillingStreet;     updated=true;}
                if(con.MailingCity       !=((Account)mapAccs.get(con.AccountId)).BillingCity)       {con.MailingCity       =((Account)mapAccs.get(con.AccountId)).BillingCity;       updated=true;}
                if(con.MailingPostalCode !=((Account)mapAccs.get(con.AccountId)).BillingPostalCode) {con.MailingPostalCode =((Account)mapAccs.get(con.AccountId)).BillingPostalCode; updated=true;}
                if(con.MailingState      !=((Account)mapAccs.get(con.AccountId)).BillingState)      {con.MailingState      =((Account)mapAccs.get(con.AccountId)).BillingState;      updated=true;}
                if(con.MailingCountry    !=((Account)mapAccs.get(con.AccountId)).BillingCountry)    {con.MailingCountry    =((Account)mapAccs.get(con.AccountId)).BillingCountry;    updated=true;}
            }
        }
    }
    
}