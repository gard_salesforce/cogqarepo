global class BatchUpdateGardContacts implements Database.Batchable<sObject>
{
    global BatchUpdateGardContacts(){
              // Batch Constructor
     }
   
     // Start Method
     global Database.QueryLocator start(Database.BatchableContext BC)
     {
        //Date todayDt = System.Today();
        String query = 'select id, email,contactid__c from User WHERE isActive = true';// where LastModifiedDate =:todayDt';
        system.debug('scheduler_class : '+query);
        return Database.getQueryLocator(query);
     }
   
     // Execute Logic
     global void execute(Database.BatchableContext BC, List<User> scope)
     {
         set<id> userId = new set<Id>();
         for(User users:scope)
         {
             userId.add(users.contactid__c);
         }
         map<id,Gard_contacts__c> mapGC = new map<Id, Gard_Contacts__c>([select id, email__c from Gard_Contacts__c where id =: userId]);
         
         for(User users:scope)
         {
             if(mapGC.containsKey(users.contactid__c))
             {
                 mapGC.get(users.contactid__c).email__c = users.email;
             }
         }
         update mapGC.values();
     }
   
     global void finish(Database.BatchableContext BC)
     {
   
     }
   
 }