@isTest(seeAllData=true)

public class TestGardNoDm {
    private static testMethod void TestgetDocument_element(){
        new gardNoDm.getDocument_element();
    }
    private static testMethod void TestgetDocument_element1(){
        new gardNoDm_v1.getDocument_element();
    }
    private static testMethod void TestgetDocumentResponse_element(){
        new gardNoDm.getDocumentResponse_element();
    }
    private static testMethod void TestgetDocumentResponse_element1(){
        new gardNoDm_v1.getDocumentResponse_element();
    }
    private static testMethod void TestgetDocumentByGardUserId_element(){
        new gardNoDm.getDocumentByGardUserId_element();
    }
    private static testMethod void TestgetDocumentFault_element(){
        new gardNoDm.getDocumentFault_element();
    }
    private static testMethod void TestgetDocumentFault_element1(){
        new gardNoDm_v1.getDocumentFault_element();
    }
    private static testMethod void TestgetDocumentWithExternalGroupResponse_element(){
        new gardNoDm.getDocumentWithExternalGroupResponse_element();
    }
    private static testMethod void TestgetDocumentWithExternalGroupResponse_element1(){
        new gardNoDm_v1.getDocumentWithExternalGroupResponse_element();
    }
    private static testMethod void TestgetDocumentByGardUserIdResponse_element(){
        new gardNoDm.getDocumentByGardUserIdResponse_element();
    }
    private static testMethod void TestgetDocumentWithExternalGroup_element(){
        new gardNoDm.getDocumentWithExternalGroup_element();
    }
    private static testMethod void TestgetDocumentWithExternalGroup_element1(){
        new gardNoDm_v1.getDocumentWithExternalGroup_element();
    }
    private static testMethod void TestGardDocumentServicePort(){
        //new gardNoDm.GardDocumentServicePort();
    }
    private static testMethod void TestGardDocumentServicePort1(){
        new gardNoDm_v1.GardDocumentServicePort();
    }
    private static testMethod void TestgetDocument(){
        Attachment att = new Attachment();
        Claims_picklist_value__c obj = new Claims_picklist_value__c();
        insert obj;
        att.Name='Unit Test Attachment';
        att.Body=Blob.valueOf('Unit Test Attachment Body');
        att.parentId = obj.id;
        insert att;
        new gardNoDm.GardDocumentServicePort().getDocument(userInfo.getUserId(),att.id);
    }
    private static testMethod void TestgetDocument1(){
        
        Attachment att = new Attachment();
        Claims_picklist_value__c obj = new Claims_picklist_value__c();
        insert obj;
        att.Name='Unit Test Attachment';
        att.Body=Blob.valueOf('Unit Test Attachment Body');
        att.parentId = obj.id;
        insert att;
        System.assertEquals('Unit Test Attachment',att.Name);
        new gardNoDm_v1.GardDocumentServicePort().getDocument(userInfo.getUserId(),att.id);
    }
    private static testMethod void TestgetDocumentByGardUserId(){
        //new gardNoDm.GardDocumentServicePort().getDocumentByGardUserId(null,null);
    }
    private static testMethod void TestgetDocumentWithExternalGroup(){
        //new gardNoDm.GardDocumentServicePort().getDocumentWithExternalGroup(null,null,null);
    }
    private static testMethod void TestgetDocumentWithExternalGroup1(){
        new gardNoDm_v1.GardDocumentServicePort().getDocumentWithExternalGroup(null,null,null);
    }
    
}