@isTest private class TestCaseLastActivity {
    private static Case referenceCase;
    private static final Id ctRecTypeId;
    static{
        
        ctRecTypeId = [SELECT id,name FROM RecordType WHERE Name LIKE '%customer transaction%'].id;
    }
    
    private static void createData(){
        
        
        referenceCase = new Case();
        referenceCase.recordTypeId = ctRecTypeId;
        insert referenceCase;
    }
    
    @isTest private static void testFeedItemTrigger(){
        createData();
        FeedItem newFeedItem = new FeedItem();
        newFeedItem.ParentId = referenceCase.Id;
        newFeedItem.type ='TextPost';
        newFeedItem.body ='Test Post for FeedItem Trigger';
        insert newFeedItem;
        
    }
    
    @isTest private static void testCaseCommentTrigger(){
        createData();
        CaseComment newCaseComment = new CaseComment();
        newCaseComment.CommentBody= 'Test comment for casecomment trigger';
        newCaseComment.ParentId = referenceCase.Id;
        insert newCaseComment;
    }
    
    @isTest private static void testEmailMessageTrigger(){
        createData();
        EmailMessage emailToCase = new EmailMessage();
        emailToCase.Subject = 'test';
        emailToCase.Incoming = true;
        emailToCase.TextBody = 'test body';
        emailToCase.ParentId = referenceCase.Id;
        insert emailToCase;
        
    }
    
}