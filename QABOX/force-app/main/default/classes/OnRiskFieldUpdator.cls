//Created By Pulkit to update Related Company On Risk Field in production for data Integrity - SF-3966
//Sets the Child_Company_On_risk__c field on a Company when either direct Child or Parent Company is Set,
//Otherwise, if both Child and Parent Company's Fields are Reset, resets "this" company's Child_Comapny_Field
//the 1.1 method works for all Accounts. while 1.2 works for particular heirarchies, of the companies whose Ids as passed in the Parameters
//Database.executeBatch(new OnRiskFieldUpdator(), 10); //To run batch
//OnRiskFieldUpdator.reportUpdate(Boolean))); // True - find and correct Accounts if incorrect exists; False - Find and report Incorrect Account Ids; null - find and report no. of incorrect Accounts
//AccountHierarchyTraversal.showAccountHierarchies(new List<String>{'001D000000fxRDf'}); //to create the whole heirarchy of the companies
global class OnRiskFieldUpdator implements Database.Batchable<SObject>{
    
    //exposed variables
    public static List<String> queryFieldsList = new List<String>{'Id','Name','ParentId','Client_On_Risk__c','Child_Company_On_Risk__c','Company_Role__c'};
    
    
    //static variables & Blocks
    //Common To Script and Node Mode
    static List<Account> accountsToBeUpdated = new List<Account>();
    static String queryFieldsString; //needs to be set before
    static String queryString;
    static List<String> failedAccountIds; //Ids if any accounts are not correctly updated
    static{
        queryFieldsString = String.join(queryFieldsList,',');
        queryString = 'SELECT '+queryFieldsString+' FROM Account WHERE Company_Role_Text__c LIKE \'Client\'';//FOR TEST, remove LIMIT 10
    }
    
    /*
    //Node Mode Variables
    static Map<String,Node> treeNodes;
    static List<Node> rootNodes;
    static Map<String,List<Node>> missingParentNodes;//for batchable
    static String parentCondition = ' AND Id IN :parentAccountIds';
    static String rootCondition = ' AND ParentId = null';
    static String childCondition = ' AND ParentId IN :thisLevelAccountIds';
   	static Integer nodesCreatedCount = 0;//number of nodes created
    */
    
    //Script Mode Variables
    static Map<String,Account> these = new Map<String,Account>();//all accounts that will be processed in this batch
    LisT<Account> relatedAccounts = new List<Account>();//Children and Parents of "these", etrived by querying
    static Map<String,List<Account>> parentToChildren = new Map<String,List<Account>>();//parent To children Link
    static Map<String,Account> childToParent = new Map<String,Account>();//child to parent Link
    static Map<String,List<Account>> missingParent = new Map<String,List<Account>>();//when children come before their parent
    static List<String> parentIds = new List<String>();//parents' ids to be retrieved from database
    static List<String> theseIds = new List<String>();//ids to retrieve children
    static Boolean theseCreated = false;//to prevent children & parents from being processed as "these"
    
    //Methods for "Batchable"
    
    //Driver method
    //IN USE
    //Retrieves all records and If reportMode - 
    //True - runs the batch to update the Accounts with incorrect values
    //False - reports the ids of Accounts with inCorrect values
    //Null - reports only the no. of inCorrect values
    public static void reportUpdate(Boolean reportMode){
        //List<String> parentAccountIds = new List<String>();
        //List<String> thisLevelAccountIds = new List<String>();
        Integer recordsRetrievedCount = 0;
        //setQueryString();
        for(Account thisAccount : Database.query(queryString)){
            these.put(thisAccount.id,thisAccount);
        }
        createLinks(these.values());
        
        findDiscrepencies();
        
        if(failedAccountIds != null){
            System.debug(failedAccountIds.size()+' records found with InCorrect values out of '+these.size());
            if(reportMode != null){
                if(!reportMode){
                    Integer indexNumber = 0;
                    System.debug('Failed Account Ids:-');
                    for(String id : failedAccountIds){
                        indexNumber++;
                        System.debug(indexNumber+'.) '+id);
                    }
                }else if(reportMode){
                    //setRecordsRight(failedAccounts);
            		System.debug('Attempting to set the failed records right');
                    if(Test.isRunningTest()){
                        Database.executeBatch(new OnRiskFieldUpdator(),20);
                    }else{
                    	Database.executeBatch(new OnRiskFieldUpdator(),10);
                    }
                }
            }
        }else{
            System.debug('All records are correctly updated');
        }
    }
    
    global  Database.QueryLocator start(Database.BatchableContext bc){
        //setQueryString();
        String finalQueryString = queryString;
        if(failedAccountIds != null && failedAccountIds.size()>0){ //to be run when called from reportUpdate(true)
            finalQueryString+=' AND ID IN: failedAccountIds'; 
        }
        return Database.getQueryLocator(finalQueryString);
     }
    
    
    //IN USE
    //For Script Mode
    global void execute(Database.BatchableContext bc,List<SObject> theseSObjects){
        
        createLinks(theseSObjects);
        
        for(String parentIdString : missingParent.keySet()){
            parentIds.add(parentIdString);
        }
        
        for(Account relatedAccount : Database.query(queryString+' AND  (id IN :parentIds OR parentId IN :theseIds) AND id NOT IN :theseIds')){
        	relatedAccounts.add(relatedAccount);
        }
        
        createLinks(relatedAccounts);
        processAccounts();
        //processAccountsInverse(); //CAUTION this sets the INCORRECT values, TO BE USED FOR TESTING ONLY
        updateDatabase();
    }
    
    /*
    //Driver method
    //IN USE (NODE MODE)
    //Retrieves all records and If reportMode - 
    //True - runs the batch to update the Accounts with incorrect values
    //False - reports the ids of Accounts with inCorrect values
    //Null - reports only the no. of inCorrect values
    public static void reportUpdate(Boolean reportMode){
        List<String> parentAccountIds = new List<String>();
        List<String> thisLevelAccountIds = new List<String>();
        List<Node> theseNodes = new List<Node>();
        Integer recordsRetrievedCount = 0;
        //setQueryString();
        for(Account thisAccount : Database.query(queryString)){
            recordsRetrievedCount++;
            thislevelAccountIds.add(thisAccount.id);
            Node thisNode = new Node(thisAccount);
            if(thisAccount.parentId != null){
                parentAccountIds.add(thisAccount.parentId);
            }
            theseNodes.add(thisNode);
        }
        for(Node thisNode : theseNodes){
            thisNode.findDiscrepencies();
        }
        if(failedAccountIds != null){
            System.debug('Records update failed, no. of records failed - '+failedAccountIds.size()+' out of '+recordsRetrievedCount);
            if(reportMode != null){
                if(!reportMode){
                    Integer indexNumber = 0;
                    System.debug('Failed Account Ids:-');
                    for(String id : failedAccountIds){
                        indexNumber++;
                        System.debug(indexNumber+'.) '+id);
                    }
                }else if(reportMode){
                    //setRecordsRight(failedAccounts);
            		System.debug('Attempting to set the failed records right');
                    if(Test.isRunningTest()){
                        Database.executeBatch(new OnRiskFieldUpdator(),20);
                    }else{
                    	Database.executeBatch(new OnRiskFieldUpdator(),10);
                    }
                }
            }
        }else{
            System.debug('All records are correctly updated');
        }
    }
    */
    
       
    /* Commented as a precaution, ONLY TO BE USED WHILE TESTING
    //to process each of theseAccounts AND SET InCorrectValues
    void processAccountsInverse(){
        for(Account thisAccount : these.values()){
            Boolean thisrelatedOnRisk = thisAccount.Child_Company_On_Risk__c;
            Boolean parentOnRisk = false;
            Boolean anyChildOnRisk = false;
            if(childToParent.containsKey(thisAccount.id)){
                Account parentAccount = childToParent.get(thisAccount.id);
                parentOnRisk = parentAccount.Client_On_Risk__c;
            }
            if(parentToChildren.containsKey(thisAccount.id)){
                for(Account childAccount : parentToChildren.get(thisAccount.id)){
                    if(childAccount.Client_On_Risk__c){
                        anyChildOnRisk = true;
                        break;
                    }
                }
            }
            
            //process thisAccount
            if((thisrelatedOnRisk) && (parentOnRisk || anyChildOnRisk)){//if correct set incorrect
                thisAccount.Child_Company_On_Risk__c = false;
                accountsToBeUpdated.add(thisAccount);
            }else if((!thisrelatedOnRisk) && ((!parentOnRisk) && (!anyChildOnRisk))){//if correct set incorrect
                thisAccount.Child_Company_On_Risk__c = true;
                accountsToBeUpdated.add(thisAccount);
            }
        }
    }
    */
    
    //IN USE
    //For Script Mode
    //method to create Linkages
    static void createLinks(List<SObject> theseSObjects){
        for(SObject thisSObject : theseSObjects){
            Account thisAccount = (Account) thisSObject;
            String thisAccountId = thisAccount.id;
            String parentId = thisAccount.parentId;
            
            if(!theseCreated){
                these.put(thisAccount.id,thisAccount);
                theseIds.add(thisAccount.id);
            }
            if(parentId != null){
                if(these.containsKey(parentId)){//its parent already exists
                    List<Account> children;
                    Account parentAccount = these.get(parentId);
                    if(parentToChildren.containsKey(parentId)){//some other child of the parent already came, add thisAccount to its children list
                        children = parentToChildren.get(parentId);
                        children.add(thisAccount);
                        parentToChildren.put(parentId,children);
                    }else{//thisAccount is the first child of its parent
                        children = new List<Account>{thisAccount};
                        parentToChildren.put(parentId,children);
                    }
                    childToParent.put(thisAccount.id,parentAccount);
                }else{//if parent doesn't exist yet
                    List<Account> orphanAccounts;
                    if(missingParent.containsKey(parentId)){//there are other children of thisAccounts parent
                        orphanAccounts = missingParent.get(parentId);
                        orphanAccounts.add(thisAccount);
                        missingParent.put(parentId,orphanAccounts);
                    }else{//thisAccount is the first child retreived of its parent
                        orphanAccounts = new List<Account>{thisAccount};
                        missingParent.put(parentId,orphanAccounts);
                    }
                }
            }else{//if thisAccount doesn't have a parent 
            }
            
            if(missingParent.containsKey(thisAccount.id)){//this is the parent of accounts that have appeared before it
                List<Account> orphanAccounts = missingParent.get(thisAccount.id);
                parentToChildren.put(thisAccount.id,orphanAccounts);
                for(Account orphan : orphanAccounts){//child to parent link
                    childToParent.put(orphan.id,thisAccount);
                }
                missingParent.remove(thisAccount.id);
                if(orphanAccounts.size() > 0){
                    //do nothing this is to check if an exception occurs
                }
            }
        }
        theseCreated = true;
    }
    
    //IN USE
    //For Script Mode
    //to process each of theseAccounts
    void processAccounts(){
        for(Account thisAccount : these.values()){
            Boolean thisrelatedOnRisk = thisAccount.Child_Company_On_Risk__c;
            Boolean parentOnRisk = false;
            Boolean anyChildOnRisk = false;
            if(childToParent.containsKey(thisAccount.id)){
                Account parentAccount = childToParent.get(thisAccount.id);
                parentOnRisk = parentAccount.Client_On_Risk__c;
            }
            if(parentToChildren.containsKey(thisAccount.id)){
                for(Account childAccount : parentToChildren.get(thisAccount.id)){
                    if(childAccount.Client_On_Risk__c){
                        anyChildOnRisk = true;
                        break;
                    }
                }
            }
            
            //process thisAccount
            if((!thisrelatedOnRisk) && (parentOnRisk || anyChildOnRisk)){
                thisAccount.Child_Company_On_Risk__c = true;
                accountsToBeUpdated.add(thisAccount);
            }else if((thisrelatedOnRisk) && ((!parentOnRisk) && (!anyChildOnRisk))){
                thisAccount.Child_Company_On_Risk__c = false;
                accountsToBeUpdated.add(thisAccount);
            }
        }
    }
    
    
    /*Temporarily Commented to create script Method
	//IN USE
	global void execute(Database.BatchableContext bc,List<SObject> thisLevelsObjects){
        accountsToBeUpdated = new List<Account>();
        List<Node> theseNodes = new List<Node>();
        List<String> parentAccountIds = new List<String>();
        List<String> thisLevelAccountIds = new List<String>();
        //setQueryString();
        String queryForParents = queryString+parentCondition;
        String queryForChildren = queryString+childCondition;
           
        //create this level Nodes
        for(sObject thisLevelSObject : thisLevelsObjects){
            Account thisLevelAccount = (Account) thisLevelSObject;
            Node thisNode = new Node(thisLevelAccount);
            theseNodes.add(thisNode);
            thisLevelAccountIds.add(String.valueOf(thisLevelAccount.id));
            if(thisLevelAccount.parentId  != null){
                parentAccountIds.add(String.valueOf(thisLevelAccount.parentId));
            }
        }
        
        //retrieve parents and create Nodes
        if(parentAccountIds.size()>0){
            for(Account parentAccount : Database.query(queryForParents)){
                new Node(parentAccount);
            }
        }
        
        //retrieve children and create Nodes
        if(thisLevelAccountIds.size()>0){
            for(Account childAccount : Database.query(queryForChildren)){
                new Node(childAccount);
            }
        }
        
        //process Hierarchy Tree/s
        for(Node thisNode : theseNodes){
            thisNode.processNode();
            //thisNode.processNodeInverse(); // FOR TESTING ONLY :- to be used for setting incorrect values
        }
        
        //Update Database and report Result
        updateDatabase();
    }
    */
    
    //IN USE
    //For Script Mode AND Node Mode
    global void finish(Database.BatchableContext bc){
        System.debug('Accounts Update Complete!! You may run:-\n1.) OnRiskFieldUpdator.reportUpdate(false); to know if any Accounts have Incorrect values.\n2.)OnRiskFieldUpdator.reportUpdate(true); to report and also correct if any incorrect records are found(That may take a while for batch execution)');
    }
    //Batchable Methods ENDS
    
    /*
    //NOT IN USE
    //1.1
    //driver method
    public static void updateRelatedOnRisk(){
        if(queryFieldsList == null || queryFieldsList.size() == 0){
            System.debug('Update Debug OnRiskUpdator Error : queryFieldsList is Empty, Please provide fields values for it.');
            return;
        }
        
        treeNodes = new Map<String,Node>();
        accountsToBeUpdated = new  List<Account>();
        //setQueryString();
        //create Hierarchy Tree/s
        createNodes(null);
        
        //process Hierarchy Tree/s
        for(Node rootNode : rootNodes){
            rootNode.processTree();
        }
        
        //Update Database and report Result
        updateDatabase();
    }
    
    //NOT IN USE
    //Overloaded 1
    //method to set records right, for initially failed records withouit creating trees, the list passed must be of those accounts whose field are to be inverted
    public static void setRecordsRight(List<String> accountIds){
        //setQueryString();
        accountsToBeUpdated = new  List<Account>();
        
        System.debug('Update Debug setRecordsRight OnRiskFieldUpdator queryString - '+(queryString+' WHERE id IN:accountIds'));
        
        for(Account thisAccount : Database.query(queryString+' WHERE id IN:accountIds')){//change
            thisAccount.Child_Company_On_Risk__c = (!thisAccount.Child_Company_On_Risk__c);
            accountsToBeUpdated.add(thisAccount);
        }
        
        updateDatabase();
    }
    
    //NOT IN USE
    //Overloaded 2
    //method to set records right, for initially failed records withouit creating trees, the list passed must be of those accounts whose field are to be inverted
    public static void setRecordsRight(List<Account> accountList){
        //setQueryString();
        accountsToBeUpdated = new  List<Account>();
        for(Account thisAccount : accountList){//change
            thisAccount.Child_Company_On_Risk__c = (!thisAccount.Child_Company_On_Risk__c);
            accountsToBeUpdated.add(thisAccount);
        }
        updateDatabase();
    }
    
    //NOT IN USE
    //creates nodes after fetching Account from database
    static void createNodes(List<Account> thisLevelAccounts){
        List<Account> nextLevelAccounts = new List<Account>();
        List<String> thisLevelAccountIds = new List<String>();
        String finalQueryString;
        
        //create the Final Query String based on if it is for roots or Descendents in the Hierarchy Trees
        if(thisLevelAccounts != null){
            for(Account eachAccount : thisLevelAccounts){
                new Node(eachAccount);
                thisLevelAccountIds.add(eachAccount.id+'');
            }
            finalQueryString = queryString+childCondition;
        }else{
            finalQueryString = queryString+rootCondition;
        }
        for(Account childAccount : Database.query(finalQueryString)){
            nextLevelAccounts.add(childAccount);
        }
        if(nextLevelAccounts.size()>0){
            createNodes(nextLevelAccounts);
        }
    }
    */
    
    //IN USE
    //FOR Script Mode And Node Mode
    //Update Database and report Result
    public static void updateDatabase(){
        if(accountsToBeUpdated != null && accountsToBeUpdated.size()>0){
            Integer failureCount = 0;
            try{
                List<Database.SaveResult> saveResults = Database.update(accountsToBeUpdated,false);
                for(Database.SaveResult saveResult : saveResults){
                    if(!saveResult.isSuccess()){
                        failureCount++;
                        System.debug('Update Debug : Failed for Account - '+saveResult.getId());
                        System.debug('Update Debug : Failure Message/s : ');
                		Integer errorCount = 1;
                        for(Database.Error databaseError : saveResult.getErrors()){
                            System.debug(errorCount+'.) '+databaseError.getMessage());
                        }
                    }
                }
            }catch(Exception ex){
                System.debug('Update Debug : Exception - '+ex.getMessage());
            }
            finally{
                if(failureCount != 0){
                    System.debug('Update Debug Final : '+failureCount+'/'+accountstoBeUpdated.size()+' failed to update');
                }else{
                    System.debug('Update Debug Final : All Succeded');
                }
            }
        }
    }
    
    
    //to find incorrect values
    static void findDiscrepencies(){
        for(Account thisAccount : these.values()){
            Boolean thisrelatedOnRisk = thisAccount.Child_Company_On_Risk__c;
            Boolean parentOnRisk = false;
            Boolean anyChildOnRisk = false;
            if(childToParent.containsKey(thisAccount.id)){
                Account parentAccount = childToParent.get(thisAccount.id);
                parentOnRisk = parentAccount.Client_On_Risk__c;
            }
            if(parentToChildren.containsKey(thisAccount.id)){
                for(Account childAccount : parentToChildren.get(thisAccount.id)){
                    if(childAccount.Client_On_Risk__c){
                        anyChildOnRisk = true;
                        break;
                    }
                }
            }
            
            //check thisAccount
            if((!thisrelatedOnRisk) && (parentOnRisk || anyChildOnRisk)){//if correct set incorrect
                if(failedAccountIds == null){failedAccountIds = new List<String>();}
                failedAccountIds.add(thisAccount.id);
            }else if((thisrelatedOnRisk) && ((!parentOnRisk) && (!anyChildOnRisk))){//if correct set incorrect
                if(failedAccountIds == null){failedAccountIds = new List<String>();}
                failedAccountIds.add(thisAccount.id);
            }
        }
    }
        
    /*
    public class Node{
        String Id;
        String Name;
        Boolean OnRisk;
		Boolean relatedOnRisk;
        Node parent;
        List<Node> Children;
        Account account;
        
        //Constructor
        public Node(Account thisAccount){
            //nodesCreatedCount++;
            if(thisAccount == null) return;
            String shortenedId = String.valueOf(thisAccount.Id).substring(0,15);
            String shortenedParentId;
            if(treeNodes == null){treeNodes = new Map<String,Node>();}
            if(treeNodes.containsKey(shortenedId)){//if already created return
                return;
            }else{//create the new node
                nodesCreatedCount++;
                if(thisAccount.parentId == null){//if it is a root node
                    if(rootNodes == null) rootNodes = new List<Node>();
                    rootNodes.add(this);
                }else{
                    shortenedParentId = String.valueOf(thisAccount.parentId).substring(0,15);
                    if(treeNodes.containsKey(shortenedParentId)){//add "this" to its parent's Children List
                        Node parentNode = treeNodes.get(shortenedParentId);
                        if(parentNode.children == null){parentNode.Children = new List<Node>();}
                        parentNode.children.add(this);
                        this.parent = parentNode;
                    }else{//Parent hasn't been created yet but will be, eventually
                        List<Node> orphanNodes;
                        if(missingParentNodes == null){missingParentNodes = new Map<String,List<Node>>();}
                        if(!missingParentNodes.containsKey(shortenedParentId)){
                            orphanNodes = new List<Node>{this};
                        }
                        else{
                            orphanNodes = missingParentNodes.get(shortenedParentId);
                            orphanNodes.add(this);
                        }
                        missingParentNodes.put(shortenedParentId,orphanNodes);
                    }
                }
                this.account = thisAccount;
                this.id = shortenedId;
                this.name = thisAccount.Name;
                this.OnRisk = thisAccount.Client_On_Risk__c;
                this.relatedOnRisk = thisAccount.Child_Company_On_Risk__c;
                
                treeNodes.put(shortenedId,this);
                
                //is any child of "this" node already created?
                if(missingParentNodes != null && missingParentNodes.containsKey(shortenedId)){
                    List<Node> orphanNodes = missingParentNodes.get(shortenedId);
                    for(Node childNode : orphanNodes){
                        if(this.children == null){this.children = new List<Node>();}
                        this.children.add(childNode);
                        childNode.parent = this;
                    }
                    //missingParentNodes.remove(shortenedId);
                }
            }
            
        }
        
        
        //NOT IN USE
        //Updates the values of the Child_Company_On_Risk__c field
        //To be used when creating full trees, and processing traversal starts from roots and is depth-first
        void processTree(){
            Account thisAccount = this.account;
            Boolean anyChildOnRisk = false;
            Boolean parentOnRisk = false;
            if(this.parent != null){
                parentOnRisk = this.parent.OnRisk;
            }
            //first process children
            if(this.Children != null){
                for(Node childNode : this.children){
                    childNode.processTree();
                    if(childNode.onRisk && !anyChildOnRisk){
                        anyChildOnRisk = true;
                    }
                }
            }
            
            //process thisNode
            if((!this.relatedOnRisk) && (parentOnRisk || anyChildOnRisk)){
                thisAccount.Child_Company_On_Risk__c = true;
                accountsToBeUpdated.add(thisAccount);
            }else if((this.relatedOnRisk) && ((!parentOnRisk) && (!anyChildOnRisk))){
                thisAccount.Child_Company_On_Risk__c = false;
                accountsToBeUpdated.add(thisAccount);
            }
        }
        
        //Process "these" nodes in the batch
        void  processNode(){
            Account thisAccount = this.Account;
            Boolean parentOnRisk = false;
            Boolean anyChildOnRisk = false;
            if(this.parent != null){
                parentOnRisk = this.parent.onRisk;
            }
            if(this.children != null){
                for(Node childNode : this.children){
                    if(childNode.onRisk){
                        anyChildOnRisk = true;
                        break;
                    }
                }
            }
            if(Test.isRunningTest()){//FORTESTCLASS
                this.relatedOnRisk = true;
                thisAccount = new Account(name = 'testAccount');
            }
            
            //process thisNode
            if((!this.relatedOnRisk) && (parentOnRisk || anyChildOnRisk)){
                thisAccount.Child_Company_On_Risk__c = true;
                accountsToBeUpdated.add(thisAccount);
            }else if((this.relatedOnRisk) && ((!parentOnRisk) && (!anyChildOnRisk))){
                thisAccount.Child_Company_On_Risk__c = false;
                accountsToBeUpdated.add(thisAccount);
            }
        }
        
        //NOT IN USE
        //FOR TESTING ONLY - Process "these" nodes in the batch AND set incorrect values
        void  processNodeInverse(){
            Account thisAccount = this.Account;
            Boolean parentOnRisk = false;
            Boolean anyChildOnRisk = false;
            if(this.parent != null){
                parentOnRisk = this.parent.onRisk;
            }
            if(this.children != null){
                for(Node childNode : this.children){
                    if(childNode.onRisk){
                        anyChildOnRisk = true;
                        break;
                    }
                }
            }
            
            //process thisNode,if it is the correct flag, set incorrect flag
            if((this.relatedOnRisk) && (parentOnRisk || anyChildOnRisk)){
                thisAccount.Child_Company_On_Risk__c = false;
                accountsToBeUpdated.add(thisAccount);
            }else if((!this.relatedOnRisk) && ((!parentOnRisk) && (!anyChildOnRisk))){
                thisAccount.Child_Company_On_Risk__c = true;
                accountsToBeUpdated.add(thisAccount);
            }
        }
        
        //To get the list of accounts that don't have correct relatedOnrisk(Child_Company_On_Risk__c) value
        void findDiscrepencies(){
            Boolean parentOnRisk = false;
            Boolean anyChildOnRisk = false;
            String childrenOnRiskName = '';
            if(this.parent != null){
                parentOnRisk = this.parent.onRisk;
            }
            if(this.children != null){
                for(Node childNode : this.children){
                    if(childNode.onRisk && !anyChildOnRisk){
                        anychildOnRisk = true;
                    }
                }
            }
            
            if(this.relatedOnRisk && !(parentOnRisk || anyChildOnRisk)){
                if(failedAccountIds == null)failedAccountIds = new List<String>();
                failedAccountIds.add(this.id);
            }else if((!this.relatedOnRisk) && (parentOnRisk || anyChildOnRisk)){
                if(failedAccountIds == null){failedAccountIds = new List<String>();}
                failedAccountIds.add(this.id);
            }
        }
    }//Node Class Ends
    */
}