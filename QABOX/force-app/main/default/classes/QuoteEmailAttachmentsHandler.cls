global class QuoteEmailAttachmentsHandler implements Messaging.InboundEmailHandler {
    /************************************************************
     * Author: Sam Wadhwani
     * Company: cDecisions Ltd
     * Date: 03/03/2011
     * Description: Email handler class for quote emails. Class
     *            is invoked when the Email Quote button is
     *            used. The User composes an email and attaches
     *            any documents. The EmailQuote VF page BCCs the 
     *            email service address. This class extracts the
     *            email data, matches to an SF user by email address,
     *            locates the email task by owner and subject, and
     *            attaches any email documents to the task
     * Modifications:
     *    [ENTER DATE AND DESCRIPTION OF MODS HERE]
     *************************************************************/
      
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        System.debug('Start Email Service Processing with User ' + UserInfo.getName());
        
        String taskSubject = 'Email: ' + email.subject.replace('Force.com Sandbox: ', '');    //make sure this works in the sandbox too!
        String taskOwnerEmail = email.fromAddress;

        List<User> taskOwner = [Select Id from User where email=:taskOwnerEmail AND isActive = true limit 1];
        System.debug('Number of Users ' + taskOwner.size());
        
        if(taskOwner.size() > 0){   
            
            List<Task> task = [Select Id from Task where subject=:taskSubject AND ownerId=:taskOwner[0].Id order by createddate desc limit 1];
            System.debug('Number of tasks ' + task.size());
            if(task.size() > 0){
                List<Attachment> taskAttachments = new List<Attachment>();
                //System.debug('Number of Email Attachments ' + email.binaryAttachments.size());
                if (email.binaryAttachments!=null) {
                    for(Messaging.InboundEmail.binaryAttachment att: email.binaryAttachments){
                        
                        Attachment attachment = new Attachment();
                        attachment.Body = att.Body;
                        attachment.Name = att.fileName;
                        attachment.OwnerId = taskOwner[0].Id;
                        attachment.ParentId = task[0].Id;
                        
                        taskAttachments.add(attachment);
                    }
                }
                
                if(taskAttachments.size() > 0)
                    insert taskAttachments;
            }
        }
        
        return result;
    }
    
    static TestMethod void testInboundEmail() {
        String strRecId = [SELECT Id FROM RecordType WHERE Name='Marine' AND sObjectType='Opportunity' LIMIT 1].Id;
        String strEmail = [SELECT Email FROM User where Id = :UserInfo.getUserId() LIMIT 1].Email;
        //create an account
        Account acc = TestDataGenerator.getClientAccount();//new Account(Name='APEXTESTACC001');
        //insert acc;
        String strAccId = acc.Id;
        //create an opportunity
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = strRecId;
        opp.Name = 'APEXTESTOPP001';
        opp.AccountId = strAccId;
        opp.StageName = 'Renewable Opportunity';
        opp.Type = 'New Business';
        opp.Sales_Channel__c = 'Direct';
        opp.CloseDate = Date.Today();
        insert opp;
        String strOppId = opp.Id;
        //create a quote
        Quote qt = new Quote(Name='APEXTESTQTE001', OpportunityId=strOppId);
        insert qt;
        String strQuoteId = qt.Id;
        //create a task
        Task act = new Task();
        act.OwnerId = UserInfo.getUserId();
        act.Subject = 'Email: APEX TEST EMAIL SUBJECT';
        insert act;
        String strActId = act.Id;
        //create email
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Messaging.InboundEmail.BinaryAttachment inAtt = new Messaging.InboundEmail.BinaryAttachment();
        email.plainTextBody = 'APEX TEST ATTACHMENT BODY';
        email.fromAddress = 'sameer.wadhwani@cdecisions.com';
        email.subject = 'APEX TEST EMAIL SUBJECT';
        inAtt.body = blob.valueOf('APEX TEST ATTACHMENT CONTENT');
        inAtt.filename = 'APEX TEST ATTACHMENT FILENAME';
        inAtt.mimeTypeSubType = 'plain/txt';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] {inAtt};
        
        Test.startTest();
            QuoteEmailAttachmentsHandler testhandler = new QuoteEmailAttachmentsHandler();
            testhandler.handleInboundEmail(email, env);        
        Test.stopTest();
        
        List<Attachment> lsAttach = [SELECT Id, (SELECT Id, Name FROM Attachments) FROM Task WHERE Id=:strActId LIMIT 1].Attachments;
        //System.assertEquals(1, lsAttach.size());
    }
}