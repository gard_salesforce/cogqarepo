@isTest
private class CheckEditAccountExtTests {
	
	private static List<Valid_Role_Combination__c> validRoles;
	
	private static Account setupTestAccount() {
		return new Account (Name = 'Unit Test Account',Deleted__c = false, Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
	}
	
	private static void setupValidRoles() {
		////For some reason this seems to be throwing an internal Salesforce error /
		////System.UnexpectedException: Salesforce System Error: 1583762872-13077 (1716107030) (1716107030)
		//validRoles = (List<Valid_Role_Combination__c>)Test.loadData(Valid_Role_Combination__c.sObjectType, 'MDMValidRoles');
		////END...
		validRoles = new List<Valid_Role_Combination__c>();
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 1', Sub_Role__c = 'Sub Role 11'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 1', Sub_Role__c = 'Sub Role 12'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 2', Sub_Role__c = 'Sub Role 21'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 2', Sub_Role__c = 'Sub Role 22'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Other', Sub_Role__c = 'Other Sub Role'));
		insert validRoles;
	}
	
	static testMethod void CheckAndRedirect_SyncInProgress() {
		///ARRANGE...
		setupValidRoles();
		Account a = setupTestAccount();
		a.Synchronisation_Status__c = 'Sync In Progress';
		insert a;
		a = [SELECT id, Account_Sync_Status__c, Synchronisation_Status__c, deleted__c FROM Account WHERE id = :a.id];
		Apexpages.currentPage().getParameters().put('id',a.Id);     
		ApexPages.StandardController sc = new ApexPages.standardController(a);
        CheckEditAccountExt checkAccount = new CheckEditAccountExt(sc);
		
		///ACT...
		Test.startTest();
		PageReference pr = checkAccount.checkAndRedirect();
		Test.stopTest();
		
		///ASSERT...
		System.assertEquals(null, pr, 'Check Edit should return null when Sync In Progress');
	}
	
	static testMethod void CheckAndRedirect_SyncFailed() {
		///ARRANGE...
		setupValidRoles();
		Account a = setupTestAccount();
		a.Synchronisation_Status__c = 'Sync Failed';
		insert a;
		a = [SELECT id, Account_Sync_Status__c, Synchronisation_Status__c, deleted__c FROM Account WHERE id = :a.id];
		Apexpages.currentPage().getParameters().put('id',a.Id);     
		ApexPages.StandardController sc = new ApexPages.standardController(a);
        CheckEditAccountExt checkAccount = new CheckEditAccountExt(sc);
		
		///ACT...
		Test.startTest();
		PageReference pr = checkAccount.checkAndRedirect();
		Test.stopTest();
		
		///ASSERT...
		System.assertEquals(null, pr, 'Check Edit should return null when Sync Failed');
	}
	
	static testMethod void CheckAndRedirect_Synchronised() {
		///ARRANGE...
		setupValidRoles();
		Account a = setupTestAccount();
		a.Synchronisation_Status__c = 'Synchronised';
		insert a;
		a = [SELECT id, Account_Sync_Status__c, Synchronisation_Status__c, deleted__c FROM Account WHERE id = :a.id];
		Apexpages.currentPage().getParameters().put('id',a.Id);     
		ApexPages.StandardController sc = new ApexPages.standardController(a);
        CheckEditAccountExt checkAccount = new CheckEditAccountExt(sc);
		
		///ACT...
		Test.startTest();
		PageReference pr = checkAccount.checkAndRedirect();
		Test.stopTest();
		
		///ASSERT...
		System.assertNotEquals(null, pr, 'Check Edit should not return null when Synchronised');
	}
	
	static testMethod void CheckEditAccountExt_Coverage() {
		setupValidRoles();
		Account a = setupTestAccount();
		a.Synchronisation_Status__c = 'Synchronised';
		insert a;
		a = [SELECT id, Account_Sync_Status__c, Synchronisation_Status__c, deleted__c FROM Account WHERE id = :a.id];
		Apexpages.currentPage().getParameters().put('id',a.Id);     
		ApexPages.StandardController sc = new ApexPages.standardController(a);
        CheckEditAccountExt checkAccount = new CheckEditAccountExt(sc);
		checkAccount.checkAndRedirect();
		checkAccount.ignoreWarningAndEdit();
		checkAccount.goBack();
	}
	
}