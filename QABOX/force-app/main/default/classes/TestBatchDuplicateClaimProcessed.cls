@isTest
private class TestBatchDuplicateClaimProcessed  {
    
    Static testMethod void  BatchDuplicateClaimProcessedTest ()
    {   
        Id ClaimRecordTypeId = Schema.SObjectType.Claim__c.getRecordTypeInfosByName().get('Claim').getRecordTypeId();
        Id MyGardClaimRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('MyGardClaim').getRecordTypeId();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
        User u1 = new User(Alias = 'standt', Email='NewTestingUser1111@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='NewTestingUser', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = p.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='NewTestingUser1111@testorg.com');
        insert u1;
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting; 
        Case cs1 = new Case();
        cs1.Status = 'Pending';
        cs1.RecordTypeId = MyGardClaimRecordTypeId;
        cs1.Claim_Reference_Number__c = null;
        cs1.SFDC_Claim_Ref_ID__c = 'E132456';
        cs1.MyGard_Claim_ID__c = 'E132456';
        cs1.ParentId = null;
        system.runAs(u1){
            insert cs1;
        }
        //cs1.MyGard_Claim_ID__c = 'E13245';
        //update cs1;
        //System.runAs(u1) {
            
        //}
        //SELECT ID,Status,MyGard_Claim_ID__c FROM CASE WHERE RecordTypeId = : claimRecordTypeId and MyGard_Claim_ID__c in :mygardclaimid and Claim_Reference_Number__c =NULL and SFDC_Claim_Ref_ID__c like 'E%' and MyGard_Claim_ID__c like'E%'  and parentid=null  and createdby.name<>'integration'
        
        
        
        User u = new User(Alias = 'standt', Email='integration1111@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='integration_mule', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='integration1111@testorg.com');
        

        Test.startTest();
            List<Claim__c> csList = new List<Claim__c>();
            Claim__c cs = new Claim__c();
            cs.Status__c = 'New';
            cs.RecordTypeId = ClaimRecordTypeId;
            cs.Claim_Reference_Number__c = '12345';
            cs.SFDC_Claim_Ref_ID__c = '53245';
            cs.MyGard_Claim_ID__c = 'E132456';
            csList.add(cs);
            
            System.runAs(u) {
                //insert cs;
                insert csList;
            }
        
            List<Claim__c> caseList = new List<Claim__c>();
            caseList.add(cs);
            
            BatchDuplicateClaimProcessed2 job = new BatchDuplicateClaimProcessed2();
            Database.QueryLocator ql = job.start(null);
            job.execute(null,caseList);
            job.Finish(null);
        Test.StopTest();
        
    }
    
     Static testMethod void  BatchDuplicateClaimProcessedTest1 ()
    {   
        string mainClaimId1 = 'E0679395';
        mainClaimId1 = mainClaimId1.trim();
        Id ClaimRecordTypeId = Schema.SObjectType.Claim__c.getRecordTypeInfosByName().get('Claim').getRecordTypeId();
        Id MyGardClaimRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('MyGardClaim').getRecordTypeId();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
        User u1 = new User(Alias = 'standt', Email='NewTestingUser1111@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='NewTestingUser', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='NewTestingUser1111@testorg.com');
        
         AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting; 
        Case cs1 = new Case();
        cs1.Status = 'Pending';
        cs1.RecordTypeId = MyGardClaimRecordTypeId;
        cs1.Claim_Reference_Number__c = null;
        cs1.SFDC_Claim_Ref_ID__c = 'E132456';
        cs1.MyGard_Claim_ID__c = mainClaimId1;
        cs1.ParentId = null;
        insert cs1;
        
        //cs1.MyGard_Claim_ID__c = 'E13245';
        //update cs1;
        //System.runAs(u1) {
            
        //}
        //SELECT ID,Status,MyGard_Claim_ID__c FROM CASE WHERE RecordTypeId = : claimRecordTypeId and MyGard_Claim_ID__c in :mygardclaimid and Claim_Reference_Number__c =NULL and SFDC_Claim_Ref_ID__c like 'E%' and MyGard_Claim_ID__c like'E%'  and parentid=null  and createdby.name<>'integration'
        
        
        
        User u = new User(Alias = 'standt', Email='integration1111@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='integration_mule', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='integration1111@testorg.com');
        

        Test.startTest();
            List<Claim__c> csList = new List<Claim__c>();
            Claim__c cs = new Claim__c();
            cs.Status__c = 'New';
            cs.RecordTypeId = ClaimRecordTypeId;
            cs.Claim_Reference_Number__c = '12345';
            cs.SFDC_Claim_Ref_ID__c = '53245';
            cs.MyGard_Claim_ID__c = mainClaimId1;
        
           Claim__c cs10 = new Claim__c();
            //cs10.Status__c = 'New';
            cs10.RecordTypeId = ClaimRecordTypeId;
            cs10.Claim_Reference_Number__c = null;
            cs10.SFDC_Claim_Ref_ID__c = 'E3245';
            cs10.MyGard_Claim_ID__c = mainClaimId1;
            cs10.Parent__c = null;
            cs10.Status__c = 'Pending';
           
             csList.add(cs10);
            csList.add(cs);
            
            System.runAs(u1) {
                //insert cs;
                insert csList;
                system.debug('MyGard_Claim_ID__c--->>' + csList[0].MyGard_Claim_ID__c);
                 system.debug('MyGard_Claim_ID__c--->>' + csList[1].MyGard_Claim_ID__c);
            }
        
            List<Claim__c> caseList = new List<Claim__c>();
            caseList.add(cs);
            BatchDuplicateClaimProcessed2 job = new BatchDuplicateClaimProcessed2();
            Database.QueryLocator ql = job.start(null);
            job.execute(null,caseList);
            job.Finish(null);
        Test.StopTest();
        
    }
    
}