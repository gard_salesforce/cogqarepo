global class BatchInsertMyCover implements Database.Batchable<sObject> {
    
    //Declaring variables.
    global String strquery;
    global String  strTrueOrFalse;
   
    
    global BatchInsertMyCover (String truefalse){                  
        
        //strTrueOrFalse = truefalse;
        
        //strquery = 'Select Id, contactID from User Where ContactId !=null';                                                              
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){               
        return Database.getQueryLocator(strquery);        
    }
    
    /**
     * Implementation
     *
     * @return : null
     */
     global void execute(Database.BatchableContext BC, List<User> scope){  
     
      /*Boolean blBrokerView;
      List<MyCoverList__c> lstMyCover = new List<MyCoverList__c>();
      List<MyCoverList__c> delLstMyCover;
      List<MyCoverList__c> delLstMyCoverAll = new List<MyCoverList__c>();
      
        
        try{
            for(User usr:scope){
                Contact myContact;
                    if(usr!= null && usr.contactID != null){
                        myContact = [Select AccountId from Contact Where id=: usr.contactID];
                    }
                    Account acc;
                    List<Object__c> lstObject = new List<Object__c>();
                    List<Contract> lstContract = new List<Contract>();
                    List<Contract> lstContractClient = new List<Contract>();        
                    
                    if(myContact != null && myContact.AccountId != null){
                        acc = [Select ID,Company_Role__c from Account Where id =: myContact.AccountId ];        
                        if(acc != null && acc.Company_Role__c != null &&  acc.Company_Role__c.contains('Broker')){
                            blBrokerView = true;
                           
                           lstContractClient = [Select Id, Client__c from Contract Where Broker__c =: acc.id];
                        }else{
                            blBrokerView = false;          
                            lstContractClient = [Select id from Contract Where Client__c =: myContact.AccountId AND (broker__c=null or Shared_With_Client__c=true)];
                        }
                    }
                    
                    Set<ID> setCoverID = new Set<ID>();
                    for(Contract obj : lstContractClient){
                        setCoverID.add(obj.ID);
                    }
                   
                    string strforClient = '';
                    string strforClientGroupby = '';
                    
                    if(blBrokerView == true){
                        strforClient = 'Agreement__r.Client__r.Name clientname, Agreement__r.Client__r.Id clientId,';
                        strforClientGroupby = 'Agreement__r.Client__r.Name,Agreement__r.Client__r.Id,';
                    }
                    List<Boolean> lstblOnrisk = new List<Boolean>();
                    lstblOnrisk.add(Boolean.valueof(strTrueOrFalse));
                    String strCondition = ' AND On_risk_indicator__c IN : lstblOnrisk ';
                    
                    String sSoqlQuery = 'SELECT MAX(Expiration_Date__c) expdate, '+strforClient+' UnderwriterNameTemp__c underwriter, UnderwriterId__c underwriterId,Product_Name__c prod,Agreement__r.Business_Area__c busarea,'+ 
                                        'Agreement__r.Broker__r.Name AggName,Agreement__r.Broker__r.Id AggId, Agreement__r.Policy_Year__c plcyyr, '+
                                        'Claims_Lead__c claim, Gard_Share__c grdshr, On_risk_indicator__c rskind,'+
                                         'Count(id) FROM Asset Where Agreement__c IN : setCoverID '+strCondition+
                                        'GROUP BY '+strforClientGroupby+' UnderwriterNameTemp__c, UnderwriterId__c, Product_Name__c, '+
                                                 'Agreement__r.Business_Area__c,'+
                                                 'Agreement__r.Broker__r.Name,Agreement__r.Broker__r.Id,'+
                                                 'Agreement__r.Policy_Year__c,'+
                                                 'Claims_Lead__c, Gard_Share__c, On_risk_indicator__c';
                                                 
                    AggregateResult[] groupedResults = database.query(sSoqlQuery);
                    
                    if(Boolean.valueof(strTrueOrFalse)==true){
                        delLstMyCover = [Select id from MyCoverList__c Where userId__c =: usr.Id AND onrisk__c=true];
                    }else{
                        delLstMyCover = [Select id from MyCoverList__c Where userId__c =: usr.Id AND onrisk__c=false];
                    }
                    
                    for(MyCoverList__c mcl : delLstMyCover){
                        delLstMyCoverAll.add(mcl);
                    }
                    
                          
                    for (AggregateResult ar : groupedResults)  {
                        if(blBrokerView == false){
                            lstMyCover.add(new MyCoverList__c(
                                                 client__c='',
                                                 client_id__c='',
                                                 underwriter__c=string.valueof(ar.get('underwriter')),
                                                 UnderwriterId__c=string.valueof(ar.get('underwriterId')),
                                                 product__c=string.valueof(ar.get('prod')),
                                                 productarea__c=string.valueof(ar.get('busarea')),
                                                 broker__c=string.valueof(ar.get('AggName')),
                                                 broker_id__c=string.valueof(ar.get('AggId')), 
                                                 expirydate__c=string.valueof(ar.get('expdate')),
                                                 policyyear__c=Integer.valueof(ar.get('plcyyr')),                                           
                                                 claimslead__c=string.valueof(ar.get('claim')),
                                                 gardshare__c=Double.valueof(ar.get('grdshr')),
                                                 onrisk__c=Boolean.valueof(ar.get('rskind')),
                                                 viewobject__c=Integer.valueOf(ar.get('expr0')),
                                                 userId__c = usr.Id
                                             ));
                        }else{
                            lstMyCover.add(new MyCoverList__c(
                                                            client__c=string.valueof(ar.get('clientname')),
                                                            client_id__c=string.valueof(ar.get('clientId')),
                                                             underwriter__c=string.valueof(ar.get('underwriter')),
                                                             UnderwriterId__c=string.valueof(ar.get('underwriterId')),
                                                             product__c=string.valueof(ar.get('prod')),
                                                             productarea__c=string.valueof(ar.get('busarea')),
                                                             broker__c=string.valueof(ar.get('AggName')),
                                                             broker_id__c=string.valueof(ar.get('AggId')),
                                                             expirydate__c=string.valueof(ar.get('expdate')),
                                                             policyyear__c=Integer.valueof(ar.get('plcyyr')),                                           
                                                             claimslead__c=string.valueof(ar.get('claim')),
                                                             gardshare__c=Double.valueof(ar.get('grdshr')),
                                                             onrisk__c=Boolean.valueof(ar.get('rskind')),
                                                             viewobject__c=Integer.valueOf(ar.get('expr0')),
                                                             userId__c = usr.Id
                                                           
                                              ));
                        }
                     }
            }
            Database.delete( delLstMyCoverAll, false);
            Database.insert( lstMyCover, false);           
                            
        }catch( Exception ex){            
        }*/
      
    } 
    
    global void finish(Database.BatchableContext BC){ 
    
    }
}