/**************************************************************************
* Author  : Phil Spenceley
* Company : cDecisions
* Date    : 18/10/2013
***************************************************************************/
    
/// <summary>
///  This proxy class provides methods to access the MDM Contact Service
/// </summary>
public without sharing class MDMContactProxy extends MDMProxy {

	private Set<String> getContactFields(String requestName) {
		Set<String> fields = new Set<String>();
		for (MDM_Service_Fields__c field : MDM_Service_Fields__c.getAll().values()) {
			if (requestName.equalsIgnoreCase(field.Request_Name__c) && 'Contact'.equalsIgnoreCase(field.Source_Object__c) && 'Field'.equalsIgnoreCase(field.Type__c)) {
				fields.add(field.Field_Name__c.toLowerCase());
			}
		}
		return fields;
	}
	private Contact getContactById(id contactId, String requestName) {
		return (Contact)Database.query(buildSoqlRequest(getContactFields(requestName), 'Contact', contactId));
    }
    private List<Contact> getContactsByIds(List<id> contactIds, String requestName) {
        return (List<Contact>)Database.query(buildSoqlRequest(getContactFields(requestName), 'Contact', contactIds));
    }
	
	private MDMUpsertContactResponse MDMUpsertContacts(MDMUpsertContactRequest request) {
        DOM.Document responseDoc = sendRequest(request);
		MDMUpsertContactResponse response = new MDMUpsertContactResponse();
		try {
			response.Deserialize(
	            responseDoc
	                .getRootElement()
	                .getChildElement(SOAP_Body, SOAPNS)
	                .getChildElement(MDMChangeResponse_Label, MDMChangeResponse_Namespace)
	        );
		} catch (Exception ex) {
			throw new MDMDeserializationException(responseDoc, 'Failed to deserialize MDMContactProxy.MDMUpsertContactResponse', ex);
		}
		return response;
    }
	private MDMDeleteContactResponse MDMDeleteContacts(MDMDeleteContactRequest request) {
        DOM.Document responseDoc = sendRequest(request);
		MDMDeleteContactResponse response = new MDMDeleteContactResponse();
		try {
			response.Deserialize(
	            responseDoc
	                .getRootElement()
	                .getChildElement(SOAP_Body, SOAPNS)
	                .getChildElement(MDMChangeResponse_Label, MDMChangeResponse_Namespace)
	        );
		} catch (Exception ex) {
			throw new MDMDeserializationException(responseDoc, 'Failed to deserialize MDMContactProxy.MDMDeleteContactResponse', ex);
		}
		return response;
    }
	
	public void MDMUpsertContact(Map<id, Contact> upsertContacts) {
		MDMUpsertContactRequest request = new MDMUpsertContactRequest();
		Map<id, Contact> contacts = new Map<id, Contact>(getContactsByIds(new List<id>(upsertContacts.keySet()), request.getName()));
		
        try {
			request.contacts = contacts.values();
            MDMUpsertContactResponse response = MDMUpsertContacts(request);
			for (MDMResponse mdmResponse : response.MDMResponses) {
				System.debug('*** MDMResponse:[ id: + ' + mdmResponse.id + ', success: ' + string.valueOf(mdmResponse.success) + ']');
				if (mdmResponse.Success) {
	                contacts.get(mdmResponse.Id).Synchronisation_Status__c = 'Synchronised';
	            } else {
	                contacts.get(mdmResponse.Id).Synchronisation_Status__c = 'Sync Failed';
	            }	
			}
			
			update contacts.values(); 
			
        } catch (MDMDeserializationException ex) {
			Logger.LogException('MDMContactProxy.MDMDeleteContactAsync (@Future, ex)', ex, ex.xmlString);
			if (contacts != null) {
				SetContactsSyncFailed(contacts.values(), 'MDMContactProxy.MDMUpsertContactAsync (@Future, innerEx)');
			}
			
        } catch (Exception ex) {
            Logger.LogException('MDMContactProxy.MDMUpsertContactAsync (@Future, ex)', ex);
            if (contacts != null) {
				SetContactsSyncFailed(contacts.values(), 'MDMContactProxy.MDMUpsertContactAsync (@Future, innerEx)');
			}
        } 
	}
	
	public void MDMDeleteContact(Map<id, Contact> deleteContacts) {
		MDMDeleteContactRequest request = new MDMDeleteContactRequest();
		Map<id, Contact> contacts = new Map<id, Contact>(getContactsByIds(new List<id>(deleteContacts.keySet()), request.getName()));
		
        try {
			request.contacts = contacts.values();
            MDMDeleteContactResponse response = MDMDeleteContacts(request);
            //do something with the response...
            for (MDMResponse mdmResponse : response.MDMResponses) {
				System.debug('*** MDMResponse:[ id: + ' + mdmResponse.id + ', success: ' + string.valueOf(mdmResponse.success) + ']');
				if (mdmResponse.Success) {
	                contacts.get(mdmResponse.Id).Synchronisation_Status__c = 'Synchronised';
	            } else {
	                contacts.get(mdmResponse.Id).Synchronisation_Status__c = 'Sync Failed';
	            }	
			}
			
			update contacts.values(); 
			
        } catch (MDMDeserializationException ex) {
			Logger.LogException('MDMContactProxy.MDMDeleteContactAsync (@Future, ex)', ex, ex.xmlString);
			if (contacts != null) {
				SetContactsSyncFailed(contacts.values(), 'MDMContactProxy.MDMDeleteContactAsync (@Future, innerEx)');
			}
			
        } catch (Exception ex) {
            Logger.LogException('MDMContactProxy.MDMDeleteContactAsync (@Future, ex)', ex);
            if (contacts != null) {
				SetContactsSyncFailed(contacts.values(), 'MDMContactProxy.MDMDeleteContactAsync (@Future, innerEx)');
			}
        } 
	}
	
	@future (callout = true)
    public static void MDMUpsertContactAsync(list<id> contactIds) {
		MDMContactProxy proxy = new MDMContactProxy();
		MDMUpsertContactRequest request = new MDMUpsertContactRequest();
        Map<id, Contact> contacts = new Map<id, Contact>(proxy.getContactsByIds(contactIds, request.getName()));
		proxy.MDMUpsertContact(contacts);
	}
	
	@future (callout = true)
    public static void MDMDeleteContactAsync(list<id> contactIds) {
		MDMContactProxy proxy = new MDMContactProxy();
		MDMDeleteContactRequest request = new MDMDeleteContactRequest();
        Map<id, Contact> contacts = new Map<id, Contact>(proxy.getContactsByIds(contactIds, request.getName()));
		proxy.MDMDeleteContact(contacts);
	}
	
	//Called from an exception to set all sync status to failed
	private static void SetContactsSyncFailed(List<Contact> contacts, String source) {
		try {
			if (contacts != null) {
				for (Contact c : contacts) {
					c.Synchronisation_Status__c = 'Sync Failed';
				}
				update contacts; 
			}
		} catch (Exception ex) {
			//Simple update, hopefully shouldn't get here.
			Logger.LogException(source, ex);
		}
	}
	
	/*
	//USED FOR Manual testing...
	public static String TestMDMUpsert(List<id> contactIds) {
		MDMContactProxy proxy = new MDMContactProxy();
		MDMUpsertContactRequest request = new MDMUpsertContactRequest();
        List<Contact> contacts = proxy.getContactsByIds(contactIds, request.getName());
		
        try {
			request.contacts = contacts;
			String output = proxy.buildRequest(request.getSoapBody().getRootElement());
			System.debug(output);
			return output;
		} catch (Exception ex) {
			return '';
		}
	}
	*/
	
/// ----------------------------------------------------------------------------
///  -- Sub classes for the request and response messages called above
/// ----------------------------------------------------------------------------
	public virtual class MDMContactRequest extends MDMRequest {
		//TODO: move to custom setting...
		protected String MDMChangeContactsRequest_Label {
			get {
				return 'changeContactsRequest';
			}
		}
		protected String MDMChangeContactsRequest_Namespace {
			get {
				return 'http://www.gard.no/mdm/v1_0/mdmmessage';
			}
		}
		protected String MDMChangeContactsRequest_Prefix {
			get {
				return 'mdm';
			}
		}
		
        public virtual override String getName() { return 'changeContactsRequest'; }
		public List<Contact> Contacts { get; set; }
		
        public virtual override DOM.Document getSoapBody() {
            return getSoapBody(this.getName(), MDMChangeContactsRequest_Namespace, MDMChangeContactsRequest_Prefix);
        }
		
        public DOM.Document getSoapBody(string method, String namespace, String prefix) {
			system.debug('*** method: ' + method);
            DOM.Document body = new DOM.Document();
			DOM.XMLNode root = body.createRootElement(MDMChangeContactsRequest_Label, MDMChangeContactsRequest_Namespace, MDMChangeContactsRequest_Prefix);		

			//Get XML element list from Custom Setting
			List<MDM_Service_Fields__c> serviceFields = [
				SELECT Name, Request_Name__c, Sequence__c, Namespace_Prefix__c, Parent_Node__c, Type__c, XmlLabel__c, Source_Object__c, Field_Name__c, Address_Type__c, Omit_Node_If_Null__c, Strip_Special_Characters__c, Strip_Whitespaces__c
				FROM MDM_Service_Fields__c 
				WHERE Request_Name__c = :method
				ORDER BY Sequence__c];
			
            for(Contact c : this.Contacts) {
				DOM.XmlNode contactXml = root.addChildElement(method, namespace, prefix);
				appendSObjectXml(contactXml, c, serviceFields);
            }
            return body;
        }
    }
    
	public class MDMUpsertContactRequest extends MDMContactRequest {
		public override String getName() { return 'upsertContactRequest'; }
    }
	
	public virtual class MDMChangeResponse { 
		public List<MDMResponse> mdmResponses { get; set; }
		
		public MDMChangeResponse() {
			mdmResponses = new List<MDMResponse>();
		}
		
		protected void Deserialize(Dom.XmlNode root, String parentNode) {
			try {
				for  (Dom.XmlNode node : root.getChildElements()) {
					try {
						XmlSerializer s = new XmlSerializer();
			            object o = s.Deserialize(node, 'MDMContactProxy.MDMResponse');
			            mdmResponses.add((MDMResponse)o);
					} catch (Exception ex) {
						//TODO: handle exceptions better...
						//Don't let one failure failt he whole batch...?
					}
				}
			} catch (Exception ex) {
				//TODO: handle exceptions better...
				throw ex;
			}
		}
	}
	
	public class MDMResponse implements XmlSerializable {
		public Boolean Success { get; set; }
		public String Id { get; set; }
		public String Error { get; set; }
		
		public object get(string fieldName) {
	        Map<string, object> thisobjectmap = new Map<string, object> 
	        { 
	            'id' => this.Id, 
	            'success' => this.Success,
				'error' => this.Error
	        };
	        return thisobjectmap.get(fieldname);
	    }
	    
	    public boolean put(string fieldName, object value) {
	        if (fieldName == 'id') {
	            this.Id = String.valueOf(value);
	        } else if (fieldName == 'success') {
	            this.Success = Boolean.valueOf(value);  
			} else if (fieldName == 'error') {
	            this.Error = String.valueOf(value);
	        } else {
	            return false;
	        }
	        return true;
	    }
	    
	    public Set<string> getFields() {
	        Set<string> fields = new Set<string> 
	        { 
	            'success',
				'id',
				'error'
	        };
	        
	        return fields;
	    }
	}
	
	public class MDMUpsertContactResponse extends MDMChangeResponse {
		public void Deserialize(Dom.XmlNode root) {
			Deserialize(root, 'MDMContactProxy.MDMChangeResponse');
		}
	}
	
	public class MDMDeleteContactRequest extends MDMContactRequest {
		public override String getName() { return 'deleteContactRequest'; }
    }
	
	public class MDMDeleteContactResponse extends MDMChangeResponse {
		public void Deserialize(Dom.XmlNode root) {
			Deserialize(root, 'MDMContactProxy.MDMChangeResponse');
		}
	}

}