// Test class for BlueCardReq .
@isTest(seeAllData=false)
Public class TestBlueCardReq1Ctrl
{
    /*
    @testSetup 
    static void setup() {
        GardTestData test_Rec = new GardTestData();
        test_Rec.commonrecord();  
        test_Rec.customsettings_rec();   
    }
    */
    
    Public static testmethod void blueCardReqCtrl(){
        // Create test data................. 
           
        GardTestData test_Rec = new GardTestData();
        test_Rec.commonrecord();  
        test_Rec.customsettings_rec();
        
        
        ClientActivityEvents__c clientActivityEvent1 = new ClientActivityEvents__c(name = 'Request new Blue Card',Event_Type__c='Request new Blue Card');
        insert clientActivityEvent1;
        ClientActivityEvents__c clientActivityEvent2 = new ClientActivityEvents__c(name = 'Request renewal of Blue Card',Event_Type__c='Request renewal of Blue Card');
        insert clientActivityEvent2; 
        
        Organization_Wide_Email__c cV= new Organization_Wide_Email__c(name = 'test', value__c ='no-reply@gard.no');
        insert cV;
        
        
        List<Blue_Card_Object_Change_Request__c> lstBCOCRmail = new List<Blue_Card_Object_Change_Request__c>();
        Blue_Card_Object_Change_Request__c bco1= new Blue_Card_Object_Change_Request__c(Blue_Card_Request__c=GardTestData.bluecard_broker.id,case__c=GardTestData.brokerCase.id);
        lstBCOCRmail.add(bco1);
        
        insert lstBCOCRmail;
        
        // RequestObjectChange roc=new RequestObjectChange(newRegdOwnerAdd='testadd');
        //insert roc;
        //Invoking methods of BlueCardReqCtrl for broker and client users....
        // List<RequestObjectChange> addtempObj1 = new List<RequestObjectChange>();
        System.runAs(GardTestData.brokerUser){ 
            Test.startTest();
            BlueCardReqCtrl test_broker=new BlueCardReqCtrl();
            List<SelectOption> flag=test_broker.getFlagOption();
            List<SelectOption> port=test_broker.getPort();
            
            test_broker.dateEffective ='17.02.1960';// Date.newInstance(1960, 2, 17);
            test_broker.regOwnerChng=true;
            test_broker.flagChng=true;
            test_broker.flag='Norway';
            test_broker.callSign='NewSign';
            test_broker.vesselNameChng=true;
            test_broker.vesselName='NewVessel';
            test_broker.dateEffective ='12.12.2014';// Date.valueof('2014-12-12');
            test_broker.regOwnerName='testname';
            test_broker.comments='This is test';
            test_broker.objMode=true;
            test_broker.selectedPort='Arendal';
            String objId = GardTestData.test_object_1st.id;
            BlueCardReqCtrl.RequestObjectChange roc = new BlueCardReqCtrl.RequestObjectChange(objId,test_broker.flag,'Port','IMO',test_broker.callSign,'objName','Gross','newRegdOwnerName','newRegdOwnerAdd');
            test_broker.prvUrl='https://cs8.salesforce.com/setup/build/viewApexClass.apexp?objid='+objId+'';
            test_broker.ursFaithfully='Test';
            test_broker.LOUConfirmation=true;
            test_broker.populateClient();
            test_broker.selectedObj=new List<String>();
            test_broker.selectedObj.add(GardTestData.test_object_1st.id);
            test_broker.selectedObj.add(GardTestData.test_object_2nd.id);
            test_broker.selectedObj.add(GardTestData.test_object_3rd.id);
            system.assertequals(test_broker.selectedObj.size()>0,true,true);
            test_broker.SelectedClient=GardTestData.brokerContact.AccountId; 
            test_broker.loggedInAccId=GardTestData.brokerContact.AccountId;
            Case singleCase = [SELECT ID FROM CASE WHERE Claim_Reference_Number__c='kol23232' LIMIT 1];
            test_broker.caseTestId = singleCase.Id;
            test_broker.selectedClient=GardTestData.brokerAcc.id;
            //test_broker.selectedBlueCardType= 'Request new Blue Card';            
            test_broker.selectedBlueCardType='New Blue Card';
            test_broker.dateEffective ='17.02.1960';//Date.newInstance(1960, 2, 17);
            test_broker.dt = datetime.newInstance(1960, 02,17);
            test_broker.emailBody = '[HEADER][USERNAME] of [COMPANYNAME] has submitted a request for a change in Blue Card.The details submitted by are as follows: Client: [CLIENT]Object name: [OBJECTS] Date Effective: [DATE]Comments: [COMMENTS]Type of Blue Card: [TYPE]IMO number: [IMONUMBER]Flag: [FLAG]Port: [Port]Call sign: [CALLSIGN]Registered owners name and address: [NEWNAMEADDRESS]Preferred method of contact:Phone: [PHONEPREF]Email: [EMAILPREF]If you have any queries, please contact us at MyGard.support@gard.no.[SIGNATURE][FOOTER]';
            test_broker.saveCase();
            test_broker.lstBCOCRmail=[SELECT Blue_Card_Request__c,CallSign_New__c,CallSign_Old__c,CreatedById,CreatedDate,CurrencyIsoCode,
                                      Flag_New__c,Flag_Old__c,Gross_New__c,Gross_Old__c,Id,IMO_New__c,IMO_Old__c,IsDeleted,LastModifiedById,LastModifiedDate,
                                      Name,ObjName_New__c,Port_New__c,Port_Old__c,SystemModstamp,New_registered_owner_name__c,New_registered_owner_address__c FROM Blue_Card_Object_Change_Request__c WHERE Blue_Card_Request__c =: GardTestData.bluecard_broker.id];
            test_broker.lstBCOCRmail.add(bco1);
            test_broker.sendMails();
            //test_broker.lstBCOCRmail=[SELECT Blue_Card_Request__c,CallSign_New__c,CallSign_Old__c,CreatedById,CreatedDate,CurrencyIsoCode,
            //Flag_New__c,Flag_Old__c,Gross_New__c,Gross_Old__c,Id,IMO_New__c,IMO_Old__c,IsDeleted,LastModifiedById,LastModifiedDate,
            //Name,ObjName_New__c,Port_New__c,Port_Old__c,SystemModstamp,New_registered_owner_name__c,New_registered_owner_address__c FROM Blue_Card_Object_Change_Request__c WHERE Blue_Card_Request__c =: GardTestData.bluecard_broker.id];
            //test_broker.populateObj();
            //test_broker.objectMode();
            //test_broker.saveCase();
            //test_broker.submit();
            Test.stopTest();
        }
    }
    Public static testmethod void blueCardReqCtrl5(){
        // Create test data.................       
        
        GardTestData test_Rec = new GardTestData();
        test_Rec.commonrecord();  
        test_Rec.customsettings_rec();
		
        
        Organization_Wide_Email__c cV= new Organization_Wide_Email__c(name = 'test', value__c ='no-reply@gard.no');
        insert cV;
        List<Blue_Card_Object_Change_Request__c> lstBCOCRmail = new List<Blue_Card_Object_Change_Request__c>();
        Blue_Card_Object_Change_Request__c bco1= new Blue_Card_Object_Change_Request__c(Blue_Card_Request__c=GardTestData.bluecard_broker.id,case__c=GardTestData.brokerCase.id);
        lstBCOCRmail.add(bco1);
        insert lstBCOCRmail;
        
        System.runAs(GardTestData.clientUser){ 
            Test.startTest();
            BlueCardReqCtrl test_client=new BlueCardReqCtrl();
            List<SelectOption> selOpts=test_client.blueCardOption;
            List<SelectOption> flag=test_client.getFlagOption();
            List<SelectOption> port=test_client.getPort();
            List<SelectOption> objOp=test_client.objectOption;
            system.assertequals(flag.size()>0,true,true);
            system.assertequals(port.size()>0,true,true);
            test_client.regOwnerChng=true;
            test_client.flagChng=true;
            test_client.flag='Norway';
            test_client.callSign='NewSign';
            test_client.vesselNameChng=true;
            test_client.vesselName='NewVessel';
            test_client.dateEffective ='12.12.2014' ;//Date.valueof('2014-12-12');
            test_client.regOwnerName='testname';
            test_client.comments='This is test';
            test_client.objMode=false;
            test_client.selectedPort='Arendal';
            String objId = GardTestData.test_object_1st.id;
            test_client.prvUrl='https://cs8.salesforce.com/setup/build/viewApexClass.apexp?objid='+objId+'';
            test_client.ursFaithfully='Test';
            test_client.LOUConfirmation=true; 
            test_client.SelectedClient=GardTestData.clientContact.AccountId;
            test_client.fetchGlobalClients();
            test_client.selectedObj=new List<String>();
            test_client.selectedObj.add(GardTestData.test_object_1st.id);
            test_client.selectedObj.add(GardTestData.test_object_2nd.id);
            test_client.selectedObj.add(GardTestData.test_object_3rd.id);
            system.assertequals(test_client.selectedObj.size()>0,true,true);
            //test_client.populateClient();
            test_client.selectedBlueCardType='cancellation';
            test_client.selectedBlueCardType='Request ';
            test_client.selectedClient=GardTestData.clientAcc.id;
            test_client.selectedBlueCardType='Request renewal of Blue Card';
            Case singleCase = [SELECT ID FROM CASE WHERE Claim_Reference_Number__c = 'ClaiMRef1234' LIMIT 1];
            test_client.caseTestId = singleCase.Id;
            test_client.prvUrl='https://cs8.salesforce.com/setup/build/viewApexClass.apexp?objid=MyObjectDetails';
            
            System.assertNotEquals(null,test_client.selectedClient);
            test_client.displaydoc();
            test_client.cancel();
            Test.stopTest();
        } 
        
        
    }
    
    Public static testmethod void blueCardReqCtrl2(){
        // Create test data.................   
            
        GardTestData test_Rec = new GardTestData();
        test_Rec.commonrecord();  
        test_Rec.customsettings_rec();
		
        
        Organization_Wide_Email__c cV= new Organization_Wide_Email__c(name = 'test', value__c ='no-reply@gard.no');
        insert cV;
        //Invoking methods of blueCardReqCtrl for broker and client users....
        
        System.runAs(GardTestData.brokerUser){ 
            
            Test.startTest();
            BlueCardReqCtrl test_broker=new BlueCardReqCtrl();
            List<SelectOption> flag=test_broker.getFlagOption();
            List<SelectOption> port=test_broker.getPort();
            test_broker.dateEffective ='17.02.1960';// Date.newInstance(1960, 2, 17);
            test_broker.regOwnerChng=true;
            test_broker.flagChng=true;
            test_broker.flag='Norway';
            test_broker.callSign='NewSign';
            test_broker.vesselNameChng=true;
            test_broker.vesselName='NewVessel';
            test_broker.dateEffective ='12.12.2014';// Date.valueof('2014-12-12');
            test_broker.regOwnerName='testname';
            test_broker.comments='This is test';
            test_broker.objMode=false;
            test_broker.selectedPort='Arendal';
            String objId = GardTestData.test_object_1st.id;
            test_broker.prvUrl='https://cs8.salesforce.com/setup/build/viewApexClass.apexp?objid='+objId+'';
            test_broker.ursFaithfully='Test';
            test_broker.LOUConfirmation=true;
            test_broker.populateClient();
            test_broker.selectedObj=new List<String>();
            test_broker.selectedObj.add(GardTestData.test_object_1st.id);
            test_broker.selectedObj.add(GardTestData.test_object_2nd.id);
            test_broker.selectedObj.add(GardTestData.test_object_3rd.id);
            system.assertequals(test_broker.selectedObj.size()>0,true,true);
            test_broker.SelectedClient=GardTestData.brokerContact.AccountId; 
            test_broker.loggedInAccId=GardTestData.brokerContact.AccountId;
            test_broker.selectedBlueCardType='Request new Blue Card';
            Case singleCase = [SELECT ID FROM CASE WHERE Claim_Reference_Number__c='kol23232' LIMIT 1];
            test_broker.caseTestId = singleCase.Id;
            test_broker.selectedClient=GardTestData.brokerAcc.id;
            //test_broker.selectedBlueCardType='Request new Blue Card';
            test_broker.dateEffective ='17.02.1960';//Date.newInstance(1960, 2, 17);
            test_broker.dt = datetime.newInstance(1960, 02,17);
            test_broker.emailBody = '[HEADER][USERNAME] of [COMPANYNAME] has submitted a request for a change in Blue Card.The details submitted by are as follows: Client: [CLIENT]Object name: [OBJECTS] Date Effective: [DATE]Comments: [COMMENTS]Type of Blue Card: [TYPE]IMO number: [IMONUMBER]Flag: [FLAG]Port: [Port]Call sign: [CALLSIGN]Registered owners name and address: [NEWNAMEADDRESS]Preferred method of contact:Phone: [PHONEPREF]Email: [EMAILPREF]If you have any queries, please contact us at MyGard.support@gard.no.[SIGNATURE][FOOTER]';
            
            test_broker.populateObj();
            //test_broker.objectMode();
            //test_broker.addObj[0].CallSign='test1234';
            test_broker.lstBCOCRmail=[SELECT Blue_Card_Request__c,CallSign_New__c,CallSign_Old__c,CreatedById,CreatedDate,CurrencyIsoCode,
                                      Flag_New__c,Flag_Old__c,Gross_New__c,Gross_Old__c,Id,IMO_New__c,IMO_Old__c,IsDeleted,LastModifiedById,LastModifiedDate,
                                      Name,ObjName_New__c,Port_New__c,Port_Old__c,SystemModstamp,New_registered_owner_name__c,New_registered_owner_address__c FROM Blue_Card_Object_Change_Request__c WHERE Blue_Card_Request__c =: GardTestData.bluecard_broker.id];
            
            test_broker.saveCase();
            //test_broker.lstBCOCRmail=[SELECT Blue_Card_Request__c,CallSign_New__c,CallSign_Old__c,CreatedById,CreatedDate,CurrencyIsoCode,
            //Flag_New__c,Flag_Old__c,Gross_New__c,Gross_Old__c,Id,IMO_New__c,IMO_Old__c,IsDeleted,LastModifiedById,LastModifiedDate,
            //Name,ObjName_New__c,Port_New__c,Port_Old__c,SystemModstamp,New_registered_owner_name__c,New_registered_owner_address__c FROM Blue_Card_Object_Change_Request__c WHERE Blue_Card_Request__c =: GardTestData.bluecard_broker.id];
            
            test_broker.sendMails();
            
            test_broker.submit();
            
            test_broker.resetChkboxFields();
            test_broker.checkSelectedBlueCard();
            test_broker.sendType();
            test_broker.resetGTBox();
            test_broker.resetGTField();
            
        }        
    }
    
    @isTest private static void blueCardRequestCtrl2Client(){
        // Create test data.................       
        GardTestData test_Rec = new GardTestData();
        test_Rec.commonrecord();  
        test_Rec.customsettings_rec();
        Organization_Wide_Email__c cV= new Organization_Wide_Email__c(name = 'test', value__c ='no-reply@gard.no');
        insert cV;
        System.runAs(GardTestData.clientUser){ 
            Test.startTest();
            BlueCardReqCtrl test_client=new BlueCardReqCtrl();
            List<SelectOption> selOpts=test_client.blueCardOption;
            List<SelectOption> flag=test_client.getFlagOption();
            List<SelectOption> port=test_client.getPort();
            List<SelectOption> objOp=test_client.objectOption;
            system.assertequals(flag.size()>0,true,true);
            system.assertequals(port.size()>0,true,true);
            test_client.regOwnerChng=true;
            test_client.flagChng=true;
            test_client.flag='Norway';
            test_client.callSign='NewSign';
            test_client.vesselNameChng=true;
            test_client.vesselName='NewVessel';
            test_client.dateEffective ='12.12.2014' ;//Date.valueof('2014-12-12');
            test_client.regOwnerName='testname';
            test_client.comments='This is test';
            test_client.objMode=false;
            test_client.selectedPort='Arendal';
            String objId = GardTestData.test_object_1st.id;
            test_client.prvUrl='https://cs8.salesforce.com/setup/build/viewApexClass.apexp?objid='+objId+'';
            test_client.ursFaithfully='Test';
            test_client.LOUConfirmation=true; 
            test_client.SelectedClient=GardTestData.clientContact.AccountId;
            
            test_client.selectedObj=new List<String>();
            test_client.selectedObj.add(GardTestData.test_object_1st.id);
            test_client.selectedObj.add(GardTestData.test_object_2nd.id);
            test_client.selectedObj.add(GardTestData.test_object_3rd.id);
            system.assertequals(test_client.selectedObj.size()>0,true,true);
            //test_client.populateClient();
            test_client.selectedBlueCardType='cancellation';
            test_client.selectedBlueCardType='Request ';
            test_client.selectedClient=GardTestData.clientAcc.id;
            test_client.selectedBlueCardType='Request renewal of Blue Card';
            Case singleCase = [SELECT ID FROM CASE WHERE Claim_Reference_Number__c = 'ClaiMRef1234' LIMIT 1];
            test_client.caseTestId = singleCase.Id;
            test_client.prvUrl='https://cs8.salesforce.com/setup/build/viewApexClass.apexp?objid=MyObjectDetails';
            System.assertNotEquals(null,test_client.selectedClient);
            test_client.displaydoc();
            
            test_client.cancel();
            Test.stopTest();
        }                
    }
    
    Public static testmethod void blueCardReqCtrl3(){
        // Create test data.................       
        
        GardTestData test_Rec = new GardTestData();
        test_Rec.commonrecord();  
        test_Rec.customsettings_rec();
		
        
        Organization_Wide_Email__c cV= new Organization_Wide_Email__c(name = 'test', value__c ='no-reply@gard.no');
        insert cV;
        ClientActivityEvents__c clientActs = new ClientActivityEvents__c(name = 'New Blue Card',Event_Type__c='Blue Card for new Object');
        insert clientActs;
        //Invoking methods of BlueCardReqCtrl for broker and client users....
        System.runAs(GardTestData.brokerUser){ 
            
            Test.startTest();
            BlueCardReqCtrl test_broker=new BlueCardReqCtrl();
            List<SelectOption> flag=test_broker.getFlagOption();
            List<SelectOption> port=test_broker.getPort();
            test_broker.dateEffective ='17.02.1960';// Date.newInstance(1960, 2, 17);
            test_broker.regOwnerChng=true;
            test_broker.flagChng=true;
            test_broker.flag='Norway';
            test_broker.callSign='NewSign';
            test_broker.vesselNameChng=true;
            test_broker.vesselName='NewVessel';
            test_broker.dateEffective ='12.12.2014';// Date.valueof('2014-12-12');
            test_broker.regOwnerName='testname';
            test_broker.comments='This is test';
            test_broker.objMode=false;
            test_broker.selectedPort='Arendal';
            String objId = GardTestData.test_object_1st.id;
            test_broker.prvUrl='https://cs8.salesforce.com/setup/build/viewApexClass.apexp?objid='+objId+'';
            test_broker.ursFaithfully='Test';
            test_broker.LOUConfirmation=true;
            test_broker.populateClient();
            //system.assertequals(test_broker.selectedObj.size()>0,true,true);
            test_broker.SelectedClient=GardTestData.brokerContact.AccountId; 
            test_broker.loggedInAccId=GardTestData.brokerContact.AccountId;
            Case singleCase = [SELECT ID FROM CASE WHERE Claim_Reference_Number__c='kol23232' LIMIT 1];
            test_broker.caseTestId = singleCase.Id;
            test_broker.selectedClient=GardTestData.brokerAcc.id;
            test_broker.selectedBlueCardType='Request renewal of Blue Card';
            test_broker.dateEffective ='17.02.1960';//Date.newInstance(1960, 2, 17);
            test_broker.dt = datetime.newInstance(1960, 02,17);
            test_broker.emailBody = '[HEADER][USERNAME] of [COMPANYNAME] has submitted a request for a change in Blue Card.The details submitted by are as follows: Client: [CLIENT]Object name: [OBJECTS] Date Effective: [DATE]Comments: [COMMENTS]Type of Blue Card: [TYPE]IMO number: [IMONUMBER]Flag: [FLAG]Port: [Port]Call sign: [CALLSIGN]Registered owners name and address: [NEWNAMEADDRESS]Preferred method of contact:Phone: [PHONEPREF]Email: [EMAILPREF]If you have any queries, please contact us at MyGard.support@gard.no.[SIGNATURE][FOOTER]';
            //test_broker.sendMails();
            test_broker.populateObj();
            test_broker.selectedObj=new List<String>();
            test_broker.selectedObj.add(GardTestData.test_object_1st.id);
            test_broker.selectedObj.add(GardTestData.test_object_2nd.id);
            test_broker.selectedObj.add(GardTestData.test_object_3rd.id);
            
            test_broker.objectMode();
            test_broker.selectedBlueCardType='Request renewal of Blue Card';
            test_broker.lstBCOCRmail=[SELECT Blue_Card_Request__c,CallSign_New__c,CallSign_Old__c,CreatedById,CreatedDate,CurrencyIsoCode,
                                      Flag_New__c,Flag_Old__c,Gross_New__c,Gross_Old__c,Id,IMO_New__c,IMO_Old__c,IsDeleted,LastModifiedById,LastModifiedDate,
                                      Name,ObjName_New__c,Port_New__c,Port_Old__c,SystemModstamp,New_registered_owner_name__c,New_registered_owner_address__c FROM Blue_Card_Object_Change_Request__c WHERE Blue_Card_Request__c =: GardTestData.bluecard_broker.id];  
            test_broker.saveCase();
            //test_broker.submit();
            test_broker.selectedBlueCardType='Request new Blue Card';
            test_broker.saveCase();
            test_broker.addObj[0].CallSign='test1234';
            //test_broker.submit();
            Test.stopTest();
        }
    }
    Public static testmethod void blueCardReqCtrl4(){   //sfedit new method created from blueCardReqCtrl3->4
        
        GardTestData test_Rec = new GardTestData();
        test_Rec.commonrecord();  
        test_Rec.customsettings_rec();
		
        
        Organization_Wide_Email__c cV= new Organization_Wide_Email__c(name = 'test', value__c ='no-reply@gard.no');
        insert cV;
        System.runAs(GardTestData.clientUser){ 
            Test.startTest();
            BlueCardReqCtrl test_client=new BlueCardReqCtrl();
            List<SelectOption> selOpts=test_client.blueCardOption;
            List<SelectOption> flag=test_client.getFlagOption();
            List<SelectOption> port=test_client.getPort();
            List<SelectOption> objOp=test_client.objectOption;
            system.assertequals(flag.size()>0,true,true);
            system.assertequals(port.size()>0,true,true);
            test_client.regOwnerChng=true;
            test_client.flagChng=true;
            test_client.flag='Norway';
            test_client.callSign='NewSign';
            test_client.vesselNameChng=true;
            test_client.vesselName='NewVessel';
            test_client.dateEffective ='12.12.2014' ;//Date.valueof('2014-12-12');
            test_client.regOwnerName='testname';
            test_client.comments='This is test';
            test_client.objMode=false;
            test_client.selectedPort='Arendal';
            String objId = GardTestData.test_object_1st.id;
            test_client.prvUrl='https://cs8.salesforce.com/setup/build/viewApexClass.apexp?objid='+objId+'';
            test_client.ursFaithfully='Test';
            test_client.LOUConfirmation=true; 
            test_client.SelectedClient=GardTestData.clientContact.AccountId;
            
            test_client.selectedObj=new List<String>();
            test_client.selectedObj.add(GardTestData.test_object_1st.id);
            test_client.selectedObj.add(GardTestData.test_object_2nd.id);
            test_client.selectedObj.add(GardTestData.test_object_3rd.id);
            //system.assertequals(test_client.selectedObj.size()>0,true,true);
            test_client.populateClient();
            test_client.selectedBlueCardType='cancellation';
            test_client.selectedBlueCardType='Request ';
            test_client.selectedClient=GardTestData.clientAcc.id;
            test_client.selectedBlueCardType='Request renewal of Blue Card';
            //Case singleCase = [SELECT ID FROM CASE WHERE Claim_Reference_Number__c = 'ClaiMRef1234' LIMIT 1];
            //test_client.caseTestId = singleCase.Id;
            
            test_client.prvUrl='https://cs8.salesforce.com/setup/build/viewApexClass.apexp?objid=MyObjectDetails';
            System.assertNotEquals(null,test_client.selectedClient);
            test_client.displaydoc();
            test_client.cancel();
            test_client.objMode=true;
            test_client.objectMode();
            Test.stopTest();
        }                        
    }
}