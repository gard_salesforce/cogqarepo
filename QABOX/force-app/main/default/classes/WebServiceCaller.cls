//TestClass : TestPemeWebService
/*
*    Purpose: This class creates the JSON request and makes PEMEInvoice WS call-out.
*/
public class WebServiceCaller{
    @future(callout = true)
    public static void callPemeWs(Id pemeInvoiceId, Id cvId){
        
        Mule_Web_Service__c mws = Mule_Web_Service__c.getInstance('PEME Invoice And File Service');
		if(mws == null || !mws.Enabled__c) return;
        
        //Create the Payload
        PEME_Invoice__c pemeInvoice = (PEME_Invoice__c) getRecord(pemeInvoiceId,pemeInvoiceId.getSobjectType().getDescribe().getName());
        ContentVersion pemeFile = (ContentVersion) getRecord(cvId,cvId.getSobjectType().getDescribe().getName());
        JsonDataWrapper.PemeInvoiceFile fileBody = new JsonDataWrapper.PemeInvoiceFile(pemeInvoice,pemeFile);
        
        //make Auth Call
        JsonRequestWrapper.PemeInvoiceFileAuth authRqst = new JsonRequestWrapper.PemeInvoiceFileAuth();
        JsonResponseWrapper.PemeInvoiceFileAuth authResp = authRqst.callWs();
        
        //Make Data Call
        JsonRequestWrapper.PemeInvoiceFile fileRqst = new JsonRequestWrapper.PemeInvoiceFile(authResp,fileBody);
        JsonResponseWrapper.PemeInvoiceFile fileResp = fileRqst.callWs();
    }
    
    private static SObject getRecord(Id recordId,String sObjectName){
        String sObjectFieldsStr;
        Map<String, Schema.SObjectField> emFields = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap();
        for(Schema.SObjectField emField : emFields.values()){
            Schema.DescribeFieldResult fieldResult = emField.getDescribe();
            sObjectFieldsStr = (sObjectFieldsStr == null) ? fieldResult.getName() : sObjectFieldsStr+','+fieldResult.getName();
        }
        
        if(recordId.getSobjectType().getDescribe().getName() == 'PEME_Invoice__c')sObjectFieldsStr+=',clinic__r.company_Id__c';
        
        List<SObject> retRecords = Database.query('SELECT '+sObjectFieldsStr+' FROM '+sObjectName+' WHERE Id = :recordId');
        
        if(retRecords.isEmpty()){
            return null;
        }
        return retRecords[0];
    }
}