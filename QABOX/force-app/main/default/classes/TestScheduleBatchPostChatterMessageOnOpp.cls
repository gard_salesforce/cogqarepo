/***************************************************
* Class Name: TestScheduleBatchPostChatterMessageOnOpp
* Created By: Nagarjuna Kaipu
* Date: 12-04-2021
* Description: Test class for TestScheduleBatchPostChatterMessageOnOpp
***************************************************/
@IsTest(SeeAllData=true)
public class TestScheduleBatchPostChatterMessageOnOpp {
    public static testmethod void TestScheduleBatchPostChatterMessage(){
        //Account acc = [Select Id, Type From Account Where Type = 'Customer' Limit 1];
        //Id recTypeId = [SELECT Id FROM RecordType WHERE Name='Marine' AND sObjectType='Opportunity'].id;
        //Opportunity opp = [Select Id, Expiry_Date__c From Opportunity Where Type = 'Renewal' AND RecordType.Name = 'Marine' Limit 1];
        /*Opportunity opportunity = new Opportunity();
        opportunity.RecordTypeId = recTypeId;
        opportunity.Type = 'Renewal';
        opportunity.Name = 'Test opportunity';
        //opportunity.AccountId = GardTestData.client_Acc.Id;
        opportunity.AccountId = acc.id;
        opportunity.StageName = 'Quote';
        opportunity.CloseDate = Date.today();
        opportunity.Business_Type__c = 'Ship Owners';
        opportunity.Amount = 150000;
        opportunity.Approval_Criteria__c = '';
        opportunity.Senior_Approver__c = null;      
        opportunity.Expiry_Date__c = date.today().adddays(60);
        insert opportunity;
*/
        
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        ScheduleBatchPostChatterMessageOnOpp schBatch = new ScheduleBatchPostChatterMessageOnOpp();
        system.schedule('Testing Scheduler Batch', CRON_EXP, schBatch);   
        Test.stopTest();
    } 
}