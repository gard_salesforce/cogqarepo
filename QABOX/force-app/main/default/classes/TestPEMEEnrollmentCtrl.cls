@isTest(seeAllData=false)
public class TestPEMEEnrollmentCtrl
{
    Public static List<Account> AccountList=new List<Account>();
    Public static List<Contact> ContactList=new List<Contact>();
    Public static List<User> UserList=new List<User>();
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 

1].id;
    Public static string partnerLicenseIdStd = [SELECT Id,name FROM profile where name like 'Gard Standard User' limit 

1].id;
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 

1].id;
    public static Gard_Contacts__c gc;
    Public static List<Object__c> ObjectList=new List<Object__c>();
    public static list<Asset> assetTest=new list<Asset>();
    public static List<Contract> contractList=new List<Contract>();
    public static List<Extranet_Global_Client__c> extClientList=new List<Extranet_Global_Client__c>();
    Public static Account_Contact_Mapping__c Accmap_1st, Accmap_2nd;
    Public static list<Account_Contact_Mapping__c> Accmap_list = new list<Account_Contact_Mapping__c>();
    Public static AccountToContactMap__c map_broker = new AccountToContactMap__c();
    Public static AccountToContactMap__c map_client = new AccountToContactMap__c();
    //Added for R3 broker share CR --- Start
    Public static Agreement_set__c agrSet2 = new Agreement_set__c();
    Public static Broker_share__c brokerShr4 = new Broker_share__c();
    Public static contact_share__c contactShr4 = new contact_share__c();
    //Added for R3 broker share CR --- End
    static Account brokerAcc,clientAcc;
    static User brokerUser,clientUser,brokerUser1;
    static Contact brokerContact,clientContact;
    
    public static void createTestData(){
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        gc = new Gard_Contacts__c(FirstName__c ='test',lastName__c = 'test');
        insert gc;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt;
         Gard_Team__c gt = new Gard_Team__c();
        gt.Active__c = true;
        gt.Office__c = 'test';
        gt.Name = 'GTeam';
        gt.Region_code__c = 'TEST';
        insert gt;
        Country__c country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA',Gard_Team__c=gt.Id,Claims_Support_Team__c=gt.Id);
        insert country;
        List<Valid_Role_Combination__c> vrcList = new List<Valid_Role_Combination__c>();
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
        vrcList.add(vrcClient);
        Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
        vrcList.add(vrcBroker);
        insert vrcList;
        User user = new User(
                            Alias = 'standt', 
                            profileId = salesforceLicenseId ,
                            Email='standarduser@testorg.com',
                            EmailEncodingKey='UTF-8',
                            CommunityNickname = 'test13',
                            LastName='Testing',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US',  
                            TimeZoneSidKey='America/Los_Angeles',
                            UserName='test008@testorg.com',
                            ContactId__c = gc.id
                             );
        insert user ;
        User user1 =new User();
        user1 = [select id, contactid__c from user where id=:user.id];
        
        brokerAcc = new Account( Name='brokerAccount',
                                        BillingCity = 'Bristol',
                                        BillingCountry = 'United Kingdom',
                                        BillingPostalCode = 'BS1 1AD',
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.Broker_Contact_Record_Type,
                                        Site = '_www.cts.se',
                                        Type = 'Broker' ,
                                        Area_Manager__c = user1.id,      
                                        Market_Area__c = Markt.id,
                                        PEME_Enabled__c=true,
                                        PEME_Enrollment_Status__c = 'Not Enrolled',
                                        BillingCity_1__c='Bristol',
                                        BillingPostalcode__c='BS1 1AD',
                                        BillingCountry__c='United Kingdom',
                                        BillingState__c='Avon',
                                        Billing_Street_Address_Line_1__c='1 Elmgrove Road',
                                        Billing_Street_Address_Line_2__c='Bristol',
                                        Billing_Street_Address_Line_3__c='Avon',
                                        Billing_Street_Address_Line_4__c ='United Kingdom',
                                        Confirm_not_on_sanction_lists__c = true,
                                        License_description__c  = 'some desc',
                                        Description = 'some desc',
                                        Licensed__c = 'Pending',
                                        company_Role__c = 'Broker',
                                        Sub_Roles__c = 'Broker - Reinsurance Broker',
                                        country__c = country.id
                                       );
        AccountList.add(brokerAcc);
        clientAcc= New Account();
        clientAcc.Name = 'clientAccount';
        clientAcc.recordTypeId = System.Label.Client_Contact_Record_Type;
        clientAcc.Site = '_www.test.se';
        clientAcc.Type = 'Customer';
        clientAcc.BillingStreet = 'Gatan 1';
        clientAcc.BillingCity = 'Stockholm';
        clientAcc.BillingCountry = 'SWE';    
        clientAcc.Area_Manager__c = user1.id;        
        clientAcc.Market_Area__c = Markt.id; 
        clientAcc.PEME_Enabled__c=true;
        clientAcc.PEME_Enrollment_Status__c = 'Not Enrolled';
        clientAcc.BillingCity_1__c='Stockholm';
        clientAcc.BillingPostalcode__c='BS1 1AD';
        clientAcc.BillingCountry__c='SWE';
        clientAcc.BillingState__c='Avon';
        clientAcc.Billing_Street_Address_Line_1__c='Gatan 1';
        clientAcc.Billing_Street_Address_Line_2__c='Stockholm';
        clientAcc.Billing_Street_Address_Line_3__c='Avon';
        clientAcc.Billing_Street_Address_Line_4__c ='SWE';
        clientAcc.country__c = country.id;
        AccountList.add(clientAcc);
        insert AccountList;
        Blob b = Crypto.GenerateAESKey(128);            
        String h = EncodingUtil.ConvertTohex(b);            
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20);
        brokerAcc.GUID__c=guid;
        update brokerAcc;
        Blob b1 = Crypto.GenerateAESKey(128);            
        String h1 = EncodingUtil.ConvertTohex(b1);            
        String guid1 = h1.SubString(0,8)+ '-' + h1.SubString(8,12) + '-' + h1.SubString(12,16) + '-' + h1.SubString

(16,20);
        clientAcc.GUID__c=guid1;
        update clientAcc;
        
        brokerContact = new Contact( 
                                             FirstName='Raan',
                                             LastName='Baan',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState = 'London',
                                             MailingStreet = '1 London Road',
                                             AccountId = brokerAcc.Id,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(brokerContact);
        clientContact= new Contact(  
                                             FirstName='tsat_1',
                                             LastName='chak',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AE',
                                             MailingState = 'London',
                                             MailingStreet = '4 London Road',
                                             AccountId = clientAcc.Id,
                                             Email = 'test321@gmail.com'
                                          );
        ContactList.add(clientContact);
        insert ContactList;
        
        brokerUser = new User(
                                    Alias = 'standt', 
                                    profileId = partnerLicenseId,
                                    Email='test120@test.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='estTing',
                                    LanguageLocaleKey='en_US',
                                    CommunityNickname = 'brokerUser',
                                    LocaleSidKey='en_US',  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testbrokerUser@testorg.com.mygard',
                                    ContactId = BrokerContact.Id
                                   );
        UserList.add(brokerUser);
        clientUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'clientUser',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey='en_US',
                                    LocaleSidKey='en_US',
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testclientUser@testorg.com.mygard',
                                    ContactId = clientContact.id
                                   );
                
        UserList.add(clientUser) ;
        brokerUser1 = new User(
                                    Alias = 'standt1', 
                                    profileId = partnerLicenseIdStd,//partnerLicenseId,
                                    Email='test120@test.com1',
                                    EmailEncodingKey='UTF-8',
                                    LastName='estTing1',
                                    LanguageLocaleKey='en_US',
                                    CommunityNickname = 'brokerUser1',
                                    LocaleSidKey='en_US',  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testbrokerUser@testorg.com.mygard1'//,
                                    //ContactId = BrokerContact.Id
                                   );
        UserList.add(brokerUser1);        
        insert UserList;
        
        Object__c Object1=new Object__c(Name= 'TestObject1',
                                     //CurrencyIsoCode='NOK-Norwegian Krone',
                                     Object_Unique_ID__c='test11223'
                                     );
        ObjectList.add(Object1);
        Object__c Object2=new Object__c(
                                     Name= 'TestObject2',
                                    // CurrencyIsoCode='NOK-Norwegian Krone',
                                     Object_Unique_ID__c='test11224'
                                     );
        ObjectList.add(Object2);
        insert ObjectList;
        
        Contract brokerContract=new Contract(
                                            Accountid = brokerAcc.id, 
                                            Account = brokerAcc, 
                                            Status = 'Draft',
                                            CurrencyIsoCode = 'SEK', 
                                            StartDate = Date.today(),
                                            ContractTerm = 2,
                                            Agreement_Type__c = 'Marine', 
                                            Broker__c = brokerAcc.id,
                                            Client__c = clientAcc.id,
                                            Broker_Name__c = brokerAcc.id,
                                            Expiration_Date__c = date.valueof('2016-02-01'),
                                            Inception_date__c = date.valueof('2012-01-01'),
                                            Agreement_Type_Code__c = 'Agreement127',
                                            Agreement_ID__c = 'Agreement_ID_881',
                                            Business_Area__c='Marine',
                                            Accounting_Contacts__c = brokerUser.id,
                                            Contract_Reviewer__c = brokerUser.id,
                                            Contracting_Party__c = brokerAcc.id,
                                            Policy_Year__c='2015',
                                            Name_of_Contract__c = 'ABC'             
                                            );
        contractList.add(brokerContract);                                    
        
        Contract clientContract=new Contract (
                                              Accountid = clientAcc.id, 
                                              Account = clientAcc, 
                                              Status = 'Draft',
                                              CurrencyIsoCode = 'SEK', 
                                              StartDate = Date.today(),
                                              ContractTerm = 2,
                                              //Broker__c = brokerAcc.id,
                                              Client__c = clientAcc.id,
                                              Expiration_Date__c = date.valueof('2016-02-01'),
                                              Agreement_Type__c = 'P&I',
                                              Agreement_Type_Code__c = 'Agreement123',
                                              Inception_date__c = date.valueof('2012-01-01'),
                                              Business_Area__c='P&I',
                                              Policy_Year__c='2014',
                                              Name_of_Contract__c = 'ABC'
                                             );
        contractList.add(clientContract);
        insert contractList;
        
        Asset brokerAsset=new Asset();
        brokerAsset.AccountId=brokerAcc.ID;
        brokerAsset.ContactId=brokerContact.ID;
        brokerAsset.Agreement__c=brokerContract.ID;
        brokerAsset.Object__c=Object1.ID;
        brokerAsset.Name='Crew Cover';
        brokerAsset.Expiration_Date__c=date.valueof('2016-06-01');
        brokerAsset.On_risk_indicator__c=true;
        brokerAsset.Inception_date__c =date.valueof('2016-04-10');
        brokerAsset.Risk_ID__c='112233eeffddggff';
        assetTest.add(brokerAsset);
        
        Asset clientAsset=new Asset();
        clientAsset.AccountId=clientAcc.ID;
        clientAsset.ContactId=clientContact.ID;
        clientAsset.Agreement__c=clientContract.ID;
        clientAsset.Object__c=Object2.ID;
        clientAsset.Name='P&I Cover';
        clientAsset.Expiration_Date__c=date.valueof('2016-06-01');
        clientAsset.On_risk_indicator__c=true;
        clientAsset.Inception_date__c =date.valueof('2016-04-10');
        clientAsset.Risk_ID__c='112233eeffddggft';
        assetTest.add(clientAsset);
        insert assetTest;
        
        Accmap_1st = new Account_Contact_Mapping__c(Account__c = brokerAcc.id,
                                                   Active__c = true,
                                                   Administrator__c = 'Admin',
                                                   Contact__c = brokerContact.id,
                                                   IsPeopleClaimUser__c = false,
                                                   Marine_access__c = true,
                                                   PI_Access__c = true
                                                   //Show_Claims__c = false,
                                                   //Show_Portfolio__c = false 
                                                   );
           
                                 
          
         Accmap_2nd = new Account_Contact_Mapping__c(Account__c =  clientAcc.id,
                                                   Active__c = true,
                                                   Administrator__c = 'Normal',
                                                   Contact__c = clientContact.id,
                                                   IsPeopleClaimUser__c = false,
                                                   Marine_access__c = true,
                                                   PI_Access__c = true
                                                   //Show_Claims__c = false,
                                                   //Show_Portfolio__c = false 
                                                   );
        Accmap_list.add(Accmap_1st);
        Accmap_list.add(Accmap_2nd);
        
        Insert Accmap_list;
        
        Extranet_Global_Client__c brokerClient1=new Extranet_Global_Client__c();
        brokerClient1.Selected_By__c=brokerUser.Id;
        brokerClient1.Selected_For_Client__c=brokerAcc.Id;
        brokerClient1.Selected_Global_Clients__c=brokerAcc.Id+';'+clientAcc.Id;
        extClientList.add(brokerClient1);
        
        Extranet_Global_Client__c clientClient1=new Extranet_Global_Client__c();
        clientClient1.Selected_By__c=clientUser.Id;
        clientClient1.Selected_For_Client__c=clientAcc.Id;
        clientClient1.Selected_Global_Clients__c=brokerAcc.Id+';'+clientAcc.Id;
        extClientList.add(clientClient1);
        
        insert extClientList;
        
        map_broker = new AccountToContactMap__c(AccountId__c=brokerAcc.id,Name=brokerContact.id);
        map_client = new AccountToContactMap__c(AccountId__c= clientAcc.id,Name=clientContact.id);
        
        Insert map_broker;
        Insert map_client;
        
        /////**** Added for R3 broker share CR
        agrSet2.agreement_set_type__c = 'Test data for broker share';
        agrSet2.shared_by__c = brokerAcc.ID;
        agrSet2.shared_with__c = brokerAcc.ID;
        agrSet2.status__c = 'Active';
        
        insert agrSet2;
        
        brokerShr4.Active__c = true;
        brokerShr4.Agreement_set_id__c = agrSet2.ID;
        brokerShr4.contract_external_id__c = String.valueOf(brokerContract.ID);
        brokerShr4.Contract_id__c = brokerContract.ID;
        brokerShr4.shared_to_client__c = true;
        
        insert brokerShr4;
        
        contactShr4.Broker_share_id__c = brokerShr4.id;
        contactShr4.ContactId__c = brokerUser.id;
        
        insert contactShr4;
        /////**** END
        
    }
    
    @isTest private static void testBrokerLogin(){
        createTestData();
        System.runAs(brokerUser){
            PEMEEnrollmentCtrl PEMEEnrollmentCntl=new PEMEEnrollmentCtrl();
            PEMEEnrollmentCntl.companyAddress='testAddr';
            PEMEEnrollmentCntl.Add1='add1';
            PEMEEnrollmentCntl.Add2='add2';
            PEMEEnrollmentCntl.Add3='add3';
            PEMEEnrollmentCntl.Add4='add4';
            PEMEEnrollmentCntl.city='tempCity';
            PEMEEnrollmentCntl.state='tempState';
            PEMEEnrollmentCntl.zip='tempZip';
            PEMEEnrollmentCntl.country='tempCountry';
            PEMEEnrollmentCntl.selectedClient = brokerAcc.id+'';
            PEMEEnrollmentCntl.loadData();
            PEMEEnrollmentCntl.populateContact();
            PEMEEnrollmentCntl.selectedClient = brokerAcc.id+'';
            //PEMEEnrollmentCntl.populateObject();
            PEMEEnrollmentCntl.selectedContact.add(brokerContact.name);
            PEMEEnrollmentCntl.getConName();
            PEMEEnrollmentCntl.getClientLst();
            //PEMEEnrollmentCntl.getObjectLst();
            PEMEEnrollmentCtrl.ManningAgent manAgent=new PEMEEnrollmentCtrl.ManningAgent();
            manAgent.companyName=String.valueOf(brokerAcc.ID);
            manAgent.contactPerson='TestContactPerson';
            manAgent.phone='4455336677';
            manAgent.email='abc@demo.com';
            manAgent.address1='adressDemo1';
            manAgent.address2='adressDemo2';
            manAgent.address3='adressDemo3';
            manAgent.address4='adressDemo4';
            manAgent.city='demoCity';
            manAgent.zip='112232';
            manAgent.state='stateDemo';
            manAgent.country='countryDemo';
            PEMEEnrollmentCntl.addAgent = new List<PEMEEnrollmentCtrl.ManningAgent>();
            PEMEEnrollmentCntl.addAgent.add(manAgent);
            PEMEEnrollmentCntl.getManningLst();
            PEMEEnrollmentCntl.getCName();
            PEMEEnrollmentCntl.cancel();
            PEMEEnrollmentCntl.addTextBox();
            PEMEEnrollmentCntl.removeTextBox();
            PEMEEnrollmentCntl.goBankdetailsNext();
            PEMEEnrollmentCntl.goPrevMannning();
            PEMEEnrollmentCntl.goReviewNext();
            PEMEEnrollmentCntl.goprevBank();
            PEMEEnrollmentCntl.selectedClient = brokerAcc.id+'';
            PEMEEnrollmentCntl.saveSubmit();
            PEMEEnrollmentCntl.debitAddress=false;
            PEMEEnrollmentCntl.saveSubmit();
            Test.setCurrentPageReference(new PageReference('Page.PEMEEnroll1'));
            System.currentPageReference().getParameters().put('debitDetailsLstPosParam', '0');
            System.currentPageReference().getParameters().put('debitAddressParam', 'true');
            PEMEEnrollmentCntl.showaddress();
            //PEMEEnrollmentCntl.strGlobalClient=new String();
            //PEMEEnrollmentCntl.strGlobalClient=brokerAcc.Id+';'+clientAcc.Id;
            PEMEEnrollmentCntl.fetchGlobalClients();
            PEMEEnrollmentCntl.strSelected=brokerAcc.Id+';'+clientAcc.Id;
            PEMEEnrollmentCntl.fetchSelectedClients();
            PageReference pg = PEMEEnrollmentCntl.refreshPage();
        }
    }
    
    @isTest private static void testClientLogin(){
        createTestData();
        System.runAs(clientUser)
        {
            PEMEEnrollmentCtrl PEMEEnrollmentCntl=new PEMEEnrollmentCtrl();
            PEMEEnrollmentCntl.companyAddress='testAddr';
            PEMEEnrollmentCntl.Add1='add1';
            PEMEEnrollmentCntl.Add2='add2';
            PEMEEnrollmentCntl.Add3='add3';
            PEMEEnrollmentCntl.Add4='add4';
            PEMEEnrollmentCntl.city='tempCity';
            PEMEEnrollmentCntl.state='tempState';
            PEMEEnrollmentCntl.zip='tempZip';
            PEMEEnrollmentCntl.country='tempCountry';
            PEMEEnrollmentCntl.selectedClient = clientAcc.id+'';
            PEMEEnrollmentCntl.loggedInContactId = ''+clientContact.id;
            PEMEEnrollmentCntl.loggedInAccountId = ''+clientAcc.id;
            PEMEEnrollmentCntl.loadData();
            PEMEEnrollmentCntl.populateContact();
            PEMEEnrollmentCntl.selectedClient = clientAcc.id+'';
            //PEMEEnrollmentCntl.populateObject();
            PEMEEnrollmentCntl.selectedContact.add(clientContact.name);
            PEMEEnrollmentCntl.getConName();
            PEMEEnrollmentCntl.getClientLst();
            //PEMEEnrollmentCntl.getObjectLst();
            PEMEEnrollmentCtrl.ManningAgent manAgent=new PEMEEnrollmentCtrl.ManningAgent();
            manAgent.companyName=String.valueOf(brokerAcc.ID);
            manAgent.contactPerson='TestContactPerson';
            manAgent.phone='4455336677';
            manAgent.email='abc@demo.com';
            manAgent.address1='adressDemo1';
            manAgent.address2='adressDemo2';
            manAgent.address3='adressDemo3';
            manAgent.address4='adressDemo4';
            manAgent.city='demoCity';
            manAgent.zip='112232';
            manAgent.state='stateDemo';
            manAgent.country='countryDemo';
            PEMEEnrollmentCntl.addAgent = new List<PEMEEnrollmentCtrl.ManningAgent>();
            PEMEEnrollmentCntl.addAgent.add(manAgent);
            PEMEEnrollmentCntl.getManningLst();
            PEMEEnrollmentCntl.getCName();
            PEMEEnrollmentCntl.cancel();
            PEMEEnrollmentCntl.addTextBox();
            PEMEEnrollmentCntl.removeTextBox();
            PEMEEnrollmentCntl.goBankdetailsNext();
            PEMEEnrollmentCntl.goPrevMannning();
            PEMEEnrollmentCntl.goReviewNext();
            PEMEEnrollmentCntl.goprevBank();
            PEMEEnrollmentCntl.selectedClient = clientAcc.id+'';
            PEMEEnrollmentCntl.saveSubmit();
            PEMEEnrollmentCntl.debitAddress=false;
            PEMEEnrollmentCntl.saveSubmit();
            Test.setCurrentPageReference(new PageReference('Page.PEMEEnroll1'));
            System.currentPageReference().getParameters().put('debitDetailsLstPosParam','0');
            System.currentPageReference().getParameters().put('debitAddressParam','true');
            System.currentPageReference().getParameters().put('rowIndexManningParam','0');
            System.currentPageReference().getParameters().put('rowIndexDebitParam','0');
            PEMEEnrollmentCntl.showaddress();
            //PEMEEnrollmentCntl.strGlobalClient=new String();
            //PEMEEnrollmentCntl.strGlobalClient=brokerAcc.Id+';'+clientAcc.Id;
            //PEMEEnrollmentCntl.fetchGlobalClients();
            PEMEEnrollmentCntl.strSelected=brokerAcc.Id+';'+clientAcc.Id;
            PEMEEnrollmentCntl.fetchSelectedClients();
            PEMEEnrollmentCntl.addManning();
            PEMEEnrollmentCntl.removeManning();
            PEMEEnrollmentCntl.getastectLst();
            PEMEEnrollmentCntl.goPrevManning();
            PEMEEnrollmentCntl.addDebit();
            PEMEEnrollmentCntl.removeDebit();
        }
    }
}