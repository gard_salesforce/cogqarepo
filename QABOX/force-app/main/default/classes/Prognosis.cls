public with sharing class Prognosis {
    /************************************************************************
     * Date: 14/08/2013
     * Author: Martin Gardner
     * Company: cDecisions Ltd.
     * Description: Toolkit class for creating Prognosis records
     * Modifications: [ENTER MODS HERE]
     ************************************************************************/
     public static final String OLIToken = 'OpportunityLineItem';
     public static final String OppToken = 'Opportunity';
     public static final String MetaToken = 'Meta'; 
     
     
     private static Map<String, Map<String,String>> progMap{
     	get{
     		
     		if(null==progMap){progMap = PrognosisMap.GetPrognosisMap(); System.Debug('** Prognosis Map = ' + progMap);return progMap;}
     		else{System.Debug('** Prognosis Map = ' + progMap);return progMap;}     		
     	}
     	set;
     } 
     public static List<String> OppFields {
     	get{
     		if(null==OppFields){
     			OppFields = new List<String>();
     			if(progMap.containsKey(OppToken)){
     				Set<String> oppFieldDedupe = new Set<String>();
     				oppFieldDedupe.addAll(progMap.get(OppToken).Values());
     				OppFields.addAll(oppFieldDedupe);
     			}
     		}
     		return OppFields;     		
     	}
     	set;
     }
     
     public static List<String> OLIFields {
     	get{
     		if(null==OLIFields){
     			OLIFields = new List<String>();
     			//OLIFields.addAll(Schema.SObjectType.Prognosis__c.fields.getMap().keySet());
     			//run through the prognosis map getting all the OLI Fields
     			if(progMap.containsKey(OLIToken)){
     				Set<String> oliFieldDedupe = new Set<String>();
     				oliFieldDedupe.addAll(progMap.get(OLIToken).Values()); 
     				OLIFields.addAll(oliFieldDedupe);
     			}
     			//else there are no OLI fields defined

     		}
     		return OLIFields;
     	}
     	set;
     }
    public static String OppQuery {
     	get{
     		if(null==OppQuery){
     			OppQuery = 'SELECT ' + String.join(OppFields, ',') + ' FROM Opportunity ';     			
     		}
     		return OppQuery;			
     	}    		
     	
     	set;
     }
     
     public static String OppSubQuery {
     	get{
     		if(null==OppSubQuery){
     			List<String> subQueryFields = new List<String>();
     			for(String aField : OppFields){
     				subQueryFields.add(OppToken + '.'+aField);
     			}
     			OppSubQuery = String.join(subQueryFields, ',');     			
     		}
     		return OppSubQuery;			
     	}    		
     	
     	set;
     }	
     public static String OLIQuery {
     	get{
     		if(null==OLIQuery){
     			OLIQuery = 'SELECT ' + String.join(OLIFields, ',') + ', '+ Prognosis.OppSubQuery +' FROM OpportunityLineItem ';
     		}
     		return OLIQuery;
     	}
     	set;	
     }
     
     public static Prognosis__c getPrognosis(OpportunityLineItem oli){
     	//create a prognois record from an oli
     	Prognosis__c aProg = new Prognosis__c();
     	Opportunity parentOpp = new Opportunity();
     	String oquery = Prognosis.OppQuery + ' WHERE Id=\'' + oli.OpportunityId +'\' LIMIT 1';
     	String olquery = Prognosis.OLIQuery + ' WHERE Id=\'' + oli.Id + '\' LIMIT 1';
     	
     	System.Debug('Query = ' + olquery);
     	//look for the opportunity if not found query for it.
     	try{
	     	if(null==oli.Opportunity){
	     		System.Debug('oli.Opportunity is null : Query = ' + olquery);     		
	     		parentOpp = Database.query(oquery);
	     	}else{
	     		parentOpp = oli.Opportunity;
	     	}
     	}catch(System.SObjectException e){
     		System.Debug('Exception found Query = ' + oquery);
     		parentOpp = Database.query(oquery);     		
     	}
     	
     	//Add metadata
     	aProg = Prognosis.addMetaData(aProg); 
     	
     	//Add snapshot facts    
     	//Parent Opp fields
     	try{
     		aProg = Prognosis.addOppFields(aProg, parentOpp);
     	}catch(System.SObjectException e){
     		System.Debug('Exception found Query = ' + oquery);
     		parentOpp = Database.query(oquery);
     		aProg = Prognosis.addOppFields(aProg, parentOpp);     		
     	}
	 	
	 	//OLI Fields
	 	try{
			aProg = Prognosis.addOLIFields(aProg, oli);
	 	}catch(System.SObjectException e){
	 		System.Debug('Exception Query = ' + olquery);
     		oli = Database.query(olquery);
     		aProg = Prognosis.addOLIFields(aProg, oli);     		
     	}
     	
     	return aProg;
     }
     
     public static list<Prognosis__c> getPrognosisList(List<OpportunityLineItem> olis){
     	List<Prognosis__c> proglist = new List<Prognosis__c>();
     	
     	//Check that each oli includes the parent opp info that we need
     	//if one of them doesn't then requery
     	boolean requery = false;
     /*	try{
     		for(OpportunityLineItem anOLI:olis){
     			if(null==oli.Opportunity){ 
     				requery=true;
     				break;
     			}
     		}
     	}catch(System.SObjectException e){
     		
     	}
     */	
     	for(OpportunityLineItem anOLI:olis){
     		proglist.add(Prognosis.getPrognosis(anOLI));
     	}
     	return proglist;
     }
     
     public static Prognosis__c addOppFields(Prognosis__c prog, Opportunity Opp){
     //		prog.Budget_Year__c						= Opp.Budget_Year__c;//get from opp
	//	 	prog.Opportunity__c						= Opp.Name;
	//	 	prog.OwnerId							= Opp.OwnerId;
	//	 	prog.Sales_Stage__c						= Opp.StageName;
	//	 	prog.Type_of_Business__c	 			= Opp.Type;
	//	 	prog.Premium_Development_pc__c			= Opp.Premium_Development__c;
	
			if(progMap.containsKey(OppToken)){
				for(String aProgField : progMap.get(OppToken).keySet()){
					prog.put(aProgField, opp.get(progMap.get(OppToken).get(aProgField)));
				}
			}
		 	return prog;
     }
     
     public static Prognosis__c addOLIFields(Prognosis__c prog, OpportunityLineItem oli){
     /*	prog.Actual_Premium__c 						= oli.Actual_Premium__c;     	
     	prog.Comments__c							= oli.Budget_Comments__c;
     	prog.CurrencyIsoCode						= oli.CurrencyIsoCode;
     	prog.EPI__c									= oli.Budget_Premium__c;
     	prog.Expected_Individual_Increase__c		= oli.Record_Adjustment__c;
     	prog.GI__c									= oli.General_Increase__c;
     	prog.LR__c									= oli.LR__c;
     	prog.OpportunityId__c						= oli.OpportunityId;     	
     	prog.Opportunity_Id__c						= oli.OpportunityId;     	
     	prog.Product__c								= oli.Product_Name__c;
     	prog.Prognosis_Renewable_Premium__c			= oli.Renewal_Premium__c;
     	prog.RA__c									= oli.RA__c;
     	prog.Renewal_Probability__c					= oli.Renewable_Probability__c;
     	prog.Variance__c							= oli.Variance__c;
     	prog.Volume_Adjustment__c					= oli.Volume_Adjustment__c;
     	prog.Premium_Development__c     			= oli.Premium_Development__c;
     	prog.Prognosis_Volume__c					= oli.Prognosis_Volume__c;
     */	
     	if(progMap.containsKey(OLIToken)){
				for(String aProgField : progMap.get(OLIToken).keySet()){
					prog.put(aProgField, oli.get(progMap.get(OLIToken).get(aProgField)));
				}
			}
     	
     	return prog;
     }
     
     public static Prognosis__c addMetaData(Prognosis__c prog){
		//Snapshot meta data
		if(progMap.containsKey(MetaToken)){
			for(String aProgField : progMap.get(MetaToken).keySet()){
					prog.put(aProgField, progMap.get(MetaToken).get(aProgField));
			}
		}
     	//prog.Type__c								= 'User';
     	prog.Date__c								= System.today();
     	prog.Time__c								= System.now();     
     	return prog;	
     }
}