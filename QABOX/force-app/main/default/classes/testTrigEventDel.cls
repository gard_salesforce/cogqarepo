/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testTrigEventDel {
    
    private class EventDeleteException extends Exception{}

    static testMethod void testTrigEventDel_Published(){
       
        //create an account record
        Account acc = new Account(  Name='APEXTESTACC001',
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',
                                    Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id,
                                    Market_Area__c = TestDataGenerator.getMarketArea().Id
                                );
        insert acc;
        String strAccId = acc.Id;
        
        //system.assertNotEquals(null, e.Id);
        Contact con = new Contact();
        con.LastName='Sutherland';
        con.AccountId = strAccId;
        insert (con);
        String strConId = con.Id;
        
        //create opportunity
        String strRecId = [SELECT Id FROM RecordType WHERE Name='Marine' AND sObjectType='Opportunity' LIMIT 1].Id;
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = strRecId;
        opp.Name = 'APEXTESTOPP001';
        opp.AccountId = strAccId;
        opp.StageName = 'Renewable Opportunity';
        opp.Type = 'New Business';
        opp.Sales_Channel__c = 'Direct';
        opp.CloseDate = Date.Today();
        insert opp;
        String strOppId = opp.Id;        
        
        Event evt = new Event(  Subject='TESTCOVERAGE2', 
                                StartDateTime=datetime.now().addDays(1), 
                                EndDateTime=datetime.now().addDays(2),
                                WhoId = strConId,
                                WhatId = strOppId);
        insert evt;
        Event e = [select id from Event where subject='TESTCOVERAGE2' order by CreatedDate  desc limit 1];        
        //Event e = new Event(Subject = 'test');
        //e.StartDateTime = datetime.now().addDays(1);
        //e.EndDateTime = datetime.now().adddays(2);
        //insert(e);
        system.assertNotEquals(null, e.Id);
        //EventAttendee ea1 = new EventAttendee(EventId = e.id);
        //ea1.EventId = e.id;
        //ea1.AttendeeId = con.Id;
        //ea1.Status = 'New';
        //insert(ea1);
        Meeting_Minute__c a = new Meeting_Minute__c(event_id__c = e.Id);
        insert(a);
        system.assertNotEquals(null, a.Id);
        Account_Meeting_Minute__c accmm = new Account_Meeting_Minute__c(Account__c = strAccId ,Meeting_Minute__c = a.Id );
        insert accmm;
        
        // get the list of standard attendees
        List<EventAttendee> stdAtt = [Select e.Status, 
                                          e.Id, 
                                          e.EventId, 
                                          e.AttendeeId 
                                          From EventAttendee e
                                          Where e.EventId = : e.Id ];
        System.debug('>>Attendees>>' + stdAtt);
        
        List <Meeting_Attendee__c> maToAdd = new List<Meeting_Attendee__c>();
        for(EventAttendee ea : stdAtt){
            string maType = 'Contact';
            if (maType == 'User'){
                Meeting_Attendee__c ma = new Meeting_Attendee__c(User__c=ea.attendeeId, Type__c=maType, Meeting_Minute__c=a.Id, Meeting_Attendee__c = ea.AttendeeId, Event_Id__c=ea.EventId);
                maToAdd.add(ma);
            }else if (maType == 'Contact'){
                Meeting_Attendee__c ma = new Meeting_Attendee__c(Contact__c=ea.attendeeId, Type__c=maType, Meeting_Minute__c=a.Id, Meeting_Attendee__c = ea.AttendeeId, Event_Id__c=ea.EventId);
                maToAdd.add(ma);
            }else if (maType == 'Lead'){
                Meeting_Attendee__c ma = new Meeting_Attendee__c(Lead__c=ea.attendeeId, Type__c=maType, Meeting_Minute__c=a.Id, Meeting_Attendee__c = ea.AttendeeId, Event_Id__c=ea.EventId);
                maToAdd.add(ma);
            } else{
                Meeting_Attendee__c ma = new Meeting_Attendee__c(Type__c=maType, Meeting_Minute__c=a.Id, Meeting_Attendee__c = ea.AttendeeId, Event_Id__c=ea.EventId);
                maToAdd.add(ma);
            }
        }
        if(stdAtt==null || stdAtt.Size() == 0){
            Meeting_Attendee__c ma = new Meeting_Attendee__c(Contact__c=con.Id, Type__c='Contact', Meeting_Minute__c=a.Id, Meeting_Attendee__c = con.Id, Event_Id__c=e.id);
                maToAdd.add(ma);
        }
        insert (maToAdd);
        
        //SOC 1/5/13 - Added a check to see if custom settings already exist, otherwise test failed
        List<Meeting_Minutes_Config__c> mmcfg = new List<Meeting_Minutes_Config__c>(); 
            
        List<Meeting_Minutes_Config__c> existingMmcfg = [SELECT Name, Value__c FROM Meeting_Minutes_Config__c WHERE Name='emailContacts'];
        if(existingMmcfg.size() == 0)
        {
            mmcfg.add(new Meeting_Minutes_Config__c(Name='emailContacts', Value__c='1'));
        }
        existingMmcfg = [SELECT Name, Value__c FROM Meeting_Minutes_Config__c WHERE Name='postToChatter'];
        if(existingMmcfg.size() == 0)
        {
            mmcfg.add(new Meeting_Minutes_Config__c(Name='postToChatter', Value__c='1'));
        }   
        insert mmcfg;
        
        ApexPages.currentPage().getParameters().put('eventId', e.id);
        ApexPages.currentPage().getParameters().put('Id', a.id);
        ApexPages.currentPage().getParameters().put('id', a.id);
            
        ApexPages.StandardController sc = new ApexPages.Standardcontroller(a);
        MeetingMinutesExtensions mme = new MeetingMinutesExtensions(sc);
        
        pageReference pr = mme.checkStatus();
        pr = mme.saveOnly();
        pr = mme.cancel();
        
        mme.searchTerm = 'TEST';
        pr = mme.attendeeSearch();
        pr = mme.clearSearch();
        Contact c = new Contact(lastname='Test', accountId = TestDataGenerator.getClientAccount().id);
        insert c;
        //mme.selectedContacts = c.id;
        pr = mme.addSelected();

        
        pr = mme.distribute();
        pr = mme.checkStatus();

        
        //Attempt to delete the event now that it has published meeting minutes
        try{
            delete(e);
            throw new EventDeleteException('Test class was able to delete an event that had published meeting minutes.');
        }catch(Exception exp){
            Boolean expectedExceptionThrown =  exp.getMessage().contains('Published meeting minutes exist for this Event. You may not delete this record.') ? true : false;
            //System.AssertEquals(expectedExceptionThrown, true);
        }
        
    }    
    static testMethod void testTrigEventDel_Attendees(){
       
        //create an account record
        Account acc = new Account(  Name='APEXTESTACC001',
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',
                                    Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id,
                                    Market_Area__c = TestDataGenerator.getMarketArea().Id
                                );
        insert acc;
        String strAccId = acc.Id;
        
        //system.assertNotEquals(null, e.Id);
        Contact con = new Contact();
        con.LastName='Sutherland';
        con.AccountId = strAccId;
        insert (con);
        String strConId = con.Id;
        
        //create opportunity
        String strRecId = [SELECT Id FROM RecordType WHERE Name='Marine' AND sObjectType='Opportunity' LIMIT 1].Id;
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = strRecId;
        opp.Name = 'APEXTESTOPP001';
        opp.AccountId = strAccId;
        opp.StageName = 'Renewable Opportunity';
        opp.Type = 'New Business';
        opp.Sales_Channel__c = 'Direct';
        opp.CloseDate = Date.Today();
        insert opp;
        String strOppId = opp.Id;        
        
        Event evt = new Event(  Subject='TESTCOVERAGEtestTrigEventDel_Attendees', 
                                StartDateTime=datetime.now().addDays(1), 
                                EndDateTime=datetime.now().addDays(2),
                                WhoId = strConId,
                                WhatId = strOppId);
        insert evt;
        Event e = [select id from Event where subject='TESTCOVERAGEtestTrigEventDel_Attendees' order by CreatedDate  desc limit 1];  
        system.debug('--> Event : ' + e);      
        //Event e = new Event(Subject = 'test');
        //e.StartDateTime = datetime.now().addDays(1);
        //e.EndDateTime = datetime.now().adddays(2);
        //insert(e);
        system.assertNotEquals(null, e.Id);
        //EventAttendee ea1 = new EventAttendee(EventId = e.id);
        //ea1.EventId = e.id;
        //ea1.AttendeeId = con.Id;
        //ea1.Status = 'New';
        //insert(ea1);
        Meeting_Minute__c a = new Meeting_Minute__c(event_id__c = e.Id);
        insert(a);
        system.assertNotEquals(null, a.Id);
        Account_Meeting_Minute__c accmm = new Account_Meeting_Minute__c(Account__c = strAccId ,Meeting_Minute__c = a.Id );
        insert accmm;
        
        // get the list of standard attendees
        List<EventAttendee> stdAtt = [Select e.Status, 
                                          e.Id, 
                                          e.EventId, 
                                          e.AttendeeId 
                                          From EventAttendee e
                                          Where e.EventId = : e.Id ];
        System.debug('>>Attendees>>' + stdAtt);
        
        List <Meeting_Attendee__c> maToAdd = new List<Meeting_Attendee__c>();
        for(EventAttendee ea : stdAtt){
            string maType = 'Contact';
            if (maType == 'User'){
                Meeting_Attendee__c ma = new Meeting_Attendee__c(User__c=ea.attendeeId, Type__c=maType, Meeting_Minute__c=a.Id, Meeting_Attendee__c = ea.AttendeeId, Event_Id__c=ea.EventId);
                maToAdd.add(ma);
            }else if (maType == 'Contact'){
                Meeting_Attendee__c ma = new Meeting_Attendee__c(Contact__c=ea.attendeeId, Type__c=maType, Meeting_Minute__c=a.Id, Meeting_Attendee__c = ea.AttendeeId, Event_Id__c=ea.EventId);
                maToAdd.add(ma);
            }else if (maType == 'Lead'){
                Meeting_Attendee__c ma = new Meeting_Attendee__c(Lead__c=ea.attendeeId, Type__c=maType, Meeting_Minute__c=a.Id, Meeting_Attendee__c = ea.AttendeeId, Event_Id__c=ea.EventId);
                maToAdd.add(ma);
            } else{
                Meeting_Attendee__c ma = new Meeting_Attendee__c(Type__c=maType, Meeting_Minute__c=a.Id, Meeting_Attendee__c = ea.AttendeeId, Event_Id__c=ea.EventId);
                maToAdd.add(ma);
            }
        }
        if(stdAtt==null || stdAtt.Size() == 0){
            Meeting_Attendee__c ma = new Meeting_Attendee__c(Contact__c=con.Id, Type__c='Contact', Meeting_Minute__c=a.Id, Meeting_Attendee__c = con.Id, Event_Id__c=e.id);
                maToAdd.add(ma);
        }
        insert (maToAdd);
        
        //SOC 1/5/13 - Added a check to see if custom settings already exist, otherwise test failed
        List<Meeting_Minutes_Config__c> mmcfg = new List<Meeting_Minutes_Config__c>(); 
            
        List<Meeting_Minutes_Config__c> existingMmcfg = [SELECT Name, Value__c FROM Meeting_Minutes_Config__c WHERE Name='emailContacts'];
        if(existingMmcfg.size() == 0)
        {
            mmcfg.add(new Meeting_Minutes_Config__c(Name='emailContacts', Value__c='1'));
        }
        existingMmcfg = [SELECT Name, Value__c FROM Meeting_Minutes_Config__c WHERE Name='postToChatter'];
        if(existingMmcfg.size() == 0)
        {
            mmcfg.add(new Meeting_Minutes_Config__c(Name='postToChatter', Value__c='1'));
        }   
        insert mmcfg;
        
        ApexPages.currentPage().getParameters().put('eventId', e.id);
        ApexPages.currentPage().getParameters().put('Id', a.id);
        ApexPages.currentPage().getParameters().put('id', a.id);
            
        ApexPages.StandardController sc = new ApexPages.Standardcontroller(a);
        MeetingMinutesExtensions mme = new MeetingMinutesExtensions(sc);
        
        pageReference pr = mme.checkStatus();
        pr = mme.saveOnly();
        pr = mme.cancel();
        
        Contact c = new Contact(lastname='Test', AccountId = TestDataGenerator.getClientAccount().id);
        Contact c2 = new Contact(lastname='Test2');     
        insert c;
        
        List<Meeting_Attendee__c> newAttendees = new List<Meeting_Attendee__c>();
        

                
        Meeting_Attendee__c ma = new Meeting_Attendee__c(Type__c = 'Contact', Contact__c = c.Id, Meeting_Minute__c = a.ID, Event_Id__c = e.Id, Attended__c = true);
        Meeting_Attendee__c ma2 = new Meeting_Attendee__c(Type__c = 'Contact', Contact__c = c2.Id, Meeting_Minute__c = a.ID, Event_Id__c = e.Id, Attended__c = true);
        
        newAttendees.add(ma);

        insert(newAttendees);
        
        Meeting_Minute__c thisMinute = [SELECT id, event_id__c, Attendee_Count__c FROM Meeting_Minute__c Where Id =:a.Id];
       

        //Don't distribute this time
        //pr = mme.distribute();
        pr = mme.checkStatus();
       
        system.debug('--> thisMinute.Attendee_Count__c =' +thisMinute.Attendee_Count__c);
        List<EventAttendee> evtAtt = new List<EventAttendee>();
        evtAtt = [SELECT AttendeeId FROM EventAttendee WHERE EventId = :evt.Id AND AttendeeId != :evt.OwnerId];
        if(evtAtt != null)
        {
            system.debug('--> evtAtt.size() = ' + evtAtt.size());                   
            if(evtAtt.size()>0){
                //Attempt to delete the event now that it has no published meeting minutes but does have some attendees
                try{
                    delete(e);
                    throw new EventDeleteException('Test class was able to delete an event that had a meeeting minute with attendees.');
                }catch(Exception exp){
                    System.debug('---> Exception Message = :' + exp.getMessage());
                    Boolean expectedExceptionThrown =  exp.getMessage().contains('Test class was able to delete an event that had a meeeting minute with attendees.') ? false : true;
                    //System.AssertEquals(true, expectedExceptionThrown);
                }
            }
        }
        
    }   
    static testMethod void testTrigEventDel_Pass(){
       
        //create an account record
        Account acc = new Account(  Name='APEXTESTACC001',
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',
                                    Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id,
                                    Market_Area__c = TestDataGenerator.getMarketArea().Id
                                );
        insert acc;
        String strAccId = acc.Id;
        
        //system.assertNotEquals(null, e.Id);
        Contact con = new Contact();
        con.LastName='Sutherland';
        con.AccountId = strAccId;
        insert (con);
        String strConId = con.Id;
        
        //create opportunity
        String strRecId = [SELECT Id FROM RecordType WHERE Name='Marine' AND sObjectType='Opportunity' LIMIT 1].Id;
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = strRecId;
        opp.Name = 'APEXTESTOPP001';
        opp.AccountId = strAccId;
        opp.StageName = 'Renewable Opportunity';
        opp.Type = 'New Business';
        opp.Sales_Channel__c = 'Direct';
        opp.CloseDate = Date.Today();
        insert opp;
        String strOppId = opp.Id;        
        
        Event evt = new Event(  Subject='TESTCOVERAGE', 
                                StartDateTime=datetime.now().addDays(1), 
                                EndDateTime=datetime.now().addDays(2),
                                WhoId = strConId,
                                WhatId = strOppId);
        insert evt;
        Event e = [select id from Event where subject='TESTCOVERAGE' order by CreatedDate  desc limit 1];        

        system.assertNotEquals(null, e.Id);

        Meeting_Minute__c a = new Meeting_Minute__c(event_id__c = e.Id);
        insert(a);
        system.assertNotEquals(null, a.Id);       
        
        //Attempt to delete the event now that it has no published meeting minutes and no attendees
          delete(e);        
    }         
}