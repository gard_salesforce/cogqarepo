public class FollowerService{
    //Constants
    private static final Integer pageSize = 10;//how many records per pgae
    
    //static variables
    private static PageWrapper thisPage;
    private static Id currentUserId;
    private static Map<Id,FollowerWrapper> accountFollowers;
    
    //Service methods
    //returns "pageNumber" count of FollowerWrappers in PageWrapper of the "parentId" passed
    @auraEnabled
    public static PageWrapper getPageWrappers(Id parentId,Integer pageNumber){
        PageWrapper retPage;//to be returned
        thisPage = new PageWrapper();
        thisPage.pageNumber = pageNumber;
        createPageFor(parentId,pageNumber);
        retPage = thisPage;
        return retPage;
    }
    
    //follow/unFollow Users of this user
    @AuraEnabled public static void followUnFollowUserServer(String subjectId,String action){
        try{
            System.debug('FollowerService.followUnFollowUser STARTS');
            System.debug('action / subjectId - '+action+'/'+subjectId);
            if(action.containsIgnoreCase('unfollow')){
                ConnectApi.Subscription subscription = ConnectApi.ChatterUsers.follow(null, UserInfo.getUserId(), subjectId);
                if(!String.isEmpty(subscription.id)){
                    ConnectApi.Chatter.deleteSubscription(null,subscription.id);
                    System.debug('unFollow Succeded');
                }
            }else if(action.containsIgnoreCase('follow')){
                ConnectApi.ChatterUsers.follow(null, UserInfo.getUserId(), Id.valueOf(subjectId));
                System.debug('Follow Succeded');
            }
        }catch(Exception ex){
            System.debug('Exception caught @ FollowerService.setunfollow - '+ex);
        }
        System.debug('FollowerService.followUnFollowUser STARTS');
    }
    
    private static void createPageFor(Id parentId,Integer pageNumber){
        try{
            getFollowers(parentId);
            List<FollowerWrapper> retFollowerWrapper = accountFollowers.values();
            retFollowerWrapper.sort();
            thisPage.followers = new List<FollowerWrapper>();
            thisPage.followers.addAll(retFollowerWrapper);
            //System.debug('FollowerList.getFollowerWrappers returning - '+retFollowerWrapper);
        }catch(Exception ex){
            System.debug('Exception caught @ FollowerService.getFollowerWrappers - '+ex);
        }
    }
    
    //Private methods
    //To list who follow "this" account
    private static void getFollowers(Id parentId){//should be an accountId
        try{
            accountFollowers = new Map<Id,FollowerWrapper>();
            Map<Id,User> subscriberUserMap = new Map<Id,User>();
            FollowerWRapper followerWrapper;
            ConnectApi.FollowerPage fp;
            Set<Id> subscriberIds = new Set<Id>();
            
            //get pre-defined number(pageSize) of records with given offset(thisPage.pageNumber)
            fp = ConnectAPI.Chatter.getFollowers(null, parentId,thisPage.pageNumber,pageSize);
            for (ConnectAPI.Subscription s: fp.followers){
                followerWrapper = new FollowerWrapper((ConnectApi.User)s.subscriber);
                accountFollowers.put(s.Subscriber.Id,followerWrapper);
                subscriberIds.add(s.Subscriber.Id);
            }
            for(User subscriberUser : Database.query('SELECT id,fullPhotoURL FROM User WHERE Id IN:subscriberIds')){
                followerWrapper = accountFollowers.get(subscriberUser.Id);
                followerWrapper.photoURL = subscriberUser.fullPhotoUrl != null ? subscriberUser.fullPhotoUrl : '';
            }
            
            thisPage.hasNext = fp.nextPageUrl != null ? true : false;
            thisPage.hasPrevious = fp.previousPageUrl != null ? true : false;
            getFollowings();
        }catch(Exception ex){
            System.debug('Exception caught @ FollowerService.getFollowers - '+ex.getMessage());
            throw ex;
        }
    }
    
    //whom "this" user follows
    private static void getFollowings(){
        try{
            FollowerWrapper followerWrapper;
            FollowerWrapper followWrapper;
            ConnectApi.FollowingPage fp;
            Integer pageNumber = 0;
            
            //get all pages in this
            do{
                fp = ConnectApi.ChatterUsers.getFollowings(null,UserInfo.getUserId(),pageNumber);//,thispage.pageNumber,pageSize
                for(ConnectApi.Subscription s : fp.following){
                    followerWrapper = accountFollowers.get(s.subject.Id);
                    if(followerWrapper != null){//if the users that follows parentAccount are followed by currentUuser
                        followerWrapper.currentUserFollows = true;
                    }
                }
                pageNumber++;
            }while(fp.nextPageUrl != null);
        }catch(Exception ex){
            System.debug('Exception caught @ FollowerService.getFollowers - '+ex.getMessage());
            throw ex;
        }
    }
    
    //Inner Wrapper Classes
    //Represents Each User that follows parentAccount
    //And relationship with currentLoggedInUser
    public class FollowerWrapper implements Comparable{
        @AuraEnabled public ConnectApi.User followerUser;//the user that follows parentAccount
        @AuraEnabled public Boolean currentUserFollows;//if currentLoggedInUser follows this followerUser
        @AuraEnabled public Boolean self;//if currentLoggedInUser is this followerUser
        @AuraEnabled public String photoURL;//to show avatar
        
        public FollowerWrapper(ConnectApi.User followerUser){
            this.followerUser = followerUser;
            this.currentUserFollows = false;
            if(this.followerUser.id == UserInfo.getUserId()) this.self = true;
            else this.self = false;
            photoURL = '';
        }
        
        public Integer compareTo(Object that){
            FollowerWrapper thatFollowerWrapper = (FollowerWrapper)that;
            return (this.followerUser.name).compareTo(thatFollowerWrapper.followerUser.name);
        }
    }
    
    //Represents one page on the client side
    public class pageWrapper{
        @AuraEnabled public Integer pageNumber;
        @AuraEnabled public Boolean hasPrevious;
        @AuraEnabled public Boolean hasNext;
        @AuraEnabled public List<FollowerWrapper> followers;
    }
    
}