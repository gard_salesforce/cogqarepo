/*************************************************************************************************
    Author      : Martin Gardner
    Company     : cDecisions
    Date Created: 
    Description : This class provides tests the ContactToolkit class
    Modified    :   
***************************************************************************************************/
@isTest
private class ContactToolkitTest {

    @isTest(SeeAllData = true)
    static void testContactToolkit() {
        //Setup test data
        
        Map<Id, RecordType> testRecTypes = ContactToolkit.ContactRecordTypes;
        List<Schema.PicklistEntry> testContactTypes = ContactToolKit.ContactTypes;
        
        //Create accounts 
        SetupTestData.setupAccountData();
        
        //Create contacts
        SetupTestData.setupContactData();
        
        SetupTestData.insertContactData();
        
        ContactToolKit.checkForAccountRecordType(SetupTestData.testCon);
        ContactToolKit.UseContactTypes = false;
        ContactToolKit.checkForAccountRecordType(SetupTestData.testCon);
        ContactToolKit.checkForAccountRecordType(SetupTestData.testCon[0]);
        
    }
}