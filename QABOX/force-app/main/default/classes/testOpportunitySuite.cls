@isTest
private class testOpportunitySuite {
    public static Account acc;
    public static String strRecId;
    
    static TestMethod void testQuoteAccepted() {
        //### create test records ###
        strRecId = [SELECT Id FROM RecordType WHERE Name='Marine' AND sObjectType='Opportunity' LIMIT 1].Id;
        //create Account
         acc = TestDataGenerator.getApprovedMemberAccount();//new Account(Name='APEXTESTACC001', Membership_Status__c = 'Approved');
        //insert acc;
        String strAccId = acc.Id;
        //create opportunity
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = strRecId;
        opp.Name = 'APEXTESTOPP001';
        opp.AccountId = strAccId;
        opp.StageName = 'Risk Evaluation';//Renewable Opportunity
        opp.Type = 'New Business';        
        opp.Business_Type__c = 'MOUs';
        opp.Approval_Criteria__c = 'Self Approval';
        opp.Sales_Channel__c = 'Direct';
        opp.CloseDate = Date.Today();
        opp.Confirm_not_on_sanction_list__c = true;
        opp.Amount = 100;
        
        
        insert opp;
        
        //opp.StageName = '';
        //update opp;
        Opportunity_Checklist__c checklist = new Opportunity_Checklist__c(Opportunity__c = opp.id, Checklist_Completed__c = true);
        insert checklist;
        
        
        
        String strOppId = opp.Id;
        //create quote
        Quote qt = new Quote(Name='APEXTESTQTE001', OpportunityId=strOppId);
        insert qt;
        String strQuoteId = qt.Id;
        
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting Request for Approval');
        req1.setObjectId(opp.Id);
        //Submit the approval request for the opportunity
        Approval.ProcessResult result = Approval.process(req1);
        //Verify the result
        System.assert(result.isSuccess());
        System.assertEquals('Pending', result.getInstanceStatus(),
                            'Instance Status' + result.getInstanceStatus());
        
        //approve the submitted request
        //first get the id of the newly created item
        List<Id> newWorkItemIds = result.getNewWorkitemIds();
        
        //Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest req2= new Approval.ProcessWorkitemRequest();
        req2.setComments('Approving request.');
        req2.setAction('Approve');
        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        
        //Use the id from the newly created item to specify the item to be worked
        req2.setWorkitemId(newWorkItemIds.get(0));
        Gard_RecursiveBlocker.blocker = false;
        //Submit the request for approval
        Approval.ProcessResult result2 = Approval.process(req2);
        
        //Verify the resutls
        System.assert(result2.isSuccess(), 'Result Status:' + result2.isSuccess());
        
        System.assertEquals('Approved', result2.getInstanceStatus(),
                            'Instance Status'+result2.getInstanceStatus());                 
        
        Test.startTest();
            System.Debug('*** Opportunity Before= ' + opp);
            qt.Status = 'Accepted';
            update qt;
            System.Debug('*** Opportunity After= ' + opp);
        
        
        opp = [SELECT Id, Name, StageName FROM Opportunity WHERE Id=:strOppId];
        System.assertEquals('Closed Won', opp.StageName);
        opp.StageName = 'Closed Lost';
        update opp;
        opp.StageName = 'Risk Evaluation';
        update opp;
        Test.stopTest();
    }
}