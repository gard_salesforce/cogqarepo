public class DocumentResponseWrapper{  
    public Boolean Result{get;set;}
    public integer Count{get;set;}
    public Boolean MoreResultsAvailable{get;set;} 
    public integer ObjectType{get;set;}
    public List<String> ObjectTypeAlias{get;set;}
    public List<ResultObject> ResultObjects{get;set;}
    
    //SF-3859
    public List<List<ResultObject>> pagifiedResultObects{get;set;}//each inner list represents a page of records
    
    public void pagify(List<String> selectedObjs,List<String> selectedDocTypes,String docName){
        if(ResultObjects == null) return;
        
        List<String> objectsToFilter = new List<String>();
        List<String> docTypesToFilter = new List<String>();
        if(selectedObjs != null){
            for(String selectedObj : selectedObjs){
                System.debug('selectedObj - '+selectedObj);
                objectsToFilter.add(selectedObj.trim());
            }
        }
        if(selectedDocTypes != null){
            for(String selectedDocType : selectedDocTypes){
                System.debug('selectedDocType - '+selectedDocType);
                docTypesToFilter.add(selectedDocType.trim());
            }
        }
        
        System.debug('docName - '+docName);
        
        Integer PAGE_SIZE = 10;
        //System.debug('DocumentResponseWrapper.pagify() Called');
        System.debug('DocumentResponseWrapper.pagify() ResultObjects.size() : '+ResultObjects.size());
        pagifiedResultObects = new List<List<ResultObject>>();
        Integer aggregateRecords = 0;
        while(aggregateRecords < ResultObjects.size()){
            List<ResultObject> innerROs = new List<ResultObject>();
            System.debug('innerROs.size() / PAGE_SIZE : '+(innerROs.size() + '/' + PAGE_SIZE));
            System.debug('innerROs.size() < PAGE_SIZE : '+(innerROs.size() < PAGE_SIZE));
            
            while(innerROs.size() < PAGE_SIZE && aggregateRecords < ResultObjects.size()){
                DocumentResponseWrapper.ResultObject docResWrap_ResObj = ResultObjects.get(aggregateRecords);
                System.debug('this record - '+docResWrap_ResObj.ObjectProperties.pd_objectnameondocument+'/'+docResWrap_ResObj.ObjectProperties.pd_certificatetype+'/'+docResWrap_ResObj.ObjectProperties.pd_policydocumenttype+'/'+docResWrap_ResObj.ObjectProperties.name);
                if(
                    (objectsToFilter.isEmpty() || objectsToFilter.contains(docResWrap_ResObj.ObjectProperties.pd_objectnameondocument))
                    && (docTypesToFilter.isEmpty() || docTypesToFilter.contains(docResWrap_ResObj.ObjectProperties.pd_certificatetype) || docTypesToFilter.contains(docResWrap_ResObj.ObjectProperties.pd_policydocumenttype))
                    && (String.isBlank(docName) || docName.trim().containsIgnoreCase(docResWrap_ResObj.ObjectProperties.name.trim()) || docResWrap_ResObj.ObjectProperties.name.trim().containsIgnoreCase(docName.trim()))
                ){                    
                    System.debug('adding to List - '+docResWrap_ResObj.ObjectProperties);
                    innerROs.add(docResWrap_ResObj);
                    //System.debug('aggregateRecords : '+aggregateRecords);
                }
                aggregateRecords++;
            }
            pagifiedResultObects.add(innerROs);
        }
        System.debug('pagifiedResultObects.size() : '+pagifiedResultObects.size());
        if(pagifiedResultObects.size() > 0)System.debug('last list size pagifiedResultObects.size() : '+pagifiedResultObects.get(pagifiedResultObects.size()-1).size());
        System.debug('ResultObjects.size() : '+ResultObjects.size());
        
    }
    
    private static List<List<ResultObject>> pagify(List<ResultObject> linearResultObjects){
        List<List<ResultObject>> pagifiedResultObjects = new List<List<ResultObject>>();
        Integer PAGE_SIZE = 10;
        Integer aggregateRecords = 0;
        while(aggregateRecords < linearResultObjects.size()){
            List<ResultObject> resultObjectsPage = new List<ResultObject>();
            while(resultObjectsPage.size() < PAGE_SIZE && aggregateRecords < linearResultObjects.size()){
                resultObjectsPage.add(linearResultObjects.get(aggregateRecords));
                aggregateRecords++;
            }
            pagifiedResultObjects.add(resultObjectsPage);
        }
        
        
        return pagifiedResultObjects;
    }
    
    //SF-3859    
    public void sortIt(String sortCol,Boolean sortOrder){
        DocumentResponseWrapper.sortCol = sortCol;
        DocumentResponseWrapper.sortOrder = sortOrder;
        List<ResultObject> tempResObjs = new List<ResultObject>();
        for(List<ResultObject> resultObjectPage : this.pagifiedResultObects) tempResObjs.addAll(resultObjectPage);
        tempResObjs.sort();//sorts the single document list
        this.pagifiedResultObects = pagify(tempResObjs);
    }
    
    public static String sortCol;
    public static Boolean sortOrder;//true : ASC, false : DESC
    
    public class ResultObject implements Comparable{
        public String ObjectID {get;set;}
        public String ExternalID {get;set;}
        public String Name {get;set;}       
        public ObjectPropertie ObjectProperties{get;set;}
        
        public Integer compareTo(Object thatObj){
            ResultObject that = (ResultObject) thatObj;
            switch on DocumentResponseWrapper.sortCol{
                when 'name'{return DocumentResponseWrapper.sortOrder ? this.ObjectProperties.name.compareTo(that.ObjectProperties.name) : that.ObjectProperties.name.compareTo(this.ObjectProperties.name) ;}
                when 'polYear'{return DocumentResponseWrapper.sortOrder ? this.ObjectProperties.pd_years.compareTo(that.ObjectProperties.pd_years) : that.ObjectProperties.pd_years.compareTo(this.ObjectProperties.pd_years);}
                //when 'object'{return DocumentResponseWrapper.sortOrder ? this.ObjectProperties.pd_object.compareTo(that.ObjectProperties.pd_object) : that.ObjectProperties.pd_object.compareTo(this.ObjectProperties.pd_object);}
                when 'object'{return DocumentResponseWrapper.sortOrder ? this.ObjectProperties.pd_objectnameondocument.compareTo(that.ObjectProperties.pd_objectnameondocument) : that.ObjectProperties.pd_objectnameondocument.compareTo(this.ObjectProperties.pd_objectnameondocument);}
                when 'docTypeDesc'{ return DocumentResponseWrapper.sortOrder ? this.ObjectProperties.pd_docCertype.compareTo(that.ObjectProperties.pd_docCertype) : that.ObjectProperties.pd_docCertype.compareTo(this.ObjectProperties.pd_docCertype);}
                when 'validFrom' {return sortOrder ? compareToDateTime(this.ObjectProperties.pd_validfromDT,that.ObjectProperties.pd_validfromDT) : compareToDateTime(that.ObjectProperties.pd_validfromDT,this.ObjectProperties.pd_validfromDT);}
                when 'lastUpdate' {return sortOrder ? compareToDateTime(this.ObjectProperties.lastModifiedDT,that.ObjectProperties.lastModifiedDT) : compareToDateTime(that.ObjectProperties.lastModifiedDT,this.ObjectProperties.lastModifiedDT);}
                when else {return DocumentResponseWrapper.sortOrder ? this.ObjectProperties.name.compareTo(that.ObjectProperties.name) : that.ObjectProperties.name.compareTo(this.ObjectProperties.name);}
            }
        }
        
        private Integer compareToDateTime(DateTime thisDT,DateTime thatDT){
            return (
                thisDT.getTime() == thatDT.getTime() ? 0 : 
                thisDT.getTime() < thatDT.getTime() ? 1 :
                -1
            );
        }
    }
    
    public class ObjectPropertie{
        public String pd_productarea {get;set;}
        public String pd_years {get;set;}// ------Available in Response-----
        public String pd_policydocumenttype {get;set;}        
        public String pd_docCertype {get;set;}
        public String pd_covercovertypes {get;set;}
        public String pd_partner {get;set;} // ------Available in Response-----
        public String documentClass {get;set;}// ------Available in Response-----//100  pd_document_type 
        public String name {get;set;}// ------Available in Response-----//0 pd_doc_name
        public String pd_creator {get;set;}// ------Available in Response-----//Need to check 
        public String lastModified {get;set;}// ------Available in Response----- // pd_last_modified_date
        public String pd_sourcesystem {get;set;}// ------Available in Response-----
        public String pd_validfrom {get;set;}//------Available in Response-----
        public String pd_validto {get;set;}//------Available in Response-----
        public String pd_objectnameondocument {get;set;}// ------Available in Response-----
        //public String pd_client {get;set;}
        public Datetime pd_validfromDT {get;set;}// ------Available in Response-----
        public Datetime lastModifiedDT {get;set;}// ------Available in Response-----
        
        
        //Newly Added 
        public String pd_certificatetype {get;set;}// ------Available in Response-----
        public String pd_author {get;set;}// ------Available in Response-----
        public String pd_object {get;set;}// ------Available in Response-----
        
        //Newly added for P&I Blue card
        public String blue_Object_flag {get;set;}// ------Not Available in Response-----
        public String blue_Object_Port {get;set;}// ------Not Available in Response-----
        public String blue_Object_CallSign {get;set;}// ------Not Available in Response-----
        public String blue_Object_GUID{get;set;}// ------Not Available in Response-----
        
        
       public ObjectPropertie(string pd_productarea, string pd_years,string pd_policydocumenttype,string pd_covercovertypes, string documentClass, string name,string pd_creator,string lastModified, Datetime lastModifiedDT, string pd_sourcesystem, string pd_validfrom, Datetime pd_validfromDT, string pd_validto, string pd_objectnameondocument,string pd_client, string pd_certificatetype, string pd_author, string pd_object ){
             //System.debug('ObjectPropertie Construtor run - ');
            system.debug('first********');
            this.pd_productarea=pd_productarea;
            this.pd_years=pd_years;
            this.pd_policydocumenttype=pd_policydocumenttype;
            this.pd_certificatetype=pd_certificatetype;
            
            
            this.pd_covercovertypes=pd_covercovertypes; 
            this.documentClass=documentClass; 
            this.name=name;
            this.pd_creator=pd_creator;
            //System.debug('lastModified - ' + lastModified);
           
            //this.lastModifiedDT=createDateTime(lastModified);
            this.lastModifiedDT=lastModifiedDT;
            this.lastModified=lastModified;
            this.pd_sourcesystem=pd_sourcesystem;   
            System.debug('pd_validfrom - ' + pd_validfrom);
           
           
            //this.pd_validfromDT=createDateTime(string.valueOf(pd_validfrom));
            this.pd_validfromDT=pd_validfromDT;
          
            this.pd_validfrom=pd_validfrom;
            this.pd_validto=pd_validto;
            this.pd_objectnameondocument=pd_objectnameondocument;
            //this.pd_client=pd_client; 
            
             //Newly Added 
            //this.pd_certificatetype=pd_certificatetype;
            if(documentClass=='Policy document'){
               this.pd_docCertype= pd_policydocumenttype;
            }
            if(documentClass=='Certificate'){
                this.pd_docCertype= pd_certificatetype;
            }
            this.pd_author=pd_author;
            this.pd_object=pd_object;
           
        }
        
       public ObjectPropertie(string pd_productarea, string pd_years,string pd_policydocumenttype,string pd_covercovertypes, string documentClass, string name,string pd_creator,string lastModified, Datetime lastModifiedDT, string pd_sourcesystem, string pd_validfrom, Datetime pd_validfromDT, string pd_validto, string pd_objectnameondocument,string pd_client, string pd_certificatetype, string pd_author, string pd_object, string blue_Object_flag, string blue_Object_Port, string blue_Object_CallSign, string blue_Object_GUID ){
             //System.debug('ObjectPropertie Construtor run - ');
            system.debug('second********');
            this.pd_productarea=pd_productarea;
            this.pd_years=pd_years;
            this.pd_policydocumenttype=pd_policydocumenttype;
            this.pd_certificatetype=pd_certificatetype;
            
            
            this.pd_covercovertypes=pd_covercovertypes; 
            this.documentClass=documentClass; 
            this.name=name;
            this.pd_creator=pd_creator;
            //System.debug('lastModified - ' + lastModified);
           
            //this.lastModifiedDT=createDateTime(lastModified);
            this.lastModifiedDT=lastModifiedDT;
            this.lastModified=lastModified;
            this.pd_sourcesystem=pd_sourcesystem;   
            System.debug('pd_validfrom - ' + pd_validfrom);
           
           
            //this.pd_validfromDT=createDateTime(string.valueOf(pd_validfrom));
            this.pd_validfromDT=pd_validfromDT;
          
            this.pd_validfrom=pd_validfrom;
            this.pd_validto=pd_validto;
            this.pd_objectnameondocument=pd_objectnameondocument;
            //this.pd_client=pd_client; 
            
             //Newly Added 
            //this.pd_certificatetype=pd_certificatetype;
            if(documentClass=='Policy document'){
               this.pd_docCertype= pd_policydocumenttype;
            }
            if(documentClass=='Certificate'){
                this.pd_docCertype= pd_certificatetype;
            }
            this.pd_author=pd_author;
            this.pd_object=pd_object;
           
           
           this.blue_Object_flag=blue_Object_flag;
           this.blue_Object_Port=blue_Object_Port;
           this.blue_Object_CallSign=blue_Object_CallSign;
           this.blue_Object_GUID = blue_Object_GUID;
        }
        

    }//END of ObjectPropertie
    
    public Datetime createDateTime(String strDateTime){
        DateTime dt;
        //System.debug('String strDateTime : '+ strDateTime);
        if(strDateTime !=''){
            
            string str =strDateTime.replace('.', '/');
            //System.debug('String str : '+ str);
            List<String> strDateTimeArr = str.split('/');
            //System.debug('String strDateTimeArr : '+ strDateTimeArr);
            List<String> year = new List<String>();
            if(strDateTimeArr.size() >= 2){
               year = strDateTimeArr[2].split(' ');
            }
            try{                
                //dt = DateTime.newInstance(integer.valueOf(year[0]) ,integer.valueOf(strDateTimeArr[0]),integer.valueOf(strDateTimeArr[1]), 0, 0, 0) ;  
                dt = DateTime.newInstance(integer.valueOf(year[0]), integer.valueOf(strDateTimeArr[1]),integer.valueOf(strDateTimeArr[0]), 1, 0, 0) ;
           
            }catch(Exception ex){
                System.debug('Exception while creating DateTime instance - '+ex.getMessage());
                return null;
            }
        }
        //system.debug('dt-------- --->' + dt);
        return dt;
    }
    
    
    
    //To initialise null variables to prevent page crash
    public void postCreationModification(){
        if(resultObjects == null) return;
        for(ResultObject resObj : ResultObjects){
            if(resObj.ObjectProperties != null){
                //if(resObj.ObjectProperties.pd_covercovertypes == null || resObj.ObjectProperties.pd_covercovertypes == 'null') resObj.ObjectProperties.pd_covercovertypes = 'No Cover';
                
                if(resObj.ObjectProperties.pd_validfromDT == null && resObj.ObjectProperties.pd_validfrom != null) resObj.ObjectProperties.pd_validfromDT = createDateTime(resObj.ObjectProperties.pd_validfrom);
              //  if(resObj.ObjectProperties.lastModifiedDT == null && resObj.ObjectProperties.lastModified != null) resObj.ObjectProperties.lastModifiedDT = createDateTime(resObj.ObjectProperties.lastModified);
                system.debug('Date--'+resObj.ObjectProperties.lastModified);
                if(resObj.ObjectProperties.lastModifiedDT == null && resObj.ObjectProperties.lastModified != null) resObj.ObjectProperties.lastModifiedDT = dateTime.parse(resObj.ObjectProperties.lastModified).addhours(2);
                //if(resObj.ObjectProperties.pd_productarea == null || resObj.ObjectProperties.pd_productarea == 'null') resObj.ObjectProperties.pd_productarea = '';
                if(resObj.ObjectProperties.pd_years == null || resObj.ObjectProperties.pd_years == 'null') resObj.ObjectProperties.pd_years = 'n/a';
                if(resObj.ObjectProperties.pd_policydocumenttype == null || resObj.ObjectProperties.pd_policydocumenttype == 'null' || resObj.ObjectProperties.pd_policydocumenttype == '') resObj.ObjectProperties.pd_policydocumenttype = 'n/a';
                if(resObj.ObjectProperties.documentClass == null || resObj.ObjectProperties.documentClass == 'null') resObj.ObjectProperties.documentClass = '';
                if(resObj.ObjectProperties.name == null || resObj.ObjectProperties.name == 'null') resObj.ObjectProperties.name = 'n/a';
                if(resObj.ObjectProperties.pd_creator == null || resObj.ObjectProperties.pd_creator == 'null') resObj.ObjectProperties.pd_creator = '';
                if(resObj.ObjectProperties.pd_sourcesystem == null || resObj.ObjectProperties.pd_sourcesystem == 'null') resObj.ObjectProperties.pd_sourcesystem = '';
                if(resObj.ObjectProperties.pd_validto == null || resObj.ObjectProperties.pd_validto == 'null') resObj.ObjectProperties.pd_validto = 'n/a';
                if(resObj.ObjectProperties.pd_objectnameondocument == null || resObj.ObjectProperties.pd_objectnameondocument == 'null') resObj.ObjectProperties.pd_objectnameondocument = 'n/a';
                //if(resObj.ObjectProperties.pd_client == null || resObj.ObjectProperties.pd_client == 'null') resObj.ObjectProperties.pd_client = '';
                
                //Newly Added                 
                if(resObj.ObjectProperties.pd_certificatetype == null || resObj.ObjectProperties.pd_certificatetype == 'null') resObj.ObjectProperties.pd_certificatetype = '';
                if(resObj.ObjectProperties.pd_author == null || resObj.ObjectProperties.pd_author == 'null') resObj.ObjectProperties.pd_author = '';
                if(resObj.ObjectProperties.pd_object == null || resObj.ObjectProperties.pd_object == 'null') resObj.ObjectProperties.pd_object = '';                
            
                if(resObj.ObjectProperties.documentClass=='Policy document'){
                    //System.debug('resObj.ObjectProperties.documentClass : Policy document'+resObj.ObjectProperties.pd_policydocumenttype);                    
                    resObj.ObjectProperties.pd_docCertype = resObj.ObjectProperties.pd_policydocumenttype;
                }
                if(resObj.ObjectProperties.documentClass =='Certificate'){
                    //System.debug('resObj.ObjectProperties.documentClass : Certificate'+resObj.ObjectProperties.pd_certificatetype);
                    resObj.ObjectProperties.pd_docCertype = resObj.ObjectProperties.pd_certificatetype;
                } 
                if(resObj.ObjectProperties.pd_docCertype == null || resObj.ObjectProperties.pd_docCertype =='null'){
                    resObj.ObjectProperties.pd_docCertype ='n/a';
                }
                
                if(resObj.ObjectProperties.blue_Object_flag == null || resObj.ObjectProperties.blue_Object_flag =='null'){
                    resObj.ObjectProperties.blue_Object_flag ='n/a';
                }
                
                if(resObj.ObjectProperties.blue_Object_Port == null || resObj.ObjectProperties.blue_Object_Port =='null'){
                    resObj.ObjectProperties.blue_Object_Port ='n/a';
                }
                if(resObj.ObjectProperties.blue_Object_CallSign == null || resObj.ObjectProperties.blue_Object_CallSign =='null'){
                    resObj.ObjectProperties.blue_Object_CallSign ='n/a';
                }
                if(resObj.ObjectProperties.blue_Object_GUID == null || resObj.ObjectProperties.blue_Object_GUID =='null'){
                    resObj.ObjectProperties.blue_Object_GUID = 'n/a';
                }
                
                System.debug('resObj.ObjectProperties.pd_docCertype'+resObj.ObjectProperties.pd_docCertype);   
                
            }else{
                System.debug('Warning :-0 DocumentResponseWrapper.postCreationModification - a null ObjectPropertie found in ResultObjects.ObjectProperties');
            }
        }
    } 
}