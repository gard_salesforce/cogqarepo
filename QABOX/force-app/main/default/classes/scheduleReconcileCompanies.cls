global class scheduleReconcileCompanies implements Schedulable {
    global void execute (SchedulableContext SC) {
        reconcileCompanies rs = new reconcileCompanies();
        //get the list of company and psc fields from the mapping table
        List<Reconciliation_Mapping__c> lsMap = [SELECT Id, Name, Company_Field__c, Admin_System_Field__c 
                                                     FROM Reconciliation_Mapping__c 
                                                     ORDER BY Company_Field__c];        
        String strAccFields = 'Name, ';
        String strPSCFields = 'Name, ';
        for (Reconciliation_Mapping__c rec: lsMap) {
            strAccFields+=rec.Company_Field__c+', ';
            strPSCFields+=rec.Admin_System_Field__c+', ';
        }
        strAccFields = strAccFields.substring(0, strAccFields.length()-2)+' ';
        strPSCFields = strPSCFields.substring(0, strPSCFields.length()-2)+' ';
        
        rs.query = 'SELECT Id, Salesforce_Company__c, ' + strPSCFields
                  +'FROM Admin_System_Company__c '
                  +'WHERE Salesforce_Company__c <>\'\' '
                  +'ORDER BY Name';
        rs.AccFields = strAccFields;
        rs.PSCFields = strPSCFields;
        rs.Mappings = lsMap;
        Database.executeBatch(rs, 20);
    }
    
    static testMethod void testSchedule() {
        Test.StartTest();
            scheduleReconcileCompanies testSched = new scheduleReconcileCompanies();
            String strCRON = '0 0 4 * * ?';
            System.schedule('reconcileCompanies Test', strCRON, testSched);
        Test.StopTest();
    }
}