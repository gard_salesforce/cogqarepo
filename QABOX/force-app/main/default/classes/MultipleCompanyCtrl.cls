//SF-4516, after final deployment:
//remove printTimeFor() method
//Almost whole class is refactored, needs thorough testing
public without sharing class MultipleCompanyCtrl{
    
    //Added For Multi Company
    public static boolean isMultiCompanyChecked{get;set;}{isMultiCompanyChecked = false;}
    public boolean isMultiCompanyUser{get;set;}{isMultiCompanyUser = false;} //maybe redundant
    public boolean showMultiCompPopup{get;set;}{showMultiCompPopup = false;}
    public map<Id,Account_Contact_Mapping__c> mapCompany{get;set;}{mapCompany = new map<Id,Account_Contact_Mapping__c>();} 
    public List<selectOption> lstCompany{get;set;}{lstCompany = new List<selectOption>();}
    public List<selectOption> lstCompany2{get;set;}{lstCompany2 = new List<selectOption>();}//because for generic support users, companies can exceed 1000 records
    public Id selectedAccContId{get;set;} //SF-4516 remove this after 18R3, due to retrospective dependencies from VF
    public String contactId;
    public string url = '';
    public boolean redirect{get; set;}{redirect = false;}
    public String renderCondn{get; set;}{renderCondn = '';}
    public AccountToContactMap__c acmCS;
    Integer activeRecordSize = 0;
    public boolean checkMultiLogin {get;set;}
    
    // variables for SF-4516 STARTS
    public Integer selectedACMWrapperId{get;set;}
    static Map<Integer,ACMWrapper> acmWrapperMap;
    Map<Integer,ACMWrapper> acmWrapperMapInstance;
    private static Integer acmWrapperCount;
	static final String strBroker,strClient,strClinic,strESP;
    static{
        acmWrapperMap = new Map<Integer,ACMWrapper>();
        strBroker = 'Broker';
        strClient = 'Client';
        strClinic = 'Clinic';
        strESP = 'External Service Provider'; // To Be removed after 18R3
        acmWrapperCount = 0;
    }    
    // variables for SF-4516 ENDS
    
    
    /*public PageReference forwardToCustomAuthPage() 
    {
    System.debug('I am : '+UserInfo.getUserType() ); 
    if(UserInfo.getUserType() == 'Guest')
    {
    return new PageReference('/GardLogin');
    }
    else
    {
    checkIfMultiCompany();        
    return null;
    }
	}*/ 
    
    public MultipleCompanyCtrl(){
        //printTimeFor('MultipleCompanyCtrl');//System.debug('sfEdit Constructor Starts');
        lstCompany.add(new selectOption('',''));   
        lstCompany2.add(new selectOption('',''));
        url = Apexpages.currentPage().getUrl();
        System.debug('sfEdit url - '+url);
        
                               
        checkMultiLogin = multiLoginCheck();
        /*
        if(multiLoginCheck() > 1){
            checkMultiLogin=true;
        }else{
            checkMultiLogin = false;//Added by Anju
        }
        */
        if(url.contains('pg=r') || url.contains('pg%3Dr')){
            redirect = true;
            renderCondn = 'render';
        }else{
            renderCondn = 'dontrender';
        }
        
        System.debug('inside MultipleCompanyCtrl checkMultiLogin - '+checkMultiLogin+'/ renderCondn - '+renderCondn);//System.debug('sfEdit Constructor Ends');
        //printTimeFor('MultipleCompanyCtrl');
    }
    
    //Added by Anju
    public pageReference redirectedToMultiCompany(){
        //printTimeFor('redirectedToMultiCompany');//System.debug('sfEdit redirectedToMultiCompany Starts');
        checkMultiLogin = false;
        //printTimeFor('redirectedToMultiCompany');//System.debug('sfEdit redirectedToMultiCompany Ends');
        return checkIfMultiCompany();
    }
    
    //SF-4516 method refactored by Pulkit
    public PageReference checkIfMultiCompany(){
        PageReference pg;
        //printTimeFor('checkIfMultiCompany');//System.debug('sfEdit checkIfMultiCompany Starts');
        
        if(!checkMultiLogin){  //Added by Anju
        	AccountToContactMap__c acmCS;
            activeRecordSize = 0;
            lstCompany = new List<SelectOption>();
            lstCompany2 = new List<SelectOption>();
            List<User> users = new List<User>([select  firstName,lastName,id,Contactid,Contactid__c from User where id=:UserInfo.getUserId()]);
            isMultiCompanyChecked = true; //gotta figure out what is it used for
            
            //retreive ACMs
            List<Account_Contact_Mapping__c> acms = [select id, account__c, Active__c, IsPeopleClaimUser__c, account__r.MyGard_enabled__c,account__r.name, account__r.Company_Role__c,account__r.role_broker__c,
                                                           account__r.role_client__c,account__r.role_esp__c,account__r.site, account__r.country__r.country_name__c, contact__c from Account_Contact_Mapping__c
                                                           where Active__c = true AND contact__c =:users[0].Contactid order by Account__r.Name];//SF-4690 AND account__r.MyGard_enabled__c = true, check removed by Pulkit, after concern raised by Olga
            System.debug('acms  - '+(acms != null ? (acms.size()+'') : 'null'));
            if(acms != null && acms.size() > 0){ //this contact/user has Login access to atleast one Company
                acmWrapperBuilder(acms);
                activeRecordSize = sequencedACMWrapperIds.size();
                showMultiCompPopup = activeRecordSize > 1 ? true : false;
                acmWrapperMapInstance = acmWrapperMap;
                System.debug('sfEdit users[0].id/users[0].contactId - '+users[0].id+'/'+users[0].Contactid+'/ activeRecordSize - '+activeRecordSize+'/ showMultiCompPopup - '+showMultiCompPopup+'/ acmWrapperMap.size() - '+acmWrapperMap.size());
                if(showMultiCompPopUp){//if contact/user has multiple types/companies login access
                    ACMWrapper thisACMWRapper;
                    Integer acmWrapperCount = 0;
                    for(Integer wrapperId : sequencedACMWrapperIds){
                        thisACMWrapper = acmWrapperMap.get(wrapperId);
                        System.debug('wrapperId - '+wrapperId+' / thisACMWrapper.wrapperId - '+thisACMWrapper.wrapperId+' / thisACMWrapper.displayString - '+thisACMWrapper.displayString);
                        if(acmWrapperCount < 1000){
                            lstCompany.add(new selectOption(thisACMWrapper.wrapperId+'',thisACMWrapper.displayString));
                        }else if(acmWrapperCount < 2000){
                            lstCompany2.add(new selectOption(thisACMWrapper.wrapperId+'',thisACMWrapper.displayString));
                        }
                        acmWrapperCount++;
                    }
                    System.debug('sfEdit sizes of lists - '+lstCompany.size()+'/'+lstCompany2.size());
                    pg = null;
                }else{//if this contact/user can login only through a single company
                    //acmCS = AccountToContactMap__c.getValues(users[0].Contactid);//because acmWrapper will be created even for a single company too, it'll be better to use the values in it
                    Integer wrapperId = sequencedACMWrapperIds.get(0);
                    ACMWrapper acmWrapper = acmWrapperMap.get(wrapperId);
                    acmCS = AccountToContactMap__c.getValues(acmWrapper.contactId);
                    
                    //if(acmCS == null){ acmCS = new AccountToContactMap__c(Name = users[0].Contactid); }
                    if(acmCS == null){
                        acmCS = new AccountToContactMap__c(
                            Name = acmWrapper.Contactid
                        );
                    }
                    
                    if(acmCS.AccountId__c == null || 
                       acmcs.RolePreference__c == null || 
                       acmCS.AccountId__c != acmWrapper.accountId || 
                       acmCS.RolePreference__c != acmWrapper.role
                      ){//update only if acmCS needs to be changed
                        acmCS.AccountId__c = acmWrapper.accountId;//link thisContact with the only Account
                        acmCS.RolePreference__c = acmWrapper.role;
                        try{
                            upsert acmCS;
                        }catch(Exception ex){
                            System.debug('AccountToContactMap__C CS upsert failed - '+ex.getMessage());
                        }
                    }
                    //Lead To Homepage
                    pg = new PageReference('/apex/HomePage');
                    //if(acmCS != null && acmCS.AccountId__c != null)MyGardHelperCtrl.ACCOUNT_ID_SET = new Set<String>{acmCS.AccountId__c};
                    MyGardHelperCtrl.setExtranetGobalClients();
                    //printTimeFor('checkIfMultiCompany');//System.debug('sfEdit checkIfMultiCompany Ends');
                    //return pg;
                }
            }else{//The contact/user doesn't has access into MyGard
                pg = new PageReference('/apex/GardLogout');
                //printTimeFor('checkIfMultiCompany');//System.debug('sfEdit checkIfMultiCompany Ends');
                //return pg;
            }
        }else{//The contact/user is logged in from some other place too
            pg = null;
        }
        //printTimeFor('checkIfMultiCompany');
        return pg;
    }    
    
    //updates ACM-CS value with what company the user selected
    public PageReference setCompanyForMultiple(){
        //printTimeFor('setCompanyForMultiple');//System.debug('sfEdit setCompanyForMultiple Starts');
        PageReference pg;
        Boolean upsertRequired = false;
    	ACMWrapper acmWrapper;
        AccountToContactMap__c acmCS;
        System.debug('selectedACMWrapperId - '+selectedACMWrapperId);
        System.debug('acmWrapperMap - '+acmWrapperMap);
        System.debug('acmWrapperMapInstance - '+acmWrapperMapInstance);
        if(!Test.isRunningTest())acmWrapperMap = acmWrapperMapInstance; // because static Map can't retain value between two requests
        //System.debug('acmWrapperMap - '+acmWrapperMap);
        System.debug('acmWrapper - '+acmWrapper);
        acmWrapper = acmWrapperMap.get(selectedACMWrapperId);//Account_Contact_Mapping__c acm = mapCompany.get(selectedACMWrapperId);
        System.debug('acmWrapper - '+acmWrapper);
        contactId = acmWrapper.contactId;
        acmCS = AccountToContactMap__c.getValues(contactId);
        
        showMultiCompPopup = false;
        
        // update the acmCS records according to the selction made by the user in the select list
        if(acmCS == null){ //if the acmCS doesn't yet exists for this user
            acmCS = new AccountToContactMap__c(
                name = contactId,
                AccountId__c = acmWrapper.accountId,
                RolePreference__c = acmWrapper.role
            );
            upsertRequired = true;
        }else{//if acmCS exists
            if(acmCS.AccountId__c != acmWrapper.accountId || acmCS.RolePreference__c != acmWrapper.role){
                acmCS.AccountId__c = acmWrapper.accountId;
                acmCS.RolePreference__c = acmWrapper.role;
                upsertRequired = true;
            }
        }
        
        if(upsertRequired){//update only if atleast company or role changes
            try{
                upsert acmCS;
            }catch(Exception ex){
                System.debug('update for multiple companies selection failed - '+ex.getMessage());
            }
        }
        
        redirect = true;
        if(acmCS != null && acmCS.AccountId__c != null)//MyGardHelperCtrl.ACCOUNT_ID_SET = new Set<String>{acmCS.AccountId__c};
        MyGardHelperCtrl.setExtranetGobalClients();
        pg = page.HomePage;
        pg.setRedirect(true);
        
        //printTimeFor('setCompanyForMultiple');//System.debug('sfEdit setCompanyForMultiple Ends');
        return pg;
    }
        
    public pagereference cancel(){
        //printTimeFor('cancel');
        PageReference pg;
        
        if(redirect == true){
            pg = page.HomePage;
            pg.setRedirect(true);
        }else{
            pg = new PageReference('/apex/GardLogout');
        }
        //printTimeFor('cancel');
        return pg;
    }
    
    // added by Anju
    // if the user is already logged in return how many session exists for that user
    private Boolean multiLoginCheck(){
        //printTimeFor('multiLoginCheck');//System.debug('sfEdit multiLoginCheck Starts');
        authSession [] sessionList = new List<AuthSession>([SELECT id,usersId FROM AuthSession 
                                                          WHERE sessionType='ChatterNetworks' 
                                                          AND usersId=:UserInfo.getUserId() ]);
        //printTimeFor('multiLoginCheck');//System.debug('sfEdit multiLoginCheck Ends');
        return sessionList.size() > 1 ? true : false;
    }
    
    //SF-4516 STARTS
    private static List<Integer> sequencedACMWrapperIds = new List<Integer>();//to sequentially traverse records
        
    public void acmWrapperBuilder(List<Account_Contact_Mapping__c> acms){
        sequencedACMWrapperIds = new List<Integer>();
        acmWrapperMap = new Map<Integer,ACMWrapper>();
        acmWrapperCount = 0;
        for(Account_Contact_Mapping__c acm : acms){
            //if(acm.Account__r.MyGard_enabled__c){
                ACMWrapper acmWrapperClient,acmWrapperBroker,acmWrapperClinic;
            	String strHyphen; // added for SF-4608
                if(acm.Account__r.role_broker__c ||acm.Account__r.role_client__c){
                    if(acm.Account__r.role_broker__c){
                        acmWrapperBroker = new ACMWrapper(acm);
                        acmWrapperBroker.role = strBroker;
                    }
                    if(acm.Account__r.role_client__c){
                        acmWrapperClient = new ACMWrapper(acm);
                        acmWrapperClient.role = strClient;
                    }
                    //if both role exists for an account, append roles at the end
                    if(acmWrapperBroker != null && acmWrapperClient != null){
                        strHyphen = ((acm.Account__r.site != null || acm.Account__r.country__r.country_name__c != null) ? ' - ' : '');
                        acmWrapperClient.displayString += (strHyphen+acmWrapperClient.role);
                        acmWrapperBroker.displayString += (strHyphen+acmWrapperBroker.role);
                    }
                }else{// SF-4516 won't be functional right now but in prospect might be needed
                    if(acm.Account__r.Role_ESP__c){
                        acmWrapperClinic = new ACMWrapper(acm);
                        acmWrapperClinic.role = strClinic;
                    }
                }
            //}
        }
        
        for(Integer wrapperId : acmWrapperMap.keySet()){
            System.debug('sfEdit acmWrapper object created - '+acmWrapperMap.get(wrapperId).accountId+' / '+acmWrapperMap.get(wrapperId).contactId);
        }
    }
    
    
    //stores contactID, available AccountIds, and available Roles for each such account that has dual roles
    class ACMWrapper{
        public Integer wrapperId;//to act as key in Map
        public Id accountId;
        public Id contactId;
        public String displayString;//shown in SelectList
        public String role;//to be saved in ACM-CS
        
        ACMWrapper(Account_Contact_Mapping__c acm){
            String site,country,strSeperator; // added for SF-4608, to replace null value with blank spaces 
            this.wrapperId = acmWrapperCount++;
            this.accountId = acm.Account__c;
            this.contactId = acm.Contact__c;
            site = (acm.Account__r.site != null ? (acm.Account__r.site+'') : '');
            country = (acm.Account__r.country__r.country_name__c != null ? (acm.Account__r.country__r.country_name__c+'') : '');
            strSeperator = ((acm.Account__r.site !=null && acm.Account__r.country__r.country_name__c != null) ? ', ' : '');
            this.displayString = acm.Account__r.Name + ';' +site+strSeperator+country;
            sequencedACMWrapperIds.add(this.wrapperId);
            acmWrapperMap.put(this.wrapperId,this);
        }
    }
    //SF-4516 ENDS
    
    //SF-4516, to be removed before deployment to PROD
    /*
    static Map<String,Boolean>methodToStateMap = new Map<String,Boolean>();
    static void printTimeFor(String invokingMethodName){
        
        Integer timeNow = Integer.valueOf(Math.mod( System.currentTimeMillis(),Long.valueOf(10000+'')));
        Boolean alreadyRunning = methodToStateMap.get(invokingMethodName);
        String stateStr;
        String debugIdentifier = 'sfEdit ';
        String auxillaryStr = ' @ - ';
        
        if(alreadyRunning == null || !alreadyRunning){ //if not alreadyRunning
            alreadyRunning = true;
            stateStr = ' STARTS';
        }else{ //if already Running
            stateStr = ' ENDS';
            alreadyRunning = false;
        }
        
        methodToStateMap.put(invokingMethodName,alreadyRunning);
        System.debug(debugIdentifier+invokingMethodName+stateStr+auxillaryStr+timeNow);
    }
 	*/   
}