@isTest(seeAllData=false)
Public class TestUWRChartersVesselCtrl 
{
    
    private static testMethod void ChartersVesselCtrlBroker1(){
        // Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        ID usrRoleId = [SELECT id FROM UserRole WHERE name = 'Admin'].id;
        User adminUsr = new User(Alias = 'standt',
                                 Email='NewTestingUser1111@testorg.com',
                                 EmailEncodingKey='UTF-8',
                                 LastName='NewTestingUser',
                                 LanguageLocaleKey='en_US', 
                                 LocaleSidKey='en_US',
                                 ProfileId = p.Id, 
                                 TimeZoneSidKey='America/Los_Angeles',
                                 UserName='NewTestingUser1111@testorg.com',
                                 UserRoleId = usrRoleId
                                );
        insert adminUsr;
        
        System.runAs(adminUsr)
        {
            Test.startTest();
            GardTestData test_data=new GardTestData();
            test_data.commonRecord();
            test_data.customsettings_rec();
            //Test.stopTest();
            //system.debug('**********GardTestData.brokerAcc.ownerID'+GardTestData.brokerAcc.ownerID);
            //Invoking methods of UWRChartersVesselCtrl for broker and client users...
        }
        
        System.runAs(GardTestData.brokerUser)         
        {
            //Test.startTest();
            UWRChartersVesselCtrl test_broker=new UWRChartersVesselCtrl();
            system.debug('******************loggedInAccountId'+test_broker.loggedInAccountId);
            /* test_broker.FindBy='imo'; */         
            //test_broker.getCountry();//Prakhar
            test_broker.changeSelectedClientIdFormsCS();//Prakhar
            test_broker.confirmationCheckbox=true;
            test_broker.selectedCountryCode = 'IN - India';
            test_broker.classS = 'abc';
            test_broker.imoNo = 123;
            test_broker.lstContact = new List<Contact>();
            test_broker.IMONotKnown=true;
            test_broker.lstContract=new List<Contract>();
            test_broker.lstAsset=new List<Asset>();
            test_broker.selectedObject='new';
            test_broker.isSame = true;
            // test_broker.ast=brokerAsset_1st;
            test_broker.selectedObj = GardTestData.test_object_1st.id;
            // test_broker.isSelGlobalClient = false;
            //system.debug('***********GardTestData.clientAcc.id'+GardTestData.clientAcc.id);
            /*test_broker.selectedGlobalClient = new list<string>();
test_broker.selectedGlobalClient.add(GardTestData.clientAcc.id);
test_broker.setGlobalClientIds = new List<String>{GardTestData.clientAcc.Id};
test_broker.strSelected = GardTestData.brokerAcc.Id+';'+GardTestData.clientAcc.Id;
List<SelectOption> clientOptions =  test_broker.getClientLst();*/
            /*
List<SelectOption> flagoptions=test_broker.getFlagList();
List<SelectOption> portoptions=test_broker.getPortList();
List<SelectOption> classificationoptions=test_broker.getClassificationList();
List<SelectOption> Countrycodes= test_broker.getCountrycodes();
System.assertequals(clientOptions.size()>0,true,true);
System.assertequals(flagoptions.size()>0,true,true);
System.assertequals(portoptions.size()>0,true,true);
System.assertequals(classificationoptions.size()>0,true,true);*/
            test_broker.assetFinal = null;
            test_broker.selectedClient = GardTestData.clientAcc.id;
            test_broker.ObjName = 'testObj'; 
            test_broker.FindBy='objname';
            //test_broker.searchByImoObjectName();
            test_broker.ObjName = string.valueof(GardTestData.test_object_1st.Imo_Lloyds_No__c); 
            test_broker.FindBy='imo';          
            //test_broker.searchByImoObjectName();
            test_broker.assetFinal = null;
            test_broker.selectedClient = GardTestData.clientAcc.id;
            test_broker.ObjName = string.valueof(GardTestData.test_object_1st.name); 
            test_broker.FindBy='objname';
            //test_broker.searchByImoObjectName();
            test_broker.dateOutput='05-02-2015';
            test_broker.dateOutputForReview='05-02-2015';
            test_broker.SelectedClassification.add('class B');
            test_broker.selectedObj=GardTestData.test_object_1st.id;
            //test_broker.setObjectSelection();
            system.debug('*************test_broker.uwf.id'+test_broker.uwf.id);
            //test_broker.uwf = new UWForm__c();
            test_broker.uwf.tonnage__c=integer.valueof('123456');
            test_broker.loggedInAccountId = GardTestData.brokerAcc.ID;
            //test_broker.saveRecord();
            system.debug('*************test_broker.uwf.id'+test_broker.uwf.id);
            //  test_broker.saveRecordNSubmit();
            system.debug('*************test_broker.uwf.id'+test_broker.uwf.id);
            test_broker.uwf.id=GardTestData.brokerCase.id;
            //test_broker.uwf = new UWForm__c();
            test_broker.uwf.tonnage__c=integer.valueof('123456');
            test_broker.selectedCountryCode = 'NOR';
            //test_broker.saveRecord();
            test_broker.goNext();
            //test_broker.clearRecord();
            //cv.saveRecordNSubmit();
            //test_broker.uwf.id=GardTestData.uwf.id;
            test_broker.AcknowledgeMail='ownergard@gmail.com';
            test_broker.uwf = new Case();
            test_broker.uwf.id=GardTestData.brokerCase.id;
            test_broker.uwf.tonnage__c=integer.valueof('123456');
            //test_broker.saveRecordNSubmit();
            test_broker.goPrev();
            test_broker.getCName();
            test_broker.sendToForm();
            test_broker.chkAccess();
            //test_broker.AcknowledgeMail='ownergard@gmail.com';
            //test_broker.sendAckMail();
            //test_broker.sendNotiMail();
            
            test_broker.strSelected=GardTestData.clientAcc.id+';'+GardTestData.clientAcc.id;
            //test_broker.fetchSelectedClients();
            //test_broker.fetchGlobalClients();
            test_broker.AcknowledgeMail='ownergard@gmail.com';
            test_broker.isSelGlobalClient = true;
            test_broker.setGlobalClientIds = new List<String>();
            test_broker.setGlobalClientIds.clear();
            test_broker.selectedGlobalClient = new List<String>();
            test_broker.selectedGlobalClient.clear();
            List<SelectOption> testoptsel = new List<SelectOption>();
            //testoptsel = test_broker.getClientLst();
            test_broker.loggedInAccountId = gardtestdata.brokerAcc.ID;
            //test_broker.saveRecord();
            Test.stopTest();
            Case updtUWF = [SELECT Id,Object_name__c,Imo_Lloyds_No__c,Flag__c,tonnage__c,Port_Of_Registry__c,Year_built__c,
                            Signal_Letters_Call_sign__c,Classification_society__c,Limit_of_Cover_USD__c,CGL_Offshore_Cover_PI__c,
                            Name_of_Assured_Member__c,Co_assureds_capacity__c,Name_on_Premium_Invoice__c,VAT_number__c,VAT_Name_AddressOf_Company__c,
                            Country_prefix__c  FROM Case WHERE Id =: GardTestData.brokerCase.Id];
            updtUWF.Object_name__c = null;
            updtUWF.Imo_Lloyds_No__c = null;
            updtUWF.Flag__c = null;
            updtUWF.tonnage__c = null;
            updtUWF.Port_Of_Registry__c = null;
            updtUWF.Year_built__c = null;
            updtUWF.Signal_Letters_Call_sign__c = null;
            updtUWF.Classification_society__c = null;
            updtUWF.short_description_vessel__c = 'text';
            updtUWF.Limit_of_Cover_USD__c = null;
            updtUWF.CGL_Offshore_Cover_PI__c = 'Yes';
            updtUWF.Name_of_Assured_Member__c = null;
            updtUWF.Co_assureds_capacity__c = 'text';
            updtUWF.Name_on_Premium_Invoice__c = 'text';
            updtUWF.VAT_Name_AddressOf_Company__c = null;
            updtUWF.VAT_number__c = null;
            updtUWF.Country_prefix__c = 'IND';
            test_broker.dateOutput = null;
            update updtUWF;
            test_broker.selectedGardEmployee = '';//Prakhar
            test_broker.selectedGardTeam = '';//Prakhar
            
            test_broker.selectedGlobalClient = new list<string>();
            test_broker.selectedGlobalClient.add(GardTestData.clientAcc.id);
            test_broker.setGlobalClientIds = new List<String>{GardTestData.clientAcc.Id};
                test_broker.accountIdSet = new Set<String>{GardTestData.brokerAcc.Id};
                    test_broker.strSelected = GardTestData.brokerAcc.Id+';'+GardTestData.clientAcc.Id;
            //List<SelectOption> clientOptions =  test_broker.getClientLst();
            test_broker.fetchSelectedClients();
            test_broker.fetchGlobalClients();
            //test_broker.saveRecordNSubmit();
            
            //test_broker.ackSendPdf();
            //Test.stopTest();
        }
    }
    
    private static testMethod void ChartersVesselCtrlBroker2(){
        // Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        ID usrRoleId = [SELECT id FROM UserRole WHERE name = 'Admin'].id;
        User adminUsr = new User(Alias = 'standt',
                                 Email='NewTestingUser1111@testorg.com',
                                 EmailEncodingKey='UTF-8',
                                 LastName='NewTestingUser',
                                 LanguageLocaleKey='en_US', 
                                 LocaleSidKey='en_US',
                                 ProfileId = p.Id, 
                                 TimeZoneSidKey='America/Los_Angeles',
                                 UserName='NewTestingUser1111@testorg.com',
                                 UserRoleId = usrRoleId
                                );
        insert adminUsr;
        
        System.runAs(adminUsr)
        {
            Test.startTest();
            GardTestData test_data=new GardTestData();
            test_data.commonRecord();
            test_data.customsettings_rec();
            //Test.stopTest();
            //system.debug('**********GardTestData.brokerAcc.ownerID'+GardTestData.brokerAcc.ownerID);
            //Invoking methods of UWRChartersVesselCtrl for broker and client users...
        }
        
        System.runAs(GardTestData.brokerUser)         
        {
            //Test.startTest();
            UWRChartersVesselCtrl test_broker=new UWRChartersVesselCtrl();
            system.debug('******************loggedInAccountId'+test_broker.loggedInAccountId);
            /* test_broker.FindBy='imo'; */         
            //test_broker.getCountry();//Prakhar
            test_broker.changeSelectedClientIdFormsCS();//Prakhar
            test_broker.confirmationCheckbox=true;
            test_broker.selectedCountryCode = 'IN - India';
            test_broker.classS = 'abc';
            test_broker.imoNo = 123;
            test_broker.lstContact = new List<Contact>();
            test_broker.IMONotKnown=true;
            test_broker.lstContract=new List<Contract>();
            test_broker.lstAsset=new List<Asset>();
            test_broker.selectedObject='new';
            test_broker.isSame = true;
            // test_broker.ast=brokerAsset_1st;
            test_broker.selectedObj = GardTestData.test_object_1st.id;
            // test_broker.isSelGlobalClient = false;
            //system.debug('***********GardTestData.clientAcc.id'+GardTestData.clientAcc.id);
            /*test_broker.selectedGlobalClient = new list<string>();
test_broker.selectedGlobalClient.add(GardTestData.clientAcc.id);
test_broker.setGlobalClientIds = new List<String>{GardTestData.clientAcc.Id};
test_broker.strSelected = GardTestData.brokerAcc.Id+';'+GardTestData.clientAcc.Id;
List<SelectOption> clientOptions =  test_broker.getClientLst();*/
            /*
List<SelectOption> flagoptions=test_broker.getFlagList();
List<SelectOption> portoptions=test_broker.getPortList();
List<SelectOption> classificationoptions=test_broker.getClassificationList();
List<SelectOption> Countrycodes= test_broker.getCountrycodes();
System.assertequals(clientOptions.size()>0,true,true);
System.assertequals(flagoptions.size()>0,true,true);
System.assertequals(portoptions.size()>0,true,true);
System.assertequals(classificationoptions.size()>0,true,true);*/
            test_broker.assetFinal = null;
            test_broker.selectedClient = GardTestData.clientAcc.id;
            test_broker.ObjName = 'testObj'; 
            test_broker.FindBy='objname';
            //test_broker.searchByImoObjectName();
            test_broker.ObjName = string.valueof(GardTestData.test_object_1st.Imo_Lloyds_No__c); 
            test_broker.FindBy='imo';          
            //test_broker.searchByImoObjectName();
            test_broker.assetFinal = null;
            test_broker.selectedClient = GardTestData.clientAcc.id;
            test_broker.ObjName = string.valueof(GardTestData.test_object_1st.name); 
            test_broker.FindBy='imo';
            //test_broker.searchByImoObjectName(); //too many SOQL
            test_broker.dateOutput='05-02-2015';
            test_broker.dateOutputForReview='05-02-2015';
            test_broker.SelectedClassification.add('class B');
            test_broker.selectedObj=GardTestData.test_object_1st.id;
            //test_broker.setObjectSelection();
            system.debug('*************test_broker.uwf.id'+test_broker.uwf.id);
            //test_broker.uwf = new UWForm__c();
            test_broker.uwf.tonnage__c=integer.valueof('123456');
            test_broker.loggedInAccountId = GardTestData.brokerAcc.ID;
            //test_broker.saveRecord();
            system.debug('*************test_broker.uwf.id'+test_broker.uwf.id);
            //  test_broker.saveRecordNSubmit();
            system.debug('*************test_broker.uwf.id'+test_broker.uwf.id);
            test_broker.uwf.id=GardTestData.brokerCase.id;
            //test_broker.uwf = new UWForm__c();
            test_broker.uwf.tonnage__c=integer.valueof('123456');
            test_broker.selectedCountryCode = 'NOR';
            //test_broker.saveRecord();
            test_broker.goNext();
            //test_broker.clearRecord();
            //cv.saveRecordNSubmit();
            //test_broker.uwf.id=GardTestData.uwf.id;
            test_broker.AcknowledgeMail='ownergard@gmail.com';
            test_broker.uwf = new Case();
            test_broker.uwf.id=GardTestData.brokerCase.id;
            test_broker.uwf.tonnage__c=integer.valueof('123456');
            //test_broker.saveRecordNSubmit();
            test_broker.goPrev();
            test_broker.getCName();
            test_broker.sendToForm();
            test_broker.chkAccess();
            //test_broker.AcknowledgeMail='ownergard@gmail.com';
            //test_broker.sendAckMail();
            //test_broker.sendNotiMail();
            
            test_broker.strSelected=GardTestData.clientAcc.id+';'+GardTestData.clientAcc.id;
            //test_broker.fetchSelectedClients();
            //test_broker.fetchGlobalClients();
            test_broker.AcknowledgeMail='ownergard@gmail.com';
            test_broker.isSelGlobalClient = true;
            test_broker.setGlobalClientIds = new List<String>();
            test_broker.setGlobalClientIds.clear();
            test_broker.selectedGlobalClient = new List<String>();
            test_broker.selectedGlobalClient.clear();
            List<SelectOption> testoptsel = new List<SelectOption>();
            //testoptsel = test_broker.getClientLst();
            test_broker.loggedInAccountId = gardtestdata.brokerAcc.ID;
            //test_broker.saveRecord();
            Test.stopTest();
            Case updtUWF = [SELECT Id,Object_name__c,Imo_Lloyds_No__c,Flag__c,tonnage__c,Port_Of_Registry__c,Year_built__c,
                            Signal_Letters_Call_sign__c,Classification_society__c,Limit_of_Cover_USD__c,CGL_Offshore_Cover_PI__c,
                            Name_of_Assured_Member__c,Co_assureds_capacity__c,Name_on_Premium_Invoice__c,VAT_number__c,VAT_Name_AddressOf_Company__c,
                            Country_prefix__c  FROM Case WHERE Id =: GardTestData.brokerCase.Id];
            updtUWF.Object_name__c = null;
            updtUWF.Imo_Lloyds_No__c = null;
            updtUWF.Flag__c = null;
            updtUWF.tonnage__c = null;
            updtUWF.Port_Of_Registry__c = null;
            updtUWF.Year_built__c = null;
            updtUWF.Signal_Letters_Call_sign__c = null;
            updtUWF.Classification_society__c = null;
            updtUWF.short_description_vessel__c = 'text';
            updtUWF.Limit_of_Cover_USD__c = null;
            updtUWF.CGL_Offshore_Cover_PI__c = 'Yes';
            updtUWF.Name_of_Assured_Member__c = null;
            updtUWF.Co_assureds_capacity__c = 'text';
            updtUWF.Name_on_Premium_Invoice__c = 'text';
            updtUWF.VAT_Name_AddressOf_Company__c = null;
            updtUWF.VAT_number__c = null;
            updtUWF.Country_prefix__c = 'IND';
            test_broker.dateOutput = null;
            update updtUWF;
            test_broker.selectedGardEmployee = '';//Prakhar
            test_broker.selectedGardTeam = '';//Prakhar
            
            test_broker.selectedGlobalClient = new list<string>();
            test_broker.selectedGlobalClient.add(GardTestData.clientAcc.id);
            test_broker.setGlobalClientIds = new List<String>{GardTestData.clientAcc.Id};
                test_broker.accountIdSet = new Set<String>{GardTestData.brokerAcc.Id};
                    test_broker.strSelected = GardTestData.brokerAcc.Id+';'+GardTestData.clientAcc.Id;
            //List<SelectOption> clientOptions =  test_broker.getClientLst();
            test_broker.fetchSelectedClients();
            test_broker.fetchGlobalClients();
            //test_broker.saveRecordNSubmit();
            
            //test_broker.ackSendPdf();
            //Test.stopTest();
        }
    }
    
    private static testMethod void ChartersVesselCtrlClient1(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        ID usrRoleId = [SELECT id FROM UserRole WHERE name = 'Admin'].id;
        User adminUsr = new User(Alias = 'standt',
                                 Email='NewTestingUser1111@testorg.com',
                                 EmailEncodingKey='UTF-8',
                                 LastName='NewTestingUser',
                                 LanguageLocaleKey='en_US', 
                                 LocaleSidKey='en_US',
                                 ProfileId = p.Id, 
                                 TimeZoneSidKey='America/Los_Angeles',
                                 UserName='NewTestingUser1111@testorg.com',
                                 UserRoleId = usrRoleId
                                );
        insert adminUsr;
        
        System.runAs(adminUsr)
        {
            Test.startTest();
            GardTestData test_data=new GardTestData();
            //Test.startTest();
            test_data.commonRecord();
            test_data.customsettings_rec();
            //system.debug('**********GardTestData.brokerAcc.ownerID'+GardTestData.brokerAcc.ownerID);
            //Invoking methods of UWRChartersVesselCtrl for broker and client users...      
            Test.stopTest();
        }
        
        System.runAs(GardTestData.clientUser )  
        {
            //Test.startTest();
            UWRChartersVesselCtrl test_client=new UWRChartersVesselCtrl();
            system.debug('******************loggedInAccountId'+test_client.loggedInAccountId);
            test_client.confirmationCheckbox=true;
            test_client.IMONotKnown=true;
            test_client.lstContract=new List<Contract>();
            test_client.lstAsset=new List<Asset>();
            test_client.selectedObject='new';
            test_client.ast=GardTestData.clientAsset_1st;
            test_client.selectedObj=GardTestData.test_object_1st.id;
            test_client.selectedGlobalClient = new list<string>();
            
            test_client.selectedGlobalClient.add(GardTestData.clientAcc.id);
            test_client.setGlobalClientIds = new List<String>{GardTestData.clientAcc.Id};
                test_client.strSelected = GardTestData.brokerAcc.Id+';'+GardTestData.clientAcc.Id;
            List<SelectOption> clientOptions = test_client.getClientLst();
            List<SelectOption> flagoptions=test_client.getFlagList();
            List<SelectOption> portoptions=test_client.getPortList();
            List<SelectOption> classificationoptions=test_client.getClassificationList();
            //System.assertequals(clientOptions.size()>0,true,true);
            //System.assertequals(flagoptions.size()>0,true,true);
            //System.assertequals(portoptions.size()>0,true,true);
            //System.assertequals(classificationoptions.size()>0,true,true);
            test_client.assetFinal = null;
            test_client.selectedClient = GardTestData.clientAcc.id;
            test_client.ObjName = string.valueof(GardTestData.test_object_1st.Imo_Lloyds_No__c);
            test_client.assetFinal = null;
            test_client.selectedClient = GardTestData.clientAcc.id;
            test_client.ObjName = string.valueof(GardTestData.test_object_1st.Name); 
            test_client.FindBy='objname';
            test_client.searchByImoObjectName();
            test_client.dateOutput='05-02-2015';
            test_client.dateOutputForReview='05-02-2015';
            test_client.SelectedClassification.add('class B');
            test_client.selectedObj=GardTestData.test_object_1st.id;
            //test_client.setObjectSelection();
            //test_client.grossTonnage=234567;
            //test_client.uwf = new UWForm__c();
            test_client.uwf.tonnage__c=integer.valueof('123456');
            //test_client.selectedCountryCode = (''+GardTestData.country.Display_value__c);
            test_client.loggedInAccountId = GardTestData.brokerAcc.Id;
            test_client.mapObjects = new Map<Id,Object__c>();
            test_client.mapObjects.put(test_client.selectedObj,GardTestData.test_object_1st);
            test_client.setObjectSelection();
            test_client.saveRecord();
            test_client.uwf.id=GardTestData.Clientcase.id;
            //test_client.uwf = new UWForm__c();
            test_client.uwf.tonnage__c=integer.valueof('123456');
            //test_client.saveRecord();
            //test_client.goNext();
            test_client.clearRecord();
            //test_client.uwf = new UWForm__c();
            test_client.uwf.tonnage__c=integer.valueof('123456');
            test_client.saveRecordNSubmit();
            test_client.uwf.id=GardTestData.clientCase.id;
            //test_client.uwf = new UWForm__c();
            test_client.uwf.tonnage__c=integer.valueof('123456');
            //test_client.saveRecordNSubmit();
            test_client.goPrev();
            test_client.getCName();
            test_client.sendToForm();
            test_client.chkAccess();
            test_client.strGlobalClient = 'testing';
            //test_client.sendAckMail();
            //test_client.sendNotiMail();
            test_client.getCountry();
            test_client.getCountrycodes();
            
            test_client.setGlobalClientIds = new List<String>{GardTestData.clientAcc.Id};
                test_client.strSelected = GardTestData.brokerAcc.Id+';'+GardTestData.clientAcc.Id;
            clientOptions =  test_client.getClientLst();
            test_client.fetchSelectedClients();
            MyGardHelperCtrl.ACM_JUNC_OBJ = null;
            test_client.userType = MyGardHelperCtrl.strClinic;
            test_client.chkAccess();
            //Test.stopTest();
        }
    }
    
    private static testMethod void ChartersVesselCtrlClient2(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        ID usrRoleId = [SELECT id FROM UserRole WHERE name = 'Admin'].id;
        User adminUsr = new User(Alias = 'standt',
                                 Email='NewTestingUser1111@testorg.com',
                                 EmailEncodingKey='UTF-8',
                                 LastName='NewTestingUser',
                                 LanguageLocaleKey='en_US', 
                                 LocaleSidKey='en_US',
                                 ProfileId = p.Id, 
                                 TimeZoneSidKey='America/Los_Angeles',
                                 UserName='NewTestingUser1111@testorg.com',
                                 UserRoleId = usrRoleId
                                );
        insert adminUsr;
        
        System.runAs(adminUsr)
        {
            Test.startTest();
            GardTestData test_data=new GardTestData();
            //Test.startTest();
            test_data.commonRecord();
            test_data.customsettings_rec();
            //system.debug('**********GardTestData.brokerAcc.ownerID'+GardTestData.brokerAcc.ownerID);
            //Invoking methods of UWRChartersVesselCtrl for broker and client users...      
            Test.stopTest();
        }
        
        System.runAs(GardTestData.clientUser )  
        {
            //Test.startTest();
            UWRChartersVesselCtrl test_client=new UWRChartersVesselCtrl();
            system.debug('******************loggedInAccountId'+test_client.loggedInAccountId);
            test_client.confirmationCheckbox=true;
            test_client.IMONotKnown=true;
            test_client.lstContract=new List<Contract>();
            test_client.lstAsset=new List<Asset>();
            test_client.selectedObject='new';
            test_client.ast=GardTestData.clientAsset_1st;
            test_client.selectedObj=GardTestData.test_object_1st.id;
            test_client.selectedGlobalClient = new list<string>();
            
            test_client.selectedGlobalClient.add(GardTestData.clientAcc.id);
            test_client.setGlobalClientIds = new List<String>{GardTestData.clientAcc.Id};
                test_client.strSelected = GardTestData.brokerAcc.Id+';'+GardTestData.clientAcc.Id;
            List<SelectOption> clientOptions = test_client.getClientLst();
            List<SelectOption> flagoptions=test_client.getFlagList();
            List<SelectOption> portoptions=test_client.getPortList();
            List<SelectOption> classificationoptions=test_client.getClassificationList();
            //System.assertequals(clientOptions.size()>0,true,true);
            //System.assertequals(flagoptions.size()>0,true,true);
            //System.assertequals(portoptions.size()>0,true,true);
            //System.assertequals(classificationoptions.size()>0,true,true);
            test_client.assetFinal = null;
            test_client.selectedClient = GardTestData.clientAcc.id;
            test_client.ObjName = string.valueof(GardTestData.test_object_1st.Imo_Lloyds_No__c); 
            test_client.FindBy='imo';          
            //test_client.searchByImoObjectName();
            test_client.assetFinal = null;
            test_client.selectedClient = GardTestData.clientAcc.id;
            test_client.ObjName = string.valueof(GardTestData.test_object_1st.Name); 
            /*test_client.FindBy='objname';
test_client.searchByImoObjectName();*/
            test_client.FindBy='imo';
            test_client.searchByImoObjectName();
            test_client.dateOutput='05-02-2015';
            test_client.dateOutputForReview='05-02-2015';
            test_client.SelectedClassification.add('class B');
            test_client.selectedObj=GardTestData.test_object_1st.id;
            //test_client.setObjectSelection();
            //test_client.grossTonnage=234567;
            //test_client.uwf = new UWForm__c();
            test_client.uwf.tonnage__c=integer.valueof('123456');
            //test_client.selectedCountryCode = (''+GardTestData.country.Display_value__c);
            test_client.loggedInAccountId = GardTestData.brokerAcc.Id;
            test_client.mapObjects = new Map<Id,Object__c>();
            test_client.mapObjects.put(test_client.selectedObj,GardTestData.test_object_1st);
            test_client.setObjectSelection();
            test_client.saveRecord();
            test_client.uwf.id=GardTestData.Clientcase.id;
            //test_client.uwf = new UWForm__c();
            test_client.uwf.tonnage__c=integer.valueof('123456');
            //test_client.saveRecord();
            //test_client.goNext();
            test_client.clearRecord();
            //test_client.uwf = new UWForm__c();
            test_client.uwf.tonnage__c=integer.valueof('123456');
            test_client.saveRecordNSubmit();
            test_client.uwf.id=GardTestData.clientCase.id;
            //test_client.uwf = new UWForm__c();
            test_client.uwf.tonnage__c=integer.valueof('123456');
            //test_client.saveRecordNSubmit();
            test_client.goPrev();
            test_client.getCName();
            test_client.sendToForm();
            test_client.chkAccess();
            test_client.strGlobalClient = 'testing';
            //test_client.sendAckMail();
            //test_client.sendNotiMail();
            test_client.getCountry();
            test_client.getCountrycodes();
            
            test_client.setGlobalClientIds = new List<String>{GardTestData.clientAcc.Id};
                test_client.strSelected = GardTestData.brokerAcc.Id+';'+GardTestData.clientAcc.Id;
            clientOptions =  test_client.getClientLst();
            test_client.fetchSelectedClients();
            MyGardHelperCtrl.ACM_JUNC_OBJ = null;
            test_client.userType = MyGardHelperCtrl.strClinic;
            test_client.chkAccess();
            //Test.stopTest();
        }
        
    }
}