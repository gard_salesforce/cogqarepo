@isTest
private class TestShareWithClient{
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Market_Area__c Markt;
    public static User testUser;
    public static Account clientAcc;
    public static Account brokerAcc;
    public static Contract AccContract;
    public static Country__c Country;
    public static User salesforceLicUser;
    private static String orgId = UserInfo.getOrganizationId();
    private static String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    public static Asset broker_Asset;
    private static void createUser(){
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt + '.mygard';
        salesforceLicUser = new User(
                                Alias = uniqueName.substring(18, 23), 
                                profileId = salesforceLicenseId ,
                                Email=uniqueName + '@test' + orgId + '.org',
                                EmailEncodingKey='UTF-8',
                                CommunityNickname = 'test13',
                                LastName='Testing',
                                LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US',  
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName=uniqueName + '@test' + orgId + '.org.mygard'
                               );
        insert salesforceLicUser;
    }
    
    private static void createCompany(){
         AdminUsers__c setting = new AdminUsers__c();
            setting.Name = 'Number of users';
            setting.Value__c = 3;
            insert setting;

        Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt; 
        createUser(); 

        clientAcc = new Account( Name = 'Test_1', 
                                        Site = '_www.test_1.se', 
                                        Type = 'Client', 
                                        BillingStreet = 'Gatan 1',
                                        BillingCity = 'Stockholm', 
                                        BillingCountry = 'SWE', 
                                        BillingPostalCode = 'BS1 1AD',
                                        recordTypeId=System.Label.Client_Contact_Record_Type,
                                        Market_Area__c = Markt.id,
                                        Area_Manager__c = salesforceLicUser.id 
                                       );
        insert clientAcc;
        
        brokerAcc = new Account(  Name='testre',
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',
                                    recordTypeId=System.Label.Broker_Contact_Record_Type,
                                    Site = '_www.cts.se',
                                    Type = 'Broker',
                                    Market_Area__c = Markt.id,
                                    Area_Manager__c = salesforceLicUser.id                                                                       
                                 );
         insert brokerAcc;
    }
    
    static testMethod void testBehaviour(){
        TestShareWithClient.createCompany();
        Test.startTest();
        
        Contract aContract = new Contract(Name = 'Test Contract 1',
                                        client__c = clientAcc.ID,
                                        Broker__c = brokerAcc.ID,
                                        AccountId = clientAcc.ID);
        insert aContract;
        //asset inserted
        Object__c Object1 = new Object__c(Name= 'TestObject1',
                              //CurrencyIsoCode='NOK-Norwegian Krone',
                              Object_Unique_ID__c='test11223'
                             );
        insert Object1;
        broker_Asset = new Asset(Name=  'SampleAsset' ,
                                 accountid =brokerAcc.id,
                                 Agreement__c = aContract.id, 
                                 Cover_Group__c =  'P&I' ,
                                 CurrencyIsoCode= 'USD'  ,
                                 //product_name__c =  piStr ,
                                 //Object__c = testobj.id,
                                 //Underwriter__c = broker_User.id,
                                 On_risk_indicator__c = true,
                                 Risk_ID__c = '123456ris',
                                 Product_Name__c= 'P&I Cover' ,
                                 Expiration_Date__c =  date.today(),
                                 Inception_date__c =   date.valueof( '2012-01-01' )  ,
                                 Gard_Share__c = '100.00',
                                 Object__c = Object1.Id,
                                 Claims_Lead__c = 'N'
                                ); 
        insert broker_Asset;
        //asset insert ends
        Contract fetchedContract = [SELECT ID, Shared_With_Client__c FROM CONTRACT WHERE Name = 'Test Contract 1'];
        System.assertEquals(false,fetchedContract.Shared_With_Client__c,false);
        
        fetchedContract.Shared_With_Client__c = true;
        update fetchedContract;
        fetchedContract.Shared_With_Client__c = false;
        update fetchedContract;
        fetchedContract = [SELECT ID, Shared_With_Client__c FROM CONTRACT WHERE Name = 'Test Contract 1'];
        System.assertEquals(false,fetchedContract.Shared_With_Client__c,false);
        
        Contract newContract = new Contract(Name = 'New Test Contract 1',
                                        client__c = clientAcc.ID,
                                        Broker__c = brokerAcc.ID,
                                        AccountId = clientAcc.ID);
        insert newContract;
        
        Contract fetchedNewContract = [SELECT ID, Shared_With_Client__c FROM CONTRACT WHERE Name = 'New Test Contract 1'];       
        //System.assertEquals(true,fetchedNewContract.Shared_With_Client__c,true);
        Test.stopTest();
        
        
        
    }


}