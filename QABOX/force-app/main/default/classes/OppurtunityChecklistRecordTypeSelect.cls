global class OppurtunityChecklistRecordTypeSelect{
    Opportunity_Checklist__c t;
    public String currentOppId,currentOppId2;
    public String currentOppChecklistRecordTypeId;
    public String selectedRecordTypeName;
    public boolean checkError = false;
    ApexPages.StandardController controller;    
    public OppurtunityChecklistRecordTypeSelect(ApexPages.StandardController controller) {
        this.controller = controller;
        t = (Opportunity_Checklist__c) Controller.getRecord();
        
        //SF-4655 STARTS,this probably would never work, as it is handled in the JS button
        /*
        try{//added to see parent records state to be removed PARTIALLY
        	currentOppId = System.currentPageReference().getParameters().get('masterId');
            currentOppId2 = System.currentPageReference().getParameters().get('CF00ND0000005D8AX_lkid');
        	Opportunity currentOpportunity = Database.query('SELECT id,name FROM Opportunity WHERE id = :currentOppId');
            //Code to be removed STARTS
            System.debug('this record - '+t);
            System.debug('controller instance - '+this.controller);
            System.debug('parent record id - '+currentOppId);
            System.debug('parent record2 id - '+currentOppId);
            System.debug('parent record - '+currentOpportunity);
            System.debug('parent record locked - '+Approval.isLocked(currentOpportunity.id));
            if(Approval.isLocked(currentOpportunity.id)){
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This record is locked. So, Opportunity Checklist can\'t be created.'));
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This message shouldn\'t be showing up if you see this contact Pulkit.'));
            }
            //Code to be removed ENDS
        }catch(Exception ex){
            System.debug('Exception Caught in OppurtunityChecklistRecordTypeSelect Controller - '+ex.getMessage());
        }
        */
        //SF-4655 ENDS
    }
    public list<SelectOption> getRecordTypes() {
        list<SelectOption> options = new list<SelectOption>();
        for (list<RecordType> rts : [SELECT ID, name FROM RecordType WHERE SObjectType = 'Opportunity_Checklist__c' ORDER BY name desc]) {
            for (RecordType rt : rts) {
            
                if((rt.Name != 'Old P&I' ) && (rt.Name != 'P&I') && (rt.Name != 'Marine') && (rt.Name != 'Marine newbiz/upsell') && (rt.Name != 'Marine renewal'))
                    {
                        options.add(new SelectOption(rt.ID, rt.Name));
                    }
            } 
        }
        return options;
    }
    
     public pagereference newOppChecklist() {
        currentOppId = System.currentPageReference().getParameters().get('masterId');
        ID oppID = Id.valueOf(currentOppId);
        System.debug('getting parentID'+ currentOppId );
        currentOppChecklistRecordTypeId = t.RecordTypeID;
        System.debug('getting currentOppChecklistRecordTypeId '+ currentOppChecklistRecordTypeId );
        if(currentOppChecklistRecordTypeId!=null){
            RecordType rname=[SELECT Name FROM RecordType where id =: currentOppChecklistRecordTypeId];
            selectedRecordTypeName = rname.name;
        }
        System.debug('getting selectedRecordTypeName '+ selectedRecordTypeName );
        List<Opportunity_Checklist__c> allOppCheckList = [SELECT ID, RecordType.Name FROM Opportunity_Checklist__c WHERE Opportunity__c =: currentOppId ];
        /*if(allOppCheckList.size()>0){
        
            for(Opportunity_Checklist__c a : allOppCheckList){
                if(a.RecordType.Name == selectedRecordTypeName){
                    System.debug('testing'+a.RecordType.Name);
                    checkError = true;
                    break;
                    //a.addError('Only one checklist with agreement type: ' + a.RecordType.Name + ' is allowed');
                   // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Only one checklist with agreement type: '+ a.RecordType.Name + ' is allowed'));
                }else{
                     checkError = false;
                }
            }
        } */
        //modified for sf-3965
        Opportunity oppName= [SELECT name FROM Opportunity WHERE id =: currentOppId ];
        String oppN = String.valueOf(oppName.Name);
        System.debug('getting oppName'+ oppName);
        System.debug('getting oppN '+ oppN );
        if(oppN.length() >55){
            oppN = oppN.substring(0,54);
        }
        String oppNFinal = EncodingUtil.urlEncode(oppN,'UTF-8');
        String checklistName = 'Checklist - '+oppNFinal;
        PageReference reSend = new PageReference('/a0l/e?CF00ND0000005D8AX='+ oppNFinal + '&&Name=' + checklistName + '&&CF00ND0000005D8AX_lkid='+oppID+ '&&retURL='+oppID+'&&RecordType='+currentOppChecklistRecordTypeId);
        reSend.setRedirect(true);
        return reSend ;
        
        
    }
    
    //SF-4566,relaying method for new Opportunity Checklist Button
    webservice static Boolean isLocked(Id opportunityId){
        return Approval.isLocked(opportunityId);
    }
    
    
}