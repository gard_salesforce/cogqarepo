@isTest(seeAllData=true)

private class Test_MeetingMinutesAction
{
  public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
  public static Market_Area__c Markt;
  public static User salesforceLicUser;
  public static Account clientAcc;
  private static testMethod void MeetingMinutesAction()
  
  {
    Test.startTest();

    // Use existing meeting
  //  Event myEvent = [SELECT Id, Location, location__c FROM EVENT WHERE ID ='00U8E00000265bSUAQ' limit 1];
//    System.assertNotEquals(null,myEvent);
 
  // Create Market area   
       Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
       insert Markt;  
    // Create user
         salesforceLicUser = new User(
                                Alias = 'standt', 
                                profileId = salesforceLicenseId ,
                                Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8',
                                CommunityNickname = 'test13',
                                LastName='Testing',
                                LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US',  
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName='testtor008007@testorg.com'
                                     );
        insert salesforceLicUser;             

      // Create Company
        clientAcc = new Account( Name = 'Test_1', 
                                Site = '_www.test_1.se', 
                                Type = 'Client', 
                                BillingStreet = 'Gatan 1',
                                BillingCity = 'Stockholm', 
                                BillingCountry = 'SWE', 
                                BillingPostalCode = 'BS1 1AD',
                                recordTypeId=System.Label.Client_Contact_Record_Type,
                                Company_Role_Text__c='Client',
                                Market_Area__c = Markt.id,
                                Area_Manager__c = salesforceLicUser.id,
                                OwnerId = salesforceLicUser.id
                               );
        insert clientAcc;

   // Create a meeting
    Event myEvent = new Event();
      {
        myEvent.Type = 'Meeting';
        myEvent.Subject = 'Test Meeting Event';
        myEvent.Location = 'Test Location';
        myEvent.location__c = 'Arendal';
        myEvent.Description = 'Test Description';
        myEvent.StartDateTime = Datetime.now();
        myEvent.EndDateTime = Datetime.now();
        myEvent.WhatId = clientAcc.id;
        insert myEvent;
      }
   
     PageReference pRef = new PageReference('/apex/MeetingMinutes');
     pRef.getParameters().put('id',myEvent.Id);
     Test.setCurrentPage(pRef);  
     ApexPages.StandardController sc = new ApexPages.standardController(myEvent);
     MeetingMinutesAction testMM = new MeetingMinutesAction(sc);
      
    // Verification

     System.assertNotEquals(null,myEvent);
     System.assertEquals(myEvent.Location__c, 'Arendal'); 
     System.assertNotEquals(null,testMM.autoRun());

     Test.stopTest();
  }
}