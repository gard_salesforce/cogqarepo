@isTest
private class TestBatchCorrespodentPIHullFlagUpdate  {
    
    Static testMethod void  BatchCorrespodentPIHullFlagUpdatetest ()
    {
        Account acc = TestDataGenerator.getClientAccount();
        
        CorrespondentPort__c cp = new CorrespondentPort__c();
        cp.Company__c = acc.id;
        cp.Stop_Date_P_I__c = System.today().addDays(-2);
        cp.Stop_Date_Hull__c = System.today().addDays(-4);
        insert cp;
        
        List<CorrespondentPort__c> cpList = new List<CorrespondentPort__c>();
        cpList.add(cp);
        
        Test.startTest();
            BatchCorrespodentPIHullFlagUpdate job = new BatchCorrespodentPIHullFlagUpdate();
            Database.QueryLocator ql = job.start(null);
            job.execute(null,cpList);
            job.Finish(null);
        Test.StopTest();
        
    }
    Static testMethod void  TriggerCorrespodentPIHullFlagUpdatetest ()
    {
        Account acc = TestDataGenerator.getClientAccount();
        
        CorrespondentPort__c cp = new CorrespondentPort__c();
        cp.Company__c = acc.id;
        cp.Stop_Date_P_I__c = System.today().addDays(4);
        cp.Stop_Date_Hull__c = System.today().addDays(5);
        List<CorrespondentPort__c> cpList = new List<CorrespondentPort__c>();
        cpList.add(cp);
        
        Test.startTest();
            insert cpList ;
        Test.StopTest();
        
    }
}