/**************************************************************************
* Author  : Phil Spenceley
* Company : cDecisions
* Date    : 13/11/2013
***************************************************************************/
    
/// <summary>
///  This proxy class provides methods to access the MDM Partner Service
/// </summary>
public without sharing class MDMMergePartnerProxy extends MDMProxy {

	private Set<String> getMergedAccountFields(String requestName) {
		Set<String> fields = new Set<String>();
		for (MDM_Service_Fields__c field : MDM_Service_Fields__c.getAll().values()) {
			if (requestName.equalsIgnoreCase(field.Request_Name__c) && 'MDM_Account_Merge__c'.equalsIgnoreCase(field.Source_Object__c) && 'Field'.equalsIgnoreCase(field.Type__c)) {
				fields.add(field.Field_Name__c.toLowerCase());
			}
		}
		return fields;
	}
    private List<MDM_Account_Merge__c> getMergedAccountsByIds(List<id> mergedAccountIds, String requestName) {
        return (List<MDM_Account_Merge__c>)Database.query(buildSoqlRequest(getMergedAccountFields(requestName), 'MDM_Account_Merge__c', mergedAccountIds));
    }
	
	
	private MDMMergePartnerResponse MDMMergePartners(MDMMergePartnerRequest request) {
        DOM.Document responseDoc = sendRequest(request);
		MDMMergePartnerResponse response = new MDMMergePartnerResponse();
		try {
			response.Deserialize(
	            responseDoc
	                .getRootElement()
	                .getChildElement(SOAP_Body, SOAPNS)
	                .getChildElement(MDMChangeResponse_Label, MDMChangeResponse_Namespace)
	        );
		} catch (Exception ex) {
			throw new MDMDeserializationException(responseDoc, 'Failed to deserialize MDMPartnerProxy.MDMMergePartnerResponse', ex);
		}
		return response;
    }
	
	public void MDMMergePartner(List<id> mergedAccountIds) {
		Map<string, MDM_Account_Merge__c> mergedAccounts = new Map<string, MDM_Account_Merge__c>();
		MDMMergePartnerRequest request = new MDMMergePartnerRequest();
		for (MDM_Account_Merge__c m : getMergedAccountsByIds(mergedAccountIds, request.getName())) {
			mergedAccounts.put(m.from_account__c + ',' + m.to_account__c, m);
		}
			
		try {
			request.MergedAccounts = mergedAccounts.values();
			MDMMergePartnerResponse response = MDMMergePartners(request);
			
			for (MDMResponse mdmResponse : response.MDMResponses) {
				System.debug('*** MDMResponse:[ id: + ' + mdmResponse.id + ', success: ' + string.valueOf(mdmResponse.success) + ']');
				if (mdmResponse.Success) {
	                mergedAccounts.get(mdmResponse.Id).Synchronisation_Status__c = 'Synchronised';
	            } else {
	                mergedAccounts.get(mdmResponse.Id).Synchronisation_Status__c = 'Sync Failed';
	            }	
			}
			
			update mergedAccounts.values();
			
		} catch (MDMDeserializationException ex) {
			Logger.LogException('MDMMergePartnerProxy.MDMMergePartnerAsync (@Future, ex)', ex, ex.xmlString);
			if (mergedAccounts != null) {
				SetAccountsSyncFailed(mergedAccounts.values(), 'MDMMergePartnerProxy.MDMMergePartnerAsync (@Future, innerEx)');
			}
			
        } catch (Exception ex) {
            Logger.LogException('MDMMergePartnerProxy.MDMMergePartnerAsync (@Future, ex)', ex);
            if (mergedAccounts != null) {
				SetAccountsSyncFailed(mergedAccounts.values(), 'MDMMergePartnerProxy.MDMMergePartnerAsync (@Future, innerEx)');
			}
			
        }
	}
		
	
	@future (callout = true) 
	public static void MDMMergePartnerAsync(List<id> mergedAccountIds) {
		MDMMergePartnerProxy proxy = new MDMMergePartnerProxy();
		proxy.MDMMergePartner(mergedAccountIds);
	}
	
	//Called from an exception to set all sync status to failed
	private static void SetAccountsSyncFailed(List<MDM_Account_Merge__c> mergedAccounts, String source) {
		try {
			if (mergedAccounts != null) {
				for (MDM_Account_Merge__c a : mergedAccounts) {
					a.Synchronisation_Status__c = 'Sync Failed';
				}
				update mergedAccounts; 
			}
		} catch (Exception ex) {
			//Simple update, hopefully shouldn't get here.
			Logger.LogException(source, ex);
		}
	}
	
/// ----------------------------------------------------------------------------
///  -- Sub classes for the request and response messages called above
/// ----------------------------------------------------------------------------
	public virtual class MDMPartnerRequest extends MDMRequest {
		//TODO: move to custom setting...
		protected String MDMChangePartnersRequest_Label {
			get {
				return 'changePartnersRequest';
			}
		}
		protected String MDMChangePartnersRequest_Namespace {
			get {
				return 'http://www.gard.no/mdm/v1_0/mdmmessage';
			}
		}
		protected String MDMChangePartnersRequest_Prefix {
			get {
				return 'mdm';
			}
		}
		
        public virtual override String getName() { return 'changePartnersRequest'; }
		public List<MDM_Account_Merge__c> MergedAccounts { get; set; }
		        
        public virtual override DOM.Document getSoapBody() {
            return getSoapBody(this.getName(), MDMChangePartnersRequest_Namespace, MDMChangePartnersRequest_Prefix);
        }
		
        public DOM.Document getSoapBody(string method, String namespace, String prefix) {
			system.debug('*** method: '+method);
            DOM.Document body = new DOM.Document();
			DOM.XMLNode root = body.createRootElement(MDMChangePartnersRequest_Label, MDMChangePartnersRequest_Namespace, MDMChangePartnersRequest_Prefix);		

			//Get XML element list from Custom Setting
			List<MDM_Service_Fields__c> serviceFields = [
				SELECT Name, Request_Name__c, Sequence__c, Namespace_Prefix__c, Parent_Node__c, Type__c, XmlLabel__c, Source_Object__c, Field_Name__c, Address_Type__c, Omit_Node_If_Null__c, Strip_Special_Characters__c, Strip_Whitespaces__c
				FROM MDM_Service_Fields__c 
				WHERE Request_Name__c = :method
				ORDER BY Sequence__c];
			
            for(MDM_Account_Merge__c m : this.MergedAccounts) {
				DOM.XmlNode partnerXml = root.addChildElement(method, namespace, prefix);
				appendSObjectXml(partnerXml, m, serviceFields);
            }
            return body;
        }
    }
    	
	public virtual class MDMChangeResponse { 
		public List<MDMResponse> mdmResponses { get; set; }
		
		public MDMChangeResponse() {
			mdmResponses = new List<MDMResponse>();
		}
		
		protected void Deserialize(Dom.XmlNode root, String parentNode) {
			try {
				for  (Dom.XmlNode node : root.getChildElements()) {
					try {
						XmlSerializer s = new XmlSerializer();
			            object o = s.Deserialize(node, 'MDMMergePartnerProxy.MDMResponse');
			            mdmResponses.add((MDMResponse)o);
					} catch (Exception ex) {
						//TODO: handle exceptions better...
						//Don't let one failure fail the whole batch...?
					}
				}
			} catch (Exception ex) {
				//TODO: handle exceptions better...
				throw ex;
			}
		}
	}
	
	public class MDMResponse implements XmlSerializable {
		public Boolean Success { get; set; }
		public String Id { get; set; }
		public String Error { get; set; }
		
		public object get(string fieldName) {
	        Map<string, object> thisobjectmap = new Map<string, object> 
	        { 
	            'id' => this.Id, 
	            'success' => this.Success,
				'error' => this.Error
	        };
	        return thisobjectmap.get(fieldname);
	    }
	    
	    public boolean put(string fieldName, object value) {
	        if (fieldName == 'id') {
	            this.Id = String.valueOf(value);
	        } else if (fieldName == 'success') {
	            this.Success = Boolean.valueOf(value);  
			} else if (fieldName == 'error') {
	            this.Error = String.valueOf(value);
	        } else {
	            return false;
	        }
	        return true;
	    }
	    
	    public Set<string> getFields() {
	        Set<string> fields = new Set<string> 
	        { 
	            'success',
				'id',
				'error'
	        };
	        
	        return fields;
	    }
	}
	
	
	public class MDMMergePartnerRequest extends MDMPartnerRequest {
		public override String getName() { return 'mergePartnerRequest'; }
	}
	
	public class MDMMergePartnerResponse extends MDMChangeResponse {
		public void Deserialize(Dom.XmlNode root) {
			Deserialize(root, 'MDMMergePartnerProxy.MDMChangeResponse');
		}
	}

}