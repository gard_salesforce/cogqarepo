public class MassUpdateOppLiController{

    public PageReference OppPage() {
        PageReference oppPage = new pageReference ('/apex/BulkApprovalNew');
        oppPage.setRedirect(true);
        return oppPage;
    }

    public List<OpportunityLineItem> OpportunityLineItems{get;set;}
    string userid = UserInfo.getUserId();
    public integer currPage {get;set;}
    public integer pageSize {get;set;}
    public integer recCount {get;set;}
    public boolean refreshList{get;set;}
    public string filterText {get;set;}
    private static final Integer UNIT_PRICE_DEFAULT_VALUE = 1; //SF-4533 enter the Identity value here
    
    //public Date oppCloseDate{get;set;}
    public opportunity opportunityCD {get;set;}{opportunityCD = new opportunity();}
    //SF-3732-sort by close date
    public String orderBy{get;set;}//{orderBy = '';}
    private String sortDirection = 'ASC';
    private String sortExp = '';  
    private static String Ascending='ASC';
    public String orderByNew
    {
     get
         {
            return sortExp;
         }
         set
         {
           //if the column is clicked on then switch between Ascending and Descending modes
           if (value == sortExp)
             sortDirection = (sortDirection.contains('DESC')? Ascending : 'DESC');
           else
             sortDirection = Ascending;
             sortExp = value;
         }
     }
     public String getSortDirection()
     {
        //if not column is selected 
        if (orderByNew == null || orderByNew == '')
          return Ascending;
        else
         return sortDirection;
     }
     public void setSortDirection(String value)
     {  
       sortDirection = value;
     }
    //
    //MG cDecisions make the budget year selection dynamic so we don't have to update the vf page with new years. Just adding to the picklist should be sufficient
    //
    public List<SelectOption> budgetYearList{
        get{
            if(null==budgetYearList){
                budgetYearList = new List<SelectOption>();
                Schema.DescribeFieldResult F = Opportunity.Budget_Year__c.getDescribe();
                List<Schema.PicklistEntry> P = F.getPicklistValues();
                for(Schema.PicklistEntry pVal: P){
                    budgetYearList.add(new SelectOption(pVal.getValue(), pVal.getLabel()));
                }
            }
            return budgetYearList;
        }
        set;
    }
    
    
    public List<SelectOption> productFamilyList {
        get {
            if (null==productFamilyList) {
            
                productFamilyList = new List<SelectOption>();
                productFamilyList.add(new SelectOption('All', 'Show all'));

                Schema.DescribeSObjectResult opportunityDescription = Opportunity.SObjectType.getDescribe();
                List<Schema.RecordTypeInfo> recordTypeDescriptions = opportunityDescription.getRecordTypeInfos();
                
                for (Schema.RecordTypeInfo rti : recordTypeDescriptions) {
                    if (rti.getName() != 'Master') {
                        productFamilyList.add(new SelectOption(rti.getRecordTypeId(),rti.getName()));
                    }
                }

                
               /* Schema.DescribeFieldResult fieldDescription = Product2.Product_Type__c.getDescribe();
                List<Schema.PicklistEntry> picklistEntries = fieldDescription.getPicklistValues();
                
                for (Schema.PicklistEntry picklistValue : picklistEntries) {
                    productFamilyList.add(new SelectOption(picklistValue.getValue(), picklistValue.getLabel()));
                }*/
            }
            return productFamilyList;
        }
        set;
    }
    
    public string budgetYear {
        get {
            return budgetYear;}
        set {
            budgetYear = value;
            System.Debug('MassUpdateOppLiController.Set budget year to ' + budgetYear);
            if (budgetYear!=budgetYearPrevious) {
                //OpportunityLineItems = getOpportunityLineItems();
                 refreshList=True;
                budgetYearPrevious = budgetYear;
            }
        }
    }
    
    public string productFamily {
        get {
            return productFamily;
        }
        set {
            productFamily = value;
            System.Debug('MassUpdateOppLiController.Set product family to ' + productFamily);
            if (productFamily != productFamilyPrevious) {
                refreshList = true;
                productFamilyPrevious = productFamily;
            }
        }
    }
    
    public string productFamilyPrevious {get;set;}
    
    public string budgetYearPrevious {get;set;}   
    public boolean savePrognosisHistory{get;set;} 
    public MassUpdateOppLiController() {
        orderBy = '';
        orderByNew = 'Opportunity.Name';
        opportunityCD.closeDate = date.newinstance(date.today().year(), date.today().month(), date.today().day());
        //opportunityCD.closeDate = date.today();
        //oppCloseDate = date.today();
        currPage = 0;
        pageSize = 30;
        recCount = 0;
        refreshList = false;
        filterText = '';
        OpportunityLineItems = new List<OpportunityLineItem>();
        //budgetYear='2013';
        //MG cDecisions 19/08/2013 Change budget year to be controlled by a custom setting
        budgetYear=PrognosisConfigSupport.GetSetting(UserInfo.getUserId()).Prognosis_Year__c;
        System.Debug('*** MassUpdateOppLiController.Budget Year =' + budgetYear);
        savePrognosisHistory = PrognosisConfigSupport.GetSetting(UserInfo.getUserId()).Enable_User_History__c;
        System.Debug('MassUpdateOppLiController.savePrognosisHistory = ' + savePrognosisHistory);
        PopulateOpportunityLineItems();
    }
    
    //MG cDecisions PCI-000135 Remove filter on stage Name - budget set from soql. Look for open opps instead
    public void PopulateOpportunityLineItems() {
        system.debug('--orderByNew-----'+orderByNew+'----sortDirection--'+sortDirection);
        //system.debug('opportunityCD.closeDate----b4---'+opportunityCD.closeDate);
        String queryStr;
         system.debug('refreshList----OpportunityLineItems-'+refreshList+OpportunityLineItems);
        if(OpportunityLineItems == null || refreshList == true) {
                system.debug('refreshList-----'+refreshList);
            String filterValue = filterText.Length() < 2 ? '%' : '%' + filterText + '%';
        
            
        
            if (productFamily == 'All' || productFamily == null) {
                system.debug('filterValue ----from if-----'+filterValue);
                system.debug('budgetYear  ----from if-----'+budgetYear );
                system.debug('userId ----from if-----'+userId);
                
                //MA_C added as SF-3680 26.9.16
             //   system.debug('**sortExpression+sortDirection*IF** : '+ sortExpression + ' ' +sortDirection);
                queryStr = 'SELECT   id, opportunityId,product_name__c,actual_premium__c,expiry_date__c,general_increase__c,RA__c,MA__c,volume_adjustment__c,record_adjustment__c,renewable_probability__c,Prognosis_EPI__c,Premium_Development_pc__c, renewal_premium__c,Opportunity.Name,budget_status__c,LR__c,Prognosis_Variance__c,Prognosis_Comments__c, Opportunity.CloseDate, Opportunity.Budget_Year__c,Opportunity.OwnerId, UnitPrice, Index__c FROM     opportunitylineitem ' ;
                //sfEdit SF-4533 test restore  next line
                queryStr = queryStr +' '+ ' WHERE    Opportunity.IsClosed = false AND      Opportunity.Type = \'Renewal\' AND Opportunity.Budget_Year__c =:budgetYear AND  Opportunity.OwnerId = :userId AND      Opportunity.Name LIKE :filterValue ' ;
                //queryStr = queryStr +' '+ ' WHERE    Opportunity.IsClosed = false AND Opportunity.Type = \'Renewal\' AND Opportunity.Budget_Year__c =:budgetYear AND Opportunity.Name LIKE :filterValue' ;//AND  Opportunity.OwnerId = :userId //sfEdit SF-4533 remove it
  /*:opportunityCD.closeDate */         
            } else {system.debug('refreshList--else---'+refreshList);
                //system.debug('filterValue ----from else-----'+filterValue);
                //system.debug('budgetYear ----from else-----'+budgetYear );
                //system.debug('userId ----from else-----'+userId);
                //system.debug('productFamily ----from else-----'+productFamily);
                
                //MA_C added as SF-3680 26.9.16
              //  system.debug('**sortExpression+sortDirection*ELSE** : '+ sortExpression + ' ' +sortDirection);
                queryStr = 'SELECT   id, opportunityId,product_name__c,actual_premium__c,expiry_date__c,general_increase__c,RA__c,MA__c,volume_adjustment__c,record_adjustment__c,renewable_probability__c,Prognosis_EPI__c,Premium_Development_pc__c, renewal_premium__c,Opportunity.Name,budget_status__c,LR__c,Prognosis_Variance__c,Prognosis_Comments__c, Opportunity.CloseDate, Opportunity.Budget_Year__c,Opportunity.OwnerId, UnitPrice, Index__c FROM opportunitylineitem ' ;
                queryStr = queryStr +' '+ ' WHERE  Opportunity.IsClosed = false AND      Opportunity.Type = \'Renewal\' AND Opportunity.Budget_Year__c =:budgetYear AND   Opportunity.OwnerId = :userId AND   Opportunity.Name LIKE :filterValue AND      Opportunity.recordTypeId = :productFamily '  ;
            }
            system.debug('orderByNew******** : '+ orderByNew);
            if(orderByNew != null && orderByNew != ''){
                 orderBy = ' ORDER BY ' + orderByNew + ' ' + sortDirection;  
                 system.debug('orderBy******** : '+ orderBy);
                 queryStr = queryStr+ ' ' +orderBy ; 
               // queryStr = queryStr + ' ' +'ORDER BY '+' '+ sortExpression + ' '+ sortDirection ;
            }
            else {
                system.debug('queryStr******** : '+ queryStr);
                //sfEdit SF-4533 rewrote next line
                queryStr = queryStr +' '+ ' ORDER BY Opportunity.Name,product_name__c LIMIT 1000' ;
                //queryStr = queryStr +' '+ ' ORDER BY Opportunity.Name,product_name__c' ;//sfEdit SF-4533 remove it
            }
            //queryStr+=' LIMIT 1000'; sfEdit SF-4533 remove it
            system.debug('queryStr********OpportunityLineItems  : '+ queryStr);
            OpportunityLineItems = database.query(queryStr);
        }
        else{
            String filterValue = filterText.Length() < 2 ? '%' : '%' + filterText + '%';
        
            
        
            if (productFamily == 'All' || productFamily == null) {
                system.debug('filterValue ----from if-----'+filterValue);
                system.debug('budgetYear  ----from if-----'+budgetYear );
                system.debug('userId ----from if-----'+userId);
                
                //MA_C added as SF-3680 26.9.16
             //   system.debug('**sortExpression+sortDirection*IF** : '+ sortExpression + ' ' +sortDirection);
                queryStr = 'SELECT   id, opportunityId,product_name__c,actual_premium__c,expiry_date__c,general_increase__c,RA__c,MA__c,volume_adjustment__c,record_adjustment__c,renewable_probability__c,Prognosis_EPI__c,Premium_Development_pc__c, renewal_premium__c,Opportunity.Name,budget_status__c,LR__c,Prognosis_Variance__c,Prognosis_Comments__c, Opportunity.CloseDate, Opportunity.Budget_Year__c,Opportunity.OwnerId, UnitPrice, Index__c FROM     opportunitylineitem ' ;
                queryStr = queryStr +' '+ ' WHERE    Opportunity.IsClosed = false AND      Opportunity.Type = \'Renewal\' AND Opportunity.Budget_Year__c =:budgetYear AND  Opportunity.OwnerId = :userId AND      Opportunity.Name LIKE :filterValue ' ;
                
  /*:opportunityCD.closeDate */         
            } else {system.debug('refreshList--else---'+refreshList);
                //system.debug('filterValue ----from else-----'+filterValue);
                //system.debug('budgetYear ----from else-----'+budgetYear );
                //system.debug('userId ----from else-----'+userId);
                //system.debug('productFamily ----from else-----'+productFamily);
                
                //MA_C added as SF-3680 26.9.16
              //  system.debug('**sortExpression+sortDirection*ELSE** : '+ sortExpression + ' ' +sortDirection);
                queryStr = 'SELECT   id, opportunityId,product_name__c,actual_premium__c,expiry_date__c,general_increase__c,RA__c,MA__c,volume_adjustment__c,record_adjustment__c,renewable_probability__c,Prognosis_EPI__c,Premium_Development_pc__c, renewal_premium__c,Opportunity.Name,budget_status__c,LR__c,Prognosis_Variance__c,Prognosis_Comments__c, Opportunity.CloseDate, Opportunity.Budget_Year__c,Opportunity.OwnerId, UnitPrice, Index__c FROM     opportunitylineitem ' ;
                queryStr = queryStr +' '+ ' WHERE    Opportunity.IsClosed = false AND      Opportunity.Type = \'Renewal\' AND Opportunity.Budget_Year__c =:budgetYear AND   Opportunity.OwnerId = :userId AND   Opportunity.Name LIKE :filterValue AND      Opportunity.recordTypeId = :productFamily '  ;
            }
            system.debug('orderByNew******** : '+ orderByNew);
            if(orderByNew != null && orderByNew != ''){
                 orderBy = ' ORDER BY ' + orderByNew + ' ' + sortDirection;  
                 system.debug('orderBy******** : '+ orderBy);
                 queryStr = queryStr+ ' ' +orderBy ; 
               // queryStr = queryStr + ' ' +'ORDER BY '+' '+ sortExpression + ' '+ sortDirection ;
            }
            else {
                system.debug('queryStr******** : '+ queryStr);
                queryStr = queryStr +' '+ ' ORDER BY Opportunity.Name,product_name__c LIMIT    1000' ;
            }
            system.debug('queryStr********OpportunityLineItems  : '+ queryStr);
            OpportunityLineItems = database.query(queryStr);
        }
                    
        recCount = opportunitylineitems.size();
        refreshList = false;
        currPage = 0;
        //return opportunitylineitems;
        
    }
    
    public void saveall() {
        if (OpportunityLineItems.size() > 0){
            //SF-4533 STARTS
            /*
            for(OpportunityLineItem oli : OpportunityLineItems){
                if(oli.unitPrice == null){
                    //oli.unitPrice = UNIT_PRICE_DEFAULT_VALUE;
                }
            }
			*/
            Database.update(OpportunityLineItems,false);
            populateOpportunityLineItems();
            //SF-4533 ENDS
            //update(OpportunityLineItems);//commented for SF-4550
            if(this.savePrognosisHistory){
                List<Prognosis__c> proglist = new List<Prognosis__c>();
                proglist = Prognosis.getPrognosisList(OpportunityLineItems);
                insert(proglist);
            }
        }
            
        refreshList = true;
    }
    public void pageNext() {
        if (recCount-currPage > pageSize)
            currPage += pageSize;
    }
    public void pagePrev() {
        if (currPage >= pageSize)
            currPage -= pageSize;
    }
    public void pageFirst() {
        currPage = 0;
    }
    public void setFilterText() {
        PopulateOpportunityLineItems();
        refreshList = true;
    }

}