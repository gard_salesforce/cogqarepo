@isTest(seeAllData=true)
public class ShowCaseRelatedFilesControllerTest {
    public static Case caseOne;
    public static List<Id> cdIds;
    public static List<Id> emIds;
    
    public static void createTestData(){
        //QueueInEmail__c qe = new QueueInEmail__c(name='Marine Support North', Email__c = 'testcc@test.com', Team__c='CT North');
        //insert qe;   
        
        caseOne = listBuilderModalControllerTest.createCases(1, false, true)[0];
        EmailMessage emailToCase = new EmailMessage();
        emailToCase.Subject = 'test';
        emailToCase.TextBody = 'test body';
        emailToCase.ParentId = caseOne.Id;
        insert emailToCase;
        emIds = new List<Id>();
        emIds.add(emailToCase.id);
        
        //insert files
        List<ContentVersion> cvList= new List<ContentVersion>();
        ContentVersion contentVersion1 = new ContentVersion(Title = 'Penguins',
                                                            PathOnClient = 'Penguins.jpg',
                                                            VersionData = Blob.valueOf('Test Content1'),
                                                            IsMajorVersion = true
                                                           );
        ContentVersion contentVersion2 = new ContentVersion(Title = 'Penguins',
                                                            PathOnClient = 'Penguins.jpg',
                                                            VersionData = Blob.valueOf('Test Content2'),
                                                            IsMajorVersion = true
                                                           );
        cvList.add(contentVersion1);
        cvList.add(contentVersion2);
        insert cvList;
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument order by createdDate desc limit 100]; 
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        
        ContentDocumentLink caseContentLink = new ContentDocumentLink();
        caseContentLink.LinkedEntityId = caseOne.id;
        caseContentLink.ShareType = 'I';
        caseContentLink.ContentDocumentId = documents[0].Id;
        caseContentLink.Visibility = 'AllUsers'; 
        
        ContentDocumentLink emailContentLink = new ContentDocumentLink();
        emailContentLink.LinkedEntityId = emailToCase.id;
        emailContentLink.ShareType = 'V';
        emailContentLink.ContentDocumentId = documents[1].Id;
        emailContentLink.Visibility = 'AllUsers'; 
        
        cdlList.add(caseContentLink);
        cdlList.add(emailContentLink);
        insert cdlList;
        
        cdIds = new List<Id>();
        for(ContentDocument cd : documents){
            cdIds.add(cd.id);
        }
        
        //listBuilderModalControllerTest.createContentDocumentsLinkedToRecord(1, emailToCase.Id);
        
        List<id> conDocId = new List<Id>{(contentVersion1.id),(contentVersion2.Id)};
        List<Id> emailId = new List<Id>{(emailToCase.Id)};
    }
    
    @isTest
    private static void testMethod1(){
        createTestData();        
        showCaseRelatedFilesController.fetchCaseFiles(caseOne.Id,'cloneCase');
        showCaseRelatedFilesController.fetchCaseFiles(caseOne.Id,'sendFiles');
    }
    
     @isTest
    private static void testMethod2(){
        createTestData();        
        showCaseRelatedFilesController.cloneCase(caseOne.Id,cdIds,emIds);
    }
    
}