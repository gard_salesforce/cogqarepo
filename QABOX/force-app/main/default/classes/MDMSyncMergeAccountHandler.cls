/**************************************************************************
* Author  : Phil Spenceley
* Company : cDecisions
* Date    : 13/11/2013
***************************************************************************/
    
/// <summary>
///  Handler class for the MDM_Account_Merge__c.MDMSyncMergedAccount trigger
/// </summary>
public without sharing class MDMSyncMergeAccountHandler{

	public static void InsertMergeAccountSyncStatus(List<MDM_Account_Merge__c> newMergeAccounts, Boolean webServicesEnabled) {
		for(MDM_Account_Merge__c a : newMergeAccounts) {
			if (webServicesEnabled) {
				if (System.IsBatch() || System.IsFuture()) {
					a.Synchronisation_Status__c = 'Waiting To Sync';
					a.Sync_Date__c = Datetime.now().addMinutes(-59);
				} else {
					a.Synchronisation_Status__c = 'Sync In Progress';
				}
			} else {
				a.Synchronisation_Status__c = 'Synchronised';
			}					
        }
	}
	
	public static void SyncMergePartner(List<MDM_Account_Merge__c> newMergeAccounts, Map<id, MDM_Account_Merge__c> newMergeAccountsMap, Boolean webServicesEnabled) {
		List<id> mergedAccountIds = new List<id>();
		mergedAccountIds.addAll(newMergeAccountsMap.keyset());
		if (webServicesEnabled) {
			if (!System.IsBatch() && !System.IsFuture()) {
				MDMMergePartnerProxy.MDMMergePartnerAsync(mergedAccountIds);
			}
		}
	}
	
	
	public static void BeforeInsert(List<MDM_Account_Merge__c> newMergeAccounts, Map<id, MDM_Account_Merge__c> newMergeAccountsMap) {
		try {
			MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
			InsertMergeAccountSyncStatus(newMergeAccounts, mdmSettings.Web_Services_Enabled__c);
		} catch (Exception ex) {
            Logger.LogException('MDMSyncMergeAccountHandler.BeforeInsert', ex);
			throw ex;
        }
	}
	
	public static void AfterInsert(List<MDM_Account_Merge__c> newMergeAccounts, Map<id, MDM_Account_Merge__c> newMergeAccountsMap) {
		try {
			MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
			SyncMergePartner(newMergeAccounts, newMergeAccountsMap, mdmSettings.Web_Services_Enabled__c);
		} catch (Exception ex) {
            Logger.LogException('MDMSyncMergeAccountHandler.AfterInsert', ex);
			throw ex;
        }
	}
	
	public static void BeforeUpdate(List<MDM_Account_Merge__c> newMergeAccounts, Map<id, MDM_Account_Merge__c> newMergeAccountsMap, List<MDM_Account_Merge__c> oldMergeAccounts, Map<id, MDM_Account_Merge__c> oldMergeAccountsMap) {
		try {
			MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
		} catch (Exception ex) {
            Logger.LogException('MDMSyncMergeAccountHandler.BeforeUpdate', ex);
			throw ex;
        }
		
	}
	
	public static void AfterUpdate(List<MDM_Account_Merge__c> newMergeAccounts, Map<id, MDM_Account_Merge__c> newMergeAccountsMap, List<MDM_Account_Merge__c> oldMergeAccounts, Map<id, MDM_Account_Merge__c> oldMergeAccountsMap) {
		try {
			MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
			List<MDM_Account_Merge__c> newMergeAccountsToSync = new List<MDM_Account_Merge__c>();
			for (MDM_Account_Merge__c m : newMergeAccounts) {
				MDM_Account_Merge__c oldM = oldMergeAccountsMap.get(m.id);
				if (m.Synchronisation_Status__c != oldM.Synchronisation_Status__c && m.Synchronisation_Status__c == 'Sync In Progress') {
					newMergeAccountsToSync.add(m);
				}
			}
			if (newMergeAccountsToSync.size() > 0) {
				SyncMergePartner(newMergeAccountsToSync, new Map<id, MDM_Account_Merge__c> (newMergeAccountsToSync), mdmSettings.Web_Services_Enabled__c);
			}
		
		} catch (Exception ex) {
            Logger.LogException('MDMSyncMergeAccountHandler.AfterUpdate', ex);
			throw ex;
        }
		
	}
	
	public static void BeforeDelete(List<MDM_Account_Merge__c> oldMergeAccounts, Map<id, MDM_Account_Merge__c> oldMergeAccountsMap) {
		
	}
	
	public static void AfterDelete(List<MDM_Account_Merge__c> oldMergeAccounts, Map<id, MDM_Account_Merge__c> oldMergeAccountsMap) {
		
	}

}