public class PostRefreshUpdateBatchHelper{
    //Process-Helper Methods START
    public static void maskAccountEmails(List<Account> inputAccounts,UtilityData utilData){
        List<Account> modifiedAccounts = new List<Account>();
        for(Account inputAccount : inputAccounts){
            /* Commented for SF-6417
            if(!String.isEmpty(inputAccount.Email__c)){
                inputAccount.Email__c = utilData.maskEmailId;
            }
            */
            if(!String.isEmpty(inputAccount.Corporate_email__c)){
                inputAccount.Corporate_email__c = utilData.maskEmailId;
            }
            /*    // Commented for SF-6417
            if(!String.isEmpty(inputAccount.Email_1__c)){
                inputAccount.Email_1__c = utilData.maskEmailId;
            }*/
            if(!String.isEmpty(inputAccount.Email_Address_2__c)){
                inputAccount.Email_Address_2__c = utilData.maskEmailId;
            }
            if(!String.isEmpty(inputAccount.E_Mail_Address_3__c)){
                inputAccount.E_Mail_Address_3__c = utilData.maskEmailId;
            }
            if(!String.isEmpty(inputAccount.Initial_Submitter__c)){
                inputAccount.Initial_Submitter__c = utilData.maskEmailId;
            }
            
            modifiedAccounts.add(inputAccount);
        }
        
        if(!modifiedAccounts.isEmpty()) update modifiedAccounts;//Database.update(modifiedAccounts,false);//update modifiedAccounts;
    }
    
    public static void maskCaseEmails(List<Case> inputCases,UtilityData utilData){
        List<Case> modifiedCases = new List<Case>();
        
        for(Case inputCase : inputCases){
            if(!String.isEmpty(inputCase.suppliedEmail)){
                inputCase.suppliedEmail = utilData.maskEmailId;
            }
            if(!String.isEmpty(inputCase.Claim_Submitter_Email__c)){
                inputCase.Claim_Submitter_Email__c = utilData.maskEmailId;
            }
            if(!String.isEmpty(inputCase.Email_UwForm__c)){
                inputCase.Email_UwForm__c = utilData.maskEmailId;
            }
            if(!String.isEmpty(inputCase.Email_address__c)){
                inputCase.Email_address__c = utilData.maskEmailId;
            }
            if(!String.isEmpty(inputCase.E_mail_address1__c)){
                inputCase.E_mail_address1__c = utilData.maskEmailId;
            }
            if(!String.isEmpty(inputCase.E_mail_address__c)){
                inputCase.E_mail_address__c = utilData.maskEmailId;
            }
            if(!String.isEmpty(inputCase.Email_address_MRF__c)){
                inputCase.Email_address_MRF__c = utilData.maskEmailId;
            }
            
            modifiedCases.add(inputCase);
        }
        
        if(!modifiedCases.isEmpty()) update modifiedCases;
    }
    
    public static void maskContactEmails(List<Contact> inputContacts,UtilityData utilData){
        List<Contact> modifiedContacts = new List<Contact>();
        for(Contact inputContact : inputContacts){
                        
            if(!String.isEmpty(inputContact.Email)){
                inputContact.Email = utilData.maskEmailId;
            }
            // Commented for SF-6417
            /*
            if(!String.isEmpty(inputContact.Email_1__c)){
                inputContact.Email_1__c = utilData.maskEmailId;
            }*/
            if(!String.isEmpty(inputContact.Email_2__c)){
                inputContact.Email_2__c = utilData.maskEmailId;
            }
            if(!String.isEmpty(inputContact.Email_3__c)){
                inputContact.Email_3__c = utilData.maskEmailId;
            }
            if(!String.isEmpty(inputContact.Email_4__c)){
                inputContact.Email_4__c = utilData.maskEmailId;
            }
            if(!String.isEmpty(inputContact.Email_5__c)){
                inputContact.Email_5__c = utilData.maskEmailId;
            }
            
            modifiedContacts.add(inputContact);
        }
        
        if(!modifiedContacts.isEmpty()) update modifiedContacts;//Database.update(modifiedContacts,false);//
    }
    
    public static void maskGardContactsEmails(List<Gard_Contacts__c> inputGardContacts,UtilityData utilData){
        List<Gard_Contacts__c> modifiedGardContacts = new List<Gard_Contacts__c>();
        for(Gard_Contacts__c inputGardContact : inputGardContacts){
            
            if(!String.isEmpty(inputGardContact.Email__c)){
                inputGardContact.Email__c = utilData.maskEmailId;
            }
            
            modifiedGardContacts.add(inputGardContact);
        }
        
        if(!modifiedGardContacts.isEmpty()) update modifiedGardContacts;//Database.update(modifiedGardContacts,false);//
    }
    
    public static void maskGardTeamEmails(List<Gard_Team__c> inputGardTeams,UtilityData utilData){
        List<Gard_Team__c> modifiedGardTeams = new List<Gard_Team__c>();
        for(Gard_Team__c inputGardTeam : inputGardTeams){
            
            if(!String.isEmpty(inputGardTeam.P_I_email__c)){
                inputGardTeam.P_I_email__c = utilData.maskEmailId;
            }
            if(!String.isEmpty(inputGardTeam.Marine_Email__c)){
                inputGardTeam.Marine_Email__c = utilData.maskEmailId;
            }            
            
            modifiedGardTeams.add(inputGardTeam);
        }
        
        if(!modifiedGardTeams.isEmpty()) update modifiedGardTeams;//Database.update(modifiedGardTeams,false);//
    }
    
    public static void onRiskContracts(List<Contract> inputContracts,UtilityData utilData){
        List<Account> clientAccounts = new List<Account>();Integer clientIndex = 0;
        List<Account> brokerAccounts = new List<Account>();Integer brokerIndex = 0;
        for(Account account : [SELECT id,company_role__c FROM Account WHERE company_role__c IN ('Client') AND Active__c = true LIMIT 10]) clientAccounts.add(account);
        for(Account account : [SELECT id,company_role__c FROM Account WHERE company_role__c IN ('Broker') AND Active__c = true LIMIT 10]) brokerAccounts.add(account);
        
        List<Contract> modifiedContracts = new List<Contract>();
        for(Contract inputContract : inputContracts){
            
            if((inputContract.client__c == null || inputContract.accountId == null)){
                Id clientAccountId;
                if(!clientAccounts.isEmpty() && inputContract.client__c == null && inputContract.accountId == null){
                    clientAccountId = clientAccounts.get(clientIndex).Id;
                    clientIndex = clientIndex == clientAccounts.size()-1 ? 0 : clientIndex+1;
                }else{
                     clientAccountId = inputContract.client__c != null ? inputContract.client__c : inputContract.accountId;
                }
                inputContract.client__c = clientAccountId;
                inputContract.accountId = clientAccountId;
            }
            
            if(!brokerAccounts.isEmpty() && inputContract.broker__C == null && inputContract.Shared_with_Client__c){
                inputContract.broker__c = brokerAccounts.get(brokerIndex).Id;
                brokerIndex = brokerIndex == brokerAccounts.size()-1 ? 0 : brokerIndex+1;
            }
            
            inputContract.shared_with_client__c = true;
            inputContract.status = 'Activated';
            inputContract.Inception_Date__C = DateTime.now().Date().addYears(-1);
            inputContract.Expiration_Date__c = DateTime.now().Date().addYears(1);
            
            modifiedContracts.add(inputContract);
        }
        
        if(!modifiedContracts.isEmpty()) update modifiedContracts;
    }
    
    public static void onRiskAssets(List<Asset> inputAssets,UtilityData utilData){
        List<Asset> modifiedAssets = new List<Asset>();
        List<Object__c> randomObjects = [SELECT id FROM Object__c ORDER BY Name LIMIT 100];
        List<Contract> randomPiContracts = new List<Contract>();
        List<Contract> randomMarineContracts = new List<Contract>();
        for(Contract con : [SELECT id,Inception_Date__c,Expiration_Date__c,Business_Area__c FROM Contract WHERE Business_Area__c = 'Marine' AND status = 'Activated' ORDER BY Expiration_Date__c DESC,Inception_Date__c DESC,LastModifiedDate DESC LIMIT 50])randomMarineContracts.add(con);
        for(Contract con : [SELECT id,Inception_Date__c,Expiration_Date__c,Business_Area__c FROM Contract WHERE Business_Area__c != 'Marine' AND status = 'Activated' ORDER BY Expiration_Date__c DESC,Inception_Date__c DESC,LastModifiedDate DESC LIMIT 50])randomPiContracts.add(con);
        
        Integer objIndex = 0;
        Integer conPiIndex = 0;
        Integer conMarineIndex = 0;
        for(Asset inputAsset : inputAssets){
            Boolean piAsset = inputAsset.name.containsIgnoreCase('P&I Cover') || inputAsset.name.containsIgnoreCase('Defence Cover');
            if(inputAsset.agreement__c == null || (piAsset && inputAsset.Agreement__r.Business_area__c != 'P&I')){
                Contract parentContract = piAsset ? randomPiContracts.get(conPiIndex) : randomMarineContracts.get(conMarineIndex);
                if(piAsset) conPiIndex = conPiIndex == randomPiContracts.size()-1 ? 0 : conPiIndex+1;
                else conMarineIndex = conMarineIndex == randomMarineContracts.size()-1 ? 0 : conMarineIndex+1;
                
                inputAsset.Agreement__c = parentContract.Id;
                inputAsset.Inception_Date__c = parentContract.Inception_Date__c;
                inputAsset.Expiration_Date__c = parentContract.Expiration_Date__c;
            }else{
                
                inputAsset.Inception_Date__c = DateTime.now().Date().addYears(-1);
                inputAsset.Expiration_Date__c = DateTime.now().Date().addYears(1);
            }
            
            inputAsset.On_risk_indicator__c = true;
            
            //if(true || inputAsset.object__c == null){
            inputAsset.object__c = randomObjects.get(objIndex).Id;
            objIndex = objIndex == randomObjects.size()-1 ? 0 : objIndex+1;
            //}
            modifiedAssets.add(inputAsset);
            
        }
        
        //if(!modifiedAssets.isEmpty()) update modifiedAssets;
        if(!modifiedAssets.isEmpty()) Database.update(modifiedAssets,false);
        
    }
    
    //Process-Helper Methods END
    
    //Creates and returns queryString for all jobTypes
    public static String getQueryStrings(String sObjectName,String service){
        String queryString;
        //List<String> queryStrings = new List<String>();
        
        if(sObjectName.equalsIgnoreCase('account') && service.equalsIgnoreCase('emailMasking')){
                queryString = 'SELECT'
                +' Id,Corporate_email__c,Email_Address_2__c,E_Mail_Address_3__c,Initial_Submitter__c'    //Email_1__c Commented for SF-6417
                +' FROM Account'
                +' WHERE Corporate_email__c != NULL'
                //+' OR Email_1__c != NULL'    // Commented for SF-6417
                +' OR Email_Address_2__c != NULL'
                +' OR E_Mail_Address_3__c != NULL'
                +' OR Initial_Submitter__c != NULL'; //Email__c != NULL Commented for SF-6417
            //queryStrings.add(queryString);
        }else if(sObjectName.equalsIgnoreCase('case') && service.equalsIgnoreCase('emailMasking')){
            Schema.DescribeSObjectResult dSOR = Case.SObjectType.getDescribe();
            Id claimRecordTypeId = dSOR.getRecordTypeInfosByDeveloperName().get('Claim').getRecordTypeId();
            queryString = 'SELECT'
                +' Id,suppliedEmail,Claim_Submitter_Email__c,Email_UwForm__c,Email_address__c,E_mail_address1__c,E_mail_address__c,Email_address_MRF__c'
                +' FROM Case'
                +' WHERE RecordTypeId != \''+claimRecordTypeId+'\''
                +' AND (suppliedEmail != NULL'
                +' OR Claim_Submitter_Email__c != NULL'
                +' OR Email_UwForm__c != NULL'
                +' OR Email_address__c != NULL'
                +' OR E_mail_address1__c != NULL'
                +' OR E_mail_address__c != NULL'
                +' OR Email_address_MRF__c != NULL)';
            //queryStrings.add(queryString);
        }else if(sObjectName.equalsIgnoreCase('contact') && service.equalsIgnoreCase('emailMasking')){
            queryString = 'SELECT'
                +' Id, Email,Email_1__c,Email_2__c,Email_3__c,Email_4__c,Email_5__c'
                +' FROM Contact'
                +' WHERE Email != Null'
                +' OR Email_1__c != Null'
                +' OR Email_2__c != Null'
                +' OR Email_3__c != Null'
                +' OR Email_4__c != Null'
                +' OR Email_5__c != Null';
        }else if(sObjectName.equalsIgnoreCase('Gard_Contacts__c') && service.equalsIgnoreCase('emailMasking')){
            queryString = 'SELECT'
                +' Id, Email__c'
                +' FROM Gard_Contacts__c'
                +' WHERE Email__c != Null';
        }else if(sObjectName.equalsIgnoreCase('Gard_Team__c') && service.equalsIgnoreCase('emailMasking')){
            queryString = 'SELECT'
                +' Id,P_I_email__c,Marine_Email__c'
                +' FROM Gard_Team__c'
                +' WHERE P_I_email__c != Null'
                +' OR Marine_Email__c != Null';
        }else if(sObjectName.equalsIgnoreCase('Contract') && service.equalsIgnoreCase('onRisking')){
            queryString = 'SELECT'
                +' Id,AccountId,Client__c,Broker__c,Inception_date__c,Expiration_Date__c,Business_area__c,Shared_With_Client__c'
                +' FROM Contract';
        }else if(sObjectName.equalsIgnoreCase('Asset') && service.equalsIgnoreCase('onRisking')){
            queryString = 'SELECT'
                +' Id,Name,Inception_date__c,Expiration_Date__c,On_risk_indicator__c,Object__c,Agreement__c,Agreement__r.Business_area__c'
                +' FROM Asset';
        }
        
        return queryString;
    }
    
    //Conversion-Helper methods START
    public static List<Account> sObjectsToAccounts(List<SObject> inputRecords){
        List<Account> retAccounts = new List<Account>();
        for(sObject inputRecord : inputRecords) retAccounts.add((Account) inputRecord);
        return retAccounts;
    }
    
    public static List<Case> sObjectsToCases(List<Case> inputRecords){
        List<Case> retCases = new List<Case>();
        for(sObject inputRecord : inputRecords) retCases.add((Case) inputRecord);
        return retCases;
    }
    
    public static List<Contact> sObjectsToContacts(List<SObject> inputRecords){
        List<Contact> retContacts = new List<Contact>();
        for(sObject inputRecord : inputRecords) retContacts.add((Contact) inputRecord);
        return retContacts;
    }
    
    public static List<Gard_Contacts__c> sObjectsToGardContacts(List<SObject> inputRecords){
        List<Gard_Contacts__c> retContacts = new List<Gard_Contacts__c>();
        for(sObject inputRecord : inputRecords) retContacts.add((Gard_Contacts__c) inputRecord);
        return retContacts;
    }
    
    public static List<Gard_Team__c> sObjectsToGardTeam(List<SObject> inputRecords){
        List<Gard_Team__c> retGardTeams = new List<Gard_Team__c>();
        for(sObject inputRecord : inputRecords) retGardTeams.add((Gard_Team__c) inputRecord);
        return retGardTeams;
    }
    
    public static List<Contract> sObjectsToContracts(List<SObject> inputRecords){
        List<Contract> retContracts = new List<Contract>();
        for(sObject inputRecord : inputRecords) retContracts.add((Contract) inputRecord);
        return retContracts;
    }
    
    public static List<Asset> sObjectsToAssets(List<SObject> inputRecords){
        List<Asset> retAssets = new List<Asset>();
        for(sObject inputRecord : inputRecords) retAssets.add((Asset) inputRecord);
        return retAssets;
    }
    
    //Conversion-Helper methods START
}