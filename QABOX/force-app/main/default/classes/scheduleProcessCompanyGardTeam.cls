global class scheduleProcessCompanyGardTeam implements Schedulable {
    global void execute (SchedulableContext SC) {
        processCompanyGardTeam pc = new processCompanyGardTeam();
        
        pc.Query ='SELECT Id, Accounting__c, Accounting_P_I__c, UW_Assistant_1__c, U_W_Assistant_2__c,UW_Assistant_Energy__c, Claim_handler_Energy__c, Claim_handler_Marine__c, '+
                 // 4619 added the fields
                'Claim_handler_Cargo_Dry__c, Claim_handler_Cargo_Liquid__c, Claim_handler_CEP__c, Claim_handler_Charterers__c, '+
                'Claim_handler_Crew__c, Claim_handler_Defence__c, Claims_handler_Builders_Risk__c, X2nd_UWR__c,X3rd_UWR__c,Underwriter_4__c,' +
                'Key_Claims_Contact__c,Key_Claims_Contact__r.isActive,Claim_handler_Cargo_Dry__r.isActive,Claim_Adjuster_Marine_lk__c,'+ 
              'X2nd_UWR__r.isActive,X3rd_UWR__r.isActive,Underwriter_4__r.isActive,UW_Assistant_1__r.isActive,U_W_Assistant_2__r.isActive,UW_Assistant_Energy__r.isActive,Accounting_P_I__r.isActive,Accounting__r.isActive,'+
                //'Claim_handler_Cargo_Dry_2_lk__c,Claim_handler_Cargo_Liquid_2_lk__c,Claim_handler_CEP_2_lk__c,'+
                //'Claim_handler_Charterers_2_lk__c,Claim_handler_Defence_2_lk__c,Claim_handler_Energy_2_lk__c,'+
                //'Claim_handler_Marine_2_lk__c,Claim_handler_Crew_2_lk__c,Claims_handler_Builders_Risk_2_lk__c,'+
                //'Claim_handler_Cargo_Dry_2_lk__r.isActive,Claim_handler_Cargo_Liquid_2_lk__r.isActive,Claim_handler_CEP_2_lk__r.isActive,Claim_handler_Charterers_2_lk__r.isActive,Claim_handler_Defence_2_lk__r.isActive,'+
              //'Claim_handler_Energy_2_lk__r.isActive,Claim_handler_Marine_2_lk__r.isActive,Claim_handler_Crew_2_lk__r.isActive,Claims_handler_Builders_Risk_2_lk__r.isActive,'+
              'Claim_handler_Cargo_Liquid__r.isActive,Claim_handler_CEP__r.isActive,Claim_handler_Charterers__r.isActive,Claim_handler_Defence__r.isActive, '+
                'Claim_handler_Energy__r.isActive,Claim_handler_Marine__r.isActive,Claim_handler_Crew__r.isActive,'+
                'Claims_handler_Builders_Risk__r.isActive,Claim_Adjuster_Marine_lk__r.isActive,'+
                '(SELECT Source_System__c, Accounting__c, U_W_Assistant_1__c,U_W_Assistant_2__c,U_W_Assistant_Energy__c, Claim_handler_Energy__c, Claim_handler_Marine__c, '+
                'Claims_handler_Builders_Risk__c, Claim_handler_Cargo_Dry__c, Claim_handler_Cargo_Liquid__c, Claim_handler_CEP__c, '+
                'Claim_handler_Charterers__c, Claim_handler_Crew__c, Claim_handler_Defence__c, On_Risk__c, '+
                //SOC 24/4/13 PCI-000104 - Added secondary claims handler fields
                'Claim_handler_Energy_2__c, Claim_handler_Marine_2__c, Claims_handler_Builders_Risk_2__c, '+
                'Claim_handler_Cargo_Dry_2__c, Claim_handler_Cargo_Liquid_2__c, Claim_handler_CEP_2__c, '+
                'Claim_handler_Charterers_2__c, Claim_handler_Crew_2__c, Claim_handler_Defence_2__c, '+
                  
                  //SF 4368, the following two fields are added
                 'Office_Country__c, Gard_team__c, '+
                  
                  //SOC 4/9/13 PCI-000138 - Added claim adjuster marine
                'Claim_adjuster_Marine__c '+
                'from Admin_System_Companies__r where Company_Status__c = \'Active\' AND On_Risk__c=true '+
                'order by Source_System__c, GIC_Priority__c, Mapper_Number__c DESC, Company_ID__c DESC) '+
                  
                'from Account ';
        Database.executeBatch(pc, 20);
    }
    
    static testMethod void testSchedule() {
        Test.StartTest();
            scheduleProcessCompanyGardTeam testSched = new scheduleProcessCompanyGardTeam();
            String strCRON = '0 0 4 * * ?';
            System.schedule('scheduleProcessCompanyGardTeam Test', strCRON, testSched);
        Test.StopTest();
    }
}