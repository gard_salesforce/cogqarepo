@isTest
private class TestGardContactUtil {
    private static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    private static Gard_Contacts__c dummyGardContact;
    //User dummyUser;
    private static User testUser;
    private static GardContacts__c dummyCustomSetting ;
    
    public static void setupCustomSettingTestData()
    {
        dummyCustomSetting = new GardContacts__c(Email__c= 'companymail@gard.no',
                                            Office_city__c= 'Arendal',
                                            Phone__c ='+47 37019100',
                                            Name='DeactivatedUsers'
                                           );
        insert  dummyCustomSetting ;                                       
    }
    
    public static void setupTestData(){
    dummyGardContact= new Gard_Contacts__c(Email__c='test.test@gmail.com',
                                            FirstName__c='test firstname',
                                            isActive__c = true,
                                            LastName__c='test lastname',
                                            MobilePhone__c='11111',
                                            Nick_Name__c='test nick name',
                                            Office_city__c='zurich',
                                            Phone__c='22222',
                                            Title__c='test title'
                           );
        insert dummyGardContact;
        
        testUser = new User();
        testUser.Alias = 'standt';
        testUser.profileId = salesforceLicenseId;
        testUser.Email='rajasen@gmail.com';
        testUser.LastName='tetst_005';
        testUser.LanguageLocaleKey='en_US';
        testUser.CommunityNickname = 'dummyUser';
        testUser.LocaleSidKey='en_US';
        testUser.TimeZoneSidKey='America/Los_Angeles';
        testUser.UserName='mygardtestbrokerUser@testorg.com.mygard';
        testUser.contactId__c = dummyGardContact.id;
        testUser.EmailEncodingKey='UTF-8';
        testUser.IsActive = true;
        testUser.LanguageLocaleKey='en_US';
        insert testUser;
    }
   
    
   
    private static testmethod void testDeactiveUser(){
        Test.startTest();
        TestGardContactUtil.setupCustomSettingTestData();
        TestGardContactUtil.setupTestData();
        
        System.assert(dummyGardContact != null , true);
        List<User> UserList = [SELECT ID FROM User WHERE id =: testUser.id];
        //List<Gard_Contacts__c> fetchedGardContact = [SELECT ID FROM Gard_Contacts__c WHERE FirstName__c='test firstname'];
        Set<ID> UserID = new Set<ID>();
        if(UserList.size() > 0){
            UserID.add(UserList.get(0).ID);
        }
        
        if(UserList.size() > 0){
            GardContactUtil.updateGardContacts(UserID);
        }
        Test.stopTest();
        
        //fetchedGardContact = [SELECT ID,Email__c,Office_city__c,Phone__c,Name,isActive__c FROM Gard_Contacts__c WHERE FirstName__c='test firstname'];
        
        /*System.assertEquals('companymail@gard.no',fetchedGardContact.get(0).Email__c,'companymail@gard.no');
        System.assertEquals('Arendal',fetchedGardContact.get(0).Office_city__c,'Arendal');
        System.assertEquals('+47 37019100',fetchedGardContact.get(0).Phone__c,'+47 37019100');
        System.assertEquals(false,fetchedGardContact.get(0).isActive__c,false);
    */
    }
}