global class processESPSalesforceCompanies implements Database.Batchable<sObject> {
    global String query;
    global Map<String,String> mapUsers;
    global Map<String,String> mapCountries;
    global string ESPRecTypeID;
    public class processESPSalesforceCompaniesException extends Exception{}
    private static DQ__c OriginalDQSettings{
        get{
            if(OriginalDQSettings==null){
                OriginalDQSettings = DataQualitySupport.GetSetting(UserInfo.getUserId());
                if(OriginalDQSettings.Id ==null || Test.isRunningTest()){insert OriginalDQSettings;}
                return OriginalDQSettings;
            }else{
                return OriginalDQSettings;
            }
        }
        set;
    }
    
    private static DQ__c DQSettings{
        get{
            if(DQSettings==null){
                DQSettings = OriginalDQSettings.clone(true, true, true, true);
                return DQSettings;
            }else{
                return DQSettings;
            }
        }
        set;
    }   
    
    //global static MDM_Settings__c OriginalMDMSettings;
    
    //global static MDM_Settings__c MDMSettings {
    //    get{
    //        if(MDMSettings==null){
    //            MDMSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
    //            if(MDMSettings.Id == null || Test.isRunningTest()){insert MDMSettings;}
    //            OriginalMDMSettings = MDMSettings.clone(true, true, true, true);
    //            return MDMSettings;
    //        }else{
    //            return MDMSettings;
    //        }
    //    }
    //    set;
    //}
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query); }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        try {
            if (scope.size() > 0) {
                String strWithAccESPIds = '';
                String strNoAccESPIds = '';
                String strUpdateAccIds = '';
                
                // get the ESP record type for use later
                ESPRecTypeID = [Select ID from RecordType where Name = 'External Service Provider' LIMIT 1].Id;            
                
                // build a map of user ids->GIC Ids for later
                mapUsers = new Map<String,String>();
                for (User u : [Select GIC_ID__c, Id from User WHERE GIC_ID__c <> null AND GIC_ID__c <> '' AND isActive = true])
                    mapUsers.put(u.GIC_ID__c, u.Id);
                
                // build a map of country ids->country ISO codes for later
                mapCountries = new Map<String,String>();
                for (Country__c c : [Select Country_ISO_Code__c, Id from Country__c])
                    mapCountries.put(c.Country_ISO_Code__c, c.Id);
                
                Admin_System_Company__c esppsc;
                
                // build the PSC and (if related accounts exist) Account query strings
                for (sObject lstpsc: scope) {
                    esppsc = (Admin_System_Company__c)lstpsc;
                    String strType = esppsc.Type__c;
                    //RCallear 29 01 14: replaced If statement to include more ESP 'types'    - REVERTED on 24 02 14               
                    if (strType=='ESP') {
                        //if(strType=='ESP'||strType=='Lawyer'||strType=='Surveyor'||strType=='Accounts Misc.'||strType=='Claim Adjuster'){
                        string strAccId = esppsc.Salesforce_Company__c;
                        if (strAccId == null || strAccId == '')
                            strNoAccESPIds+='\''+esppsc.Id+'\',';
                        else {
                            strWithAccESPIds+='\''+esppsc.Id+'\',';
                            strUpdateAccIds+='\''+esppsc.Salesforce_Company__c+'\','; }
                    }
                }
                
                // Process Updates (i.e. a related account already exists) -------------------------------------------------------------------------------
                if (strUpdateAccIds.length() > 0)
                {
                    system.debug('#### updateaccids:'+strUpdateAccIds);
                    Set<Id> accswithGICpsc = new Set<Id>();
                    Set<Id> accswithMasters = new Set<Id>();        // to be used when implementing Master PSC functionality
                    strUpdateAccIds = strUpdateAccIds.subString(0, strUpdateAccIds.length()-1);
                    
                    // Build a map of PSC ID->Acc ID where the Account is in the list built above, and the PSC is a GIC PSC. This will be used
                    // to exclude updates of any PSC Accounts where an existing GIC PSC exists (as this would be used to update the Account).
                    String qry = 'SELECT ' + getPSCQueryFields() + ' from Admin_System_Company__c where Source_System__c = \'GIC\' AND Salesforce_Company__c IN ('+ strUpdateAccIds +')';
                    List<Admin_System_Company__c> lstMap = Database.query(qry);
                    for (Admin_System_Company__c psc : lstMap)
                        accswithGICpsc.add(psc.Salesforce_Company__c);
                    
                    // Build a map of all relevant PSCs flagged as Master so later we can just process the master record if one exists
                    String qryMasters = 'SELECT Id from Admin_System_Company__c where Master__c = True AND Salesforce_Company__c IN ('+ strUpdateAccIds +')';
                    List<Admin_System_Company__c> lstMasters = Database.query(qryMasters);
                    for (Admin_System_Company__c pscm : lstMasters)
                        accswithMasters.add(pscm.Id);
                    
                    List<Account> lsUpdateAcc = new List<Account>();    // holds the collection of accounts to be updated
                    List<Account> lsInsertAcc = new List<Account>();    // holds the collection of accounts to be created
                    Map<Id, Account> mUpdateAcc = new Map<Id, Account>();
                    List<Account> lsAcc = new List<Account>();          // holds the collection of accounts which are due to be updated
                    List<Admin_System_Company__c> lsESP = new List<Admin_System_Company__c>();    // holds the collections of PSCs which will be processed
                    
                    // process ESPs with a related account first
                    if (strUpdateAccIds!='') {
                        system.debug('#### strupodaccids:'+strUpdateAccIds);
                        lsAcc = Database.query('Select ' + getAccQueryFields() + ' from Account where Id IN (' +strUpdateAccIds+' )');}    // get a list of the related accounts
                    if (strWithAccESPIds!='') {
                        strWithAccESPIds = strWithAccESPIds.substring(0, strWithAccESPIds.length()-1);
                        lsESP = Database.query('SELECT ' + getPSCQueryFields() +' FROM Admin_System_Company__c WHERE Id IN (' +strWithAccESPIds+ ')');
                    }
                    
                    // Process Paris PSCs
                    for (Admin_System_Company__c psc: lsESP) {
                        for (Account acc: lsAcc) {
                            if (acc.Id == psc.Salesforce_Company__c && psc.Source_System__c == 'Paris' && !accswithGICpsc.contains(acc.Id) && !accswithMasters.contains(psc.Id)) {
                                //Have we added this account to the map previously? if so this will replace the data on the account with this latest record
                                //16/12/2013 - RCallear - added section to set Role Fields - previously only in the 'insert' section below
                                //mUpdateAcc.put(acc.Id,setAccFields(acc,psc));
                                mUpdateAcc.put(acc.Id,setAccRoleFields(setAccFields(acc,psc),psc));
                            }
                        }
                    }
                    
                    //Temporarily disable MDM Triggers
                    /*   boolean oldTriggerVal = MDMSettings.Account_Trigger_Enabled__c;
if(oldTriggerVal){
MDMSettings.Account_Trigger_Enabled__c = false;
update MDMSettings;
}*/
                    
                    if (mUpdateAcc.values().size()>0) {updateAccounts( mUpdateAcc.values()); mUpdateAcc.clear();}
                    
                    // Process GIC PSCs
                    for (Admin_System_Company__c psc: lsESP) {
                        for (Account acc: lsAcc) {
                            if (acc.Id == psc.Salesforce_Company__c && psc.Source_System__c == 'GIC' && !accswithMasters.contains(psc.Id)) {
                                //Have we added this account to the map previously? if so this will replace the data on the account with this latest record
                                //16/12/2013 - RCallear - added section to set Role Fields - previously only in the 'insert' section below
                                //mUpdateAcc.put(acc.Id,setAccFields(acc,psc));
                                mUpdateAcc.put(acc.Id,setAccRoleFields(setAccFields(acc,psc),psc));
                            }
                        }
                    }
                    if (mUpdateAcc.values().size()>0) {updateAccounts( mUpdateAcc.values()); mUpdateAcc.clear();}
                    
                    // Finally process PSCs marked as Master
                    for (Admin_System_Company__c psc: lsESP) {
                        for (Account acc: lsAcc) {
                            if (acc.Id == psc.Salesforce_Company__c && psc.Master__c == true && accswithMasters.contains(psc.Id)) {
                                //Have we added this account to the map previously? if so this will replace the data on the account with this latest record
                                //16/12/2013 - RCallear - added section to set Role Fields - previously only in the 'insert' section below
                                //mUpdateAcc.put(acc.Id,setAccFields(acc,psc));
                                mUpdateAcc.put(acc.Id,setAccRoleFields(setAccFields(acc,psc),psc));
                            }
                        }
                    }
                    
                    if (mUpdateAcc.values().size()>0) {updateAccounts( mUpdateAcc.values());}
                    //Temporarily disable MDM Triggers                
                    /* if(oldTriggerVal){
MDMSettings.Account_Trigger_Enabled__c = oldTriggerVal;
update MDMSettings;
}*/
                }
                
                // Process Inserts (i.e. where no related account exists and so needs creating) ----------------------------------------------------------
                if (strNoAccESPIds.length() > 0)
                {
                    // create all the new accounts
                    strNoAccESPIds = strNoAccESPIds.subString(0, strNoAccESPIds.length()-1);
                    List<Admin_System_Company__c> lsESP = new List<Admin_System_Company__c>();
                    //List<Account> lsNewAccs = new List<Account>();
                    //Use a map of new accounts based on the production system id to avoid duplictes.
                    Map<String, Account> mNewAccs = new Map<String, Account>();
                    lsESP = Database.query('SELECT ' + getPSCQueryFields() + ' FROM Admin_System_Company__c WHERE Id IN (' +strNoAccESPIds+ ')');
                    //get any matching exisitng accounts that share the same production uid.
                    Set<String> setProdUIDs = new Set<String>();
                    
                    for(Admin_System_Company__c psc:lsESP){
                        setProdUIDs.add('\''+psc.Production_System_UID__c+'\'');
                    }
                    
                    String strProdUIDs = String.join(new List<String>(setProdUIDs),', ');
                    List<Account> existingAccts = new List<Account>();
                    existingAccts = Database.query('SELECT ' + getAccQueryFields() + ' from Account where Production_System_UID__c IN (' +strProdUIDs+' )');
                    Map<String, Account> mProdAccMap = new Map<String, Account>();
                    for(Account acc:existingAccts){
                        mProdAccMap.put(acc.Production_System_UID__c, acc);
                    }
                    
                    //To hold all pscs that need updating with account ids.
                    Map<Id, Admin_System_Company__c> mapNewPSCs = new Map<Id, Admin_System_Company__c>();
                    Map<Id, Account> mapAcctsToUpdate = new Map<Id, Account>();
                    
                    for (Admin_System_Company__c psc: lsESP) {
                        if(mProdAccMap.containsKey(psc.Production_System_UID__c)){
                            //update the psc with the account id
                            Account acc = mProdAccMap.get(psc.Production_System_UID__c);
                            psc.Salesforce_Company__c = acc.Id;
                            mapAcctsToUpdate.put(acc.Id, setAccRoleFields(acc, psc));
                            mapNewPSCs.put(psc.Id, psc);
                        }else{
                            //Check to see if we have encountered this account before
                            if(mNewAccs.containsKey(psc.Production_System_UID__c)){
                                //update this account rather than creating a new one.
                                mNewAccs.put(psc.Production_System_UID__c, setAccRoleFields(mNewAccs.get(psc.Production_System_UID__c), psc));
                            }else{
                                //create an account
                                Account acc =  new Account();
                                acc = setAccFields(acc, psc);
                                acc.RecordTypeId = ESPRecTypeID;
                                acc.Production_System_UID__c = psc.Production_System_UID__c;
                                mNewAccs.put(acc.Production_System_UID__c, acc);
                            }
                        }
                        
                    }
                    //Temporarily disable MDM Triggers
                    /* boolean oldTriggerVal = MDMSettings.Account_Trigger_Enabled__c;
if(oldTriggerVal){
MDMSettings.Account_Trigger_Enabled__c = false;
update MDMSettings;
}*/
                    System.Debug('Inserting new Accounts ' + mNewAccs);
                    insert(mNewAccs.values());
                    System.Debug('Inserted new Accounts ' + mNewAccs);
                    
                    
                    System.Debug('Updating existing Accounts ' + mapAcctsToUpdate);
                    updateAccounts(mapAcctsToUpdate.values());
                    System.Debug('Updated existing Accounts ' + mapAcctsToUpdate);
                    
                    //Reenable MDM Triggers                
                    /* if(oldTriggerVal){
MDMSettings.Account_Trigger_Enabled__c = oldTriggerVal;
update MDMSettings;
}*/
                    // Update the PSCs to point to the relevant new account
                    
                    for (Admin_System_Company__c psc: lsESP) {
                        for (Account acc : mNewAccs.values()) {
                            if (psc.Production_System_UID__c == acc.Production_System_UID__c) {
                                psc.Salesforce_Company__c = acc.Id;
                                mapNewPSCs.put(psc.Id, psc);
                            }
                        }
                    }
                    update(mapNewPSCs.values());
                }
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            //update OriginalMDMSettings;
            setValidation(OriginalDQSettings.Enable_Validation__c);
        }
        
    }
    private static void setValidation(boolean enable){
        //disable validation during the execution of this operation 
        //only if we are allowed
        System.Debug('setValidation DQSettings.Enable_Validation_for_Processing_PSC__c = ' + DQSettings.Enable_Validation_for_Processing_PSC__c);
        if(!DQSettings.Enable_Validation_for_Processing_PSC__c){    
            DQSettings.Enable_Validation__c = enable;       
            System.Debug('setValidation - updating settings');
            update DQSettings;
        }
    } 
    
    private void updateAccounts(List<Account> lstAcc){
        if (lstAcc.Size() > 0){
            //setValidation(false);
            System.debug('Accounts to update ' + lstAcc);
            setValidation(false);
            Database.SaveResult[] srList = Database.update(lstAcc, false);
            setValidation(OriginalDQSettings.Enable_Validation__c);
            System.debug('Updated accounts. ' + srList);
            // Iterate through each returned result
            //Use an iterator i so we know which source id caused the problem
            //We assume that the results are in the same order as the input list.
            for(Integer i=0;i<srList.size();i++){
                Database.SaveResult sr = srList[i];
                Account acc = new Account();
                if(i<lstAcc.size()){ acc= lstAcc[i];}
                
                //for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully updated account. Account ID: ' + sr.getId());
                }
                else {
                    System.debug('Error occured for account. Account ID: ' + acc.Id);                   
                    // Operation failed, so get all errors 
                    
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug(' Account fields that affected this error: ' + err.getFields());
                        processESPSalesforceCompaniesException ex = new processESPSalesforceCompaniesException('Exception for Account Id ' + acc.Id + ' : '+ err.getStatusCode() + ': ' + err.getMessage() + ' Account fields that affected this error: ' + err.getFields());
                        Logger.LogException('processESPSalesforceCompanies.updateAccounts', ex);
                    }
                }
            }
        }       
    }
    
    global void finish(Database.BatchableContext BC) {
        // do nothing
    }
    
    public string getPSCQueryFields()
    {
        // build the querystring for PSCs; having it in one place will make it easier to maintain.
        string retval = '';
        retval+= 'Id,';
        retval+= 'Name,';
        retval+= 'Source_System__c,';
        retval+= 'Master__c,';
        retval+= 'Company_Status__c,';
        retval+= 'Salesforce_Company__c,';
        retval+= 'Type__c,';
        retval+= 'Production_System_UID__c,';
        retval+= 'Company_Sub_Type__c,';
        retval+= 'Company_Owner__c,';
        retval+= 'Office_Location__c,';
        retval+= 'Office_Country__c,';
        retval+= 'Company_Website__c,';
        retval+= 'Company_Email__c,';
        retval+= 'Phone__c,';
        retval+= 'Fax__c,';
        retval+= 'X2nd_UWR__c,';
        retval+= 'X3rd_UWR__c,';
        retval+= 'U_W_Assistant_1__c,';
        retval+= 'U_W_Assistant_2__c,';
        retval+= 'Accounting__c,';
        retval+= 'Claim_handler_Cargo_Dry__c,';
        retval+= 'Claim_handler_Cargo_Liquid__c,';
        retval+= 'Claim_handler_CEP__c,';
        retval+= 'Claim_handler_Charterers__c,';
        retval+= 'Claim_handler_Crew__c,';
        retval+= 'Claim_handler_Defence__c,';
        retval+= 'Claim_handler_Energy__c,';
        retval+= 'Claim_handler_Marine__c,';
        retval+= 'Claims_handler_Builders_Risk__c,';
        retval+= 'Billing_Street__c,';
        retval+= 'Billing_City__c,';
        retval+= 'Billing_Country__c,';
        retval+= 'Billing_State__c,';
        retval+= 'Billing_Postal_Code__c,';
        retval+= 'Visiting_Street__c,';
        retval+= 'Visiting_City__c,';
        retval+= 'Visiting_Country__c,';
        retval+= 'Visiting_State__c,';
        retval+= 'Visiting_Postal_Code__c';
        return retval;
    }
    
    public string getAccQueryFields()
    {
        // build the querystring for Accounts; having it in one place will make it easier to maintain.
        string retval = '';
        retval+= 'Id,';
        retval+= 'Name,';
        retval+= 'OwnerId,';
        retval+= 'Type,';
        retval+= 'Company_Role__c,';
        retval+= 'Sub_Roles__c,';
        retval+= 'Production_System_UID__c,';
        retval+= 'Company_Status__c,';
        retval+= 'Site,';
        retval+= 'Country__c,';
        retval+= 'Website,';
        retval+= 'Corporate_email__c,';
        retval+= 'Phone,';
        retval+= 'Fax,';
        retval+= 'X2nd_UWR__c,';
        retval+= 'X3rd_UWR__c,';
        retval+= 'UW_Assistant_1__c,';
        retval+= 'U_W_Assistant_2__c,';
        retval+= 'Accounting__c,';
        retval+= 'Claim_handler_Cargo_Dry__c,';
        retval+= 'Claim_handler_Cargo_Liquid__c,';
        retval+= 'Claim_handler_CEP__c,';
        retval+= 'Claim_handler_Charterers__c,';
        retval+= 'Claim_handler_Crew__c,';
        retval+= 'Claim_handler_Defence__c,';
        retval+= 'Claim_handler_Energy__c,';
        retval+= 'Claim_handler_Marine__c,';
        retval+= 'Claims_handler_Builders_Risk__c,';
        retval+= 'BillingStreet,';
        retval+= 'BillingCity,';
        retval+= 'BillingCountry,';
        retval+= 'BillingState,';
        retval+= 'BillingPostalCode,';
        retval+= 'ShippingStreet,';
        retval+= 'ShippingCity,';
        retval+= 'ShippingCountry,';
        retval+= 'ShippingState,';
        retval+= 'ShippingPostalCode';
        return retval;
    }
    public Account setAccRoleFields(Account acc, Admin_System_Company__c psc){
        //MG cDecisions 4/12/2013 amending PSC/ESP functionality for MDM
        //Create a set from the company roles and then add esp to that set
        //convert the set back into a string
        //mapping roles based on CLAIMS-PI-FDD- 5699079 - SRS Document 19th September.docx
        String newRole = getRole(psc.Type__c);
        
        if(acc.Company_Role__c!=null && newRole!=null){
            Set <String> corpRoles = new Set<String>(new List<String>(acc.Company_Role__c.split(';')));         
            corpRoles.add(newRole);
            acc.Company_Role__c = String.join(new List<String>(corpRoles),';');
        }else if(acc.Company_Role__c == null && newRole!=null){
            acc.Company_Role__c = newRole;
        }
        System.Debug('*** Account Roles for Id ' + acc.Id +' set to : ' + acc.Company_Role__c);
        //Create a set from the existing sub roles and add the new role to it
        //convert the set of sub roles back into a string.
        
        //  RCallear 29 01 14 Sub Role now held in Type not SubType - REVERTED on 24 02 14
        String newSubRole = getSubRole(psc.Company_Sub_Type__c, getPrefix(newRole));
        
        if(acc.Sub_Roles__c != null && newSubRole!=null){
            Set <String> subRoles = new Set<String>(new List<String>(acc.Sub_Roles__c.split(';')));
            //find the right sub role for the given ESP role            
            subRoles.add(newSubRole);
            acc.Sub_Roles__c    = String.join(new List<String>(subRoles),';');
        }else if(acc.Sub_Roles__c==null && newSubRole!=null){
            acc.Sub_Roles__c    = newSubRole;
        }
        System.Debug('*** Account Sub Roles for Id ' + acc.Id +' set to : ' + acc.Sub_Roles__c);
        return acc;     
    }
    public Account setAccFields(Account acc, Admin_System_Company__c psc)
    {
        // put all the field mappings from PSC to Account in this method
        acc.Name                         = psc.Name;
        acc.Company_Status__c            = psc.Company_Status__c;
        acc.Type                         = psc.Company_Sub_Type__c;
        acc.Site                         = psc.Office_Location__c;
        acc.Website                      = psc.Company_Website__c;
        acc.Corporate_email__c           = psc.Company_Email__c;
        acc.Phone                        = psc.Phone__c;
        acc.Fax                          = psc.Fax__c;
        //acc.Corporate_email__c           = psc.Company_Email__c;
        
        // postal and visit addresses
        acc.BillingStreet      = psc.Billing_Street__c;
        acc.BillingCity        = psc.Billing_City__c;
        acc.BillingCountry     = psc.Billing_Country__c;
        acc.BillingState       = psc.Billing_State__c;
        acc.BillingPostalCode  = psc.Billing_Postal_Code__c;
        acc.ShippingStreet     = psc.Visiting_Street__c;
        acc.ShippingCity       = psc.Visiting_City__c;
        acc.ShippingCountry    = psc.Visiting_Country__c;
        acc.ShippingState      = psc.Visiting_State__c;
        acc.ShippingPostalCode = psc.Visiting_Postal_Code__c;
        if(psc.Office_Country__c != null)              acc.Country__c                   = mapCountries.Get(psc.Office_Country__c);
        // user fields
        // MM 15/12/11 removed user fields following Olga's email confirmation that they shouldn't be updated
        //if(mapUsers.Get(psc.Company_Owner__c) != null)                acc.OwnerId                         = mapUsers.Get(psc.Company_Owner__c);
        //if(mapUsers.Get(psc.X2nd_UWR__c) != null)                     acc.X2nd_UWR__c                     = mapUsers.Get(psc.X2nd_UWR__c);
        //if(mapUsers.Get(psc.X3rd_UWR__c) != null)                     acc.X3rd_UWR__c                     = mapUsers.Get(psc.X3rd_UWR__c);
        //if(mapUsers.Get(psc.U_W_Assistant_1__c) != null)              acc.UW_Assistant_1__c               = mapUsers.Get(psc.U_W_Assistant_1__c);
        //if(mapUsers.Get(psc.U_W_Assistant_2__c) != null)              acc.U_W_Assistant_2__c              = mapUsers.Get(psc.U_W_Assistant_2__c);
        //if(mapUsers.Get(psc.Accounting__c) != null)                   acc.Accounting__c                   = mapUsers.Get(psc.Accounting__c);
        //if(mapUsers.Get(psc.Claim_handler_Cargo_Dry__c) != null)      acc.Claim_handler_Cargo_Dry__c      = mapUsers.Get(psc.Claim_handler_Cargo_Dry__c);
        //if(mapUsers.Get(psc.Claim_handler_Cargo_Liquid__c) != null)   acc.Claim_handler_Cargo_Liquid__c   = mapUsers.Get(psc.Claim_handler_Cargo_Liquid__c);
        //if(mapUsers.Get(psc.Claim_handler_CEP__c) != null)            acc.Claim_handler_CEP__c            = mapUsers.Get(psc.Claim_handler_CEP__c);
        //if(mapUsers.Get(psc.Claim_handler_Charterers__c) != null)     acc.Claim_handler_Charterers__c     = mapUsers.Get(psc.Claim_handler_Charterers__c);
        //if(mapUsers.Get(psc.Claim_handler_Crew__c) != null)           acc.Claim_handler_Crew__c           = mapUsers.Get(psc.Claim_handler_Crew__c);
        //if(mapUsers.Get(psc.Claim_handler_Defence__c) != null)        acc.Claim_handler_Defence__c        = mapUsers.Get(psc.Claim_handler_Defence__c);
        //if(mapUsers.Get(psc.Claim_handler_Energy__c) != null)         acc.Claim_handler_Energy__c         = mapUsers.Get(psc.Claim_handler_Energy__c);
        //if(mapUsers.Get(psc.Claim_handler_Marine__c) != null)         acc.Claim_handler_Marine__c         = mapUsers.Get(psc.Claim_handler_Marine__c);
        //if(mapUsers.Get(psc.Claims_handler_Builders_Risk__c) != null) acc.Claims_handler_Builders_Risk__c = mapUsers.Get(psc.Claims_handler_Builders_Risk__c);
        //MG we don't want to reflect the PSC types onto the account roles
        //return setAccRoleFields(acc, psc);
        return acc;
    }
    
    public static string getRole(String corpRole){
        if(corpRole!=null){
            if(corpRole.equalsIgnoreCase('Broker')){
                return 'Broker';
            }else if(corpRole.equalsIgnoreCase('Client')){
                return 'Client';
            }else if(corpRole.equalsIgnoreCase('Correspondent')){
                return 'Correspondent';
            }else if(corpRole.equalsIgnoreCase('ESP')){
                //RCallear 29 01 14 - extended this If clause to include other ESP roles - REVERTED on 24 02 14
                return 'External Service Provider';
            }else if(corpRole.equalsIgnoreCase('Other')){
                return 'Other';
            }else if(corpRole.equalsIgnoreCase('Prospect')){
                return 'Client';
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
    
    public static string getPrefix(String corpRole){
        if(corpRole!=null){
            if(corpRole.equalsIgnoreCase('Broker')){
                return 'Broker - ';
            }else if(corpRole.equalsIgnoreCase('Client')){
                return '';
            }else if(corpRole.equalsIgnoreCase('Correspondent')){
                return '';
            }else if(corpRole.equalsIgnoreCase('External Service Provider')){
                return 'ESP - ';
            }else if(corpRole.equalsIgnoreCase('Other')){
                return 'Other - ';
            }else if(corpRole.equalsIgnoreCase('Prospect')){
                return '';
            }else if(corpRole.equalsIgnoreCase('Bank')){
                return 'Bank - ';
            }else if(corpRole.equalsIgnoreCase('Insurance Company')){
                return 'Insurance Company - ';
            }else if(corpRole.equalsIgnoreCase('Investments')){
                return 'Investments - ';
            }else{
                return null;
            }
        }else{
            return null;
        }       
    }
    
    public static string getSubRole(String espRole, String prefix){
        //based on CLAIMS-PI-FDD- 5699079 - SRS Document 19th September.docx return the correct
        //sub role string
        if(espRole!=null){
            if(espRole.equalsIgnoreCase('Surveyor')){
                return prefix  + 'P&I Surveyor';
            }else if(espRole.equalsIgnoreCase('Broker')){
                return 'Broker - Broker';
            }else if(espRole.equalsIgnoreCase('Accounts Misc.')||espRole.equalsIgnoreCase('ESP')){
                return prefix + 'Other';
            }else{
                return prefix + espRole;
            }
        }else{
            return null;
            
        }
    }
    
    public static testmethod void runTest() {
        //Create User
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        String userNameToBeApplied = String.valueof(DateTime.now().getTime()) + 'testing@testorg.com';
        User newUser = new User(Alias = 'standt', 
                          Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', 
                          FirstName='TestFirstName',
                          LastName='Testing', 
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', 
                          ProfileId = p.Id, 
                          IsActive =true,
                          TimeZoneSidKey='America/Los_Angeles', 
                          UserName=userNameToBeApplied
                         );
        insert newUser;
		
        //User u = [SELECT Id FROM User WHERE IsActive = true AND GIC_ID__c != null LIMIT 1];
		User u = [SELECT Id FROM User WHERE Email='standarduser@testorg.com'];
        if(u != null){
            System.RunAs(u) {
                
                DQ__c dq = DataQualitySupport.GetSetting(UserInfo.getUserId());
                dq.Enable_Validation__c = false;
                dq.Enable_Workflow__c = false;
                dq.Enable_Role_Governance__c = false;
                dq.Enable_Account_Market_Area_Manager__c = false;
                dq.Enable_Contact_Type_Validation__c = false;
                dq.Enable_Opportunity_Delete_History__c = false;
                dq.Enable_Opportunity_Line_Delete_History__c = false;
                dq.Enable_Validation_for_Processing_PSC__c = false;
                dq.SetupOwnerId = u.id;
                upsert dq;
                
                OriginalDQSettings = dq;
                
                String tst;
                String odd = 'odd';
                processESPSalesforceCompanies.getRole(tst);
                processESPSalesforceCompanies.getSubRole(tst,'prefix - ');
                processESPSalesforceCompanies.getRole(odd);
                processESPSalesforceCompanies.getSubRole(odd, 'prefix - ');
                processESPSalesforceCompanies.getSubRole('Surveyor' ,getPrefix(getRole('Broker')));
                processESPSalesforceCompanies.getSubRole('Broker' ,getPrefix(getRole('Client')));
                processESPSalesforceCompanies.getSubRole('Correspondent' ,getPrefix(getRole('Correspondent')));
                processESPSalesforceCompanies.getSubRole('ESP' ,getPrefix(getRole('ESP')));
                processESPSalesforceCompanies.getSubRole('Other' ,getPrefix(getRole('Other')));
                processESPSalesforceCompanies.getSubRole('Prospect' ,getPrefix(getRole('Prospect')));
                
                // TEST 1: create a new non-ESP PSC to make sure no related account is created
                Admin_System_Company__c psc1 = new Admin_System_Company__c(Name='espxxxtest1',Type__c='Client',Company_Owner__c=u.Id);
                insert(psc1);
                // TEST 2: create a new ESP PSC to ensure a related account is created
                Admin_System_Company__c psc2 = new Admin_System_Company__c(Name='espxxxtest2',Type__c='ESP',Company_Owner__c=u.Id, Source_System__c='GIC');
                insert(psc2);
                // TEST 3: create a new Account with related GIC and Paris ESPs. Set the Paris ESP name to be different, and ensure the Account details 
                // aren't updated (as there's a GIC PSC which takes precedence)
                Account acc3 = new Account(Name='espxxxtest3');
                insert(acc3);
                Admin_System_Company__c psc3G = new Admin_System_Company__c(Name='espxxxtest3',Type__c='ESP',Salesforce_Company__c=acc3.Id,Source_System__c='GIC',Company_Owner__c=u.Id);
                Admin_System_Company__c psc3P = new Admin_System_Company__c(Name='espxxxtest3Pupd',Type__c='ESP',Salesforce_Company__c=acc3.Id,Source_System__c='Paris',Company_Owner__c=u.Id);
                insert(psc3G);
                insert(psc3P);
                
                test.startTest();
                processESPSalesforceCompanies pe = new processESPSalesforceCompanies();
                pe.Query = 'SELECT Id, Name, Type__c, Salesforce_Company__c from Admin_System_Company__c where Type__c = \'ESP\''+
                    'AND Id IN (\''+psc1.ID+'\',\''+psc2.ID+'\', \''+psc3G.ID+'\',\''+psc3P.ID+'\')';
                system.debug('####:'+pe.query);
                Database.executeBatch(pe, 20);        
                test.stopTest();
                
                // TEST 1:
                system.assert(psc1.Salesforce_Company__c == null);
                // TEST 2:
                string newacc2id = [SELECT Id, Name, Salesforce_Company__c from Admin_System_Company__c where Id=:psc2.Id].Salesforce_Company__c;
                Account acc2 = [SELECT Id, Name from Account where Id = :newacc2id];
                system.assert(acc2.Name == psc2.Name);
                // TEST 3:
                system.assert([SELECT Id, Name from Account where ID = :acc3.Id].Name != [SELECT Id, Name from Admin_System_Company__c where ID = :psc3P.Id].Name);
                system.assert([SELECT Id, Name from Account where ID = :acc3.Id].Name == [SELECT Id, Name from Admin_System_Company__c where ID = :psc3G.Id].Name);
            }
        }
        
    }
}