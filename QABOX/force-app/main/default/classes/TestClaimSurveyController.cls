@IsTest public class TestClaimSurveyController{
    public static GardTestData gtd;
    public static void createTestData(){
        gtd = new GardTestData();
        gtd.commonRecord();
        gtd.customsettings_rec();
    }
     @IsTest static void TestClaimSurveyController(){
        createTestData();
        ClaimSurveyController claimSurveyObj = new ClaimSurveyController();
        List<Id> contactIdList = new List<Id>();
        ClaimSurveyController.fetchRecords(GardTestData.brokerContact.FirstName,JSON.serialize(contactIdList) );
        contactIdList.add(GardTestData.clientContact.Id);
        ClaimSurveyController.sendInvitation(GardTestData.brokerClaim.Id, contactIdList);
        ClaimSurveyController.fetchRecords(GardTestData.brokerContact.FirstName,JSON.serialize(contactIdList));
     }
}