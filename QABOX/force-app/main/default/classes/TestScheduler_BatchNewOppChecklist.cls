@isTest
private class TestScheduler_BatchNewOppChecklist{
  public static TestMethod void testScheduler_BatchNewOppChecklist() {
        
        test.StartTest();
        
        // schedule a new job
        String jobId = Scheduler_BatchNewOppChecklistCreation.scheduleMe();
        
        // get the newly created trigger record
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, 
            NextFireTime
            FROM CronTrigger WHERE id = :jobId];
        
        // assert that the values in the job are correct
        System.assertEquals(0, ct.TimesTriggered);
        
        test.StopTest();
    }
}