global class GardCloneEventMembersBatch implements Database.Batchable<SObject>, Database.Stateful { 
  global id NewSurveyId;
  global id OldSurveyId;
  
  public GardCloneEventMembersBatch(id pNewSurveyId,id pOldSurveyId){
    NewSurveyId=pNewSurveyId;
    OldSurveyId=pOldSurveyId;    
  }
   
    //START
    public List<fluidoconnect__Invitation__c> start (Database.BatchableContext BC) {         
        return [SELECT fluidoconnect__UsesStandardLanguage__c, fluidoconnect__Survey__c, fluidoconnect__Reference3__c, fluidoconnect__Reference2__c,
                fluidoconnect__Reference1__c, fluidoconnect__Liidi_del__c, fluidoconnect__Language__c, fluidoconnect__Custom_Contact__c,
                fluidoconnect__Contact__c, fluidoconnect__ImmediateInvitation__c, fluidoconnect__user__c
                FROM fluidoconnect__Invitation__c
                WHERE fluidoconnect__Survey__c=:OldSurveyId AND fluidoconnect__Contact__r.No_Longer_Employed_by_Company__c=false];

      } 
  
 
   //EXECUTE
  public void execute(Database.BatchableContext BC, List<sObject> scope){ 
    List<fluidoconnect__Invitation__c> MemberList = (List<fluidoconnect__Invitation__c>) scope;
    List<fluidoconnect__Invitation__c> clonedInvitations=new List<fluidoconnect__Invitation__c>();
       
    for(fluidoconnect__Invitation__c i : MemberList){
                fluidoconnect__Invitation__c iClone= i.clone(false,true);
                iClone.fluidoconnect__ImmediateInvitation__c=false;
                iClone.fluidoconnect__Status__c='Not Responded';
                // new survey id
                iClone.fluidoconnect__Survey__c=NewSurveyId;
                clonedInvitations.add(iClone);
            }
    
    if (clonedInvitations.size()>0) insert clonedInvitations;   
  }
  
  
  // FINISH
  public void finish(Database.BatchableContext BC){
    // send email to user that has fired clone action
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       
    mail.setTargetObjectId(UserInfo.getUserId());
    mail.setSaveAsActivity(false); // Must be false when User is set as targetObjectId !!!
    mail.setReplyTo('no-reply@salesforce.com');
    mail.setSenderDisplayName('Cloning Event');
    mail.setSubject('Event Mass Clone Completed');
    
    AsyncApexJob[] jobs = [select TotalJobItems, Status, NumberOfErrors, JobType, 
                  JobItemsProcessed, Id, ApexClassId, ExtendedStatus
                            from AsyncApexJob
                            where Id = :BC.getJobId()];
    String errTxt;
    if(jobs.size()>0)errTxt = jobs[0].NumberOfErrors > 0 ? 'There were total of '
                                +jobs[0].TotalJobItems
                                +' event members to be cloned of which '
                                +jobs[0].NumberOfErrors+' failed. '
                                +'Not all members have been cloned.\n\n'
                                +'Detailed status: '+jobs[0].ExtendedStatus : '';
    mail.setPlainTextBody(  'Mass clone request to selected event has completed. \n\n'+errTxt);
    
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
  }
  
  
}