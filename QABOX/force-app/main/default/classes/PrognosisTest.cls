/************************************************************************
     * Date: 14/08/2013
     * Author: Martin Gardner
     * Company: cDecisions Ltd.
     * Description: Test class for Prognosis records
     * Modifications: [ENTER MODS HERE]
     ************************************************************************/
@isTest (seeAllData = true) //Need this to see standard price book
private class PrognosisTest {

	public static Opportunity[] testOpps;
	public static OpportunityLineItem[] oppLineItems;
	public static Set<Id> oppLineIds;
	public static Integer noOfMarineProds = -1;
	public static Integer noOfEnergyProds = -1;
	public static String currencyISOCode = 'USD';

	private static void setupTestData()
	{
		/*
		//Pricebooks
		Pricebook2[] priceBooks = new Pricebook2[]{
			//new Pricebook2(Name='Standard', isStandard=true),
			new Pricebook2(Name='Marine'),
			new Pricebook2(Name='Energy')
		};
		insert priceBooks;*/
		
		Pricebook2 standardPb = [SELECT Id FROM Pricebook2 WHERE isStandard=true];
		System.Assert(standardPb != null);
		
		Product2[] products = new Product2[]{
			new Product2(Name='testProd1', Description='Test Product One', Product_Type__c = 'Marine'),
			new Product2(Name='testProd2', Description='Test Product Two', Product_Type__c = 'Energy'),
			new Product2(Name='testProd3', Description='Test Product Three', Product_Type__c = 'Energy;Marine'),
			new Product2(Name='testProd3', Description='Test Product Four', Product_Type__c = 'Energy;Marine')
		};
		insert products;
		
		PricebookEntry[] standardPrices = new PricebookEntry[]{
			new PricebookEntry(Pricebook2Id = standardPb.Id, Product2Id = products[0].Id, UnitPrice=1, isActive=true),
			new PricebookEntry(Pricebook2Id = standardPb.Id, Product2Id = products[1].Id, UnitPrice=1, isActive=true),
			new PricebookEntry(Pricebook2Id = standardPb.Id, Product2Id = products[2].Id, UnitPrice=1, isActive=true)
		};
		insert standardPrices;
		
		/*PricebookEntry[] priceBookEntries = new PricebookEntry[]{
			new PricebookEntry(Pricebook2Id = priceBooks[0].Id, Product2Id = products[0].Id, UnitPrice=1, UseStandardPrice=true, isActive=true),
			new PricebookEntry(Pricebook2Id = priceBooks[1].Id, Product2Id = products[0].Id, UnitPrice=1, UseStandardPrice=true, isActive=true),
			new PricebookEntry(Pricebook2Id = priceBooks[0].Id, Product2Id = products[1].Id, UnitPrice=1, UseStandardPrice=true, isActive=true),
			new PricebookEntry(Pricebook2Id = priceBooks[1].Id, Product2Id = products[2].Id, UnitPrice=1, UseStandardPrice=true, isActive=true)
		};
		insert priceBookEntries;*/
		
		RecordType oppMarineRecType = [SELECT Id FROM RecordType WHERE SObjectType='Opportunity' AND Name='Marine'];
		RecordType oppEnergyRecType = [SELECT Id FROM RecordType WHERE SObjectType='Opportunity' AND Name='Energy'];
		
		
		testOpps = new Opportunity[]{
			new Opportunity(Name='testOpp1', RecordTypeId=oppMarineRecType.Id, StageName='Renewable Opportunity', CloseDate=Date.Today()+7, CurrencyISOCode = currencyISOCode),
			new Opportunity(Name='testOpp2', RecordTypeId=oppEnergyRecType.Id, StageName='Renewable Opportunity', CloseDate=Date.Today()+7, CurrencyISOCode = currencyISOCode)
		};
		insert testOpps;
		
		oppLineItems = new OpportunityLineItem[]{
			new OpportunityLineItem(OpportunityId 			= testOpps[1].Id, 
									PriceBookEntryId		= standardPrices[1].Id, 
									Line_Size__c 			= 100,
									Actual_Premium__c 		= 100,
									Prognosis_Comments__c 		= 'Testing',
									Record_Adjustment__c 	= 1,
									General_Increase__c 	= 1,
									Volume_Adjustment__c 	= 1,
									LR__c					= 1,
									Renewal_Premium__c		= 100,
									RA__c					= 1,
									Renewable_Probability__c= 100
									)
		};
		insert oppLineItems;
		
		//retreive the opp line items to get all needed fields
		oppLineIds = new Set<Id>();
		for(OpportunityLineItem anOLI: oppLineItems){
			oppLineIds.add(anOLI.Id);
		}
		//oppLineItems = [SELECT Id, Actual_Premium__c, Budget_Comments__c, CurrencyIsoCode, Budget_Premium__c, Record_Adjustment__c, General_Increase__c,
		//						LR__c, OpportunityId, Product_Name__c, Renewal_Premium__c, RA__c, Renewable_Probability__c, Variance__c, Volume_Adjustment__c
		//						
		//				FROM OpportunityLineItem 
		//				WHERE
		//				Id in:oppLineIds];
		
		//Useful metrics
		AggregateResult ar = [SELECT COUNT(Id) FROM PriceBookEntry WHERE Product2.Product_Type__c includes ('Marine') AND PriceBook2.IsStandard=true AND CurrencyISOCode = :currencyISOCode];
		noOfMarineProds = (Integer)ar.get('expr0');
		System.Debug('Number of Marine products: ' + noOfMarineProds);
		
		AggregateResult ar2 = [SELECT COUNT(Id) FROM PriceBookEntry WHERE Product2.Product_Type__c includes ('Energy') AND PriceBook2.IsStandard=true AND CurrencyISOCode = :currencyISOCode];
		noOfEnergyProds = (Integer)ar2.get('expr0');
		System.Debug('Number of Energy products: ' + noOfEnergyProds);
		
		
	}

    static testMethod void myUnitTest() {
        setupTestData();        
        Test.startTest();
        
        	List<Prognosis__c> progs = new List<Prognosis__c>();
        	for(OpportunityLineItem anOLI: oppLineItems){
        		Prognosis__c aProg = Prognosis.getPrognosis(anOLI);
        		progs.add(aProg);
        		//Check that all the values are correct
        		//System.AssertEquals(aProg.Budget_Year__c, testOpps[1].Budget_Year__c);
			 	//System.AssertEquals(aProg.Opportunity__c, testOpps[1].Name);
			 	//System.AssertEquals(aProg.OwnerId, testOpps[1].OwnerId);
		
		     	System.AssertEquals(aProg.Actual_Premium__c					, anOLI.Actual_Premium__c);
		     	
		     	System.AssertEquals(aProg.Comments__c						, anOLI.Prognosis_Comments__c);
		     	System.AssertEquals(aProg.CurrencyIsoCode					, anOLI.CurrencyIsoCode);
		     	System.AssertEquals(aProg.Date__c							, System.today());
		     	//System.AssertEquals(aProg.Time__c							, anOLI.System.now();
		     	System.AssertEquals(aProg.EPI__c							, anOLI.Prognosis_EPI__c);
		     	//System.AssertEquals(aProg.Expected_Record_Adjustment__c	, anOLI.Record_Adjustment__c);
		     	System.AssertEquals(aProg.GI__c								, anOLI.General_Increase__c);
		     	System.AssertEquals(aProg.LR__c								, anOLI.LR__c);
		     	System.AssertEquals(aProg.OpportunityId__c					, anOLI.OpportunityId);
		     	
		     	//System.AssertEquals(aProg.Opportunity_Id__c					, anOLI.OpportunityId);
		     	
		     	System.AssertEquals(aProg.Product__c						, anOLI.Product_Name__c);
		     	System.AssertEquals(aProg.Prognosis_Renewable_Premium__c	, anOLI.Renewal_Premium__c);
		     	System.AssertEquals(aProg.RA__c								, anOLI.RA__c);
		     	System.AssertEquals(aProg.Renewal_Probability__c			, anOLI.Renewable_Probability__c);
		     	System.AssertEquals(aProg.Variance__c						, anOLI.Prognosis_Variance__c);
		     	System.AssertEquals(aProg.Volume_Adjustment__c				, anOLI.Volume_Adjustment__c);
        		
        	}
        	
        	System.Debug('Prognosis: ' + progs);
        	System.Debug('olis: ' + oppLineItems);
        	
        	//Requery for the opp line items excluding the opportunity object 
        	//this should cause an System.SObjectException exception that is caught in the 
        	//class
        	oppLineItems = [SELECT Id, Actual_Premium__c, Prognosis_Comments__c, CurrencyIsoCode, Prognosis_EPI__c, Record_Adjustment__c, General_Increase__c,
								LR__c, OpportunityId, Product_Name__c, Renewal_Premium__c, RA__c, Renewable_Probability__c, Prognosis_Variance__c, Volume_Adjustment__c
						FROM OpportunityLineItem 
						WHERE
						Id in:oppLineIds];
			//Repeat the test
			progs = new List<Prognosis__c>();
        	for(OpportunityLineItem anOLI: oppLineItems){
        		Prognosis__c aProg = Prognosis.getPrognosis(anOLI);
        		progs.add(aProg);
        		//System.AssertEquals(aProg.Budget_Year__c, testOpps[1].Budget_Year__c);
			 	//System.AssertEquals(aProg.Opportunity__c, testOpps[1].Name);
			 	//System.AssertEquals(aProg.OwnerId, testOpps[1].OwnerId);
		
		     	System.AssertEquals(aProg.Actual_Premium__c					, anOLI.Actual_Premium__c);
		     	
		     	System.AssertEquals(aProg.Comments__c						, anOLI.Prognosis_Comments__c);
		     	System.AssertEquals(aProg.CurrencyIsoCode					, anOLI.CurrencyIsoCode);
		     	System.AssertEquals(aProg.Date__c							, System.today());
		     	//System.AssertEquals(aProg.Time__c							, anOLI.System.now();
		     	System.AssertEquals(aProg.EPI__c							, anOLI.Prognosis_EPI__c);
		     	//System.AssertEquals(aProg.Expected_Record_Adjustment__c	, anOLI.Record_Adjustment__c);
		     	System.AssertEquals(aProg.GI__c								, anOLI.General_Increase__c);
		     	System.AssertEquals(aProg.LR__c								, anOLI.LR__c);
		     	System.AssertEquals(aProg.OpportunityId__c					, anOLI.OpportunityId);
		     	
		     	//System.AssertEquals(aProg.Opportunity_Id__c					, anOLI.OpportunityId);
		     	
		     	System.AssertEquals(aProg.Product__c						, anOLI.Product_Name__c);
		     	System.AssertEquals(aProg.Prognosis_Renewable_Premium__c	, anOLI.Renewal_Premium__c);
		     	System.AssertEquals(aProg.RA__c								, anOLI.RA__c);
		     	System.AssertEquals(aProg.Renewal_Probability__c			, anOLI.Renewable_Probability__c);
		     	System.AssertEquals(aProg.Variance__c						, anOLI.Prognosis_Variance__c);
		     	System.AssertEquals(aProg.Volume_Adjustment__c				, anOLI.Volume_Adjustment__c);        		
        	}
        	
        	System.Debug('Prognosis: ' + progs);
        	System.Debug('olis: ' + oppLineItems);        	
        	
        	//Requery for the opp line items including the opportunity object
        	oppLineItems = [SELECT Id, Actual_Premium__c, Prognosis_Comments__c, CurrencyIsoCode, Prognosis_EPI__c, Record_Adjustment__c, General_Increase__c,
								LR__c, OpportunityId, Product_Name__c, Renewal_Premium__c, RA__c, Renewable_Probability__c, Prognosis_Variance__c, Volume_Adjustment__c,
								Opportunity.Name, Opportunity.Budget_Year__c, Opportunity.OwnerId
						FROM OpportunityLineItem 
						WHERE
						Id in:oppLineIds];
			//Repeat the test
			progs = new List<Prognosis__c>();
        	for(OpportunityLineItem anOLI: oppLineItems){
        		Prognosis__c aProg = Prognosis.getPrognosis(anOLI);
        		progs.add(aProg);
        		//System.AssertEquals(aProg.Budget_Year__c, testOpps[1].Budget_Year__c);
			 	//System.AssertEquals(aProg.Opportunity__c, testOpps[1].Name);
			 	//System.AssertEquals(aProg.OwnerId, testOpps[1].OwnerId);
		
		     	System.AssertEquals(aProg.Actual_Premium__c					, anOLI.Actual_Premium__c);
		     	
		     	System.AssertEquals(aProg.Comments__c						, anOLI.Prognosis_Comments__c);
		     	System.AssertEquals(aProg.CurrencyIsoCode					, anOLI.CurrencyIsoCode);
		     	System.AssertEquals(aProg.Date__c							, System.today());
		     	//System.AssertEquals(aProg.Time__c							, anOLI.System.now();
		     	System.AssertEquals(aProg.EPI__c							, anOLI.Prognosis_EPI__c);
		     	//System.AssertEquals(aProg.Expected_Record_Adjustment__c	, anOLI.Record_Adjustment__c);
		     	System.AssertEquals(aProg.GI__c								, anOLI.General_Increase__c);
		     	System.AssertEquals(aProg.LR__c								, anOLI.LR__c);
		     	System.AssertEquals(aProg.OpportunityId__c					, anOLI.OpportunityId);
		     	
		     	//System.AssertEquals(aProg.Opportunity_Id__c					, anOLI.OpportunityId);
		     	
		     	System.AssertEquals(aProg.Product__c						, anOLI.Product_Name__c);
		     	System.AssertEquals(aProg.Prognosis_Renewable_Premium__c	, anOLI.Renewal_Premium__c);
		     	System.AssertEquals(aProg.RA__c								, anOLI.RA__c);
		     	System.AssertEquals(aProg.Renewal_Probability__c			, anOLI.Renewable_Probability__c);
		     	System.AssertEquals(aProg.Variance__c						, anOLI.Prognosis_Variance__c);
		     	System.AssertEquals(aProg.Volume_Adjustment__c				, anOLI.Volume_Adjustment__c);        		
        	}
        	
        	System.Debug('Prognosis: ' + progs);
        	System.Debug('olis: ' + oppLineItems);
        	
        	//test the list method just for coverage, it calls the individual method so we can assume that if that works
        	//then this should work
        	List<Prognosis__c> proglist = Prognosis.getPrognosisList(oppLineItems);
        				
        Test.stopTest();
    }
}