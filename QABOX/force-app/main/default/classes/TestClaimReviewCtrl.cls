@isTest
public class TestClaimReviewCtrl
{
    
    static testMethod void cover()
    {   
        list<MyGardDocumentAvailability__c> docAvailableList = new List<MyGardDocumentAvailability__c>();
        MyGardDocumentAvailability__c docAvailabilityPF = new MyGardDocumentAvailability__c(name='My Portfolio Document Tab',IsAvailable__c=true);
        docAvailableList.add(docAvailabilityPF);
        MyGardDocumentAvailability__c docAvailabilityOD = new MyGardDocumentAvailability__c(name='Object Details Document Tab',IsAvailable__c=true);
        docAvailableList.add(docAvailabilityOD);
        MyGardDocumentAvailability__c docAvailabilityBC = new MyGardDocumentAvailability__c(name='P&I Renewal Blue Card Tab',IsAvailable__c=true);
        docAvailableList.add(docAvailabilityBC);
        MyGardDocumentAvailability__c docAvailabilityRC = new MyGardDocumentAvailability__c(name='P&I Renewal Certificate Tab',IsAvailable__c=true);
        docAvailableList.add(docAvailabilityRC);
        insert docAvailableList;
        
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        GardTestData test_record = new GardTestData();
        test_record.commonRecord();
        PIrenewalAvailability__c PIRenAv = new PIrenewalAvailability__c(Name = 'value',
                                                                        Enable_Certificate__c = true, Enable_General_increase__c= true,
                                                                        Enable_Renewal_terms__c = true
                                                                        );
        
        MLC_Certificate_availability__c testMLC = new MLC_Certificate_availability__c(
                                                                                         Name = 'MLC availability',
                                                                                         Value__c = true
                                                                                     );
        insert testMLC;
        
        insert PIRenAv ;
        system.runAs(GardTestData.brokerUser)
        {
            Test.startTest();
            ClaimReviewCtrl crc_broker = new ClaimReviewCtrl();
            System.currentPageReference().getParameters().put('favId',String.valueOf(GardTestData.test_fav.ID));
            ClaimReviewCtrl crc_broker1 = new ClaimReviewCtrl();
            ClaimReviewCtrl.ClaimReviewWrapper ClmWrappr = new ClaimReviewCtrl.ClaimReviewWrapper();
            //ClaimReviewCtrl crc_broker = new ClaimReviewCtrl();
            ClmWrappr.currentComment = 'test comment';
            String sortDir = crc_broker.getSortDirection();
            /*crc_broker.yearSet = new set<Integer>();
            crc_broker.objSet = new set<String>();
            crc_broker.claimTypeSet = new set<String>();
            //crc_broker.yearSet.add(2011);
            //crc_broker.objSet.add('hennessey');
            //crc_broker.claimTypeSet.add('Cargo');*/
            crc_broker.filterRecords();
            crc_broker.fetchFilterList();
            
            crc_broker.getClaimStatusLst();
            
            //getFilterCriteria();
            //getAllClaimDetails();
            crc_broker.getClientLst();
            crc_broker.getShares();
            crc_broker.newFilterName = 'test';
            crc_broker.createFavourites();
            crc_broker.deleteFavourites();
            crc_broker.fetchFavourites();
            crc_broker.caseId = String.valueOf(GardTestData.brokerCase.ID);
            crc_broker.caseId = GardTestData.brokerClaim.id;
            crc_broker.showCaseDetails();
            crc_broker.setpageNumber();
            crc_broker.hasNext = true;
            crc_broker.hasPrevious = false;
            crc_broker.pageNumber = 12;
            crc_broker.navigate();
            
            crc_broker.SaveClaimReviews();
            
            crc_broker.first();
            crc_broker.next();
            crc_broker.last();
            crc_broker.previous();
            crc_broker.clearOptions();
            crc_broker.exportToExcel();
            crc_broker.print();
            String testStr = ClaimReviewCtrl.getGenerateTimeStamp();
            String testStr1 = ClaimReviewCtrl.getGenerateTime();
            String testStr2 = ClaimReviewCtrl.getUserName();
            crc_broker.SaveClaimReviews();
            crc_broker.getYearLst();
            crc_broker.getObjectName();
            crc_broker.getClaimTypeLst();
            crc_broker.selectedClaim = GardTestData.brokerCase.Id + '#' + GardTestData.clientCase.Id ;
            crc_broker.showClaimPopup();
            PageReference pageRef = page.ClaimsReview;
            Test.setCurrentPage(pageRef);
            crc_broker.fetchSelectedClients();
            crc_broker.sendBackToForm();
            ClaimReviewCtrl.callmiddleware(GardTestData.brokerCase.Id , 'test');
            crc_broker.claimsReviewData();
            //Mriganko editing start
            crc_broker.selectedClaim = GardTestData.brokerClaim.id + '#' + '1';
            crc_broker.showClaimPopup();
            List<RecordType> caseRecordType = new List<RecordType>();
            caseRecordType = [select id,name from recordtype where sObjectType =: 'Case'];
            if(caseRecordType.size() > 0){
                GardTestData.brokerCase.recordTypeid = caseRecordType[0].id;
                GardTestData.brokerCase.claim__c = GardTestData.brokerClaim.id;
                update GardTestData.brokerCase;
            }
            ClaimReviewCtrl.ClaimReviewWrapper clmWrp = new  ClaimReviewCtrl.ClaimReviewWrapper();
            clmWrp.currentComment = GardTestData.brokerCase.description;
            clmWrp.currentComment = 'TestComment';
            clmWrp.wrapperCase = GardTestData.brokerClaim;
            //clmWrp.
            crc_broker.lstClaimWrapper = new List<ClaimReviewCtrl.ClaimReviewWrapper>();
            crc_broker.lstClaimWrapper.add(clmWrp);
            crc_broker.SaveClaimReviews();
            crc_broker.fetchFilterList();
            crc_broker.userType = 'Client';
            crc_broker.fetchAllClaimDetails();
            //end
            Test.stopTest();
         }
    }
}