public class EmailInfoWrapper{
/*
    //Constructors
    public EmailInfoWrapper(){
        sendCopyToExtranetAdmin = false;
        mailSignature = 'Kind Regards,<br/>MyGard';
        footer = '<div style="font-size: 10px;">This email has been sent for and on behalf of an entity of the Gard Group comprised of, inter alia; Gard P. & I. (Bermuda) Ltd, Assuranceforeningen Gard - gjensidig -, Gard Marine & Energy Limited and Gard Marine & Energy Insurance (Europe) AS. Gard AS is registered as an insurance intermediary by the Norwegian Financial Supervisory Authority.</div>' + 
                 '<br/><div style="font-size: 11px;">- CONFIDENTIALITY NOTICE – </div>' + 
                 '<div style="font-size: 10px;">This message is confidential and may be protected by legal privilege. Access to this message by any person other than the addressee is unauthorised and any disclosure or copying of or reliance upon the information contained in this message or any attachments is prohibited. If you receive this message in error you must preserve its confidentiality, advise the sender and delete the material from your computer.<a href="www.gard.no">www.gard.no</a></div>';
        logo = '';//'<img src="https://c.cs14.content.force.com/servlet/servlet.ImageServer?id=015c0000000Awct&oid=00Dc0000003tGeb&lastMod=1410272055000" height="60" width="120"/>';
    }
    
    //Gard Extranet Administrator Information
    public String gardExtranetAdministrator{get;set;}
    public String gardExtranetAdministratorEmail{get;set;}
    
    //New User Information
    public String newUserInfoFirstName{get;set;}
    public String newUserInfoLastName{get;set;}
    public String newUserInfoPhone{get;set;}
    public String newUserInfoMobile{get;set;}
    public String newUserInfoEmail{get;set;}
    public boolean sendCopyToExtranetAdmin{get;set;}
    
    //Client/Broker Admin Information
    public String clientBrokerOrganization{get;set;}
    public String clientBrokerAdminFirstName{get;set;}
    public String clientBrokerAdminLastName{get;set;}
    public String clientBrokerAdminPhone{get;set;}
    public String clientBrokerAdminMobile{get;set;}
    public String clientBrokerAdminEmail{get;set;}
    
    public String removeUserInfoName{get;set;}
    
    //change of Admin functionality
    public String clientBrokerOldAdminName{get;set;}
    public String clientBrokerNewAdminName{get;set;}
    
    //Mail signature and other properties
    public String mailSignature{get;set;}
    public String footer{get;set;}
    public String logo{get;set;}
    
    //Contact Me - Gard contact Email variables
    public String contactMe_recipentName {get;set;}
    public String contactMe_loggedInUser {get;set;}
    public String contactMe_loggedInUserOrganization {get;set;}
    public String contactMe_area {get;set;}
    public String contactMe_client {get;set;}
    public String contactMe_cover {get;set;}
    public String contactMe_claimType{get;set;}
    public String contactMe_subject {get;set;}
    public boolean contactMe_phonePreference{get;set;}
    public boolean contactMe_emailPreference {get;set;}
    
    //Start Sharing records with Client - variables
    public String clientOrganization{get;set;}
    
    //Contact Me Recipient
    //public String contactMeRecipientName{get;set;}
    public String contactMeRecipientEmail{get;set;}
    
    //Register a New Claim Functionality
    public String registerClaim_recipientEmail{get;set;}
    public String registerClaim_recipientName{get;set;}
    
    public String registerClaim_caseID{get;set;}
    public String registerClaim_myClaimId{get;set;}
    public String registerClaim_userType{get;set;}
    public String registerClaim_clientName{get;set;}
    public String registerClaim_referencenumber{get;set;}
    public String registerClaim_objectName{get;set;}
    public String registerClaim_eventDate{get;set;}
    public String registerClaim_place{get;set;}
    public String registerClaim_coverName{get;set;}
    public String registerClaim_claimType{get;set;}
    public String registerClaim_claimDescription{get;set;}
    
    public String registerClaim_name{get;set;}
    public String registerClaim_rank{get;set;}
    public String registerClaim_nationality{get;set;}
    public String registerClaim_claimDetail{get;set;}
    
    public String registerClaim_portOfEmbarkation{get;set;}
    public String registerClaim_dateOfEmbarkation{get;set;}
    public String registerClaim_numberofStowaways{get;set;}
    
    //Send attachment
    public List<attachment> attachmentlist{get;set;}
    
    //editObject functionality
    //public String editObject_to_recipeintemails{get;set;}
    //public String editObject_cc_recipeintemails{get;set;}
    public List<String> editObject_to_recipeintemails{get;set;}
    public List<String> editObject_cc_recipeintemails{get;set;}
    public String editObject_emailBody{get;set;}
    public String editObject_fromemail{get;set;}
    
    //user Feedback functionality
    public String feedbackId{get;set;}
    public String feedbackComments{get;set;}
    public String feedbackReason{get;set;}
    public String feedbackRating{get;set;}
    public String feedbackObjective{get;set;}
    
    
    
        
    public void send(String emailAction){
        //reserving email capacity for the current Apex transaction
        Messaging.reserveSingleEmailCapacity(2);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        //Set recipient explicitly for contact me
        if(emailAction.equalsIgnoreCase('contactme')){
            mail.setToAddresses(new String[] {contactMeRecipientEmail}); 
            //mail.setToAddresses(new String[] {contactMeRecipientEmail, 'aritram1@gmail.com'});        
        }
        //Set recipient explicitly for claim assignment logic
        else if(emailAction.equalsIgnoreCase('registerclaim_copytoRequestor') || emailAction.equalsIgnoreCase('registerclaim_copytoClaimHandler')){    
            mail.setToAddresses(new String[] {registerClaim_recipientEmail});   
            //mail.setToAddresses(new String[] {'aritra.mukherjee@gard.no'});    //hardcoded for instant debugging
        }
        //For Edit Object functionality
        else if(emailAction.equalsIgnoreCase('editObject')){
            //mail.setToAddresses(new String[] {editObject_to_recipeintemails, 'aritram1@gmail.com'});
            //mail.setccAddresses(new String[] {editObject_cc_recipeintemails, 'aritram1@gmail.com'});
            
            //mail.setToAddresses(new String[] {'ownergard@gmail.com'});
            
            mail.setToAddresses(editObject_to_recipeintemails);
            mail.setccAddresses(editObject_cc_recipeintemails);
        }
        //For User Feedback functionality
        else if(emailAction.equalsIgnoreCase('userFeedback')){            
            String[] lstRecipient=new String[]{};
            for(HomeFeedbackRecipient__c objFeedbackRecipient:HomeFeedbackRecipient__c.getAll().values()){
              lstRecipient.add(objFeedbackRecipient.Email__c);  
            }
            
            mail.setToAddresses(lstRecipient);
            //mail.setToAddresses(new String[] {'Lene-camilla.nordlie@gard.no','Torgrim.andersen@gard.no','Ingebjorg.eliassen@gard.no','Alice.amundsen@gard.no'});                      
        }
        //For User Management emails. Recipient (Gard Admin) Value exported from Custom Settings
        else{
            mail.setToAddresses(new String[] {gardExtranetAdministratorEmail});                          //hardcoded for instant debugging
            //mail.setToAddresses(new String[] {gardExtranetAdministratorEmail, 'aritram1@gmail.com'});    //hardcoded for instant debugging
        }
        
        if(sendCopyToExtranetAdmin == true){
            mail.setccAddresses(new String[] {clientBrokerAdminEmail});
        }
        mail = createBody(mail, emailAction);
        
        //System.debug('emailAction-->' + emailAction);
        //System.debug('mail-->' + mail); 
        
        if((gardExtranetAdministratorEmail != null  &&  gardExtranetAdministratorEmail != '')  ||
            (registerClaim_recipientEmail   != null &&  registerClaim_recipientEmail   != '')  ||
            (contactMeRecipientEmail != null  &&  contactMeRecipientEmail  != '')           )
        {
            if(!Test.isRunningTest())
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        else if((editObject_to_recipeintemails != null  &&  editObject_to_recipeintemails.size()>0))
        {
            if(!Test.isRunningTest())
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        
        else if(emailAction.equalsIgnoreCase('userFeedback'))
        {
            if(!Test.isRunningTest())
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }    
        }  
    
    public Messaging.SingleEmailMessage createBody(Messaging.SingleEmailMessage mail, String emailAction){
        
        String emailPlainTextBody, emailHTMLBody, mail_subject ;
        String startrow = '<tr><td style="vertical-align: top; font-family: verdana;">';
        String separator = '</td><td style="vertical-align: top; font-family: verdana;"> : </td><td style="vertical-align: top; font-family: verdana;">';
        String endrow = '</td></tr>';
        
        if(emailAction == 'adduser'){
            mail_subject = clientBrokerAdminFirstName + ' ' + clientBrokerAdminLastName + ' from ' + clientBrokerOrganization  + ' requested to add ' + 
                              newUserInfoFirstName + ' ' + newUserInfoLastName;
            emailPlainTextBody = '';
            emailHTMLBody = logo + '<br/><br/>' +
                                
                                'Dear ' + gardExtranetAdministrator + ',' + '<br/><br/>' + 
                                clientBrokerAdminFirstName + ' ' + clientBrokerAdminLastName + ', who is an administrator user of organization - '  +  clientBrokerOrganization + 
                                ' has requested to provide the access for a new user. The new user details are as follow:' + '<br/><br/>' +
                                '<u>' + 'New User Details:' + '</u>' + + '<br/><br/>' + 
                                
                                '<table style="margin: 0 0 0 20px;">' + 
                                startrow + '• First name'     + separator + newUserInfoFirstName + endrow + 
                                startrow + '• Last name'      + separator + newUserInfoLastName  + endrow + 
                                startrow + '• Phone'          + separator + newUserInfoPhone     + endrow + 
                                startrow + '• Email address'  + separator + newUserInfoEmail     + endrow +
                                '</table>' + '<br/>' + 
                                
                                clientBrokerAdminFirstName + ' ' + clientBrokerAdminLastName + ' can be reached on:' + '<br/><br/>' +
                                
                                //'<table style="font-family:Calibri">' + 
                                '<table style="margin: 0 0 0 20px;">' + 
                                startrow + '• Phone'         + separator + clientBrokerAdminPhone  + endrow + 
                                startrow + '• Mobile'        + separator + clientBrokerAdminMobile + endrow + 
                                startrow + '• Email address' + separator + clientBrokerAdminEmail  + endrow + 
                                '</table>' + '<br/><br/>' + 
                                
                                mailSignature + '<br/><br/>' +
                                footer;
        }
        
         else if(emailAction == 'userFeedback'){
            mail_subject = clientBrokerAdminFirstName + ' ' + clientBrokerAdminLastName + ' from ' + clientBrokerOrganization  + ' has submitted feedback on the Extranet application'; 
                              
            emailPlainTextBody = '';
            
            feedbackComments = feedbackComments.replace('\n', '<br/>');
            feedbackReason = feedbackReason.replace('\n', '<br/>');
            
            String myUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/mygard';             
            emailHTMLBody = logo + '<br/><br/>' + 
                            'Dear Sirs' +  ',' + '<br/><br/>' + 
                             clientBrokerAdminFirstName + ' ' + clientBrokerAdminLastName + '   of   '  +  clientBrokerOrganization + 
                            ' has submitted the following feedback on MyGard.' + '<br/><br/>' +
                            '<table style="margin: 0 0 0 20px;">' +
                            
                            startrow  + '• Comments' + separator + feedbackComments + endrow + 
                            startrow  + '• Main reason for visiting MyGard today' + separator + feedbackReason + endrow + 
                            startrow  + '• Overall site rating' + separator + feedbackRating + endrow + 
                            startrow  + '• Objective accomplished through MyGard' + separator + feedbackObjective + endrow + 
                            '</table>' +
                            '<br/>' + 'The feedback is also stored in Salesforce. To view the feedback, please click the link below:' + '<br/><br/>' +
                            
                            '<a href="'+ myUrl +'/'+feedbackId +'">Click for Feedback Details</a>'+
                            '<br/> <br/>'+
                            clientBrokerAdminFirstName + ' ' + clientBrokerAdminLastName + ' can be reached on:' + '<br/><br/>' + 
                            '<table style="margin: 0 0 0 20px;">' +
                            startrow + '• Phone' + separator + clientBrokerAdminPhone + endrow + 
                            startrow + '• Mobile' + separator + clientBrokerAdminMobile + endrow +
                            startrow + '• Email address' + separator + clientBrokerAdminEmail + endrow +
                            '</table>' + '<br/>' +
                            mailSignature + '<br/><br/>' +
                            footer;
        }

        else if(emailAction == 'removeuser'){
            mail_subject = clientBrokerAdminFirstName + ' ' + clientBrokerAdminLastName + ' requested to remove ' +
                              removeUserInfoName + ' from Gard Application';
            emailPlainTextBody = '';
            emailHTMLBody = logo + '<br/><br/>' + 
                            'Dear ' + gardExtranetAdministrator + ',' + '<br/><br/>' + 
                            clientBrokerAdminFirstName + ' ' + clientBrokerAdminLastName + ' who is an Administrator user at ' + clientBrokerOrganization + 
                            ' has requested that the access of user ' + removeUserInfoName + ' to the MyGard portal is revoked.' + 
                            '<br/> <br/>' + 
                            
                            //removeUserInfoName + ' also needs to be removed as a contact for ' + clientBrokerOrganization + '.' + '<br/><br/>' + 
                            
                            clientBrokerAdminFirstName + ' ' + clientBrokerAdminLastName + ' can be reached on:' + '<br/><br/>' + 
                            '<table style="margin: 0 0 0 20px;">' + 
                            //'<table style="font-family:Calibri">' + 
                            startrow + '• Phone'         + separator + clientBrokerAdminPhone  + endrow + 
                            startrow + '• Mobile'        + separator + clientBrokerAdminMobile + endrow + 
                            startrow + '• Email address' + separator + clientBrokerAdminEmail  + endrow + 
                            '</table>' + 
                            '<br/>' +
                            mailSignature + '<br/><br/>' +
                            footer;
        }
        
        else if(emailAction == 'makeadmin'){
            mail_subject = clientBrokerOldAdminName + ' requested to make ' + 
                              clientBrokerNewAdminName + ' as the new Administrator for ' + clientBrokerOrganization;
            emailPlainTextBody = '';
            emailHTMLBody = logo + '<br/><br/>' + 
                            'Dear ' + gardExtranetAdministrator + ',' + '<br/><br/>' + 
                            clientBrokerOldAdminName + ' who is an Administrator user at ' + clientBrokerOrganization + 
                            ' has requested a change of Administrator user.' +  
                            '<br/><br/>' +
                            
                            'The new Administrator user for ' + clientBrokerOrganization + ' should be ' + clientBrokerNewAdminName + '.' + 
                            '<br/><br/>' +
                            
                            'Please contact ' + clientBrokerOldAdminName + ' in case of any queries.' + '<br/><br/>' + 
                            
                            clientBrokerOldAdminName + ' can be reached on:' + '<br/><br/>' + 
                            
                            //'<table style="font-family:Calibri">' + 
                            '<table style="margin: 0 0 0 20px;">' + 
                            startrow + '• Phone'         + separator + clientBrokerAdminPhone  + endrow + 
                            startrow + '• Mobile'        + separator + clientBrokerAdminMobile + endrow + 
                            startrow + '• Email address' + separator + clientBrokerAdminEmail  + endrow + 
                            '</table>' + 
                            '<br/>' +
                            
                            mailSignature + '<br/><br/>' +
                            footer;
        }

else if(emailAction == 'startshare'){
            mail_subject = clientBrokerOldAdminName + ' is now sharing all the records with ' + clientOrganization;
            emailPlainTextBody = '';
            emailHTMLBody = logo + '<br/><br/>' + 
                            'Dear ' + gardExtranetAdministrator + ',' + '<br/><br/>' + 
                            
                            clientBrokerOldAdminName + ' who is an Administrator user at ' + clientBrokerOrganization + 
                            ' is now sharing all the records with ' + clientOrganization + '.' + 
                            '<br/><br/>' +
                            
                            'All extranet users of ' + clientOrganization + ' will now be able to see ' + clientBrokerOrganization + 
                            '’s records pertaining to their organisation in the extranet.' + 
                            '<br/><br/>' +
                            
                            'Please contact ' + clientBrokerOldAdminName + ' in case of any queries.' + '<br/><br/>' + 
                            
                            clientBrokerOldAdminName + ' can be reached on:' + '<br/>' + 
                            
                            '<table style="margin: 0 0 0 20px;">' + 
                            startrow + '• Phone'         + separator + clientBrokerAdminPhone  + endrow + 
                            startrow + '• Mobile'        + separator + clientBrokerAdminMobile + endrow + 
                            startrow + '• Email address' + separator + clientBrokerAdminEmail  + endrow + 
                            '</table>' + 
                            '<br/>' +
                            
                            mailSignature + '<br/><br/>' +
                            footer;
        }







        //stopshare block
        else if(emailAction == 'stopshare'){
            mail_subject = clientBrokerOldAdminName + ' - The Administrator user of organization ' + clientBrokerOrganization + 
            ' has stopped sharing all the records with ' + clientOrganization;
            emailPlainTextBody = '';
            emailHTMLBody = logo + '<br/><br/>' + 
                            'Dear ' + gardExtranetAdministrator + ',' + '<br/><br/>' + 
                            
                            clientBrokerOldAdminName + ' who is an Administrator user at ' + clientBrokerOrganization + 
                            ' has stopped sharing all the records with ' + clientOrganization + '.' + 
                            '</br> </br>' +
                            
                            'Please contact ' + clientBrokerOldAdminName + ' in case of any queries.' + '<br/><br/>' + 
                            
                            clientBrokerOldAdminName + ' can be reached on:' + '<br/><br/>' + 
                            
                            '<table style="margin: 0 0 0 20px;">' + 
                            startrow + '• Phone'         + separator + clientBrokerAdminPhone  + endrow + 
                            startrow + '• Mobile'        + separator + clientBrokerAdminMobile + endrow + 
                            startrow + '• Email address' + separator + clientBrokerAdminEmail  + endrow + 
                            '</table>' + 
                            '<br/>' +
                            
                            mailSignature + '<br/><br/>' +
                            footer;
                            
        }
        else if(emailAction == 'contactme'){
            mail_subject = contactMe_loggedInUser + ' from ' + contactMe_loggedInUserOrganization  + ' has placed a request for contact';
            
            System.debug('contactMe_subject -- previous --' +contactMe_subject );
            contactMe_subject = contactMe_subject.replace('\n', '<br/>');
            System.debug('contactMe_subject -- post --' +contactMe_subject );
            
            String str_contactMe_phonePreference = 'No';
            String str_contactMe_emailPreference = 'No'; 
            
            if(contactMe_phonePreference)    str_contactMe_phonePreference = 'Yes';
            if(contactMe_emailPreference)    str_contactMe_emailPreference = 'Yes';
            
            String areaInfo = '';
            if(contactMe_area != null && contactMe_area != 'N/A'){
                areaInfo = startrow + '• Area' + separator + contactMe_area + endrow;
            }
            
            String clientInfo = '';
            if(contactMe_client != null && contactMe_client != 'N/A'){
                clientInfo = startrow + '• Client' + separator + contactMe_client + endrow;
            }
            
            String coverInfo = '';
            if(contactMe_cover != null && contactMe_cover != 'N/A'){
                coverInfo = startrow + '• Cover' + separator + contactMe_cover + endrow;
            }
            
            String claimTypeInfo = '';
            if(contactMe_claimType != null && contactMe_claimType != 'N/A'){
                claimTypeInfo = startrow + '• Claim type' + separator + contactMe_claimType + endrow;
            }
            
            String subjectInfo = '';
            if(contactMe_subject != null && contactMe_subject != 'N/A'){
                //subjectInfo = '<tr><td style="vertical-align: top; font-family: verdana;">' + '• Subject' + '</td><td style="vertical-align: top; font-family: verdana;">:</td><td style="vertical-align: top;">' + contactMe_subject + endrow;
                subjectInfo = startrow + '• Subject' + separator + contactMe_subject + endrow;
            }
            
            emailPlainTextBody = '';
            emailHTMLBody = logo + '<br/><br/>' + 
                            'Dear ' + contactMe_recipentName + ',' + '<br/><br/>' + 
                            contactMe_loggedInUser + ' of ' + contactMe_loggedInUserOrganization + 
                            ' has requested that Gard contact him/her. Please follow up with ' +  contactMe_loggedInUser + '.' +
                            '<br/> <br/>' +
                            
                            contactMe_loggedInUser + ' can be reached on:' + '<br/><br/>' + 
                            
                            '<table style="margin: 0 0 0 20px;">' + 
                            startrow + '• Phone'          + separator + clientBrokerAdminPhone + endrow + 
                            startrow + '• Mobile'         + separator + clientBrokerAdminMobile + endrow + 
                            startrow + '• Email address'  + separator + clientBrokerAdminEmail + endrow +
                            '</table>' + '<br/>' +
                            
                            contactMe_loggedInUser + ' provided the following additional information when he/she made the request.' + '<br/><br/>' + 
                            
                            '<table style="margin: 0 0 0 20px;">' + 
                            areaInfo      +
                            clientInfo    +
                            coverInfo     +
                            claimTypeInfo +
                            subjectInfo   +
                            '</table>' +
                            
                            '<br/>' + 
                            
                            'Contact Method Preference:' + '<br/>' +
                            //'<table style="font-family:Calibri">' + 
                            '<table style="margin: 0 0 0 20px;">' + 
                            startrow + '• By phone'  + separator + str_contactMe_phonePreference + endrow  +
                            startrow + '• By e-mail' + separator + str_contactMe_emailPreference + endrow  + 
                            '</table>' + '<br/>' + 
                            
                            mailSignature + '<br/><br/>' +
                            footer;
        }
        else if(emailAction == 'registerclaim_copytoRequestor'){
            
            System.debug('registerClaim_claimDescription -- previous --' +registerClaim_claimDescription );
            registerClaim_claimDescription = registerClaim_claimDescription.replace('\n', '<br/>');
            System.debug('registerClaim_claimDescription -- post --' +registerClaim_claimDescription );
            
            //MYG-1282
            //System.debug('copytoRequestor Previous registerClaim_eventDate -->' + registerClaim_eventDate);
            //System.debug('copytoRequestor Previous registerClaim_dateOfEmbarkation -->' + registerClaim_dateOfEmbarkation);
            
            String e_date = convertDateToText(registerClaim_eventDate);
            String e_doe = '';
            if(registerClaim_dateOfEmbarkation != null && registerClaim_dateOfEmbarkation != ''){
                e_doe = convertDateToText(registerClaim_dateOfEmbarkation);
            }
            
            //System.debug('copytoRequestor Post registerClaim_eventDate -->' + registerClaim_eventDate);
            //System.debug('copytoRequestor Post registerClaim_dateOfEmbarkation -->' + registerClaim_dateOfEmbarkation);
            
            mail_subject = 'Your Claim has been registered successfully';
            emailPlainTextBody = '';
            emailHTMLBody = logo + '<br/><br/>' + 
                            'Dear ' + clientBrokerOldAdminName + ',' + '<br/><br/>' + 
                            'Thank you for contacting Gard. Your request will be processed Gard shortly.' +  '<br/><br/>' + 
                            'Below are the details submitted by you on the MyGard portal.' + '<br/><br/>' + 
                            
                            //'<table style="font-family:Calibri">' + 
                            '<table style="margin: 0 0 0 20px;">' + 
                            //startrow + 'MyGard claim id'  + separator + '<b>' + registerClaim_myClaimId  + '</b>'         + endrow +
                            startrow + 'Reported by'      + separator         + clientBrokerOldAdminName                  + endrow + 
                            startrow + 'User type'        + separator         + registerClaim_userType                    + endrow +   
                            startrow + 'Client name'      + separator         + registerClaim_clientName                  + endrow +
                            startrow + 'Reference number' + separator         + registerClaim_referencenumber             + endrow +   
                            startrow + 'Event date'       + separator         + e_date                                    + endrow +
                            startrow + 'Cover name'       + separator         + registerClaim_coverName                   + endrow +
                            startrow + 'Object name'      + separator         + registerClaim_objectName.toUpperCase()    + endrow +
                            startrow + 'Claim type'       + separator         + registerClaim_claimType                   + endrow +
                            startrow + 'Place'            + separator         + registerClaim_place                       + endrow ;
                            
            if(registerClaim_claimType != null && registerClaim_claimType != ''){
                if(registerClaim_claimType.equalsIgnoreCase('Crew') || registerClaim_claimType.equalsIgnoreCase('Passenger, Person(s) other than crew') ||  registerClaim_claimType.equalsIgnoreCase('passenger')){
                emailHTMLBody = emailHTMLBody + 
                                startrow + 'Name'             + separator + registerClaim_name            + endrow +
                                startrow + 'Rank'             + separator + registerClaim_rank            + endrow + 
                                startrow + 'Nationality'      + separator + registerClaim_nationality     + endrow +
                                startrow + 'Claim detail'     + separator + registerClaim_claimDetail     + endrow ;
                }
                if(registerClaim_claimType.equalsIgnoreCase('Stowaways')){
                emailHTMLBody = emailHTMLBody + 
                                startrow + 'Port of embarkation'    + separator + registerClaim_portOfEmbarkation     + endrow +
                                startrow + 'Date of embarkation'    + separator + e_doe                               + endrow + 
                                startrow + 'Number of stowaways'    + separator + registerClaim_numberofStowaways     + endrow ;
                }
            }               
            emailHTMLBody = emailHTMLBody + 
                            '<tr><td style="vertical-align: top; font-family: verdana;">' + 'Brief description' + '</td><td style="vertical-align: top; font-family: verdana;">:</td><td style="vertical-align: top; font-family: verdana;">' + registerClaim_claimDescription + endrow +
                            '</table>';
            emailHTMLBody = emailHTMLBody + '<br/>' + mailSignature + '<br/><br/>' + footer;
        }
                    
        else if(emailAction == 'registerclaim_copytoClaimHandler'){
           
            System.debug('registerClaim_claimDescription -- previous --' +registerClaim_claimDescription );
            registerClaim_claimDescription = registerClaim_claimDescription.replace('\n', '<br/>');
            System.debug('registerClaim_claimDescription -- post --' +registerClaim_claimDescription );
            
            //MYG-1282
            //System.debug('copytoClaimHandler Previous registerClaim_eventDate -->' + registerClaim_eventDate);
            //System.debug('copytoClaimHandler Previous registerClaim_dateOfEmbarkation -->' + registerClaim_dateOfEmbarkation);
            
            String e_date = convertDateToText(registerClaim_eventDate);
            String e_doe = '';
            if(registerClaim_dateOfEmbarkation != null && registerClaim_dateOfEmbarkation != ''){
                e_doe = convertDateToText(registerClaim_dateOfEmbarkation);
            }
            
            //System.debug('copytoClaimHandler Post registerClaim_eventDate -->' + registerClaim_eventDate);
            //System.debug('copytoClaimHandler Post registerClaim_dateOfEmbarkation -->' + registerClaim_dateOfEmbarkation);
            
               
            
            mail_subject = 'A Claim has been assigned to you';
            emailPlainTextBody = '';
            emailHTMLBody = logo + '<br/><br/>' + 
                            'Dear Sirs, '+'<br/><br/>' +
                            //'Hi ' + clientBrokerOldAdminName + ',' + '<br/><br/>' + 
                            'A new claim has been registered via the MyGard portal. The details of the claim are: ' + '<br/><br/>' + 
                            
                            //'<table style="font-family:Calibri">' + 
                            '<table style="margin: 0 0 0 20px;">' + 
                            startrow + 'MyGard claim id'  + separator + '<b>' + registerClaim_myClaimId  + '</b>'         + endrow +
                            startrow + 'Reported by'      + separator         + clientBrokerOldAdminName                  + endrow + 
                            startrow + 'User type'        + separator         + registerClaim_userType                    + endrow +   
                            startrow + 'Client name'      + separator         + registerClaim_clientName                  + endrow +
                            startrow + 'Reference number' + separator         + registerClaim_referencenumber             + endrow +   
                            startrow + 'Event date'       + separator         + e_date                                    + endrow +
                            startrow + 'Cover name'       + separator         + registerClaim_coverName                   + endrow +
                            startrow + 'Object name'      + separator         + registerClaim_objectName.toUpperCase()    + endrow +
                            startrow + 'Claim type'       + separator         + registerClaim_claimType                   + endrow +
                            startrow + 'Place'            + separator         + registerClaim_place                       + endrow ;
                            
            if(registerClaim_claimType != null && registerClaim_claimType != ''){
                if(registerClaim_claimType.equalsIgnoreCase('Crew') || registerClaim_claimType.equalsIgnoreCase('Passenger, Person(s) other than crew') ||  registerClaim_claimType.equalsIgnoreCase('passenger')){
                emailHTMLBody = emailHTMLBody + 
                                startrow + 'Name'             + separator + registerClaim_name            + endrow +
                                startrow + 'Rank'             + separator + registerClaim_rank            + endrow + 
                                startrow + 'Nationality'      + separator + registerClaim_nationality     + endrow +
                                startrow + 'Claim detail'     + separator + registerClaim_claimDetail     + endrow ;
                }
                if(registerClaim_claimType.equalsIgnoreCase('Stowaways')){
                emailHTMLBody = emailHTMLBody + 
                                startrow + 'Port of embarkation'    + separator + registerClaim_portOfEmbarkation     + endrow +
                                startrow + 'Date of embarkation'    + separator + e_doe                               + endrow + 
                                startrow + 'Number of stowaways'    + separator + registerClaim_numberofStowaways     + endrow ;
                }                
            }
            emailHTMLBody = emailHTMLBody + 
                            '<tr><td style="vertical-align: top; font-family: verdana;">' + 'Brief description' + '</td><td style="vertical-align: top; font-family: verdana;">:</td><td style="vertical-align: top; font-family: verdana;">' + registerClaim_claimDescription + endrow +
                            '</table>' + '<br/>';
            
            
                           
            emailHTMLBody = emailHTMLBody +  
                            clientBrokerOldAdminName + ' can be reached on:' + '<br/><br/>' + 
                            
                            //'<table style="font-family:Calibri">' + 
                            '<table style="margin: 0 0 0 20px;">' + 
                            startrow + '• Phone'         + separator + clientBrokerAdminPhone  + endrow + 
                            startrow + '• Mobile'        + separator + clientBrokerAdminMobile + endrow + 
                            startrow + '• Email address' + separator + clientBrokerAdminEmail  + endrow + 
                            '</table>' + 
                            '<br/>' +
                            
                            'If you find that the claim submitted is not valid, please forward this email to clarice.tan@gard.no stating that the claim is rejected.' + '<br/><br/>' + 
                            mailSignature + '<br/><br/>' + footer;
        }
        
        else if(emailAction == 'editObject'){
            mail_subject  = 'Request for Edit Object Details';
            emailHTMLBody = '<br/><br/>' + 
                            'Dear Sirs,' + '<br/><br/>' + 
                            editObject_emailBody + '<br/><br/>' + 
                            mailSignature + '<br/><br/>' + footer;
        }
        
        System.debug('emailHTMLBody-->' + emailHTMLBody);
        emailHTMLBody = '<font face="verdana" color="black">' + emailHTMLBody + '</font>';
        
        mail.setSubject(mail_subject);
        mail.setPlainTextBody(emailPlainTextBody);
        mail.setHtmlBody(emailHTMLBody);
        if(attachmentlist != null && attachmentlist.size() > 0){
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
            for(attachment a : attachmentlist){
                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                efa.setFileName(a.Name);
                efa.setBody(a.Body);
                fileAttachments.add(efa);
            }
            mail.setFileAttachments(fileAttachments);
        }
        
        
        //Set Organization Wide email address from Custom Settings
        if(emailAction == 'registerclaim_copytoRequestor' || emailAction == 'registerclaim_copytoClaimHandler'){
            mail.setOrgWideEmailAddressId(Organization_Wide_Email__c.getInstance('claim@gard.no').value__c);
        }
        else{
            mail.setOrgWideEmailAddressId(Organization_Wide_Email__c.getInstance('no-reply@gard.no').value__c);
        }
        
        System.debug('emailAction is-->' + emailAction);
        System.debug('mail is-->' + mail);
        return mail;
    }
    
    //MYG-1282
    public String convertDateToText(String event_date){
        System.debug('within convert method-->' + event_date);
        String[] eventArray = event_date.split('/');
        System.debug('within convert method eventArray -->' + eventArray );
        
        String tempdate = eventArray[0];
        String month = eventArray[1];
        String year = eventArray[2];
        
        if (tempdate.startsWith('0')) tempdate = tempdate.substring(1);    //to convert 01/01/2014 to 1/1/2014
        
             if(month == '01') month = 'January';
        else if(month == '02') month = 'February';
        else if(month == '03') month = 'March';
        else if(month == '04') month = 'April';
        else if(month == '05') month = 'May';
        else if(month == '06') month = 'June';
        else if(month == '07') month = 'July';
        else if(month == '08') month = 'August';
        else if(month == '09') month = 'September';
        else if(month == '10') month = 'October';
        else if(month == '11') month = 'November';
        else if(month == '12') month = 'December';
        
        String converteddate = tempdate + ' ' + month + ' ' + year;
        return converteddate;
    }
    */
}