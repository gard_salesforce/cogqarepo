public class ViewPdf {
    
    public String pdf{get;set;}
    public String mimetype{get;set;}
    public attachment att{get;set;}
    
    
    public Pagereference fetchDoc(){
        gardNoDm_v1.GardDocumentServicePort test = new gardNoDm_v1.GardDocumentServicePort();
        pdf = test.getDocumentWithExternalGroup('OPEN','2540','7414638');
        Blob b = EncodingUtil.base64Decode(pdf);
        att = new Attachment();
        att.Body = b; //b.toPDF()
        att.Name = 'xyz test';
        att.contentType = 'application/pdf';
        att.parentId = '003D000000iYrnt'; //UserInfo.getUserId();//'001L000000f8LaC';        
        insert att;
        return new Pagereference('/servlet/servlet.FileDownload?file=' + att.id);
    }
}