public without sharing class RequestPageCtrl{

public String selectedRequest{ get; set; }
public String selectPopUp{ get; set;}
public String prvUrl{ get; set;}
public String isObjectDetailsMode{ get; set;}
List<Account_Contact_Mapping__c> acmJuncObjLst; //MYG-1829
public boolean marine{get;set;}
public boolean PI{get;set;}
public string loggedInContactId;
public string loggedInAccountId;
public boolean avialibility{get;set;} //MYGP-104
public boolean MLC_avialibility{get;set;}
public boolean isClinicUser{get;set;}
public Boolean isObjectView; //SF-4038
public String requestedPageName{get;set;} //SF-4038
    
    public RequestPageCtrl(){
        System.debug('RequestPageCtrl.Constructor STARTS');
        Marine = false;
        PI = false;
        isClinicUser = false;
        setObjectView();
        requestedPageName = 'BlankPage';
        System.debug('RequestPageCtrl.Constructor ENDS');
    }
    
    //SF-4038
    public void setObjectView(){
       system.debug('---in setObjectView prvurl---->'+prvUrl);
        //if(isObjectView != null) return;
        if(prvUrl != null && prvUrl.contains('ObjectDetail')){
           //system.debug('---entered---->'+prvUrl);
            isObjectView = true;
        }else{
            isObjectView = false;
        }
    }
    
   
public void requestChnage(){
    setObjectView();//Newly Added moved here by Pulkit
    User usr = [Select Id, contactID,contact.AccountId from User Where Id =: UserInfo.getUserId()];
    loggedInContactId = usr.Contactid;
    //added for multi company
    AccountToContactMap__c acmCS = AccountToContactMap__c.getValues(loggedInContactId);
     //   system.debug('****************** acmCS : '+acmCS);
        if(acmCS!=null && acmCS.name!=null && acmCS.name!='')
        {
       //     system.debug('****************** acmCS : '+acmCS);
            loggedInAccountId = acmCS.AccountId__c;
        }
    //loggedInAccountId = usr.contact.accountId;
    acmJuncObjLst = [Select PI_access__c, Marine_access__c, id from Account_Contact_Mapping__c where Contact__c =:loggedInContactId AND Account__c =:loggedInAccountId];
    System.debug('---acmJuncObjLst---------->'+acmJuncObjLst);
    if(acmJuncObjLst != null && acmJuncObjLst.size() >0 && acmJuncObjLst[0].PI_access__c)
        PI = true;
    if(acmJuncObjLst != null && acmJuncObjLst.size() >0 && acmJuncObjLst[0].Marine_access__c)
        marine = true;
    System.debug('Maeinr -->'+marine +' P&I -->'+PI+'URL---'+prvUrl);
    if(prvUrl != null && prvUrl.contains('ObjectDetail')){
        isObjectDetailsMode = 'objectView';
        //setObjectView();//Newly Added
    }
    else{
        isObjectDetailsMode = 'normalView';
    }
    system.debug('######### prvUrl ->'+prvUrl); 
    system.debug('######### selectedRequest ->'+selectedRequest);
    if((selectedRequest != null) || (selectedRequest != 'Select request forms')){   
        system.debug('######### selectedRequest ->'+selectedRequest);     
        if(selectedRequest.contains('change to terms') || selectedRequest.contains('additional covers for existing objects') || selectedRequest.contains('covers for new objects') || selectedRequest.contains('change to assureds') || selectedRequest.contains('change to mortgagees') || selectedRequest.contains('miscellaneous changes') ){
            selectPopUp = 'requestChangeType';
            system.debug('---isObjectView---->'+isObjectView);
            if(isObjectView){
                    requestedPageName = 'RequestChangeObjectdetails';
                    RequestChangeCtrl.transferField = selectedRequest;
                }else{
                    requestedPageName = 'RequestChange';
                    RequestChangeCtrl.transferField = selectedRequest;
                }
        }
        if( selectedRequest.contains('Request change to the Blue Card')){
            selectPopUp = 'BlueCardProcessType';  
            requestedPageName = 'BlueCardRequest';
         } 
         if(selectedRequest.contains('Request new Blue Card')){
             selectPopUp = 'NewBlueCardProcessType';
             requestedPageName = 'RequestNewBlueCard';
         }
         if( selectedRequest.contains('renewal of Blue Card')){
            selectPopUp = 'BlueCardRenewProcessType'; 
            requestedPageName = 'BlueCardRenew'; 
         }         
         if(selectedRequest == 'Request for LOC') {
             selectPopUp = 'LOC'; 
             requestedPageName = 'ContractReviewLOCRequest';
         } 
         //for MYG - 2756 
         if(selectedRequest == 'Request for COFR letter') {
             selectPopUp = 'COFR_Rquest';
             requestedPageName = 'COFR_Rquest'; 
         } 
         if(selectedRequest == 'Request new MLC certificate') 
         {
             selectPopUp = 'MLC_Certificate';
             requestedPageName = 'RequestMLCCertificate';
         } 
         if(selectedRequest == 'Request change to MLC Certificate')      
         {      
             selectPopUp = 'MLC_Certificate_change';
             requestedPageName = 'RequestMLCCertificate';
         } 
        
            System.debug('selectPopUp ->'+selectPopUp);
            System.debug('requestedPageName ->'+requestedPageName); 
    }
}
    public void chkProdArea()
    {
        System.debug('RequestPageCtrl.chkProdArea STARTS');
        //MYGP-104 for blue card renewal
        Blue_Card_renewal_availability__c CSObj = Blue_Card_renewal_availability__c.getInstance('value'); 
        if(CSObj != null)
            avialibility = CSObj.BlueCardRenewalAvailability__c;
        
        //SF-3703 for MLC Certificate
        MLC_Certificate_availability__c MLC_Certificate_CS_Obj = MLC_Certificate_availability__c.getInstance('MLC availability');
        if(MLC_Certificate_CS_Obj != null)
            MLC_avialibility = MLC_Certificate_CS_Obj.value__c;
        //MYG-1829
        User usr = [Select Id, contactID,contact.AccountId from User Where Id =: UserInfo.getUserId()];
        loggedInContactId = usr.Contactid;
        //Added for SF-4282
        List<Account> acc=[select id,Company_Role__c from Account where Company_Role__c = 'External Service Provider' and id=:loggedInAccountId];
        if(acc.size()>0 || acc.size()!=null)
        {
            isClinicUser = false;
        }
        else{isClinicUser = true;}
        system.debug('the value of the bnoolean varuiavble usertype is****'+isClinicUser);
        //added for multi company
        AccountToContactMap__c acmCS = AccountToContactMap__c.getValues(loggedInContactId);
        system.debug('****************** acmCS : '+acmCS);
        if(acmCS!=null && acmCS.name!=null && acmCS.name!='')
        {
            system.debug('****************** acmCS : '+acmCS);
            loggedInAccountId = acmCS.AccountId__c;
        }
        
        System.debug('---loggedInContactId ------loggedInAccountId---->'+loggedInContactId +' '+loggedInAccountId);
        acmJuncObjLst = [Select PI_access__c, Marine_access__c, id from Account_Contact_Mapping__c where Contact__c =:loggedInContactId AND Account__c =:loggedInAccountId];
        System.debug('---acmJuncObjLst---------->'+acmJuncObjLst);
        if(acmJuncObjLst != null && acmJuncObjLst.size() >0 && acmJuncObjLst[0].PI_access__c)
            PI = true;
        if(acmJuncObjLst != null && acmJuncObjLst.size() >0 && acmJuncObjLst[0].Marine_access__c)
            marine = true;
        System.debug('Maeinr -->'+marine +' P&I -->'+PI);
        System.debug('RequestPageCtrl.chkProdArea ENDS');
    }
    public Pagereference redirectToLoc()
    {
        return page.ContractReviewLOCRequest;
    }
}