//Created by Pulkit for SF-3904
//due to the sandwich problem while activating a deActivated acm, the user is activated in first job and the acm is activated in the second
public class UserManagementProcesses implements Queueable{
    private Integer processId;
    private Id contactId;
    private Id accountId;
    @TestVisible private static Boolean letChain = true;
    
    
    //Job initiating Constructor
    public UserManagementProcesses(Id contactId,Id accountId){
        processId = 1;
        this.contactId = contactId;
        this.accountId = accountId;
    }
    //job propagating constructor
    @TestVisible private UserManagementProcesses(Id contactId,Id accountId,Integer processId){
        this.processId = processId;
        this.contactId = contactId;
        this.accountId = accountId;
    }
    
    public void execute(QueueableContext qc){
        switch on processId{
            when 1{//activates a user of this contact and call the job to activate the acm
                activateUser(contactId);
                if(letChain)System.enqueueJob(new UserManagementProcesses(contactId,accountId,processId+1));
            }when 2{//activates the acm for this contact
                activateAcm(contactId,accountId);
            }
        }
    }
    
    //activate related User
    private static void activateUser(Id contactId){
        System.debug('UserManagementProcesses.activateUser() parameters recieved - '+contactId);
        User userToBeActivated = [SELECT IsActive FROM User WHERE contactId = :contactId];
        if(!userToBeActivated.IsActive || Test.isRunningTest()){
        	userToBeActivated.IsActive = true;
        	update userToBeActivated;
        }
    }
    
    //activate related ACM
    @TestVisible //Because running this method requires chaining of Jobs which is not possible
    private static void activateAcm(Id contactId,Id accountId){
        System.debug('UserManagementProcesses.activateAcm() parameters recieved - '+contactId+'/'+accountId);
        Account_Contact_Mapping__c acmToBeActivated = [SELECT Id,contact__c,account__c,Active__c FROM Account_Contact_Mapping__c WHERE contact__c = :contactId AND account__c = :accountId];
        if(!acmToBeActivated.Active__c || Test.isRunningTest()){
            acmToBeActivated.Active__c = true;
        	update acmToBeActivated;
        }
    }

}