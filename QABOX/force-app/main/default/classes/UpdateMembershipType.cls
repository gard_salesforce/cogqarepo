public class UpdateMembershipType{ 
   public void doOperation(){
        List<String> statusList= new List<String>{'Declined','Approved','Ready for Approval'};
        List<String> typeList= new List<String>{'Prospect','Customer','Non Prospect'};  
        //also run the query for PI_member_review_count__c > 1      
        List<Account> compList = [Select id, name, Membership_Type__c,P_I_Member_Flag__c from Account 
                                  where Membership_Type__c = '' and Membership_Status__c in: statusList
                                  and Type in: typeList and PI_member_review_count__c = 1 limit 180];
        /* 
        // for approved membership date not updated
        List<Account> compList = [Select id, name, Membership_Type__c,Membership_Approved_Date__c,P_I_Member_Flag__c from Account 
                          where Membership_Type__c = '' and Membership_Approved_Date__c = null and Membership_Status__c in: statusList
                          and Type in: typeList and PI_member_review_count__c >= 1];
        
        List<Company_Membership_Review__c> cpList = [Select ID, status__c , Approval_type_required__c,Company_Name__c ,createdDate
                                                from Company_Membership_Review__c where status__c ='Approved'
                                                and Company_Name__c in: compList 
                                                and Approval_type_required__c !=''];
        
        */
        List<Company_Membership_Review__c> cpList = [Select ID, status__c , Approval_type_required__c,Company_Name__c 
                                                        from Company_Membership_Review__c where status__c in: statusList 
                                                        and Company_Name__c in: compList 
                                                        and Approval_type_required__c !=''];  
        //Membership_Type__c = '' and id in: IDList
        system.debug('list size 1: '+compList.size());
        system.debug('list size 2: '+cpList.size());
        Map<String,List<Company_Membership_Review__c>> multiPICompList= new Map<string,List<Company_Membership_Review__c>>();
        List<Company_Membership_Review__c> piList;
        
        for(Account acc : compList){
            for(Company_Membership_Review__c cp : cpList){
                if(cp.Company_Name__c == acc.id){
                    piList = new List<Company_Membership_Review__c>();
                    piList.add(cp);
                    List<Company_Membership_Review__c> existingList = multiPICompList.get(cp.Company_Name__c);
                    if(existingList != null && existingList.size()!=0) {
                        piList.addAll(existingList);
                    }
                    multiPICompList.put(cp.Company_Name__c , piList);
                }
            }
        }
        system.debug('list size 3: '+multiPICompList.size());
        List<Account> finalAccList = new List<Account>();
        Account accFinal;
        Map<String,List<Account>> finalAccountMap = new Map<String,List<Account>>();
        for(Account acc : compList){
          for(String s : multiPICompList.keySet())
                {   
                     for(Company_Membership_Review__c a :multiPICompList.get(s)){
                            System.debug('checking the id = '+ a.ID + ' checking the comp = '+ s);
                            accFinal = new Account();
                         if(acc.id == s){
                             System.debug('checking the id of company = '+ s);
                             List<Account> newList = finalAccountMap.get(String.valueOf(acc.id));
                             if(newlist==null || newList.size()==0){
                                 accFinal.id = acc.id;
                                 accFinal.Membership_Type__c = a.Approval_type_required__c;
                                 //for records with date not updated
                                 //accFinal.Membership_Approved_Date__c = a.createdDate.date();
                                 
                                 //for records with Membership_Status__c not corectly updated
                                 //accFinal.Membership_Status__c = a.status__c;
                                 finalAccList.add(accFinal);
                                 finalAccountMap.put(String.valueOf(acc.id),finalAccList);
                             }
                         }
                     }
                }
        }
        
        
        if(finalAccList.size() > 0){
            System.debug('finally successfullll '+finalAccList.size());
            List<Database.UpsertResult> updatedAcc = Database.upsert(finalAccList,false);
   
            for (Database.UpsertResult sr : updatedAcc){
              System.debug('error size: '+sr.getErrors().size());
              //---------->size is 2
              for (Database.Error err : sr.getErrors()){
                System.debug('error = ' + err);
                //----------each error object output to log
              }
            }
         }
     }       
}