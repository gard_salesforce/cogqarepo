global class BatchSendClaimsSurveyNotification implements Database.batchable <sObject>{
    
	global List<Id> userList = new List<Id>();
    global Id feedback;
    public String jobName;
    public BatchSendClaimsSurveyNotification(List<Id> userList,Id feedback,String jobName){
        this.userList = userList;
        this.feedback = feedback;
        this.jobName = jobName;
    }
    global Database.QueryLocator start(Database.BatchableContext bc){
        system.debug('--Send claims customer feedback batch fired--');
        String soqlQuery = 'SELECT email,UserRole.ParentRoleId FROM User WHERE Id IN: userList';
        system.debug('soqlQuery MFiles is: '+soqlQuery);
        return Database.getQueryLocator(soqlQuery);

    }
    
    global void execute(Database.BatchableContext bc, List<User> scope){/*
        set<String> NotifcationToAddress = new set<String>();
        set<Id> parentAndCXRoleIds = new set<Id>();
        for(User usr:scope){
            NotifcationToAddress.add(usr.email);
            parentAndCXRoleIds.add(usr.UserRole.ParentRoleId);
        }
        parentAndCXRoleIds.add(Label.Customer_Experience_Role_Id);
        for(User mgrUsr:[SELECT email FROM User WHERE UserRoleId IN: parentAndCXRoleIds]){
            NotifcationToAddress.add(mgrUsr.email);
        }
        integer count = 1;
        for(String emailIds:NotifcationToAddress){
        	system.debug('NotifcationToAddress--'+count+':'+emailIds);
            count++;
        }
        system.debug('feedback--'+feedback);
        //Create email
        String emailBody = '';
        String mail_Subject = '';
        List<EmailTemplate> notifyUsers = new List<EmailTemplate>([SELECT Id, body, subject FROM EmailTemplate WHERE Id =: Label.Claim_customer_feedback_email_template_id]);
        emailBody = notifyUsers[0].body;
        system.debug('feedback.Name+feedback.Title__c--'+feedback.Name+' '+feedback.Title__c);
        mail_Subject = notifyUsers[0].subject.replace('{!Customer_Feedback__c.Name} {!Customer_Feedback__c.Title__c}',feedback.Name+' '+feedback.Title__c);
        emailBody = emailBody.replace('{!Customer_Feedback__c.Name} {!Customer_Feedback__c.Title__c}',feedback.Name+' '+feedback.Title__c);
        
        system.debug('feedback.Customer_feedback_type__c--'+feedback.Customer_feedback_type__c);
        emailBody = emailBody.replace('{!Customer_Feedback__c.Customer_feedback_type__c}',feedback.Customer_feedback_type__c);
        
        system.debug('Customer_Feedback__c.Responsible__c--'+feedback.Responsible__c);
        emailBody = emailBody.replace('{!Customer_Feedback__c.Responsible__c}',feedback.Responsible__r.name);
        
        system.debug('feedback.Type_of_feedback__c--'+feedback.Type_of_feedback__c);
        emailBody = emailBody.replace('{!Customer_Feedback__c.Type_of_feedback__c}',feedback.Type_of_feedback__c);
        
        system.debug('feedback.From_Company__c--'+feedback.From_Company__c);
        emailBody = emailBody.replace('{!Customer_Feedback__c.From_Company__c}',feedback.From_Company__c);
        
        system.debug('feedback.Company_to__c--'+feedback.Company_to__c);
        emailBody = emailBody.replace('{!Customer_Feedback__c.Company_to__c}',feedback.Company_to__c);
        
        system.debug('feedback.Id--'+feedback.Id);
        emailBody = emailBody.replace('{!Customer_Feedback__c.Link}',Label.InternalLightningOrgURL.replace('Case','Customer_Feedback__c')+feedback.Id+'/view');
        system.debug('emailBody--'+emailBody);
        Messaging.SingleEmailMessage notifMail = new Messaging.SingleEmailMessage(); 
        notifMail.setToAddresses(new List<String>(NotifcationToAddress));
        notifMail.setReplyTo(CommonVariables__c.getInstance('ReplyToMailId').Value__c);
        notifMail.setPlainTextBody(emailBody);
        notifMail.setSubject(mail_Subject);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {notifMail});  */
    }
    global void finish(Database.BatchableContext bc){
        //Abort job SF-6601 added
        /*
        for(CronTrigger ct: [SELECT Id, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType, CreatedDate FROM CronTrigger
                             Where CronJobDetail.Name =: jobName]){
                                 System.abortJob(ct.Id);
                             }
*/
    }
    
}