@isTest
public class TestUpdateContentDocumentLink{
public static Group g1;
public static QueuesObject q1;
 
 public static void testData(){
            g1 = new Group(Name='group name', type='Queue');
            insert g1;
            q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
            insert q1;
            }
            
static testMethod void testcontentDocLink(){
//testData();
String customerTransactionString = 'Customer Transactions';
Id customerTransactionsCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(customerTransactionString).getRecordTypeId();
 
//String qId = string.valueof(q1.id);
QueueInEmail__c qe = new QueueInEmail__c(name='Marine Support North', Email__c = 'testcc@test.com', Team__c='CT North', Id__c='00G3z000005Oa92');
insert qe;   
          
Case c = new Case();
c.status='Closed';
//c.RecordTypeId = customerTransactionsCaseRecordTypeId;
//c.RecordType.name='Customer Transactions';
insert c;

List<EmailMessage> emailMessages = new List<EmailMessage>();
EmailMessage incomingEmailMessage = new EmailMessage(
                    FromAddress = 'test@test.com',
                    ToAddress = 'test@test.com',
                    Subject = 'ref:_test Subject',
                    CcAddress ='testcc@test.com',
                    TextBody = 'test TextBody',
                    ParentId = c.Id,
                    Incoming = TRUE
            );
emailMessages.add(incomingEmailMessage);
insert emailMessages;
            

//Matter__c thisMatter = [select id,Request__r.Requestor__r.Id from matter__c  limit 1];
   Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
    ContentVersion contentVersion_1 = new ContentVersion(
        Title='Header_Picture1', 
        PathOnClient ='/Header_Picture1.jpg',
        VersionData = bodyBlob, 
        origin = 'H'
    );
    insert contentVersion_1;
   
    ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
    List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
    
    List<ContentDocumentLink> cdList = new List<ContentDocumentLink>();
    ContentDocumentLink contentlink = new ContentDocumentLink();
    contentlink.LinkedEntityId = c.id;
    contentlink.contentdocumentid = contentVersion_2.contentdocumentid;
    contentlink.ShareType = 'V';
    contentlink.Visibility='AllUsers';
    //insert contentlink; 
    cdList.add(contentlink);
    
    ContentDocumentLink contentlink1 = new ContentDocumentLink();
    contentlink1.LinkedEntityId = emailMessages[0].id;
    contentlink1.contentdocumentid = contentVersion_2.contentdocumentid;
    contentlink1.ShareType = 'V';
    contentlink1.Visibility='AllUsers';
    
    cdList.add(contentlink1);
    insert cdList;

}

}