/**************************************************************************
* Author  : Phil Spenceley
* Company : cDecisions
* Date    : 26/07/2013
***************************************************************************/
    
/// <summary>
///  This class performs basic XmlSerialization of standard and custom sObjects and objects which implement the XmlSerializable interface.
/// </summary>
public without sharing class XmlSerializer {
    
/// -----------------------------------------------------------------------
///  -- Serialize objects (which implement the XMLSerializable interface) 
/// -----------------------------------------------------------------------
	
    /// <summary>
    ///  This method serializes a custom object that implements the XmlSerializable class
    /// </summary>
    /// <param name="obj"> The object (implements XmlSerializable) that needs to be serialized to Xml </param>
    /// <param name="objectType"> The System.type of the object (use <c>ClassName.class</c> to obtain the type) </param>
    public DOM.Document Serialize(XmlSerializable obj, System.Type objectType) {
        return Serialize(obj, objectType, new List<String>(obj.getFields()), false); 
    }
    
    /// <summary>
    ///  This method serializes a custom object that implements the XmlSerializable class
    /// </summary>
    /// <param name="obj"> The object (implements XmlSerializable) that needs to be serialized to Xml </param>
    /// <param name="objectType"> The System.type of the object (use <c>ClassName.class</c> to obtain the type) </param>
    /// <param name="failOnFirstException"> Boolean, if false, exceptions when setting XmlNodes based on fields will be ignored </param>
    public DOM.Document Serialize(XmlSerializable obj, System.Type objectType, List<String> fields, Boolean failOnFirstException) {
        DOM.Document xmlDoc = new DOM.Document(); 
        DOM.XMLNode root = xmlDoc.createRootElement(objectType.getName(), null, null);
        root.setNamespace(XmlUtils.XSI_PREFIX, XmlUtils.XSI_NS);
		
        fields.sort();
        
        for(string s : fields) {
            try {
                string innerText = '';
                boolean isNull = false;
                if (obj.get(s) != null) {
                    object value = obj.get(s);
						
					if (value instanceOf Date) {
			            innerText = Datetime.newInstance(((Date)value).year(), ((Date)value).month(), ((Date)value).day()).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
			        } else if (value instanceOf Datetime){
			            innerText = ((Datetime)value).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
			        } else {
			            innerText = string.valueof(value);
			        }
                } else {
                    isNull = true;
                }
                if (isNull) {
                    DOM.XmlNode child = root.addChildElement(s, null, null);
					child.setAttributeNs('nil', 'true', XmlUtils.XSI_NS, null);
                } else {
                    root.addChildElement(s, null, null).addTextNode(innerText);    
                }
            } catch (Exception ex) {
                if (failOnFirstException) {
                    throw ex;
                }
            }
        }
        
        System.debug('XmlSerializer.Serialize: ' + xmlDoc.toXmlString());
        
        return xmlDoc;
    }

/// -----------------------------------------------------------------------
///  -- Serialize sObjects
/// -----------------------------------------------------------------------  

    /// <summary>
    ///  This method serializes an sObject to Xml
    /// </summary>
    /// <param name="obj"> The sObject that needs to be serialized to Xml </param>
    public DOM.Document Serialize(sObject obj) {
        List<String> fields = new List<String>{};
        fields.addAll(obj.getsObjectType().getDescribe().fields.getMap().keySet());
        
        return Serialize(obj, fields, false);
    }
    
    /// <summary>
    ///  This method serializes an sObject to Xml
    /// </summary>
    /// <param name="obj"> The sObject that needs to be serialized to Xml </param>
    /// <param name="outputAllFields"> Determines whether all fields should be serialized into Xml or only fields previously selected by SOQL (fields not selected by SOQL will be NULL) </param>
    public DOM.Document Serialize(sObject obj, Boolean outputAllFields) {
        List<String> fields = new List<String>{};
        fields.addAll(obj.getsObjectType().getDescribe().fields.getMap().keySet());
        
        return Serialize(obj, fields, false, outputAllFields);
    }
    
    /// <summary>
    ///  This method serializes an sObject to Xml
    /// </summary>
    /// <param name="obj"> The sObject that needs to be serialized to Xml </param>
    /// <param name="fields"> A list of field names which should be serialized to Xml </param>
    public DOM.Document Serialize(sObject obj, List<String> fields) {
        List<String> objectFields = new List<String>{};
        for (String s : obj.getsObjectType().getDescribe().fields.getMap().keySet()) {
            for (string f : fields) {
                if (s.toLowerCase() == f.toLowerCase()) {
                    objectFields.add(f);
                }
            }
        }
        
        return Serialize(obj, objectFields, true);
    }
    
    /// <summary>
    ///  This method serializes an sObject to Xml
    /// </summary>
    /// <param name="obj"> The sObject that needs to be serialized to Xml </param>
    /// <param name="fields"> A list of field names which should be serialized to Xml </param>
    /// <param name="failOnFirstException"> Boolean, if false, exceptions when setting XmlNodes based on fields will be ignored </param>
    public DOM.Document Serialize(sObject obj, List<String> fields, Boolean failOnFirstException) {
        return Serialize(obj, fields, failOnFirstException, false);
    }
    
    /// <summary>
    ///  This method serializes an sObject to Xml
    /// </summary>
    /// <param name="obj"> The sObject that needs to be serialized to Xml </param>
    /// <param name="fields"> A list of field names which should be serialized to Xml </param>
    /// <param name="failOnFirstException"> Boolean, if false, exceptions when setting XmlNodes based on fields will be ignored </param>
    /// <param name="outputAllFields"> Determines whether all fields should be serialized into Xml or only fields previously selected by SOQL (fields not selected by SOQL will be NULL) </param>
    public DOM.Document Serialize(sObject obj, List<String> fields, Boolean failOnFirstException, Boolean outputAllFields) {
        DOM.Document xmlDoc = new DOM.Document(); 
        string rootName = obj.getsObjectType().getDescribe().getName();
        if(rootName == 'Account')
        	rootName = 'Account'; //change this to Partner when possible
        DOM.XMLNode root = xmlDoc.createRootElement(rootName, null, null);
        root.setNamespace(XmlUtils.XSI_PREFIX, XmlUtils.XSI_NS);
		
        fields.sort();
		
        for(string s : fields) {
            try {
                string innerText = '';
                boolean isNull = false;
                try {
                    if (obj.get(s) != null) {
						object value = obj.get(s);
						
						if (value instanceOf Date) {
				            innerText = Datetime.newInstance(((Date)value).year(), ((Date)value).month(), ((Date)value).day()).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
				        } else if (value instanceOf Datetime){
				            innerText = ((Datetime)value).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
				        } else {
				            innerText = string.valueof(value);
				        }
						
                    } else {
                        isNull = true;
                    }
                } catch (sObjectException ex) {
                    if (!outputAllFields) {
                        throw ex;
                    }
                }
                if (isNull) {
                    DOM.XmlNode child = root.addChildElement(s, null, null);
					child.setAttributeNs('nil', 'true', XmlUtils.XSI_NS, null);
                } else {
                    root.addChildElement(s, null, null).addTextNode(innerText);    
                }
            } catch (sObjectException ex) {
                if (failOnFirstException) {
                    throw ex;
                }
            }
        }

        System.debug('XmlSerializer.Serialize: ' + xmlDoc.toXmlString());

        return xmlDoc;
    }
    
/// ===================================================================================
///  -- N.B.     Deserialize has not been fully tested yet
/// ===================================================================================
    
	
/// -----------------------------------------------------------------------
///  -- Deserialize Objects
/// -----------------------------------------------------------------------    
    public XmlSerializable Deserialize(DOM.Document xmlDoc, String typeName) {
    	system.debug('*** XmlSerializable Deserialize xmlDoc: ' + xmlDoc.toXmlString());
        DOM.XmlNode root = xmlDoc.getRootElement();
        return Deserialize(root, typeName);
    }
	
	public XmlSerializable Deserialize(DOM.XmlNode root, String typeName) {
		XmlSerializable obj;
        
        system.debug('*** XmlSerializable Deserialize root: ' + root);
        
		obj = (XmlSerializable)createObject(typeName);
        
        for(DOM.XmlNode node : root.getChildElements()) {
			obj.put(node.getName(), node.getText());
        }
        
        return obj;
	}
	
/// -----------------------------------------------------------------------
///  -- Deserialize sObjects
/// -----------------------------------------------------------------------    
    public sObject Deserialize(DOM.Document xmlDoc) {
        DOM.XmlNode root = xmlDoc.getRootElement();
        return Deserialize(root);
    }
	
	public sObject Deserialize(DOM.XmlNode root) {
		sObject obj;
        
        if (root.getChildElement('id', null) != null) {
            obj = createsObject(root.getName(), root.getChildElement('id', null).getText());
        } else {
            obj = createsObject(root.getName());
        }
        
        for(DOM.XmlNode node : root.getChildElements()) {
            try {
            	obj.put(node.getName(), node.getText());
			} catch (sObjectException ex) {
				//field might not be editable...e.g. createdDate
			}
        }
        
        return obj;
	}
    
/// -----------------------------------------------------------------------
///  -- Dynamically Instantiate Objects
/// -----------------------------------------------------------------------
    private XmlSerializable createObject(String typeName) {
        Type targetType = Type.forName(typeName);
        if (targetType == null) {
            throw new InvalidObjectTypeException();
        }
        XmlSerializable obj = (XmlSerializable)targetType.newInstance();
        return obj;
    }
    
/// -----------------------------------------------------------------------
///  -- Dynamically Instantiate sObjects
/// ----------------------------------------------------------------------- 
    private sObject createsObject(String typeName) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        if (targetType == null) {
            throw new InvalidsObjectTypeException();
        }
        
        // Instantiate an sObject with the type passed in as an argument
        //  at run time.
        return targetType.newSObject(); 
    }
    
    private sObject createsObject(String typeName, string id) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        if (targetType == null) {
            throw new InvalidsObjectTypeException();
        }
        
        // Instantiate an sObject with the type passed in as an argument
        //  at run time.
        return targetType.newSObject(id); 
    }
 
/// -----------------------------------------------------------------------
///  -- Invalid sObject Type Exception
/// -----------------------------------------------------------------------  
    public class InvalidsObjectTypeException extends Exception {
        
    }
    
    public class InvalidObjectTypeException extends Exception {
        
    }
}

/*
//Example Usage 

XmlSerializer x = new XmlSerializer ();
Account a = [SELECT id, name, AccountNumber, BillingCity, BillingCountry, BillingLatitude,
             BillingLongitude, BillingPostalCode, BillingState, BillingStreet, Company_ID__c,
             CreatedById, CreatedDate FROM Account LIMIT 1];
DOM.XmlDocument = x.Serialize(a, a.getsObjectType(), false);

*/