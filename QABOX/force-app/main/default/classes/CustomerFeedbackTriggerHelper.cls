/*
*    Name of Company:     CTS    
*    Purpose of Class:   Business Logic of CustomerFeedback Trigger are implemented here. 
    
**    Modification Log
*    --------------------------------------------------------------------------------------------------------
*    Developer                 |               Date             |           Description
*    --------------------------------------------------------------------------------------------------------
     Abhirup                   |            1/11/2018           |     SF-4503 CF Feedback post in Account Chatter
     Abhirup                   |            11/12/2018          |     SF-4593 CF Handling Resp access Confidential feedback
*/
public class CustomerFeedbackTriggerHelper{
List<Task> lstTask =new List<Task>();
List<Task> updatedlstTask =new List<Task>();
List<Id> cfIds = new List<Id>();


public void updateCompanyOwner(List<Customer_Feedback__c> newCFs){
    for(Customer_Feedback__c aCustomerFeedback : newCFs){
        if(aCustomerFeedback.Responsible__c == null && aCustomerFeedback.Key_feedback_issue_area__c != 'PEME'){
            if(aCustomerFeedback.Company_to__c != null){
                Account selectedAccount = [SELECT ID, OwnerId FROM ACCOUNT WHERE ID =: aCustomerFeedback.Company_to__c];
                aCustomerFeedback.Responsible__c = selectedAccount.OwnerId ;
            }
            else if(aCustomerFeedback.Company_to__c == null && aCustomerFeedback.From_Company__c != null){
                Account selectedAccount = [SELECT ID, OwnerId FROM ACCOUNT WHERE ID =: aCustomerFeedback.From_Company__c];
                aCustomerFeedback.Responsible__c = selectedAccount.OwnerId ;
            }
        }
        if(aCustomerFeedback.Key_feedback_issue_area__c != null && aCustomerFeedback.Key_feedback_issue_area__c == 'PEME'){    //added for SF-1940
            aCustomerFeedback.Responsible__c = System.Label.People_Claim_area_leader; 
        }
    }
}

public void updateTaskStatus(List<Customer_Feedback__c> newCFs){
    for (Customer_Feedback__c cf: newCFs){
        if(cf.Type_of_feedback__c == 'Negative feedback' || cf.Type_of_feedback__c=='Positive feedback' || cf.Type_of_feedback__c=='Suggestion')
        {
            cfIds.add(cf.id);
        }
    }  
    lstTask=[SELECT Id,Status FROM Task where WhatId in :cfIds];
    for(Task tas:lstTask){
        tas.Status='Completed';
        updatedlstTask.add(tas);
    }
    if(updatedlstTask.size()>0){
        update updatedlstTask;
    }
}
    //SF-4593 starts
    public void accessToHandlerResp(List<Customer_Feedback__c> newCFs){        
        List<Customer_Feedback__share> cf_shares = new List<Customer_Feedback__share>();
        for(Customer_Feedback__c objCf : newCFs){            
            if(objCf.Customer_feedback_type__c == 'Confidential' && objCF.Responsible__c != NULL){
                Customer_Feedback__share cf_share = new Customer_Feedback__share();
                    cf_share.ParentId = objCF.Id;
                    cf_share.UserOrGroupId = objCF.Responsible__c;
                    cf_share.AccessLevel = 'Edit';
                    cf_share.RowCause = Schema.Customer_Feedback__share.rowCause.RespHandler_owner__c;
        
                cf_shares.add(cf_share);
            }
        }
        if(cf_shares.size()>0)
            insert cf_shares;
    }
    public void removeOldHandlerResp_Share(List<Customer_Feedback__c> newCFs, Map<ID, Customer_Feedback__c> oldCFsMap){
        Map<Id,Customer_Feedback__share> user_cfshrMap = new Map<Id,Customer_Feedback__share>();
        List<Customer_Feedback__share> oldRespHand_ShrLst = new List<Customer_Feedback__share>();
        List<Customer_Feedback__share> lstCF_shr =[SELECT Id,AccessLevel,ParentId,RowCause,UserOrGroupId FROM Customer_Feedback__Share where RowCause='RespHandler_owner__c'];
        for(Customer_Feedback__share shr : lstCF_shr){
            user_cfshrMap.put(shr.UserOrGroupId,shr);
        }
        for(Customer_Feedback__c cf : newCFs){
            if(oldCFsMap.get(cf.Id).Responsible__c != NULL && cf.Responsible__c != oldCFsMap.get(cf.Id).Responsible__c && cf.Customer_feedback_type__c == 'Confidential'){
                oldRespHand_ShrLst.add(user_cfshrMap.get(oldCFsMap.get(cf.Id).Responsible__c));
            }
        }
        System.debug('Hi outside IF b4 del');
        if(oldRespHand_ShrLst.size() > 0 && !oldRespHand_ShrLst.isEmpty() && oldRespHand_ShrLst != null){
            System.debug('Hi inside IF b4 del');
            delete oldRespHand_ShrLst;        
        }
    }
    //SF-4593 ends
    //SF-4503 starts
    public void createChatterFeed(Customer_Feedback__c objCF){
        List<FeedItem> postList = new List<FeedItem>();
        FeedItem post = new FeedItem();
        post.ParentId = objCF.From_Company__c;
        post.Body = 'New Customer feedback: '+objCF.Title__c+'\n'+
            'Type of Customer Feedback'+' : '+objCF.Type_of_feedback__c+'\n'+
            'Date feedback received: '+objCF.Date_feedback_received__c.format();
        post.LinkURL =URL.getSalesforceBaseUrl().toExternalForm() + '/' +objCF.Id;
        post.title ='View Feedback';
        postList.add(post); 
        if(postList.size() > 0){
            insert postList;  
        }        
    }
    public void postChatterFeed(List<Customer_Feedback__c> newCFs, Map<ID, Customer_Feedback__c> oldCFsMap){
        for(Customer_Feedback__c objCF: newCFs){
            if((objCF.Customer_feedback_type__c == 'Open') && objCF.From_Company__c != NULL ){
                createChatterFeed(objCF);
            }
            
        }  
    }
    
    public void postChatterFeedOnUpdate(List<Customer_Feedback__c> newCFs, Map<ID, Customer_Feedback__c> oldCFsMap){
        for(Customer_Feedback__c objCF: newCFs){       
            if((objCF.Customer_feedback_type__c== 'Open') && objCF.From_Company__c != NULL && oldCFsMap.get(objCF.Id).Customer_feedback_type__c!='Open'){
                createChatterFeed(objCF);
            }
            
        } 
    }
    //SF-4503 ends
}