@isTest(seeAllData = false)
public class testTagMigration{
    public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id; 
    static testMethod void testMigrateTags(){
        test.startTest();
        //GardTestData test_data = new GardTestData();
        //test_data.commonRecord();
        Country__c country=new Country__c();
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt;
        user salesforceLicUser = new User(
                                        Alias = 'aliasT', 
                                        profileId = salesforceLicenseId ,
                                        Email= 'test@test.com',
                                        EmailEncodingKey='UTF-8',
                                        CommunityNickname = 'testing tests',
                                        LastName='test LastName',
                                        LanguageLocaleKey= 'en_US',
                                        LocaleSidKey= 'en_US', 
                                        //contactID = brokercontact.id, 
                                        TimeZoneSidKey= 'America/Los_Angeles' ,
                                        UserName='mygardtest008@testorg.com.mygard'
                                   );
         insert salesforceLicUser;
        Account newAcc = new Account(Name = 'Test Account' , 
                                    BillingStreet = 'Test Street',
                                    BillingCity = 'phuket', 
                                    BillingCountry =  'test country'  , 
                                    BillingPostalCode =  'test code' ,
                                    BillingPostalCode__c= 'test',
                                    BillingCity__c= 'test',
                                    Country__c=country.id,
                                    Market_Area__c = Markt.id,
                                    Area_Manager__c = salesforceLicUser.id
                                    );
        tagMigration tm = new tagMigration();
        tm.executeBatch();
        test.stopTest();
    }
}