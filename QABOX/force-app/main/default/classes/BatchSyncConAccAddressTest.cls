@isTest
private class BatchSyncConAccAddressTest {
		public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    	public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    static testmethod void processContacts() {
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        setupTestData();
        
        Test.startTest();
        
        String query = 'SELECT Id FROM Contact WHERE LastName = \'TEST LAST NAME\' AND FirstName = \'TEST FIRST NAME\'';
        BatchSyncConAccAddress batch = new BatchSyncConAccAddress();
        batch.query = query;
        Database.executeBatch(batch);
        
        Test.stopTest();
    
        validate();
    }
    
    private static void setupTestData() {
        Country__c country = new Country__c(name='TESTCOUNTRY');
        insert country;
        system.assert(country.Id!=null);
        
        Market_Area__c marketarea = new Market_Area__c(name='TESTMA');
        insert marketarea;
        system.assert(marketarea.Id!=null);
        
        User areamgr = [SELECT Id FROM User WHERE Position__c LIKE '%Area Manager%' LIMIT 1];
        system.assert(areamgr!=null);
        
        Account acc = new Account();
        acc.Name = 'TEST ADDRESS ACCOUNT';
        acc.RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Client').RecordTypeId;
        acc.Site = 'TESTSITE';
        acc.Country__c = country.Id;
        acc.Market_Area__c = marketarea.Id;
        acc.Area_Manager__c = areamgr.Id;
        acc.BillingStreet = 'BILLING 1\nBILLING 2\nBILLING 3\nBILLING 4';
        acc.BillingCity = 'BILLINGCITY';
        acc.BillingState = 'BILLINGSTATE';
        acc.BillingPOstalCode = 'BILLINGPOSTALCODE';
        acc.BillingCountry = 'BILLINGCOUNTRY';
        insert acc;
        system.assert(acc.Id!=null);
        
        Contact con = new Contact();
        con.LastName = 'TEST LAST NAME';
        con.FirstName = 'TEST FIRST NAME';
        con.MailingStreet = 'old street';
        con.AccountId = acc.Id;
        insert con;
        system.assert(con.Id!=null);
        system.assert([SELECT Id FROM Contact WHERE LastName = 'TEST LAST NAME' AND FirstName = 'TEST FIRST NAME' LIMIT 1].size()==1);
    }
    
    private static void validate() {
        Contact con = [SELECT Id, FirstName, LastName, MailingStreet, MailingCity, MailingPostalCode, MailingState, MailingCountry, Account.BillingStreet, Account.BillingCity, Account.BillingPostalCode, Account.BillingState, Account.BillingCountry FROM Contact WHERE LastName = 'TEST LAST NAME' AND FirstName = 'TEST FIRST NAME' LIMIT 1];
        system.assertNotEquals(con, null);
        system.assertEquals(con.MailingStreet, con.Account.BillingStreet);
        system.assertEquals(con.MailingCity, con.Account.BillingCity);
        system.assertEquals(con.MailingPostalCode, con.Account.BillingPostalCode);
        system.assertEquals(con.MailingState, con.Account.BillingState);
        system.assertEquals(con.MailingCountry, con.Account.BillingCountry);
    }
}