//Test Class : TestEmailMessageReparenting
//PROBLEM : If an Email2Case chain EmailMessage is replied back to from a mailbox to SF, with different E2C addresses in To or CC, SF adds all the cc'ed EmailMessages under the same case, this class : 
//1. Find if an incoming Email Message to an already Existing case has duplicates :
//1.1 If the Parent case is Closed:
//1.1.1 Create new child clone cases fo the main parent, as many in number as the duplicate EmailMessages are.
//1.2 If the Parent case is Open:
//1.2.1 Leave 1 EmailMessage with parent case, and create child clone cases as many as the remaining EmailMessages
//1.3 Reparent the EmailMessages  to these child clone cases
global class EmailMessageReparentingJobs implements Queueable{
    //instance
    private String jobName;
    private Id parentCaseId;
    private String fromAddress;
    private String messageDate;
    private Integer trialNumber;
    private Case parentCase;
    List<Id> cloneCandidateTeamIds;
    List<Id> cloneCandidateEmIds;
    List<Case> childCloneCases;
    private Integer jobNumber;
    private Boolean delayProcessing;
    
    //static
    public static final String delimiter = '##';
    
    //later, remove it, only for debug
    //private List<String> debugStrings;
    
    //QUEUEABLE CONSTRUCTORS
    public EmailMessageReparentingJobs(String jobName){//Job initiation
        //debugStrings = new List<String>();
        List<String> emailFilters = jobName.split(delimiter);
        if(emailFilters.size() == 4){
            this.parentCaseId = Id.valueOf(emailFilters[0]);
            this.fromAddress = emailFilters[1];
            this.messageDate = emailFilters[2];
            this.trialNumber = Integer.valueOf(emailFilters[3]);
            this.jobName = jobName;
            this.jobNumber = 1;
            //parentCase = [SELECT Id,Team__c,OwnerId,Status,ClosedDate FROM Case WHERE Id = :parentCaseId];
            //debugStrings.add('Job will run. jobName - '+jobName);
        }else{
            this.jobNumber = 0;//error
            //debugStrings.add('Job won\'t run. jobName - '+jobName);
        }
    }
    
    private EmailMessageReparentingJobs(EmailMessageReparentingJobs eMRJ){//job propagation
        //debugStrings = new List<String>();
        this.jobNumber = eMRJ.jobNumber+1;
        this.jobName = eMRJ.jobName;
        this.parentCaseId = eMRJ.parentCaseId;
        this.parentCase = eMRJ.parentCase;
        this.cloneCandidateEmIds = eMRJ.cloneCandidateEmIds;
        this.cloneCandidateTeamIds = eMRJ.cloneCandidateTeamIds;
        this.childCloneCases = eMRJ.childCloneCases;
    }
    
    //QUEUEABLE METHODS
    public void execute(QueueableContext qc){
        //logs
        //debugStrings.add('JOB-'+this.jobNumber+' QUEUEABLE LOGS :');
        //debugStrings.add('execute() this.parentCaseId - '+this.parentCaseId);
        //debugStrings.add('execute() this.cloneCandidateTeamIds - '+this.cloneCandidateTeamIds);
        //debugStrings.add('execute() this.cloneCandidateEmIds - '+this.cloneCandidateEmIds);
        
        Boolean jobSuccess = false;
        delayProcessing = false;
        //process selection and job call
    try{
            switch on this.jobNumber{
                when 1{
                    jobSuccess = createLists();
                    if(jobSuccess) System.enqueueJob(new EmailMessageReparentingJobs(this));
                    else if(delayProcessing) EmailMessageJobQueueingBatchScheduler.startDelayedQueue(this.parentCaseId,this.fromAddress,this.messageDate,this.trialNumber+1);
                }
                when 2{
                  system.debug('in When 2*****');
                    jobSuccess = createChildCloneCases();
                    if(jobSuccess) System.enqueueJob(new EmailMessageReparentingJobs(this));   
                }
                when 3{
                    jobSuccess = cloneEMsAndReparentFiles();
                }
                when else{
                    //debugStrings.add('No job ran. incorrect jobName');
                }
            }
    }catch(Exception ex){
            //debugStrings.add('Exception Occurred in job : '+this.jobNumber);
            //debugStrings.add('\ngetStackTraceString()'+ex.getStackTraceString());
            //debugStrings.add('\ngetMessage() - '+ex.getMessage());
            //debugStrings.add('\ngetTypeName() - '+ex.getTypeName());
    }
        
        //debugStrings.add('Job-'+this.jobNumber+(jobSuccess ? ' Succeded' : ' Failed'));
        //final logging code
        //debugStrings.add('JOB-'+this.jobNumber+' QUEUEABLE LOGS END');
    }
    
    //QUEUEABLE HELPER METHODS
    //Creates 2 Maps, caseIdToEmailIds and caseIdToRecipientTeamIds
    @TestVisible private Boolean createLists(){
        //debugStrings.add('createMaps() LOGS:\n');
        Boolean retVal = false;
        if(parentCaseId == null){
            //debugStrings.add('parentCaseId - '+parentCaseId);
            return retVal;
        }
        parentCase = [SELECT id,Team__c,ownerId,status FROM Case WHERE Id = :parentCaseId];
        //debugStrings.add('parentCase - '+parentCase);
        List<EmailMessage> triggerEms = new List<EmailMessage>();
        //debugStrings.add('filters - '+parentCaseId+' - '+fromAddress+' - '+messageDate);
        for(EmailMessage parentCaseEm : [SELECT id,ParentId,TextBody,fromAddress,toAddress,ccAddress,subject,CreatedDate,MessageDate FROM EmailMessage Where ParentId = :this.parentCaseId AND FromAddress = :this.fromAddress AND Incoming = true]){
           
            if((parentCaseEm.messageDate+'') == messageDate){
                triggerEms.add(parentCaseEm);
            }
            //debugStrings.add('DateTime.valueOf(parentCaseEm.messageDate+\'\') - '+(DateTime.valueOf(parentCaseEm.messageDate+'')));
            //debugStrings.add('(parentCaseEm.messageDate+\'\') - '+(parentCaseEm.messageDate+''));
            //debugStrings.add('messageDate - '+messageDate);
        }

        
        
        if(triggerEms.size() < 1){
            //debugStrings.add('triggerEms.size() < 1 for case - '+this.parentCaseId);
            return retVal;
        }
        //debugStrings.add('triggerEms.size() for case - '+triggerEms.size());
        
        try{
            EmailMessage triggerEmProto = triggerEms[0];
            List<String> recipientTeamIds = getRecipientTeamIds(triggerEmProto);
            //debugStrings.add('recipientTeamIds - '+recipientTeamIds);
            //debugStrings.add('triggerEms.size() - '+triggerEms.size());
            //debugStrings.add('recipientTeamIds.size() - '+recipientTeamIds.size());
            cloneCandidateEmIds = new List<Id>();
            cloneCandidateTeamIds = new List<Id>();
            if(!recipientTeamIds.isEmpty() && !(triggerEms.size() < recipientTeamIds.size())){
                for(Integer index = 0; index < recipientTeamIds.size() ; index++){
                    cloneCandidateEmIds.add(triggerEms[index].id);
                    cloneCandidateTeamIds.add(recipientTeamIds[index]);
                }
                delayProcessing = false;
                retVal = true;
            }else{
                delayProcessing = true;
                retVal = false;
            }
            
            //debugStrings.add('cloneCandidateTeamIds - '+cloneCandidateTeamIds);
            //debugStrings.add('cloneCandidateEmIds - '+cloneCandidateEmIds);
        }catch(Exception ex){//later, temp debugs added remove these
            System.debug('E2C error in Email duplicate flow - '+ex.getMessage());
            //debugStrings.add('Exception Occurred : ');
            //debugStrings.add('\ngetStackTraceString()'+ex.getStackTraceString());
            //debugStrings.add('\ngetMessage() - '+ex.getMessage());
            //debugStrings.add('\ngetTypeName() - '+ex.getTypeName());
        }
        
        return retVal;        
    }
    
    //Creates Child Clones based on the Duplicate Child EmailMessages
    @TestVisible private Boolean createChildCloneCases(){
        //debugStrings.add('createChildCloneCases() LOGS:\n');
        //debugStrings.add('(cloneCandidateTeamIds == null || cloneCandidateTeamIds.isEmpty()) -'+(cloneCandidateTeamIds == null || cloneCandidateTeamIds.isEmpty()));
        if(cloneCandidateTeamIds == null || cloneCandidateTeamIds.isEmpty()) return false;
        Map<String,QueueInEmail__c> teamIdToQieCsMap = EmailMessageReparentingJobs.getQieMap('id');
        system.debug('teamIdToQieCsMap *****'+teamIdToQieCsMap );
        // create soql query with all editable fields and run query
        //Set<String> caseIds = new Set<Id>{parentCaseId};//caseIdToEmIds.keySet();
        List<String> recordTypeName=new List<String>{'Customer_Transactions','Paris_document_support'};
        String caseWhereQuery = 'Id = :parentCaseId AND RecordType.DeveloperName IN:recordTypeName';  //SF-6203
        Set<String> caseExceptionStrings = new Set<String>{'SFDC_Claim_Ref_ID__c','GUID__c'};
        List<Case> originalCases = database.query(CaseCloneController.queryStringAllFields('Case', caseWhereQuery, caseExceptionStrings));
        
        ////debugStrings.add('caseIdMsgDateToTeamIds - '+caseIdMsgDateToTeamIds);
        if(!originalCases.isEmpty()){
            Case originalCase = originalCases[0];
            //debugStrings.add('originalCase.Id - '+originalCase.Id);
            //caseIdMsgDateToChildClones = new Map<String,List<Case>>();
            childCloneCases = new List<Case>();
            //SF-6203
            Id pardiRecordId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Paris document support').getRecordTypeId();
            system.debug('cloneCandidateTeamIds****'+cloneCandidateTeamIds);
            for(String cloneCandidateTeamId : cloneCandidateTeamIds){
                //debugStrings.add('cloneCandidateTeamId - '+cloneCandidateTeamId);
                String cloneCandidateTeamIdName = (teamIdToQieCsMap.containsKey(cloneCandidateTeamId)) ? teamIdToQieCsMap.get(cloneCandidateTeamId).Team__c : teamIdToQieCsMap.get(cloneCandidateTeamId.substring(0,15)).Team__c;
                String cloneCandidateTeamQueueId = (teamIdToQieCsMap.containsKey(cloneCandidateTeamId)) ? teamIdToQieCsMap.get(cloneCandidateTeamId).id__c : teamIdToQieCsMap.get(cloneCandidateTeamId.substring(0,15)).id__c;
                //added for SF-6203
                String cloneCandidateTeamName=(teamIdToQieCsMap.containsKey(cloneCandidateTeamId)) ? teamIdToQieCsMap.get(cloneCandidateTeamId).name : teamIdToQieCsMap.get(cloneCandidateTeamId.substring(0,15)).name;
                Case childClone = originalCase.clone(false, true, false, false);
                childClone.Status = 'New';
                childClone.Reasons_for_Waiting_for_Customer__c = '';//added for SF-6436
                childClone.ParentId = originalCase.Id;
                childClone.Team__c = cloneCandidateTeamIdName;
                childClone.sub_team__c = null;
                childClone.I_Am_A_Clone__c = TRUE;
                childClone.Category__c=''; 
                childClone.Sub_Category__c='';     
                childClone.is_auto_clone__c=true;
                childClone.childcreated__c =true;
                childClone.ownerId = cloneCandidateTeamQueueId;
                
                if(cloneCandidateTeamName.equalsIgnoreCase('Paris document support')){ //SF-6203
                  childClone.recordtypeId=pardiRecordId;
                   childClone.Team__c = null;
                }
                childCloneCases.add(childClone);
            }
            system.debug(' childCloneCases****'+ childCloneCases);
            if(!childCloneCases.isEmpty()){
                Database.SaveResult[] dbSRs;
                try{
                    dbSRs = Database.insert(childCloneCases,true);
                }catch(Exception ex){
                    //debugStrings.add('Exception Occurred in cloneCaseInsertion : ');
                    //debugStrings.add('\ngetStackTraceString()'+ex.getStackTraceString());
                    //debugStrings.add('\ngetMessage() - '+ex.getMessage());
                    //debugStrings.add('\ngetTypeName() - '+ex.getTypeName());
                    return false;
                }
                
                for(Database.SaveResult dbSR : dbSRs){
                    if(!dbSR.isSuccess())return false;
                }
            }
        }
        return true;
    }
    
    //reparents the EmailMessages to the last-job-created child clone cases
    @TestVisible private Boolean cloneEMsAndReparentFiles(){
        if(cloneCandidateEmIds == null) return true;
        
        Boolean retVal = true;
        //debugStrings.add('cloneEMsAndReparentFiles() LOGS:\n ');
        
        List<EmailMessage> clonedEms = new List<EmailMessage>();
        Map<String,EmailMessage> emIdToCloneEmMap = new Map<String,EmailMessage>();
        Map<String,List<ContentDocumentLink>> emIdToCdlsMap = new Map<String,List<ContentDocumentLink>>();
        
        //get all writeable fields of EM
        String emFieldsStr;
        Map<String, Schema.SObjectField> emFields = Schema.getGlobalDescribe().get('EmailMessage').getDescribe().fields.getMap();
        system.debug('emFields *****'+emFields );
        for(Schema.SObjectField emField : emFields.values()){
            Schema.DescribeFieldResult fieldResult = emField.getDescribe();
            if(fieldResult.isCreateable() && !fieldResult.isRestrictedPicklist()){
                emFieldsStr = (emFieldsStr == null) ? fieldResult.getName() : emFieldsStr+','+fieldResult.getName();
            }
        }
        system.debug('emFieldsStr ***'+emFieldsStr );
        system.debug('cloneCandidateEmIds***'+[select status from EmailMessage where id IN:cloneCandidateEmIds]);
        //retreive EMs to be cloned with all writeable fields
        Map<String,EmailMessage> emMap = new Map<String,EmailMessage>();
        for(EmailMessage em : Database.query('SELECT status,'+emFieldsStr+' FROM EmailMessage WHERE Id IN :cloneCandidateEmIds')){
            emMap.put(em.id,em);
        }
        //debugStrings.add('emMap.size() - '+emMap.size());
        
        //retreive cdls of reference EMs to be cloned for cloned EMs
        for(ContentDocumentLink cdl : Database.query('SELECT Id,LinkedEntityId,ContentDocumentId,ShareType,Visibility FROM ContentDocumentLink WHERE LinkedEntityId IN :cloneCandidateEmIds')){
            List<ContentDocumentLink> cdls = emIdToCdlsMap.containsKey(cdl.linkedEntityId) ? emIdToCdlsMap.get(cdl.linkedEntityId) : new List<ContentDocumentLink>();
            cdls.add(cdl);
            emIdToCdlsMap.put(cdl.LinkedEntityId,cdls);
        }
        
        //debugStrings.add('cloneCandidateEmIds.size() == childCloneCases.size() - '+(cloneCandidateEmIds.size() == childCloneCases.size()));
        system.debug('childCloneCases******'+childCloneCases);
       // if(!childCloneCases.isEmpty()){
        for(Integer index = 0; index < childCloneCases.size(); index++){
            Id refEmId = cloneCandidateEmIds[index];
            EmailMessage cloneEm = emMap.get(refEmId).clone(false,false,false,false);
            cloneEm.ParentId = childCloneCases[index].id;
            //cloneEm.Incoming = true;
            cloneEm.isCloned__C = true;
            system.debug('cloneEm****'+cloneEm.status);
            clonedEms.add(cloneEm);
            emIdToCloneEmMap.put(refEmId,cloneEm);
        }
        
        Database.SaveResult[] dbSRs;
        Database.DeleteResult[] dbDRs;
   try{
            //insert EmClones
            if(!clonedEms.isEmpty()){
                dbSRs = Database.insert(clonedEms,true);
                for(Database.SaveResult dbSR : dbSRs){
                    if(!dbSR.isSuccess()){
                        retVal = false;
                        break;
                    }
                }
                //debugStrings.add('clonedEMs Insert '+(retVal ? 'Succeded' : 'Failed'));
            }
            
            //create cdls for migrating EMs' Files to EmClones
            List<ContentDocumentLink> newCdls = new List<ContentDocumentLink>();
            for(Id refEmId : emIdToCloneEmMap.keySet()){
                EmailMessage clonedEm = emIdToCloneEmMap.get(refEmId);
                
                if(!emIdToCdlsMap.containsKey(refEmId)) 
                    continue;
                
                for(ContentDocumentLink cdl : emIdToCdlsMap.get(refEmId)){
                    newCdls.add(new ContentDocumentLink(ContentDocumentId = cdl.ContentDocumentId,linkedEntityId = clonedEm.Id,ShareType = cdl.ShareType,Visibility = cdl.Visibility));
                    newCdls.add(new ContentDocumentLink(ContentDocumentId = cdl.ContentDocumentId,linkedEntityId = clonedEm.parentId,ShareType = cdl.ShareType,Visibility = cdl.Visibility));
                }
            }
            //insert EmClones cdls
            if(!newCdls.isEmpty()){
                dbSRs = Database.insert(newCdls,true);
                for(Database.SaveResult dbSR : dbSRs){
                    if(!dbSR.isSuccess()){
                        retVal = false;
                        break;
                    }
                }
                //debugStrings.add('newCdls Insert '+(retVal ? 'Succeded' : 'Failed'));
            }
            
            //delete exisitng  EMs
            if(!cloneCandidateEmIds.isEmpty()){
                dbDRs = Database.delete(cloneCandidateEmIds,true);
                for(Database.DeleteResult dbDR : dbDRs){
                    if(!dbDR.isSuccess()){
                        retVal = false;
                        break;
                    }
                }
            }
            //debugStrings.add('existingEms Delete '+(retVal ? 'Succeded' : 'Failed'));
       }catch(Exception ex){
            //debugStrings.add('Exception Occurred in EM reParenting : ');
            //debugStrings.add('\ngetStackTraceString()'+ex.getStackTraceString());
            //debugStrings.add('\ngetMessage() - '+ex.getMessage());
            //debugStrings.add('\ngetTypeName() - '+ex.getTypeName());
         retVal = false;
       }
        return retVal;
    }
    
    //QUEUEABLE UTILITY METHODS    
    //returns qieCS records with different mapping than default
    public static Map<String,QueueInEmail__c> getQieMap(String mapType){
        Map <String,QueueInEmail__c> retQieCsMap = new Map<String,QueueInEmail__c>();
        Map<String,QueueInEmail__c> qieCsMap = QueueInEmail__c.getAll();
        
        for(String qieName : qieCsMap.keySet()){
            QueueInEmail__c qie = qieCsMap.get(qieName);
          //  if(!String.isEmpty(qie.Team__c) && !String.isEmpty(qie.Email__c)){ Modified as part of SF-6203
                if(mapType.equalsIgnoreCase('teamname') && !String.isEmpty(qie.Team__c)){
                    retQieCsMap.put(qie.Team__c,qie);
                }else if(mapType.equalsIgnoreCase('email') && !String.isEmpty(qie.Email__c)){
                    retQieCsMap.put(qie.Email__c.toLowerCase(),qie);
                }else if(mapType.equalsIgnoreCase('id') && !String.isEmpty(qie.Id__c)){
                    retQieCsMap.put(qie.Id__c,qie);
                }
          //  }
        }
        system.debug('retQieCsMap****'+retQieCsMap);
        return retQieCsMap;
    } 
    
    
    //get the emails of teams related to which the cases should be created
    private List<String> getRecipientTeamIds(EmailMessage em){
        List<String> retRecipientTeamIds = new List<String>();
        List<String> nonRecipients = new List<String>();
        Map<String,QueueInEmail__c> teamNameToQieCsMap = EmailMessageReparentingJobs.getQieMap('teamName');
        Map<String,QueueInEmail__c> emailToQieCsMap = EmailMessageReparentingJobs.getQieMap('email');
        Boolean parentCaseClosed = parentCase.Status.containsIgnoreCase('closed');
         String parentCaseTeamEmail='';
        //if(parentCase.Team__c!=null){
        parentCaseTeamEmail = (parentCase.Team__c != null) ? teamNameToQieCsMap.get(parentCase.Team__c).Email__c.trim().toLowerCase() 
        : teamNameToQieCsMap.get('Paris').Email__c.trim().toLowerCase() ;//Because only Paris cases dont have a team
        //}
        //debugStrings.add('em.toAddress - '+em.toAddress);
        //debugStrings.add('em.ccAddress - '+em.ccAddress);
        String allRecipients = em.toAddress+((!String.isEmpty(em.ccAddress)) ? (';'+em.ccAddress) : '' );
        for(String toAddress : allRecipients.split(';')){
            String lowerCaseEmailId = toAddress.trim().toLowerCase();
            if(emailToQieCsMap.containsKey(lowerCaseEmailId)
               && !parentCaseTeamEmail.equalsIgnoreCase(lowerCaseEmailId) /*&& (parentCaseClosed || !parentCaseTeamEmail.equalsIgnoreCase(lowerCaseEmailId))*///commented and replaced for SF-5382, but may be uncommented in a future release
              ) retRecipientTeamIds.add(emailToQieCsMap.get(lowerCaseEmailId).Id__c);
        }
        //system.debug('retRecipientTeamIds***'+);
        return retRecipientTeamIds;
    }
    
}