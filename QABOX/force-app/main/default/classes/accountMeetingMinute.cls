public with sharing class accountMeetingMinute {
	
	public List<Account_Meeting_Minute__c> minutes {get; set;}
	
	public accountMeetingMinute(ApexPages.StandardController con){
		
		con.getrecord();
		minutes = [Select a.Meeting_Minute__r.Event_StartDateTime__c, a.Meeting_Minute__r.Event_Location__c, a.Meeting_Minute__r.Event_Subject__c, a.Meeting_Minute__r.Event_Id__c, a.Meeting_Minute__r.Id, a.Meeting_Minute__c, a.Event__c, a.Account__c From Account_Meeting_Minute__c a where a.Account__c = : con.getId()];
	}
	
	static testMethod void test(){
		Account_Meeting_Minute__c a = new Account_Meeting_Minute__c();
		ApexPages.StandardController sc = new ApexPages.Standardcontroller(a);
		accountMeetingMinute amm = new accountMeetingMinute(sc);
		
	}
	
}