public with sharing class WebServiceUtil {
    /*public static String getSoapHeader() 
    {
        String soapMessage = '';    
        soapMessage+='<soapenv:Header />';
        return soapMessage;
    }*/
    
    public static Dom.Document processRequest(String ep, String msg, String actionURL) 
    {
        String authToken = '';
        Dom.Document doc;
        System.debug('EndPoint:'+ep);
        //Get Auth_Token for OAuth2.0 : Added for SF-5557
        Mule_Web_Service__c accessTokenDetails = Mule_Web_Service__c.getInstance('Retreive access token');
        TokenResponseWrapper tokenResponseWrapper = new TokenResponseWrapper();
        
        Http sendTokenReq = new Http();
        Httprequest tokenReq = new Httprequest();
        tokenReq.setEndpoint(accessTokenDetails.Endpoint_Url__c);
        tokenReq.setheader('scope','READ WRITE');
        tokenReq.setheader('client_id',accessTokenDetails.Client_Id__c);
        tokenReq.setheader('client_secret',accessTokenDetails.Client_secret__c);
        tokenReq.setheader('grant_type','client_credentials');
        tokenReq.setMethod('POST');
        if (!Test.isRunningTest()) {
            HttpResponse response = sendTokenReq.send(tokenReq);
            //system.debug('Token received : '+response.getBody());
        	tokenResponseWrapper = (TokenResponseWrapper)System.JSON.deserialize(response.getBody(), TokenResponseWrapper.class);
             authToken = 'Bearer '+tokenResponseWrapper.access_token;
        }
        else{
            authToken = 'Bearer NO2e7p0vwQVBdMmpP4RrOMy6EQprYbhJS177PsmvfHLSJZ5cCxeKeHTBBthUnj70DDRPr0naFVa4uRls3fu4uw';
        }
        system.debug('Access token : '+tokenResponseWrapper.access_token);
       
        MDMConfig__c mdmConfig = MDMConfig__c.getInstance();
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
       /* if (actionURL!=null && actionURL!='') 
            req.setHeader('SOAPAction', actionURL);
        else 
            req.setHeader('SOAPAction', '""');
        req.setHeader('Content-Type', 'text/xml;charset=utf-8');*/
        req.setHeader('Content-Type',mdmConfig.Application_type__c);
        req.setHeader('Accept',mdmConfig.Application_type__c);
        //req.setHeader('client_id',mdmConfig.Client_Id__c);    //Commented for SF-5557
        //req.setHeader('client_secret',mdmConfig.Client_secret__c);    //Commented for SF-5557
        req.setheader('Authorization',authToken);    //Added for SF-5557
        req.setEndpoint(ep);
        req.setBody(msg);       
        req.setTimeout(20000);

        HttpResponse res;
        if (!Test.isRunningTest()) {
            System.debug('Web Service Call request: '+String.valueOf(req));
            res = h.send(req);   
            System.debug('Web Service Call response: '+String.valueOf(res));
            doc = res.getBodyDocument();
        } else {
            // Test version of response for Apex Unit Tests
            System.debug('*** MOCK Web Service Call request: ' + String.valueOf(req));
            WebServiceUtilTests.Instance.MockWebServiceRequest = req;
            if (WebServiceUtilTests.Instance.MockWebServiceResponse != null && WebServiceUtilTests.Instance.MockWebServiceResponse instanceOf Dom.Document) {
                doc = (Dom.Document)WebServiceUtilTests.Instance.MockWebServiceResponse;
                System.debug('*** MOCK Web Service Call response: ' + doc.toXmlString());
            }
        }
        try {
            Logger.LogCallout(msg, doc.toXmlString());
        } catch (Exception e) {
            System.debug('Exception ******'+e);
            //Don't let logging cause an exception...
        }
        return doc;
    } 
    
    //Child class to desirialize access token response
    Public class TokenResponseWrapper{
        public String access_token;
        public String refresh_token;
        public String scope;
        public String token_type;
        public String expires_in;
    }
    ///Obselete code, used for connecting to Enterprise WSDL in initial service development...
    /*
    public static String LoginToEnterpriseWSDL(String username, String password) {
        DOM.Document xmlDoc = new DOM.Document();
      
        String soapNS = MDMConfig__c.getInstance().SOAPNS__c; //String xsi = 'http://www.w3.org/2001/XMLSchema-instance';
        
        String prefix = 'urn';
        String serviceNS = 'urn:enterprise.soap.sforce.com';

        DOM.XmlNode envelope = xmlDoc.createRootElement('Envelope', soapNS, 'soapenv');
        envelope.setNamespace(prefix, serviceNS);
        
        DOM.XMLNode body = envelope.addChildElement('Body', soapNS, null);
        DOM.XMLNode login = body.addChildElement('login', serviceNS, prefix);
        login.addChildElement('username', serviceNS, prefix).addTextNode(username);
        login.addChildElement('password', serviceNS, prefix).addTextNode(password);
        

        system.debug('Request: ' + xmlDoc.toXMLString());
        
        DOM.Document loginResponse = processRequest('https://test.salesforce.com/services/Soap/c/28.0/0DFc00000004CZ1', xmlDoc.toXMLString(), null);
        
        system.debug('Response: ' + loginResponse.toXMLString());
        
        try {
            system.debug('Session ID: ' + loginResponse.getRootElement().getChildElement('Body', soapNS).getChildElement('loginResponse', serviceNS).getChildElement('result', serviceNS).getChildElement('sessionId', serviceNS).getText());
            return loginResponse.getRootElement().getChildElement('Body', soapNS).getChildElement('loginResponse', serviceNS).getChildElement('result', serviceNS).getChildElement('sessionId', serviceNS).getText();
        } catch (Exception ex) {
            return null;
        }

    }
    
    public static String getMDMTestSession() {
        return LoginToEnterpriseWSDL('mdmtest@gard.no.mdmdev','6gyhsdHrJmI3UcURXYDswRQ4SIIxOMPfYDLT3ehxI71vYHTXWZD8I7HzpIx5');
    }
    */
}