@isTest
public class TestMyGardHelperCtrl {
    static User clientUser,brokerUser;
    static GardTestData gtd;
    
    private static void createTestData(){
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        gtd = new GardTestData();
        gtd.Mycover_list_objectCtrl_testrecord(); 
        gtd.customsettings_rec();
    }
    
    @isTest private static void TestMyGardHelperBroker(){
        createTestData();
        System.runAs(GardTestData.broker_User){
            Test.startTest();
            MyCoverCtrl test_broker = new MyCoverCtrl();
            MyGardHelperCtrl.fetchGlobalClientsHelper(GardTestData.broker_acc.id,GardTestData.broker_User.id);
            MyGardHelperCtrl.LOGGED_IN_CONTACT_ID = GardTestData.broker_contact.id;
            MyGardHelperCtrl.LOGGED_IN_ACCOUNT_ID = GardTestData.broker_acc.id;
            MyGardHelperCtrl.dummyCovers();
            MyGardHelperCtrl.hidePrepemeObject();
            MyGardHelperCtrl.showSwitchCompany();
            MyGardHelperCtrl.setExtranetGobalClients();
            MyGardHelperCtrl.fetchSelectedClientsHelper(GardTestData.client_Acc.id+'');
            MyGardHelperCtrl.getOrganizationDetails();
            MyGardHelperCtrl.fetchUWRFormHelper(GardTestData.broker_contact.id);
            MyGardHelperCtrl.attachMailToObject(GardTestData.broker_contact.id+'', null, 'mailBody', 'mailSubject');
            MyGardHelperCtrl.attachMailToObject(GardTestData.broker_contact.id+'', GardTestData.testobj.id, 'mailBody', 'mailSubject');
            //attachMailToObject(String loggedInContactId, Id objectId, String mailBody, String mail_subject)
            MyGardHelperCtrl.fetchSelectedClientName(GardTestData.client_Acc.id);
            MyGardHelperCtrl.queueEmailMembers('Portfolio requests handler queue');
            MyGardHelperCtrl.fetchUsersOfLoggedInCompany();
            MyGardHelperCtrl.getAllHoursGMT();
            MyGardHelperCtrl.getAllHours();
            MyGardHelperCtrl.getAllTimeZones();
            //MyGardHelperCtrl.createClientEvents(GardTestData.broker_User.Id+'' ,GardTestData.client_Acc.id+'', 'evetType', 'object1,object2,object3', new List<String>{GardTestData.testobj.name}, String coverNames, List<String> covers, String additionalDetails);
            //createClientEvents(String loggedInUserId ,String selectedClientId, String eventType, String objectNames, List<String> objects, String coverNames, List<String> covers, String additionalDetails)
            Test.stopTest();
        }
    }
    
    @isTest private static void TestMyGardHelperClient(){
        createTestData();
        System.runAs(GardTestData.client_User){
            Test.startTest();
            MyCoverCtrl test_client = new MyCoverCtrl();
            
            MyGardHelperCtrl.fetchGlobalClientsHelper(GardTestData.client_acc.id,GardTestData.client_User.id);
            MyGardHelperCtrl.LOGGED_IN_CONTACT_ID = GardTestData.client_contact.id;
            MyGardHelperCtrl.LOGGED_IN_ACCOUNT_ID = GardTestData.client_acc.id;
            MyGardHelperCtrl.dummyCovers();
            MyGardHelperCtrl.hidePrepemeObject();
            MyGardHelperCtrl.showSwitchCompany();
            MyGardHelperCtrl.setExtranetGobalClients();
            MyGardHelperCtrl.fetchSelectedClientsHelper((GardTestData.client_Acc.id+''));
            MyGardHelperCtrl.getOrganizationDetails();
            MyGardHelperCtrl.fetchUWRFormHelper(GardTestData.client_contact.id);
            MyGardHelperCtrl.attachMailToObject(GardTestData.client_contact.id+'', null, 'mailBody', 'mailSubject');
            MyGardHelperCtrl.attachMailToObject(GardTestData.client_contact.id+'', GardTestData.testobj.id, 'mailBody', 'mailSubject');
            //attachMailToObject(String loggedInContactId, Id objectId, String mailBody, String mail_subject)
            MyGardHelperCtrl.fetchSelectedClientName(GardTestData.client_Acc.id);
            MyGardHelperCtrl.queueEmailMembers('Portfolio requests handler queue');
            MyGardHelperCtrl.fetchUsersOfLoggedInCompany();
            MyGardHelperCtrl.getAllHoursGMT();
            MyGardHelperCtrl.getAllHours();
            MyGardHelperCtrl.getAllTimeZones();
            
            Test.stopTest();
        }
    }
}