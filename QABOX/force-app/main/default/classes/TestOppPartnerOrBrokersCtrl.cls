@isTest
public class TestOppPartnerOrBrokersCtrl{
    public static testMethod void OppPartnerOrBrokersTestMethod(){
        GardTestData testRecs = new GardTestData();
        testRecs.commonRecord();
        test.startTest();
        //Id RecordTypeIdOpportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('P_I').getRecordTypeId();
        Opportunity opportunity = new Opportunity();
        //opportunity.RecordTypeId = RecordTypeIdOpportunity;
        opportunity.Type = 'New Business';
        opportunity.Name = 'Test opportunity';
        opportunity.AccountId = GardTestData.clientAcc.Id;
        opportunity.StageName = 'Quote';
        opportunity.CloseDate = Date.today();
        opportunity.Business_Type__c = 'Ship Owners';
        opportunity.Amount = 150000;
        opportunity.Agreement_Market_Area__c = GardTestData.Markt.Id;
        opportunity.Area_Manager__c = GardTestData.salesforceLicUser.id;
        opportunity.Approval_Criteria__c = '';
        opportunity.Senior_Approver__c = null;      
        insert opportunity;
        
        Account acc = [select id, name from Account where Id =:GardTestData.clientAcc.Id];
        
        //String json=  '[{'+'"Role": "Other",'+'"IsPrimary ": false,'+'"AccountToId ": GardTestData.clientAcc.Id,'+'"OpportunityId":opportunity.Id'+'}'+']';
        
        //String json=  '[{'+'"Role": "Other",'+'"IsPrimary ": false,'+'"acc":'+'{'+'"Id":acc.Id,'+'"Name":acc.Name},'+'}]';
 
        JSON2ApexPartner js = new JSON2ApexPartner ();
        List<JSON2ApexPartner> jsList = new List<JSON2ApexPartner>();
        List<JSON2ApexPartner> deList = new List<JSON2ApexPartner>();
        js.role = 'Other';
        js.primary = 'False';
        JSON2ApexPartner.Acct acct1 = new JSON2ApexPartner.Acct();
        acct1.Id = GardTestData.brokerAcc.Id;
        acct1.Name = 'TestAcc';
        js.acct = acct1;
        jsList.add(js);
        String str = JSON.serialize(jsList);
        system.debug('str:::::::'+str);
        //testOpp = TestDataGenerator.getOpportunity('P&I','Quote','Ship Owners');
        //CloseWonOpportunity_lightning closeWonObj = new CloseWonOpportunity_lightning();
        OppPartnerOrBrokersCtrl.getRoles();
        OppPartnerOrBrokersCtrl.getOppName(opportunity.Id);
        //OppPartnerOrBrokersCtrl.getRecordList();
        //OppPartnerOrBrokersCtrl.createNewPartnersOrBrokers('test',opportunity.Id);
        OppPartnerOrBrokersCtrl.createNewPartnersOrBrokers(str,opportunity.Id);
        OppPartnerOrBrokersCtrl.getPrePopulatedForm();
        OppPartnerOrBrokersCtrl.getPartnersOrBrokers(opportunity.Id);
        OppPartnerOrBrokersCtrl.deletePartnersOrBrokers('test');
        OppPartnerOrBrokersCtrl.getCustomerDetails(opportunity.Id);
        OppPartnerOrBrokersCtrl.getRecordList('Opportunity','New Business','Type');
        test.stopTest();
    }
 
}