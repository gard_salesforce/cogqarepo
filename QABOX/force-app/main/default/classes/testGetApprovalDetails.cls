/*

Test class for GetApprovalDetails

 */
@isTest
private class testGetApprovalDetails {

    public static Opportunity testOpp;

    static void setupTestData()
    {
        Market_Area__c testMarketArea = new Market_Area__c(Name='Nordic', Market_Area_Code__c = 'NORD');
        insert testMarketArea;
        
        RecordType clientAccRecType = [SELECT Id From RecordType WHERE Name='Client' and SObjectType='Account'];
        
        Account testAcc = new Account(RecordTypeId = clientAccRecType.Id, Name='Test Company', Market_Area__c = testMarketArea.Id);
        insert testAcc;
        
        RecordType energyOppRecType = [SELECT Id From RecordType WHERE Name='Energy' and SObjectType='Opportunity'];
                
        testOpp = new Opportunity(RecordTypeId = energyOppRecType.Id, AccountId = testAcc.Id, Name='Test Opp', Type='New Business', Amount = 100, Sales_Channel__c='Direct', StageName='Risk Evaluation', CloseDate=Date.Today()+30, Business_Type__c='Energy Renewal', Approval_Criteria__c='Deviation from Budget');
        insert testOpp;
        
        //Need approvals data to test class
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
        app.setObjectId(testOpp.Id);
        Approval.ProcessResult result = Approval.process(app);
        
        System.assert(result.isSuccess());
        
        // Approve the submitted request
        // First, get the ID of the newly created item
        List<Id> newWorkItemIds = result.getNewWorkitemIds();
        
        // Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest request = new Approval.ProcessWorkitemRequest();
        request.setComments('Approving request.');
        request.setAction('Approve');
        request.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        
        // Use the ID from the newly created item to specify the item to be worked
        request.setWorkitemId(newWorkItemIds.get(0));
        
        // Submit the request for approval
        Approval.ProcessResult result2 =  Approval.process(request);
        
        // Verify the results
        System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());
        
    }

    static testMethod void myUnitTest() {
        
        setupTestData();
        
        Test.startTest();
        
        GetApprovalDetails GAD = new GetApprovalDetails();
        GAD.objId = testOpp.Id;
        System.assert(GAD.hasResults == true);
        //System.assert(GAD.approvalDetails.size() == 1);
        //System.assert(GAD.approvalDetails[0].Comments == 'Approving request.');

        Test.stopTest();
    }
}