public without sharing class ClaimsDocumentUtil {

 public static pagereference viewAllClaimDocuments(String claimGUID){
    if (!string.isEmpty(claimGUID)){
       
       PageReference page = new PageReference('/apex/ClaimDocumentDetails?claimid='+ claimGUID);
       //PageReference page = new PageReference('/apex/ClaimsDetailDocument?claimid='+ claimGUID);
       
       page.setRedirect(false);
        return page;
    }
    else
        return null;
 }
 
  
      public  static map<String,boolean> sortTable(String sortCol,boolean sortOrder, Map<String,Boolean> colToOrderMap,ClaimDocumentResponseWrapper claimDocResponseWrapper){
        sortOrder = !colToOrderMap.get(sortCol);
       
        claimDocResponseWrapper.sortIt(sortCol,sortOrder);
        colToOrderMap.put(sortCol,sortOrder);
        return colToOrderMap;
    }
    //Client activity for Document dowload
     public static void  createClientActivity(string currentDocumentId,String loggedInAccountId,String loggedInContactId,String selectLocalClient,boolean hasMultipleClients, Map <String,ClaimDocumentResponseWrapper.ResultObject>docIdObjectMap){
        final String DocumentName= 'Document name: '; 
        final String DocumentType= 'Document type description: ';
        final String DownloadAt= 'Download At: ';
        final String DocumentId= 'Document ID: ';
        system.debug('currentDocumentId***********'+currentDocumentId);
        system.debug('docIdObjectMap***********'+docIdObjectMap);
        List<Contact>conList=new List<Contact>();
        try{
          conList = [Select Name from Contact where id=:loggedInContactId AND (NOT Name  like 'Test%') AND (NOT Name like '%support%')];

             String eventType ='Document Downloaded'; 
             if(conList != null && conList.size() > 0)
             { 
                 ClaimDocumentResponseWrapper.ObjectProperty objProperties = docIdObjectMap.get(currentDocumentId).objectProperties;
                 system.debug('objProperties*******'+objProperties);
                 string pd_docCertype = objProperties.pd_payclddocumenttype;
                system.debug('pd_docCertype *******'+pd_docCertype );
                 if(hasMultipleClients)//4302 replaced userType = 'Broker'
                 {   
                 MyGardHelperCtrl.createClientEvents(UserInfo.getUserId(), selectLocalClient  , eventType, objProperties.objectName, null, objProperties.coverName, null, DocumentId+currentDocumentId+','+DocumentName+docIdObjectMap.get(currentDocumentId).name+','+DocumentType+pd_docCertype+','+DownloadAt+dateTime.now());  
                }
                else{
                MyGardHelperCtrl.createClientEvents(UserInfo.getUserId(), loggedInAccountId, eventType, objProperties.objectName, null, objProperties.coverName, null, DocumentId+currentDocumentId+','+DocumentName+docIdObjectMap.get(currentDocumentId).name+','+DocumentType+pd_docCertype+','+DownloadAt+dateTime.now());    
              }
        }}
         
      catch(Exception ex){
       Logger__c logMessage=new Logger__c(  Log_Description__c=ex.getMessage()+ ': ' + ex.getLineNumber());
       insert logMessage;
        
      }
    }
      public static void createFavourites(String newFilterName ){
        
        /*String currentUserId = UserInfo.getUserId();
        List<Extranet_Favourite__c> listExisting = new List<Extranet_Favourite__c>();
        String pageName = FavouritesCtrl.fetchPageName(ApexPages.CurrentPage().getUrl());                       
        listExisting.addAll(FavouritesCtrl.fetchExistingFavsList(pageName));
        String itemType = 'Saved Search';
        String query = queryString + ' limit 9000';
        system.debug('queryString : '+query);
        if(newFilterName!=null && newFilterName!=''){
            Extranet_Favourite__c objFav = new Extranet_Favourite__c();
            //added For Global Client - Start
            List<String> listClientIds = new List<String>();
            if(lstGlobalClientIds!=null && lstGlobalClientIds.size()>0){                    
                listClientIds.addAll(lstGlobalClientIds);                   
            }
            //added For Global Client - End 
            objFav = FavouritesCtrl.createFavs(objFav, pageName, newFilterName,itemType, currentUserId, query, listExisting);
            objFav.Filter_Client__c = selectedClient1;
            objFav.Filter_Policy_Year__c = selectedPolicyYr1;
            system.debug('checkbox : '+ checkbox);
            objFav.Filter_Document_Type__c = FavouritesCtrl.generateFavFilters(selectedAllDocType);
            system.debug('objFav : '+ system.JSON.serialize(objFav));
            insert objFav;
            system.debug('objFav ID : '+ objFav.Id);
            favouriteId = objFav.Id;
        }*/
      
    }  
}