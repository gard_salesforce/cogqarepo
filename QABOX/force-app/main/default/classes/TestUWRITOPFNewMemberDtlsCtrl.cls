@isTest(seeAllData=false)
public class TestUWRITOPFNewMemberDtlsCtrl
{ 
            Public static testmethod void TestUWRITOPFNewMemberDtlsCtrl(){
            
            GardTestData test_rec=new GardTestData();
            test_rec.commonrecord();
            test_rec.customsettings_rec();
            //Invoking methods of UWRITOPFNewMemberDtlsCtrl for broker users...
            
            System.runAs(GardTestData.brokerUser)         
            {
                test.startTest();
                trading_certificates__c tcCS = new trading_certificates__c();tcCS.name = 'testReciever';tcCS.value__c = 'ownergard@gmail.com'; insert tcCS;//sfedit remove
                UWRITOPFNewMemberDtlsCtrl test_broker= new UWRITOPFNewMemberDtlsCtrl();
                test_broker.lstContact = new List<Contact>();
                test_broker.noImoNo = true;
                test_broker.lstObject=new List<Object__c>();
                test_broker.lstAsset=new List<Asset>();
                test_broker.ast=GardTestData.brokerAsset_1st;
                test_broker.confirmationCheckbox=true;
                test_broker.IMONotKnown=true;
                test_broker.selectedObj=GardTestData.test_object_1st.id; 
                
                List<SelectOption> clientOptions =  test_broker.getClientLst();
                List<SelectOption> flag=test_broker.getFlagList();
                List<SelectOption> port=test_broker.getPortList();
                //List<SelectOption> classification=test_broker.getClassificationList();
                List<SelectOption> classlist = test_broker.getclassS();
                system.assertequals(clientOptions.size()>0,true,true);
                system.assertequals(flag.size()>0,true,true);
                system.assertequals(port.size()>0,true,true);
                test_broker.assetFinal = null;
                test_broker.selectedClient = GardTestData.clientAcc.id;
                test_broker.ObjName = string.valueof(GardTestData.test_object_1st.Imo_Lloyds_No__c); 
                test_broker.FindBy='imo';          
                test_broker.searchByImoObjectName();
                test_broker.selectedClient = GardTestData.clientAcc.id;
                test_broker.assetFinal = null;
                test_broker.selectedClient = GardTestData.clientAcc.id;
                test_broker.ObjName = string.valueof(GardTestData.test_object_1st.name); 
                test_broker.FindBy='objname';
                test_broker.searchByImoObjectName();
                //test_broker.ObjName = string.valueof(test_object_1.Imo_Lloyds_No__c); 
                //test_broker.FindBy='imo';          
                //test_broker.searchByImoObjectName();
                //test_broker.ObjName = string.valueof(test_object_1.name); 
                //test_broker.FindBy='objname';
                //test_broker.searchByImoObjectName();
                test_broker.dateOutput='05.02.2015';
                test_broker.dateOutputForReview='05.02.2015';
                test_broker.setObjectSelection();
                test_broker.brokersClientDtls();
                test_broker.chkAccess();
               // test_broker.saveRecord();
                test_broker.clearRecord();
                test_broker.goNext();
                test_broker.goPrev();
                test_broker.saveRecordNSubmit();
                test_broker.getCName();
                test_broker.sendToForm();
                //test_broker.sendAckMail();
                //test_broker.sendNotiMail();
                test_broker.uwf.id=GardTestData.brokerCase.id;
               // test_broker.saveRecord();
                test_broker.saveRecordNSubmit();
                test_broker.strSelected=GardTestData.brokerUser.id+';'+GardTestData.brokerUser.id;
                test_broker.fetchSelectedClients();
                test_broker.strGlobalClient=GardTestData.brokerUser.id+';'+GardTestData.brokerUser.id;
                test_broker.fetchGlobalClients();
                
                test_broker.loggedInAccountId = GardTestData.brokerAcc.Id;
                test_broker.saveRecord();
                test_broker.setGlobalClientIds.clear();
                test_broker.selectedGlobalClient.clear();
                List<SelectOption> clientOptions2 =  test_broker.getClientLst();
                test.stopTest();
            }
       }
  }