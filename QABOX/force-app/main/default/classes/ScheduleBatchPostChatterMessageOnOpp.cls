/***************************************************
* Class Name: ScheduleBatchPostChatterMessageOnOpp
* Created By: Nagarjuna Kaipu
* Date: 12-04-2021
* Description: SF-6270 - Used to schedule BatchUpdateDocumentIdOnFiles to get and update document id on files
***************************************************/
global class ScheduleBatchPostChatterMessageOnOpp implements Schedulable {
    global void execute(SchedulableContext sc) {
        BatchPostChatterMessageOnOpportunity batchPostChatter = new BatchPostChatterMessageOnOpportunity();
        Database.executeBatch(batchPostChatter);
    }
}