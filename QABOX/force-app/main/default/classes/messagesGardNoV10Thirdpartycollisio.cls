//Generated by wsdl2apex

public class messagesGardNoV10Thirdpartycollisio {
    public class ThirdPartyCollisionClaimDetailsBase {
        public messagesGardNoV10Shipoperation.ShipOperationBase ShipOperation;
        public String ShipOperationDescription;
        public messagesGardNoV10Shipoperation.ShipOperationBase OtherShipOperation;
        public String OtherShipOperationDescription;
        public DateTime Time_x;
        public String Light;
        public String Visibility;
        public Boolean RadarNavigation;
        public String InitialContactPint;
        public String ShipManouver;
        public String ShipManouverDescription;
        public String OtherShipManouver;
        public String OtherShipManouverDescription;
        public String ConsequenceEnteredShip;
        public String ConsequenceOtherShip;
        public String OtherShipName;
        public String DetailsDefenceSpecification;
        public Boolean ExternalLawyers;
        public Boolean PreferredLawyers;
        public String OnBridge;
        private String[] ShipOperation_type_info = new String[]{'ShipOperation','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] ShipOperationDescription_type_info = new String[]{'ShipOperationDescription','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] OtherShipOperation_type_info = new String[]{'OtherShipOperation','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] OtherShipOperationDescription_type_info = new String[]{'OtherShipOperationDescription','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] Time_x_type_info = new String[]{'Time','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] Light_type_info = new String[]{'Light','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] Visibility_type_info = new String[]{'Visibility','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] RadarNavigation_type_info = new String[]{'RadarNavigation','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] InitialContactPint_type_info = new String[]{'InitialContactPint','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] ShipManouver_type_info = new String[]{'ShipManouver','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] ShipManouverDescription_type_info = new String[]{'ShipManouverDescription','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] OtherShipManouver_type_info = new String[]{'OtherShipManouver','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] OtherShipManouverDescription_type_info = new String[]{'OtherShipManouverDescription','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] ConsequenceEnteredShip_type_info = new String[]{'ConsequenceEnteredShip','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] ConsequenceOtherShip_type_info = new String[]{'ConsequenceOtherShip','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] OtherShipName_type_info = new String[]{'OtherShipName','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] DetailsDefenceSpecification_type_info = new String[]{'DetailsDefenceSpecification','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] ExternalLawyers_type_info = new String[]{'ExternalLawyers','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] PreferredLawyers_type_info = new String[]{'PreferredLawyers','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] OnBridge_type_info = new String[]{'OnBridge','http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://messages.gard.no/v1_0/ThirdPartyCollisionClaimDetails','true','false'};
        private String[] field_order_type_info = new String[]{'ShipOperation','ShipOperationDescription','OtherShipOperation','OtherShipOperationDescription','Time_x','Light','Visibility','RadarNavigation','InitialContactPint','ShipManouver','ShipManouverDescription','OtherShipManouver','OtherShipManouverDescription','ConsequenceEnteredShip','ConsequenceOtherShip','OtherShipName','DetailsDefenceSpecification','ExternalLawyers','PreferredLawyers','OnBridge'};
    }
}