@isTest
private class TestUserTriggerToUpdateAccount {
	private static Account anAccount;
	private static Contact contact1;
	private static Contact contact2;
	private static Contact contact3;
	private static List<Contact> allContacts;
	private static User myGardUser;
	private static User externalUser1;
	private static User externalUser2;
	private static String partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
	private static String orgId = UserInfo.getOrganizationId();
    private static String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
	private static void setUpAccount(){
       	anAccount = new Account(  
		            Name = 'Test Account',
		            BillingCity = 'Bristol',
		            BillingCountry = 'United Kingdom',
		            BillingPostalCode = 'BS1 1AD',
		            BillingState = 'Avon' ,
		            BillingStreet = '1 Elmgrove Road',
		            Company_Role__c = 'Client',
		            Area_Manager__c = UserInfo.getUserId()
        );
        insert anAccount;
	}
	
	private static void setupContacts(){
		allContacts = new List<Contact>();
		contact1 = new Contact(
                  FirstName='Test',
                  LastName='User1',
                  MailingCity = 'London',
                  MailingCountry = 'United Kingdom',
                  MailingPostalCode = 'SE1 1AE',
                  MailingState = 'London',
                  MailingStreet = '4 London Road',
                  AccountId = anAccount.Id,
                  Email = 'test123@gmail.com'
                   );
        allContacts.add(contact1);
       	contact2 = new Contact(
                  FirstName='Test',
                  LastName='User2',
                  MailingCity = 'London',
                  MailingCountry = 'United Kingdom',
                  MailingPostalCode = 'SE1 1AE',
                  MailingState = 'London',
                  MailingStreet = '4 London Road',
                  AccountId = anAccount.Id,
                  Email = 'test345@gmail.com'
                   );
        allContacts.add(contact2);
        contact3 = new Contact(
                  FirstName='Test',
                  LastName='User3',
                  MailingCity = 'London',
                  MailingCountry = 'United Kingdom',
                  MailingPostalCode = 'SE1 1AE',
                  MailingState = 'London',
                  MailingStreet = '4 London Road',
                  AccountId = anAccount.Id,
                  Email = 'test567@gmail.com'
                   );
        allContacts.add(contact3);
        insert allContacts;
	}
	
	private static void setUpSupportUsers(Boolean isActiveSupportUser){
		Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
    	String uniqueName = orgId + dateString + randomInt + '.mygard';
		
		myGardUser = new User(
                        Alias = uniqueName.substring(18, 23), 
                        profileId = partnerLicenseId ,
                        Email = uniqueName + '@test' + orgId + '.org',
                        EmailEncodingKey='UTF-8',
                        CommunityNickname = 'myGardTest1',
                        FirstName ='MyGard',
                        LastName='Test',
                        LanguageLocaleKey='en_US',
                        LocaleSidKey='en_US',  
                        TimeZoneSidKey='America/Los_Angeles',
                        Username = uniqueName + '@test' + orgId + '.org.mygard',
                        IsActive = isActiveSupportUser,
                        Contactid=contact1.id
                        );
        insert myGardUser;
	}
	
	private static void setUpPortalUsers(Boolean isActivePortalUser){
		Integer randomInt1 = Integer.valueOf(math.rint(math.random()*1000000));
    	String uniqueName1 = orgId + dateString + randomInt1 + '.mygard';
		
		externalUser1 = new User(
                        Alias = uniqueName1.substring(18, 23),
                        profileId = partnerLicenseId ,
                        Email = uniqueName1 + '@test' + orgId + '.org',
                        EmailEncodingKey='UTF-8',
                        CommunityNickname = 'extTest1',
                        FirstName ='FirstTestName',
                        LastName='Test',
                        LanguageLocaleKey='en_US',
                        LocaleSidKey='en_US',  
                        TimeZoneSidKey='America/Los_Angeles',
                        Username = uniqueName1 + '@test' + orgId + '.org.mygard',
                        IsActive = isActivePortalUser,
                        Contactid=contact2.id
                        );
        
    	insert externalUser1;
	}
	
	private static void setupTestDataWithUsers(Boolean isAnySupportUser, Boolean isActiveSupportUser, Boolean isAnyPortalUser, Boolean isActivePortalUser){
		TestUserTriggerToUpdateAccount.setUpAccount();
		TestUserTriggerToUpdateAccount.setupContacts();
		if(isAnySupportUser){
			TestUserTriggerToUpdateAccount.setUpSupportUsers(isActiveSupportUser);
		}
		if(isAnyPortalUser){
			TestUserTriggerToUpdateAccount.setUpPortalUsers(isActivePortalUser);
		}
	}
	
	static testMethod void testBehaviourWithNoUsersAndThreeContacts(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
		Test.startTest();
		TestUserTriggerToUpdateAccount.setupTestDataWithUsers(false,false,false,false);
		Test.stopTest();
		
		Account fetchedAccount = [SELECT MyGard_enabled__c FROM ACCOUNT WHERE Name = 'Test Account'];
		System.assertEquals(false, fetchedAccount.MyGard_enabled__c, false);
	}
    
    static testMethod void testBehaviourWithOneSupportUserAndThreeContacts(){
       conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
    	Test.startTest();
		TestUserTriggerToUpdateAccount.setupTestDataWithUsers(true,true,false,false);
		Test.stopTest();
		
		Account fetchedAccount = [SELECT MyGard_enabled__c FROM ACCOUNT WHERE Name = 'Test Account'];
		System.assertEquals(false, fetchedAccount.MyGard_enabled__c, false);
    }
    
    static testMethod void testBehaviourWithSupportAndPortalUser(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
    	Test.startTest();
		TestUserTriggerToUpdateAccount.setupTestDataWithUsers(true,true,true,true);
		Test.stopTest();
		
		Account fetchedAccount = [SELECT MyGard_enabled__c FROM ACCOUNT WHERE Name = 'Test Account'];
		//System.assertEquals(true, fetchedAccount.MyGard_enabled__c, true);
    }
    
    static testMethod void testBehaviourWithInActiveSupportAndActivePortalUser(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
    	Test.startTest();
		TestUserTriggerToUpdateAccount.setupTestDataWithUsers(true,false,true,true);
		Test.stopTest();
		
		Account fetchedAccount = [SELECT MyGard_enabled__c FROM ACCOUNT WHERE Name = 'Test Account'];
		//System.assertEquals(true, fetchedAccount.MyGard_enabled__c, true);
    }
    
    static testMethod void testBehaviourWithActiveSupportAndInActivePortalUser(){
       conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
    	Test.startTest();
		TestUserTriggerToUpdateAccount.setupTestDataWithUsers(true,true,true,false);
		Test.stopTest();
		
		Account fetchedAccount = [SELECT MyGard_enabled__c FROM ACCOUNT WHERE Name = 'Test Account'];
		System.assertEquals(false, fetchedAccount.MyGard_enabled__c, false);
    }
}