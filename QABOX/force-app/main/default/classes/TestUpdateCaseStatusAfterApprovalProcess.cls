@istest
public class TestUpdateCaseStatusAfterApprovalProcess 
{ 
    @istest
    static void test()
    {
        Account testAccount= new Account();    
        testAccount.name='test';
        insert testAccount;
        
        Case testcase= new case();
        testcase.Status='new';
        testcase.Origin='phone';
        insert testcase;
        
        List<Account> allaccounts=[select id,StoreCase_Id__c from Account where id=:testAccount.id];
        list<ID> accountIDS=new list<ID>();
        for(Account acc:allaccounts)
        {
            accountIDS.add(acc.id);
        }
        List<Case> case_status=[select id,Status from Case where id=:testcase.Id];
        UpdateCaseStatusAfterApprovalProcess.UpdateCaseStatus(accountIDS);
        
    }
}