@isTest(seeAllData = false)
public class TestPageFrame2Client { 
    Public static attachment attach , attachmnt ;  
    Public static list<attachment> attachmentLst = new list<attachment>();   
    Public static List<Account> AccountList=new List<Account>();
    Public static List<Contact> ContactList=new List<Contact>();
    Public static List<Contract> ContractList=new List<Contract>();
    Public static List<User> UserList=new List<User>();
    Public static List<Asset> AssetList=new List<Asset>();
    Public static List<Case> caseList =new List<Case>();
    Public static List<Extranet_Favourite__c> listfav1 = new List<Extranet_Favourite__c>();
    Public static List<Account_Contact_Mapping__c> Accmap_list= new List<Account_Contact_Mapping__c>();
    Public static accounttocontactmap__c map_broker, map_client;
    public static Group testGroup;
    public static QueueSobject testQueue;
    public static Gard_Contacts__c gard_contact_1;
    public static Market_Area__c Markt;
    public static User salesforceLicUser ,brokerUser ,clientUser ;
    public static Account dummyAcc,brokerDummyAcc,brokerAcc ,clientAcc ;
    public static Contact dummyContact ,brokerDummyContact,brokerContact ,clientContact ;
    public static Extranet_Global_Client__c global_client;
    public static Account_Contact_Mapping__c Accmap_1st,Accmap_2nd ;
    public static Contract contract;
    public static asset brokerAsset , clientAsset;
    public static contact_share__c contactShrTest;
    public static List<Claim_Types_Master_List__c> claimTypesLst;
    public static List<AccountToContactMap__c> acmLst;
    ///////*******Variables for removing common literals********************//////////////
    public static string utfStr =  'UTF-8';
    public static string enUsrStr =  'en_US';
    public static string tymZnStr =   'America/Los_Angeles';
    public static string bilCntryStr =   'United Kingdom'  ;
    public static string postlCdStr =   'BS1 1AD';
    public static string typeBrStr =   'Broker';
    public static string mailngStateStr =   'London'  ;
    /////**********************end**************************/////
    @future
    public static void gettemplate()
    {
       List<EmailTemplate> emailtemLst = new List<EmailTemplate>();
       EmailTemplate x1= new EmailTemplate( name='MyGard-Contact Me-Acknowledgement', body = '[HEADER] Thank you for contacting Gard. Your request for Gard to contact you has been registered. Below is a summary of the details submitted on MyGard:    Area: [AREA]    Client: [CLIENT]    Cover: [COVER]  Claim type: [CLAIMTYPE] Subject: [SUBJECT]  Preferred method of contact:    Phone: [PHONE]  Email: [EMAIL]  [SIGNATURE] [FOOTER]');
       emailtemLst.add(x1);
       x1 = new EmailTemplate( name='MyGard-Extranet Feedback-Acknowledgement', body = '[HEADER] Thank you for contacting Gard. Your feedback has been received. Below is a summary of the details submitted on MyGard:  Key feedback issue/area: [AREA] Comments: [COMMENTS]    Main reason for visiting MyGard today: [REASON] Overall site rating: [SITE_RATING]  Objective accomplished through MyGard: [OBJECTIVES_ACCOMPLISHED]    Any other comments: [OTHER_COMMENTS]    Preferred method of contact:    Phone: [PREF_PHONE] Email: [PREF_EMAIL] [SIGNATURE] [FOOTER]');
       emailtemLst.add(x1);
       insert emailtemLst;
    }   
    
    public static void createtestDataGTD(){
        GardTestData test_data = new GardTestData();      
        test_data.commonRecord();
        salesforceLicUser = GardTestData.salesforceLicUser;
            brokerUser = GardTestData.brokerUser;
            clientUser =  GardTestData.clientUser;
            brokerAcc = GardTestData.brokerAcc;
            clientAcc = GardTestData.clientAcc;
            Accmap_1st = GardTestData.Accmap_1st;
            Accmap_2nd = GardTestData.Accmap_2nd;
            brokerContact = GardTestData.brokerContact;
            clientContact = GardTestData.clientContact;
            brokerAsset = GardTestData.brokerAsset_1st;
            clientAsset = GardTestData.ClientAsset_1st;
            contract = GardTestData.brokercontract_1st;
        	gard_contact_1 = GardTestData.grdobj;
    }
    
   public static void createTestData()
    { 
         AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
       insert numberofusers;
        GardTestData test_data=new GardTestData();      
        test_data.commonRecord();
        
        string partnerLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Partner Community' and name='Partner Community Login User Custom' limit 1].id;
        string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
        //String GardStandLicenseId = [Select id from profile Where Name ='Gard Standard User (UWR)' limit 1].id;
        testGroup = new Group(Name = 'MyGard_Gard_Administrator', Type = 'Queue');
        //insert testGroup;

        testQueue = new QueueSObject(QueueId = testGroup.Id, SobjectType = 'Case');
        //insert testQueue;
             
         gard_contact_1= new Gard_Contacts__c(LastName__c='Sengupta');
         insert gard_contact_1;
         system.assertequals(gard_contact_1.LastName__c,'Sengupta',true);
         
        Markt = new Market_Area__c(Market_Area_Code__c = 'ab120120');                                                    
        insert Markt;       
       
        /*User gardStandardUser = new User(
                                            Alias = 'standt', 
                                            contactId__c = gard_contact_1.id,
                                            profileId = GardStandLicenseId ,
                                            Email='standarduse11r@testorg.com',
                                            EmailEncodingKey= utfStr ,
                                            CommunityNickname = 'test13123',
                                            LastName='Testing',
                                            LanguageLocaleKey= enUsrStr ,
                                            LocaleSidKey= enUsrStr ,  
                                            TimeZoneSidKey= tymZnStr ,
                                            UserName='test01108@testorg.com.mygard'                                      
                                         );
        insert gardStandardUser; */
        salesforceLicUser = new User(
                                            Alias = 'standt', 
                                            contactId__c = gard_contact_1.id,
                                            profileId = salesforceLicenseId ,
                                            Email='standarduser@testorg.com',
                                            EmailEncodingKey= utfStr ,
                                            CommunityNickname = 'tessqwt13',
                                            LastName='Testing',
                                            LanguageLocaleKey= enUsrStr ,
                                            LocaleSidKey= enUsrStr ,  
                                            TimeZoneSidKey= tymZnStr ,
                                            UserName='test008@testorg.com.mygard'                                      
                                         );
        insert salesforceLicUser;
       
        // The dummyUser is for salesforceLicUser's FederationIdentifier field.
        dummyAcc = new Account(  Name='testree',
                                        BillingCity = 'Bristol',
                                        BillingCountry =  bilCntryStr ,
                                        BillingPostalCode =  postlCdStr ,
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        Site = '_www.cts.se',
                                        Type =  typeBrStr ,
                                        Market_Area__c = Markt.id,
                                        Confirm_not_on_sanction_lists__c = true,
                                        License_description__c  = 'some desc',
                                        Description = 'some desc',
                                        Licensed__c = 'Pending',
                                        Area_Manager__c = salesforceLicUser.id                      
                                     );
        insert dummyAcc;                       
                                 
        dummyContact = new Contact( 
                                             FirstName='Raan',
                                             LastName='Baan',
                                             MailingCity =  mailngStateStr ,
                                             MailingCountry =  bilCntryStr ,
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState =  mailngStateStr ,
                                             MailingStreet = '1 London Road',
                                             AccountId = dummyAcc.Id,
                                             Email = 'mtyt@gmail.com'
                                          );                                         
        //insert dummyContact;                
        
                
        brokerDummyAcc = new Account(   Name='testree1',
                                                BillingCity = 'Bristol',
                                                BillingCountry =  bilCntryStr ,
                                                BillingPostalCode =  postlCdStr ,
                                                BillingState = 'Avon' ,
                                                BillingStreet = '1 Elmgrove Road',
                                                Site = '_www.cts.se',
                                                Type =  typeBrStr ,
                                                Market_Area__c = Markt.id,
                                                Confirm_not_on_sanction_lists__c = true,
                                                License_description__c  = 'some desc',
                                                Description = 'some desc',
                                                Licensed__c = 'Pending',
                                                Area_Manager__c = salesforceLicUser.id                       
                                            );
        insert brokerDummyAcc;                        
                                 
        brokerDummyContact = new Contact( 
                                                 FirstName='Raan',
                                                 LastName='Baan',
                                                 MailingCity =  mailngStateStr ,
                                                 MailingCountry =  bilCntryStr ,
                                                 MailingPostalCode = 'SE1 1AD',
                                                 MailingState =  mailngStateStr ,
                                                 MailingStreet = '1 London Road',
                                                 AccountId = brokerDummyAcc.Id,
                                                 Email = 'mtytd.tyt@gmail.com'
                                               );                                         
       // insert brokerDummyContact;                
       
       //Accounts............
       brokerAcc = new Account(  Name='testreNewGard',
                                        BillingCity = 'Bristol',
                                        BillingCountry =  bilCntryStr ,
                                        BillingPostalCode =  postlCdStr ,
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.Broker_Contact_Record_Type,
                                       Site = '_www.cts.se',
                                        Type =  typeBrStr ,
                                        Company_role__c= typeBrStr ,
                                        Main_Claims_Handler__c = salesforceLicUser.id,
                                        Key_Claims_Contact__c = salesforceLicUser.id,
                                        Claim_handler_Cargo_Dry__c = salesforceLicUser.id,
                                        Claim_handler_Cargo_Liquid__c = salesforceLicUser.id,
                                        Claim_handler_CEP__c = salesforceLicUser.id,
                                        Claim_handler_Energy__c = salesforceLicUser.id,
                                        Claims_handler_Builders_Risk__c = salesforceLicUser.id,
                                        Claim_handler_Defence__c = salesforceLicUser.id,
                                        Claim_handler_Marine__c = salesforceLicUser.id,
                                        Claim_handler_Crew__c= salesforceLicUser.id,
                                        Claim_Adjuster_Marine_lk__c= salesforceLicUser.id,
                                        Market_Area__c = Markt.id,
                                        Area_Manager__c =  salesforceLicUser.id,
                                        Accounting_P_I__c = salesforceLicUser.id,
                                        Confirm_not_on_sanction_lists__c = true,
                                        License_description__c  = 'some desc',
                                        Description = 'some desc',
                                        Licensed__c = 'Pending',
                                        Accounting__c = salesforceLicUser.id
                                        //OwnerId = gardStandardUser.id
                                     );
       AccountList.add(brokerAcc);
    
      
       clientAcc = new Account(Name = 'GardTest_1', 
                                        Site = '_www.test_1.se', 
                                        Type = 'Client', 
                                        BillingStreet = 'Gatan 1',
                                        BillingCity = 'Stockholm', 
                                        BillingCountry = 'SWE', 
                                        BillingPostalCode =  postlCdStr ,
                                        Company_role__c='Client',
                                       recordTypeId=System.Label.Client_Contact_Record_Type,                                        
                                        Area_Manager__c = salesforceLicUser.id,
                                        Main_Claims_Handler__c = salesforceLicUser.id,
                                        Key_Claims_Contact__c = salesforceLicUser.id,
                                        Claim_handler_Cargo_Dry__c = salesforceLicUser.id,
                                        Claim_handler_Cargo_Liquid__c = salesforceLicUser.id,
                                        Claim_handler_CEP__c = salesforceLicUser.id,
                                        Claim_handler_Energy__c = salesforceLicUser.id,
                                        Claims_handler_Builders_Risk__c = salesforceLicUser.id,
                                        Claim_handler_Defence__c = salesforceLicUser.id,
                                        Claim_handler_Marine__c = salesforceLicUser.id,
                                        Claim_handler_Crew__c= salesforceLicUser.id,
                                        Claim_Adjuster_Marine_lk__c= salesforceLicUser.id,
                                        Market_Area__c = Markt.id,                                        
                                        Accounting_P_I__c = salesforceLicUser.id, 
                                        Accounting__c = salesforceLicUser.id
                                        //OwnerId = gardStandardUser.id  
                                      );
                                    
        AccountList.add(clientAcc);           
        
        insert AccountList ;
        
        //Contacts.................
        brokerContact = new Contact( 
                                             FirstName='Raan',
                                             LastName='Baan',
                                             MailingCity =  mailngStateStr ,
                                             MailingCountry =  bilCntryStr ,
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState =  mailngStateStr ,
                                             MailingStreet = '1 London Road',
                                             AccountId = brokerAcc.Id,
                                             Email = 'mdjtuy@gmail.com'
                                           );
        ContactList.add(brokerContact);

        clientContact= new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity =  mailngStateStr ,
                                              MailingCountry =  bilCntryStr ,
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState =  mailngStateStr ,
                                              MailingStreet = '4 London Road',
                                              AccountId = clientAcc.Id,
                                              Email = 'mdjytut@gmail.com'
                                          );
        ContactList.add(clientContact);
        insert ContactList;
        //Test.startTest();
       
        // custom setting for account-contact 
        acmLst = new List<AccountToContactMap__c>();
        map_broker = new AccountToContactMap__c(AccountId__c=brokerAcc.id,Name=brokerContact.id);
        acmLst.add(map_broker);
        map_client = new AccountToContactMap__c(AccountId__c= clientAcc.id,Name=clientContact.id);
        acmLst.add(map_client);
        Insert acmLst;         
         
        Accmap_1st = new Account_Contact_Mapping__c(Account__c = brokerAcc.id,
                                                                               Active__c = true,
                                                                               Administrator__c = 'Admin',
                                                                               Contact__c = brokerContact.id,
                                                                               IsPeopleClaimUser__c = true,
                                                                               Marine_access__c = true,
                                                                               PI_Access__c = true
                                                                               //Show_Claims__c = false,
                                                                               //Show_Portfolio__c = false 
                                                                               );
          
        Accmap_2nd = new Account_Contact_Mapping__c(Account__c =  clientAcc.id,
                                                                               Active__c = true,
                                                                               Administrator__c = 'Admin',
                                                                               Contact__c = clientContact.id,
                                                                               IsPeopleClaimUser__c = true,
                                                                               Marine_access__c = true,
                                                                               PI_Access__c = true
                                                                               //Show_Claims__c = false,
                                                                               //Show_Portfolio__c = false 
                                                                               );                                                                                
        Accmap_list.add(Accmap_1st);
        Accmap_list.add(Accmap_2nd);        
        Insert Accmap_list;
         
        //Users..................
        brokerUser = new User(
                                    Alias = 'standt', 
                                    profileId = partnerLicenseId,
                                    Email='mrtrt@gmail.com',
                                    EmailEncodingKey= utfStr ,
                                    LastName='estTing',
                                    LanguageLocaleKey= enUsrStr ,
                                    CommunityNickname = 'brokerUser_4646',
                                    LocaleSidKey= enUsrStr ,  
                                    TimeZoneSidKey= tymZnStr ,
                                    UserName='testbrokerUser@testorg.com.mygard',
                                    ContactId = BrokerContact.Id
                                   );
        UserList.add(brokerUser);
                                   
        clientUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'clientUser_4545',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey= utfStr ,
                                    LastName='tetst_006',
                                    LanguageLocaleKey= enUsrStr ,
                                    LocaleSidKey= enUsrStr ,
                                    TimeZoneSidKey= tymZnStr ,
                                    UserName='testclientUser@testorg.com.mygard',
                                    ContactId = clientContact.id
                                   );
                
         UserList.add(clientUser);       
         insert UserList;   
    
         contract = New Contract(Accountid = brokerAcc.id, 
                                        Account = brokerAcc, 
                                        Status = 'Draft',
                                        CurrencyIsoCode = 'SEK', 
                                        StartDate = Date.today(),
                                        ContractTerm = 2, 
                                        Broker__c = brokerAcc.id,
                                        Client__c = clientAcc.id,
                                        Broker_Name__c = brokerAcc.id,
                                        Contracting_Party__c = brokerDummyAcc.id
                                        );         
         ContractList.add(contract);
         insert ContractList ;        
          
         Object__c obj = New Object__c( Dead_Weight__c= 20, Object_Unique_ID__c = '56456465');
         insert obj ;
         
         brokerAsset = new Asset(Name= 'Asset 1',
                                 accountid =brokerAcc.id,
                                 contactid = BrokerContact.Id,  
                                 Agreement__c = contract.id, 
                                 CurrencyIsoCode='USD',
                                 product_name__c = 'Crew Cover',                                 
                                 Object__c = obj.id,
                                 Underwriter__c = brokerUser.id,
                                 Expiration_Date__c = date.valueof('2015-01-01'),
                                 Inception_date__c = date.valueof('2012-01-01'),
                                 Risk_ID__c = '564562'
                                 ); 
         AssetList.add(brokerAsset);
         
         clientAsset = new Asset(Name= 'Asset 1',
                                 accountid =brokerAcc.id,
                                 contactid = clientContact.Id,  
                                 Agreement__c = contract.id, 
                                 CurrencyIsoCode='USD',
                                 product_name__c = 'War',                                 
                                 Object__c = obj.id,
                                 Underwriter__c = clientUser.id,
                                 Expiration_Date__c = date.valueof('2015-01-01'),
                                 Inception_date__c = date.valueof('2012-01-01'),
                                 Risk_ID__c = '56456'
                                 ); 
         AssetList.add(clientAsset);         
         insert AssetList;
         
         /*Case brokerCase = new Case(Accountid = brokerAcc.id, 
                                    ContactId = brokerContact.id, 
                                    Claim_Incurred_USD__c= 40000, 
                                    Claim_Reference_Number__c='123ert',                                    
                                    Object__c= obj.id,
                                    Risk_Coverage__c = brokerAsset.id,                                    
                                    Contract_for_Review__c = contract.id
                                   );
        caseList.add(brokerCase);
        
        Case clientCase = new Case(Accountid = clientAcc.id, 
                                   ContactId = clientContact.id,
                                   Claim_Incurred_USD__c= 40000, 
                                   Claim_Reference_Number__c='123ert',
                                   Object__c= obj.id,
                                   Risk_Coverage__c = clientAsset.id,                                   
                                   Contract_for_Review__c = contract.id                                   
                                  );
        caseList.add(clientCase); 
        insert caseList;*/
               
         claimTypesLst = new List<Claim_Types_Master_List__c>();              
        claimTypesLst.add(new Claim_Types_Master_List__c(name = 'TestTing1',Value__c ='cargo', middleware_value__c = 'cargo'));       
        claimTypesLst.add(new Claim_Types_Master_List__c(name = 'TestTing2',Value__c ='Defence', middleware_value__c = 'Defence'));        
        insert claimTypesLst;       
        
        List<Business_Area_Coverage_Mapping__c> bacMappingLst = new List<Business_Area_Coverage_Mapping__c>();
        
        bacMappingLst.add(new Business_Area_Coverage_Mapping__c(name = 'sdf',
                                                                Coverage_Description__c ='Delay Insurance WAR' ,
                                                                Coverage_Code__c ='WG',
                                                                Business_Area_Code__c = 'MA' ));       
        bacMappingLst.add(new Business_Area_Coverage_Mapping__c(name ='abc',
                                                                Coverage_Description__c ='Crew Cover' ,
                                                                Coverage_Code__c ='CR',
                                                                Business_Area_Code__c = 'PI' ));        
        insert bacMappingLst;        

        //brokerAcc.Area_Manager__c =  salesforceLicUser.id;  // coomented on 23.9     
        //update brokerAcc;  // coomented on 23.9  
        
        global_client=new Extranet_Global_Client__c(Selected_Global_Clients__c=clientAcc.id,Selected_By__c=clientUser.id);
        insert global_client;
         
        contactShrTest = new contact_share__c();
        contactShrTest.Broker_share_id__c = GardTestData.brokerShr1.id;
        contactShrTest.ContactId__c = brokerUser.id;

        insert contactShrTest; 
    }
    
    public static testMethod void cover2()
    { 
        //createTestData();  
        Test.startTest();
        createtestDataGTD();
        Test.stopTest();
        System.runAs(clientUser)
        {
            PageFrameCtrl test_client= new PageFrameCtrl();
            test_client.total_contract_id_set = new Set<String>();
            test_client.total_contract_id_set.add(contract.ID);
            //test_client.selectedClientId = clientAcc.ID;
           // test_client.getcoverList();
            
        }  
         //Test.stoptest();
    }
}