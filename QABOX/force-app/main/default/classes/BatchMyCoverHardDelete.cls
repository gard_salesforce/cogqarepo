global class BatchMyCoverHardDelete implements Database.Batchable<sObject>{ 
    global String coverQuery;  
    global BatchMyCoverHardDelete(){
        coverQuery = 'Select id from MyCoverList__c where IsDeleted = true ALL ROWS';
    }

    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(coverQuery);
    } 
    global void execute(Database.BatchableContext BC, list<sObject> scope){  
        system.debug('scope'+scope.size());   
        //delete scope;   
        DataBase.emptyRecycleBin(scope); 
    }

    global void finish(Database.BatchableContext BC){}
}