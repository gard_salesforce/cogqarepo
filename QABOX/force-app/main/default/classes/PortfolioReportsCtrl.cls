public without sharing class PortfolioReportsCtrl{

   /* public PageReference refreshpage { get; set; }
    public List<SelectOption> clientoptions{get;set;}
    public String SelectedClient {get;set;}
    
    public List<SelectOption> agreementTypeList{get;set;}
    public String SelectedAgreementType {get;set;}
    public Custom_Messages__c customMessage {get;set;}
    
    public List<SelectOption> coverGroupList{get;set;}
    public String SelectedCoverGroup {get;set;}
    public map<Id,String> mapClientId{get;set;}
    public Boolean hideSearch{get;set;}
    public String searchUrl{get;set;}
    public static String redirectUrl{get;set;}
    
    public boolean isBroker{get;set;}
    public string loggedInContactId;
    public String loggedInAccountId;
    public String loggedCompanyId;
    public String loggedContractId;
    public String userType{get;set;}
    public set<Id> clientIdSet{get;set;}
    public String clientId{get;set;}    
   
    public boolean showReport{get;set;}
    public String htmlRequest{get;set;}
    public String allowPeopleClaim{get;set;}
    public static boolean booleanPeopleClaim{get;set;}
    
    public String brokerIdToPass{get;set;}
    public Integer clientIdToPass{get;set;}  
    
    public PortfolioReportsCtrl(){
        system.debug('-----brokerIdToPass in Constructor -----'+brokerIdToPass);
        system.debug('-----clientIdToPass in Constructor -----'+clientIdToPass);
        getClientList();
        //getCoverGroupList();
        //getAgreementList();
        checkHideSearch();
        getCustomMessages();
    }
        
    public void getCustomMessages(){
        customMessage = Custom_Messages__c.getValues('PortfolioReport');
    }
    
    public static boolean isPeopleClaimUser(){
        boolean isPeopleClaimUser;
        List<User> lstUser = new List<User>([select id,Contactid from User where id=:UserInfo.getUserId()]);
        if(lstUser!=null && lstUser.size()>0 && lstUser[0].Contactid!=null){
            String loggedInCntId = lstUser[0].Contactid;
            List<Contact> lstContact = new List<Contact>([select IsPeopleLineUser__c  from Contact where id=:loggedInCntId]);
            if(lstContact[0]!= null && lstContact[0].IsPeopleLineUser__c){
                isPeopleClaimUser=true;
            }else{
                isPeopleClaimUser=false;
            }
        }
        return isPeopleClaimUser;
    }    

    public void getClientList(){
        List<User> lstUser = new List<User>([select id,Contactid from User where id=:UserInfo.getUserId()]);
        mapClientId = new map<Id,String>();
        clientIdSet = new Set<id>();
        clientoptions = new List<SelectOption>();
        clientoptions.add(new SelectOption('',''));
        if(lstUser!=null && lstUser.size()>0 && lstUser[0].Contactid!=null){
            loggedInContactId = lstUser[0].Contactid;
            loggedInAccountId = lstUser[0].id;
            System.debug('loggedInContactId ---->'+loggedInContactId);
            System.debug('loggedInAccountId ---->'+loggedInAccountId);
            List<Contact> lstContact = new List<Contact>([select account.id, account.recordtypeid,account.Company_Role__c,IsPeopleLineUser__c  from Contact where id=:loggedInContactId]);
          
           if(lstContact[0]!= null && lstContact[0].account.Company_Role__c != null &&  lstContact[0].account.Company_Role__c.contains('Broker')){
                loggedCompanyId = lstContact[0].account.id;
                userType = 'Broker';
                    isBroker = true;
                    if(lstContact[0].IsPeopleLineUser__c){
                        allowPeopleClaim ='Y';
                        booleanPeopleClaim=true;
                    }
                    else{
                        allowPeopleClaim ='N';
                        booleanPeopleClaim=false;
                    } 
                    
                    List<AggregateResult> contractList = new List<AggregateResult>([select Client__r.Name, Client__r.id from Contract where Broker__c =:lstContact[0].accountId and Business_Area__c='P&I' group by  Client__r.Name, Client__r.id order by Client__r.Name ]);
                    //clientoptions = new List<SelectOption>();  
                     system.debug('-----inside IF-agreement ----'+contractList );                 
                    for(AggregateResult agreement : contractList){
                     system.debug('-----inside IF-agreement ----'+agreement );
                        if(agreement.get('Id')!=null){
                            clientId = (String.valueOf(agreement.get('Id')) +'').substring(0,15);
                            clientIdSet.add(clientId);
                            clientoptions.add(new SelectOption(clientId,String.valueOf(agreement.get('Name'))));
                        }
                    }
                }else{
                    userType = 'Client';
                    isBroker = false;
                    if(lstContact[0].IsPeopleLineUser__c){
                        allowPeopleClaim ='Y';
                        booleanPeopleClaim=true;
                    }
                    else{
                        allowPeopleClaim ='N';
                        booleanPeopleClaim=false;
                    } 
                    system.debug('-----inside Else-----'+lstContact[0].accountId );
                    List<AggregateResult> contractList = new List<AggregateResult>([select Client__c,Client__r.Name  from Contract where Client__c =:lstContact[0].accountId and Business_Area__c='P&I' group by Client__c,Client__r.Name order by Client__r.Name asc ]); 
                    system.debug('-----inside Else-contractList ----'+contractList  );
                    for(AggregateResult agreement :contractList ){
                        system.debug('-----inside Else-agreement ----'+agreement );
                        if(agreement != null){
                        
                            system.debug('-----inside Else-broker id----'+agreement.get('client__c'));
                            if(agreement.get('Client__c') != null){ 
                            system.debug('-----inside ******************----');                           
                                 clientId = (String.valueOf(agreement.get('Client__c')) +'').substring(0,15);
                                 clientIdSet.add(clientId);                            
                                 if(clientId  != null ){                                
                                     if(agreement.get('Name') != null){
                                         clientoptions.add(new SelectOption(clientId ,String.valueOf(agreement.get('Name'))));
                                         SelectedClient = String.valueOf(clientId);
                                         }
                                     else{
                                         clientoptions.add(new SelectOption(clientId ,''));
                                         SelectedClient = String.valueOf(clientId);
                                         }
                                         
                                 }
                                 if(SelectedClient != null){
                                    System.debug('SelectedClient---->'+SelectedClient);
                                    getAgreementList();
                                }
                            }
                        }
                         
                     }
                }
            
        }
    }
    public void rerenderList(){
        SelectedClient = ApexPages.currentPage().getParameters().get('paramValue');
        system.debug('inside rerenderList SelectedClient '+SelectedClient);
        getAgreementList();
        SelectedAgreementType = '';
        SelectedCoverGroup = '';
        //getCoverGroupList();
        coverGroupList= new List<SelectOption>();
        coverGroupList.add(new SelectOption('',''));
        
        retrieveReportsData();
        
    }
    public void rerenderListCover()
    {
        SelectedAgreementType = ApexPages.currentPage().getParameters().get('paramValueCover');
        system.debug('inside rerenderList SelectedAgreementType '+SelectedAgreementType);
        //getAgreementList();
        getCoverGroupList();
    }
    public void getAgreementList(){
        agreementTypeList = new List<SelectOption>();
        agreementTypeList.add(new SelectOption('',''));
        List<AggregateResult> resultList;       
        //agreementTypeList  = getPicklistValues('Contract','Business_Area__c');
        if(SelectedClient != null && !''.equals(SelectedClient )){
            //for broker
            if(userType == 'Broker'){
                system.debug('@@@@@ if block in Broker @@@@@@@@@@@@@');
                System.debug('loggedCompanyId---->'+loggedCompanyId);
                resultList = new List<AggregateResult>([select  Agreement_Type__c, Agreement_Type_Code__c from Contract  where business_area__c='P&I' and agreement_type__c in ('Charterers','Mou Operators','Owners') and Client__r.id =:SelectedClient and Broker__c =: loggedCompanyId group by Agreement_Type__c, Agreement_Type_Code__c order by Agreement_Type__c ]);
            }
            else{
            //for client
            system.debug('@@@@@ if block in Client @@@@@@@@@@@@@');
            resultList = new List<AggregateResult>([select  Agreement_Type__c, Agreement_Type_Code__c from Contract  where business_area__c='P&I' and agreement_type__c in ('Charterers','Mou Operators','Owners') and Client__r.id =:SelectedClient group by Agreement_Type__c, Agreement_Type_Code__c order by Agreement_Type__c ]);
            }
            system.debug(' IF Agreement##########resultList->'+resultList.size());
            for(AggregateResult con : resultList ){
                system.debug('inside IF getAgreementList 1-'+con.get('Agreement_Type__c'));
                system.debug('inside IF getAgreementList 2-'+con.get('Agreement_Type_Code__c'));

                if(con.get('Agreement_Type__c') != null){
                    if(con.get('Agreement_Type_Code__c') != null)
                        agreementTypeList.add(new SelectOption(String.valueOf(con.get('Agreement_Type_Code__c')),String.valueOf(con.get('Agreement_Type__c'))));
                    else 
                        agreementTypeList.add(new SelectOption('',String.valueOf(con.get('Agreement_Type__c'))));
                }
            }
        }else{
        system.debug('@@@@@ Else block in Broker @@@@@@@@@@@@@');
            //resultList = new List<Contract>([select  Agreement_Type__c from Contract where Client__r.id  in :clientIdSet LIMIT 1000]);
            resultList = new List<AggregateResult>([select  Agreement_Type__c,Agreement_Type_Code__c from Contract where business_area__c='P&I' and agreement_type__c in ('Charterers','Mou Operators','Owners') group by Agreement_Type__c,Agreement_Type_Code__c order by Agreement_Type__c]);
                system.debug('ELSE Agreement##########resultList->'+resultList.size());
            for(AggregateResult con : resultList ){
                

                if(con.get('Agreement_Type__c') != null){
                    if(con.get('Agreement_Type_Code__c') != null)
                        agreementTypeList.add(new SelectOption(String.valueOf(con.get('Agreement_Type_Code__c')),String.valueOf(con.get('Agreement_Type__c'))));
                    else 
                        agreementTypeList.add(new SelectOption('',String.valueOf(con.get('Agreement_Type__c'))));
                }  
            }
        }
        
        
    }
   public void getCoverGroupList(){
        coverGroupList= new List<SelectOption>();
        coverGroupList.add(new SelectOption('',''));
        List<AggregateResult> resultList;
        List<Contract> lstContract;
        List<Id> allContactId = new List<Id>();
        system.debug('-----inside getCoverGroupList SelectedClient -----'+SelectedClient );
        system.debug('-----inside getCoverGroupList SelectedAgreementType-----'+SelectedAgreementType);
        if(SelectedClient != null && !''.equals(SelectedClient )){
        if(userType == 'Broker'){
        //for broker
            lstContract = new List<Contract>([select id from contract where business_area__c='P&I' and agreement_type__c in ('Charterers','Mou Operators','Owners') and Client__r.id =:SelectedClient and Broker__c =:loggedCompanyId]);
            for(Contract ct:lstContract){
                allContactId.add(ct.id);                
            }
            resultList = new List<AggregateResult>([select  Cover_Group__c,Cover_Group_Code__c from Asset where agreement__c in:allContactId and Agreement__r.Agreement_Type__c=:SelectedAgreementType and Cover_Group_Code__c in ('PI','CR','FD') group by Cover_Group__c,Cover_Group_Code__c order by Cover_Group__c ]);
        }
        else{
        //for client
            resultList = new List<AggregateResult>([select  Cover_Group__c,Cover_Group_Code__c from Asset where accountid=:SelectedClient and Agreement__r.Agreement_Type__c=:SelectedAgreementType and Cover_Group_Code__c in ('PI','CR','FD') group by Cover_Group__c,Cover_Group_Code__c order by Cover_Group__c ]);
        }
            
            system.debug('IF Cover ##########resultList->'+resultList.size());
            if(resultList != null && !resultList.isEmpty()){
                for(AggregateResult obj: resultList){

 
                     if(obj.get('Cover_Group__c')!= null){
                        if(obj.get('Cover_Group_Code__c')!=null)    
                            coverGroupList.add(new SelectOption(String.valueOf(obj.get('Cover_Group_Code__c')),String.valueOf(obj.get('Cover_Group__c'))));
                        else
                            coverGroupList.add(new SelectOption('',String.valueOf(obj.get('Cover_Group__c'))));

                    }
                }
            }
        }else{
            system.debug('-----inside ELSE getCoverGroupList --'+SelectedClient );
            resultList = new List<AggregateResult>([select Cover_Group__c,Cover_Group_Code__c from Asset where accountid IN :clientIdSet and Agreement__r.Agreement_Type__c=:SelectedAgreementType and Cover_Group_Code__c in ('PI','CR','FD')  group by Cover_Group__c,Cover_Group_Code__c order by Cover_Group__c]);
            system.debug('Else Cover ##########resultList->'+resultList.size());
            if(resultList != null && !resultList.isEmpty()){
                for(AggregateResult obj: resultList){
                   
                    if(obj.get('Cover_Group__c')!= null){
                        if(obj.get('Cover_Group_Code__c')!=null)    
                            coverGroupList.add(new SelectOption(String.valueOf(obj.get('Cover_Group_Code__c')),String.valueOf(obj.get('Cover_Group__c'))));
                        else
                            coverGroupList.add(new SelectOption('',String.valueOf(obj.get('Cover_Group__c'))));

                    }
                }
            }
        }
        
    }
    
    public void checkHideSearch(){
        if(agreementTypeList!=null && agreementTypeList.size()>0 ){
            for(SelectOption opt : agreementTypeList){
                if(opt.getValue().equals('P&I')){
                    hideSearch = true;
                    break;    
                }
            }
        }
    }
 
    public PageReference getNewReport(){
                String SelClient = ApexPages.currentPage().getParameters().get('paramClient');
                String SelectedAgreementType = ApexPages.currentPage().getParameters().get('paramAgreement');
                String SelectedCoverGroup = ApexPages.currentPage().getParameters().get('paramCoverGroup');
                Account acc = [select Company_ID__c  from Account where id=:SelClient LIMIT 1];
                String companyId = acc.Company_ID__c;                
                system.debug('-----loggedInId -----'+loggedInContactId );
                String brokerCompanyId ='';
                String paramP0 = '';
                if(userType == 'Broker'){
                    paramP0 = '4';
                     //Account accBroker = [select Company_ID__c  from Account where Contact_Client__c=:loggedInContactId LIMIT 1];
                     List<Contact> accBroker = new List<Contact>([select Account.Company_ID__c from Contact where id=:loggedInContactId LIMIT 1]);
                     if(accBroker != null){
                     brokerCompanyId = accBroker[0].Account.Company_ID__c ;
                     }
                }
                else{
                    paramP0 = '5';
                }
                //String companyId = '35';
                system.debug('-----companyId -----'+companyId);
                system.debug('-----CoverGroup -----'+SelectedCoverGroup);
                system.debug('-----Agreement Type -----'+SelectedAgreementType);  
                system.debug('-----BrokercompanyId -----'+brokerCompanyId);
                 
                String clientStr = 'Dim Client'+'"'+'.'+'"'+'Sf Company Id';
                String coverGrpStr = 'Dim Coverage'+'"'+'.'+'"'+'Gic Coverage Group Code';
                String aggrGrpStr = 'Dim Agreement Type'+'"'+'.'+'"'+'Agreement Type Code';
                
                String var1 = '"Dim Client"."Sf Company Id"';
                String var2 = '"Dim Coverage"."Gic Coverage Group Code"';
                String var3 = '"Dim Agreement Type"."Agreement Type Code"';
                             
                htmlRequest ='';                
                htmlRequest += '<html>';
                htmlRequest += '<body onload="document.forms[0].submit()">';
                //htmlRequest += '<form name="myForm" id="myForm" method="POST" action="https://soa.gard.no/mobanalytics/saw.dll?Portalpages">';
                htmlRequest += '<form name="myForm" id="myForm" method="POST" action="https://soa.gard.no/mobanalytics/saw.dll?Dashboard">';
                //htmlRequest += '<input type="hidden" name="NQuser" value="OBIEEREAD" />';
                htmlRequest += '<input type="hidden" name="NQuser" value="BIExtranetUser" />';
               // htmlRequest += '<input type="hidden" name="NQpassword" value="obi33r3ad!!" />';
                htmlRequest += '<input type="hidden" name="NQpassword" value="Jg5#Om@K4(G" />';
                htmlRequest += '<input type="hidden" name="Path" value="/shared/Extranet/_portal/Renewal Reports" />';
                htmlRequest += '<input type="hidden" name="Action" value="Extract" />';
                //htmlRequest += '<input type="hidden" name="P0" value="3" />';
                htmlRequest += '<input type="hidden" name="P0" value="'+paramP0+'" />';
                htmlRequest += '<input type="hidden" name="P1" value="eq" />';              
                htmlRequest += '<input type="hidden" name="P2" value="Q1" />';
                htmlRequest += '<input type="hidden" name="P3" value="'+companyId+'" />'; 
                htmlRequest += '<input type="hidden" name="P4" value="eq" />';  
                htmlRequest += '<input type="hidden" name="P5" value="Q2" />';
                htmlRequest += '<input type="hidden" name="P6" value="'+SelectedCoverGroup+'" />';
                htmlRequest += '<input type="hidden" name="P7" value="eq" />';
                htmlRequest += '<input type="hidden" name="P8" value="Q3" />';
                htmlRequest += '<input type="hidden" name="P9" value="'+SelectedAgreementType+'" />';
                htmlRequest += '<input type="hidden" name="P10" value="eq" />';
                if(isBroker){
                htmlRequest += '<input type="hidden" name="P11" value="Q4" />';
                htmlRequest += '<input type="hidden" name="P12" value="'+brokerCompanyId+'" />';
                htmlRequest += '<input type="hidden" name="P13" value="eq"/> ';
                }                
                htmlRequest += '<input type="hidden" name="AllowPeopleClaim" value="'+allowPeopleClaim+'" />';
                htmlRequest += '</form>';
                htmlRequest += '</body>';
                htmlRequest += '</html>';                
                system.debug('HTML--->'+htmlRequest );
                       
                showReport = true;
    return null;
    }
    
    public string retriveHtmlData(){
        system.debug('HTML  Final --->'+htmlRequest );
    return htmlRequest;
    }
     
   /* was already commented 
   public list<SelectOption>  getPicklistValues(String objName, String fld){
        SObject objType;
        list<SelectOption> options   = new list<SelectOption>();
        Schema.SobjectType  obj = Schema.getGlobalDescribe().get(objName);
        objType = obj.newSObject();
        Schema.DescribeSObjectResult fieldResult = obj.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = fieldResult.fields.getMap(); 
        list<Schema.PicklistEntry> values =
        fieldMap.get(fld).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : values)
        { 
            options.add(new SelectOption(a.getLabel(), a.getValue())); 
        }
      return options;
    }*/
    
  /*  @RemoteAction
    public static String ViewDataRemote(String SelClient, String SelectedAgreementType,String SelectedCoverGroup ){
        String params = '';
        String report = '';
        String url ='';
        system.debug('-----SelectedClient-----'+SelClient);
        Account acc = [select Company_ID__c  from Account where id=:SelClient LIMIT 1];
        String companyId = acc.Company_ID__c;
        
        system.debug('-----companyId -----'+companyId);
        system.debug('-----CoverGroup -----'+SelectedCoverGroup);
        system.debug('-----Agreement Type -----'+SelectedAgreementType);
       
       params ='3'+'|'+'eq'+'|'+'"'+'Dim Client'+'"'+'.'+'"'+'Sf Company Id'+'"'+'|'+companyId+'|'+'eq'+'|'+'"'+'Dim Coverage'+'"'+'.'+'"'+'Gic Coverage Group Code'+'"'+'|'+SelectedCoverGroup+'|'+'eq'+'"'+'Dim Agreement Type'+'"'+'.'+'"'+'Agreement Type Code'+'"'+SelectedAgreementType;
        system.debug('-----params  -----'+params );
        
        servicesGardNoMygardreports_SH.BasicHttpBinding_ImyGardReportsQSPort cls =
                            new servicesGardNoMygardreports_SH.BasicHttpBinding_ImyGardReportsQSPort();
        
        try{
        
            url = cls.getReportWithParams(report ,params);
            system.debug('before return URL --'+url);
            url = url.replace('&amp;','');
            system.debug('before return After replace URL --'+url);
            
        }catch(Exception e){
            system.debug('Exception --'+e);
        }
        redirectUrl = url;
        //ApexPages.currentPage().setParameters().put('redirectUrl', url);
        return url;
    }
    
        public PageReference refreshpage(){
        PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());

pageRef.setRedirect(true);

return pageRef;
}*/

/*
   @RemoteAction
    public  static String ViewDataRemote(String SelClient, String SelectedAgreementType,String SelectedCoverGroup ){
        String params = '';
        String report = '';
        String url ='';
        HTTPResponse res;
       
        String endpoint='';
               
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://soa-dev.gard.no/mobanalytics/saw.dll?Dashboard');
        req.setMethod('POST');
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartArray();
        gen.writeStartObject();
        system.debug('inside perform action start field ');
        gen.writeStringField('NQuser','OBIEEREAD');        
        gen.writeStringField('NQpassword','obi33r3ad!!');
        gen.writeStringField('Path','/shared/Extranet/_portal/Renewal Reports');
        gen.writeStringField('Action','Navigate');
        gen.writeStringField('P0','3');
        gen.writeStringField('P1','eq');
        gen.writeStringField('P2','\"Dim Client.Sf Company Id\"');       
        gen.writeStringField('P3','35');
        gen.writeStringField('P4','eq');
        gen.writeStringField('P5','Dim Coverage.Gic Coverage Group Code');        
        gen.writeStringField('P6','PI');
        gen.writeStringField('P7','eq');
        gen.writeStringField('P8','Dim Agreement Type.Agreement Type Code');        
        gen.writeStringField('P9','OWN');
        
        
        gen.writeEndObject(); 
        gen.writeEndArray();
        // create a string from the JSON generator
        String jsonStr = gen.getAsString();
       
        system.debug('String '+jsonStr );
        req.setHeader('Content-Type', 'application/JSON');
        req.setBody(jsonStr);
        
        String htmlRequest ='';
        htmlRequest += '<html>';
        htmlRequest += '<body onload="document.forms[0].submit()">';
        htmlRequest += '<form name="myForm" id="myForm" method="POST" action="https://soa-dev.gard.no/mobanalytics/saw.dll?Dashboard">';
        htmlRequest += '<input type="hidden" name="NQuser" value="OBIEEREAD" />';
        htmlRequest += '<input type="hidden" name="NQpassword" value="obi33r3ad!!" />';
        htmlRequest += '<input type="hidden" name="Path" value="/shared/Extranet/_portal/Renewal Reports" />';
        htmlRequest += '<input type="hidden" name="Action" value="Extract" />';
        
        htmlRequest += '</form>';
        htmlRequest += '</body>';
        htmlRequest += '</html>';
        system.debug('HTML--->'+htmlRequest );        
       req.setBody(htmlRequest);
            
       
        try{
        Http http = new Http();
         res = http.send(req);
       
        }catch(Exception e){
        system.debug('inside perform action exception  '+e.getMessage());
        }
        
       
      
return null;
}

public void retrieveReportsData(){
                
        String SelClient = ApexPages.currentPage().getParameters().get('paramValue');
        Account acc = [select Company_ID__c  from Account where id=:SelClient LIMIT 1];
        String companyId = acc.Company_ID__c;                
        system.debug('-----loggedInId -----'+loggedInContactId );
        String brokerCompanyId ='';
        
        if(userType == 'Broker'){
             List<Contact> accBroker = new List<Contact>([select Account.Company_ID__c from Contact where id=:loggedInContactId LIMIT 1]);
             if(accBroker != null){
                brokerCompanyId = accBroker[0].Account.Company_ID__c ;
             }
        }
        
        system.debug('-----companyId -----'+companyId);
        system.debug('-----BrokercompanyId -----'+brokerCompanyId);
        brokerIdToPass = brokerCompanyId;
        //clientIdToPass = companyId;
        system.debug('-----brokerIdToPass -----'+brokerIdToPass);
        system.debug('-----clientIdToPass -----'+clientIdToPass);
        String nameOfReport = 'Vessels On Risk';
        Data objData = new Data();
        objData.setPayload(nameOfReport);
        objData.setClient(companyId);
        objData.setBroker(brokerIdToPass);       
        //showReport = true;
    
    }
    
 
 @RemoteAction
    public static  Data getReport(Data reportName) {
        booleanPeopleClaim = isPeopleClaimUser();

        // Signature for reports.execute():
        OBIEEReports.execute_pt reports = new OBIEEReports.execute_pt();
                reports.timeout_x = 120000;

        String response;
        system.debug('Data: '+reportName);
        String SelClient = reportName.getClient();
        Account acc = [select Company_ID__c  from Account where id=:SelClient LIMIT 1];
        String companyId = acc.Company_ID__c;                
        //system.debug('-----loggedInId -----'+loggedInContactId );
        String brokerCompanyId ='';
        
        String contactId='';
        for(User obj:[select id,Contactid from User where id=:UserInfo.getUserId()]){
            contactId = obj.ContactId;  
        }
        
        if(reportName.getBroker() == 'Broker'){
             List<Contact> accBroker = new List<Contact>([select Account.Company_ID__c from Contact where id=:contactId LIMIT 1]);
             if(accBroker != null){
                brokerCompanyId = accBroker[0].Account.Company_ID__c ;
             }
        }
        
        
        
        
        Integer clientId = Integer.valueOf(companyId);
        String brokerId = brokerCompanyId;
        system.debug('clientId: '+clientId);
        system.debug('brokerId: '+brokerId);
        string formattedbrokerid;
        if(brokerId!=null){formattedbrokerid=brokerId ;} else formattedbrokerid='';
        String parameters = formattedbrokerid+ ','+reportName.getCoverGroup()+','+reportName.getAgreementType();
        system.debug('parameters: '+parameters);
        
        if(brokerId!=null && brokerId!=''){
            response = reports.execute(clientId, reportName.getPayload(),parameters, '', true, booleanPeopleClaim);        
        }else{
            response = reports.execute(clientId, reportName.getPayload(), parameters, '', false, booleanPeopleClaim);
        }
        //system.debug('response: '+ EncodingUtil.base64Encode(response));
        
        Data data = new Data();
        data.setPayload(response);
        return data;        
    }
    
 public class Data {
        private String payload;
        private String client;
        private String broker;
        private String coverGroup;
        private String agreementType;
        public String getPayload() { return payload; }
        public String getClient() { return client; }
        public String getBroker() { return broker; }
        public String getCoverGroup() { return coverGroup; }
        public String getAgreementType() { return agreementType; }
        public void setPayload(String payload) { this.payload = payload; }
        public void setClient(String client) { this.client = client; }
        public void setBroker(String broker) { this.broker = broker; }
        public void setCoverGroup(String coverGroup) { this.coverGroup = coverGroup; }
        public void setAgreementType(String agreementType) { this.agreementType = agreementType; }
    }
 public PageReference exportToExcelData(){
  return page.DisplayReportsVFPage;
 }   
     */
    
}