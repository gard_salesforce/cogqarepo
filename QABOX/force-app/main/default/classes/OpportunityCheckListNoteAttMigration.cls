public class OpportunityCheckListNoteAttMigration{
    public void doOperation(){
        /*
        Commented to delete fields SF-3456
        List<Opportunity> allOpp = [SELECT ID FROM Opportunity WHERE RecordTypeName__c = 'P_I' AND Checklist_Completed_Count__c >1];
        System.Debug('Count: ' + allOpp.size());
        Id oldPIRecId =[SELECT Id FROM RecordType WHERE Name='Old P&I' AND sObjectType='Opportunity_Checklist__c'].id;
        Id newPIRecId =[SELECT Id FROM RecordType WHERE Name='P&I' AND sObjectType='Opportunity_Checklist__c'].id;
        Id ownersRecId =[SELECT Id FROM RecordType WHERE Name='Owners' AND sObjectType='Opportunity_Checklist__c'].id;
        Id mouRecId =[SELECT Id FROM RecordType WHERE Name='MOU' AND sObjectType='Opportunity_Checklist__c'].id;
        Id charterersRecId =[SELECT Id FROM RecordType WHERE Name='Charterers' AND sObjectType='Opportunity_Checklist__c'].id; 
        //in case of new P&I checklist if more than one checklist, use any 1 agreement completed one at a time 
        // for rest of all old P&I and P&I and with only one checklist use  Owners_P_I_Completed__c = true or MOU_Completed__c = true or Charterers_Completed__c = true  
        List<Opportunity_Checklist__c> allOppCheckLists = [SELECT ID,Opportunity__c FROM Opportunity_Checklist__c 
                                                           WHERE Charterers_Completed__c = true 
                                                            //Owners_P_I_Completed__c = true
                                                            //or MOU_Completed__c = true 
                                                            //or Charterers_Completed__c = true) 
                                                            and RecordTypeId  =: newPIRecId 
                                                            and Opportunity__c  IN: allOpp]; 
                                                           //and Test_Count__c IN: allCount];      
        
        */
        /* UNCOMMENT FOR Attachment MIGRATION 
         */
        /*
        Commented to delete fields SF-3456
        Map<ID,ID> mapOppAndChecklistID = new Map<ID,ID>();
        for(Opportunity_Checklist__c o : allOppCheckLists){
            System.Debug('Check 1 ' + o.ID);
            System.Debug('Check 2 ' + o.Opportunity__c);
            mapOppAndChecklistID.put(o.ID, o.Opportunity__c);
        }
        List<Attachment> attachments = [select Id, Name, Body, ContentType,Parentid from Attachment 
                                                        where Parentid IN: allOppCheckLists];
        
        
        Map<String,List<Attachment>> parentWithAttachment= new Map<string,List<Attachment>>();
        List<Attachment> t;
        for(Attachment att : attachments){
                
            t = new List<Attachment>();
            t.add(att);
            //
            List<Attachment> existingList = parentWithAttachment.get(att.Parentid);
            if(existingList != null && existingList.size()!=0) {
                t.addAll(existingList);
            }
            // 
            parentWithAttachment.put(att.Parentid,t);
         }
         
        //System.debug('T SIZE: '+t.size());
        //System.debug('parentWithAttachment'+parentWithAttachment.size());       
        Map<String,List<Attachment>> attachmentWithOpp= new Map<string,List<Attachment>>();
        List<Attachment> at;
        for(String s : parentWithAttachment.keySet())
        {   
             for(Attachment a :parentWithAttachment.get(s)){
                for(Opportunity_Checklist__c o : allOppCheckLists){
                    if(s.equals(String.valueOf(o.ID))){
                        at = new List<Attachment>();
                        at.add(a); 
                        //
                        List<Attachment> existingList = attachmentWithOpp.get(String.valueOf(o.Opportunity__c));
                        if(existingList != null && existingList.size()!=0) {
                            at.addAll(existingList);
                        }
                        //
                        attachmentWithOpp.put(String.valueOf(o.Opportunity__c), at); 
                    }
                }
            }
        }
        */
        /* UNCOMMENT FOR Attachment MIGRATION end
        */
        
        /*UNCOMMENT FOR NOTES MIGRATION 
        
        List<Note> notes = [select Id, CreatedBy.Name, Body, Title,Parentid from Note 
                            where Parentid IN: allOppCheckLists];
       
        
        Map<String,List<Note>> parentWithNote= new Map<string,List<Note>>();
        for(Note nt : notes){
                
            List<Note> t = new List<Note>();
            t.add(nt); 
            parentWithNote.put(nt.Parentid,t);
         }
        
        Map<String,List<Note>> noteWithOpp= new Map<string,List<Note>>();
        for(String s : parentWithNote.keySet())
        {   
             for(Note n :parentWithNote.get(s)){
                for(Opportunity_Checklist__c o : allOppCheckLists){
                    if(s.equals(String.valueOf(o.ID))){
                        List<Note> nt = new List<Note>();
                        nt.add(n); 
                        noteWithOpp.put(String.valueOf(o.Opportunity__c), nt); 
                    }
                }
            }
        }
        
        */
        /*UNCOMMENT FOR NOTES MIGRATION end */
        
        // fetch new OppChecklist for Owners/Charterers/MOU and recordtype Owners/Charterers/MOU
        /*
        Commented to delete fields SF-3456
        List<Opportunity_Checklist__c> newAllOppCheckLists = [SELECT ID,Opportunity__c FROM Opportunity_Checklist__c 
                                                               WHERE RecordTypeId  =: charterersRecId 
                                                               and Opportunity__c  IN: allOpp];
                                                         
        
        Map<ID,Opportunity_Checklist__c> newMapOppAndChecklist = new Map<ID,Opportunity_Checklist__c>();
        for(Opportunity_Checklist__c o : newAllOppCheckLists){
            newMapOppAndChecklist.put(o.Opportunity__c, o);
            System.Debug('Check 11 ' + o.ID);
            System.Debug('Check 12 ' + o.Opportunity__c);            
        }
        
        List<Attachment> finalAttachment = new List<Attachment>();
        Attachment newAtt;
        List<Note> finalNote = new List<Note>();
        Note newNote;
        // UNCOMMENT FOR Attachment MIGRATION 
         
        for(String s : attachmentWithOpp.keySet()){
            for(Attachment a :attachmentWithOpp.get(s)){
                if(newMapOppAndChecklist.containsKey(ID.valueOf(s))){
                    Opportunity_Checklist__c newOppChecklist = newMapOppAndChecklist.get(ID.valueOf(s));
                    System.debug('successfullll');
                    newAtt = new Attachment(Name = a.name, body = a.body);
                    newAtt.ParentId = newOppChecklist.Id;
                    finalAttachment.add(newAtt);
                }
            
            }
        }
        
        if(finalAttachment.size() > 0){
                System.debug('finally successfullll');
                Database.insert(finalAttachment,false);
            }
       */
        /* UNCOMMENT FOR Attachment MIGRATION end
         */
         
        // UNCOMMENT FOR NOTES MIGRATION 
        /*
        for(String s : noteWithOpp.keySet()){
            for(Note n :noteWithOpp.get(s)){
                if(newMapOppAndChecklist.containsKey(ID.valueOf(s))){
                    Opportunity_Checklist__c newOppChecklist = newMapOppAndChecklist.get(ID.valueOf(s));
                    System.debug('successfullll with note');
                    newNote = new Note(Title = n.title, body = n.body);
                    newNote.ParentId = newOppChecklist.Id;
                    finalNote.add(newNote);
                }
            
            }
        }
        
        if(finalNote.size() > 0){
                Database.insert(finalNote,false);
            }
         */
        
    }
}