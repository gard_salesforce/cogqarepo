/******************************************************************************************************
* Class Name: BatchSendDocumentsToMFiles
* Created By: Nagarjuna Kaipu
* Date: 11-02-2021
* Description: SF-5422 - Used to to send documents to store in MFiles
*******************************************************************************************************
*  Version		Developer			Date			Description
=======================================================================================================
*  1.0          Nagarjuna Kaipu		11/02/2021  	Created
*  1.1          Nagarjuna Kaipu     25/05/2021      SF-6601 
*  1.2          Nagarjuna Kaipu     25/05/2021      SF-6604 
*******************************************************************************************************/
global class BatchSendDocumentsToMFiles Implements Database.Batchable <sObject>,Database.Stateful,Database.AllowsCallouts {
    Public Id caseId;
    String authToken; 
    String subjectDesc; 
    List<String> uwrTopic; 
    List<String> agreementIds; 
    Set<String> objUniqueIds; 
    String companyId; 
    String policyYr; 
    String prodArea; 
    Id fileId;
    List<Id> conDocIdList;
    global Map<Id, String> fileCorelIdMap;
    
    public BatchSendDocumentsToMFiles(Id caseId, List<Id> conDocIdList, String authToken, String subjectDesc, List<String> uwrTopic, List<String> agreementIds, Set<String> objUniqueIds, String companyId, String policyYr, String prodArea){
        this.caseId = caseId;
        this.conDocIdList = conDocIdList;
        this.authToken = authToken; 
        this.subjectDesc = subjectDesc; 
        this.uwrTopic = uwrTopic; 
        this.agreementIds = agreementIds; 
        this.objUniqueIds = objUniqueIds; 
        this.companyId = companyId; 
        this.policyYr = policyYr; 
        this.prodArea = prodArea; 
        fileCorelIdMap = new Map<Id, String>();
    }
    global Database.QueryLocator start(Database.BatchableContext bc){
        system.debug('Inside Start Method MFiles'+conDocIdList);
        String soqlQuery = 'Select Id, Description From ContentVersion where Id IN: conDocIdList';
        system.debug('soqlQuery MFiles is: '+soqlQuery);
        return Database.getQueryLocator(soqlQuery);
    }
    
    global void execute(Database.BatchableContext bc, List<ContentVersion> scope){
        system.debug('Inside execute MFiles: '+scope);
        try{
            for(ContentVersion cd: scope){
                system.debug('Inside loop MFiles');
                restCallOut(caseId, authToken, subjectDesc, uwrTopic, agreementIds, objUniqueIds, companyId, policyYr, prodArea, cd.Id);
            }
            system.debug('fileCorelIdMap in execute is: '+fileCorelIdMap);
        }
        catch(Exception ex){ //SF-6604 added
            Exception_Log__c errLog = new Exception_Log__c(source__c = 'BatchSendDocumentsToMFiles:execute', Type_Name__c = 'MFiles Exception: '+ ex.getTypeName(), 
                                                           exception_Message__c = ex.getMessage(), Stack_Trace__c = ex.getStackTraceString());
            insert errLog;
        }
    }
    public void restCallOut(Id caseId, String authToken, String subjectDesc, List<String> uwrTopic, List<String> agreementIds, Set<String> objUniqueIds, String companyId, String policyYr, String prodArea, Id fileId){
        system.debug('heap size first line of callout method :'+Limits.getHeapSize());
        if(uwrTopic == null || uwrTopic.size() == 0){
            uwrTopic.add('26');
            //uwrTopic.add('OTH');
        }
        Map<String,Object> reqWrapperMap = new Map<String,Object>();
        reqWrapperMap.put('pd_desc',subjectDesc);
        reqWrapperMap.put('pd_uwrtopic',uwrTopic);
        if(!agreementIds.IsEmpty()){
            reqWrapperMap.put('pd_agreement',agreementIds); 
        }
        System.debug('objUniqueIds is: '+objUniqueIds);
        if(objUniqueIds != null && objUniqueIds.size() > 0 && prodArea != null && policyYr != null){
            objUniqueIds.remove('');
            objUniqueIds.remove(null);
            reqWrapperMap.put('pd_object',objUniqueIds);
        }
        if(String.isNotEmpty(companyId)){
            reqWrapperMap.put('pd_client',companyId);
        }
        if(String.isNotEmpty(policyYr)){
            reqWrapperMap.put('pd_years',policyYr);  
        } 
        Product_area_for_MFiles__c prodAreaCode = new Product_area_for_MFiles__c();
        if(prodArea != null && prodArea != '')
            prodAreaCode = Product_area_for_MFiles__c.getInstance(prodArea);
        reqWrapperMap.put('pd_productarea',prodAreaCode.Product_area_code__c);
        reqWrapperMap.put('pd_sourcesystem','SALESFORCE');
        //reqWrapperMap.put('pd_creator',[SELECT User_ID__c, alias FROM User where Id =: UserInfo.getUserId()].alias);
        //Fetch details for case metadata tag - ENDS
        system.debug('reqWrapperMap:'+reqWrapperMap);
        String fileObjClass = 'cl_covermanagementdocument';
        String caseMetadata = '';
        system.debug('file id is: '+fileId);
        List<ContentVersion> fileList = new List<ContentVersion>([SELECT Id, ContentDocumentId, FileExtension,FileType, Title,VersionData FROM ContentVersion WHERE Id =: fileId]);
        system.debug('fileList is: '+fileList);
        //system.debug('get file body:'+EncodingUtil.base64Encode(fileList[0].VersionData));
        if(fileList != null && fileList.size() > 0){
            String fileTitle = '';
            if(fileList != null && fileList.size() > 0 && fileList[0].Title.contains('"'))
                fileTitle = fileList[0].Title.replace('"','\'');
            else
                fileTitle = fileList[0].Title;
            reqWrapperMap.put('0','"'+fileTitle+'"');
            caseMetadata = json.serialize(reqWrapperMap);
            String fileExt = '';
            if((fileList[0].FileExtension == null || fileList[0].FileExtension == '') && fileList[0].Title.contains('.eml'))
                fileExt = 'eml';
            else
                fileExt = fileList[0].FileExtension;
            String reqBody = '------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"metadata\"\r\n\r\n'+caseMetadata+'\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"fileObjectClass\"\r\n\r\n'+fileObjClass+'\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"fileName\"\r\n\r\n'+'"'+fileTitle+'"'+'\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"fileExtension\"\r\n\r\n'+fileExt+'\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"file\"\r\n\r\n'+EncodingUtil.base64Encode(fileList[0].VersionData)+'\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"sourceSystem\"\r\n\r\n'+'SALESFORCE'+'\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--';
            system.debug('heap size callout method :'+Limits.getHeapSize());
            Mule_Web_Service__c docEndpoint = Mule_Web_Service__c.getInstance('M-Files endpoint for document storing');
            Http sendDocReq = new Http();
            Httprequest DocReq = new Httprequest();
            DocReq.setEndpoint(docEndpoint.Endpoint_Url__c);
            DocReq.setheader('Content-Type','multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW');
            DocReq.setheader('Authorization',authToken);
            DocReq.setMethod('POST');
            DocReq.setbody(reqBody);
            system.debug('  JSON DOC Request body: '+reqBody);
            HttpResponse docResponse;
            if(!test.isRunningTest()){
                docResponse = sendDocReq.send(DocReq);
            }
            system.debug('File response received : '+docResponse.getBody());
            system.debug('File response status code : '+docResponse.getStatusCode());
            
            //Commented since the requirement for storing the Document Id in Case is deprioritized.
            if (docResponse.getStatusCode() == 202) {
                FileResponseWrapper fileResponseWrapper = new FileResponseWrapper();
                fileResponseWrapper = (FileResponseWrapper)System.JSON.deserialize(docResponse.getBody(), FileResponseWrapper.class);
                fileCorelIdMap.put(fileId, fileResponseWrapper.correlationId);
            }
            else{ //SF-6604 added
                Exception_Log__c errLog = new Exception_Log__c(source__c = 'BatchSendDocumentsToMFiles:restCallOut', Type_Name__c = 'MFiles Exception: Failed to store document', 
                                                               exception_Message__c = String.valueOf(docResponse.getBody()), Additional_Details__c = 'Case Id: '+caseId);
                insert errLog;
            }
            system.debug('fileCorelIdMap is: '+fileCorelIdMap);
        }
        //Sending Documents : Ends
    }
    public class FileResponseWrapper{
        public String status;
        public String message;
        public String correlationId;
        public String documentId;	
    }
    global void finish(Database.BatchableContext bc){
        try{
            String hour = String.valueOf(Datetime.now().hour());
            String min = String.valueOf(Datetime.now().minute() + 1); 
            String ss = String.valueOf(Datetime.now().second());
            
            //parse to cron expression
            String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
            Integer randomNumber = Integer.valueof((Math.random() * 100));
            String jobName = 'Update Document Id'+ randomNumber + ' '+ String.valueOf(Datetime.now()); //SF-6601 added
            
            ScheduleDocBatchPopulateDocumentId sBatch = new ScheduleDocBatchPopulateDocumentId(authToken, fileCorelIdMap, jobName); //SF-6601 added 
            if(!test.isRunningTest()){
                //System.schedule('Update Document Id'+ randomNumber + ' '+ String.valueOf(Datetime.now()), nextFireTime, sBatch);    
                System.schedule(JobName , nextFireTime, sBatch);  ////SF-6601 modified
            }
        }
        catch(Exception ex){ //SF-6604 added
            Exception_Log__c errLog = new Exception_Log__c(source__c = 'BatchSendDocumentsToMFiles:finish', Type_Name__c = 'MFiles Exception: '+ ex.getTypeName(), 
                                                           exception_Message__c = ex.getMessage(), Stack_Trace__c = ex.getStackTraceString());
            insert errLog;
        }
    }
}