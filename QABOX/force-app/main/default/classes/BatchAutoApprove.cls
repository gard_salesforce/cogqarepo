global class BatchAutoApprove implements Database.Batchable<sObject>{
    public String query;
    List<User> lsUser = [SELECT ManagerId FROM User WHERE Id=:UserInfo.getUserId()];
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        for (sObject qt: scope) {
            String strObjectId = String.valueOf(qt.get('Id'));
            System.debug('### Starting Approval process for Id:'+strObjectId);
            List<ProcessInstanceWorkItem> lsWork = [SELECT Id FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId=:strObjectId AND ProcessInstance.Status='Pending'];
        
            //populate next approver list
            ID[] strMgrIds = new ID[0];
            for (User usr: lsUser) {strMgrIds.add(usr.ManagerId);}
            System.debug(lsUser);
            System.debug(lsWork);
            //populate other parameters
            if(lsWork != null && lsWork.size()>0){
                ID strWorkId = lsWork[0].Id;
                System.debug('### Processing work id:'+strWorkId);
                Date dtToday = Date.today();
                
                //set up approval request
                Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                req.setComments('This record was auto-approved on '+dtToday);
                req.setAction('Approve');
                req.setNextApproverIds(strMgrIds);
                req.setWorkitemId(strWorkId);
                
                // Submit the request for approval  
                Approval.ProcessResult result =  Approval.process(req);
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        //ASSERT: do something here
    }
    
    //### TEST METHODS ###
    public static TestMethod void testAutoApprove() {
        //set up test records
        ID strMarineRec = [SELECT Id, Name FROM RecordType WHERE sObjectType='Opportunity' AND Name = 'Marine' LIMIT 1].Id;
        ID strClientRec = [SELECT Id, Name FROM RecordType WHERE sObjectType='Account' AND Name='Client' LIMIT 1].Id;
        ID strAreaMgrId = [SELECT Id, Name FROM User WHERE Position__c='Area Manager' LIMIT 1].Id;
        ID strSVPId = [SELECT Id, Name FROM User WHERE Position__c='SVP' LIMIT 1].Id;
        //create an account record
        Account acc = new Account(Name='APEXTESTACC001', RecordTypeId=strClientRec, Area_Manager__c=strAreaMgrId, Market_Area__c = TestDataGenerator.getMarketArea().id);
        insert acc;
        ID strAccId = acc.Id;
        //create an oppty record
        Date dtToday = Date.Today();
        Opportunity opp = new Opportunity(Name='APEXTESTOPPTY001', RecordTypeId=strMarineRec, AccountId=strAccId, Type='New Business', Amount=100, Sales_Channel__c='Direct', CloseDate=dtToday, StageName='Risk Evaluation', Business_Type__c = 'Ship Owners' , Approval_Criteria__c = 'Silent Approval' ,Ship_Owners_Approval__c='Silent Approval with Standard T&Cs', Approval_Submitted__c=dtToday-2, Area_Manager__c=strAreaMgrId, Senior_Approver__c=strSVPId);
        insert opp;
        system.debug([Select Id, Risk_Evaluation_Status__c from Opportunity where Id = :opp.Id LIMIT 1]);
        ID strOpptyId = opp.Id;
        Double intDuration = [SELECT Approval_Duration__c FROM Opportunity WHERE Id=:strOpptyId LIMIT 1].Approval_Duration__c;
        System.assert(intDuration.intValue()>0, 'ERROR: Submitted Date not greater than 24hrs ago');
        //construct the approval request
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(strOpptyId);
        // Submit the approval request for the account  
        /*Approval.ProcessResult result = Approval.process(req1);
        // Verify the result  
        System.assert(result.isSuccess());
        System.assertEquals('Pending', result.getInstanceStatus(), 'Instance Status'+result.getInstanceStatus());
        */
        //execute test case
        Test.StartTest();
            BatchAutoApprove testBatch = new BatchAutoApprove();
            testBatch.query = 'SELECT Id, Name FROM Opportunity '
                             +'WHERE Id = \''+strOpptyId+'\'';
            Database.ExecuteBatch(testBatch, 100);
        Test.StopTest();
        
        //validate test - no longer valid as we could have two approvers so wouldn't be quoted after the first pass
        //String strStage = [SELECT StageName FROM Opportunity WHERE Id=:strOpptyId LIMIT 1].StageName;
        //System.assertEquals('Quote', strStage, 'ERROR: Opportunity not approved.');
    }
}