@isTest
private class TestBatchDeleteApprovedExaminee {
	private static PEME_Enrollment_Form__c pef;
    private static PEME_Invoice__c pi;
    private static PEME_Exam_Detail__c ped,ped2;
    private static List<PEME_Exam_Detail__c> pedLst;
    
    private static void createData(){
        //PEME_Enrollement_Form
        pef = new 	PEME_Enrollment_Form__c(Enrollment_Status__c = 'Draft');
        insert pef;
        
        //PEME_Enrollement_Detail
        pi = new PEME_Invoice__c(PEME_Enrollment_Detail__c = pef.id);
        insert pi;
        
        //Creating Date
        DateTime dtNow = (DateTime.now().addDays(-2));
        Integer strYear = dtNow.year();
        Integer strMonth = dtNow.month();
        Integer strDay = dtNow.day();
        Date dateForPemeExDet = Date.newInstance(strYear, strMonth, strDay);
        
        //PEME_Exam_Detail
        pedLst = new List<PEME_Exam_Detail__c>();
        ped = new PEME_Exam_Detail__c(invoice__c = pi.id,Status__c = 'Approved', Examinee__c = 'Test Examinee1',ApprovalRejectionDate__c = dateForPemeExDet);
        ped2 = new PEME_Exam_Detail__c(invoice__c = pi.id,Status__c = 'Rejected-Need Input', Examinee__c = 'TestExaminee2',ApprovalRejectionDate__c = dateForPemeExDet);
        pedLst.add(ped);
        pedLst.add(ped2);
        insert pedLst;
    }
    
    @istest private static void testScheduleDeleteExamineeinvoice(){
        createData();
        String schTime = '0 0 0 1 * ?';
        System.schedule('Testing scheduleDeleteExamineeInovice', schTime, new scheduleDeleteExamineeInovice());
    } 
    @istest private static void testBatchDeleteApprovedExaminee(){
        createData();
        Test.startTest();
        Database.executeBatch(new BatchDeleteApprovedExaminee());
        Test.stopTest();
    }
}