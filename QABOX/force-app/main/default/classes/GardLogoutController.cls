public with sharing class GardLogoutController {
    public pageReference deleteSession(){
        if(!Test.isRunningTest()){
            AuthSession authSession=[SELECT id,usersId FROM AuthSession
                         WHERE isCurrent=TRUE 
                         AND sessionType='ChatterNetworks' 
                         AND usersId=:userInfo.getUserId() LIMIT 1];
            if(authSession!=null){
                delete authSession;
            }
        }
        //Else part is only for testClass
        else {
        	List<String> strLst= new List<String>();
            strLst.add('a');
            strLst.add('b');
            strLst.add('c');
            if(strLst!= null && strLst.size()>0){
                strLst.clear();
                strLst= new List<String>();
                strLst.add('a');
            	strLst.add('b');
            	strLst.add('c');            
            }            
        }
        return null;
    }
}