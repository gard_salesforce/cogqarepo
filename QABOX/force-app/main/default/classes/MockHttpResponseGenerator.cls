@isTest

global class MockHttpResponseGenerator implements HttpCalloutMock {
  global HTTPResponse respond(HTTPRequest req) {
     if (req.getEndpoint().endsWith('salesforce')  ) {
        
        HttpResponse res = new HttpResponse();
       
        res.setHeader('Content-Type', 'application/json');

       res.setBody('{'
+'  "Result": true,'
+'  "MoreResultsAvailable": false,'
+'  "Count": 7,'
+'  "ObjectType": 0,'
+'  "ObjectTypeAlias": [],'
+'  "ResultObjects": ['
+'    {'
+'      "ObjectID": 1150675,'
+'      "ExternalID": "1150675",'
+'      "Name": "529210 - ATHENIAN FREEDOM (P&I)",'
+'      "ObjectProperties": {'
+'        "pd_partner": "Athenian Sea Carriers Ltd. - Athens - 214",'
+'        "pd_years": "2018",'
+'        "pd_policydocumenttype": "OWN Owner - certificate of entry",'
+'        "documentClass": "Policy document",'
+'        "name": "529210 - ATHENIAN FREEDOM (P&I)",'
+'        "pd_creator": "Irene Sandvik Nupen",'
+'        "lastModified": "26.04.2020 04.40",'
+'        "pd_validfrom": "20.02.2018 00.00",'
+'        "pd_validto": "20.02.2019 00.00",'
+'        "pd_objectnameondocument": "ATHENIAN FREEDOM"'
+'      }'
+'    },'
+'    {'
+'      "ObjectID": 1110861,'
+'      "ExternalID": "1110861",'
+'      "Name": "  ATHENIAN FREEDOM_CLC92_2018",'
+'      "ObjectProperties": {'
+'        "pd_partner": "Athenian Sea Carriers Ltd. - Athens - 214",'
+'        "pd_years": "2018",'
+'        "pd_object": "ATHENIAN FREEDOM",'
+'        "pd_certificatetype": "CLC92 Blue Card 1992",'
+'        "documentClass": "Certificate",'
+'        "name": "  ATHENIAN FREEDOM_CLC92_2018",'
+'        "pd_author": "Inger Kristine Øynes",'
+'        "pd_creator": "Inger Kristine Øynes",'
+'        "lastModified": "24.04.2020 02.57",'
+'        "pd_validfrom": "20.02.2018 00.00",'
+'        "pd_validto": "20.02.2019 00.00",'
+'        "pd_objectnameondocument": "ATHENIAN FREEDOM"'
+'      }'
+'    },'
+'    {'
+'      "ObjectID": 3083021,'
+'      "ExternalID": "3083021",'
+'      "Name": "TEST - ATHENIAN FREEDOM_WRC_2020",'
+'      "ObjectProperties": {'
+'        "pd_partner": "Athenian Sea Carriers Ltd. - Athens - 214",'
+'        "pd_years": "2020",'
+'        "pd_object": "ATHENIAN FREEDOM",'
+'        "pd_certificatetype": "WRC Blue Card",'
+'        "documentClass": "Certificate",'
+'        "name": "TEST - ATHENIAN FREEDOM_WRC_2020",'
+'        "pd_author": "Rahul Shaw",'
+'        "pd_creator": "Rahul Shaw",'
+'        "lastModified": "03.09.2020 11.39",'
+'        "pd_validfrom": "21.02.2020 00.00",'
+'        "pd_validto": "20.02.2021 00.00",'
+'        "pd_objectnameondocument": "ATHENIAN FREEDOM"'
+'      }'
+'    },'
+'    {'
+'      "ObjectID": 2044587,'
+'      "ExternalID": "2044587",'
+'      "Name": "  ATHENIAN FREEDOM_WRC_2019",'
+'      "ObjectProperties": {'
+'        "pd_partner": "Athenian Sea Carriers Ltd. - Athens - 214",'
+'        "pd_years": "2019",'
+'        "pd_object": "ATHENIAN FREEDOM",'
+'        "pd_certificatetype": "WRC Blue Card",'
+'        "documentClass": "Certificate",'
+'        "name": "  ATHENIAN FREEDOM_WRC_2019",'
+'        "pd_author": "Charlotte Wensell",'
+'        "pd_creator": "Charlotte Wensell",'
+'        "lastModified": "13.05.2020 01.24",'
+'        "pd_validfrom": "20.02.2019 00.00",'
+'        "pd_validto": "20.02.2020 00.00",'
+'        "pd_objectnameondocument": "ATHENIAN FREEDOM"'
+'      }'
+'    },'
+'    {'
+'      "ObjectID": 2044585,'
+'      "ExternalID": "2044585",'
+'      "Name": "  ATHENIAN FREEDOM_BBC_2019",'
+'      "ObjectProperties": {'
+'        "pd_partner": "Athenian Sea Carriers Ltd. - Athens - 214",'
+'        "pd_years": "2019",'
+'        "pd_object": "ATHENIAN FREEDOM",'
+'        "pd_certificatetype": "BBC Bunkers Blue Card",'
+'        "documentClass": "Certificate",'
+'        "name": "  ATHENIAN FREEDOM_BBC_2019",'
+'        "pd_author": "Charlotte Wensell",'
+'        "pd_creator": "Charlotte Wensell",'
+'        "lastModified": "13.05.2020 01.24",'
+'        "pd_validfrom": "20.02.2019 00.00",'
+'        "pd_validto": "20.02.2020 00.00",'
+'        "pd_objectnameondocument": "ATHENIAN FREEDOM"'
+'      }'
+'    },'
+'    {'
+'      "ObjectID": 2044588,'
+'      "ExternalID": "2044588",'
+'      "Name": "  ATHENIAN FREEDOM_MLC_2019",'
+'      "ObjectProperties": {'
+'        "pd_partner": "Athenian Sea Carriers Ltd. - Athens - 214",'
+'        "pd_years": "2019",'
+'        "pd_object": "ATHENIAN FREEDOM",'
+'        "pd_certificatetype": "MLC Maritime Labour Convention",'
+'        "documentClass": "Certificate",'
+'        "name": "  ATHENIAN FREEDOM_MLC_2019",'
+'        "pd_author": "Charlotte Wensell",'
+'        "pd_creator": "Charlotte Wensell",'
+'        "lastModified": "13.05.2020 01.24",'
+'        "pd_validfrom": "20.02.2019 00.00",'
+'        "pd_validto": "20.02.2020 00.00",'
+'        "pd_objectnameondocument": "ATHENIAN FREEDOM"'
+'      }'
+'    },'
+'    {'
+'      "ObjectID": 1120007,'
+'      "ExternalID": "1120007",'
+'      "Name": "  ATHENIAN FREEDOM_MLC_2018",'
+'      "ObjectProperties": {'
+'        "pd_partner": "Athenian Sea Carriers Ltd. - Athens - 214",'
+'        "pd_years": "2018",'
+'        "pd_object": "ATHENIAN FREEDOM",'
+'        "pd_certificatetype": "MLC Maritime Labour Convention",'
+'        "documentClass": "Certificate",'
+'        "name": "  ATHENIAN FREEDOM_MLC_2018",'
+'        "pd_author": "Hanna Renate Kristensen",'
+'        "pd_creator": "Hanna Renate Kristensen",'
+'        "lastModified": "20.04.2020 20.07",'
+'        "pd_validfrom": "20.02.2018 00.00",'
+'        "pd_validto": "20.02.2019 00.00",'
+'        "pd_objectnameondocument": "ATHENIAN FREEDOM"'
+'      }'
+'    }]}'
);
        
        

        res.setStatusCode(200);
        
        system.debug('response*********'+res);
        return res;

    }
    else if(req.getEndpoint().contains( 'documents/')){
     HttpResponse res = new HttpResponse();
     res.setHeader('Content-Type', 'application/json');
        res.setHeader('fileextension', '.pdf');
         res.setHeader('filename', 'Attachment');
       res.setbody('tAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUJBxfXwH0wFw3nF9fAfTAQAAAAAAAAAAAAAAAF8AXwBhAHQAdABhAGMAaABfAHYAZQByAHMAaQBvAG4AMQAuADAAXwAjADAAMAAwADAAMAAwADAAMwAAAAAAAAA8AAEBCAAAAAwAAACoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABw3nF9fAfTAZAscn18B9MBAAAAAAAAAAAAAAAAXwBfAGEAdAB0AGEAYwBoAF8AdgBlAHIAcwBpAG8AbgAxAC4AMABfACMAMAAwADAAMAAwADAAMAA0AAAAAAAAADwAAQH//////////5wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJAscn18B9MBoFNyfXwH0wEAAAAAAAAAAAAAAABfAF8AYQB0AHQAYQBjAGgAXwB2AGUAcgBzAGkAbwBuADEALgAwAF8AIwAwADAAMAAwADAAMAAwADUAAAAAAAAAPAABAQsAAAAOAAAAkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoFNyfXwH0wHAoXJ9fAfTAQAAAAAAAAAAAAAAAF8AXwBhAHQAdABhAGMAaABfAHYAZQByAHMAaQBvAG4AMQAuADAAXwAjADAAMAAwADAAMAAwADAANgAAAAAAAAA8AAEB//////////+EAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAoXJ9fAfTAdDIcn18B9MBAAAAAAAAAAAAAAAAXwBfAGEAdAB0AGEAYwBoAF8AdgBlAHIAcwBpAG8AbgAxAC4AMABfACMAMAAwADAAMAAwADAAMAA3AAAAAAAAADwAAQANAAAADwAAAHgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAODvcn18B9MB8BZzfXwH0wEAAAAAAAAAAAAAAABfAF8AYQB0AHQAYQBjAGgAXwB2AGUAcgBzAGkAbwBuADEALgAwAF8AIwAwADAAMAAwADAAMAAwADgAAAAAAAAAPAABAf////9WAAAAbQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8BZzfXwH0wEAPnN9fAfTAQAAAAAAAAAAAAAAAF8AXwBzAHUAYgBzAHQAZwAxAC4AMABfADAAMAAxAEEAMAAwADEARgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAqAAIBAgAAABEAAAD/////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAxAAAABAAAAAAAAAAXwBfAHMAdQBiAHMAdABnADEALgAwAF8AMAAwADMANwAwADAAMQBGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACoAAgH///////////////8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC/AAAAMAEAAAAAAABfAF8AcwB1AGIAcwB0AGcAMQAuADAAXwAwADAAMwBCADAAMQAwADIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKgACARAAAAAUAAAA/////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAL0AAABkAAAAAAAAAF8AXwBzAHUAYgBzAHQAZwAxAC4AMABfADAAMAAzAEYAMAAxADAAMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAqAAIBYwAAAP');
         res.setStatusCode(200);
     return res;
        
    }
    else if(req.getEndpoint().endsWith('access-token')){
     HttpResponse res = new HttpResponse();
       
        res.setbody('{'
    +'"access_token": "Jvdbt4n2HfzT_euMnQmJfSksRE1Kwd8du605JPwBYGfkdmQqgGEvC8HzntRdMnrxXfxZgBqaSd2SLLBPlEgVLQ",'
    +'"refresh_token": "gjY81jCY2O-1YsSBaspV3eXDyv-IhasTE8tjVrAEMGfdjtyE89D08WuBBTUn0IUN_jXBwIO_e68sjI259DkvUg",'
    +'"scope": "READ WRITE",'
    +'"token_type": "Bearer",'
    +'"expires_in": 3600}');
    res.setStatusCode(200);
     return res;
        
    }
    return null;
}
}