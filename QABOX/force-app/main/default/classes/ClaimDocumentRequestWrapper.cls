public class ClaimDocumentRequestWrapper {
    Public List<String> propertyResult {get;set;}
    Public List<Object> filter {get;set;}
    public ClaimDocumentRequestWrapper(String UserType, String pd_client, String pd_broker, String pd_years, List<String> documentTypes){
        filter = new List<Object>();
        
        List<Map<String,Object>> propertyFilterGIC;
        List<Map<String,Object>> propertyFilterPARIS;
        Map<String,Object> GicFilterObjMap;
        Map<String,Object> ParisFilterObjMap;
        //GIC request
        //Populate selected year
        Map<String,Object> tempYearMap = new Map<String,Object>{'pd_years' => pd_years};
        //Populate selected logged in broker
        Map<String,Object> tempBrokerMap;
        Map<String,Object> tempRiskBrokerMap;
        propertyFilterGIC = new List<Map<String,Object>>();
        propertyFilterPARIS = new List<Map<String,Object>>();
        //Populate broker
        if(pd_broker != NULL && pd_broker != ''){
            tempBrokerMap = new Map<String,Object>{'pd_broker' => pd_broker.trim()};
            //GIC request
            tempRiskBrokerMap = new Map<String,Object>{'pd_risk' => tempBrokerMap};
            propertyFilterGIC.add(new Map<String,Object>{'pd_claim'=>tempRiskBrokerMap});
            //PARIS request
            Map<String,Object> tempClaimBrokerMap = new Map<String,Object>{'pd_claim' => tempBrokerMap};
            propertyFilterPARIS.add(new Map<String,Object>{'pd_subclaim'=>tempClaimBrokerMap});
        }
        //Populate selected client
        Map<String,Object> tempSelClientMap;
        Map<String,Object> tempRiskSelClientMap;
        if(pd_client != null && pd_client != ''){
            tempSelClientMap = new Map<String,Object>{'pd_client' => pd_client.trim()};
            tempRiskSelClientMap= new Map<String,Object>{'pd_risk' => tempSelClientMap};
            propertyFilterGIC.add(new Map<String,Object>{'pd_claim'=>tempRiskSelClientMap});
            //PARIS request
            Map<String,Object> tempClaimClientMap = new Map<String,Object>{'pd_claim' => tempSelClientMap};
            propertyFilterPARIS.add(new Map<String,Object>{'pd_subclaim'=>tempClaimClientMap});
        } 
        propertyFilterGIC.add(tempYearMap);
        propertyFilterPARIS.add(tempYearMap);
        //Prepare document type filter
        Map<String,String> docTypeClassMap = new Map<String,String>();
        List<String> docClassList = new List<String>();
        List<String> docClassForObjList;
        List<String> docTypeList;
        Map<String,String> docClassTypefieldMap = new Map<String,String>();
        Map<String,List<String>> docClassTypeMap = new Map<String,List<String>>();
        Map<String,Object> tempClasTypeMap;
        
        if(documentTypes == null || documentTypes.size() == 0){
            
            for(MyGardDocumentType__c mgDoc : [SELECT Document_Class__c, Document_Type__c, Document_Type_Field__c FROM MyGardDocumentType__c where Claim_document__c = true]){
                documentTypes.add(mgDoc.Document_Type__c);
            }
        }
        system.debug('documentTypes--'+documentTypes);
        for(MyGardDocumentType__c allDocType : [SELECT Document_Class__c, Document_Type__c, Document_Type_Field__c FROM MyGardDocumentType__c where Document_Type__c IN: documentTypes AND Claim_document__c = true]){
            docTypeClassMap.put(allDocType.Document_Type__c, allDocType.Document_Class__c);
            if(!docClassList.contains(allDocType.Document_Class__c)){
                docClassList.add(allDocType.Document_Class__c);
                docClassTypefieldMap.put(allDocType.Document_Class__c,allDocType.Document_Type_Field__c);
            }
        }
        for(String docClassListObj : docClassList){
            docTypeList = new List<String>();
            for(String docTypeClassMapKeys : docTypeClassMap.KeySet()){
                if(docTypeClassMap.get(docTypeClassMapKeys) == docClassListObj){
                    docTypeList.add(docTypeClassMapKeys);
                }
            }
            docClassTypeMap.put(docClassListObj,docTypeList);
        }
        
        for(String docClassObj : docClassTypeMap.KeySet()){
            //GIC request
            tempClasTypeMap = new Map<String,Object>();
            tempClasTypeMap.put(docClassTypefieldMap.get(docClassObj),docClassTypeMap.get(docClassObj));
            propertyFilterGIC.add(tempClasTypeMap);
            propertyFilterPARIS.add(tempClasTypeMap);
            GicFilterObjMap = new Map<String,Object>();
            docClassList = new List<String>{docClassObj};
            GicFilterObjMap.put('ObjectClass',docClassList);
            GicFilterObjMap.put('PropertyFilter',propertyFilterGIC);
            filter.add(GicFilterObjMap);
            //PARIS request
            ParisFilterObjMap = new Map<String,Object>();
            ParisFilterObjMap.put('ObjectClass',docClassList);
            ParisFilterObjMap.put('PropertyFilter',propertyFilterPARIS);
            filter.add(ParisFilterObjMap);
        }
        system.debug('docClassTypeMap--'+docClassTypeMap);
        propertyResult = new List<String>(); 
        if(!filter.isEmpty()){
            propertyResult.add('pd_client');
            propertyResult.add('pd_claim');
            propertyResult.add('pd_years');
            propertyResult.add('pd_productarea');
            propertyResult.add('pd_object');
            propertyResult.add('pd_certificatetype');
            propertyResult.add('pd_policydocumenttype');
            propertyResult.add('pd_payclddocumenttype');
            propertyResult.add('pd_covercovertypes');
            propertyResult.add('pd_documentid');
            propertyResult.add('100');
            propertyResult.add('0');
            propertyResult.add('pd_author');
            propertyResult.add('pd_creator');
            propertyResult.add('21');
            propertyResult.add('pd_underwritingyear');
            propertyResult.add('pd_validfrom');
            propertyResult.add('pd_validto');
            propertyResult.add('pd_objectnameondocument');
        }
    }
    public ClaimDocumentRequestWrapper(String pd_gardClaimref){
        filter = new List<Object>();
		//GIC
        Map<String,Object> claimRefMapGIC = new Map<String,Object>{'pd_gardclaimreference'=>pd_gardClaimref.trim()};
        Map<String,Object> tempclaimRefMapClaimGIC = new Map<String,Object>{'pd_claim' => claimRefMapGIC};
        Map<String,Object> tempclaimRefMapSubClaimGIC = new Map<String,Object>{'pd_subclaim' => tempclaimRefMapClaimGIC};
        Map<String,Object> filterObjMapGIC = new Map<String,Object>();
        List<Map<String,Object>> propertyFilterListGIC = new List<Map<String,Object>>();
        propertyFilterListGIC.add(tempclaimRefMapSubClaimGIC);
        filterObjMapGIC.put('PropertyFilter',propertyFilterListGIC);
        filterObjMapGIC.put('ObjectClass','cl_paycldclaimrelatedpaymentdocument');
        
		filter.add(filterObjMapGIC);
		
		//PARIS
		Map<String,Object> claimRefMapPARIS = new Map<String,Object>{'pd_gardclaimreference'=>pd_gardClaimref.trim()};
        Map<String,Object> tempclaimRefMapClaimPARIS = new Map<String,Object>{'pd_claim' => claimRefMapPARIS};
        Map<String,Object> filterObjMapPARIS = new Map<String,Object>();
        List<Map<String,Object>> propertyFilterListPARIS = new List<Map<String,Object>>();
        propertyFilterListPARIS.add(tempclaimRefMapClaimPARIS);
        filterObjMapPARIS.put('PropertyFilter',propertyFilterListPARIS);
        filterObjMapPARIS.put('ObjectClass','cl_paycldclaimrelatedpaymentdocument');
		
		filter.add(filterObjMapPARIS);
		
        propertyResult = new List<String>(); 
        if(!filter.isEmpty()){
            propertyResult.add('pd_client');
            propertyResult.add('pd_claim');
            propertyResult.add('pd_years');
            propertyResult.add('pd_productarea');
            propertyResult.add('pd_object');
            propertyResult.add('pd_certificatetype');
            propertyResult.add('pd_policydocumenttype');
            propertyResult.add('pd_covercovertypes');
            propertyResult.add('pd_payclddocumenttype');
            propertyResult.add('pd_documentid');
            propertyResult.add('100');
            propertyResult.add('0');
            propertyResult.add('pd_author');
            propertyResult.add('pd_creator');
            propertyResult.add('21');
            propertyResult.add('pd_underwritingyear');
            propertyResult.add('pd_validfrom');
            propertyResult.add('pd_validto');
            propertyResult.add('pd_objectnameondocument');
        }
    }
}