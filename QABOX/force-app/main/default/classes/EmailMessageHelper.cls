//Created by Preethika for SF-5381/ 5382

public class EmailMessageHelper{
    
    public static void onAfterInsert(Map<Id, String> emailMap, set<Id> caseIds){
    
    String CcAddress ='';
    List<String> emailList = new List<String>();
    List<Case> casesToClone = new List<Case>();
    List<String> CcList = new List<String>();
    
    for(QueueInEmail__c q: QueueInEmail__c.getAll().values()){
    emailList.add(q.email__c);
    }
    
    if(!caseIds.isEmpty() && caseIds.size()>0){
        casesToClone = [Select Id, Status, Team__c, ParentId From Case Where recordType.name=:'Customer Transactions' and Id in: caseIds];
    }
               
    for(Case c: casesToClone){
        CcAddress = emailMap.get(c.Id);
        if(CcAddress!=null)
            CcList = CcAddress.split('; ');
        if(CcList.size()>0 && !CcList.isEmpty()){
            for(String emailStr : CcList){
                if(emailList.contains(emailStr)){
                        CaseCloneController.CcAddress = emailStr;
                        CaseCloneController.cloneCaseWithRelatedRecords(c.Id);
                }
            }
        } 
        if(c.status == 'Closed'){
            CaseCloneController.CcAddress = null;
            CaseCloneController.cloneCaseWithRelatedRecords(c.Id);
        }
          
    }
        
    }
}