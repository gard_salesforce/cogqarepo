@isTest
private class TestAccountOnRisk {
	public static Integer numLevels = 3;
	public static Integer numAccountsPerLevel = 2;
	public static List<Account> testAccounts;
	public static List<Account> lowestLevelAccounts;
	public static Set<Id> lowestLevelAccountIds;

	public static List<Account> setupTestData{
		get{
			if(setupTestData==null){
				//setup top level
				String accountName= 'TestHierarchyAccount';
				Account template = new Account(name=accountName+'0', ParentId = null, On_Risk__c = false, Child_Company_On_Risk__c = false, ShippingStreet='1 Main St.', ShippingState='VA', ShippingPostalCode='12345', ShippingCountry='USA', ShippingCity='Anytown', Description='This is a test account', BillingStreet='1 Main St.', BillingState='VA', BillingPostalCode='12345', BillingCountry='USA', BillingCity='Anytown', AnnualRevenue=10000);
				
				list<Account> prevLevelAccounts = new list<Account>();
				testAccounts = new List<Account>();
				lowestLevelAccounts = new List<Account>();
				for(Integer i=0;i<numLevels;i++){
					list<Account> levelAccounts = new list<Account>();		
					
					if(prevLevelAccounts==null || prevLevelAccounts.size()==0){
						//Top level
						for(Integer j=0;j<numAccountsPerLevel;j++){
							levelAccounts.add(template.clone(false,true));
						}				
					}else{
						//iterate through each account in the level before
						for(Account parent:prevLevelAccounts){
							//clone the template numAccountsPerLevel times
							for(Integer j=0;j<numAccountsPerLevel;j++){
								Account thisAccount = template.clone(false,true);
								thisAccount.ParentId = parent.Id;
								levelAccounts.add(thisAccount);	
													
							}
						}
						
					}
					if(levelAccounts!=null && levelAccounts.size()>0){
						insert levelAccounts;
						prevLevelAccounts = levelAccounts.deepClone(true);
						testAccounts.addAll(levelAccounts.deepClone(true));
						if(i==numLevels-1){
							lowestLevelAccounts.addAll(levelAccounts.deepClone(true));
							if(lowestLevelAccountIds==null){lowestLevelAccountIds = new Set<Id>();}
							for(Account acc:lowestLevelAccounts){
								lowestLevelAccountIds.add(acc.Id);
							}
						}
					}
					
				}
				System.Debug('Created ' + testAccounts.size() + ' accounts for testing.');
			
			}
			return setupTestData;			
		}
		set;

		
	}
	
	static testMethod void testAccountOnRiskTrigger() {
     	Test.startTest();
        MDM_Settings__c testSetting = MDMSettingSupport.GetSetting(UserInfo.getUserId());

		testSetting.Triggers_Enabled__c = true;
		testSetting.AccountOnRisk_Trigger_Enabled__c = true;
		
		upsert testSetting;
		
        ObjectTree.TestTrigger = true;
        List<Account> testData = setupTestData;
        Test.stopTest();        
    }
}