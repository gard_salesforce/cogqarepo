@isTest
Class TestMyObjectDetailsAllCoversCtrl
{    
    public static testmethod void cover()
    { 
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        GardTestData gtdInstance = new GardTestData();
        gtdInstance.commonRecord();
        gtdInstance.customsettings_rec();
        GardTestData.test_object_1st.guid__c = '8ce8ac89-a6fd-1836-9e07';
        update GardTestData.test_object_1st; 
        system.assertEquals(GardTestData.test_object_1st.guid__c , '8ce8ac89-a6fd-1836-9e07');
        System.runAs(GardTestData.brokerUser)
        {   
            test.startTest();
            ApexPages.StandardController scontroller = new ApexPages.StandardController(GardTestData.brokerAcc);
            Pagereference pge = page.MyObjectDetailsAllCovers;
            pge.getparameters().put('objid',GardTestData.test_object_1st.guid__c);
            test.setcurrentpage(pge);
            MyObjectDetailsAllCoversCtrl MyObjects = new MyObjectDetailsAllCoversCtrl();
            MyObjectDetailsAllCoversCtrl MyObject = new MyObjectDetailsAllCoversCtrl(scontroller);            
            system.debug('**************MyObject.objectGUIID'+MyObject.objectGUIID);
            MyObject.underwrId='abc#xyz';
            MyObject.jointSortingParam = 'ASC##Product_Name__c';
            string value = 'ASC';
            MyObject.setSortDirection(value);
            String sDir = MyObject.getSortDirection();
            System.assertEquals('ASC',sDir);
            MyObject.underwriterPopup();
            MyObject.getCovers(); 
            MyObject.ViewData();
            MyObject.exportToExcel();
            MyObject.setpageNumber();
            MyObject.hasNext=true;
            MyObject.hasPrevious=true;
            MyObject.pageNumber=100;
            MyObject.navigate();
            MyObject.first();
            MyObject.next();
            MyObject.last();
            MyObject.previous();
            MyObject.printData();
            MyObjectDetailsAllCoversCtrl.getUserName();
            MyObject.closeconfirmPopUp();
            MyObject.displayEditPopUp();
            MyObject.caseTestId = GardTestData.brokerCase.id;
            MyObject.sendMailforEditObject();
            MyObject.createFavourites();
            MyObject.deleteFavourites();
            Myobject.downloadVcardAllCover();
            test.stopTest();
        }
    }
}