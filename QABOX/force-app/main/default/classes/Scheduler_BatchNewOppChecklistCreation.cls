global class Scheduler_BatchNewOppChecklistCreation implements Schedulable{
    
    public static String sched = '0 00 01 * * ?';

    global static String scheduleMe() {
        Scheduler_BatchNewOppChecklistCreation SC = new Scheduler_BatchNewOppChecklistCreation(); 
        return System.schedule('My batch Job', sched, SC);
    }
    global void execute(SchedulableContext sc) {
        if(!Test.isRunningTest())  
        {
            BatchNewOppChecklistCreation bhd= new BatchNewOppChecklistCreation();
            ID batchprocessid = Database.executeBatch(bhd,50);   
        }        
      }
}