/*****************************************************************************************************
* Class Name: CustomerFeedbackFromAutResponses
* Created By: Arpan Muhuri
* Date:  03.12.2020
* Description: SF-5859 - Used to create customer feedback from survey responses(Claims and event)
* Test class : TestCustomerFeedbackFromAutResponses
*******************************************************************************************************
*  Version    Developer        Date          Description
=======================================================================================================
*  1.0        Arpan Muhuri      03.12.2020    Created (for SF-5707 and SF-5865)
*  2.0        Arpan Muhuri      17.06.2021    SF-5967
*/
public class CustomerFeedbackFromAutResponses{
    
    private static final String claimSurveyTypeStr = 'Claim';
    private static final String eventSurveyTypeStr = 'Event';
    private static Map<String,List<String>> permittedQuestionType;//responseTypeToPermittedQuestionType Map; if a response returns null, add all questions, otherwise the specified ones only
    private static List<Id> userList = new List<Id>();
    static{
        permittedQuestionType = new Map<String,List<String>>{//later, we would need to make it configurable
            claimSurveyTypeStr => new List<String>{'NetPromoter'}
        };
    }
    
    //dispatcher created to prevent creation of different class for PB invocation
    @InvocableMethod
    public static void dispatcher(List<Id> responseIds){
        
        //fetch records
        List<GetFeedback_Aut__Response__c> responses = getReponseRecords(responseIds);
        
        //segregate responses
        List<GetFeedback_Aut__Response__c> eventResponses = new List<GetFeedback_Aut__Response__c>();
        List<GetFeedback_Aut__Response__c> claimResponses = new List<GetFeedback_Aut__Response__c>();
        for(GetFeedback_Aut__Response__c response : responses){
            if(response.GetFeedback_Aut__Survey__r.Survey_Type__c.equalsIgnoreCase(claimSurveyTypeStr)) claimResponses.add(response);
            if(response.GetFeedback_Aut__Survey__r.Survey_Type__c.equalsIgnoreCase(eventSurveyTypeStr)) eventResponses.add(response); 
        }
        
        //dispatch to processing method
        createClaimCustFeedbacks(claimResponses);
        createEventCustFeedbacks(eventResponses);
    }
    
    private static List<GetFeedback_Aut__Response__c> getReponseRecords(List<Id> responseIds){
        
        List<GetFeedback_Aut__Response__c> responses = [SELECT GetFeedback_Aut__Contact__r.AccountId, Survey_Member__r.createdById,GetFeedback_Aut__Survey__r.Survey_Type__c,Event_Id__c,Claim__c,GetFeedback_Aut__Survey__c,
                                                        Claim__r.Claim_Adjuster__c, Claim__r.Claims_handler__c, Claim__r.Account__r.Key_Claims_Contact__c,
                                                        (SELECT GetFeedback_Aut__QuestionTitle__c, GetFeedback_Aut__GridItem__c, GetFeedback_Aut__DisplayValue__c,GetFeedback_Aut__QuestionType__c
                                                         FROM GetFeedback_Aut__Answers__r ORDER BY GetFeedback_Aut__QuestionTitle__c ASC)
                                                        FROM GetFeedback_Aut__Response__c WHERE Id IN: responseIds];
        return responses;
    }
    
    private static void createClaimCustFeedbacks(List<GetFeedback_Aut__Response__c> responses){//for SF-5865
        if(responses.isEmpty()) return;
        List<String> permittedQuestionTypes = permittedQuestionType.get(claimSurveyTypeStr);
        system.debug('permittedQuestionTypes--'+permittedQuestionTypes);
        system.debug('claimSurveyTypeStr--'+claimSurveyTypeStr);
        Map<GetFeedback_Aut__Response__c,Customer_Feedback__c> responseCfMap = new Map<GetFeedback_Aut__Response__c,Customer_Feedback__c>();
        for(GetFeedback_Aut__Response__c response : responses){
            String cfDescription = '';
            
            List<GetFeedback_Aut__Answer__c> answerList = response.GetFeedback_Aut__Answers__r;
            if(!answerList.isEmpty()){
                for(GetFeedback_Aut__Answer__c answer : answerList){
                    Boolean isAnswerPermitted = (permittedQuestionTypes == null || permittedQuestionTypes.contains(answer.GetFeedback_Aut__QuestionType__c));
                    system.debug('answer.GetFeedback_Aut__QuestionType__c--'+answer.GetFeedback_Aut__QuestionType__c);
                    if(isAnswerPermitted){
                        cfDescription = cfDescription + answer.GetFeedback_Aut__QuestionTitle__c + ' : '+answer.GetFeedback_Aut__DisplayValue__c+'\n';
                    }
                }
                system.debug('cfDescription--'+cfDescription);
                if(String.isNotEmpty(cfDescription)){
                    Customer_Feedback__c feedback = new Customer_Feedback__c(
                        Title__c = 'Claim Survey',
                        Feedback_for_Gard_Department__c = 'Claims',
                        Source__c = 'Surveys',
                        Survey_Response__c = response.Id,
                        Responsible__c = '00520000001K5hG', 
                        From_Company__c = response.GetFeedback_Aut__Contact__r.AccountId,
                        Type_of_feedback__c = 'Not assessed',
                        Customer_feedback_type__c = 'Open',
                        Key_feedback_issue_area__c = 'Service',
                        Description__c = cfDescription
                    ); //Responsible__c added for SF-6797
                    
                        if(response.Claim__r.Claim_Adjuster__c != null)
                            userList.add(response.Claim__r.Claim_Adjuster__c);
                            //feedback.Claim_Adjuster__c = response.Claim__r.Claim_Adjuster__c;
                        if(response.Claim__r.Claims_handler__c != null)
                            userList.add(response.Claim__r.Claims_handler__c);
                            //feedback.Claim_handler__c = response.Claim__r.Claims_handler__c;
                        if(response.Claim__r.Account__r.Key_Claims_Contact__c != null)
                            userList.add(response.Claim__r.Account__r.Key_Claims_Contact__c);
                            //feedback.Responsible_claim_contact__c = response.Claim__r.Account__r.Key_Claims_Contact__c;
                            
                    responseCfMap.put(response,feedback);
                }
            }
        }
        system.debug('responseCfMap--'+responseCfMap);
        if(!responseCfMap.isEmpty()){
            insert responseCfMap.values();
            //sendEmail(userList,responseCfMap.values()[0].Id);
        }
    }
    
    private static void createEventCustFeedbacks(List<GetFeedback_Aut__Response__c> responses){//for SF-5707
        if(responses.isEmpty()) return;
        
        List<String> permittedQuestionTypes = permittedQuestionType.get(eventSurveyTypeStr);
        Map<GetFeedback_Aut__Response__c,Customer_Feedback__c> responseCfMap = new Map<GetFeedback_Aut__Response__c,Customer_Feedback__c>();
        for(GetFeedback_Aut__Response__c response : responses){
            String cfDescription = '';
            
            List<GetFeedback_Aut__Answer__c> answerList = response.GetFeedback_Aut__Answers__r;
            if(!answerList.isEmpty()){
                for(GetFeedback_Aut__Answer__c answer : answerList){
                    Boolean isAnswerPermitted = (permittedQuestionTypes == null || permittedQuestionTypes.contains(answer.GetFeedback_Aut__QuestionType__c));
                    
                    if(isAnswerPermitted){
                        cfDescription = cfDescription + answer.GetFeedback_Aut__QuestionTitle__c + '\n';
                        if(String.isNotEmpty(answer.GetFeedback_Aut__GridItem__c)) cfDescription += answer.GetFeedback_Aut__GridItem__c + '\n';
                        cfDescription += '\n';
                        cfDescription += answer.GetFeedback_Aut__DisplayValue__c + '\n' + '\n';
                    }
                }
                
                if(String.isNotEmpty(cfDescription)){
                    Customer_Feedback__c feedback = new Customer_Feedback__c(
                        Feedback_for_Gard_Department__c = 'Claims',
                        From_Company__c = response.GetFeedback_Aut__Contact__r.AccountId,//maybe a change here
                        Key_feedback_issue_area__c = 'Service',
                        Survey_Response__c = response.Id,
                        Responsible__c = '00520000001K5hG', 
                        Source__c = 'Surveys',
                        Title__c = 'KCC Survey Response',//change here
                        //Type_of_feedback__c = 'Neutral feedback',//SF-6185
                        Type_of_feedback__c = 'Not assessed',
                        Customer_feedback_type__c = 'Open',
                        Description__c = cfDescription
                    );//Responsible__c modified from response.Survey_Member__r.createdById TO Olga's user id for SF-6797
                    responseCfMap.put(response,feedback);
                }
            }
        }
        
        if(!responseCfMap.isEmpty()) insert responseCfMap.values();
    }
    //Send claim feedback email to CH,CH Manager,CA,CA Manager,CX team and RCC of the client (if there is one)
    public static void sendEmail(List<Id> userList,Id feedbackId){
        Customer_Feedback__c feedback = new Customer_Feedback__c();
        feedback = [SELECT Id,Name,Title__c,Customer_feedback_type__c, Responsible__r.name, Type_of_feedback__c,From_Company__r.name,Company_to__r.name FROM Customer_Feedback__c WHERE Id =: feedbackId];
        //------------------------------------------
        set<String> NotifcationToAddress = new set<String>();
        set<Id> parentAndCXRoleIds = new set<Id>();
        for(User usr:[SELECT email,UserRole.ParentRoleId FROM User WHERE Id IN: userList]){
            NotifcationToAddress.add(usr.email);
            parentAndCXRoleIds.add(usr.UserRole.ParentRoleId);
        }
        parentAndCXRoleIds.add(Label.Customer_Experience_Role_Id);
        for(User mgrUsr:[SELECT email FROM User WHERE UserRoleId IN: parentAndCXRoleIds]){
            NotifcationToAddress.add(mgrUsr.email);
        }
        system.debug('NotifcationToAddress--'+NotifcationToAddress);
        integer count = 1;
        for(String emailIds:NotifcationToAddress){
            system.debug('NotifcationToAddress--'+count+':'+emailIds);
            count++;
        }
        system.debug('feedback--'+feedback);
        //Create email
        String emailBody = '';
        String mail_Subject = '';
        List<EmailTemplate> notifyUsers = new List<EmailTemplate>([SELECT Id, body, subject FROM EmailTemplate WHERE Id =: Label.Claim_customer_feedback_email_template_id]);
        emailBody = notifyUsers[0].body;
        system.debug('feedback.Name+feedback.Title__c--'+feedback.Name+' '+feedback.Title__c);
        mail_Subject = notifyUsers[0].subject.replace('{!Customer_Feedback__c.Name}',feedback.Name).replace('{!Customer_Feedback__c.Title__c}',feedback.Title__c);
        if(feedback.Name != null && feedback.Name != '')
            emailBody = emailBody.replace('{!Customer_Feedback__c.Name}',feedback.Name);
        else
            emailBody = emailBody.replace('{!Customer_Feedback__c.Name}','');
        if(feedback.Title__c != null && feedback.Title__c != '')
            emailBody = emailBody.replace('{!Customer_Feedback__c.Title__c}',feedback.Title__c);
        else
            emailBody = emailBody.replace('{!Customer_Feedback__c.Title__c}','');
        
        system.debug('feedback.Customer_feedback_type__c--'+feedback.Customer_feedback_type__c);
        if(feedback.Customer_feedback_type__c != null)
            emailBody = emailBody.replace('{!Customer_Feedback__c.Customer_feedback_type__c}',feedback.Customer_feedback_type__c);
        
        system.debug('Customer_Feedback__c.Responsible__c--'+feedback.Responsible__c);
        if(feedback.Responsible__r.name != null)
            emailBody = emailBody.replace('{!Customer_Feedback__c.Responsible__c}',feedback.Responsible__r.name);
        
        system.debug('feedback.Type_of_feedback__c--'+feedback.Type_of_feedback__c);
        if(feedback.Type_of_feedback__c != null)
            emailBody = emailBody.replace('{!Customer_Feedback__c.Type_of_feedback__c}',feedback.Type_of_feedback__c);
        
        system.debug('feedback.From_Company__c--'+feedback.From_Company__c);
        if(feedback.From_Company__r.name != null)
            emailBody = emailBody.replace('{!Customer_Feedback__c.From_Company__c}',feedback.From_Company__r.name);
        else
            emailBody = emailBody.replace('{!Customer_Feedback__c.From_Company__c}','');
        
        system.debug('feedback.Company_to__c--'+feedback.Company_to__c);
        if(feedback.Company_to__r.name != null)
            emailBody = emailBody.replace('{!Customer_Feedback__c.Company_to__c}',feedback.Company_to__r.name);
        else
            emailBody = emailBody.replace('{!Customer_Feedback__c.Company_to__c}','');
        
        system.debug('feedback.Id--'+feedback.Id);
        emailBody = emailBody.replace('{!Customer_Feedback__c.Link}',Label.InternalLightningOrgURL.replace('Case','Customer_Feedback__c')+feedback.Id+'/view');
        system.debug('emailBody--'+emailBody);
        Messaging.SingleEmailMessage notifMail = new Messaging.SingleEmailMessage(); 
        notifMail.setToAddresses(new List<String>(NotifcationToAddress));
        notifMail.setReplyTo(CommonVariables__c.getInstance('ReplyToMailId').Value__c);
        notifMail.setPlainTextBody(emailBody);
        notifMail.setSubject(mail_Subject);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {notifMail}); 
    
    }        
}