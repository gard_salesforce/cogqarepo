global class BatchInsertMyCover2 implements Database.Batchable<sObject> {

    //Declaring variables.
    global String strquery;
    global String  strTrueOrFalse;
    global List<String> accountIdList;
    
    //Defining constructor
    global BatchInsertMyCover2 (String truefalse, List<String> accountIds){                  
    
        strTrueOrFalse = truefalse;
        accountIdList = new List<String>();
        
        //If from Trigger etc for RealTime scenarios
        if(accountIds != null && accountIds.size() > 0){
            for(String acc_id : accountIds){
                accountIdList.add(acc_id);
            }
            System.debug('Accounts from RTime Triggers - ' + accountIdList);
        }
        
        //Else it's invoked from batch
        else{
            Datetime lastRunAt = Datetime.now().addHours(-6);
            System.debug('lastRunAt -->' + lastRunAt);
            
            for(Asset a : [SELECT accountId from Asset where LastModifiedDate > :lastRunAt]){    //pull all records updated after last run
                accountIdList.add(a.accountId);
            }
            System.debug('Accounts for last hour - ' + accountIdList);
        }
        
        strquery = 'SELECT id,Company_Role__c FROM Account where id in :accountIdList';
        System.debug('strquery -->' + strquery);    
    }
    
    //Overriding the start method
    global Database.QueryLocator start(Database.BatchableContext BC){               
        return Database.getQueryLocator(strquery);        
    }
    
    //Override the execute method
    global void execute(Database.BatchableContext BC, List<account> scope){  
        System.debug('I am here 1');
        Boolean blBrokerView = false;
        List<MyCoverList__c> lstMyCover = new List<MyCoverList__c>();
        
        List<MyCoverList__c> delLstMyCover= new List<MyCoverList__c>();
        List<MyCoverList__c> delLstMyCoverAll = new List<MyCoverList__c>();
        try{
            System.debug('I am here 2');
            for(account acc:scope){
                System.debug('I am here 3');
                Contact myContact;
                List<Object__c> lstObject = new List<Object__c>();
                List<Contract> lstContract = new List<Contract>();
                List<Contract> lstContractClient = new List<Contract>();        
                
                if(acc != null && acc.Company_Role__c != null &&  acc.Company_Role__c.contains('Broker')){
                    blBrokerView = true;
                    lstContractClient = [Select Id, Client__c from Contract Where Broker__c =: acc.id];
                    System.debug('I am here 4');
                    System.debug('lstContractClient --' + lstContractClient );
                    
                    if(Boolean.valueof(strTrueOrFalse)==true){
                        delLstMyCover = [Select id from MyCoverList__c Where broker_id__c=:acc.id and onrisk__c=true];
                    }
                    else{
                        delLstMyCover = [Select id from MyCoverList__c Where broker_id__c=:acc.id and onrisk__c=false];
                    }
                }
                else{
                    System.debug('I am here 5');
                    blBrokerView = false;          
                    lstContractClient = [Select id from Contract Where Client__c =: acc.id /*AND broker__c=null*/ ];
                    if(Boolean.valueof(strTrueOrFalse)==true){
                        delLstMyCover = [Select id from MyCoverList__c Where client_id__c=:acc.id /* and broker_id__c=null*/  and onrisk__c=true];
                    }
                    else{
                        delLstMyCover = [Select id from MyCoverList__c Where client_id__c=:acc.id /* and broker_id__c=null*/ and onrisk__c=false];
                    } 
                }
                System.debug('I am here 6');
                for(MyCoverList__c mcl : delLstMyCover){
                    delLstMyCoverAll.add(mcl);
                }
                System.debug('I am here 7');
                System.debug('delLstMyCoverAll--' + delLstMyCoverAll);
                Set<ID> setCoverID = new Set<ID>();
                for(Contract obj : lstContractClient){
                    setCoverID.add(obj.ID);
                }
                system.debug('setCoverID'+setCoverID.size());
        
                String strforClient = '';
                String strforClientGroupby = '';
                strforClient = 'Agreement__r.Client__r.Name clientname, Agreement__r.Client__r.Id clientId,';
                strforClientGroupby = 'Agreement__r.Client__r.Name,Agreement__r.Client__r.Id,';
                
                List<Boolean> lstblOnrisk = new List<Boolean>();
                lstblOnrisk.add(Boolean.valueof(strTrueOrFalse));
                String strCondition = ' AND On_risk_indicator__c IN : lstblOnrisk '; 
                
                //Run the aggregate query to retrieve asset info grouped by parameters
                String sSoqlQuery = 'SELECT MAX(Expiration_Date__c) expdate,agreement__c AgId, '+strforClient+' UnderwriterNameTemp__c underwriter, UnderwriterId__c underwriterId,Product_Name__c prod,Agreement__r.Business_Area__c busarea,'+ 
                                    'Agreement__r.Broker__r.Name AggName,Agreement__r.Broker__r.Id AggId, Agreement__r.Policy_Year__c plcyyr, '+
                                    'Claims_Lead__c claim, Gard_Share__c grdshr, On_risk_indicator__c rskind,Agreement__r.Shared_With_Client__c sharedwithclient,  '+
                                    'Count(id) FROM Asset Where Agreement__c IN : setCoverID '+strCondition+
                                    'GROUP BY agreement__c,'+strforClientGroupby+' UnderwriterNameTemp__c, UnderwriterId__c, Product_Name__c, '+
                                    'Agreement__r.Business_Area__c,'+
                                    'Agreement__r.Broker__r.Name,Agreement__r.Broker__r.Id,'+
                                    'Agreement__r.Policy_Year__c,'+
                                    'Claims_Lead__c, Gard_Share__c, On_risk_indicator__c,Agreement__r.Shared_With_Client__c ';
                
                AggregateResult[] groupedResults = database.query(sSoqlQuery);
                System.debug('GroupedResults --> '+groupedResults);
                System.debug('GroupedResults --> '+groupedResults); 
                
                //Looping the aggregateresult to generate mycover list records
                for (AggregateResult ar : groupedResults){
                    if(blBrokerView == false){
                        lstMyCover.add(new MyCoverList__c(
                        AgreementId__c = string.valueof(ar.get('AgId')),
                        client__c=string.valueof(ar.get('clientname')),
                        client_id__c=string.valueof(ar.get('clientId')),
                        underwriter__c=string.valueof(ar.get('underwriter')),
                        UnderwriterId__c=string.valueof(ar.get('underwriterId')),
                        product__c=string.valueof(ar.get('prod')),
                        productarea__c=string.valueof(ar.get('busarea')),
                        broker__c=string.valueof(ar.get('AggName')),
                        broker_id__c=string.valueof(ar.get('AggId')), 
                        expirydate__c=string.valueof(ar.get('expdate')),
                        policyyear__c=Integer.valueof(ar.get('plcyyr')),                                           
                        claimslead__c=string.valueof(ar.get('claim')),
                        gardshare__c=Double.valueof(ar.get('grdshr')),
                        onrisk__c=Boolean.valueof(ar.get('rskind')),
                        viewobject__c=Integer.valueOf(ar.get('expr0')),
                        Shared_With_Client__c=Boolean.valueof(ar.get('sharedwithclient'))
                        ));
                    }
                    else{
                        lstMyCover.add(new MyCoverList__c(
                        AgreementId__c = string.valueof(ar.get('AgId')),
                        client__c=string.valueof(ar.get('clientname')),
                        client_id__c=string.valueof(ar.get('clientId')),
                        underwriter__c=string.valueof(ar.get('underwriter')),
                        UnderwriterId__c=string.valueof(ar.get('underwriterId')),
                        product__c=string.valueof(ar.get('prod')),
                        productarea__c=string.valueof(ar.get('busarea')),
                        broker__c=string.valueof(ar.get('AggName')),
                        broker_id__c=string.valueof(ar.get('AggId')),
                        expirydate__c=string.valueof(ar.get('expdate')),
                        policyyear__c=Integer.valueof(ar.get('plcyyr')),                                           
                        claimslead__c=string.valueof(ar.get('claim')),
                        gardshare__c=Double.valueof(ar.get('grdshr')),
                        onrisk__c=Boolean.valueof(ar.get('rskind')),
                        viewobject__c=Integer.valueOf(ar.get('expr0')),
                        Shared_With_Client__c=Boolean.valueof(ar.get('sharedwithclient'))
                        ));
                    }
                }
            }
    
            //Delete the old records
            Database.delete( delLstMyCoverAll, false);
            
            //insert lstMyCover;
            Database.insert( lstMyCover, false);
            
            System.debug('lstMyCover.size() -->'+lstMyCover.size());
            
        }
        catch(Exception ex){ 
            System.debug('Exception occurred -->' + ex);           
        }
        
    }

    //Placeholder for overriding the finish method
    global void finish(Database.BatchableContext BC){}

}