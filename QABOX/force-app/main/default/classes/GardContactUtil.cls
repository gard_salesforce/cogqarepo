public class GardContactUtil{
 
    @future
    public static void updateGardContactsFuture(Set<id> userIds)
    {
        updateGardContacts(userIds);
    }
    public static void updateGardContacts(Set<id> userIds)
    {
        Map<id,Gard_Contacts__c> gardContactsMap;
        Map<id,User> newMap = new Map<id,User> ([Select ContactId__c,Email,city,phone,isActive,MobilePhone,firstname,lastname,Title,
                                                    CommunityNickname from User where id In: userIds]);
        List<String> gardContactIds = new List<String>();
        for(User usr: newMap.values())
        {
            gardContactIds.add(usr.contactId__c);
        }
        
        // used for deactivated user's gard contact data - taken from custom setting
        GardContacts__c deactivatedGardContactValues = GardContacts__c.getValues('DeactivatedUsers');
        // user respective gard contact data
        gardContactsMap = new Map<id,Gard_Contacts__c>([Select Id, isActive__c, firstname__c , lastname__c, MobilePhone__c, Email__c, Title__c, 
                                                              Nick_Name__c,  office_city__c, phone__c,Portal_Image__c   
                                                              from Gard_Contacts__c where ID IN: gardContactIds]);
        for(User usr: newMap.values())
        {
            // update data of gard contact when user is activated from user data
            if(usr.isActive && gardContactsMap != null && gardContactsMap.size()>0 && newMap != null && newMap.size()>0)
            {
                gardContactsMap.get(usr.ContactId__c).Email__c = newMap.get(usr.id).Email;
                gardContactsMap.get(usr.ContactId__c).Office_city__c = newMap.get(usr.id).city;
                gardContactsMap.get(usr.ContactId__c).Phone__c = newMap.get(usr.id).Phone;
                gardContactsMap.get(usr.ContactId__c).isActive__c = newMap.get(usr.id).isActive;
                gardContactsMap.get(usr.ContactId__c).MobilePhone__c = newMap.get(usr.id).MobilePhone;
                gardContactsMap.get(usr.ContactId__c).firstname__c = newMap.get(usr.id).firstname;
                gardContactsMap.get(usr.ContactId__c).lastname__c = newMap.get(usr.id).lastname;
                gardContactsMap.get(usr.ContactId__c).Title__c = newMap.get(usr.id).Title;
                gardContactsMap.get(usr.ContactId__c).Nick_Name__c = newMap.get(usr.id).CommunityNickname;
            }
            // update gard contact from custom setting data when user is deactivated
            else if(!usr.isActive && gardContactsMap != null && gardContactsMap.size()>0 && deactivatedGardContactValues != null)
            {  
                gardContactsMap.get(usr.ContactId__c).Email__c = deactivatedGardContactValues.Email__c;
                gardContactsMap.get(usr.ContactId__c).Office_city__c = deactivatedGardContactValues.Office_city__c;
                gardContactsMap.get(usr.ContactId__c).Phone__c = deactivatedGardContactValues.Phone__c;
                gardContactsMap.get(usr.ContactId__c).isActive__c = false;   
            }
        }
        system.debug('********to be updated data******* gardContactsMap -->'+gardContactsMap);
        if(gardContactsMap != null && gardContactsMap.size() >0)
            update gardContactsMap.values();  
    }
    /* 
        GardContacts__c deactivatedGardContactValues = GardContacts__c.getValues('DeactivatedUsers');
    
        List<Gard_Contacts__c> deactivatedGardContacts =[Select Id, isActive__c, firstname__c , lastname__c, MobilePhone__c, Email__c, Title__c, 
                                                              Nick_Name__c,  office_city__c, phone__c,Portal_Image__c   
                                                              from Gard_Contacts__c where ID IN: allGardContactIds ];
             
        for( Gard_Contacts__c singleGardContact : deactivatedGardContacts ){
            System.Debug('*** About to update gard contacts with default values');
           // singleGardContact.Email__c= acc.Name  + deactivatedGardContactValues.Email_Domain__c;
            if(singleGardContact != null && deactivatedGardContactValues != null)
            {
            singleGardContact.Email__c= deactivatedGardContactValues.Email__c;
            singleGardContact.Office_city__c= deactivatedGardContactValues.Office_city__c;
            singleGardContact.Phone__c= deactivatedGardContactValues.Phone__c; 
            
            //singleGardContact.Portal_Image__c = null;
            singleGardContact.MobilePhone__c = '';
            singleGardContact.isActive__c = false;
            }
        }     
        
        if (Test.isRunningTest()) {
            System.runAs(new User(Id = Userinfo.getUserId())) {
                update deactivatedGardContacts;
            }
        } else {
            System.Debug('*** About to update gard contacts');
            update deactivatedGardContacts;
        }*/
}