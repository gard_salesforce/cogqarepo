/**************************************************************************
* Author  : Phil Spenceley
* Company : cDecisions
* Date    : 26/07/2013
***************************************************************************/
    
/// <summary>
///  This interface can be implemented to allow the XmlSerializer class to perform some basic XmlSerialization of objects
/// </summary>
public interface XmlSerializable {
    
    object get(string fieldName);
    boolean put(string fieldName, object value);
    Set<String> getFields();
    
}

/*
//Example Implementation 

public class Company implements XmlSerializable {
    
    public string Id { get; set; }
    public string Name { get; set; }
    
    public object get(string fieldName) {
        Map<string, object> thisobjectmap = new Map<string, object> 
        { 
            'Id' => this.Id, 
            'Name' => this.Name
        };
        return thisobjectmap.get(fieldname);
    }
    
    public boolean put(string fieldName, object value) {
        if (fieldName == 'Id') {
            this.Id = String.valueOf(value);
        } else if (fieldName = 'Name') {
            this.Name = String.valueOf(value);            
        } else {
            return false;
        }
        return true;
    }
    
    public Set<string> getFields() {
        Set<string> fields = new Set<string> 
        { 
            'Id', 
            'Name'
        };
        
        return fields;
    }
}

*/