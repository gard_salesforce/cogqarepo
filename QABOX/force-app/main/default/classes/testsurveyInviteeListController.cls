@isTest
public class testsurveyInviteeListController{

    @testSetup  static void setUpTestData(){
        Account acc = new Account(Name='test',
                                  Role_Client__c=true,
                                  Active__c = true,
                                  Company_Role__c = 'Client',
                                  Sub_Roles__c = ''
                                  );
        insert acc;
        
        Contact con = new Contact( FirstName='tsat_1',
                                   LastName='chak',
                                   MailingCity = 'London',
                                   MailingCountry = 'United Kingdom',
                                   MailingPostalCode = 'SE1 1AE',
                                   MailingState = 'London',
                                   MailingStreet = '4 London Road',
                                   AccountId = acc.Id,
                                   Email = 'test321@gmail.com'
                                  );
        insert con;
        Event ev = new Event();
        ev.ActivityDate = system.today();
        ev.ActivityDateTime = DateTime.newInstance(2020, 12, 30);
        ev.EndDateTime = DateTime.newInstance(2020, 12, 30 ,1, 0, 0);
        ev.CurrencyIsoCode = 'USD';
        ev.Description = 'KCC Survey';
        ev.Subject = 'Test KCC Survey';
        ev.Location = 'Barcelona';
        ev.OwnerId = userInfo.getUserId();
        ev.WhatId = acc.Id;
        insert ev;
        
        EventRelation evRel = new EventRelation(
                                                EventId = ev.id,
                                                RelationId = con.id
                                               );
        insert evRel;
    }
    @isTest public static void surveyInviteeListCtrlTestMethod(){
        Id evtId = [SELECT Id FROM Event Limit 1].id;
        List<Id> conIdList = new List<Id>();
        for(contact con:[SELECT Id FROM Contact]){
            conIdList.add(con.Id);
        }
        system.debug('Event Id : '+evtId+' --contact: '+[SELECT Id,Name from Contact]);
        surveyInviteeListController.createSurveyInvitees(conIdList,evtId);
        surveyInviteeListController.fetchSurveyMembers(evtId);
        surveyInviteeListController.fetchEventName(evtId);
        surveyInviteeListController.fetchInvitees(evtId);
        surveyInviteeListController.insertEmailMsg(new List<Id>{[SELECT Id FROM Survey_Member__c LIMIT 1].Id});
        
    }
}