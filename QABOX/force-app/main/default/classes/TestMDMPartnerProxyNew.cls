@IsTest
private class TestMDMPartnerProxyNew {
    private static Account a = TestMDMSyncAccountHandlerNew.getTestAccount();
    
    @isTest(SeeAllData=true)
    Private static void singleCalloutViaMDMSyncAccountHandlerNew(){
        a.Company_Role__c = 'Client';
        upsert a; 
        //Account a2=[select id from Account where id='0010E00000I0jhY'];
        List<Account> newAccounts = new List<Account> {a};
        Map<id, Account> newAccountsMap = new Map<id, Account> (newAccounts);
        Set<ID> accIDSet = newAccountsMap.keySet();
        /*
        try{
            MDMPartnerProxyNew.singleCallOut('RequestString', accIdSet, true, true, true, true);
        }catch(Exception ex) {
        }
        */
    }
    
    private static testmethod void testSetAccountsSyncFailed(){
            List<Account> accList = new List<Account> { a };
            MDMPartnerProxyNew.SetAccountsSyncFailed(accList,'stringParameter');
    }
    
    private static testmethod void testGetAccountById(){
            upsert a;
            new MDMPartnerProxyNew().getAccountById(a.id,'mdmChangeResponse');
    }
    
    private static testmethod void MDMResponseMethods(){
        MDMPartnerProxyNew.MDMResponse mdmRes=new MDMPartnerProxyNew.MDMResponse();
        mdmRes.put('id','123456789ABCDEF');
        mdmRes.put('success','false');
        mdmRes.put('error','TestError');
        mdmRes.get('error');
        mdmRes.getFields();
    }

    
    private static testmethod void UpsertPartnerAsync_Success() {  
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, true);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMPartnerProxyNew.MDMUpsertPartnerAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Synchronisation_Status__c FROM Account WHERE id = :id];
        //1system.assertEquals('Synchronised', a.Synchronisation_Status__c, 'Account Sync Status = Synchronised');
        
    }
    
    private static testmethod void UpsertPartnerAsync_Failure() {  
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, false);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMPartnerProxyNew.MDMUpsertPartnerAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Synchronisation_Status__c FROM Account WHERE id = :id];
        //1system.assertEquals('Sync Failed', a.Synchronisation_Status__c, 'Account Sync Status = Sync Failed');
        
    }
    
    private static testmethod void UpsertPartnerAsync_NullResponse() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = null;
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMPartnerProxyNew.MDMUpsertPartnerAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Synchronisation_Status__c FROM Account WHERE id = :id];
        //1system.assertEquals('Sync Failed', a.Synchronisation_Status__c, 'Account Sync Status = Sync Failed');
        
    }
    
    private static testmethod void DeletetPartnerAsync_Success() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, true);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        //MDMPartnerProxyNew.MDMDeletePartnerAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Synchronisation_Status__c FROM Account WHERE id = :id];
        //1system.assertEquals('Synchronised', a.Synchronisation_Status__c, 'Account Sync Status = Synchronised');
        
    }
    
    private static testmethod void DeletePartnerAsync_Failure() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, false);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        //MDMPartnerProxyNew.MDMDeletePartnerAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Synchronisation_Status__c FROM Account WHERE id = :id];
        //1system.assertEquals('Sync Failed', a.Synchronisation_Status__c, 'Account Sync Status = Sync Failed');
        
    }
    
    private static testmethod void DeletePartnerAsync_NullResponse() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = null;
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        //MDMPartnerProxyNew.MDMDeletePartnerAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Synchronisation_Status__c FROM Account WHERE id = :id];
        //1system.assertEquals('Sync Failed', a.Synchronisation_Status__c, 'Account Sync Status = Sync Failed');
        
    }

}