public without sharing class RenewalUpdatesCtrl
{

        //Added for renewal updates
       
       
        
        //public PI_Renewal__c rUpdates = new PI_Renewal__c();
        public List<String> PIRUpd{get;set;}{PIRUpd = new List<String>();}
        //End - for renewal updates
           /*for the generalIncrease Information*/
        public list<String> listAbouts {get;set;}
        public PI_Renewal__c objRenewalUpdates {get;set;}
        //public PI_Renewal__c rUpdates = new PI_Renewal__c();
        //public List<String> PIRUpd{get;set;}{PIBody = new List<String>();}
        /*End for GeneralIncrease*/
        public List<PI_Renewal__c> newsListLeft{get;set;}
        public List<PI_Renewal__c> newsListMiddle{get;set;}
        public List<PI_Renewal__c> newsListRight{get;set;}
        public String renewalUpdate{get;set;} 
        public List<String> newsWithAttachment{get;set;}
        public Map<String,String> NewsAttachmentIdMap{get;set;}
         //Start of SF-3562- disable claimsReview tab variable
        public boolean isRenewalUpdatesAvailable{get;set;} 
        public boolean isGeneralIncreaseAvailable{get;set;}
        public boolean isClaimsReviewAvailable{get;set;}  
        public boolean isCertificateAvailable{get;set;}
        public boolean isDocumentationAvailable{get;set;}
        public boolean isPortfolioReportAvailable{get;set;}
        public boolean isRenewalTermsAvailable{get;set;}
        public boolean isAttachmentAvailable{get;set;}
        ////End of SF-3562- disable claimsReview tab variable 
        public boolean blueCardTab {get;set;}
        public boolean certificateTab {get;set;}
        
        public Static Account_Contact_Mapping__c ACM_OBJ;//Added for SF-3961
    
       public RenewalUpdatesCtrl()
       {
           
           newsListLeft = new List<PI_Renewal__c>();
           newsListMiddle = new List<PI_Renewal__c>();
           newsListRight = new List<PI_Renewal__c>();
           isAttachmentAvailable = false;
           renewalUpdate ='';          
           newsWithAttachment = new List<String>();
           NewsAttachmentIdMap = new Map<String,String>();
           isRenewalUpdatesAvailable = false;
           isGeneralIncreaseAvailable = false;
           isClaimsReviewAvailable = false;
           isCertificateAvailable = false;
           isDocumentationAvailable = false;
           isPortfolioReportAvailable = false;
           isRenewalTermsAvailable = false;
          
           MyGardHelperCtrl.CreateCommonData();//Added for SF-3961
           ACM_OBJ=MyGardHelperCtrl.ACM_JUNC_OBJ;//Added for SF-3961
           for(MyGardDocumentAvailability__c csName : MyGardDocumentAvailability__c.getall().values()){
               if(csName.name == 'P&I Renewal Blue Card Tab' && ACM_OBJ.PI_Access__c!=false){
                   if(csname.isAvailable__c){
                        blueCardTab = true;
                   }else{
                        blueCardTab = false;
                   }            
               }
               if(csName.name == 'P&I Renewal Certificate Tab' && ACM_OBJ.PI_Access__c!=false){
                   if(csname.isAvailable__c){
                       certificateTab = true;
                   }else{
                       certificateTab = false;
                   } 
               }
               system.debug('------csName.name----'+csName.name+'---- blueCardTab----'+ csname.isAvailable__c + '---Boolean---' +blueCardTab);
           }
           
           //
       }
       public Pagereference fetchNews()
       {
           //Added for SF-3961--Starts
           if(ACM_OBJ.PI_Access__c==false)
            {
                System.debug('The Account Company PIAccess'+ACM_OBJ);
                return Page.HomePage;
            }
           //Added for SF-3961--Ends
           RecordType rt = [select Id from RecordType where Name = 'News' and SobjectType = 'PI_Renewal__c' limit 1];
           RecordType rt1 = [select Id from RecordType where Name = 'Renewal Updates' and SobjectType = 'PI_Renewal__c' limit 1];
           if(rt != null)
           {
               for(PI_Renewal__c piNews:[Select Id,Body__c,Header__c,Indentation__c,Video_URL__c,Display_Video__c,Attachment_Type__c,Header_URL__c from PI_Renewal__c where RecordTypeId =:rt.Id])
               {
                   if(piNews.Indentation__c=='Main News')
                   {
                       newsListLeft.add(piNews);
                   }
                   system.debug('--newsListLeft--'+newsListLeft);
                   system.debug('--piNews--'+piNews);
                   /*SF-3773 - Start 
                   else if(piNews.Indentation__c=='Middle')
                   {
                       newsListMiddle.add(piNews);
                   }
                   else if(piNews.Indentation__c=='Right')
                   {
                       newsListRight.add(piNews);
                   }
                   SF-3773 - End*/
                   if(piNews.Attachment_Type__c!='None' && piNews.Attachment_Type__c!= null && piNews.Attachment_Type__c!='') 
                   {
                       newsWithAttachment.add(piNews.Id);
                   }
                   system.debug('--newsWithAttachment--'+newsWithAttachment);
               }
           }
           if(rt1 != null)
           {
               PI_Renewal__c PI_Renewal_update = [Select Renewal_Updates__c from PI_Renewal__c where RecordTypeId =:rt1.Id LIMIT 1];
               if(PI_Renewal_update != null)
               {
                   renewalUpdate = PI_Renewal_update.Renewal_Updates__c;
               }
           }
           system.debug('--newsWithAttachment-122--'+newsWithAttachment);
           List<String> relatedCVId = new List<String>();
           List<String> CDL = new List<String>();
           if(newsWithAttachment != null && newsWithAttachment.size() > 0){
               for(ContentDocumentLink cdLink : [SELECT Id,ContentDocumentId,LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN: newsWithAttachment]){
                   CDL.add(cdLink.ContentDocumentId);
               }
            }
           system.debug('--CDL--'+CDL);
           if(!newsWithAttachment.isEmpty() && newsWithAttachment.size()>0 )
           {
               //for(Attachment att:[select id,parentId from Attachment where parentId in:newsWithAttachment])
                for(ContentVersion cv:[SELECT Id,ContentDocumentId FROM ContentVersion where ContentDocumentId IN: CDL]){
                    relatedCVId.add(cv.Id);
                }
                for(ContentDistribution cd : [SELECT ContentVersionId,ContentDownloadUrl,Id FROM ContentDistribution where ContentVersionId IN: relatedCVId])
               {
                   NewsAttachmentIdMap.put(newsWithAttachment[0],cd.ContentDownloadUrl);
                   isAttachmentAvailable = true;
               }
           }
           system.Debug('NewsAttachmentIdMap--'+NewsAttachmentIdMap);
           //Start of SF-3562- disable Renewal Updates tab if custom setting- Enable Renewal updates Tab and Enable General increase tab is not checked  
         PIrenewalAvailability__c CustomStng = PIrenewalAvailability__c.getInstance('value'); 
         Boolean isPIActive = false;
        if(CustomStng !=null)
        {
            isRenewalUpdatesAvailable = CustomStng.Enable_Renewal_Update__c;
            isGeneralIncreaseAvailable = CustomStng.Enable_General_increase__c;
            isClaimsReviewAvailable = CustomStng.Enable_Claims_review__c;
            isCertificateAvailable = CustomStng.Enable_Certificate__c;
            isDocumentationAvailable = CustomStng.Enable_Documentation__c;
            isPortfolioReportAvailable = CustomStng.Enable_Portfolio_report__c;
            isRenewalTermsAvailable = CustomStng.Enable_Renewal_terms__c;
            isPIActive = CustomStng.PIRenewalAvailability__c;
        }
         if(!isPIActive || !isRenewalUpdatesAvailable)
            {
             return Page.HomePage;
            }
            else
            {
                return null;
            }
        //End of SF-3562- disable Renewal Updates tab if custom setting- Enable Renewal updates Tab and Enable General increase tab is not checked  
      
       }
       
       
   
    //added for GeneralIncrease information fetching starts
        public Pagereference fetchGeneralIncreaseAbout()
        {
           /* listAbouts = new list<String>();
            objRenewalUpdates = [SELECT About__c,Id FROM PI_Renewal__c LIMIT 1]; 
            
        
        //added for GeneralIncrease information fetching ends*/
        
             //Added for SF-3961--Starts
           if(ACM_OBJ.PI_Access__c==false)
            {
                return Page.HomePage;
            }
           //Added for SF-3961--Ends
            List<PI_Renewal__c> PI_About = new List<PI_Renewal__c>();
            
            RecordType rt2 = [select Id from RecordType where Name = 'General Increase' and SobjectType = 'PI_Renewal__c' LIMIT 1];
           
           system.debug('rt2.id-->'+rt2.id);
           
            objRenewalUpdates = [Select About__c from PI_Renewal__c where RecordTypeId =:rt2.id LIMIT 1];
            
            listAbouts = new list<String>();
            
            if(objRenewalUpdates.About__c!=null && objRenewalUpdates.About__c!='')
            {
                
                    listAbouts.add(objRenewalUpdates.About__c);
                
            }
             //Start of SF-3562- disable Renewal Updates tab if custom setting- Enable Renewal updates Tab and Enable General increase tab is not checked  
         PIrenewalAvailability__c CustomStng = PIrenewalAvailability__c.getInstance('value'); 
         Boolean isPIActive = false;
        if(CustomStng !=null)
        {
            isRenewalUpdatesAvailable = CustomStng.Enable_Renewal_Update__c;
            isGeneralIncreaseAvailable = CustomStng.Enable_General_increase__c;
            isClaimsReviewAvailable = CustomStng.Enable_Claims_review__c;
            isCertificateAvailable = CustomStng.Enable_Certificate__c;
            isDocumentationAvailable = CustomStng.Enable_Documentation__c;
            isPortfolioReportAvailable = CustomStng.Enable_Portfolio_report__c;
            isRenewalTermsAvailable = CustomStng.Enable_Renewal_terms__c;
            isPIActive = CustomStng.PIRenewalAvailability__c;
        }
         if(!isPIActive || !isGeneralIncreaseAvailable)
            {
             return Page.HomePage;
            }
            else
            {
                return null;
            }
        //End of SF-3562- disable Renewal Updates tab if custom setting- Enable Renewal updates Tab and Enable General increase tab is not checked  
    
       }     
}