public without sharing class Logger{

	public static Boolean ExceptionLoggingEnabled {
		get {
			if (test.isRunningTest()) {
                return true;
            }
			Logging_Settings__c logSettings = Logging_Settings__c.getInstance(UserInfo.getUserId());
			if (logSettings == null) {
				return false;
			}
			return logsettings.Enable_Exception_Logging__c;
		}
	}
	public static Boolean WebServiceLoggingEnabled {
		get {
			if (test.isRunningTest()) {
                return true;
            }
			Logging_Settings__c logSettings = Logging_Settings__c.getInstance(UserInfo.getUserId());
			if (logSettings == null) {
				return false;
			}
			return logsettings.Enable_Web_Service_Logging__c;
		}
	}
	
	//Exception Logging...
	public static void LogException(String source, Exception ex) {
		Logger.LogException(source, ex, null);
	}
	public static void LogException(String source, Exception ex, String additionalDetails) {
		if (ExceptionLoggingEnabled) {
			try{
				List<Exception_Log__c> exceptions = new List<Exception_Log__c>();
				while (ex != null) {
					Exception_Log__c l = new Exception_Log__c();
					l.source__c = source;
					l.Line_Number__c = ex.getLineNumber();
					l.exception_Message__c = ex.getMessage();
					l.Stack_Trace__c = ex.getStackTraceString();
					l.Type_Name__c = ex.getTypeName();
					l.additional_Details__c = additionalDetails;
					additionalDetails = null;
					
					exceptions.add(l);
					
					ex = ex.getcause();
				}
			 	
				insert exceptions;
			} catch (Exception e) {
			 	//DO NOTHING (Don't let exception log cause any other failures).
			}
		}
	}
	
	//Web Service Callout Logging...
	public static void LogCallout(String request, String response) {
		if (WebServiceLoggingEnabled) {
			try {
				Web_Service_Log__c l = new Web_Service_Log__c(Request_Message__c = request, Response_Message__c = response);
				insert l;
			} catch (Exception e) {
				//DO NOTHING (Don't let log cause any other failures).
			}
		}
	}

}