global class BatchUpdateCompanyOnRiskFields implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    global Map<String,Company_on_risk_information__c> onRiskInfoMap; 
    global List<String> companyIdList = new List<String>();
    global List<String> foundCompanyIdList = new List<String>();
    global List<String> inactiveCompanyIdList = new List<String>();
    global Database.QueryLocator start(Database.BatchableContext BC) {
        getPartnerIds();
        query = 'SELECT Id, Company_id__c, Company_Status__c, P_I_Member_Flag__c ,M_E_Client_Flag__c,Client_On_Risk_Flag__c,Broker_On_Risk_Flag__c,On_Risk__c,Small_craft_client__c,'+
                'Assured_On_Risk__c ,Builders_On_Risk__c ,Coassured_On_Risk__c ,Energy_On_Risk__c,Insurance_Company_On_Risk__c ,Marine_On_Risk__c ,'+
                'Original_Insured_Mutual_On_Risk__c ,P_I_Charterers_On_Risk__c ,P_I_MOUOnRisk__c ,P_I_Owners_On_Risk__c,Protected_Coassured_On_Risk__c '+
                'FROM Account WHERE Company_id__c IN: companyIdList';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Account> scope) {
        system.debug('--onRiskInfoMap---'+onRiskInfoMap);
        system.debug('company id size : '+companyIdList.size());
        try{
            for(Account acc:scope){
                foundCompanyIdList.add(acc.Company_id__c);
                if(acc.Company_Status__c == 'Inactive'){inactiveCompanyIdList.add(acc.Company_id__c);}
                Company_on_risk_information__c onRiskInfoObj = new Company_on_risk_information__c ();
                onRiskInfoObj = onRiskInfoMap.get(acc.Company_id__c);
                // acc. <--Fields--> = onRiskInfoObj. <--Fields-->
                system.debug('Inside for loop:'+acc.Company_id__c+'-on risk info-'+onRiskInfoObj);
                if(onRiskInfoObj != null){
                    acc.P_I_Member_Flag__c = (((onRiskInfoObj.P_I_Owners_On_Risk__c == true) || (onRiskInfoObj.P_I_Charterers_On_Risk__c == true) || (onRiskInfoObj.P_I_MOUOnRisk__c == true)) && onRiskInfoObj.Client_On_Risk__c == true) ? true : false ;
                    acc.M_E_Client_Flag__c = (((onRiskInfoObj.Marine_On_Risk__c == true) || (onRiskInfoObj.Energy_On_Risk__c == true) || (onRiskInfoObj.Builders_On_Risk__c == true)) && onRiskInfoObj.Client_On_Risk__c == true) ? true : false ;
                    acc.Client_On_Risk_Flag__c = onRiskInfoObj.Client_On_Risk__c;
                    acc.Broker_On_Risk_Flag__c = onRiskInfoObj.Broker_On_Risk__c;
                    acc.On_Risk__c = onRiskInfoObj.Company_On_Risk__c;
                    //acc.Small_craft_client__c = onRiskInfoObj.Original_Insured_Small_Craft_On_Risk__c; //Commented as it is populated based on last 5years' opportunity.
                    acc.Assured_On_Risk__c = onRiskInfoObj.Assured_On_Risk__c;
                    acc.Builders_On_Risk__c = onRiskInfoObj.Builders_On_Risk__c;
                    acc.Coassured_On_Risk__c = onRiskInfoObj.Coassured_On_Risk__c;
                    acc.Energy_On_Risk__c = onRiskInfoObj.Energy_On_Risk__c;
                    acc.Insurance_Company_On_Risk__c = onRiskInfoObj.Insurance_Company_On_Risk__c;
                    acc.Marine_On_Risk__c = onRiskInfoObj.Marine_On_Risk__c;
                    acc.Original_Insured_Mutual_On_Risk__c = onRiskInfoObj.Original_Insured_Mutual_On_Risk__c;
                    acc.P_I_Charterers_On_Risk__c = onRiskInfoObj.P_I_Charterers_On_Risk__c;
                    acc.P_I_MOUOnRisk__c = onRiskInfoObj.P_I_MOUOnRisk__c;
                    acc.P_I_Owners_On_Risk__c = onRiskInfoObj.P_I_Owners_On_Risk__c;
                    acc.Protected_Coassured_On_Risk__c = onRiskInfoObj.Protected_Coassured_On_Risk__c;
                }
            }
            update scope;
        }catch(Exception ex){Logger.LogException('BatchUpdateCompanyOnRiskFields (Batch class)',ex,'');}
    }
    global void finish(Database.BatchableContext BC) {
        //ASSERT: do something here
        //Log exception
        system.debug('Unknown companyIds:'+foundCompanyIdList.size());
        system.debug('Inactive companyIds:'+inactiveCompanyIdList.size());
        String unknownCompanyIds = '';
        for(String cId : companyIdList){
            if(!foundCompanyIdList.contains(cId)){unknownCompanyIds = unknownCompanyIds + cId + '; ';}
        }
        if(unknownCompanyIds != '')
            CompanyOnRiskInfoUpdateCtrl.logErrorResponse('BatchUpdateCompanyOnRiskFields (Batch class)','Unknown Partner Ids received : '+unknownCompanyIds,'Unknown PartnerId in response','');
        if(inactiveCompanyIdList != null && inactiveCompanyIdList.size() > 0)
            CompanyOnRiskInfoUpdateCtrl.logErrorResponse('BatchUpdateCompanyOnRiskFields (Batch class)','Inactive Partner Ids : '+inactiveCompanyIdList,'Inactive Partner Ids','');
    }
    //Fetch PartnerIds
    public void getPartnerIds(){
        Company_on_risk_information__c onRiskInfo;
        onRiskInfoMap = new Map<String,Company_on_risk_information__c>();
        String query = 'SELECT Id,Assured_On_Risk__c, Broker_On_Risk__c, Builders_On_Risk__c, Client_On_Risk__c, Coassured_On_Risk__c, Company_On_Risk__c,'
                       + 'Energy_On_Risk__c, Insurance_Company_On_Risk__c, Marine_On_Risk__c, Original_Insured_Mutual_On_Risk__c, '
                       + 'Original_Insured_Small_Craft_On_Risk__c, P_I_Charterers_On_Risk__c, P_I_MOUOnRisk__c, P_I_Owners_On_Risk__c, Partner_Id__c, CRM_Id__c,'
                       + 'Partner_Type_Code__c, Partner_Type_Description__c, Protected_Coassured_On_Risk__c, Stop_Use__c  FROM Company_on_risk_information__c';
        for(Company_on_risk_information__c comOnRisk:Database.query(query)){
            onRiskInfo = new Company_on_risk_information__c();
            onRiskInfo = comOnRisk;
            system.debug('--onRiskInfo---'+onRiskInfo);
            onRiskInfoMap.put(comOnRisk.CRM_Id__c,onRiskInfo);
            companyIdList.add(comOnRisk.CRM_Id__c);
        }
    }
}