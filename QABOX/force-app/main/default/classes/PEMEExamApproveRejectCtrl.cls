public class PEMEExamApproveRejectCtrl{
    
    public string selectedInvoice;
    public string approvalComments {get;set;}
    public List<PEME_Exam_Detail__c > lstExaminations;
    //For Approval Process Start
    public List<ProcessInstance> lstItemsForApproval{get;set;}
    //For Approval Process End
    public List<PEME_Exam_Detail__c> ex;
    private map<Id,String> mapOldExamStatus;
    private map<Id,String> mapNewExamStatus;
    Boolean isPending=false;
    Boolean isRejected=false;
    Boolean isApproved=false;
    String invoiceStatus = '';
    Decimal totalAmt = 0;
    private ApexPages.StandardSetController standardController;

    public PEMEExamApproveRejectCtrl(ApexPages.StandardSetController standardController)
    {
        this.standardController = standardController;
    }
    public PageReference Examdetail(){
       mapOldExamStatus = new map<Id,String>();
       mapNewExamStatus = new map<Id,String>();
       selectedInvoice = ApexPages.currentPage().getParameters().get('id'); 
       lstExaminations = new  List<PEME_Exam_Detail__c >();
       lstItemsForApproval = new  List<ProcessInstance >();
       lstExaminations = (List<PEME_Exam_Detail__c>) standardController.getSelected();
       System.debug('*************lstExaminations '+lstExaminations );
       
       //ex = new List<PEME_Exam_Detail__c>([Select id,status__c from PEME_Exam_Detail__c where id in:lstExaminations]);
       ex = new List<PEME_Exam_Detail__c>([Select id,status__c from PEME_Exam_Detail__c where invoice__c =:selectedInvoice]);
       try{
               
          for(PEME_Exam_Detail__c PEMEExam:ex)
          {
              mapOldExamStatus.put(PEMEExam.id,PEMEExam.status__c);
              
          }
          for (ProcessInstance process : [Select Id, TargetObjectId, isDeleted, Status,(Select Id, ProcessInstanceId, ActorId, Actor.Name, StepStatus, Comments 
          From StepsAndWorkItems Where StepStatus = 'Pending' and isDeleted = false Order By Createddate Desc Limit 1) From ProcessInstance 
          Where isDeleted = false and Status = 'Pending' and TargetObjectId in:lstExaminations  Order By Createddate Desc])
          {
              lstItemsForApproval.add(process);
              System.debug('*************lstItemsForApproval'+lstItemsForApproval);
          }  
          if(lstItemsForApproval.size()<= 0)
          {
              System.debug('All exams are approved/rejected---------------');
               ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'All exams are approved/rejected');                                              
              ApexPages.addMessage(myMsg);
              System.debug('*************'+myMsg);
          }
          
         // return process;  
      }catch(Exception e){
          System.debug('All exams are approved/rejected---------------'+e);
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'asdkrjfsdklfjklsdjfvfdgsdfag');                                              
           ApexPages.addMessage(myMsg);
      } 
       //fetchLatestApprovals();
      /* for(PEME_Exam_Detail__c objExam:lstExaminations)
       {
           lstItemsForApproval.add(fetchLatestApprovals(objExam));
         //  PEME_Exam_Detail__c peme = [SELECT Invoice__c FROM PEME_Exam_Detail__c where id =: objExam.id];
          // selectedInvoice = peme.Invoice__c; 
       }
    /*   if(selectedInvoice!=null){
           for(PEME_Exam_Detail__c objExam:[SELECT Status__c,Comments__c FROM PEME_Exam_Detail__c where Invoice__c=:selectedInvoice AND Status__c='Under Approval']){            
                lstExaminations.add(objExam);
                lstItemsForApproval.add(fetchLatestApprovals(objExam));                
           }
       }    */
       return null;
    }    
    //For Approval Process Start    
   /* public ProcessInstance fetchLatestApprovals(PEME_Exam_Detail__c objExam){
    try{
          ProcessInstance process = [Select Id, TargetObjectId, isDeleted, Status,(Select Id, ProcessInstanceId, ActorId, Actor.Name, StepStatus, Comments 
          From StepsAndWorkItems Where StepStatus = 'Pending' and isDeleted = false Order By Createddate Desc Limit 1) From ProcessInstance 
          Where isDeleted = false and Status = 'Pending' and TargetObjectId=:objExam.Id Order By Createddate Desc LIMIT 1];   
          return process;  
      }catch(Exception e){
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'All exams are approved/rejected');                                              
           ApexPages.addMessage(myMsg);
      } 
       return null;            
    }*/
    //For Approval Process End
    
    public PageReference approve(){
       performApprovals('Approve');
       approveRejectExams('Approved');
       PageReference pg= redirect();
       return pg;    
    }
    
    public PageReference reject(){
       if(approvalComments!=null && approvalComments!=''){
       system.debug('approvalComments22'+approvalComments);
            performApprovals('Reject');
         approveRejectExams('Rejected');
         PageReference pg= redirect();
         return pg; 
       }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'You must provide a comment while rejecting Exam(s)');                                              
           ApexPages.addMessage(myMsg);
            return null;  
       }     
    }
    
    public PageReference cancel(){
       PageReference pg= redirect();
       return pg;      
    }
    
    public PageReference redirect(){
      PageReference pg= new PageReference(System.Label.InternalOrgURL+selectedInvoice);
      pg.setRedirect(true);
      return pg;  
    }
    
    //For Approval Process Start
    public void performApprovals(String action){
      Approval.ProcessWorkitemRequest req;
      List<Approval.ProcessWorkitemRequest> requestList = new List<Approval.ProcessWorkitemRequest>();
      if(lstItemsForApproval.size()>0){
        for(ProcessInstance objProcess:lstItemsForApproval){
          req = new Approval.ProcessWorkitemRequest();
          req.setNextApproverIds(null);
          req.setWorkitemId(objProcess.StepsAndWorkitems[0].Id);
          system.debug('approvalCommentsss'+approvalComments);
          system.debug('actionss'+action);
          req.setComments(approvalComments);
              req.setAction(action);
              requestList.add(req);
        } 
        Approval.ProcessResult[] resultList = Approval.process(requestList);
        for(PEME_Exam_Detail__c PEMEExamNew:[select id,status__c from PEME_Exam_Detail__c where Invoice__c=:selectedInvoice])
        {
            mapNewExamStatus.put(PEMEExamNew.id,PEMEExamNew.status__c);
        }
      }  
    }
    //For Approval Process End
    
    public void approveRejectExams(String action){
        List<String> st = new List<String>();
        st.add('Rejected-Need Input');
        st.add('Rejected-Closed');
        st.add('Approved');
        system.debug('actionsname'+action);
        system.debug('lstExaminationsc'+lstExaminations);
        system.debug('lstExaminationsccc'+lstExaminations.size());
        if(lstExaminations.size()>0){
            List<PEME_Exam_Detail__c > lstExamsUpdate = new  List<PEME_Exam_Detail__c >();
            for(PEME_Exam_Detail__c objExam:[Select id ,Status__c from PEME_Exam_Detail__c  where id in:lstExaminations and Status__c not in:st]){
            if(action == 'Rejected')
            {
                System.debug('objExam----------'+objExam);
                objExam.Status__c = 'Rejected-Need Input';
            }
            else
            {
                objExam.Status__c = action;
            }
                objExam.GARD_Comments__c = approvalComments;    
                lstExamsUpdate.add(objExam);
            }
            system.debug('lstExamsUpdateee'+lstExamsUpdate);
            if(lstExamsUpdate.size()>0){              
                system.debug('lstExamsUpdatess'+lstExamsUpdate);
                update lstExamsUpdate;
            }
            if(action == 'Rejected')
            {
                GardUtils.sendPEMERejectMail(selectedInvoice, 'MyGard-PEME examination details rejection - Notification to clinics');
            }
        }
        checkStatusAllExam();
    }
    
    private void checkStatusAllExam()
    {
        System.debug('selectedInvoice-->' + selectedInvoice);
        PEME_Invoice__c objInvoice = [select Id,Name,Status__c from PEME_Invoice__c where Id =: selectedInvoice];
        integer totalPending=0,totalRejected=0,totalApproved=0,totalExam=0;
        for(String examNewStatus:mapNewExamStatus.keyset())
            {
                if(mapOldExamStatus.containsKey(examNewStatus))
                {
                    if(mapNewExamStatus.get(examNewStatus)=='Under Approval')
                    {
                        //isPending=true;
                        totalPending++;
                    }
                    else if(mapNewExamStatus.get(examNewStatus)=='Rejected-Need Input' || mapNewExamStatus.get(examNewStatus)=='Rejected-Closed')   
                    {
                        totalRejected++;
                        //isRejected=true;
                       // objInvoice.Date_of_Final_Verification__c = null;
                    } 
                    else if(mapNewExamStatus.get(examNewStatus)=='Approved')
                    {
                        totalApproved++;
                        //isApproved = true;
                        //objInvoice.Date_of_Final_Verification__c = Date.valueOf(Datetime.now());  
                    }              
                }
                totalExam++;
            }
            if(totalApproved > 0 && totalApproved == totalExam)
            {
                invoiceStatus = 'Approved'; 
            }
            else if(totalRejected > 0 && totalRejected == totalExam)
            {
                invoiceStatus = 'Rejected'; 
            }
            else if(totalRejected == 0 && totalPending == 0 && totalApproved == 0)
            {
                invoiceStatus = 'Draft'; 
            }
            else if(totalRejected == 0 && totalPending > 0 && totalApproved == 0 || totalPending == totalExam)
            {
                invoiceStatus = 'Under Approval'; 
            }
            //else if((totalRejected > 0 && totalPending > 0 && totalApproved > 0) || (totalRejected == 0 && totalPending > 0 && totalApproved > 0) || (totalRejected > 0 && totalPending == 0 && totalApproved > 0) || (totalRejected > 0 && totalPending > 0 && totalApproved == 0))
            else
            {
                invoiceStatus = 'Partially Approved'; 
            }
            /*if(isRejected && !isPending && !isApproved){
                invoiceStatus = 'Rejected';    
            }
            if(!isRejected && !isPending && isApproved){
                invoiceStatus = 'Approved';    
            }
            if(!isRejected && isPending && !isApproved){
                invoiceStatus = 'Under Approval';    
            }
            if(isApproved && isPending && !isRejected){
                invoiceStatus = 'Partially Approved';    
            }
            if(isRejected && isPending && !isApproved){
                invoiceStatus = 'Partially Approved';    
            }
            if(isRejected && isApproved && !isPending){
                invoiceStatus = 'Partially Approved';    
            }
            if(isRejected && isApproved && isPending){
                invoiceStatus = 'Partially Approved';    
            }
            if(!isRejected && !isPending && !isApproved){
                invoiceStatus = 'Draft';    
            }*/
            for(PEME_Exam_Detail__c objExam:[select Id,Name,Status__c,Amount__c,Invoice__c from PEME_Exam_Detail__c where Invoice__c =:selectedInvoice AND Status__c='Approved']){
              if(objExam.Amount__c!=null) {  // SF-4760 added null check
                totalAmt = totalAmt + objExam.Amount__c;
                }
            }   
            PEME_Invoice__c objInvoice1 =[select Id,Name,Date_of_Final_Verification__c from PEME_Invoice__c where Id =: selectedInvoice];
                if(totalApproved>0)
                objInvoice1.Date_of_Final_Verification__c = Date.valueOf(Datetime.now());
                else                
                objInvoice1.Date_of_Final_Verification__c = null;  
                objInvoice=objInvoice1;
                
            objInvoice.Status__c = invoiceStatus;
            objInvoice.CurrencyIsoCode = 'PHP';  //Added for SF-4177
            objInvoice.Total_Amount__c= totalAmt;
            system.debug('objInvoicess'+objInvoice);
            update objInvoice;
    }
 
}