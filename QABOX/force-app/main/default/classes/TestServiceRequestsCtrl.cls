@istest
public class TestServiceRequestsCtrl 
{
    @istest static void test()
    {
        List<String> contLst = new List<String>();
        set<String> AccountLst = new set<String>();
        GardTestData gtdInstance = new GardTestData();
        gtdInstance.commonRecord();
        for(Contact con: GardTestData.ContactList)
        {
            contLst.add(con.id);
        }
        for(Account acc : GardTestData.AccountList)
        {
             AccountLst.add(acc.id);  
        }
        Test.startTest();
        System.runAs(GardTestData.brokerUser)
        {
            ApexPages.StandardController scontroller = new ApexPages.StandardController(new case());//GardTestData.brokerCase);
            test.setcurrentpage(page.OpenServiceRequests);
            //ApexPages.currentPage().getParameters().put('favId',Gardtestdata.test_fav.id);
            ServiceRequestsCtrl servicereq = new ServiceRequestsCtrl(scontroller);
            servicereq.usersOfLoggedInCompany = contLst;
            servicereq.loggedInAccountId = GardTestData.brokerAcc.id;
            servicereq.getOpenRequestDetails();
            servicereq.getSortDirection();
            servicereq.getCoverLst();
            servicereq.getYearLst();
            servicereq.getObjectName();
            servicereq.getClaimTypeLst();
            servicereq.getRequestLst();
            servicereq.getStatusLst();
            servicereq.getFilterListForOpen();
            servicereq.claimingClientIdSet = AccountLst;
            servicereq.getClientLst();
            servicereq.filterRecordsForOpen();
            servicereq.createOpenRequestList();
            servicereq.hasPreviousOpen = true;
            servicereq.previousOpen(); 
            servicereq.hasNextOpen = true;
            servicereq.nextOpen();
            servicereq.pageNumberOpen = 1;
            servicereq.setpageNumberOpen();
            servicereq.lastOpen();
            servicereq.firstOpen();            
            servicereq.caseId = GardTestData.brokerCase.id;
            servicereq.showRequestDetails();
            servicereq.newFilterName = 'TEST';
            servicereq.createFavourites();
            servicereq.fetchSelectedClients();
            servicereq.deleteFavourites();
            servicereq.favouriteId = Gardtestdata.test_fav.id;
            servicereq.fetchFavourites();
            servicereq.clearOptions();
            servicereq.viewRequest();
            servicereq.ownerId = GardTestData.brokerUser.id+'#'+GardTestData.brokerUser.id;
            servicereq.ownerPopup();
            servicereq.ownerQueuePopup();
            servicereq.downloadVcard();
        }
        System.runAs(GardTestData.clientUser)
        {
            ApexPages.StandardController scontroller = new ApexPages.StandardController(new case());
            test.setcurrentpage(page.closedservicerequests);
            ServiceRequestsCtrl servicereq = new ServiceRequestsCtrl(scontroller); 
            servicereq.isFav = true;
            servicereq.getOpenRequestDetails();
            servicereq.fetchSelectedClients();
            servicereq.clearOptions();
        }
        Test.stopTest();
    }
}