//TestClass : TestPemeWebService
public class JsonRequestWrapper{
    public class PemeInvoiceFileAuth{
        String endPoint;
		String scope = 'READ WRITE';
		String client_id;
		String client_secret;
		String grant_type = 'client_credentials';
		String method = 'POST';
        
        public PemeInvoiceFileAuth(){
            Mule_Web_Service__c mws = Mule_Web_Service__c.getInstance('Retreive access token');
            this.endpoint = mws.Endpoint_Url__c;
            this.client_id = mws.Client_Id__c;
            this.client_secret = mws.Client_secret__c;
        }
        
        public JsonResponseWrapper.PemeInvoiceFileAuth callWs(){
            
            if(Test.isRunningTest()){
                this.getHttpRequest();
                return new JsonResponseWrapper.PemeInvoiceFileAuth();
            }
            
            return ((JsonResponseWrapper.PemeInvoiceFileAuth) JSON.deserialize(new Http().send(this.getHttpRequest()).getBody(),JsonResponseWrapper.PemeInvoiceFileAuth.class));
        }
        
        private HttpRequest getHttpRequest(){
            HttpRequest retRequest = new HttpRequest();
            retRequest.setEndpoint(this.endpoint);
            retRequest.setheader('scope',this.scope);
            retRequest.setheader('client_id',this.client_id);
            retRequest.setheader('client_secret',this.client_secret);
            retRequest.setheader('grant_type',this.grant_type);
            retRequest.setMethod(this.method);
            return retRequest;
        }
    }
    
    public class PemeInvoiceFile{
        String endpoint;
        String content_type;
        String method;
        String authorization;
        JsonResponseWrapper.PemeInvoiceFileAuth authResponseWrapper;
        JsonDataWrapper.PemeInvoiceFile dataWrapper;
        
        public PemeInvoiceFile(JsonResponseWrapper.PemeInvoiceFileAuth authResponseWrapper,JsonDataWrapper.PemeInvoiceFile dataWrapper){
            Mule_Web_Service__c mws = Mule_Web_Service__c.getInstance('PEME Invoice And File Service');
            endpoint = mws.Endpoint_Url__c;
            content_type = 'application/json';
            method = 'POST';
            this.authResponseWrapper = authResponseWrapper;
            this.dataWrapper = dataWrapper;
            this.authorization = this.authResponseWrapper.token_type+' '+this.authResponseWrapper.access_token;
        }
        
        public JsonResponseWrapper.PemeInvoiceFile callWs(){
            if(Test.isRunningTest()) {
                this.getHttpRequest();
                return new JsonResponseWrapper.PemeInvoiceFile('dummyStatus','dummyMessage','dummyTrace');
            }
            
            return ((JsonResponseWrapper.PemeInvoiceFile) JSON.deserialize(new Http().send(this.getHttpRequest()).getBody(),JsonResponseWrapper.PemeInvoiceFile.class));
        }
        
        private HttpRequest getHttpRequest(){
            HttpRequest retRequest = new HttpRequest();
            retRequest.setEndpoint(this.endpoint);
            retRequest.setheader('Content-Type',this.content_type);
            retRequest.setheader('Authorization',this.authorization);
            retRequest.setMethod(this.method);
            retRequest.setbody(JSON.serialize(this.dataWrapper));
            return retRequest;
        }
    }
}