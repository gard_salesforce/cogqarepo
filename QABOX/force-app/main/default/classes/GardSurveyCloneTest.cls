/**
 * This class contains unit tests for validating the behavior of Apex class GardSurveyClone
 */
@isTest
private class GardSurveyCloneTest {

    static testMethod void myUnitTest() {
         
        Test.StartTest();
        PageReference pageRef = Page.GardSurveyClone;
        Test.setCurrentPage(pageRef);
        fluidoconnect__Survey__c ts = new fluidoconnect__Survey__c(
                            fluidoconnect__Completed_URL__c='http://www.klaud.net', 
                            fluidoconnect__Override_Language__c = true, 
                            fluidoconnect__Questions_per_page__c = 4);
        insert ts;
        fluidoconnect__Question__c tq = new fluidoconnect__Question__c(
                            fluidoconnect__Question_description__c='This is ok', 
                            fluidoconnect__Type__c ='Radio (Likert)', 
                            fluidoconnect__Survey__c=ts.id ); 
        insert tq;     

        GardSurveyClone controller = new GardSurveyClone( new Apexpages.Standardcontroller(ts) );
        controller.doclone();
        controller.goToClone();
   
        Test.StopTest();
 
        
    }
}