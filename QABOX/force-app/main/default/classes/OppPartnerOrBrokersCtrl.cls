public without sharing  class OppPartnerOrBrokersCtrl {

    @AuraEnabled
    public static List<Partner> getPartnersOrBrokers(String opportunityId){        
        List<Partner> lstPartnersOrBrokers = new List<Partner>();
        String accId = [SELECT AccountId, Account.Name FROM Opportunity where id =: opportunityId limit 1].AccountId;        
        if(String.isEmpty(opportunityId)){
            return lstPartnersOrBrokers; 
        }
        lstPartnersOrBrokers = [SELECT AccountTo.Site,AccountTo.Name,AccountTo.Company_Role__c,AccountToId,Id,IsPrimary,LastModifiedById,LastModifiedDate,OpportunityId,ReversePartnerId,Role FROM Partner where OpportunityId =:opportunityId and AccountToId !=: accId order by createddate desc];
        return lstPartnersOrBrokers;
    }
    @AuraEnabled
    public static Map<String,String> deletePartnersOrBrokers(String partnerOrBrokerId){
        Map<String,String> mess = new Map<String,String>();
        //List<OpportunityPartner> partnerOrBrokersList = new List<OpportunityPartner>(); 
        List<Partner> partnerOrBrokersList = new List<Partner>();  
        if(String.isEmpty(partnerOrBrokerId)){
           mess.put('status','error'); 
           mess.put('message','Id is wrong!');
           return mess; 
        }        
        partnerOrBrokersList = [SELECT AccountTo.Site,AccountTo.Name,AccountTo.Company_Role__c,AccountToId,Id,IsPrimary,Role FROM Partner where Id =:partnerOrBrokerId];
        try{
           delete partnerOrBrokersList;
           mess.put('status','success'); 
           mess.put('message','Deleted Successfully!');            
        }catch(Exception e){
           mess.put('status','error'); 
           mess.put('message','Id is wrong!');
        }
        return mess;
    }
    @AuraEnabled
    public static List<String> getRoles(){
        List<String> lstRoles =  new List<String>(); 
        lstRoles = getPicklistValues(new Partner(), 'Role');
        return lstRoles;
    }
    @AuraEnabled
    public static String getOppName(String opportunityId){
        List<Opportunity> oppList = new List<Opportunity>(); 
        oppList = [SELECT Name FROM Opportunity where id =: opportunityId limit 1];
        String oppName = oppList[0].Name;
        return oppName;
    }
    private  static list<String> getPicklistValues(SObject obj, String fld){
          list<String> options = new list<String>();
          // Get the object type of the SObject.
          Schema.sObjectType objType = obj.getSObjectType(); 
          // Describe the SObject using its object type.
          Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
          // Get a map of fields for the SObject
          map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
          // Get the list of picklist values for this field.
          list<Schema.PicklistEntry> values =
             fieldMap.get(fld).getDescribe().getPickListValues();
          // Add these values to the selectoption list.
          for (Schema.PicklistEntry a : values)
          { 
             options.add(a.getValue()); 
          }
          return options;
    }
    
    
 	@AuraEnabled
    public static List<sObject> getRecordList(String ObjectName,String searchText,
    String fieldInsearch){
        String searchKey = '%'+searchText+'%';
        String Query = 'SELECT Id,Name ';
        	   Query+=' FROM '+ObjectName;
        	   Query+=' WHERE '+fieldInsearch+' LIKE :searchKey limit 10';
        List<sObject> sObjectList = new List<sObject>();
        sObjectList = Database.query(Query);
        return sObjectList;
    }   
    
    @AuraEnabled
    public static void createNewPartnersOrBrokers(String wrapPartnerjson,String opportunityID){       
        List<JSON2ApexPartner> wrapperPartnerList= new List<JSON2ApexPartner>();
            wrapperPartnerList = (List<JSON2ApexPartner>)System.JSON.deserialize(wrapPartnerjson, List<JSON2ApexPartner>.class);
        system.debug('wrapPartnerjson::::::::::'+wrapPartnerjson);        
        List<Partner> partnerList = new List<Partner>();
        for(JSON2ApexPartner wp: wrapperPartnerList)
        {   
            if(wp.acct != null){
                Partner p= new Partner();
                p.Role =  wp.role;
                p.IsPrimary = Boolean.valueOf(wp.primary);
                p.AccountToId = wp.acct.Id;
                p.OpportunityId = opportunityID;
                partnerList.add(p);
           }
        }
        system.debug('partnerList>>'+partnerList);
        if(partnerList.size() !=0){            
            insert partnerList;    
        }
    }
    // wrapper class
    class wrapPartner {
        //public Account acc {get; set;}
        public Account acct {get; set;}
        public Boolean primary {get; set;}
        public String  role {get; set;}        
       public wrapPartner(Account acct,Boolean primary,String role) {
            this.acct = acct;
            this.primary = primary;
            this.role = role;
        }
    }
    @AuraEnabled
    public static String getPrePopulatedForm(){
        String jsonBody;
        List<wrapPartner> lstwrapPartner = new List<wrapPartner>();
        lstwrapPartner.add(new wrapPartner(new Account(),false,''));
        lstwrapPartner.add(new wrapPartner(new Account(),false,''));
        lstwrapPartner.add(new wrapPartner(new Account(),false,''));
        lstwrapPartner.add(new wrapPartner(new Account(),false,''));
        lstwrapPartner.add(new wrapPartner(new Account(),false,''));
        jsonBody = JSON.serialize(lstwrapPartner);     
        return jsonBody;
    }
    
     @AuraEnabled
    public static Opportunity getCustomerDetails(String opportunityId){
          return [SELECT Id,AccountId, Account.Name FROM Opportunity where id =: opportunityId limit 1];
    }
}