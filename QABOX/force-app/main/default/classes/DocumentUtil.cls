public without sharing class DocumentUtil {
 public static void  createClientActivity(string currentDocumentId,String loggedInAccountId,String loggedInContactId,String selectLocalClient,boolean hasMultipleClients, Map <String,DocumentResponseWrapper.ResultObject>docIdObjectMap){
  final String DocumentName= 'Document name: '; 
   final String DocumentType= 'Document type description: ';
   final String DownloadAt= 'Download At: ';
   final String DocumentId= 'Document ID: ';
   system.debug('currentDocumentId***********'+currentDocumentId);
   system.debug('docIdObjectMap***********'+docIdObjectMap);
       List<Contact>conList=new List<Contact>();
       try{
      conList = [Select Name from Contact where id=:loggedInContactId AND (NOT Name  like 'Test%') AND (NOT Name like '%support%')];
list<PI_Renewal_Document_Download_Tracking__c> docTrackList = new list<PI_Renewal_Document_Download_Tracking__c>();
             PI_Renewal_Document_Download_Tracking__c newRec = new PI_Renewal_Document_Download_Tracking__c();
             newRec.CompanyId__c = loggedInAccountId;
             newRec.Client_Name__c = selectLocalClient; //SF-3689-By Debosmeeta
             newRec.DownloadBy__c = loggedInContactId;
             newRec.Document_External_Id__c = currentDocumentId;
             if(docIdObjectMap.containsKey(currentDocumentId)){
             newRec.Document_Name__c = docIdObjectMap.get(currentDocumentId).name;
             }
             newRec.Download_At__c = dateTime.now();
             docTrackList.add(newRec);
             
             if(conList != null && conList.size() > 0){
                 insert docTrackList;
             }
             String eventType ='Document Downloaded'; 
             /*String BlueCard=                 
             ClientActivityEvents__c BluCEvents = ClientActivityEvents__c.getValues(BlueCard);               
             if(BluCEvents!=null && BluCEvents.Name!=null)               
             {               
                eventType = BluCEvents.Event_Type__c;              
             }*/
             if(docTrackList.size()>0 && conList != null && conList.size() > 0)
             { 
                 DocumentResponseWrapper.ObjectPropertie objProperties=docIdObjectMap.get(currentDocumentId).objectProperties;
                 system.debug('objProperties*******'+objProperties);
                 string pd_docCertype =String.isEmpty(objProperties.pd_certificatetype)?objProperties.pd_policydocumenttype:objProperties.pd_certificatetype;
                system.debug('pd_docCertype *******'+pd_docCertype );
                 if(hasMultipleClients)//4302 replaced userType = 'Broker'
                 {   
                 MyGardHelperCtrl.createClientEvents(UserInfo.getUserId(), selectLocalClient  , eventType, objProperties.pd_objectnameondocument, null, null, null, DocumentId+currentDocumentId+','+DocumentName+docIdObjectMap.get(currentDocumentId).name+','+DocumentType+pd_docCertype+','+DownloadAt+newRec.Download_At__c);  
                }
                else{
                MyGardHelperCtrl.createClientEvents(UserInfo.getUserId(), loggedInAccountId, eventType, objProperties.pd_objectnameondocument, null, null, null, DocumentId+currentDocumentId+','+DocumentName+docIdObjectMap.get(currentDocumentId).name+','+DocumentType+pd_docCertype+','+DownloadAt+newRec.Download_At__c);    
              }
        }}
         
      catch(Exception ex){
       Logger__c logMessage=new Logger__c(  Log_Description__c=ex.getMessage()+ ': ' + ex.getLineNumber());
       insert logMessage;
        
      }
        }
        
        public static boolean manualCompanyShareRead(Id recordId, Id userOrGroupId){
          
               AccountShare accountShare = new AccountShare();
            accountShare.AccountId = recordId;
            accountShare.UserOrGroupId = userOrGroupId;
            accountShare.AccountAccessLevel = 'Read';
            accountShare.CaseAccessLevel = 'Read';
            accountShare.OpportunityAccessLevel = 'Read';
          
           Database.SaveResult sr = Database.insert(accountShare,false);
           if(sr.isSuccess()){
         // Indicates success
                 return true;
          }
          else {
            return false;
            }
          
        }
    public static attachment downloadDocument(String currentDocumentId,String loggedInAccountId){
         String documentName='';
        string strRestDocumentDownloadEndpoint='';
        string strContentType='';
        string strclientId='';
        string strclientSecret='';  
        string authorization ='';    
        Attachment att;   
        try{
                      
            Http http = new Http();
            HttpRequest request = new HttpRequest();
             AccessTokenResponseWrapper.AccessTokenResponse authToken=AccessTokenGenerator.getAccessTokenResponse(Label.MyGardAccessToken);
                if(authToken!=null &&  authToken.access_token !=null && authToken.token_type!=null){
              authorization = authToken.token_type+' '+authToken.access_token; 
             }
           
            
            for(MyGardDocument_Rest_service_endpoint__c value : [SELECT Id, Name, Value__c FROM MyGardDocument_Rest_service_endpoint__c order by Name desc]){
                if(value.Name == 'Rest_DocumentDownloadservice_endpoint')
                    strRestDocumentDownloadEndpoint=value.Value__c;
                if(value.Name == 'content-type')
                    strContentType=value.Value__c;
                
            }
            
            request.setEndpoint(strRestDocumentDownloadEndpoint + currentDocumentId);
            request.setMethod('GET');
            request.setHeader('content-type', strContentType);            
             request.setheader('Authorization',authorization);            
            request.setTimeout(120000);            
            System.debug('setEndpoint: '+ strRestDocumentDownloadEndpoint + currentDocumentId); 
          
            HttpResponse response = http.send(request);
            system.debug('response ******'+response );
            if (response.getStatusCode() == 200) {
                //get the list of header names (keys)
                system.debug('response.getHeaderKeys()***'+response.getHeaderKeys());
                string[] headerkeys = response.getHeaderKeys();
                
                //create an object to store your header key-value pairs
                Map<string, string> headers = new map<string, string>();
                
                //iterate through they keys, and populate your map
                for(string s : headerkeys){               
                    if(s != null && s != ''){
                        headers.put(s,response.getHeader(s));
                        system.debug('header: ' + s + ' value: ' + response.getHeader(s));
                    }                   
                }
                String base64Response = response.getBody();             
                Blob retBlob = EncodingUtil.base64Decode(base64Response);
                   UserRecordAccess companyRecAccess =[SELECT RecordId, HasReadAccess FROM UserRecordAccess WHERE UserId =:userInfo.getUserId() AND RecordId =:loggedInAccountId];
                
                boolean hasReadAccess=true;
                if(!companyRecAccess.HasReadAccess){
                  hasReadAccess=documentUtil.manualCompanyShareRead(loggedInAccountId,userInfo.getUserId());   
                }
                if(hasReadAccess){
                att = new Attachment();
                att.parentId = loggedInAccountId;   //doc.id;
                att.name = headers.get('filename') + '.' + headers.get('fileextension')  ; //'Attachment' + '.pdf';
                att.contentType=headers.get('Content-Type'); //'application/octet-stream;charset=UTF-8';            
                att.Body = retBlob;                
                insert att;
                
                System.debug('@@ att: '+ att.Id);
                
               }
               }
               
                att.Body = null;
                return att;
        }
         
        catch (exception ex){
         Logger__c logMessage=new Logger__c(  Log_Description__c=ex.getMessage()+ ': ' + ex.getLineNumber());
       insert logMessage;
        
         return null;
        }
    }
    
     @future
    public static void processDeleteDocss(Id recordId){
        /*sfEdit debugs*/Long tStart,tEnd;tStart = 0;tEnd = 0;tStart = System.currentTimeMillis();System.debug('sfEdit processDeleteDoc Starts @ - '+tStart);
       try{
        Attachment attDoc = new Attachment();
        //delete [select id, name, body, Description, ParentId from Attachment where id =: recordId];
           attDoc =[select id, name, body, Description, ParentId from Attachment where id =: recordId];
      List<AccountShare> sharesToDelete = [SELECT Id 
                                        FROM AccountShare 
                                        WHERE AccountId =:attDoc.ParentId AND UserOrGroupId =:userInfo.getUserId() 
                                        AND RowCause = 'Manual'];
              delete attDoc ;
              if(!sharesToDelete.isEmpty()){
              delete sharesToDelete ;
              }
     }
      catch(Exception ex){
       Logger__c logMessage=new Logger__c(  Log_Description__c=ex.getMessage()+ ': ' + ex.getLineNumber());
       insert logMessage;
        
      }
    }
}