/*************************************************************************************************
    Author      : Martin Gardner
    Company     : cDecisions
    Date Created: 13/09/2013
    Description : This class tests that the Data Quality Support class works correctly
    Modified    :   
***************************************************************************************************/
@isTest
private class DataQualitySupportTest {

    static testMethod void testDQConfigCurrentUser() {
      Test.startTest();
        DQ__c testSetting = DataQualitySupport.GetSetting(UserInfo.getUserId());
        System.Debug('Settings = ' + testSetting);        
        DQ__c testSetting2 = DataQualitySupport.GetSetting(UserInfo.getUserId());
        DQ__c testSetting3 = DataQualitySupport.DisableRoleGovernance(UserInfo.getUserId());
        System.Debug('Settings Governance Disabled = ' + testSetting3);
        Test.stopTest();        
    }
    static testMethod void testDQConfigDefault() {
      Test.startTest();
        DQ__c testSetting = DataQualitySupport.GetSetting();
        System.Debug('Settings = ' + testSetting);
    	DQ__c testSetting2 = DataQualitySupport.GetSetting();
        Test.stopTest();       
    }   
    
    static testMethod void testDQConfigDefaultTest() {
      Test.startTest();
        DQ__c testSetting = DataQualitySupport.GetSettingForTest(UserInfo.getUserId());
        System.Debug('Settings = ' + testSetting);
    	DQ__c testSetting2 = DataQualitySupport.GetSetting();    	
    	DQ__c disabled = DataQualitySupport.DisableRoleGovernance(UserInfo.getUserId());
        Test.stopTest();       
    }
}