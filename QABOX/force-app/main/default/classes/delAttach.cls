public class delAttach {
   public static void updateAccforCOB(Set<Id>ids,Set<Id>caseids){
       Set<Accounts_related_with_this_case__c> arcset = new Set<Accounts_related_with_this_case__c>();
       List<Accounts_related_with_this_case__c> arclist = new List<Accounts_related_with_this_case__c>();
       List<Cover_and_Objects_for_case__c> cob = new List<Cover_and_Objects_for_case__c>([SELECT Case__c,Cover__r.AccountId from Cover_and_Objects_for_case__c where id in:ids]);
       
       //awc =[SELECT Case__c,Related_account__c from Accounts_related_with_this_case__c ];
       Set<Id> accountadded = new Set<Id>();
       for(Accounts_related_with_this_case__c aa:[Select Id,Case__c ,Related_account__c from Accounts_related_with_this_case__c where Case__c in:caseids]){
           accountadded.add(aa.Related_account__c);
       
       }
       System.debug('accountadded--->'+accountadded);
       for(Cover_and_Objects_for_case__c cb: cob){
           System.debug('cb--->'+cb);
           Accounts_related_with_this_case__c arcrec = new Accounts_related_with_this_case__c();
           arcrec.Case__c = cb.Case__c;
           arcrec.Related_account__c = cb.Cover__r.AccountId;
           System.debug('1--->'+arcrec.Related_account__c);
           System.debug('2--->'+accountadded.contains(arcrec.Related_account__c));
           if(accountadded.size()>0 && accountadded.contains(arcrec.Related_account__c)){
           }
           else{
           arcset.add(arcrec);
           }
           
       
       }
       arclist.addall(arcset);
       System.debug('arclist--->'+arclist);
       try{
           insert arclist;
       }catch(Exception e){
           System.debug('Insert failed'+e.getMessage());
       }
   
   }
   public static void updateAccforCase(Map<Id,Set<Id>> ids){
       
       List<Accounts_related_with_this_case__c> arclist = new List<Accounts_related_with_this_case__c>();
       Set<Accounts_related_with_this_case__c> accset = new Set<Accounts_related_with_this_case__c>();
       
       Set<Id> accountadded = new Set<Id>();
       for(Accounts_related_with_this_case__c aa:[Select Id,Case__c ,Related_account__c from Accounts_related_with_this_case__c where Case__c in:ids.keyset()]){
           accountadded.add(aa.Related_account__c);
       
       }
       for(Id i:ids.keyset()){
           List <Id> idlist= new List <Id>();
           idlist.addall(ids.get(i));
           for(Integer a=0; a<idlist.size();a++){
               Accounts_related_with_this_case__c acr = new Accounts_related_with_this_case__c();
               acr.Case__c = i;
               acr.Related_account__c =idlist[a];
               //accset.add(acr);
               if(accountadded.size()>0 && accountadded.contains(acr.Related_account__c)){
               }
               else{
                    accset.add(acr);
               }
               System.debug('acr--->'+acr);
           }
       
       
       }
       arclist.addall(accset);
       try{
           insert arclist;
       }catch(Exception e){
           System.debug('Insert failed'+e.getMessage());
       }
       
             
   }
   public static void delAccforCase(Set<String>ids, Set<Id> caseids){
       List<Case> caselist= new List<Case>([select Id, Submitter_Company__c,Account_Id__c, AccountId from Case where id in: caseids]);
       List<Accounts_related_with_this_case__c > toDelList= new List<Accounts_related_with_this_case__c >();
       boolean flag= false;
       Set<String> acid= new Set<String>();
       System.debug('caselist--->'+caselist);
       for(String str: ids){
           for(Case cs:caselist){
               if(str!= cs.AccountId && str!= cs.Account_Id__c && str!= cs.Submitter_Company__c){
                   acid.add(str);
               }
           }
        }   
       System.debug('acid--->'+acid);
       List<Cover_and_Objects_for_case__c > objlist= new List<Cover_and_Objects_for_case__c >([SELECT Id, Case__c,Cover__r.AccountId,Asset_Company_Id__c from Cover_and_Objects_for_case__c where Cover__r.AccountId in:acid AND Case__c in:caseids]);
       System.debug('objlist---'+objlist);
       if(objlist.size()>0){
        flag= true;
       }
         System.debug('flag---'+flag);
       List<Accounts_related_with_this_case__c > acrlist= new List<Accounts_related_with_this_case__c >([Select Id from Accounts_related_with_this_case__c where Related_account__c  in:acid AND Case__c in:caseids] );
       System.debug('acrlist--->'+acrlist);
       if(acrlist.size()>0 && flag == false)
       delete acrlist;
       
   }
   public static void delforCOB(Set<String>ids,Set<Id>caseids){
       Set<Id> accountadded = new Set<Id>();
       Set<Id> accids = new Set<Id>();
       Set<Id> delaccids = new Set<Id>();
       List<String> idslist= new List<String>();
       idslist.addall(ids);
       boolean flag= false;
       System.debug('idslist--->'+ids);
       for(Case ccs:[Select Id, AccountId, Account_Id__c, Submitter_Company__c from case where id in:caseids]){
          
           if((ccs.AccountId!=null && idslist.contains(String.valueof(ccs.AccountId).substring(0,15))) ||(ccs.Account_Id__c!=null && idslist.contains(String.valueof(ccs.Account_Id__c).substring(0,15)))|| (ccs.Submitter_Company__c!=null && idslist.contains(String.valueof(ccs.Submitter_Company__c).substring(0,15))))
           flag= true;
           
       }
       List<Cover_and_Objects_for_case__c > coblistdel= new List<Cover_and_Objects_for_case__c >([SELECT Id, Case__c,Cover__r.AccountId,Asset_Company_Id__c from Cover_and_Objects_for_case__c where Asset_Company_Id__c in:ids AND Case__c in:caseids]);
      
      System.debug('coblistdel size-->'+coblistdel.size());
      if(coblistdel.size()==0){
   
       List<Accounts_related_with_this_case__c > toDelList= new List<Accounts_related_with_this_case__c >();

      
       
           List<Accounts_related_with_this_case__c > delacclist = new List<Accounts_related_with_this_case__c >([Select Id from Accounts_related_with_this_case__c where Related_account__c  in:ids AND Case__c in:caseids ]);
           System.debug('delacclist --->'+delacclist );
           for(Accounts_related_with_this_case__c acs:delacclist){
               toDelList.add(acs);
           }
       
       System.debug('flag--->'+flag);
       if(toDelList.size()>0 && flag == false){
       try{
           delete toDelList;
       }catch(Exception e){
           System.debug('Exception-->'+e.getMessage());
       }
       
       
       
       }
    }   
     
   }
   
}