@isTest
public class TestExportEventService{
    private static Event dataEvent;
    private static void createTestData(){
        dataEvent = new Event();
        dataEvent.ActivityDate=system.today();
        dataEvent.CurrencyIsoCode='USD';
        dataEvent.IsAllDayEvent=false;
        dataEvent.ActivityDateTime=DateTime.newInstance(2018, 01, 18);
        dataEvent.Description='Meeting';
        dataEvent.DurationInMinutes=60;
        dataEvent.EndDateTime=DateTime.newInstance(2018, 01, 18 ,1, 0, 0);
        dataEvent.IsDuplicate__c=false;
        dataEvent.IsPrivate=false;
        dataEvent.Location='Kolkata';
        dataEvent.ShowAs='';
        dataEvent.StartDateTime=DateTime.newInstance(2018, 01, 18);
        dataEvent.Subject='Meeting'; 
        dataEvent.Type='Meeting';
        insert dataEvent;
    }
    
	@isTest public static void testEventService(){
        createTestData();
        ExportEventService.getEvent(dataEvent.id);
        ExportEventService.getBaseURL();
        delete dataEvent;
        ExportEventService.getEvent(dataEvent.id);
    }
}