/**
 * Created by ohuuse on 02/10/2019.
 */
@isTest(seeAllData=true)
public with sharing class EmailMessageServiceTest {
public static Group g1;
public static QueuesObject q1;
public static void testData(){
            g1 = new Group(Name='group name', type='Queue');
            insert g1;
            q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
            insert q1;
            }
    @isTest
    public static void newIncomingEmailTest() {
        testData();
        System.runAs(new User(Id=UserInfo.getUserId())){
        String qId = string.valueof(q1.id);
    //   QueueInEmail__c qe = new QueueInEmail__c(name='Marine Support North', Email__c = 'testcc@test.com', Team__c='CT North', Id__c=qId);
    //    insert qe;
        String customerTransactionString = 'Customer Transactions';
        Id customerTransactionsCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(customerTransactionString).getRecordTypeId();
        
        List<Case> caseList = new List<Case>();     
        Case c = new Case();
        c.status='Closed';
        c.team__c='CT North';
        //c.RecordType.name='Customer Transactions';
        c.RecordTypeId = customerTransactionsCaseRecordTypeId;
        //insert c;
        caseList.add(c);
        
        Case c1 = new Case();
        c1.status='New';
        c1.RecordTypeId = customerTransactionsCaseRecordTypeId;
        c1.team__c='CT North';
        //c.RecordType.name='Customer Transactions';
       // insert c1;
        caseList.add(c1);
        
        insert caseList;
        System.assertequals(caseList.size(),2,'Emails inserted successfully'); 
                
        List<Case> cases = new List<Case>();
        cases.addAll(listBuilderModalControllerTest.createCases(100, false, true, customerTransactionsCaseRecordTypeId));

        List<EmailMessage> emailMessages = new List<EmailMessage>();
        for (Integer i = 0; i < 100; i++) {
            EmailMessage incomingEmailMessage = new EmailMessage(
                    FromAddress = 'test@test.com',
                    ToAddress = 'test@test.com',
                    Subject = 'test Subject',
                    CcAddress ='testcc@test.com',
                    TextBody = 'test TextBody',
                    ParentId = cases[i].Id,
                    Incoming = TRUE
            );
         emailMessages.add(incomingEmailMessage);
        }
        
        
        EmailMessage incomingEmailMessage1 = new EmailMessage(
                    FromAddress = 'test@test.com',
                    ToAddress = 'test@test.com',
                    Subject = 'ref:_test Subject',
                    CcAddress ='testcc@test.com',
                    TextBody = 'test TextBody',
                    ParentId = c.Id,
                    Incoming = TRUE
            );
        emailMessages.add(incomingEmailMessage1);
        
        EmailMessage incomingEmailMessage2 = new EmailMessage(
                    FromAddress = 'test@test.com',
                    ToAddress = 'test@test.com',
                    Subject = 'ref:_test Subject Case Number:'+C1.CaseNumber,
                    CcAddress ='testcc@test.com',
                    TextBody = 'test TextBody',
                    ParentId = c1.Id,
                    Incoming = TRUE
            );
        emailMessages.add(incomingEmailMessage2);
       
        
        insert emailMessages;
        System.assertequals(emailMessages.size(),102,'Emails inserted successfully'); 
        
        
    }
    
    }
 @isTest
    public static void newOutgoingEmailTest() {

        String customerTransactionString = 'Customer Transactions';
        Id customerTransactionsCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(customerTransactionString).getRecordTypeId();

        List<Case> cases = new List<Case>();
        cases.addAll(listBuilderModalControllerTest.createCases(100, false, true, customerTransactionsCaseRecordTypeId));

        List<EmailMessage> emailMessages = new List<EmailMessage>();
        for (Integer i = 0; i < 100; i++) {
            EmailMessage outgoingEmailMessage = new EmailMessage(
                    FromAddress = 'test@test.com',
                    ToAddress = 'test@test.com',
                    Subject = 'test Subject',
                    TextBody = 'test TextBody',
                    ParentId = cases[i].Id,
                    Incoming = false
            );
            emailMessages.add(outgoingEmailMessage );
        }
        EmailMessage outgoingEmailMessage1 = new EmailMessage(
                    FromAddress = 'test@test.com',
                    ToAddress = 'test@test.com',
                    Subject = 'test Subject [Case number'+cases[1] +']',
                    TextBody = 'test TextBody',
                    ParentId = cases[1].Id,
                    Incoming = false
            );
             emailMessages.add(outgoingEmailMessage1 );
        insert emailMessages;
         System.assertequals(emailMessages.size(),101,'Emails inserted successfully');
    }
    @isTest
    public static void automaticReplyEmailTest() {
    String customerTransactionString = 'Customer Transactions';
     Id customerTransactionsCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(customerTransactionString).getRecordTypeId();
         List<Case> caseList = new List<Case>();  
         Case c = new Case();
        c.status='Closed';
        c.team__c='CT North';
        //c.RecordType.name='Customer Transactions';
        c.RecordTypeId = customerTransactionsCaseRecordTypeId;
       
        caseList.add(c);
        Case c1 = new Case();
        c1.status='New';
        c1.RecordTypeId = customerTransactionsCaseRecordTypeId;
        c1.team__c='CT North';
        caseList.add(c1);
        insert caseList;
         String caseNumber=[select caseNumber from Case where id=:c.id].caseNumber;
        system.debug(' c1********'+ caseNumber);
         EmailMessage incomingEmailMessage3 = new EmailMessage(
                    FromAddress = 'test@test.com',
                    ToAddress = 'test@test.com',
                    Subject = 'Automatic reply:test Subject [Case Number: '+caseNumber+' ]',
                    CcAddress ='testcc@test.com',
                    TextBody = 'test TextBody',
                    ParentId = c1.Id,
                    Incoming = TRUE
            );
            system.debug('incomingEmailMessage3 ******'+incomingEmailMessage3.subject);
            insert incomingEmailMessage3 ;
            
    }
}