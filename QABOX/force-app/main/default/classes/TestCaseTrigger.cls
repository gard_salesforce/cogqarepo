@isTest private class TestCaseTrigger{
    private static Case referenceCase;
    private static final Id ctRecTypeId;
    
    static{
        ctRecTypeId = [SELECT id,name FROM RecordType WHERE Name LIKE '%customer transaction%'].id;
    }
    
    private static void createData(){
        Test.startTest();
        GardTestData gtd = new GardTestData();
        gtd.commonRecord();
        Test.stopTest();
        referenceCase = new Case();
        referenceCase.recordTypeId = ctRecTypeId;
        referenceCase.accountId = GardTestData.brokerAcc.Id;
        referenceCase.Submitter_company__c = GardTestData.brokerAcc.Id;
        referenceCase.Account_Id__c = GardTestData.brokerAcc.Id;
        insert referenceCase;
    }
    
    @IsTest private static void tesCaseTriggerInUpdate(){
        createData();
        GardUtils.isUpdateForAssignRule = false;
        referenceCase.Submitter_company__c = GardTestData.clientAcc.Id;
        update referenceCase;
    }
}