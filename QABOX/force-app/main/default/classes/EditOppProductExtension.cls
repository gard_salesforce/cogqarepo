public with sharing class EditOppProductExtension {

        public Opportunity theOpp {get;set;}
        public OpportunityLineItem[] selectedProducts {get;set;}
        //public OpportunityLineItem[] invalidProducts {get;set;}
        public PriceBookEntry[] availableProducts {get;set;}
        
        public String toSelect {get; set;}
        public String toUnselect {get; set;}
        public Integer toUnselectProdIndex {get;set;} //Necessary because a newly added product won't have a line item id
        public String searchString {get;set;}
        
        public boolean overLimit{get;set;}
        private opportunityLineItem[] forDeletion = new opportunityLineItem[]{};
        
        public EditOppProductExtension(ApexPages.StandardController controller)
        {
                theOpp = database.query('select Id, Pricebook2Id, Pricebook2.Name, CurrencyIsoCode, RecordTypeName__c, RecordType.Name from Opportunity where Id = \'' + controller.getRecord().Id + '\' limit 1');
        
        //Populate currently selected products
        selectedProducts = [select Id, Quantity, TotalPrice, UnitPrice, Description, PriceBookEntryId, PriceBookEntry.Name, PriceBookEntry.IsActive, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name, PriceBookEntry.PriceBook2Id, Line_Size__c, PriceBookEntry.Product2.ProductCode, PriceBookEntry.Product2.Family, PriceBookEntry.Product2.Description, Renewal_Premium__c, Actual_Premium__c, Index__c, RA__c, LR__c, General_Increase__c, Renewable_Probability__c, Prognosis_Comments__c from opportunityLineItem where OpportunityId=:theOpp.Id AND PriceBookEntry.PriceBook2Id=:theOpp.Pricebook2Id];
                
        updateAvailableProducts();
        }
        
        public void updateAvailableProducts()
        {
                //String prodQuery = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.IsActive, Product2.Description, UnitPrice from PricebookEntry where IsActive=true and Pricebook2Id = \'' + currPriceBook.Id + '\' and CurrencyIsoCode = \'' + theOpp.get('currencyIsoCode') + '\'';
        String prodQuery = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.IsActive, Product2.Description, UnitPrice, Product2.ProductCode from PricebookEntry where IsActive=true and Pricebook2.IsStandard=true and CurrencyIsoCode = \'' + theOpp.get('currencyIsoCode') + '\'';
        
        //SOC 4/4/13 - Need to add products more than once, which this prevents 
        /*Set<Id> selectedEntries = new Set<Id>();
        for(opportunityLineItem selectedProd:selectedProducts){
            selectedEntries.add(selectedProd.PricebookEntryId);
        }*/
        
        // Append the search string entered by the user in the name OR description
        if(searchString!=null){
            prodQuery+= ' and (Product2.Name like \'%' + searchString + '%\' or Product2.Description like \'%' + searchString + '%\')';
        }
        
        //SOC 4/4/13 - Need to add products more than once, which this prevents 
        /*if(selectedEntries.size()>0){
            String tempFilter = ' and Id not in (';
            for(Id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            
            prodQuery+= extraFilter;
        }*/
        
        //Filter products by opportunity type
        prodQuery+= ' and Product2.Product_Type__c includes (\'' + theOpp.RecordType.Name + '\')';
        prodQuery+= ' order by Product2.Name';
        prodQuery+= ' limit 101';
        
        system.debug('prodQuery = ' + prodQuery);
        availableProducts = database.query(prodQuery);
        
        
        }
        
    public void addProduct(){
    
        // This function runs when a user hits "select" button next to a product
    
        for(PricebookEntry addProd : availableProducts){
            if((String)addProd.Id==toSelect){
                selectedProducts.add(new opportunityLineItem(OpportunityId=theOpp.Id, PriceBookEntry=addProd, PriceBookEntryId=addProd.Id, UnitPrice=addProd.UnitPrice, Quantity=1, Line_Size__c = Decimal.valueOf(OpportunityLineItem.Line_Size__c.getDescribe().getDefaultValueFormula())*100)); //Fixed quantity //added *100 for SF-4748
                break;
            }
        }
        
        updateAvailableProducts();  
    }
        
        public PageReference removeProduct(){
    
        // This function runs when a user hits "remove" on an item in the "Selected Products" section
    
        Integer count = 0;
    
        for(opportunityLineItem selectedProd : selectedProducts){
            if((String)selectedProd.Id==toUnselect){
                if(selectedProd.Id!=null)
                {
                    forDeletion.add(selectedProd);
                }
                
                selectedProducts.remove(count);
                break;
            }
            else if(selectedProd.Id == null)
            {
                //This line item doesn't have an id because the record hasn't been saved yet
                //Use the VF index to remove row
                if(toUnselectProdIndex == count)
                {
                        selectedProducts.remove(count);
                        break;
                }
            }
            count++;
        }
        
        updateAvailableProducts();
        
        return null;
    }
    //
    public PageReference onSave(){
    
        // Previously selected products may have new quantities and amounts, and we may have new products listed, so we use upsert here
        try{
            if(selectedProducts.size()>0)
                upsert(selectedProducts);
             
            //SOC 17/6/13 - Moved this in to the try block and after adding products so that
            //validation on inserting new prod lines will prevent any delets   
            // If previously selected products are now removed, we need to delete them
                if(forDeletion.size()>0)
                delete(forDeletion);
        }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }  
           
        // After save return the user to the Opportunity
        Pagereference p = new Pagereference('/' + ApexPages.currentPage().getParameters().get('Id'));
        p.setRedirect(true);
        return p;
        //return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
    public PageReference onCancel(){
 
        // If user hits cancel we commit no changes and return them to the Opportunity   
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
}