public class FeedItemTriggerHelper{

    public static void processCaseFeedItems(List<FeedItem> caseFeedItems){
        List<Case> parentCases = Database.query('SELECT Id,Type,recordTypeId,recordType.Name,ownerId FROM Case WHERE Id IN (SELECT parentId FROM FeedItem WHERE Id IN :caseFeedItems)');
        for(Case parentCase : parentCases){
            parentCase.Last_Activity__c = 'A chatter post was added.';
            parentCase.Last_Activity_DateTime__c = System.now();
        }
        if(!parentCases.isEmpty()) update parentCases;
    }
}