/******************************************************************************************************
* Class Name: ScheduleDocBatchPopulateDocumentId
* Created By: Nagarjuna Kaipu
* Date: 12-02-2021
* Description: SF-5422 - Used to schedule BatchUpdateDocumentIdOnFiles to get and update document id on files
*******************************************************************************************************
*  Version		Developer			Date			Description
=======================================================================================================
*  1.0          Nagarjuna Kaipu		11/02/2021  	Created
*  1.1          Nagarjuna Kaipu     25/05/2021      SF-6601 
*******************************************************************************************************/
global with sharing class ScheduleDocBatchPopulateDocumentId implements Schedulable {
    public String authToken;
    public Map<Id, String> fileCorelIdMap;
    public String jobName; //SF-6601 added
    
    public ScheduleDocBatchPopulateDocumentId(String authToken, Map<Id, String> fileCorelIdMap, String jobName){ //SF-6601 modified
        this.authToken = authToken;
        this.fileCorelIdMap = fileCorelIdMap;
        this.jobName = jobName; //SF-6601 added
        system.debug('authToken inside Schedule is: '+authToken);
        system.debug('fileCorelIdMap inside Schedule is: '+fileCorelIdMap);
    }
    global void execute(SchedulableContext sc) {
        system.debug('authToken execute is: '+authToken);
        system.debug('fileCorelIdMap execute is: '+fileCorelIdMap);
        ID BatchId = Database.executeBatch(new BatchUpdateDocumentIdOnFiles(authToken, fileCorelIdMap, jobName), 100); //SF-6601 modified
    }
}