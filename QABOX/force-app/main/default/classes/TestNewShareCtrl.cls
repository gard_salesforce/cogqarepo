@isTest
public class TestNewShareCtrl{
    public static Account brokerAcc2;
    ///////*******Variables for removing common literals********************//////////////
    public static string testStr =  'test'; 
    public static string piStr =   'P&I';
    /////**********************end**************************/////
    public static testMethod void TestNewShareCtrlMethod(){
    GardTestData GT = new GardTestData();
    GT.commonRecord();
    
    string sfLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id; 
     user   salesforceLicUser2 = new User(
                                        Alias = 'standt1', 
                                        profileId = sfLicenseId ,
                                        Email='standarduser1@testorg.com',
                                        EmailEncodingKey='UTF-8',
                                        CommunityNickname = 'test113',
                                        LastName='Testing1',
                                        LanguageLocaleKey='en_US',
                                        LocaleSidKey='en_US', 
                                        //contactID = brokercontact.id, 
                                        TimeZoneSidKey='America/Los_Angeles',
                                        UserName='mygardtest1008@testorg.com.mygard',
                                        contactId__c = GardTestData.grdobj.id
                                   );
     Test.startTest();
     insert salesforceLicUser2;
     //GardTestData.brokerAcc.ownerID = GardTestData.salesforceLicUser.ID;
     //update GardTestData.brokerAcc;
     Market_Area__c Markt2= new Market_Area__c(Market_Area_Code__c = 'abc120');
     insert Markt2;
     brokerAcc2 = new Account(   Name='test,Name',
                                    BillingCity = 'Southampton1',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS20 AD!',
                                    BillingState = 'Test Avon LAng' ,
                                    BillingStreet = '3 Mangrove Road',
                                    recordTypeId=System.Label.Broker_Contact_Record_Type,
                                    Site = '_www.IBM.se',
                                    Type = 'prospect',
                                    company_Role__c = 'Broker',
                                    //Company_ID__c = '64143',
                                    guid__c = '8ce8ad89-a6ed-1836-9e18',
                                    Market_Area__c = Markt2.id,
                                    Product_Area_UWR_2__c= piStr ,
                                    Product_Area_UWR_4__c= piStr ,
                                    Product_Area_UWR_3__c='Marine',
                                    Role_Broker__c = true,
                                    Area_Manager__c = salesforceLicUser2.id ,
                                    X2nd_UWR__c= salesforceLicUser2.id,
                                    Acquired_By__c = GardTestData.brokerAcc.Id,
                                    Enable_share_data__c = true,
                                    Claim_handler_Crew__c = salesforceLicUser2.id,
                                    Company_Status__c='Active',
                                    Company_Role_Text__c = 'Broker'
                                  //  Ownerid=salesforceLicUser.id
                               );
    insert  brokerAcc2 ;
    
    agreement_set__c agreementSet = new agreement_set__c();
    agreementSet.agreement_set_type__c =  testStr ;
    agreementSet.shared_by__c = GardTestData.brokerAcc.Id;
    agreementSet.shared_with__c = brokerAcc2.Id;
    agreementSet.status__c = 'Suspended';
    agreementSet.Shared_with_account__c = true;
    insert agreementSet ;
    String guid = [Select guid__c from Agreement_set__c where id =:agreementSet.id].guid__c;
    agreementSet.guid__c = guid;
    update agreementSet;
    System.assertEquals(agreementSet.status__c , 'Suspended');
    broker_share__c brokerShare = new broker_share__c();
    brokerShare.Active__c = true;
    brokerShare.Agreement_set_id__c = agreementSet.Id;
    brokerShare.contract_external_id__c = 'TestId001';
    brokerShare.Contract_id__c = GardTestData.brokercontract_1st.Id;
    brokerShare.shared_to_client__c = true;
    insert brokerShare;
    System.assertEquals(brokerShare.contract_external_id__c , 'TestId001');
    
    contact_share__c conShare = new contact_share__c(
                                                         Broker_share_id__c = brokerShare.ID,
                                                         ContactId__c = GardTestData.brokerUser.ID
                                                     );
    insert conShare; 
    
    Account_Contact_Mapping__c Accmap_1st = new Account_Contact_Mapping__c(Account__c = GardTestData.brokerAcc.id,
                                                   Active__c = true,
                                                   Administrator__c = 'Admin',
                                                   Contact__c = GardTestData.brokerContact.id,
                                                   IsPeopleClaimUser__c = false,
                                                   Marine_access__c = true,
                                                   PI_Access__c = true
                                                   //Show_Claims__c = false,
                                                   //Show_Portfolio__c = false 
                                                   );
     Share_data_Target_Company__c ShareTCo = new Share_data_Target_Company__c (Shared_By_Broker__c = GardTestData.brokerAcc.Id ,
                                                                               Shared_With_Broker__c = brokerAcc2.Id
                                                                               );
    insert ShareTCo ;
    
    /*OrgWideEmailAddress oweaTest = new OrgWideEmailAddress(
                                                               DisplayName = 'no-reply@gard.no'
                                                           );
    insert oweaTest;*/
    
    CommonVariables__c cvTest = new CommonVariables__c(Name = 'ReplyToMailId',Value__c = 'ownergard@gmail.com');
    insert cvTest;
    
    //Test.startTest();
    system.runAs(GardTestData.brokerUser)
    {
        NewShareCtrl NewShare = new NewShareCtrl();
        PageReference pageRef = Page.NewShare;
        Test.setCurrentPage(pageRef);
        NewShare.shareGUID = ApexPages.CurrentPage().getParameters().put('shareId',brokerAcc2.guid__c);
        NewShare.selectedPolicyYr.add('2015');
        NewShare.selectedProdArea.add( piStr );
        NewShare.lstPolYrOptions.add('2014');
        NewShare.lstProductAreaOptions.add( testStr );
        NewShare.selectedProdAreaName =  piStr ;
        NewShare.selectedContractId.add(GardTestData.brokercontract_1st.Id);
        NewShare.selectedBroker = GardTestData.brokerAcc.Id;
        //NewShare.selectedBroker = brokerAcc2.Id;
        NewShare.selectedContact.add(String.valueOf(GardTestData.brokerContact.Id)) ;
       // NewShare.SelectedClient = brokerShare.Contract_id__r.Broker__c;
        NewShare.mapBrokerClient.put(GardTestData.brokercontract_1st.Client__c,NewShare.SelectedClient );
        NewShareCtrl.AgreementWrapper temp = new NewShareCtrl.AgreementWrapper(GardTestData.brokercontract_1st.Id, testStr ,'Charterers','2015', piStr , testStr ,true);
       // NewShareCtrl.agreementWrapperMap.add(brokerShare.Contract_id__c ,temp );

       // NewShare.selectedContractId.add(GardTestData.brokerContact.Id);
        // NewShare.selectedBroker = 'standt';
    // MyGardHelperCtrl.ACM_JUNC_OBJ = GardTestData.Accmap_list.Accmap_1st ;
  //  MyGardHelperCtrl.ACM_JUNC_OBJ = Accmap_1st ;
    //MyGardHelperCtrl.CreateCommonData();
        NewShare.mapObject.put(GardTestData.test_object_1st.Id,GardTestData.test_object_1st.Name);
        NewShare.mapIdClient.put(GardTestData.clientAcc.Id,GardTestData.clientAcc.Name );
        //NewShare.SelectedClient = GardTestData.clientAcc.Id;------------------------
        NewShare.SelectedClient = String.valueOf(GardTestData.client_Acc);
        NewShare.mapBrokerClient.put(GardTestData.brokerAcc.Id,GardTestData.clientAcc.Id);
        NewShare.mapIdBroker.put(GardTestData.brokerAcc.Id,GardTestData.brokerAcc.Name);
        NewShare.getPolYrOptions();
        NewShare.getProductAreaOptions();
        //NewShare.getBrokerLst();------------------------------------
        //NewShare.getContactLst();  ------------------------------------
        NewShare.getCName();
        NewShare.getBrokerName();
        NewShare.fetchGlobalClients();
        NewShare.populateContract();
        NewShare.populateBroker();
        NewShare.populateContact();
       // NewShare.createAggreementSet();----------------------------------
        NewShare.param = 'All-Add';
        NewShare.addRemoveContract();
        NewShare.param = 'All-Remove';
        NewShare.addRemoveContract();
        NewShare.param = 'Remove';
        NewShare.addRemoveContract();
        NewShare.param = 'All-Add';
        NewShare.addRemoveContract();
        NewShare.populateProdArea();
        NewShare.populatePolicyYr();
        NewShare.populateObject();
        NewShare.fetchClientInfo();
        NewShare.getContactLst();
        NewShare.createAggreementSet();
        NewShare.selectedBroker = brokerAcc2.Id;
        //NewShare.selectedContractId.add(brokerShare.Contract_id__c );
        NewShare.createAggreementSet();
        PageReference pageRef2 = Page.NewShare;
        Test.setCurrentPage(pageRef2);
        //System.currentPageReference().getParameters().put('shareId', brokerAcc2.Id);------------------
        
        System.currentPageReference().getParameters().put('shareId', guid );
        NewShareCtrl NewShare2 = new NewShareCtrl();
        
        System.debug('agreementSet************** '+guid);
        System.debug('GardTestData.BrokerContact **********'+GardTestData.BrokerContact);
        
        NewShare2.fetchClientInfo();
        NewShare.createAggreementSet();
        NewShare.agSet = new agreement_set__c();
        NewShare.agSet = agreementSet;
        NewShare.editAggreementSet();
        NewShare.getBrokerLst();
        NewShare.getClientLst();
        NewShare.selectedContractId.add(brokerShare.Contract_id__c );
        NewShare.agreementWrapperMap.put(String.valueOf(brokerShare.Contract_id__c),temp);
        //system.debug('agreementWrapperMap===>>'+NewShare.agreementWrapperMap.get(NewShare.selectedContractId));
        NewShare.gotoReview();
        NewShare.gotoPrevious();
        NewShare.sendToForm();
        NewShare.gotoCancel();
        NewShare.agSet = new agreement_set__c();
        NewShare.agSet = agreementSet;
        NewShare.editAggreementSet();
        NewShare.toggleShare();
        NewShare.getRecordsforEmail();
        //NewShare.isShareWithAcc = 'Selected contacts';
        NewShare.selectedBroker = GardTestData.brokerAcc.Id;
        //system.debug('loggedInAccountId ++++ '+NewShare.loggedInAccountId+GardTestData.brokerAcc.ID+GardTestData.grdobj.ID);
        NewShare.gardConIDForTest = GardTestData.grdobj.ID;
        //system.debug('test for sennotifmail()---->>'+NewShare.gardConIDForTest+[SELECT Id, Email__c from Gard_Contacts__c WHERE ID =: NewShare.gardConIDForTest]);
        NewShare.sendNotificationEmail();
        
     }
    system.runas(GardTestData.clientUser)
    {
        NewShareCtrl NewShare = new NewShareCtrl();
        PageReference pageRef = Page.NewShare;
        Test.setCurrentPage(pageRef);
        NewShare.gardConIDForTest = GardTestData.grdobj.ID;
        NewShare.selectedPolicyYr.add('2015');
        NewShare.selectedProdArea.add( piStr );
        NewShare.getPolYrOptions();
        NewShare.getProductAreaOptions();
        NewShare.getClientLst();
        NewShare.getBrokerLst();
        NewShare.getContactLst();
        NewShare.getCName();
        NewShare.getBrokerName();
        NewShare.fetchGlobalClients();
        NewShare.populateContract();
        NewShare.populateBroker();
        NewShare.populateContact();
        NewShare.createAggreementSet();
        NewShare.addRemoveContract();
        NewShare.populateProdArea();
        NewShare.populatePolicyYr();
        NewShare.populateObject();
        NewShare.fetchClientInfo();
        NewShare.gotoReview();
        NewShare.gotoPrevious();
        NewShare.sendToForm();
        NewShare.gotoCancel();
        }
    test.stopTest();
    }
}