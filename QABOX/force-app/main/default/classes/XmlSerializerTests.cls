/**************************************************************************
* Author  : Phil Spenceley
* Company : cDecisions
* Date    : 01/08/2013
***************************************************************************/
    
/// <summary>
///  Unit Tests for the XmlSerializer Class
/// </summary>
@isTest
private class XmlSerializerTests {
    
    public class CompanyMock implements XmlSerializable {
    
        public string Id { get; set; }
        public string Name { get; set; }
        public Datetime CreatedDateTime { get; set; }
        public Date CreatedDate { get { return Date.newInstance(CreatedDateTime.year(), CreatedDateTime.month(), CreatedDateTime.day()); } } 
        public string Address { get; set; }
        
        public CompanyMock() {
            this.CreatedDateTime = Datetime.now();
        }
        
        public object get(string fieldName) {
            Map<string, object> thisobjectmap = new Map<string, object> 
            { 
                'Id' => this.Id, 
                'Name' => this.Name,
                'CreatedDateTime' => this.CreatedDateTime,
                'CreatedDate' => this.CreatedDate,
                'Address' => this.Address
            };
            return thisobjectmap.get(fieldname);
        }
        
        public boolean put(string fieldName, object value) {
            if (fieldName == 'Id') {
                this.Id = String.valueOf(value);
            } else if (fieldName == 'Name') {
                this.Name = String.valueOf(value);      
            } else if (fieldName == 'CreatedDateTime') {
                this.CreatedDateTime = Datetime.valueof(string.valueOf(value));    
            } else if (fieldName == 'Address') {
                this.Address = String.valueOf(value);
            } else {
                return false;
            }
            return true;
        }
        
        public Set<string> getFields() {
            Set<string> fields = new Set<string> 
            { 
                'Id', 
                'Name',
                'CreatedDateTime',
                'CreatedDate',
                'Address'
            };
            
            return fields;
        }
    }
    
    static Account setupAccountData() {
        Account a = new Account();
        a.Name = 'ABC Shipping';
        //TODO: if we insert here, it will call the trigger... Don't want to call web service during tests...
        insert a;
        return a;
    }
    
    static CompanyMock setupCompanyMockData() {
        CompanyMock mock = new CompanyMock();
        mock.Id = 'ABC001';
        mock.Name = 'ABC Shipping';
        return mock;
    }
    
    /// <summary>
    ///  Confirm that the Serialize sObject method returns correctly structured XML
    /// </summary>  
    static testMethod void SerializeSObject() {
        //Arrange...
        Account a = setupAccountData();
        
        //Act...
        Test.startTest();
        Dom.Document accountXml = new XmlSerializer().Serialize(a);
		Dom.Document accountXml2 = new XmlSerializer().Serialize(a, false);
		Dom.Document accountXml3 = new XmlSerializer().Serialize(a, new List<String> {'id', 'name'});
        Test.stopTest();
        
        //Assert...
        System.assertEquals('Account', accountXml.getRootElement().getName(), 'Root node name = \'Account\'');
        System.assertEquals('ABC Shipping', accountXml.getRootElement().getChildElement('name', null).getText(), 'Account.Name = \'ABC Shipping\'');
		System.assertEquals('Account', accountXml2.getRootElement().getName(), 'Root node name = \'Account\'');
        System.assertEquals('ABC Shipping', accountXml2.getRootElement().getChildElement('name', null).getText(), 'Account.Name = \'ABC Shipping\'');
		System.assertEquals('Account', accountXml3.getRootElement().getName(), 'Root node name = \'Account\'');
        System.assertEquals('ABC Shipping', accountXml3.getRootElement().getChildElement('name', null).getText(), 'Account.Name = \'ABC Shipping\'');
    }
    
    /// <summary>
    ///  Confirm that the Deserialize sObject method returns the correct sObject
    /// </summary>  
    static testMethod void DeserializeSObject() {
        //Arrange...
        Dom.Document accountXml = new Dom.Document();
        Dom.XmlNode root = accountXml.createRootElement('Account', null, null);
        //root.addChildElement('id', null, null).AddTextNode('ABC001');
        root.addChildElement('name', null, null).AddTextNode('ABC Shipping');
		//TODO: date format not what we would expect in xml... 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\''
        root.addChildElement('CreatedDate', null, null).AddTextNode(Datetime.now().format('yyyy-MM-dd HH:mm:ss'));
		
		Account insertAccount = setupAccountData();
		Dom.Document accountXml2 = new Dom.Document();
        Dom.XmlNode root2 = accountXml2.createRootElement('Account', null, null);
        root2.addChildElement('id', null, null).AddTextNode(insertAccount.id);
        root2.addChildElement('name', null, null).AddTextNode('ABC Shipping');
		//TODO: date format not what we would expect in xml... 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\''
        root2.addChildElement('CreatedDate', null, null).AddTextNode(Datetime.now().format('yyyy-MM-dd HH:mm:ss'));
        
        //Act...
        Test.startTest();
        Account account = (Account)new XmlSerializer().Deserialize(accountXml);
		Account account2 = (Account)new XmlSerializer().Deserialize(accountXml2);
        Test.stopTest();
        
        //Assert...
        System.assertEquals('ABC Shipping', account.Name, 'Account.Name');
		System.assertEquals('ABC Shipping', account2.Name, 'Account2.Name');
		System.assertEquals(insertAccount.id, account2.id, 'Account2.id');
    }
    
    /// <summary>
    ///  Confirm that the Serialize Object method returns correctly structured XML
    /// </summary>  
    static testMethod void SerializeObject() {
        //Arrange...
        CompanyMock mock = setupCompanyMockData();
        
        //Act...
        Test.startTest();
        Dom.Document companyXml  = new XmlSerializer().Serialize(mock, XmlSerializerTests.CompanyMock.class);
        Test.stopTest();
        
        //Assert...
        System.assertEquals('XmlSerializerTests.CompanyMock', companyXml.getRootElement().getName(), 'Root node name = \'CompanyMock\'');
        System.assertEquals('ABC Shipping', companyXml.getRootElement().getChildElement('Name', null).getText(), 'CompanyMock.Name = \'ABC Shipping\'');
    }
    
    /// <summary>
    ///  Confirm that the Serialize Object method returns the correct object
    /// </summary>  
    static testMethod void DeserializeObject() {
        //Arrange...
        Dom.Document companyXml = new Dom.Document();
        Dom.XmlNode root = companyXml.createRootElement('XmlSerializerTests.CompanyMock', null, null);
        root.addChildElement('ID', null, null).AddTextNode('ABC001');
        root.addChildElement('Name', null, null).AddTextNode('ABC Shipping');
		//TODO: date format not what we would expect in xml... 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\''
        root.addChildElement('CreatedDateTime', null, null).AddTextNode(Datetime.now().format('yyyy-MM-dd HH:mm:ss'));
        
        //Act...
        Test.startTest();
        CompanyMock mock  = (CompanyMock)new XmlSerializer().Deserialize(companyXml, 'XmlSerializerTests.CompanyMock');
        Test.stopTest();
        
        //Assert...
        System.assertEquals('ABC Shipping', mock.Name, 'CompanyMock.Name');
    }
    
}