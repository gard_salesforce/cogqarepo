@isTest private class TestPemeWebService {

    private static PEME_Invoice__c pemeInvoice; 
    private static ContentVersion contentVersion1;
    
    @isTest private static void TestJobQueueable(){
        createData();
        System.enqueueJob(new PemeWsJobs(pemeInvoice.id));//job1
        System.enqueueJob(new PemeWsJobs(new PemeWsJobs(pemeInvoice.id)));//job2
    }
    
    @isTest private static void TestWebServiceCaller(){
        createData();
        Id cvId = SaveInvoiceAsCsvFile.saveInvoice(pemeInvoice.Id);
        if(cvId == null) cvId = contentVersion1.Id;
        WebServiceCaller.callPemeWs(pemeInvoice.Id,cvId);
    }
    
    private static void createData(){
        List<Mule_Web_Service__c> mwsCss = new List<Mule_Web_Service__c>();
        mwsCss.add(new Mule_Web_Service__c(Name = 'Retreive access token',Endpoint_Url__c = 'someEndpointUrl',Client_Id__c = 'someClientId',Client_secret__c = 'someClientSecret',Enabled__c = true));
        mwsCss.add(new Mule_Web_Service__c(Name = 'PEME Invoice And File Service',Endpoint_Url__c = 'someEndpointUrl',Client_Id__c = 'someClientId',Client_secret__c = 'someClientSecret',Enabled__c = true));
        insert mwsCss;
        	
        
        CommonVariables__c cvar = new CommonVariables__c(Name='ReplyToMailId',Value__c='test@example.com');
        insert cvar;
        Gard_Contacts__c gc = new Gard_Contacts__c(FirstName__c ='test',lastName__c = 'test');
        insert gc;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab12000');
        insert Markt;
        
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        
        Gard_Team__c gardTeam=new Gard_Team__c(P_I_email__c='test@test.com',Region_code__c='CASM', Office__c='Arendal');
        insert gardTeam;
        
        Valid_Role_Combination__c vrcESP = new Valid_Role_Combination__c(Role__c = 'External Service Provider',Sub_Role__c='ESP - Agent');
        insert vrcESP;
        
        Country__c country=new Country__c(Claims_Support_Team__c=gardTeam.id);
        insert country;
        
        String salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
        //User creation
        User user1 = new User(
                            Alias = 'standt', 
                            profileId = salesforceLicenseId ,
                            Email='standarduser@testorg.com',
                            EmailEncodingKey='UTF-8',
                            CommunityNickname = 'test13',
                            LastName='Testing',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US',  
                            TimeZoneSidKey='America/Los_Angeles',
                            UserName='testGard4553'+UserInfo.getOrganizationId()+'@testGardProd.com',
                            isActive = true ,
                            ContactId__c = gc.id,
                            City= 'Arendal'
                             );
        insert user1 ;
        
        //Client account 
        Blob b1 = Crypto.GenerateAESKey(128);            //sfedit commented by pulkit 88-92
        String h1 = EncodingUtil.ConvertTohex(b1);            
        String guid1 = h1.SubString(0,8)+ '-' + h1.SubString(8,12) + '-' + h1.SubString(12,16) + '-' + h1.SubString(16,20);
        
        List<Account> accountList = new List<Account>();
        Account clientAcc = New Account();
        clientAcc.Name = 'Testt';
        clientAcc.recordTypeId = System.Label.Client_Contact_Record_Type;
        clientAcc.Site = '_www.test.se';
        clientAcc.Type = 'Customer';
        clientAcc.BillingStreet = 'Gatan 1';
        clientAcc.BillingCity = 'Stockholm';
        clientAcc.BillingCountry = 'SWE';    
        clientAcc.Area_Manager__c = user1.id;        
        clientAcc.Market_Area__c = Markt.id; 
        clientAcc.GUID__c = guid1;
        clientAcc.company_Role__c = 'Client';
        clientAcc.PEME_Enrollment_Status__c='Enrolled';  
        clientAcc.company_Role__c = 'External Service Provider';
        clientAcc.Sub_Roles__c = 'ESP - Agent';
        clientAcc.country__c = country.Id;
        accountList.add(clientAcc);
        
        Blob b = Crypto.GenerateAESKey(128);            
        String h = EncodingUtil.ConvertTohex(b);            
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20);
        
        Account clinicAcc = new Account( Name='testClinic',
                                        BillingCity = 'Bristol',
                                        BillingCountry = 'United Kingdom',
                                        BillingPostalCode = 'BS1 1AD',
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.ESP_Contact_Record_Type,
                                        company_Role__c='External Service Provider',
                                        Sub_Roles__c = 'ESP - Agent',
                                        Site = '_www.ctsclinic.se',
                                        Type = 'Surveyor' ,
                                        Area_Manager__c = user1.id,      
                                        Market_Area__c = Markt.id,
                                        GUID__c = guid,
                                        PEME_Enrollment_Status__c='Enrolled',
                                        country__c = country.Id
                                     );
        AccountList.add(clinicAcc);
        
        insert AccountList;
        
        clinicAcc = [SELECT id,Company_Id__c,name FROM Account Where id = :clinicAcc.id];
        //GardTestData.clinicAcc = clinicAcc;
        
        List<PEME_Exams__c> pemeExamsCs = new List<PEME_Exams__c>();
        pemeExamsCs.add(new PEME_Exams__c(name='cs1',Company_Id__c=clinicAcc.Company_ID__c,Company_Name__c=clinicAcc.name,Record_Type__c='check',Cost__c=500,Exam_Name__c='check1'));
        pemeExamsCs.add(new PEME_Exams__c(name='cs2',Company_Id__c=clinicAcc.Company_ID__c,Company_Name__c=clinicAcc.name,Record_Type__c='check',Cost__c=1000,Exam_Name__c='check2'));
        pemeExamsCs.add(new PEME_Exams__c(name='cs3',Company_Id__c=clinicAcc.Company_ID__c,Company_Name__c=clinicAcc.name,Record_Type__c='radio',Cost__c=100,Exam_Name__c='radio1'));
        pemeExamsCs.add(new PEME_Exams__c(name='cs4',Company_Id__c=clinicAcc.Company_ID__c,Company_Name__c=clinicAcc.name,Record_Type__c='radio',Cost__c=200,Exam_Name__c='radio2'));
        insert pemeExamsCs;
        
        List<Contact> ContactList = new List<Contact>();
        //Client Contact
        Contact clientContact = new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity = 'London',
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState = 'London',
                                              MailingStreet = '4 London Road',
                                              AccountId = clientAcc.Id,
                                              Email = 'test321@gmail.com'
                                          );
        ContactList.add(clientContact);
        
        //Clinic contact
        Contact clinicContact = new Contact( 
                                             FirstName='medicalTest',
                                             LastName='medical',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState = 'London',
                                             MailingStreet = '1 London Road',
                                             AccountId = clinicAcc.Id,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(clinicContact) ;
        insert ContactList;
        
        List<User> UserList = new List<User>();
        String partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
        //Partner user
        User clientUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'clientUser',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey='en_US',
                                    LocaleSidKey='en_US',
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testclientUser@testorg.com.mygard',
                                    ContactId = clientContact.id
                                   );
                
         UserList.add(clientUser) ;
         
         User clinicUser = new User(
                                    Alias = 'Clinic', 
                                    profileId = partnerLicenseId,
                                    Email='testClinic120@test.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='ClinicTesting',
                                    LanguageLocaleKey='en_US',
                                    CommunityNickname = 'clinicUser',
                                    LocaleSidKey='en_US',  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testClinicUser@testorg.com.mygard',
                                    ContactId = clinicContact.Id
                                   );
        UserList.add(clinicUser);
        insert UserList;
        
        Test.startTest();
        //Object Creation
        List<Object__c> ObjectList = new List<Object__c>();
        Object__c Object1 = new Object__c(Name= 'TestObject1',Object_Unique_ID__c='test11223');
        ObjectList.add(Object1);
        Object__c Object2 = new Object__c(Name= 'TestObject2',Object_Unique_ID__c='test11224');
        ObjectList.add(Object2);
        insert ObjectList;
         
        //Enrollment Form
        List<PEME_Enrollment_Form__c> pefs = new List<PEME_Enrollment_Form__c>();
        PEME_Enrollment_Form__c pef = new PEME_Enrollment_Form__c();
        pef.ClientName__c=clientAcc.ID;
        pef.Comments__c='Report analyzed';
        pef.Enrollment_Status__c= 'Enrolled';
        pef.Gard_PEME_References__c='Test References1,Test References2';
        pef.Last_Status_Change_Date__c=Date.valueOf('2016-01-27');
        pef.Submitter__c=clinicContact.ID;
        pefs.add(pef);
        insert pefs;
         
         //Manning agent
         PEME_Manning_Agent__c pmg = new PEME_Manning_Agent__c();
         pmg.Client__c=clinicAcc.ID;
         pmg.PEME_Enrollment_Form__c=pef.ID;
         pmg.Company_Name__c='Test Company';
         pmg.Contact_Point__c=clinicContact.Id;
         pmg.Status__c='Approved';
         insert pmg;
         
        List<PEME_Debit_note_detail__c> pdnList = new List<PEME_Debit_note_detail__c>();
        //Debit Note detail
        PEME_Debit_note_detail__c pdn = new PEME_Debit_note_detail__c();
        pdn.Comments__c = 'testing';
        pdn.Company_Name__c=clientAcc.ID;
        pdn.Contact_person_Email__c = 'test@mail.com';
        pdn.Contact_person_Name__c = 'tester';
        pdn.PEME_Enrollment_Form__c = pef.ID;
        pdn.Requested_Address__c='test1';
        pdn.Requested_Address_Line_2__c='test2';
        pdn.Requested_Address_Line_3__c='test3';
        pdn.Requested_Address_Line_4__c='test4';
        pdn.Requested_City__c='testCity';
        pdn.RequestedComments__c = 'testing';
        pdn.Requested_Contact_person_Email__c = 'test@mail.com';
        pdn.Requested_Contact_person_Name__c = 'tester';
        pdn.Requested_Country__c ='testCountry';
        pdn.Requested_State__c='testState';
        pdn.Requested_Zip_Code__c='testZip';
        pdn.Status__c='Pending Update';
        
        PEME_Debit_note_detail__c pdn2 = new PEME_Debit_note_detail__c();
        pdn2.Comments__c = 'testing';
        pdn2.Company_Name__c=clientAcc.ID;
        pdn2.Contact_person_Email__c = 'test@mail.com';
        pdn2.Contact_person_Name__c = 'tester';
        pdn2.PEME_Enrollment_Form__c = pef.ID;
        pdn2.Requested_Address__c='test1';
        pdn2.Requested_Address_Line_2__c='test2';
        pdn2.Requested_Address_Line_3__c='test3';
        pdn2.Requested_Address_Line_4__c='test4';
        pdn2.Requested_City__c='testCity';
        pdn2.RequestedComments__c = 'testing';
        pdn2.Requested_Contact_person_Email__c = 'test@mail.com';
        pdn2.Requested_Contact_person_Name__c = 'tester';
        pdn2.Requested_Country__c ='testCountry';
        pdn2.Requested_State__c='testState';
        pdn2.Requested_Zip_Code__c='testZip';
        pdn2.Status__c='Approved';
        pdnList.add(pdn2);
        pdnList.add(pdn);
        insert pdnList;
        
        
         //PEME Invoice
        pemeInvoice = new PEME_Invoice__c ();
        pemeInvoice.Client__c=ClientAcc.id;
        pemeInvoice.Clinic_Invoice_Number__c='qwerty123';
        pemeInvoice.Clinic__c=clinicAcc.id;
        pemeInvoice.Crew_Examined__c=11223;
        pemeInvoice.Invoice_Date__c=Date.valueOf('2016-01-05');
        pemeInvoice.PEME_reference_No__c='aasd23';
        pemeInvoice.Status__c='Draft';
        pemeInvoice.ObjectList__c='qwerty for test purpose';
        pemeInvoice.Total_Amount__c=112230;
        pemeInvoice.Vessels__c= Object1.id;
        pemeInvoice.PEME_Enrollment_Detail__c = pef.id;
        pemeInvoice.Period_Covered_From__c=Date.valueOf('2016-01-05');
        pemeInvoice.Period_Covered_To__c =Date.valueOf('2016-01-28');
        pemeInvoice.Brief_Description__c = 'test';
        pemeInvoice.Bank_address__c = 'test';
        pemeInvoice.Manning_agent__c = pmg.Id;
        insert pemeInvoice;
        
        //PEME Exam Detail
        PEME_Exam_Detail__c PExamDetail = new PEME_Exam_Detail__c();
        PExamDetail.Age__c=30;
        PExamDetail.Amount__c=12000;
        PExamDetail.Date__c=Date.valueOf('2016-01-10');
        PExamDetail.Examination__c='Demo Exam';
        PExamDetail.Examinee__c='Demo employee';
        PExamDetail.Invoice__c=pemeInvoice.id;
        PExamDetail.Object_Related__c=Object1.id;
        PExamDetail.Rank__c='Five';
        PExamDetail.Status__c='Approved';
        insert PExamDetail;
        
        //PEME Contact
        PEME_Contact__c pcTest = new PEME_Contact__c();
        pcTest.Contact__c = clientContact.ID;
        pcTest.PEME_Enrollment_Detail__c = pef.ID;
        Insert pcTest;
        
        List<ContentVersion> cvList= new List<ContentVersion>();
        contentVersion1 = new ContentVersion(Title = 'Penguins',
                                                            PathOnClient = 'Penguins.css',
                                                            VersionData = Blob.valueOf('Test Content1'),
                                                            IsMajorVersion = true
                                                           );
        cvList.add(contentVersion1);
        insert cvList;
        
        contentVersion1 = [SELECT Id, ContentDocumentId  FROM ContentVersion where Id = :contentVersion1.id limit 1]; 
        
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        
        ContentDocumentLink caseContentLink = new ContentDocumentLink();
        caseContentLink.LinkedEntityId = pemeInvoice.id;
        caseContentLink.ShareType = 'I';
        caseContentLink.ContentDocumentId = contentVersion1.ContentDocumentId;
        caseContentLink.Visibility = 'AllUsers'; 
        
    }
}