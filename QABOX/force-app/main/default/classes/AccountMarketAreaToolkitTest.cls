/*************************************************************************************************
    Author      : Martin Gardner
    Company     : cDecisions
    Date Created: 
    Description : This class test the helper methods for working with Market areas
    This class assumes that there is some market area data already in the org to test with.
    There is a validation rule that prevents us from creating accounts with some of the market areas
    so these have been explicitly exluded from the tests.
    Modified    :   
***************************************************************************************************/
@isTest
private class AccountMarketAreaToolkitTest {

    @isTest(SeeAllData = true)
    static void testMarketAreas() {
       
       Test.startTest();
        Map<String, String> mapMA = AccountMarketAreaToolkit.mapMarketAreaManagers;
        List<Market_Area__c> lstMA = [SELECT Id, Name FROM Market_Area__c];
        Map<String, String> testMap = new Map<String,String>();
        Integer i = 0;
        for(Market_Area__c ma : lstMA){
            testMap.put('Test ' + i +' ' + ma.Name, ma.Name);
            i++;    
        }       
        
        
        AccountMarketAreaToolkit.getAccountMarketAreaManagers(testMap);
        
        System.Debug('** Test Result Map *** -> ' + testMap);
        
       Test.stopTest();
    }
    @isTest(SeeAllData = true)
    static void testMarketAreaIds() {
       
       Test.startTest();
        Map<String, String> mapMA = AccountMarketAreaToolkit.mapMarketAreaManagerIds;
        List<Market_Area__c> lstMA = [SELECT Id, Name FROM Market_Area__c];
        Map<String, String> testMap = new Map<String,String>();
        Integer i = 0;
        for(Market_Area__c ma : lstMA){
            testMap.put('Test ' + i +' ' + ma.Name, ma.Id);
            i++;    
        }       
        
        
        AccountMarketAreaToolkit.getAccountMarketAreaManagerIds(testMap);
        
        System.Debug('** Test Result Map *** -> ' + testMap);
        
       Test.stopTest();
    }  
    @isTest(SeeAllData = true)
    static void testsetAccountMarketAreaManagers(){
        List<Account> testAccounts = new List<Account>();
        //Olga added a validation rule that says we can't create accounts and
        //link them to the following market areas so we have to exclude them here.
        Set<String> excludeMA = new Set<String>{'CHATR', 'LATLN', 'MOUOS', 'AWEEA','AMEN','NRDSC'};
        List<Market_Area__c> lstMA = [SELECT Id, Name, Market_Area_Code__c FROM Market_Area__c WHERE Market_Area_Code__c not in:excludeMA];
        Integer i = 0;
        for(Market_Area__c ma : lstMA){
            testAccounts.add( new Account(Name = 'Test ' + i, Market_Area__c = ma.Id, Approving_Area_Manager__c = null, Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().id));
            i++;    
        }
        insert testAccounts;
        //now turn into a map
        Map<Id, Account> mapTestAcc = new Map<Id, Account>();
        for(Account acc:testAccounts){
            mapTestAcc.put(acc.Id, acc);
        }
        
        //Now call the method
        Test.startTest();
        
            mapTestAcc = AccountMarketAreaToolkit.setAccountMarketAreaManagers(mapTestAcc);
            
            System.Debug('** Testing Result ** - mapTestAcc -> ' + mapTestAcc);
        
        Test.stopTest();
    }  
}