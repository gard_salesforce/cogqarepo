/*************************************************************************************************
    Author      : Martin Gardner
    Company     : cDecisions
    Date Created: 13/09/2013
    Description : This class manages the creation test data for use in test cases
    Modified    :   
***************************************************************************************************/
@IsTest
public class SetupTestData {
    
    public class TestException extends Exception{}

    public static Opportunity[] testOpps;
    public static OpportunityLineItem[] oppLineItems;
    public static Set<Id> oppLineIds;
    public static Set<Id> oppIds;
    public static String currencyISOCode = 'USD';
    public static String dwhIdPrefix  ='Test';
    
    public static List<Account> testAcc;
    public static List<Id> accIds;
    public static Integer numAccts=10;
    
    public static List<Contact> testCon;
    public static Map<Id, Contact> testConMap;
    public static List<Id> conIds;
    public static Integer numCons=10;
    
    public static List<Market_Area__c> testMA;
    public static List<Id> maIds;
    public static List<String> setMA;
    public static Integer numMarketAreas = 5;
    
    public static void setupMarketAreas(){
        SetupTestData.testMA = new List<Market_Area__c>();
        
        for(Integer i=0;i<SetupTestData.numMarketAreas;i++){
            SetupTestData.testMA.add(new Market_Area__c(Name = 'Market Area '+ i, Market_Area_Code__c = 'MA ' + i));
        }
        
        insert SetupTestData.testMA;
        
        //run through the inserted records to get the ids and names
        SetupTestData.maIds = new List<Id>();
        SetupTestData.setMA = new List<String>();
        for(Market_Area__c anMA:SetupTestData.testMA){
            SetupTestData.maIds.add(anMA.Id);
            SetupTestData.setMA.add(anMA.Name);
        }
    }
    
    public static void setupAccountData(){
        SetupTestData.testAcc = new List<Account>();
        SetupTestData.setupMarketAreas();
        
        //Disable Governance approval process
        DQ__c testSetting = DataQualitySupport.DisableRoleGovernance(UserInfo.getUserId());
        System.Debug('Settings Governance Disabled = ' + testSetting);
        
        for(Integer i=0;i<SetupTestData.numAccts;i++){
            Integer maIdx = Math.mod(i,SetupTestData.numMarketAreas);
            
            SetupTestData.testAcc.add(new Account(  Name = 'Test Account ' + i,
                                                    BillingCity = 'Bristol',
                                                    BillingCountry = 'United Kingdom',
                                                    BillingPostalCode = 'BS1 1AD',
                                                    BillingState = 'Avon' ,
                                                    BillingStreet = '1 Elmgrove Road',
                                                    Market_Area__c =  SetupTestData.maIds[maIdx],
                                                    Company_Role__c = 'Broker;Correspondent;Client;Other', 
                                                    Area_Manager__c = UserInfo.getUserId()
                                    ));
            
        }
        
        insert SetupTestData.testAcc;
        SetupTestData.accIds = new List<Id>();
        //populate the set of account ids
        for(Account anAcc:SetupTestData.testAcc){
            SetupTestData.accIds.add(anAcc.Id);
        }
                
    }
    
    public static void setupContactData(){
        SetupTestData.testCon  = new List<Contact>();
        
        //build a list of record type ids
        
        List<Id> RecIds = new List<Id>();
        for(RecordType aRecType:ContactToolkit.ContactRecordTypes.values()){
            RecIds.add(aRecType.Id);
        }
        Integer numRecordTypes = RecIds.size();
        
        for(Integer i=0;i<SetupTestData.numCons;i++){
            Integer recIdx = Math.mod(i,numRecordTypes);
            Integer accIdx = Math.mod(i,SetupTestData.numAccts);
            SetupTestData.testCon.add(new Contact(  FirstName='Test',
                                                    LastName = 'Contact ' + i,
                                                    RecordTypeId = RecIds[recIdx],
                                                    //If the record type is normal use the client type otherwise 
                                                    //use the record type name which we expect will be Correspondent
                                                    Type__c = ContactToolkit.ContactRecordTypes.get(RecIds[recIdx]).Name == 'Normal' ? 'Client' : ContactToolkit.ContactRecordTypes.get(RecIds[recIdx]).Name,
                                                    AccountId = SetupTestData.accIds[accIdx]));
        }
        
        
    }
    
    public static void insertContactData(){
        if(SetupTestData.testCon != null){
            insert SetupTestData.testCon;
            SetupTestData.conIds = new List<Id>();
            SetupTestData.testConMap = new Map<Id, Contact>();
            for(Contact aCon:SetupTestData.testCon){
                SetupTestData.conIds.add(aCon.Id);
                SetupTestData.testConMap.put(aCon.Id, aCon);
            }       
        }
    }

    public static void setupTestData()
    {
        
        Pricebook2 standardPb = [SELECT Id FROM Pricebook2 WHERE isStandard=true];
        System.Assert(standardPb != null);
        
        Product2[] products = new Product2[]{
            new Product2(Name='testProd1', Description='Test Product One', Product_Type__c = 'Marine'),
            new Product2(Name='testProd2', Description='Test Product Two', Product_Type__c = 'Energy'),
            new Product2(Name='testProd3', Description='Test Product Three', Product_Type__c = 'Energy;Marine'),
            new Product2(Name='testProd3', Description='Test Product Four', Product_Type__c = 'Energy;Marine')
        };
        insert products;
        
        PricebookEntry[] standardPrices = new PricebookEntry[]{
            new PricebookEntry(Pricebook2Id = standardPb.Id, Product2Id = products[0].Id, UnitPrice=1, isActive=true),
            new PricebookEntry(Pricebook2Id = standardPb.Id, Product2Id = products[1].Id, UnitPrice=1, isActive=true),
            new PricebookEntry(Pricebook2Id = standardPb.Id, Product2Id = products[2].Id, UnitPrice=1, isActive=true)
        };
        insert standardPrices;
    
        RecordType oppMarineRecType = [SELECT Id FROM RecordType WHERE SObjectType='Opportunity' AND Name='Marine'];
        RecordType oppEnergyRecType = [SELECT Id FROM RecordType WHERE SObjectType='Opportunity' AND Name='Energy'];
        
        
        testOpps = new Opportunity[]{
            new Opportunity(Name='testOpp1', RecordTypeId=oppMarineRecType.Id, StageName='Budget Set', CloseDate=Date.Today()+7, CurrencyISOCode = currencyISOCode, DWH_Opportunity_Id__c = dwhIdPrefix +'-1'),
            new Opportunity(Name='testOpp2', RecordTypeId=oppEnergyRecType.Id, StageName='Budget Set', CloseDate=Date.Today()+7, CurrencyISOCode = currencyISOCode, DWH_Opportunity_Id__c = dwhIdPrefix + '2')
        };
        insert testOpps;
        
        oppIds = new Set<Id>();
        for(Opportunity anOpp: testOpps){
            oppIds.add(anOpp.Id);
        }
        
        oppLineItems = new OpportunityLineItem[]{
            new OpportunityLineItem(OpportunityId           = testOpps[1].Id, 
                                    PriceBookEntryId        = standardPrices[1].Id, 
                                    Line_Size__c            = 100,
                                    Actual_Premium__c       = 100,
                                    Budget_Comments__c      = 'Testing',
                                    Record_Adjustment__c    = 1,
                                    General_Increase__c     = 1,
                                    Volume_Adjustment__c    = 1,
                                    LR__c                   = 1,
                                    Renewal_Premium__c      = 100,
                                    RA__c                   = 1,
                                    Renewable_Probability__c= 100
                                    )
        };
        insert oppLineItems;
        
        //retreive the opp line items to get all needed fields
        oppLineIds = new Set<Id>();
        for(OpportunityLineItem anOLI: oppLineItems){
            oppLineIds.add(anOLI.Id);
        }
    
    }
    
}