@isTest
private class TestViewDocCtrlHelper{
    @isTest
    private static void testViewDocCtrl(){
        GardTestData gtdInstance = new GardTestData();
        gtdInstance.commonrecord();
        String clientCompanyId=[select company_id__c from account where id=:gardTestData.clientAcc.id].company_id__c;
        String BrokerCompanyId=[select company_id__c from account where id=:gardTestData.brokerAcc.id].company_id__c;
        gtdInstance.customsettings_rec();
        gtdInstance.ViewDoc_testrecord(); 
        GardTestData.junc_object_broker.Broker__c = GardTestData.brokerAcc.ID;
        
        update GardTestData.junc_object_broker;
        
        GardTestData.brokerDoc.Document_Metadata_External_ID__c = '2asas123';
        update GardTestData.brokerDoc;
        Test.startTest();
        Map<String,List<Contract>>agreementMap=ViewDocCtrlHelper.getClientwithMultipleBroker(clientCompanyId,BrokerCompanyId,new  List<String>{'2019'},new List<String> {'P&I','Marine'});
        HttpResponse res = new HttpResponse();
        
        res.setHeader('Content-Type', 'application/json');
        
        res.setBody('{ "Result": true,  "MoreResultsAvailable": false,  "Count": 1,  "ObjectType": 0,  "ObjectTypeAlias": [],'
                    + '"ResultObjects": [{"ObjectID": 2196681, "ExternalID": "2196681", "Name": "248000 - JUMBO JAVELIN (SCC)",'
                    +'"ObjectProperties": { "pd_partner": "Fagioli Spa - Marghera - 48187", "pd_years": "2019", "pd_policydocumenttype": "SCCF Comprehensive Charterer\'s cover - certificate of entry",'
                    +'"documentClass": "Policy document", "name": "248000 - JUMBO JAVELIN (SCC)","pd_creator": "Lin Anett Frøshaug", "lastModified": "15.05.2020 04.52",'
                    +'"pd_validfrom": "04.03.2019 00.00","pd_validto": "31.03.2019 00.00","pd_objectnameondocument": "JUMBO JAVELIN"}}]}');
        
        DocumentResponseWrapper docResponse= (DocumentResponseWrapper) System.JSON.deserialize(res.getBody(), DocumentResponseWrapper.class);
        ViewDocCtrlHelper.GetAgreementDetailsForClientandBroker(docResponse,agreementMap,BrokerCompanyId,Label.Broker,clientCompanyId);
        test.stopTest();
    }
}