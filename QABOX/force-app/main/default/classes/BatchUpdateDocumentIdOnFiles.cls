/*****************************************************************************************************
* Class Name: BatchUpdateDocumentIdOnFiles
* Created By: Nagarjuna Kaipu
* Date: 12-02-2021
* Description: SF-5422 - Used to retrieve Document Id from MFiels and update on Files
*******************************************************************************************************
*  Version		Developer			Date			Description
=======================================================================================================
*  1.0          Nagarjuna Kaipu		12/02/2021  	Created
*  1.1          Nagarjuna Kaipu     25/05/2021      SF-6601 
*  1.2          Nagarjuna Kaipu     25/05/2021      SF-6604 
********************************************************************************************************/

global class BatchUpdateDocumentIdOnFiles Implements Database.Batchable <sObject>, Database.Stateful,Database.AllowsCallouts{
    public String authToken;
    public Map<Id, String> fileCorelIdMap;
    public Set<Id> docIds;
    public String jobName; //SF-6601  added
    
    public BatchUpdateDocumentIdOnFiles(String authToken, Map<Id, String> fileCorelIdMap, String jobName){ //SF-6601 modified
        this.authToken = authToken;
        this.fileCorelIdMap = fileCorelIdMap;
        this.docIds = fileCorelIdMap.keySet();
        this.jobName = jobName; //SF-6601 added
        system.debug('authToken is: '+authToken);
        system.debug('fileCorelIdMap is: '+fileCorelIdMap);
    }
    global Database.QueryLocator start(Database.BatchableContext bc){
        system.debug('Inside Start Method');
        system.debug('doc ids: '+ docIds);
        system.debug('fileCorelIdMap is: '+ fileCorelIdMap);
        String soqlQuery = 'Select Id, Description From ContentVersion Where Id IN: docIds';
        system.debug('soqlQuery is: '+soqlQuery);
        return Database.getQueryLocator(soqlQuery);
    }
    
    global void execute(Database.BatchableContext bc, List<ContentVersion> scope){
        try{
            system.debug('Inside execute doc list is: '+scope);
            for(ContentVersion cd: scope){
                String docId = retreiveDocumentId(fileCorelIdMap.get(cd.Id));
                System.debug('docId in execute is: '+docId);
                if(String.isNOTBlank(docId)){
                    String preDescription = cd.Description;
                    if(String.isNotBlank(preDescription)){
                        if(preDescription.startsWith('MFiles')){
                            String oldDocId = preDescription.substringBetween('MFilesDocumentId(',')');
                            if(String.isNotBlank(oldDocId)){
                                String newDescription = preDescription.replace(oldDocId, docId);
                                cd.description = newDescription;
                            }
                            else{
                                cd.description = 'MFilesDocumentId(' + docId + ')'; 
                            }
                        }
                        else{
                            cd.description = 'MFilesDocumentId(' + docId + ') ' +cd.description; 
                        }
                    }
                    else{
                        cd.description = 'MFilesDocumentId(' + docId + ')';
                    }
                }
            }
            Database.update(scope);
        }
        catch(Exception ex){ //SF-6604 added
            Exception_Log__c errLog = new Exception_Log__c(source__c = 'BatchUpdateDocumentIdOnFiles:execute', Type_Name__c = 'MFiles Exception: ' + ex.getTypeName(), 
                                                           exception_Message__c = ex.getMessage(), Stack_Trace__c = ex.getStackTraceString());
            insert errLog;
        }
    }
    
    global void finish(Database.BatchableContext bc){
        //Abort job SF-6601 added
        for(CronTrigger ct: [SELECT Id, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType, CreatedDate FROM CronTrigger
                             Where CronJobDetail.Name =: jobName ]){
                                 System.abortJob(ct.Id);
                             }
    }
    public String retreiveDocumentId(String correlationId){
        Mule_Web_Service__c docEndpoint = Mule_Web_Service__c.getInstance('M-Files endpoint for document storing');
        system.debug('correlationId is: '+correlationId);
        system.debug('authToken is: '+authToken);
        FileResponseWrapper fileResponseWrapper = new FileResponseWrapper();
        Http sendCoRelIdReq = new Http();
        Httprequest CoRelIdReq = new Httprequest();
        CoRelIdReq.setheader('Authorization',authToken);
        CoRelIdReq.setMethod('GET');
        CoRelIdReq.setEndpoint(docEndpoint.Endpoint_Url__c+'/correlation/'+correlationId);
        HttpResponse CoRelIdResponse = new HttpResponse();
        if(!test.isRunningTest()){
            CoRelIdResponse = sendCoRelIdReq.send(CoRelIdReq);
            
            system.debug('Status is: '+CoRelIdResponse.getStatus());
            system.debug('File response is : '+CoRelIdResponse.getBody());
            fileResponseWrapper = (FileResponseWrapper)System.JSON.deserialize(CoRelIdResponse.getBody(), FileResponseWrapper.class);
            System.debug('Doc id is: '+fileResponseWrapper.documentId);
        }
        else{
            fileResponseWrapper.status = 'failure';
            fileResponseWrapper.documentId = 'test';
            CoRelIdResponse.setBody('{"Explanation":"Test Error"}');
        }
        
        if(fileResponseWrapper.status == 'failure'){ //SF-6604 added
            String exBody = String.valueOf(CoRelIdResponse.getBody());
            String errMessage = '';
            if(exBody.contains('Explanation')){
                errMessage = exBody.substringBetween('Explanation":','}'); 
                errMessage = errMessage.replace('}', '');
            }
            
            Exception_Log__c errLog = new Exception_Log__c(source__c = 'BatchUpdateDocumentIdOnFiles:retreiveDocumentId', Type_Name__c = 'MFiles Exception: '+fileResponseWrapper.message, 
                                                           exception_Message__c = errMessage, Additional_Details__c = 'correlationId: '+fileResponseWrapper.correlationId);
            insert errLog;
        }
        
        return fileResponseWrapper.documentId;
    }
    public class FileResponseWrapper{
        public String status;
        public String message;
        public String correlationId;
        public String documentId;
    }
}