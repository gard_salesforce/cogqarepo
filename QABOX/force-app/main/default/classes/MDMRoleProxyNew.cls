/**************************************************************************
* Author  : Arpan Muhuri
* Company : Cognizant
* Date    : 05/03/2018
***************************************************************************/
    
/// <summary>
///  This proxy class provides methods to access the MDM Roles Service
/// </summary>
public without sharing class MDMRoleProxyNew extends MDMProxy {

  private Set<String> getMDMRoleFields(String requestName) {
    Set<String> fields = new Set<String>();
    for (MDM_Service_Fields__c field : MDM_Service_Fields__c.getAll().values()) {
      if (requestName.equalsIgnoreCase(field.Request_Name__c) && 'MDM_Role__c'.equalsIgnoreCase(field.Source_Object__c) && 'Field'.equalsIgnoreCase(field.Type__c)) {
        fields.add(field.Field_Name__c.toLowerCase());
      }
    }
    return fields;
  }
  private MDM_Role__c getMDMRoleById(id MDMRoleId, String requestName) {
    return (MDM_Role__c)Database.query(buildSoqlRequest(getMDMRoleFields(requestName), 'MDM_Role__c', MDMRoleId));
    }
    private List<MDM_Role__c> getMDMRolesByIds(List<id> MDMRoleIds, String requestName) {
        return (List<MDM_Role__c>)Database.query(buildSoqlRequest(getMDMRoleFields(requestName), 'MDM_Role__c', MDMRoleIds));
    }
   //added for Role-merge XML request
    private Static List<MDM_Role__c> getMDMRoleSyncByIds(Set<id> MDMRoleIds) {
        return (List<MDM_Role__c>)[SELECT Account__c,Active__c,Inactivated_Date__c,Synchronisation_Action__c,Synchronisation_Status__c,Valid_Role_Combination__c FROM MDM_Role__c WHERE ID IN:MDMRoleIds];
    }
  private DOM.XMLNode MDMUpsertRoles(MDMUpsertRoleRequest request) {
    DOM.XMLNode requestXML = sendRequestMerge(request);
    System.debug('responseString for Roles-->'+requestXML);
    /*
    MDMUpsertRoleResponse response = new MDMUpsertRoleResponse();
    try {
      response.Deserialize(
              responseDoc
                  .getRootElement()
                  .getChildElement(SOAP_Body, SOAPNS)
                  .getChildElement(MDMChangeResponse_Label, MDMChangeResponse_Namespace)
          );
    } catch (Exception ex) {
      throw new MDMDeserializationException(responseDoc, 'Failed to deserialize MDMRoleProxyNew.MDMUpsertRoleResponse', ex);
    }
    */
    return requestXML ;
    }
  
  private DOM.XMLNode MDMDeleteRoles(MDMDeleteRoleRequest request) {
        DOM.XMLNode requestXML = sendRequestMerge(request);
        System.debug('responseString for Roles to delete-->'+requestXML);
        /*
        MDMDeleteRoleResponse response = new MDMDeleteRoleResponse();
        try {
          response.Deserialize(
                  responseDoc
                      .getRootElement()
                      .getChildElement(SOAP_Body, SOAPNS)
                      .getChildElement(MDMChangeResponse_Label, MDMChangeResponse_Namespace)
              );
        } catch (Exception ex) {
          throw new MDMDeserializationException(responseDoc, 'Failed to deserialize MDMRoleProxyNew.MDMDeleteRoleResponse', ex);
        }
        */
    return requestXML;
    }
 
  public DOM.XMLNode MDMUpsertRole(Map<id, MDM_Role__c> upsertRoles) {
    MDMUpsertRoleRequest request = new MDMUpsertRoleRequest();
    Map<id, MDM_Role__c> mdmRoles = new Map<id, MDM_Role__c>(getMDMRolesByIds(new List<id> (upsertRoles.keySet()), request.getName()));
    DOM.XMLNode requestRoleXML;
    try {
      request.MDMRoles = mdmRoles.values();
      requestRoleXML = MDMUpsertRoles(request);
      system.debug('response Roles to be appended--> '+requestRoleXML);
      /*
      for (MDMResponse mdmResponse : response.MDMResponses) {
        System.debug('*** MDMResponse:[ id: + ' + mdmResponse.id + ', success: ' + string.valueOf(mdmResponse.success) + ']');
        if (mdmResponse.Success) {
                  mdmRoles.get(mdmResponse.Id).Synchronisation_Status__c = 'Synchronised';
              } else {
                  mdmRoles.get(mdmResponse.Id).Synchronisation_Status__c = 'Sync Failed';
              }  
      }
      
      update mdmRoles.values(); 
      
        } catch (MDMDeserializationException ex) {
      Logger.LogException('MDMRoleProxyNew.MDMUpsertRoleAsync (@Future, ex)', ex, ex.xmlString);
      if (mdmRoles != null) {
        SetRolesSyncFailed(mdmRoles.values(), 'MDMRoleProxyNew.MDMUpsertRoleAsync (@Future, innerEx)');
      }
      */
        }catch (Exception ex) {
              Logger.LogException('MDMRoleProxyNew.MDMUpsertRoleAsync (@Future, ex)', ex);
              if (mdmRoles != null) {
                  SetRolesSyncFailed(mdmRoles.values(), 'MDMRoleProxyNew.MDMUpsertRoleAsync (@Future, innerEx)');
              }
        }
        return requestRoleXML ;
  }
  
  public DOM.XMLNode MDMDeleteRole(Map<id, MDM_Role__c> deleteRoles) {
    MDMDeleteRoleRequest request = new MDMDeleteRoleRequest();
    Map<id, MDM_Role__c> mdmRoles = new Map<id, MDM_Role__c>(getMDMRolesByIds(new List<id> (deleteRoles.keySet()), request.getName()));
    DOM.XMLNode requestRoleXML;
        try {
            request.MDMRoles = mdmRoles.values();
            requestRoleXML = MDMDeleteRoles(request);
            system.debug('response Roles to be appended--> '+requestRoleXML);
            /*
            //do something with the response...
            for (MDMResponse mdmResponse : response.MDMResponses) {
            System.debug('*** MDMResponse:[ id: + ' + mdmResponse.id + ', success: ' + string.valueOf(mdmResponse.success) + ']');
            if (mdmResponse.Success) {
                  mdmRoles.get(mdmResponse.Id).Synchronisation_Status__c = 'Synchronised';
              } else {
                  mdmRoles.get(mdmResponse.Id).Synchronisation_Status__c = 'Sync Failed';
              }  
            }
      
            update mdmRoles.values(); 
            } catch (MDMDeserializationException ex) {
                Logger.LogException('MDMRoleProxyNew.MDMDeleteRoleAsync (@Future, ex)', ex, ex.xmlString);
            if (mdmRoles != null) {
                SetRolesSyncFailed(mdmRoles.values(), 'MDMRoleProxyNew.MDMDeleteRoleAsync (@Future, innerEx)');
            }
            */
        } catch (Exception ex) {
            Logger.LogException('MDMRoleProxyNew.MDMDeleteRoleAsync (@Future, ex)', ex);
            if (mdmRoles != null) {
                SetRolesSyncFailed(mdmRoles.values(), 'MDMRoleProxyNew.MDMDeleteRoleAsync (@Future, innerEx)');
            }
        }
        return requestRoleXML;
  }
  
  //@future (callout = true)
    public static DOM.XMLNode MDMUpsertRoleAsync(list<id> mdmRoleIds) {
    MDMRoleProxyNew proxy = new MDMRoleProxyNew();
    MDMUpsertRoleRequest request = new MDMUpsertRoleRequest();
    Map<id, MDM_Role__c> mdmRoles = new Map<id, MDM_Role__c>(proxy.getMDMRolesByIds(mdmRoleIds, request.getName()));
    system.debug('MDMRoleProxyNew class.Upsert.mdmRoleIDs -> '+mdmRoleIds+' -mdmroleMap- '+mdmRoles);
    DOM.XMLNode xmlUpsertRoleRequest = proxy.MDMUpsertRole(mdmRoles);
    return xmlUpsertRoleRequest;
  }
  
  //@future (callout = true)
    public static DOM.XMLNode MDMDeleteRoleAsync(list<id> mdmRoleIds) {
    MDMRoleProxyNew proxy = new MDMRoleProxyNew();
    MDMDeleteRoleRequest request = new MDMDeleteRoleRequest();
    Map<id, MDM_Role__c> mdmRoles = new Map<id, MDM_Role__c>(proxy.getMDMRolesByIds(mdmRoleIds, request.getName()));
    system.debug('MDMRoleProxyNew class.Delete.mdmRoleIDs -> '+mdmRoleIds+' -mdmroleMap- '+mdmRoles);
    DOM.XMLNode xmlDeleteRoleRequest = proxy.MDMDeleteRole(mdmRoles);
    return xmlDeleteRoleRequest;
  } 
  
  //Called from an exception to set all sync status to failed
  @TestVisible private static void SetRolesSyncFailed(List<MDM_Role__c> roles, String source) {
    try {
      if (roles != null) {
        for (MDM_Role__c r : roles) {
          r.Synchronisation_Status__c = 'Sync Failed';
        }
        update roles; 
      }
    } catch (Exception ex) {
      //Simple update, hopefully shouldn't get here.
      Logger.LogException(source, ex);
    }
  }
  
//-----------------------Role Call out-Starts -----------------------------------------
 @future (callout = true)
    public static void roleMergeCallOut(String requestString,set<id> roleIdSet){
        Map<id, MDM_Role__c> mdmRoles = new Map<id, MDM_Role__c>(getMDMRoleSyncByIds(roleIdSet));
        system.debug('singleCallOut accounts param-->'+mdmRoles);
        DOM.Document responseDoc = new DOM.Document();
            responseDoc = WebServiceUtil.processRequest(MDMConfig__c.getInstance().Endpoint__c, requestString, null);
            if(responseDoc!=null){
                System.Debug('Response: ' + responseDoc.toXMLString());
            }
            MDMMergeRoleResponse response = new MDMMergeRoleResponse();
            try {
                response.Deserialize(
                    responseDoc
                        .getRootElement()
                        .getChildElement('Body', MDMConfig__c.getInstance().SOAPNS__c)
                        .getChildElement('mdmChangeResponse', 'http://www.gard.no/mdm/v1_0/mdmpartner')
                    );
                } catch (Exception ex) {
                    throw new MDMDeserializationException(responseDoc, 'Failed to deserialize MDMPartnerProxyNew.MDMMergeRoleResponse', ex);
            }
            try{
                for (MDMResponse mdmResponse : response.MDMResponses) {
                    System.debug('*** MDMResponse:[ id: + ' + mdmResponse.id + ', success: ' + string.valueOf(mdmResponse.success) + ']');
                    if (mdmResponse.Success) {
                    //added for updating account fields
                        system.debug('Role successful..!! -acc Id- !!'+mdmResponse.Id);
                        for(MDM_Role__c mdm:mdmRoles.values()){
                            mdm.Synchronisation_Status__c = 'Synchronised';
                        }
                    } else {
                        for(MDM_Role__c mdm:mdmRoles.values()){
                            mdm.Synchronisation_Status__c = 'Sync Failed';
                        }
                    }
                }
            }
            catch(Exception ex){
                System.debug('Exception from singleCallOut'+ex.getMessage());
            }
            system.debug('Finally updated..');
            update mdmRoles.values(); 
            
    }
//-----------------------Role Call out-Ends--------------------------------------------

/// ----------------------------------------------------------------------------
///  -- Sub classes for the request and response messages called above
/// ----------------------------------------------------------------------------
  public virtual class MDMRoleRequest extends MDMRequest {
        //TODO: move to custom setting...
    protected String MDMChangeRolesRequest_Label {
      get {
        return 'changeRolesRequest';
      }
    }
    protected String MDMChangeRolesRequest_Namespace {
      get {
        return 'http://www.gard.no/mdm/v1_0/mdmmessage';
      }
    }
    protected String MDMChangeRolesRequest_Prefix {
      get {
        return 'mdm';
      }
    }
    
    public virtual override String getName() { return 'changeRolesRequest'; }
    public List<MDM_Role__c> MDMRoles { get; set; }
    
        public virtual override DOM.Document getSoapBody() {
            return getSoapBody(this.getName(), MDMChangeRolesRequest_Namespace, MDMChangeRolesRequest_Prefix);
        }
    
        public DOM.Document getSoapBody(string method, String namespace, String prefix) {
      system.debug('*** method: ' + method);
            DOM.Document body = new DOM.Document();
      DOM.XMLNode root = body.createRootElement(MDMChangeRolesRequest_Label, MDMChangeRolesRequest_Namespace, MDMChangeRolesRequest_Prefix);    

      //Get XML element list from Custom Setting
      List<MDM_Service_Fields__c> serviceFields = [
        SELECT Name, Request_Name__c, Sequence__c, Namespace_Prefix__c, Parent_Node__c, Type__c, XmlLabel__c, Source_Object__c, Field_Name__c, Address_Type__c, Omit_Node_If_Null__c, Strip_Special_Characters__c, Strip_Whitespaces__c
        FROM MDM_Service_Fields__c 
        WHERE Request_Name__c = :method
        ORDER BY Sequence__c];
      
            for(MDM_Role__c r : this.MDMRoles) {
        DOM.XmlNode contactXml = root.addChildElement(method, namespace, prefix);
        appendSObjectXml(contactXml, r, serviceFields);
            }
            return body;
        }
    }
    
  public class MDMUpsertRoleRequest extends MDMRoleRequest {
    public override String getName() { return 'upsertRoleRequest'; }
    }
  //Response
  public virtual class MDMChangeResponse { 
    public List<MDMResponse> mdmResponses { get; set; }
    
    public MDMChangeResponse() {
      mdmResponses = new List<MDMResponse>();
    }
    
    protected void Deserialize(Dom.XmlNode root, String parentNode) {
      try {
        for  (Dom.XmlNode node : root.getChildElements()) {
          try {
            XmlSerializer s = new XmlSerializer();
                  object o = s.Deserialize(node, 'MDMRoleProxyNew.MDMResponse');
                  mdmResponses.add((MDMResponse)o);
          } catch (Exception ex) {
            //TODO: handle exceptions better...
            //Don't let one failure failt he whole batch...?
          }
        }
      } catch (Exception ex) {
        //TODO: handle exceptions better...
        throw ex;
      }
    }
  }
  
  public class MDMResponse implements XmlSerializable {
    public Boolean Success { get; set; }
    public String Id { get; set; }
    public String Error { get; set; }
    
    public object get(string fieldName) {
          Map<string, object> thisobjectmap = new Map<string, object> 
          { 
              'id' => this.Id, 
              'success' => this.Success,
        'error' => this.Error
          };
          return thisobjectmap.get(fieldname);
      }
      
      public boolean put(string fieldName, object value) {
          if (fieldName == 'id') {
              this.Id = String.valueOf(value);
          } else if (fieldName == 'success') {
              this.Success = Boolean.valueOf(value);  
      } else if (fieldName == 'error') {
              this.Error = String.valueOf(value);
          } else {
              return false;
          }
          return true;
      }
      
      public Set<string> getFields() {
          Set<string> fields = new Set<string> 
          { 
              'success',
        'id',
        'error'
          };
          
          return fields;
      }
  }
  
  public class MDMDeleteRoleRequest extends MDMRoleRequest {
    public override String getName() { return 'deleteRoleRequest'; }
    }
  public class MDMMergeRoleResponse extends MDMChangeResponse {
    public void Deserialize(Dom.XmlNode root) {
      Deserialize(root, 'MDMRoleProxyNew.MDMChangeResponse');
    }
  }
  
  /*
   public class MDMUpsertRoleResponse extends MDMChangeResponse {
    public void Deserialize(Dom.XmlNode root) {
      Deserialize(root, 'MDMRoleProxyNew.MDMChangeResponse');
    }
  }
  public class MDMDeleteRoleResponse extends MDMChangeResponse {
    public void Deserialize(Dom.XmlNode root) {
      Deserialize(root, 'MDMRoleProxyNew.MDMChangeResponse');
    }
  }
  */
}