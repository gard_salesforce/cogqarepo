@isTest
Class TestMyObjectDetailsAllCoversCtrl1
{ 
    public static testmethod void cover()
    { 
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        GardTestData gtdInstance = new GardTestData();
        gtdInstance.commonRecord();
        GardTestData.test_object_2nd.guid__c = '8ce8ac89-a6fd-1836-9e07';
        update GardTestData.test_object_2nd; 
        system.assertEquals( GardTestData.test_object_2nd.guid__c , '8ce8ac89-a6fd-1836-9e07');
        gtdInstance.customsettings_rec();
        System.runAs(GardTestData.clientUser)
        {
            test.StartTest();
            ApexPages.StandardController scontroller = new ApexPages.StandardController(GardTestData.clientAcc);
            Pagereference pge_1 = page.MyObjectDetailsAllCovers;
            pge_1.getparameters().put('objid',GardTestData.test_object_2nd.guid__c);
            test.setcurrentpage(pge_1);
            MyObjectDetailsAllCoversCtrl MyObject=new MyObjectDetailsAllCoversCtrl(scontroller);
            MyObjectDetailsAllCoversCtrl MyObjects=new MyObjectDetailsAllCoversCtrl();
            MyObject.obj = new Object__c();
            MyObject.obj.id = GardTestData.test_object_2nd.id; 
            MyObject.caseTestId = GardTestData.brokerCase.id;
            MyObject.isAuthorized = true;
            MyObject.underwrId='abc#xyz';
            MyObject.jointSortingParam='ASC##Product_Name__c';
            MyObject.underwriterPopup();
            MyObject.ViewData();
            MyObject.setpageNumber();
            MyObject.navigate();
            MyObject.first();
            MyObject.next();
            MyObject.last();
            MyObject.previous();
            MyObject.sendMailforEditObject();
            System.assertEquals(MyObject.isAuthorized , true);
            system.debug('**************gtdInstance.obj_1st.id'+GardTestData.test_object_2nd.id);
            MyObject.obj = new object__c();
            MyObject.obj.id = GardTestData.test_object_2nd.id;
            MyObject.isAuthorized = true;
            MyObject.fetchFavourites();
            test.stopTest();
        }
    }
}