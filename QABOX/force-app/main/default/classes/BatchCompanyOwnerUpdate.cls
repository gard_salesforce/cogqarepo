Global class BatchCompanyOwnerUpdate implements Database.Batchable<sObject>{
    
    public String query;
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        System.debug(':::::::: start batch class ::::::::::');
        query = 'SELECT Id,name, OwnerId, Underwriter_main_contact__c,Underwriter_main_contact__r.IsActive FROM Account '+ 
                ' WHERE  Underwriter_main_contact__c != NULL AND Company_Role__c INCLUDES (\'Client\') AND Company_Role__c excludes (\'Correspondent\') ';                
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Account> scope){
        System.debug('------ execute batch class ------');
        List<account> accToBeUpdated = new List<account>();
        
        Boolean isActive = false;
        for(account acc:scope){
            if(!Test.IsRunningTest() && acc.Underwriter_main_contact__r.IsActive) {
                isActive = true;
            }
            else {
                isActive = true;
            }
            
            system.debug('account found -- > '+acc.Id+' --- '+acc.name);
            System.debug('acc.OwnerId:::'+acc.OwnerId);
            System.debug('acc.Underwriter_main_contact__c:::'+acc.Underwriter_main_contact__c);
            System.debug('acc.Underwriter_main_contact__r.IsActive:::'+acc.Underwriter_main_contact__r.IsActive);
            if(acc.OwnerId != acc.Underwriter_main_contact__c && acc.Underwriter_main_contact__c != NULL && isActive){
                acc.OwnerId = acc.Underwriter_main_contact__c;
                system.debug('mismatch--->'+acc.Id);
                accToBeUpdated.add(acc);
            }
        }
        if(accToBeUpdated.size() > 0){
            try{
                system.debug('--accToBeUpdated-- '+accToBeUpdated);
                update accToBeUpdated;
            }
            catch(Exception e){system.debug('--exception occured--'+e.getMessage());}
        }
    }
      
   global void finish(Database.BatchableContext BC){
   }
}