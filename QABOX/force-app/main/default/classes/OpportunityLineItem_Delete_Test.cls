/*************************************************************************************************
    Author      : Martin Gardner
    Company     : cDecisions
    Date Created: 13/09/2013
    Description : This class tests that the deletion trigger on opportunity fires correctly
    Modified    :   
***************************************************************************************************/
@isTest (seeAllData = true) 
private class OpportunityLineItem_Delete_Test {

	static testMethod void TestOpportunityLineDelete() {
        //Setup the test data
		SetupTestData.setupTestData();

		Test.startTest();   	  
	    	//Attempt a delete
	       	delete SetupTestData.oppLineItems;
		Test.stopTest();   
		
		//check for the Deleted_Record__c records
		List<Deleted_Record__c> delrecs =  new List<Deleted_Record__c>();
		delrecs = [SELECT Id, RecordId__c, Object__c, UnDeleted__c FROM Deleted_Record__c WHERE RecordId__c in: SetupTestData.oppLineIds];
		
		System.assert(delrecs.Size()>0, '*** No records found in Deleted_Record__c for oppLineItems ' + SetupTestData.oppLineItems);
		if(delrecs.Size()>0){
			System.Debug('*** delrecs = ' + delrecs);
		}
		
		System.assert(delrecs.Size() == SetupTestData.oppLineItems.Size(), '*** Discrepency between the number of records in delrecs vs oppLineItems. delrecs.Size() = ' + delrecs.Size() + ' SetupTestData.oppLineItems.Size() = ' +  SetupTestData.oppLineItems.Size());
			    
	       
    }
}