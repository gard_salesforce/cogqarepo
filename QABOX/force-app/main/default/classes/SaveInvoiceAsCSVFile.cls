//Created for trial of SF-6152
//TestClass : TestSaveInvoiceAsCSVFile
/*
*    Purpose: This class will save the exam details report of a peme invoice as a CSV file as a related record of that invoice.
*/
public class SaveInvoiceAsCSVFile{
    
    private static String xmlHeader = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' + '\n' + '<data-set xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
    private static String xmlFooter = '\n' + '</data-set>';
    private static String recordTagOpen = '\n' + '<record>';
    private static String recordTagClose = '\n' + '</record>';
    private static List<String> headers = new List<String>();
    private static Map<Integer,String> indexedHeaderTagsOpen;
    private static Map<Integer,String> indexedHeaderTagsClose;
    
    public static Id saveInvoice(Id pemeInvoiceId){//created for SF-5942, creates a report of related PEME Exam Details and attach them to the parent PEME Invoice as .csv file
        Id retCvId;
        try{
            PEME_Invoice__c pemeInvoice = [SELECT id,name,Clinic__c,Clinic__r.name,Period_Covered_From__c,Period_Covered_To__c,PEME_reference_No__c FROM PEME_Invoice__c WHERE Id = :pemeInvoiceId];
            Report rep = [SELECT id,developerName,description,name,format,folderName FROM Report WHERE developerName = 'PEME_Invoice_Report'];
            Reports.ReportDescribeResult rdr = Reports.ReportManager.describeReport(rep.id);
            
            Reports.ReportMetadata rMdt = rdr.getReportMetadata();
            Reports.ReportExtendedMetadata rEMdt = rdr.getReportExtendedMetadata();
            Map<String,Reports.DetailColumn> detailColumnMap = rEMdt.getDetailColumnInfo();
            
            Map<String,Reports.AggregateColumn> aggregateColumnMap = rEMdt.getAggregateColumnInfo();
            String summariedField = 'Amount';
            Integer summariedFieldIndex;
            Decimal summariedFieldValue = 0.0;
            
            //added for SF-6152
            String xlsMainString = xmlHeader;

            List<String> reportRows = new List<String>();
            String headerRow;
            Integer headerIndex = 0;
            for(String mapKey : detailColumnMap.keySet()){
                String headerRowPart = detailColumnMap.get(mapKey).getLabel();
                headerRow = (headerRow == null) ? headerRowPart : headerRow+','+headerRowPart;
                
                if(summariedField == headerRowPart) summariedFieldIndex = headerIndex;
                headerIndex++;
            }
            
            //SF-6152
            headers.addAll(headerRow.split(','));
            createIndexedHeaderTagMaps(headers);
            
            reportRows.add(headerRow);
            List<Reports.ReportFilter> rfs = rMdt.getReportFilters();
            for(Reports.ReportFilter rf : rfs){
                if(rf.getColumn().equalsIgnoreCase('PEME_Invoice__c.Name')){
                    rf.setValue(pemeInvoice.name);
                    break;
                }
            }
            Reports.ReportResults rr = Reports.ReportManager.runReport(rep.Id,rMdt,true);
            
            Map<String,Reports.ReportFact> factMap = rr.getFactMap();
            
            for(String factKey : factMap.keySet()){
                Reports.ReportFactWithDetails rfwd = (Reports.ReportFactWithDetails) factMap.get(factKey);
                for(Reports.ReportDetailRow rDRow : rfwd.getRows()){
                    String dataRow;
                    Integer dataIndex = 0;
                    xlsMainString += recordTagOpen;
                    for(Reports.ReportDataCell rdc : rDRow.getDataCells()){
                        String dataRowPart = rdc.getLabel() != null ? rdc.getLabel().escapeXml() : '';
                        dataRow = dataRow == null ? dataRowPart : dataRow+','+dataRowPart;
                        
                        //added SF-6152
                        xlsMainString += indexedHeaderTagsOpen.get(dataIndex);
                        xlsMainString += dataRowPart;
                        xlsMainString += indexedHeaderTagsClose.get(dataIndex);
                        
                        if(dataIndex == summariedFieldIndex){
                            try{
                                Decimal amountValue = Decimal.valueOf(dataRowPart.unescapeXML().remove(' ').escapeHtml4().remove('&nbsp;').replaceAll(',','.').substringAfter('PHP'));
                                summariedFieldValue = amountValue + summariedFieldValue;
                               }catch(Exception ex){}
                            
                        }
                        dataIndex++;
                    }
                    xlsMainString += recordTagClose;
                    reportRows.add(dataRow);
                }
            }
            String csvString = String.join(reportRows,'\n');
            
            //SF-6152
            xlsMainString = insertBlankRow(xlsMainString,new Map<Integer,String>{summariedFieldIndex => (''+summariedFieldValue)});
            xlsMainString += xmlFooter;
            
            ContentVersion cv =  new ContentVersion(
                description = 'Report data',
                ContentLocation = 'S', //S specify this document is in SF, use E for external files
                //PathOnClient = (''+pemeInvoice.Clinic__r.name+' '+pemeInvoice.Period_Covered_From__c+' - '+pemeInvoice.Period_Covered_To__c+' - '+pemeInvoice.PEME_reference_No__c+'.xml'), //+ att.attIns.FileExtension;
                //Title = (''+pemeInvoice.Clinic__r.name+' '+pemeInvoice.Period_Covered_From__c+' - '+pemeInvoice.Period_Covered_To__c+' - '+pemeInvoice.PEME_reference_No__c+'.csv'),//'Report.csv';
                //VersionData = Blob.valueOf(csvString),
                PathOnClient = (''+pemeInvoice.Clinic__r.name+' '+pemeInvoice.Period_Covered_From__c+' - '+pemeInvoice.Period_Covered_To__c+' - '+pemeInvoice.PEME_reference_No__c+'.xls'), //+ att.attIns.FileExtension;
                Title = (''+pemeInvoice.Clinic__r.name+' '+pemeInvoice.Period_Covered_From__c+' - '+pemeInvoice.Period_Covered_To__c+' - '+pemeInvoice.PEME_reference_No__c+'.xls'),//'Report.csv';
                VersionData = Blob.valueOf(xlsMainString),
                NetworkId = '0DBD0000000TNzPOAW'
            );
            insert cv;
            
            //link and give access to the accessors of thisPemeInvoice's to this file
            Id conDocId = [SELECT id,ContentDocumentId FROM contentVersion WHERE Id = :cv.id].ContentDocumentId;
            ContentDocumentLink cdl = new ContentDocumentLink(
                ContentDocumentId = conDocId,
                LinkedEntityId = pemeInvoiceId,//'a2jD0000001Qzct'; // parent id assigned
                ShareType = 'I', // Inferred permission, checkout description of ContentDocumentLink object for more details
                Visibility = 'AllUsers'
            );
            insert cdl;
            retCvId = cv.Id;
        }catch(Exception ex){}
        return retCvId;
    }
    
    private static void createIndexedHeaderTagMaps(List<String> headers){
        List<Map<Integer,String>> indexedHeaderTagMaps = new List<Map<Integer,String>>();
        indexedHeaderTagsOpen = new Map<Integer,String>();
        indexedHeaderTagsClose = new Map<Integer,String>();
        
        String spaceString = ' ';
        for(Integer index = 0 ; index < headers.size() ; index++){
            String thisTag = headers[index].replaceAll(spaceString,'');
            indexedHeaderTagsOpen.put(index,'<' + thisTag + '>');
            indexedHeaderTagsClose.put(index,'</' + thisTag + '>');
        }
    }
    
    private static String insertBlankRow(String xmlMainString,Map<Integer,String> summariedFieldIndexToValue){
        xmlMainString += recordTagOpen;
        for(Integer index = 0; index < indexedHeaderTagsOpen.size()-1 ; index++){
            String formattedTotal = getFormattedTotal(summariedFieldIndexToValue.get(index));
            xmlMainString += (indexedHeaderTagsOpen.get(index) + formattedTotal + indexedHeaderTagsClose.get(index));
        }
        xmlMainString += recordTagClose;
        return xmlMainString;
    }
    
    private static String getFormattedTotal(String totalAmount){
        if(String.isBlank(totalAmount)) return '';
        
        List<String> sourceArray = totalAmount.split('\\.');
        if(sourceArray.size() < 2) return '';
        
        String formattedTotalString = 'PHP ';
        List<String> segmentedArray = new List<String>();
        
        if(!String.isBlank(sourceArray[0])){
            Integer interationCounter = 0;
            for(Integer index = sourceArray[0].length()-1; index >= 0 ; index--){
                interationCounter++;
                segmentedArray.add(
                    String.fromCharArray(
                        new List<Integer>{sourceArray[0].charAt(index)}
                    )
                );
                
                if(interationCounter == 3){
                    segmentedArray.add(' ');
                    interationCounter = 0;
                }
            }
            for(Integer index = (segmentedArray.size()-1);index >= 0;index--) formattedTotalString += segmentedArray[index];
        }else{
            formattedTotalString += '0';
        }
        
        if(String.isBlank(sourceArray[1])) formattedTotalString += ',00';
        else if(sourceArray[1].length() < 2) formattedTotalString += (',' + sourceArray[1] + '0');
        else formattedTotalString += (',' + sourceArray[1]);
        
        return formattedTotalString;
    }
}