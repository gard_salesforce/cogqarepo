@isTest
public class TestCloseWonOpportunity_lightning{
    public static testMethod void CloseWonOpportunity_lightningTestMethod(){
        GardTestData testRecs = new GardTestData();
        testRecs.commonRecord();
        test.startTest();
        //Id RecordTypeIdOpportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('P_I').getRecordTypeId();
        Opportunity opportunity = new Opportunity();
        //opportunity.RecordTypeId = RecordTypeIdOpportunity;
        opportunity.Type = 'New Business';
        opportunity.Name = 'Test opportunity';
        opportunity.AccountId = GardTestData.clientAcc.Id;
        opportunity.StageName = 'Quote';
        opportunity.CloseDate = Date.today();
        opportunity.Business_Type__c = 'Ship Owners';
        opportunity.Amount = 150000;
        opportunity.Agreement_Market_Area__c = GardTestData.Markt.Id;
        opportunity.Area_Manager__c = GardTestData.salesforceLicUser.id;
        opportunity.Approval_Criteria__c = '';
        opportunity.Senior_Approver__c = null;      
        insert opportunity;
        
        //testOpp = TestDataGenerator.getOpportunity('P&I','Quote','Ship Owners');
        //CloseWonOpportunity_lightning closeWonObj = new CloseWonOpportunity_lightning();
        CloseWonOpportunity_lightning.getOpportunityRecord(opportunity.Id);
        CloseWonOpportunity_lightning.updateOpportunities(opportunity.Id);
        test.stopTest();
    }
}