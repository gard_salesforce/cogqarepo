@isTest
public class TestSearchGard{
    public static string testStr =  'test'; 
    public static Object__c obj;
    public static Gard_Contacts__c gc;
    public static Account brokerAcc2, clinicAcc;
    public static Contact clinicContact;
    public static Case cs;
    public static PEME_Invoice__c pi;
    Public static testmethod void createTestData(){
        System.debug('SearchGard()..........');
        GardTestData test_rec = new GardTestData();
        test_rec.customsettings_rec();
        test_rec.commonrecord();  
        string sfLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id; 
       
        Search_webservice_endpoint__c wb = new Search_webservice_endpoint__c(Endpoint__c = ' https://soa-test.gard.no/MyGard/Search/SearchService', Name = 'Search_SOA_Endpoint');
        insert wb;
        
        gc = new Gard_Contacts__c(FirstName__c ='test1',lastName__c = 'test1');
        insert gc;
        
        CommonVariables__c cv = new CommonVariables__c(Value__c = '/mygard/profilephoto/005/T' , Name='DefaultProfilePic');
        insert cv;
        
        Market_Area__c Markt2= new Market_Area__c(Market_Area_Code__c = 'abc120');
        insert Markt2;
        Test.startTest();
        brokerAcc2 = new Account(   Name='testName',
                                    BillingCity = 'Southampton1',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS20 AD!',
                                    BillingState = 'Test Avon LAng' ,
                                    BillingStreet = '3 Mangrove Road',
                                    recordTypeId=System.Label.Broker_Contact_Record_Type,
                                    Site = '_www.IBM.se',
                                    Type = 'prospect',
                                    company_Role__c = 'broker',
                                    //Company_ID__c = '64143',
                                    guid__c = '8ce8ad89-a6ed-1836-9e18',
                                    Market_Area__c = Markt2.id,
                                    Product_Area_UWR_2__c='P&I',
                                    Product_Area_UWR_4__c='P&I',
                                    Product_Area_UWR_3__c='Marine',
                                    Role_Broker__c = true,
                                    Area_Manager__c = GardTestData.salesforceLicUser.id ,
                                    X2nd_UWR__c= GardTestData.salesforceLicUser.id,
                                    Claim_handler_Crew__c = GardTestData.salesforceLicUser.id
                                  );
        insert  brokerAcc2 ;
        
        clinicAcc = new Account( Name='testClinic',
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',
                                    recordTypeId=System.Label.ESP_Contact_Record_Type,
                                    company_Role__c = 'External Service Provider',
                                    Site = '_www.ctsclinic.se',
                                    Type = 'Surveyor' ,
                                    //PEME_Enrollment_Status__c = 'Enrolled',
                                    Area_Manager__c = GardTestData.salesforceLicUser.id,      
                                    Market_Area__c = GardTestData.Markt.id                                                                    
                                 );
        insert clinicAcc;
        
        SelectedClientId_AddBook__c selClient = new SelectedClientId_AddBook__c(
                                              name=GardTestData.BrokerContact.Id,
                                              Value__c = GardTestData.brokerAcc.Id);
        insert selClient ;
         
        Search_Fields__c sf = new Search_Fields__c(
                                                      FieldName__c = 'FirstName__c',
                                                      Name = 'Gard_Cont11'  
                                                  );
        insert sf;
        
        System.assert(selClient != null , true);
        Forms_List__c fl = new Forms_List__c(Type__c = 'Claim forms',User_Type__c = 'Client');
        insert fl;
        Attachment attach=new Attachment();    
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=fl.id;
        insert attach;
        
        obj = new Object__c(name = 'Baooo',
                                      Object_Unique_ID__c = '11');
        insert obj;
        
    
        
        System.runAs(GardTestData.brokerUser) 
        {
            System.debug('System.runAs(GardTestData.brokerUser..........1)');
            PageReference pageRef = Page.Search_Page;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id','testSearch$"*dolr');
            SearchGard sg = new SearchGard();
            SearchGard.Result r = new SearchGard.Result('Test/Url','TestHeader');
            sg.searchText =  testStr ;
            sg.Identifier = obj.id;
            //sg.assetRetriver();
            sg.Identifier = obj.id;
            sg.viewObject();
            sg.contactRetriver();
            
            sg.Identifier = GardTestData.brokerClaim.id;
            //sg.showCaseDetails();
            
            
            sg.Identifier = GardTestData.brokerClaim.id;            
            //sg.GrdContRetriver();
            
            sg.getImageUrl( testStr );
            sg.genretGrdContId();
            sg.exportToExcelForClaimDet();
            sg.IsPeopleLineUser = false;
            sg.doSearch();
            //sg.doSearch();
            sg.doSearchAsset();
            sg.doSearchCase();
            sg.doSearchContact();
            sg.doSearchGardNo();
            System.debug('searchText--->> ' + sg.searchText);
            sg.doSearchObject();
            sg.doSearchgardContact();
            sg.doSearchServiceRequests();
            //sg.GrdContRetriver();
            System.debug('System.runAs(GardTestData.brokerUser..........2)');
            //SearchHelper sh = new SearchHelper();
            SearchHelper.searchBySoql('testSearch$"*'+GardTestData.test_object_1st.name,'PEME_Invoice__c','',false);
            sg.grdContImg = 'imageforGardContact.jpg';
            sg.noRsltFndFrmMyGrd = 'no result found';
            sg.noRsltFndFrmTotalSrch = 'no result found from total search';
            sg.checkForPplClm = true;
            System.debug('Before Gard_Contacts__c..........');
            sg.lstCont = new List<Gard_Contacts__c>();
            sg.lstCont.add(GardTestData.grdobj);
            sg.pointingId = String.valueOf(GardTestData.grdobj.ID);
            sg.claimsHandlerId = String.valueOf(GardTestData.grdobj.ID);
            System.debug('After Gard_Contacts__c..........');
            
            sg.lstGardContact = new List<Gard_Contacts__c>();
            sg.lstGardContact.add(GardTestData.grdobj);   
            sg.claimType = 'open';
            sg.objDetail = new Object__c ();
            sg.objDetail = GardTestData.test_object_1st;
            
            sg.caseData = new Case();
            //sg.caseData = GardTestData.brokerCase;
            //sg.caseData = cs;
            sg.caseDetail = new Case();
            sg.caseDetail = GardTestData.brokerCase;
            sg.caseDetail = cs;
            sg.mapClaimsHandlerDetail = new map<String,String>();
            sg.mapClaimsHandlerDetail.put('test','test1');
        }
              
        System.runAs(GardTestData.clientUser) 
        {
            System.debug('System.runAs(GardTestData.clientUser..........)');
            PageReference pageRef = Page.Search_Page;
            Test.setCurrentPage(pageRef);
            //ApexPages.currentPage().getParameters().put('id','P&I$"*dolr');
            ApexPages.currentPage().getParameters().put('id','$"*dolr'+GardTestData.test_object_1st.name);
            SearchGard sg = new SearchGard();
            SearchGard.Result r = new SearchGard.Result('Test/Url','TestHeader');
            sg.searchText =  testStr ;
            sg.Identifier = obj.id;
            //sg.assetRetriver();
            sg.Identifier = obj.id;
            sg.viewObject();
            sg.contactRetriver();
            sg.Identifier = GardTestData.brokerCase.id;
            //sg.showCaseDetails();
            
            
            //sg.GrdContRetriver();
            //sg.getImageUrl( testStr );
            sg.getImageUrl('abcd.src.abcdef"ghgffgha.amp;');
            sg.genretGrdContId();
            sg.exportToExcelForClaimDet();
            sg.doPEMESearch();
            //commented
            //sg.doSearchAttachment();
            System.debug('System.runAs(GardTestData.clientUser..........)2');
            
            sg.Identifier=GardTestData.pi.id;
            //commented
            sg.PemeInvoiceDetails();
            //Test.stopTest();
            sg.Identifier=gc.id;
            sg.GrdContRetriver();
            sg.userType='Clinic';
            sg.Identifier=GardTestData.brokerCase.id;
            sg.viewServiceRequest();
            sg.Identifier=GardTestData.brokerClaim.id;
            sg.showCaseDetails();
            //commented
            //sg.doSearch();
            //sg.doSearchServiceRequests();
            //ApexPages.currentPage().getParameters().put('id','*hennessey');
            //SearchGard sgObj = new SearchGard();
            //ApexPages.currentPage().getParameters().put('id','*Asset');
            //SearchGard sgAsset = new SearchGard();
            //ApexPages.currentPage().getParameters().put('id','test*');
            //SearchGard sgCase = new SearchGard();
            
        }
        test.stopTest();
    }
}