@isTest
public class TestContactRoleWrapper {
    private static Id accountId;
    private static List<Contact> contacts;
    private static List<AccountContactRelation> aconRelLst = new List<AccountContactRelation>();
    
    private static void createTestData(){
        GardTestData gtd = new GardTestData();
        gtd.commonRecord();
        AccountContactRelation acrel = new AccountContactRelation();
        acrel.contactId = GardTestData.clientContact.id;
        acrel.AccountId = GardTestData.brokerAcc.Id;
        acrel.roles ='Member';
        aconRelLst.add(acrel);
        insert aconRelLst;
        accountId = GardTestData.clientAcc.Id;
        createContactsOf(1,accountId);
        Contact_roles_to_hide_in_MyGard__c setting = new Contact_roles_to_hide_in_MyGard__c();
        setting.Name = 'Member';
        setting.Role__c = 'Member';
        insert setting;
    }
    
    private static void createContactsOf(Integer numberOfContacts,Id parentAccountId){
        contacts = new List<Contact>();
        String fName = 'fName';
        String lname = 'lame';
        for(Integer index = 0;index < numberOfContacts;index++){
            contacts.add(new Contact(LastName=(lName+index),FirstName=(fName+index),accountId=parentAccountId));            
        }
        try{
            insert contacts;
        }catch(Exception e){
            System.debug('Exception caught while in createContactsOf - '+e);
        }
    }
    
    @isTest private static void test1(){
        test.startTest();
        createTestData();
        List<ContactRoleWrapper> crwl = new List<ContactRoleWrapper>();
        for(Contact contact : contacts){
            crwl.add(new ContactRoleWrapper(contact,'Role',true));
        }
        test.stopTest();
    }
    @isTest private static void test2(){
        test.startTest();
        createTestData();
        ContactRoleWrapper.showAllContact(GardTestData.brokerAcc.Id);
        List<ContactRoleWrapper> crwl = ContactRoleWrapper.cRWMap.values();
        crwl.sort();
        test.stopTest();
    }
    
    
}