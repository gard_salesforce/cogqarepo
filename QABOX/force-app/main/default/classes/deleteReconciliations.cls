global class deleteReconciliations implements Database.Batchable<sObject> {
    global String query = 'SELECT Id, Name FROM Company_Reconciliation__c';
                    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        //check whether records need to be processed
        if (scope.size()>0) {
            //delete records
            delete scope;
        }
    }

    global void finish(Database.BatchableContext BC) {
        //ASSERT: do nothing
    }
    
    public static TestMethod void testDelRec() {
        // Instantiate class
        deleteReconciliations dc = new deleteReconciliations();
        // create a new test account
        Account acc = new Account(name='test acc', Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id, Market_Area__c = TestDataGenerator.getMarketArea().Id);
        insert(acc);
        // created a new company reconciliation record and associate to account
        Company_Reconciliation__c cr = new Company_Reconciliation__c();
        cr.Company__c = acc.Id;
        insert(cr);
        string idCR = cr.Id;
        // construct the query which will be used for the delete
        dc.query='SELECT Id, Name FROM Company_Reconciliation__c where Id = \'' + idCR + '\'';
        
        // assert that the test record exists
        system.assert([select count() from Company_Reconciliation__c where id = :idCR LIMIT 1] == 1);
        
        // run the batch, inside start/stoptest to make sure the batch has completed before the assert
        test.StartTest();
        Database.executeBatch(dc);
        test.StopTest();
        
        // assert that the test record has been deleted
        system.assert([select count() from Company_Reconciliation__c where id = :idCR LIMIT 1] == 0);
        
    }
}