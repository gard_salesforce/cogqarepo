@isTest
public class testShareDataCtrl{
    static TestMethod void testShareDataCtrlMethod(){
        string agId;
        GardTestData GT = new GardTestData();
        GT.commonRecord();
        string sfLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id; 
        user   salesforceLicUser2 = new User(
                                        Alias = 'standt1', 
                                        profileId = sfLicenseId ,
                                        Email='standarduser1@testorg.com',
                                        EmailEncodingKey='UTF-8',
                                        CommunityNickname = 'test113',
                                        LastName='Testing1',
                                        LanguageLocaleKey='en_US',
                                        LocaleSidKey='en_US', 
                                        //contactID = brokercontact.id, 
                                        TimeZoneSidKey='America/Los_Angeles',
                                        UserName='mygardtest1008@testorg.com.mygard',
                                        contactId__c = GardTestData.grdobj.id
                                   );
         insert salesforceLicUser2;
         System.assertEquals(salesforceLicUser2.Alias , 'standt1');
         Market_Area__c Markt2= new Market_Area__c(Market_Area_Code__c = 'abc120');
         insert Markt2;
         Account brokerAcc2 = new Account(   Name='testName',
                                    BillingCity = 'Southampton1',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS20 AD!',
                                    BillingState = 'Test Avon LAng' ,
                                    BillingStreet = '3 Mangrove Road',
                                    recordTypeId=System.Label.Broker_Contact_Record_Type,
                                    Site = '_www.IBM.se',
                                    Type = 'prospect',
                                    company_Role__c = 'broker',
                                    //Company_ID__c = '64143',
                                    guid__c = '8ce8ad89-a6ed-1836-9e18',
                                    Market_Area__c = Markt2.id,
                                    Product_Area_UWR_2__c='P&I',
                                    Product_Area_UWR_4__c='P&I',
                                    Product_Area_UWR_3__c='Marine',
                                    Role_Broker__c = true,
                                    Area_Manager__c = salesforceLicUser2.id ,
                                    X2nd_UWR__c= salesforceLicUser2.id,
                                    Claim_handler_Crew__c = salesforceLicUser2.id
                                  //  Ownerid=salesforceLicUser.id
                               );
        insert  brokerAcc2 ;
        
        agreement_set__c agreementSet = new agreement_set__c();
        agreementSet.agreement_set_type__c = 'Test';
        agreementSet.shared_by__c = GardTestData.brokerAcc.Id;
        agreementSet.shared_with__c = brokerAcc2.Id;
        agreementSet.status__c = 'Active';
        insert agreementSet ;
        agId = agreementSet.Id;
        System.assertEquals(agreementSet.agreement_set_type__c,'Test');
        broker_share__c brokerShare = new broker_share__c();
        brokerShare.Active__c = true;
        brokerShare.Agreement_set_id__c = agreementSet.Id;
        brokerShare.contract_external_id__c = 'TestId001';
        brokerShare.Contract_id__c = GardTestData.brokercontract_1st.Id;
        brokerShare.shared_to_client__c = true;
        insert brokerShare;

        contact_share__c cs = new contact_share__c(broker_share_id__c = brokerShare.Id,
                                                   ContactId__c = salesforceLicUser2.Id);
        insert cs;
        Share_data_Target_Company__c sdtc = new Share_data_Target_Company__c(Shared_By_Broker__c = GardTestData.brokerAcc.Id,
                                                                             Shared_With_Broker__c = brokerAcc2.Id);
        insert sdtc;
        test.startTest();    
        system.RunAs(GardTestData.brokerUser){
            
            ShareDataCtrl sdc = new ShareDataCtrl();
            sdc.lstTargetCompanyOptions.add('TestCompany');
            sdc.lstTargetContactOptions.add('TestOptions');
            PageReference pageRef = Page.ShareData;
            Test.setCurrentPage(pageRef);
            sdc.lstClientOptions.add(GardTestData.clientContact.Id);
            sdc.getClientOptions();
            //sdc.selectedSharedId = agreementSet.Id;
            sdc.lstShareNameOptions.add(brokerShare.Id);
            sdc.getShareNameOptions();
            sdc.setSortDirection('ASC');
            sdc.getTargetCompanyOptions();
            sdc.getTargetContactOptions();
            String sDir = sdc.getSortDirection();
            System.assertEquals('ASC',sDir);
            sdc.getFilterData();
            sdc.strJunctionRole = 'Admin';
            sdc.populateData();
            sdc.fillData();
            sdc.getShares();
           // sdc.getRelatedData(agId);
            sdc.exportToExcelForShareData();
            //sdc.deleteRecord();
            //sdc.startsharing();
            //sdc.stopsharing();
            sdc.clearOptions();
            ShareDataCtrl.getGenerateTimeStamp();
            ShareDataCtrl.getGenerateTime();
            ShareDataCtrl.getUserName();
            sdc.print();
            sdc.setpageNumber();
            sdc.firstRecord();
            sdc.lastRecord();
            sdc.nextRecord();
            sdc.previousRecord();
            sdc.editShare();
            sdc.strParams = [SELECT Id,guid__c FROM Agreement_Set__c WHERE Id =: agreementSet.Id].guid__c;
            sdc.editShare();
            ShareDataCtrl.ShareDataWrapper sdw = new ShareDataCtrl.ShareDataWrapper('test','test','test','test','test','test','test');
            sdc.SelectedShareName.add('Test data for broker share');
            sdc.SelectedClient.add('Testuu');
            sdc.SelectedTargetContact.add('Yoo');
            sdc.SelectedTargetCompany.add('testName');
            sdc.getFilterData();
            sdc.getPaginationData();
            sdc.selectedSharedId = agreementSet.Id;
            sdc.getRelatedData(agId);
            
            sdc.startsharing();
            sdc.stopsharing();
            sdc.deleteRecord();
            
        }
         system.RunAs(GardTestData.clientUser){
            ShareDataCtrl sdc = new ShareDataCtrl();
            sdc.lstTargetCompanyOptions.add('TestCompany');
            PageReference pageRef = Page.ShareData;
            Test.setCurrentPage(pageRef);
            sdc.lstClientOptions.add(GardTestData.clientContact.Id);
            sdc.getClientOptions();
            sdc.lstShareNameOptions.add(brokerShare.Id);
            sdc.getShareNameOptions();
            sdc.setSortDirection('ASC');
            sdc.getTargetCompanyOptions();
            sdc.getTargetContactOptions();
            sdc.getSortDirection();
            sdc.getFilterData();
            sdc.isPageLoad =  false;
            sdc.getFilterData();
            sdc.populateData();
            
            }
         test.stopTest();
    }
}