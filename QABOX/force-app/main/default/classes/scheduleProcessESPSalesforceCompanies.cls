global class scheduleProcessESPSalesforceCompanies implements Schedulable {
    global void execute (SchedulableContext SC) {
        processESPSalesforceCompanies pe = new processESPSalesforceCompanies();
        
        pe.Query = 'SELECT Name, Id, Salesforce_Company__c, Type__c, Master__c from Admin_System_Company__c where Type__c = \'ESP\'';
        Database.executeBatch(pe, 20);
    }
    
    static testMethod void testSchedule() {
        Test.StartTest();
            scheduleProcessESPSalesforceCompanies testSched = new scheduleProcessESPSalesforceCompanies();
            String strCRON = '0 0 4 * * ?';
            System.schedule('scheduleProcessESPSalesforceCompanies Test', strCRON, testSched);
        Test.StopTest();
    }
}