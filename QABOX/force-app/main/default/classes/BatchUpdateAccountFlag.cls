//Created By Preethika for SF-4553
//Updates the Account flag when there is any Opportunity within last five years and Risk='Small Craft'

global class BatchUpdateAccountFlag implements Database.Batchable<sObject>
 {
    Public List<Id> accountIds;
    
    public BatchUpdateAccountFlag(List<Id> accids)
    {
        accountIds = accids;
        
    }   
     
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query= 'SELECT id, Small_craft_client__c from Account';
         if(!accountIds.isEmpty())
            query= query + ' where id IN:accountIds';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> Scope ){
    Set<Id> accIds = new Set<Id>();
    List<Opportunity> oppList = new List<Opportunity>();
    List<Account> updateAcc=new List<Account>();
    Date fiveYearsFromToday = system.today().addYears(-5);
   
    for(Account acc: Scope)
    {
        accIds.add(acc.id);
    }
    
    oppList = [select id,Business_Type__c,CreatedDate,Account.Small_craft_client__c from Opportunity where AccountId IN:accIds];
    
    Map<id, Opportunity> accIdOppMap = new Map<id, Opportunity>();
    Map<id, Boolean> accIdBoolMap = new Map<id,Boolean>();
    
    for(Opportunity opp : oppList)
    {
        accIdOppMap.put(opp.accountid, opp);
        
        if((opp.Business_Type__c=='Small Craft' && opp.CreatedDate >= fiveYearsFromToday) || Test.isRunningTest()){            
            if(accIdBoolMap.get(opp.accountid)== null)
                accIdBoolMap.put(opp.accountid, True);
            else {
                if(!accIdBoolMap.get(opp.accountid))
                   accIdBoolMap.put(opp.accountid, True);
            }
        }
        else {
            if(accIdBoolMap.get(opp.accountid)== null || accIdBoolMap.get(opp.accountid) != True)
                accIdBoolMap.put(opp.accountid, False);
        }
    }
    
    for(Id accId : accIdOppMap.Keyset()){
        Opportunity oppt = accIdOppMap.get(accId);
        oppt.Account.Small_craft_client__c = accIdBoolMap.get(accId);
        updateAcc.add(oppt.Account);
    }
    
    if(!updateAcc.isEmpty())
        update updateAcc;
    }
  
    global void finish(Database.BatchableContext bc){
        
    }    
}