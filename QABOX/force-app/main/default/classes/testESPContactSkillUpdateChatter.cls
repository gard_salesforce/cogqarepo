/**

9/4/13 - Steve O'Connell - cDecisions Ltd

Test that when a new contact skill is added or an existing skill is updated
that a chatter post is created on both the contact and contact owner Chatter
feeds. 

*/

@isTest
private class testESPContactSkillUpdateChatter {

	public static Contact testCon;
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;

	static void createData()
	{
		//Create a company
		Market_Area__c testMarketArea = new Market_Area__c(Name='Nordic', Market_Area_Code__c = 'NORD');
		insert testMarketArea;
		
		RecordType clientAccRecType = [SELECT Id From RecordType WHERE Name='Client' and SObjectType='Account'];
				
		Account testAcc = new Account(RecordTypeId = clientAccRecType.Id, Name='Test Company', Market_Area__c = testMarketArea.Id);
		insert testAcc;
		
		//Create a contact
		testCon = new Contact(FirstName='Test', LastName='Skills', AccountId=testAcc.Id);
		insert testCon;
	}

    static testMethod void unitTest() {
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        
        createData();
        
        Integer initialContactFeedPosts;
        Integer initialOwnerFeedPosts;
        Integer afterNewContactFeedPosts;
        Integer afterNewOwnerFeedPosts;
        Integer afterUpdateContactFeedPosts;
        
        initialContactFeedPosts = [SELECT Count() FROM ContactFeed where ParentId = :testCon.Id];
        initialOwnerFeedPosts = [SELECT Count() FROM UserFeed where ParentId = :testCon.OwnerId];
        
        //Test creating new skill
        ESP_Contact_skills__c newSkill = new ESP_Contact_skills__c(ESP_Contact_skill__c=testCon.Id, Main_skill__c='Cause investigation', Specialised_skill__c='Electric', Skill_comments__c='Test comment');
        insert newSkill;
        
        afterNewContactFeedPosts = [SELECT Count() FROM ContactFeed where ParentId = :testCon.Id];
        afterNewOwnerFeedPosts = [SELECT Count() FROM FeedItem where ParentId = :testCon.OwnerId];
       
       
       	System.debug('Initial Contact: ' + initialContactFeedPosts);
       	System.debug('After New Contact: ' + afterNewContactFeedPosts);
       	System.assert((initialContactFeedPosts+1) == afterNewContactFeedPosts);
       	
       	/*System.debug('Initial Owner: ' + initialOwnerFeedPosts);
       	System.debug('After New Owner: ' + afterNewOwnerFeedPosts);
       	System.assert((initialOwnerFeedPosts+1) == afterNewOwnerFeedPosts);*/
       	
       	newSkill.Main_skill__c = 'Biologist';
       	newSkill.Specialised_skill__c = 'Fisheries';
       	update newSkill;
       	
       	afterUpdateContactFeedPosts = [SELECT Count() FROM ContactFeed where ParentId = :testCon.Id];
               	
       	System.debug('Initial Contact: ' + initialContactFeedPosts);
       	System.debug('After Update Contact: ' + afterUpdateContactFeedPosts);
       	System.assert((initialContactFeedPosts+2) == afterUpdateContactFeedPosts);
       	
       	newSkill.Skill_comments__c = 'Different comment';
       	update newSkill;
       	
       	afterUpdateContactFeedPosts = [SELECT Count() FROM ContactFeed where ParentId = :testCon.Id];
        
        System.debug('Initial Contact: ' + initialContactFeedPosts);
       	System.debug('After Update Contact: ' + afterUpdateContactFeedPosts);
       	System.assert((initialContactFeedPosts+3) == afterUpdateContactFeedPosts);
    }
}