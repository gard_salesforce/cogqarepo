/*************************************************************************************************
    Author      : Martin Gardner
    Company     : cDecisions
    Date Created: 
    Description : This class provides helper methods for working with Market areas
    This helper class provides static methods for looking up the correct Area Manager for a 
    given account based on its market area. The Area Manager is defined as being the owner of the 
    Market Area record. It will be necessary to set and maintain these values in production once live
    to ensure that this functionality continues to operate correctly.
    Modified    :   
***************************************************************************************************/
public with sharing class AccountMarketAreaToolkit {

	//A map of Market Area name -> User
	
	public static boolean EnableMarketAreaManagers = DataQualitySupport.GetSetting(UserInfo.getUserId()).Enable_Account_Market_Area_Manager__c;
	
	public static Map<String, String> mapMarketAreaManagers{
		get{
			if(mapMarketAreaManagers==null){
				//query the Market Area object to build a map of Market area name to user
				mapMarketAreaManagers = new Map<String,String>();
				List<Market_Area__c> lstMarketAreas = new List<Market_Area__c>();
				lstMarketAreas = [SELECT Id, Name, OwnerId FROM Market_Area__c];
				for(Market_Area__c aMarket : lstMarketAreas){
					mapMarketAreaManagers.put(aMarket.Name, aMarket.OwnerId);
				} 
			}
			return mapMarketAreaManagers;
		}
		private set;
	}
	//A map of Market Area id -> User

	public static Map<String, String> mapMarketAreaManagerIds{
		get{
			if(mapMarketAreaManagerIds==null){
				//query the Market Area object to build a map of Market area name to user
				mapMarketAreaManagerIds = new Map<String,String>();
				List<Market_Area__c> lstMarketAreas = new List<Market_Area__c>();
				lstMarketAreas = [SELECT Id, Name, OwnerId FROM Market_Area__c];
				for(Market_Area__c aMarket : lstMarketAreas){
					mapMarketAreaManagerIds.put(aMarket.Id, aMarket.OwnerId);
				} 
			}
			return mapMarketAreaManagerIds;
		}
		private set;
	}	
	
	//input is a map of Account Id -> Market Area
	//output is a map of Account Id -> User (Area Manager) for the given Market Area
	/*This method accepts a map of account id to market area name and amends that map in memory (and returns it) 
	so that it contains a mapping of account id to market area owner id.*/
	public static Map<String,String> getAccountMarketAreaManagers(Map<String,String> mapAccMA){
		//run through the given map and replace the market area with the owner from the map
		
		if(mapAccMA!= null){
			for(String id : mapAccMA.keySet()){
				mapAccMA.put(id, mapMarketAreaManagers.get(mapAccMA.get(id)));
			}
		}
		return mapAccMA;
	}

	//input is a map of Account Id -> Market Area Id
	//output is a map of Account Id -> User (Area Manager) for the given Market Area
	/*This method accepts a map of account id to market area id and amends that map in memory (and returns it) 
	so that it contains a mapping of account id to market area owner id.*/	
	public static Map<String,String> getAccountMarketAreaManagerIds(Map<String,String> mapAccMA){
		//run through the given map and replace the market area with the owner from the map
		
		if(mapAccMA!= null){
			for(String id : mapAccMA.keySet()){
				mapAccMA.put(id, mapMarketAreaManagerIds.get(mapAccMA.get(id)));
			}
		}
		return mapAccMA;
	}
	
	//Run through the map and check that the approving area manager is set correctly. 
	//if it isn't then set it to the correct value
	/*This method accepts a map of account id to account record and amends that map in memory (and returns it) 
	so that it contains a mapping of account id to account records where the Approving_Area_Manager__c is set to the market area owner id.*/	
	public static Map<Id, Account> setAccountMarketAreaManagers(Map<Id, Account> newMap){
		
		if(newMap!=null && EnableMarketAreaManagers){
			for(Account acct: newMap.values()){
				if(acct.Market_Area__c!=null){
					if(acct.Approving_Area_Manager__c != AccountMarketAreaToolkit.mapMarketAreaManagerIds.get(acct.Market_Area__c)){
						acct.Approving_Area_Manager__c = AccountMarketAreaToolkit.mapMarketAreaManagerIds.get(acct.Market_Area__c);
						//update the map
						newMap.get(acct.Id).Approving_Area_Manager__c = AccountMarketAreaToolkit.mapMarketAreaManagerIds.get(acct.Market_Area__c);
					}					
				}
			}
		}
		return newMap;
	}
}