/****************************************************************************************************************************
*    Deveployed By   :    Cognizant Technology Solution
*    Created Date    :    05/29/2019 
*    Descriptions    :    This class is used to send reminder notification email to claim handler for all pending cases.   
*    Modification Log
*    --------------------------------------------------------------------------------------------------------
*    Developer                                Date                        Description
*    --------------------------------------------------------------------------------------------------------
*    Arnab Sarkar                              05/29/2019        SF-5013  Initial version
******************************************************************************************************************************/
global class BatchSendNotificationForClaims implements Database.batchable <sObject> {
    //global string pendingCaseQuery = 'select id, Member_reference__c, MyGard_Claim_ID__c, Account.name, Risk_Coverage__r.Name, Object__r.Name, Object__r.Imo_Lloyds_No__c, Claim_Type__c, Claim_Detail__c, Event_Date__c, Place_of_incident__c, Claims_handler__r.ContactId__c, Owner.id, Owner.Name,Owner.Phone, Owner.Email from case where MyGard_Claim_ID__c!=null and status=\'Pending\'';
    global string pendingCaseQuery = 'select id, Member_reference__c, MyGard_Claim_ID__c, Account.name, Risk_Coverage__r.Name, Object__r.Name, Object__r.Imo_Lloyds_No__c, Claim_Type__c, Claim_Detail__c, Event_Date__c, Place_of_incident__c, Claims_handler__r.ContactId__c, Owner.id, Owner.Name,Owner.Phone, Owner.Email from case where recordTypeId IN (SELECT id from RecordType where name = \'MyGardClaim\') and claims_handler__c != null and status=\'Pending\'';
    
    global Database.QueryLocator start (Database.BatchableContext bc) {
        return Database.getQueryLocator(pendingCaseQuery);
    }
    
    global void execute (Database.BatchableContext bc, List <case> pendingCases) { 
         system.debug('BatchSendReminderNotificationForClaims');
         if (pendingCases != null && pendingCases.size() >0) {
             EmailSender emailSender = new EmailSender();
             Set<Id> userIds = new Set<Id>();
             Set<Id> gardContactIds = new Set<Id>();
             //Set<Id> caseIds = (new Map<Id, Case>(pendingCases)).keySet();
             for (Case caseObj: pendingCases) {
                 userIds.add(caseObj.Owner.Id);
                 gardContactIds.add(caseObj.Claims_handler__r.ContactId__c);
                 system.debug('User ' + caseObj.Owner.Id);
                 system.debug('Gard Contact ID ' + caseObj.Claims_handler__r.ContactId__c);
             } 
             Map<id, User> users = new Map<id, User>([select id, MobilePhone from User where id in: userIds]);
             Map<id, Gard_Contacts__c> gardContacts = new Map<id, Gard_Contacts__c>([SELECT id, Email__c from Gard_Contacts__c where id IN :gardContactIds]);
             emailSender.createCaseReminderEmail('Reminder-PendingMyGardClaim', pendingCases, users, gardContacts);     
         } else {
            system.debug('No mails to send as no pending cases '); 
         }       
    }
    
    global void finish (Database.BatchableContext bc) {}
}