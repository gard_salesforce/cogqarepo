public with sharing class MDMSyncRoleRetryExtension {

	private final MDM_Role__c r;
	
	public String redirectUrl {public get; private set;}
    public Boolean shouldRedirect {public get; private set;}
	
	public Boolean IsAdmin { 
		get { 
			MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
			return mdmSettings.Retry_Buttons_Enabled__c;
		} 
	}
	
	public MDMSyncRoleRetryExtension (ApexPages.StandardController stdController) {
    	this.r = (MDM_Role__c)stdController.getRecord();
		redirectUrl = stdController.view().getUrl();
		shouldRedirect = false;
    }
	
	public ApexPages.PageReference RetryRoleSync() {
		shouldRedirect = true;
		//Hopefully a better way of doing this than below as needs no logic here and will lock the account until the future method is completed...
		r.Synchronisation_Status__c = 'Sync In Progress';
		update r;
		/*
		if (r.Active__c && r.Synchronisation_Status__c == 'Sync Failed') {
			MDMRoleProxy.MDMUpsertRoleAsync(new List<id> { r.Id });	
		} else if (!r.Active__c && r.Synchronisation_Status__c == 'Sync Failed') {
			MDMRoleProxy.MDMDeleteRoleAsync(new List<id> { r.Id });
		}
		*/
		return null;
	}

}