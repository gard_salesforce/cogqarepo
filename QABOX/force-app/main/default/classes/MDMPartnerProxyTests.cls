@IsTest
private class MDMPartnerProxyTests {

    private static testmethod void UpsertPartnerAsync_Success() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, true);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMPartnerProxy.MDMUpsertPartnerAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Synchronisation_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Synchronised', a.Synchronisation_Status__c, 'Account Sync Status = Synchronised');
        
    }
    
    private static testmethod void UpsertPartnerAsync_Failure() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, false);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMPartnerProxy.MDMUpsertPartnerAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Synchronisation_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Sync Failed', a.Synchronisation_Status__c, 'Account Sync Status = Sync Failed');
        
    }
    
    private static testmethod void UpsertPartnerAsync_NullResponse() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = null;
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMPartnerProxy.MDMUpsertPartnerAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Synchronisation_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Sync Failed', a.Synchronisation_Status__c, 'Account Sync Status = Sync Failed');
        
    }
    
    private static testmethod void DeletetPartnerAsync_Success() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, true);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMPartnerProxy.MDMDeletePartnerAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Synchronisation_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Synchronised', a.Synchronisation_Status__c, 'Account Sync Status = Synchronised');
        
    }
    
    private static testmethod void DeletePartnerAsync_Failure() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, false);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMPartnerProxy.MDMDeletePartnerAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Synchronisation_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Sync Failed', a.Synchronisation_Status__c, 'Account Sync Status = Sync Failed');
        
    }
    
    private static testmethod void DeletePartnerAsync_NullResponse() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = null;
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMPartnerProxy.MDMDeletePartnerAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Synchronisation_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Sync Failed', a.Synchronisation_Status__c, 'Account Sync Status = Sync Failed');
        
    }

}