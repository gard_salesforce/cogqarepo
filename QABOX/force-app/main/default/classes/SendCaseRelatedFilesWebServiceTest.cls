@isTest(seeAllData=true)
public class SendCaseRelatedFilesWebServiceTest{
    public static Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Transactions').getRecordTypeId();
    
    public static case parentCase = new Case();
    public static List<id> conDocIdList = new List<Id>();
    /*
    @testSetup  static void setUpTestData(){
        
        GardTestData test_rec = new GardTestData();
        test_rec.commonRecord();
        test_rec.customsettings_rec();
        system.debug('--Test records inserted--');
        
        GardTestData.brokerCase.RecordTypeId = recordTypeId;
        update GardTestData.brokerCase; 
        */
        //parentCase = GardTestData.brokerCase;
        //system.debug('--case id--'+parentCase.Id);
        //case cs = new case();
        //cs.subject = 'test';
        //cs.recordTypeId = recordTypeId;
        //insert cs;
        /*
        List<Cover_and_Objects_for_case__c> junctionCoverList = new List<Cover_and_Objects_for_case__c>();
        Cover_and_Objects_for_case__c junctionCover1 = new Cover_and_Objects_for_case__c();
        junctionCover1.Case__c = GardTestData.brokerCase.Id;
        junctionCover1.Cover__c = GardTestData.brokerAsset_1st.Id;
        junctionCover1.Object__c = GardTestData.test_object_1st.Id;
        junctionCoverList.add(junctionCover1);
        
        Cover_and_Objects_for_case__c junctionCover2 = new Cover_and_Objects_for_case__c();
        junctionCover2.Case__c = GardTestData.brokerCase.Id;
        junctionCover2.Cover__c = GardTestData.brokerAsset_2nd.Id;
        junctionCover2.Object__c = GardTestData.test_object_1st.Id;
        junctionCoverList.add(junctionCover2);
        
        insert junctionCoverList;
        */
        /*
        EmailMessage emailToCase = new EmailMessage();
        emailToCase.Subject = 'test';
        emailToCase.TextBody = 'test body';
        emailToCase.htmlbody = '<html><head></head><body><div>test body</div></body></html>\n\n';
        emailToCase.ParentId = GardTestData.brokerCase.Id;//CTCase.Id;
        //insert emailToCase;
        */
        /*
        ContentVersion contentVersion1 = new ContentVersion(Title = 'Penguins',
                                                           PathOnClient = 'Penguins.jpg',
                                                           VersionData = Blob.valueOf('Test Content1'),
                                                           IsMajorVersion = true
                                                           );
        
        insert contentVersion1;
        conDocIdList.add(contentVersion1.Id);
    }
    */
    @isTest
    public static void testSendCaseRelatedFiles() {
        test.startTest();
        Id caseId = System.Label.WebServiceCaseId;
        List<id> conDocIdList = new List<Id>{[SELECT Id from ContentVersion limit 1].Id};
        Test.setMock(HttpCalloutMock.class, new MuleWebServiceTestMock());
        showCaseRelatedFilesController.sendCaseFiles(caseId,conDocIdList);//('5002600000MWslOAAT',conDocIdList);
        //system.debug('--case id in method--'+parentCase.Id);
        //showCaseRelatedFilesController.sendCaseFiles(parentCase.Id,conDocIdList);
        test.stopTest();
    }
}