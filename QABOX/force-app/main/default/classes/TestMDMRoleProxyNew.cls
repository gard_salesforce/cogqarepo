@IsTest
private class TestMDMRoleProxyNew{
private static List<Valid_Role_Combination__c> validRoles;
    @isTest private static void testRoleMergeCallOut(){
        setupValidRoles();
        //MDMProxyTests.setMDMConfig();
        MDMProxyTests.setupMDMConfigSettings();
        //Test.loadData(MDMConfig__c.sObjectType, 'MDMConfig');
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        MDM_Role__c m = new MDM_Role__c( Account__c = a.id,  Valid_Role_Combination__c = validRoles[0].id);
        try{
            insert m;
        }catch(Exception ex){System.debug('mdm role insertion exception in testRoleMergeCallout'+ ex);}
        Set<ID> idSet = new Set<ID>();
        idSet.add(m.id);
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(m.id, true);
        MDMRoleProxyNew.roleMergeCallOut('testRequestString',idSet);
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(m.id, false);
        MDMRoleProxyNew.roleMergeCallOut('testRequestString',idSet);
        Dom.Document responseDoc = (Dom.Document)WebServiceUtilTests.Instance.MockWebServiceResponse;
        MDMRoleProxyNew.MDMMergeRoleResponse response = new MDMRoleProxyNew.MDMMergeRoleResponse();
        response.Deserialize(
                    responseDoc
                        .getRootElement()
                        .getChildElement('Body', MDMConfig__c.getInstance().SOAPNS__c)
                        .getChildElement('mdmChangeResponse', 'http://www.gard.no/mdm/v1_0/mdmpartner')
                    );
        for(MDMRoleProxyNew.MDMResponse mdmr : response.mdmResponses){
                   mdmr.get('id');mdmr.get('success');mdmr.get('error');mdmr.get('forElsePart');
                   mdmr.getFields();
        }
        //((MDMRoleProxyNew.MDMResponse)WebServiceUtilTests.Instance.MockWebServiceResponse).get('error');
        //((MDMRoleProxyNew.MDMResponse)WebServiceUtilTests.Instance.MockWebServiceResponse).get('success');
    }
    
    private static void setupValidRoles() {
        ////For some reason this seems to be throwing an internal Salesforce error /
        ////System.UnexpectedException: Salesforce System Error: 1583762872-13077 (1716107030) (1716107030)
        //validRoles = (List<Valid_Role_Combination__c>)Test.loadData(Valid_Role_Combination__c.sObjectType, 'MDMValidRoles');
        ////END...
        validRoles = new List<Valid_Role_Combination__c>();
        validRoles.add(new Valid_Role_Combination__c(Role__c = 'Other', Sub_Role__c = 'Other Sub Role'));
        insert validRoles;
    }
    private static testmethod void UpsertRoleAsync_Success() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        setupValidRoles();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        MDM_Role__c m = new MDM_Role__c( Account__c = a.id,  Valid_Role_Combination__c = validRoles[0].id);
        insert m;
        id id = m.id;
       
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, true);
         List<MDM_Role__c> mdmRolelst =new List<MDM_Role__c>();
        mdmRolelst.add(m);        
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMRoleProxyNew.MDMUpsertRoleAsync(new List<id> { id });
        MDMRoleProxyNew.SetRolesSyncFailed(mdmRolelst,'source');
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        m = [SELECT id, Synchronisation_Status__c FROM MDM_Role__c WHERE id = :id];
    }
    
    private static testmethod void UpsertRoleAsync_Failure() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        setupValidRoles();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        MDM_Role__c m = new MDM_Role__c( Account__c = a.id, Valid_Role_Combination__c = validRoles[0].id);
        insert m;
        id id = m.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, false);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMRoleProxyNew.MDMUpsertRoleAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        m = [SELECT id, Synchronisation_Status__c FROM MDM_Role__c WHERE id = :id];
    }
    
    private static testmethod void UpsertRoleAsync_NullResponse() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        setupValidRoles();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        MDM_Role__c m = new MDM_Role__c( Account__c = a.id, Valid_Role_Combination__c = validRoles[0].id);
        insert m;
        id id = m.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = null;
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMRoleProxyNew.MDMUpsertRoleAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        m = [SELECT id, Synchronisation_Status__c FROM MDM_Role__c WHERE id = :id];   
    }
    private static testmethod void DeletetRoleAsync_Success() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        setupValidRoles();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        MDM_Role__c m = new MDM_Role__c( Account__c = a.id,  Valid_Role_Combination__c = validRoles[0].id);
        insert m;
        id id = m.id;
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, true);
                
        ///Act...
        Test.StartTest();
        // manually call the web service);
        MDMRoleProxyNew.MDMDeleteRoleAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        m = [SELECT id, Synchronisation_Status__c FROM MDM_Role__c WHERE id = :id];
    }

    private static testmethod void DeleteRoleAsync_Failure() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        setupValidRoles();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        MDM_Role__c m = new MDM_Role__c( Account__c = a.id,  Valid_Role_Combination__c = validRoles[0].id);
        insert m;
        id id = m.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, false);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMRoleProxyNew.MDMDeleteRoleAsync(new List<id> { id });
        Test.StopTest();
        ///Assert...
        // ensure the sync status has been updated on the account
        m = [SELECT id, Synchronisation_Status__c FROM MDM_Role__c WHERE id = :id];
    }
    
    private static testmethod void DeleteRoleAsync_NullResponse() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        setupValidRoles();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        MDM_Role__c m = new MDM_Role__c( Account__c = a.id,  Valid_Role_Combination__c = validRoles[0].id);
        insert m;
        id id = m.id;
        WebServiceUtilTests.Instance.MockWebServiceResponse = null;
        ///Act...
        Test.StartTest();
        // manually call the web service
        //MDMRoleProxyNew.MDMDeleteRoleAsync(new List<id> { id });
        Test.StopTest();
        ///Assert...
        // ensure the sync status has been updated on the account
        m = [SELECT id, Synchronisation_Status__c FROM MDM_Role__c WHERE id = :id];              
    }
}