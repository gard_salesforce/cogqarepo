global class CompanyOnRiskInfoUpdateCtrl_Queueable implements Queueable{
    List<Company_on_risk_information__c> companyOnRiskResponseListForQueueable = new List<Company_on_risk_information__c>();
    public CompanyOnRiskInfoUpdateCtrl_Queueable(List<Company_on_risk_information__c> companyOnRiskResponseListForQueueable){
        this.companyOnRiskResponseListForQueueable = companyOnRiskResponseListForQueueable;
    }
    public void execute(QueueableContext context){
        if(companyOnRiskResponseListForQueueable != null && companyOnRiskResponseListForQueueable.size() < 10000)
        	upsert companyOnRiskResponseListForQueueable Partner_Id__c;
    }
}