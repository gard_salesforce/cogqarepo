public with sharing class HomePageController {
    //initialise controller
    public HomePageController() {}
    
    public Event[] getEvents() {
        Date dtNull = Date.newInstance(1900, 01, 01);
        Event[] qryEvents = [SELECT Id, Type, RecordType.Name, Account.Name, ActivityDate, ActivityDateTime, 
                             StartDateTime, EndDateTime, Subject, What.Name, Who.Name 
                             FROM Event 
                             WHERE OwnerId = :UserInfo.getUserId() AND ActivityDate>:dtNull
                             ORDER BY EndDateTime
                             LIMIT 20];
        return qryEvents;
    }
    
    public static TestMethod void testEvents() {
        HomePageController controller = new HomePageController();
        // created a new test event
        Event ev = new Event(OwnerId=userinfo.getuserid(),ActivityDate=system.today(),ActivityDateTime=system.now(),DurationInMinutes=10);
        Insert ev;
        // get events
        Event[] testEvents = controller.getEvents();
        // assert that at least one event record is returned
        System.Assert(testEvents.size()>0, 'ERROR: no events found!');                             
    }
}