@isTest(seeAllData=false)
Public class TestPortfolioFormsCtrl1
{
        public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
        public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    static testmethod void PortfolioFormsCtrl(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        Attachment att = new Attachment();
        gardtestdata test_rec= new gardtestdata();
        test_rec.commonrecord();
        test_rec.customsettings_rec();
        
           Forms_List__c Forms=new Forms_List__c(Type__c='Claim forms');
          insert Forms;
          
        Blob b = Blob.valueOf('Brokers Data');  
        Attachment attachment1 = new Attachment();  
        attachment1.ParentId = Forms.Id;
        attachment1.Name = 'Application form - Charterers.pdf';  
        attachment1.Body = b;  
        insert(attachment1); 
        
        //Blue_Card_renewal_availability__c CSBCAvailability = new Blue_Card_renewal_availability__c(name = 'value', BlueCardRenewalAvailability__c = true); 
        //insert CSBCAvailability;  
        //Invoking methods of Portfolioformsctrl for broker and client users.......
 
        System.runAs(gardtestdata.clientUser)         
        {
            Test.startTest();
            PortfolioFormsCtrl client_user = new PortfolioFormsCtrl();
           
            //client_user.formUrl='';
            client_user.UwGuid='';
            client_user.lstContact = new List<Contact>();
            client_user.caseId = gardtestdata.clientCase.id;
           // client_user.currentFileId = String.valueof(attachment.id);
           client_user.setGlobalClientIds = new List<String>();
           client_user.selectedGlobalClient = new List<String>();
            List<SelectOption> clientOptions =  client_user.getClientLst();
            //broker_user.strGlobalClient=brokerAcc.id+';'+brokerAcc.id;
            
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment1';
            Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body1');
            client_user.attachment.body=bodyBlob2;
            
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment11';
            Blob bodyBlob3=Blob.valueOf('Unit Test Attachment Body11');
            client_user.attachment.body=bodyBlob3;
            client_user.uwFormType = 'uw-off-obj-form-option'; 
            client_user.uploadFileForAll();
            
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment 6';
            Blob bodyBlob6c=Blob.valueOf('Unit Test Attachment Body 6');
            client_user.attachment.body=bodyBlob6c;
            client_user.uwFormType = 'uw-charter-client-form-option';
            client_user.uploadFileForAll();
            
            System.assertEquals(client_user.isSuccess , true);
            client_user.uwFormType = 'Renewal-Questionnaire-Form';
            //client_user.uploadFileForAll(); 
            client_user.uwFormType = 'uw-itopf-form-option'; 
            client_user.redirectQuestionnaire();
           // client_user.downLoadForm();
           
            client_user.showLOUForms();
            client_user.displayDoc();
            client_user.cancelForUwrForms();
            
            Test.stopTest();
          /*  client_user.uploadFileForAll();                       
            client_user.uwFormType = 'uw-pi-layup-form-option'; 
            client_user.uploadFileForAll();
            client_user.uwFormType = 'uw-ri-declaration-form-option'; 
            client_user.uploadFileForAll();
            client_user.uwFormType = 'uw-lou-loi-option'; 
            client_user.uploadFileForAll();
            client_user.uwFormType = 'uw-marine-layup-form-option'; 
            client_user.uploadFileForAll();
            client_user.strSelected=gardtestdata.clientAcc.id+';'+gardtestdata.clientAcc.id; 
            client_user.fetchSelectedClients();
            client_user.sendToForm();
            PortfolioFormsCtrl.NewAttachmentWrapper newobj = new PortfolioFormsCtrl.NewAttachmentWrapper('originalName', 'base64Value', att);
            client_user.currentFileId = 'Application form - Charterers.pdf';
            client_user.mapIdForms.put('Application form - Charterers.pdf',attachment1.id);
            client_user.forceDownload(); */
        }
      /*  System.runAs(gardtestdata.brokerUser)  
        {
            PortfolioFormsCtrl client_user=new PortfolioFormsCtrl();           
        } */
    }
}