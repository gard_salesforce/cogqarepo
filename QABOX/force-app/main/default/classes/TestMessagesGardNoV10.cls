@IsTest
public class TestMessagesGardNoV10 {
    private static testMethod void TestClaimTypeBase(){
    new messagesGardNoV10Claimtype.ClaimTypeBase();
    }
    private static testMethod void TestThirdPartyContactClaimDetailsBase(){
    new messagesGardNoV10Thirdpartycontactc.ThirdPartyContactClaimDetailsBase();
    }
    private static testMethod void TestThirdPartyCollisionClaimDetailsBase(){
    new messagesGardNoV10Thirdpartycollisio.ThirdPartyCollisionClaimDetailsBase();
    }
    private static testMethod void TestSubClaimDetailsBase(){
    new messagesGardNoV10Subclaimdetails.SubClaimDetailsBase();
    }
    private static testMethod void TestRenewBlueCardBase(){
    new messagesGardNoV10Renewbluecard.RenewBlueCardBase();
    }
    private static testMethod void TestStowawayClaimDetailsBase(){
    new messagesGardNoV10Stowawayclaimdetai.StowawayClaimDetailsBase();
    }
    private static testMethod void TestShipOperationBase(){
    new messagesGardNoV10Shipoperation.ShipOperationBase();
    }
    private static testMethod void TestReportResponseBase(){
    new messagesGardNoV10Reportresponse.ReportResponseBase();
    }
    private static testMethod void TestReimbursementClaimBase(){
    new messagesGardNoV10Reimbursementclaim.ReimbursementClaimBase();
    }
    private static testMethod void TestRankBase(){
    new messagesGardNoV10Rank.RankBase();
    }
    private static testMethod void TestPollutionClaimDetailsBase(){
    new messagesGardNoV10Pollutionclaimdeta.PollutionClaimDetailsBase();
    }
    private static testMethod void TestPersonalClaimDetailsBase(){
    new messagesGardNoV10Personalclaimdetai.PersonalClaimDetailsBase();
    }
    private static testMethod void TestPeopleClaimDetailsBase(){
    new messagesGardNoV10Peopleclaimdetails.PeopleClaimDetailsBase();
    }
    private static testMethod void TestObjectBaseContractreview(){
    new messagesGardNoV10ObjectContractreview.ObjectBase();
    }
    private static testMethod void TestObjectBase(){
    new messagesGardNoV10Object.ObjectBase();
    }
    private static testMethod void TestNationalityBase(){
    new messagesGardNoV10Nationality.NationalityBase();
    }
    private static testMethod void TestmessagesGardNoV10Locinput(){
    new messagesGardNoV10Locinput.ClientType();
    new messagesGardNoV10Locinput.CoverType();
    new messagesGardNoV10Locinput.LOCInputBase();
    }
    private static testMethod void TestLocationBase(){
    new messagesGardNoV10Location.LocationBase();
    }
    private static testMethod void TestFaultResponseBase2(){
    new messagesGardNoV10Faultresponse2.FaultResponseBase();
    }
    private static testMethod void TestFaultResponseBase(){
    new messagesGardNoV10Faultresponse.FaultResponseBase();
    }
    private static testMethod void TestEyewitnessBase(){
    new messagesGardNoV10Eyewitness.EyewitnessBase();
    }
    private static testMethod void TestExpencesBase(){
    new messagesGardNoV10Expences.ExpencesBase();
    }
    private static testMethod void TestmessagesGardNoV10Documents(){
    new messagesGardNoV10Documents.DocumentsBase();
    new messagesGardNoV10Documents.DocumentBase();
    }
    private static testMethod void TestDeductionBase(){
    new messagesGardNoV10Deduction.DeductionBase();
    }
    private static testMethod void TestCrewClaimDetailsBase(){
    new messagesGardNoV10Crewclaimdetails.CrewClaimDetailsBase();
    }
    private static testMethod void TestBlueCardTypeBase(){
    new messagesGardNoV10Bluecardtype.BlueCardTypeBase();
    }
    private static testMethod void TestCargoClaimDetailsBase(){
    new messagesGardNoV10Cargoclaimdetails.CargoClaimDetailsBase();
    }
    private static testMethod void TestCauseOfInjuryBase (){
    new messagesGardNoV10Causeofinjury.CauseOfInjuryBase();
    }
    private static testMethod void TestClaimOccuredLocationBase (){
    new messagesGardNoV10Claimoccuredlocati.ClaimOccuredLocationBase();
    }
    private static testMethod void TestClientBase(){
    new messagesGardNoV10Client.ClientBase();
    }
    private static testMethod void TestLOCSendEmailInputBase(){
    new messagesGardNoV10Locsendemailinput.LOCSendEmailInputBase();
    }
    private static testMethod void TestContractReviewRequestBase(){
    new messagesGardNoV10Contractreviewrequ.ContractReviewRequestBase();
    }
    private static testMethod void TestContractReviewRequestBase1(){
    new messagesGardNoV10Contractreviewrece.ContractReviewRequestBase();
    }
    private static testMethod void TestContractReviewPort(){
        gardtestdata test_rec= new gardtestdata();
        test_rec.customsettings_rec();
    new messagesGardNoV10Contractreviewrece.ContractReviewPort().ContractLOConfirmationRequest(null);
    }
    private static testMethod void TestLOCSendEmailRequest(){
        gardtestdata test_rec= new gardtestdata();
        test_rec.customsettings_rec();
    new messagesGardNoV10Contractreviewrece.ContractReviewPort().LOCSendEmailRequest(null);
    }
    private static testMethod void TestContractReviewRequest(){
        gardtestdata test_rec= new gardtestdata();
        test_rec.customsettings_rec();
    new messagesGardNoV10Contractreviewrece.ContractReviewPort().ContractReviewRequest(null);
    }
    private static testMethod void TestContractLOCPreviewRequest(){
        gardtestdata test_rec= new gardtestdata();
        test_rec.customsettings_rec();
    new messagesGardNoV10Contractreviewrece.ContractReviewPort().ContractLOCPreviewRequest(null);
    }
  /*  private static testMethod void TestContractReviewResponseBase(){
    new messagesGardNoV10Contractreviewresp.ContractReviewResponseBase();
    } */
}