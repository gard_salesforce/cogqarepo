@isTest(seeAllData=false)
public class TestBatchUpdateCompanyOnRiskFields{
    @isTest
    private static void fetchOnRiskInfoTestMethod(){
        Test.setMock(HttpCalloutMock.class, new MuleWebServiceTestMock());
        test.startTest();
        
        GardTestData gtdInstance = new GardTestData();
        gtdInstance.customsettings_rec();
        
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
        upsert vrcClient;
        
        Account accClient = TestDataGenerator.getClientAccount();
        
        /*Account accapprovedClient = new Account();
        accapprovedClient.name = 'Approved Member Test Account';
        accapprovedClient.Type = 'Customer';
        insert accapprovedClient;
        */
        Company_on_risk_information__c comOnRisks = new Company_on_risk_information__c(
                            Assured_On_Risk__c = true ,
                            Broker_On_Risk__c = true ,
                            Builders_On_Risk__c = true ,
                            Client_On_Risk__c = true ,
                            Coassured_On_Risk__c = true ,
                            Company_On_Risk__c = true ,
                            Energy_On_Risk__c = true ,
                            Insurance_Company_On_Risk__c = true ,
                            Marine_On_Risk__c = true ,
                            Original_Insured_Mutual_On_Risk__c = true ,
                            Original_Insured_Small_Craft_On_Risk__c = true ,
                            P_I_Charterers_On_Risk__c = true ,
                            P_I_MOUOnRisk__c = false ,
                            P_I_Owners_On_Risk__c = true ,
                            Partner_Id__c = '1236' ,
                            Name = 'Approved Member Test Account',
                            Partner_Type_Code__c = 5.0 ,
                            Partner_Type_Description__c = 'Test description',
                            Protected_Coassured_On_Risk__c = true ,
                            Stop_Use__c = false,
                            CRM_Id__c = String.valueOf([SELECT company_id__c FROM Account where Name = 'Customer Test Account'].company_id__c)
                        );
        insert comOnRisks ;                   
        //CompanyOnRiskInfoUpdateCtrl.fetchOnRiskInfo();
        
        BatchUpdateCompanyOnRiskFields job = new BatchUpdateCompanyOnRiskFields();
        job.start(null);
        job.execute(null,[SELECT Id, Company_id__c, Company_Status__c, P_I_Member_Flag__c ,M_E_Client_Flag__c,Client_On_Risk_Flag__c,Broker_On_Risk_Flag__c,On_Risk__c,Small_craft_client__c,
                Assured_On_Risk__c ,Builders_On_Risk__c ,Coassured_On_Risk__c ,Energy_On_Risk__c,Insurance_Company_On_Risk__c ,Marine_On_Risk__c ,
                Original_Insured_Mutual_On_Risk__c ,P_I_Charterers_On_Risk__c ,P_I_MOUOnRisk__c ,P_I_Owners_On_Risk__c,Protected_Coassured_On_Risk__c 
                FROM Account]);
        job.finish(null);
        
        Scheduler_BatchUpdateCompanyOnRiskFields m = new Scheduler_BatchUpdateCompanyOnRiskFields();
        String sch = '0 51 00 ? * * *';
        String jobID = system.schedule('Merge Job', sch, m);
        test.stopTest();
    }
}