@istest
public with sharing class TestAssociateCompaniesonEventCtrl{
    public static Account acc=new Account();
    public static Account acc1 = new Account();
    public static Contact con=new Contact();
    public static Event e;// = new Event();
    public static EventRelation evrel;
    public Static Contact con1;
    @istest static void test()
    {
        
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
        insert vrcClient ; 
        
        acc.Name='test';
        acc.Role_Client__c=true;
        acc.Active__c = true;
        acc.Company_Role__c = 'Client';
        acc.Sub_Roles__c = '';
        insert acc;
        
        
        acc1.Name='test1';
        acc1.Active__c = true;
        acc1.Role_Client__c=true;
        acc1.Company_Role__c = 'Client';
        acc1.Sub_Roles__c = '';
        insert acc1;
                
        con1 = new Contact( FirstName='tsat_1',
                                       LastName='chak',
                                       MailingCity = 'London',
                                       MailingCountry = 'United Kingdom',
                                       MailingPostalCode = 'SE1 1AE',
                                       MailingState = 'London',
                                       MailingStreet = '4 London Road',
                                       AccountId = acc.Id,
                                       Email = 'test321@gmail.com'
                                      );
        insert con1;

        
        AssociateCompaniesonEventCtrl.AccountWrapper aa=new AssociateCompaniesonEventCtrl.AccountWrapper(acc);
        ApexPages.StandardController sc = new ApexPages.StandardController(new Event()); 
        AssociateCompaniesonEventCtrl ase = new AssociateCompaniesonEventCtrl(sc);
        
        //list<AssociateCompaniesonEventCtrl.AccountWrapper> wrapaccountList= new  list<AssociateCompaniesonEventCtrl.AccountWrapper>();
        for(Account AccTest : [SELECT Company_ID__c,Name,Company_Status__c,Company_Role__c from Account where Active__c=true and (Role_Broker__c=true or Role_Client__c=true)])
        {
            //wrapaccountList.add(new AssociateCompaniesonEventCtrl.AccountWrapper(AccTest));
            ase.acclist.add(new AssociateCompaniesonEventCtrl.AccountWrapper(AccTest));
        }
        for(AssociateCompaniesonEventCtrl.AccountWrapper WATest :ase.acclist)
        {
            WATest.selected =true;   
        }
        ase.doSelectItem();
        ase.PopulateSearch();
        ase.filteredRecord();
        ase.CompanyName();
        ase.EditOption();
        ase.deleteOption();
        Integer i = ase.TotalPageNumber;
        ase.hasNext = true;
        ase.hasPrevious = true;
        ase.first();
        ase.next();
        ase.previous();
        ase.last();
        ase.pageNumber =100;
        ase.totalPageNumber = 100;
        
        ase.submit();
    } 
    
    public static Event createEvent(){  //sfedit new method added
        List<Event> evnt=new List<Event>();
        //Event 
        e = new Event();
        e.ActivityDate=system.today();
        e.CurrencyIsoCode='USD';
        e.IsAllDayEvent=false;
        e.ActivityDateTime=DateTime.newInstance(2018, 01, 18);
        e.Description='Meeting';
        e.DurationInMinutes=60;
        e.EndDateTime=DateTime.newInstance(2018, 01, 18 ,1, 0, 0);
        e.IsDuplicate__c=false;
        e.IsPrivate=false;
        e.Location='Kolkata';
        e.ShowAs='';
        e.StartDateTime=DateTime.newInstance(2018, 01, 18);
        e.Subject='Meeting'; 
        e.Type='Meeting';
        con = new Contact(   FirstName='test',
                             LastName='Client',
                             MailingCity =  'testCity' ,
                                              OtherPhone =  '12345667890' ,
                                              mobilephone =  '0987654321' ,
                                              MailingCountry =  'testCountry' ,
                                              MailingPostalCode = '1234',
                                              MailingState =  'testState' ,
                                              MailingStreet = '1 test Street',
                                              AccountId = acc.Id,
                                              Email =  'test@email.com'
                                          );
        insert con;
        e.WhatId = acc.id;
        e.whoID = con.id;
        evnt.add(e);
        evrel = new EventRelation();
        insert evnt;        
        evrel.EventId = e.id;
        evrel.RelationId = con1.id;
        evrel.isWhat = false;
        evrel.isInvitee = true;
        evrel.isParent = true;
        insert evrel;
        
        Event e1 = e.clone();
        e1.Meeting_ID__c = e.id;
        e1.WhatId = acc1.id;
        e1.WhoId = con1.id;
        insert e1;
        
        return e;
        
        
    }
    AssociateCompaniesonEventCtrl.AccountEventWrapper aa=new AssociateCompaniesonEventCtrl.AccountEventWrapper(acc,e);
}