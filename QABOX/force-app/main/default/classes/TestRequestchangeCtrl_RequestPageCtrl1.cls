//Test class for RequestChangeCtrl.
@istest(seealldata = false)
public class TestRequestchangeCtrl_RequestPageCtrl1{
    private static Gardtestdata gtdInstance;
    private static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    private static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    //Test method................................... 
    
    private static void createTestData(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        //Create test data..............................    
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers; 
        gtdInstance = new Gardtestdata();
        //gtdInstance.customsettings_rec(); 
        gtdInstance.commonrecord();
        gtdInstance.Requestchange_testrecord(); 
    }
    
    @isTest public static void TestRequestchangeCtrl_RequestPageCtrl1_1(){
        createTestData();
        //invoking methods for RequestChangeCtrl for client users......
        System.runAs(Gardtestdata.clientUser){    
            Test.startTest();
            RequestChangeCtrl rccInstance=new RequestChangeCtrl();        
            rccInstance.tonnage=1002;
            rccInstance.typeOfObj='Bulk Vessel';
            rccInstance.selectedYrBuilt='2014';
            rccInstance.selectedCover=Gardtestdata.clientAsset_1st.product_name__c; 
            rccInstance.selectedClient= Gardtestdata.clientAcc.id;
            rccInstance.loggedInAccId= Gardtestdata.clientAcc.id;
            rccInstance.selectedReqType='Request for change in terms';
            rccInstance.reqType=1; 
            rccInstance.selectedObj.add(Gardtestdata.test_object_1st.id);
            rccInstance.objMode = true;   
            rccInstance.SelectedObhName =Gardtestdata.test_object_1st.name; 
            rccInstance.effctvDate='03.01.2014';//date.valueof('2014-01-03');
            rccInstance.comment='test';               
            rccInstance.reqType=1;
            rccInstance.comment='test';
            rccInstance.selectedClient= Gardtestdata.clientAcc.id;
            rccInstance.populateCover();
            rccInstance.populateClient();
            rccInstance.reqType=1;
            rccInstance.selectedCover='P&I';
            rccInstance.populateClient();
            System.assertEquals(1,rccInstance.reqType);      
            rccInstance.populateObject();
            rccInstance.reqType=2;
            rccInstance.populateObject();
            rccInstance.populateClient();
            rccInstance.selectedClient= Gardtestdata.clientAcc.id;
            rccInstance.selectedObj.add(Gardtestdata.test_object_1st.id);
            rccInstance.objMode = false;
            rccInstance.populateCover();
            rccInstance.populatefromObject();
            rccInstance.reqType=2;
            rccInstance.populateCover();
            rccInstance.populateClient();
            rccInstance.selectedClient= Gardtestdata.clientAcc.id;
            rccInstance.strGlobalClient = Gardtestdata.clientAcc.id+';'+Gardtestdata.clientAcc.id;
            rccInstance.fetchGlobalClients();
            rccInstance.selectedObj=new List<String>();
            rccInstance.populateCover();
            rccInstance.btnClicked = 'save'; 
            rccInstance.reqType=1;
            rccInstance.SelectedObhName = null;
            rccInstance.popUpClose();
            rccInstance.btnClicked = 'new'; 
            rccInstance.reqType=2;
            rccInstance.popUpClose();
            rccInstance.reqType=3;
            rccInstance.SelectedObhName = 'test';
            rccInstance.objMode = true;
            rccInstance.popUpClose();
            
            Test.stopTest();
        }      
    }
}