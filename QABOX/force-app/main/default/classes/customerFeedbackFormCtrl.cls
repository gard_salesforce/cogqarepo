/***************************************************************** 
*    Deveployed By   :    Cognizant Technology Solution
*    Created Date    :    5/08/2018
*    Description     :    Controller for internal qualitative feedback (using Fluido objects)
******************************************************************/
public class customerFeedbackFormCtrl{
        /*
        1. fetch respective questions to put them in the header of each table.
        2. fetch respective options of each questions in the respective table cells.
        */
        //public ApexPages.StandardSetController con{get; set;}
        public String selectedProdArea{get;set;}
        public List<fluidoconnect__Question__c> questionsList{get;set;}  //Holds all relevant questions
        public List<fluidoconnect__Question_Option__c> optionsList{get;set;}  //Holds all answer Options for questions
        public List<fluidoconnect__Question_Option__c> optionsPerQuesList;
        public Map<id,List<fluidoconnect__Question_Option__c>> questionsOptionsMap;
        public List<Id> questionIdList;
        public Map<Decimal,List<SelectOption>> ansOptions{get;set;}  //Maps question sort order and selectoptions
        public List<questionsOptionsWrapper> quesOptsWrap{get;set;}
        public User loggedInUser;
        public String currentUserId;
        public Map<String,String> gchMap{get;set;}
        public Map<Id, questionsOptionsWrapper> mapHoldingSelectedRecords{get;set;}
        public boolean displayTable{get;set;}
        public fluidoconnect__Survey__c event;
        public fluidoconnect__Invitation__c eventMember;
        public List<fluidoconnect__Response__c> responseList;
        public List<fluidoconnect__Response__c> listToBeUpdated;
        public List<fluidoconnect__Response__c> responsePerCompanyList;
        public Map<String,List<fluidoconnect__Response__c>> questionResponseMap;
        public String eventId;
        public List<Customer_Feedback__c> customerFeedbackList;
        Map<Id,Customer_Feedback__c> customerFeedbackMap;
        List<Gard_Claim_Handler_Area__c> gchList; 
        String feedbackIssueArea;
        public customerFeedbackFormCtrl(){
            loggedInUser = new User();
            eventMember = new fluidoconnect__Invitation__c();
            responseList = new List<fluidoconnect__Response__c>();
            listToBeUpdated = new List<fluidoconnect__Response__c>();
            currentUserId = UserInfo.getUserId();
            loggedInUser = [SELECT id FROM User WHERE id =: currentUserId];
            system.debug('loggedInUser constructor - '+loggedInUser);
            questionIdList = new List<Id>();
            questionsList = new List<fluidoconnect__Question__c>();
            optionsList = new List<fluidoconnect__Question_Option__c>();
            optionsPerQuesList = new List<fluidoconnect__Question_Option__c>();
            questionsOptionsMap = new Map<id,List<fluidoconnect__Question_Option__c>>();
            quesOptsWrap = new List<questionsOptionsWrapper>();
            mapHoldingSelectedRecords = new Map<Id, questionsOptionsWrapper>();
            displayTable = false;
            gchList = Gard_Claim_Handler_Area__c.getall().values();
            gchMap = new Map<String,String>();
            responsePerCompanyList = new List<fluidoconnect__Response__c>();
            questionResponseMap = new Map<String,List<fluidoconnect__Response__c>>();
            eventId = Label.Customer_feedback_event_Id.substring(0,15);
            system.debug('--Event Id--'+eventId);
            feedbackIssueArea = '';
            for(Gard_Claim_Handler_Area__c GCHA:gchList){
                system.debug('--GCHA:gchList--'+GCHA);
                gchMap.put(String.valueOf(GCHA.Account_field__c),String.valueOf(GCHA.Name));
                if(GCHA.Name == 'Defence')
                    selectedProdArea = GCHA.Account_field__c;
            }
            system.debug('--get selectedProdArea-constructor-'+selectedProdArea);
            fetchQuestionsAndOptions();
        }
        //To populate the product area dorpdown
        public List<SelectOption> getCampaignArea(){
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('None','None'));
            system.debug('gchList -->'+gchList);
            for(Gard_Claim_Handler_Area__c GCHA:gchList){
                options.add(new SelectOption(GCHA.Account_field__c,GCHA.Name));
                system.debug('gch values -->'+GCHA.Account_field__c+' -- '+GCHA.Name);
            }
            return options;
        }
        //Fetch picklist values from Questions object
        /*
        public void fetchPickListValues(){
            Schema.DescribeFieldResult fieldResult = fluidoconnect__Question__c.Campaign_area__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for(Schema.PicklistEntry pickListVal:ple){
                pickListValuesList.add(pickListVal.getLabel());
            }
        }
        */
        // indicates whether there are more records before the current page set.
        /*
        public Boolean hasNext {
            get {
                return con.getHasNext();
            }
            set;
        }
        public Boolean hasPrevious {
            get {
                //return con.getHasPrevious();
                return true;
            }
            set;
        }

        // returns the page number of the current page set
        public Integer pageNumber {
            get {
                return con.getPageNumber();
            }
            set;
        }

        // returns the first page of records
        public void first() {
            con.first();
        }

        // returns the last page of records
        public void last() {
            con.last();
        }

        // returns the previous page of records
        public void previous() {
            con.previous();
        }

        // returns the next page of records
        public void next() {
            con.next();
        } */  
        /*
         public ApexPages.StandardSetController con {
            get {
                if(con == null) {
                     if(selectedProdArea != '' && selectedProdArea != null){
                         String queryStr = 'SELECT Id,Name FROM Account WHERE Client_On_Risk_Flag__c = true AND ' + selectedProdArea + ' =: currentUserId limit 100';
               
                    con = new ApexPages.StandardSetController(Database.getQueryLocator(queryStr));
                    system.debug('con***********'+con.getRecords());
                    // sets the number of records in each page set
                    
                    system.debug('con1***********'+con.getRecords());
                }
                }
                return con;
            }
            set;
        }*/
        public void fetchQuestionsAndOptions(){
            //Accounts associated with the logged in user
            List<fluidoconnect__Response__c> existingResponseList = new List<fluidoconnect__Response__c>();
            customerFeedbackList = new List<Customer_Feedback__c>();
            customerFeedbackMap = new Map<Id,Customer_Feedback__c>();
            event = new fluidoconnect__Survey__c();
            if(!Test.isRunningTest()){
                event = [SELECT Name,Id FROM fluidoconnect__Survey__c WHERE Id =: eventId Limit 1];
            }
            else{
                event = [SELECT Name,Id FROM fluidoconnect__Survey__c WHERE name = '2018 Qualitative Internal feedback' Limit 1];
                eventId = event.Id;
            }
            eventMember = new fluidoconnect__Invitation__c();
            List<Account> accountList = new List<Account>();
            questionsList = new List<fluidoconnect__Question__c>();
            quesOptsWrap = new List<questionsOptionsWrapper>();
            List<String> AccIdList = new List<String>();
            displayTable = false;
            system.debug('loggedInUser - '+loggedInUser+'-selectedProdArea-'+selectedProdArea);
            if(selectedProdArea != '' && selectedProdArea != null && selectedProdArea != '' && selectedProdArea != 'None'){
                String queryStr = 'SELECT Id,Name FROM Account WHERE Client_On_Risk_Flag__c = true AND ' + selectedProdArea + ' =: currentUserId';
                system.debug('queryStr - '+queryStr);
                accountList = database.query(queryStr); //tbd
                for(Account acc:accountList){
                    AccIdList.add(acc.Id);
                    system.debug('--own acc ids--'+acc.Id);
                }
                feedbackIssueArea = 'Qualitative Feedback on ' + gchMap.get(selectedProdArea);
                if(gchMap != null && gchMap.get(selectedProdArea) != null && gchMap.get(selectedProdArea) != '')
                    customerFeedbackList = [SELECT Id,Description__c,Company_to__c FROM  Customer_Feedback__c 
                                            WHERE Key_feedback_issue_area__c =: feedbackIssueArea AND CreatedById =: currentUserId 
                                            AND Company_to__c IN: AccIdList AND Related_event_id__c =: eventId  order by LastModifiedDate desc];  
                for(Customer_Feedback__c cf:customerFeedbackList){
                    if(!customerFeedbackMap.containsKey(cf.Company_to__c))
                        customerFeedbackMap.put(cf.Company_to__c,cf);
                    system.debug('--customer feedback ids--'+cf.Id);
                }
                system.debug('--customerFeedbackMap-size-'+customerFeedbackMap.size());
                system.debug('--customerFeedbackMap--'+customerFeedbackMap.keySet());
                //con.setPageSize(2);
                /*
                for(Account acc:(List<Account>)con.getRecords()){
                    accountList.add(acc);
                }
                */
               //accountList.addAll((List<Account>)con.getRecords());
                system.debug('accountList *******'+accountList);
                 system.debug('accountList size *******'+accountList.size());
                 //********** Find existing responses submitted *************
                existingResponseList = [SELECT Id, fluidoconnect__Question__c, Company__c, fluidoconnect__Response__c, fluidoconnect__Text_Area__c,
                                         fluidoconnect__Invitation__c, fluidoconnect__Invitation__r.fluidoconnect__User__c 
                                         FROM fluidoconnect__Response__c WHERE fluidoconnect__Invitation__r.fluidoconnect__User__c =: currentUserId 
                                         AND Company__c IN : AccIdList AND fluidoconnect__Question__r.Campaign_area__c =: gchMap.get(selectedProdArea) AND fluidoconnect__Question__r.fluidoconnect__Survey__c =: eventId
                                         order by LastModifiedDate DESC];
                System.debug('existingResponseList---'+existingResponseList.size()+'--'+existingResponseList);
             }
            //fetch questions depending on the selected product area
            if(selectedProdArea != null && selectedProdArea != '' && accountList.size() > 0){
                questionsList = [SELECT Id, fluidoconnect__Question_Description__c, fluidoconnect__Sort_Order__c, fluidoconnect__Type__c FROM fluidoconnect__Question__c WHERE Campaign_area__c =: gchMap.get(selectedProdArea) AND fluidoconnect__Survey__c =: eventId];
                for(fluidoconnect__Question__c ques:questionsList){
                    questionIdList.add(ques.Id);
                    system.debug('prod area: '+gchMap.get(selectedProdArea)+' --questions--'+ques);
                }
            }
            if(questionsList.size() > 0){
                optionsList = [SELECT Name, Id,fluidoconnect__Question__c FROM fluidoconnect__Question_Option__c WHERE fluidoconnect__Question__c IN: questionIdList];
                displayTable = true;
            }
            system.debug('optionsList--'+optionsList);
            //To map options list with the respective questions
            ansOptions = new Map<Decimal,List<SelectOption>>();
            for(fluidoconnect__Question__c ques:questionsList){
                List<SelectOption> Options = new List<SelectOption>();
                Options.add(new SelectOption('None','None'));
                for(fluidoconnect__Question_Option__c opts:optionsList){
                    if(ques.Id == opts.fluidoconnect__Question__c){
                        optionsPerQuesList.add(opts);
                        if(ques.fluidoconnect__Type__c == 'Radio (Custom)'){
                            Options.add(new SelectOption(opts.name,opts.name));
                        }
                    }
                }
                ansOptions.put(ques.fluidoconnect__Sort_Order__c,Options);
                questionsOptionsMap.put(ques.Id,optionsPerQuesList);
                system.debug('ansOptions--'+questionsOptionsMap);
            }
            //********** End of mapping questions and answers **********
            
            Set<fluidoconnect__Response__c> existingResponseSet = new Set<fluidoconnect__Response__c>();
            questionResponseMap = new Map<String,List<fluidoconnect__Response__c>>();
            for(Account acc:accountList){
                responsePerCompanyList = new List<fluidoconnect__Response__c>();
                for(fluidoconnect__Response__c res:existingResponseList){
                    System.debug('String.valueOf(acc.Id).subString(0,15)--'+String.valueOf(acc.Id).subString(0,15)+' === '+String.valueOf(res.Company__c).subString(0,15));
                    if(String.valueOf(acc.Id).subString(0,15) == String.valueOf(res.Company__c).subString(0,15)){
                        responsePerCompanyList.add(res);
                        system.debug('responsePerCompanyList adding---'+res);
                    }
                    system.debug('responsePerCompanyList size---'+responsePerCompanyList.Size());
                }
                questionResponseMap.put(String.Valueof(acc.Id).subString(0,15),responsePerCompanyList);
            }
            system.debug('questionResponseMap--'+questionResponseMap);
            // Map the accounts with its question set
            for(Account acc:(List<Account>)accountList){
                quesOptsWrap.add(new questionsOptionsWrapper(acc, questionsList,questionResponseMap, false));
                system.debug('quesOptsWrap--'+quesOptsWrap);
            }
            if(accountList.size() == 0 || questionsList.size() == 0){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'No records found for selected product area.'));
            }
        }
        //***********This method helps in holding/removing the filled data in the form while navigating from one page to next********//
       
        private void updatecustomerfeedbackMap() {
            for(questionsOptionsWrapper wrp:quesOptsWrap){
                Integer count = 0;
                for(questionsAnswers qa: wrp.quesAnsList){
                    System.debug('---qa---'+qa.answer);
                    if(qa.answer==null || qa.answer=='' || qa.answer.deleteWhitespace()=='None' || qa.answer.deleteWhitespace() == ''){
                        count++;
                    }
                    if(count >= questionsList.size())
                        wrp.isSelected = false;
                     else
                        wrp.isSelected = true;
                }
                if(wrp.isSelected){
                     mapHoldingSelectedRecords.put(wrp.Acc.id, wrp);  
                }
                if(wrp.isSelected == false && mapHoldingSelectedRecords.containsKey(wrp.Acc.id) && !Test.isRunningTest()){
                     mapHoldingSelectedRecords.remove(wrp.Acc.id);
                }
                System.debug('isSelected----'+wrp.isSelected+'--wrp--'+wrp.quesAnsList);
            }
            System.debug('mapHoldingSelectedRecords--'+mapHoldingSelectedRecords.size());
        }

        public PageReference saveFeedback(){
            updatecustomerfeedbackMap();
            List<Customer_Feedback__c> lstCFTobeInserted = new List<Customer_Feedback__c>();
            List<Customer_Feedback__c> lstCFTobeUpdated = new List<Customer_Feedback__c>();
            responseList = new List<fluidoconnect__Response__c>();
            //listToBeUpdated = new List<fluidoconnect__Response__c>();
            Set<fluidoconnect__Response__c> setToBeUpdated = new Set<fluidoconnect__Response__c>();
            /* List<String> clientAccList = new List<String>();
            for(CFFWrapper cffWrapItem : mapHoldingSelectedRecords.values()){
                //clientAccList.add(cffWrapItem.acc.Id);
                system.debug('cffWrapItem--'+cffWrapItem);
            } */
            boolean isMemberPresent = false;
            String eventMemberId;
            List <fluidoconnect__Invitation__c> memberList = new List<fluidoconnect__Invitation__c>([SELECT fluidoconnect__User__c,Id 
                                                  FROM fluidoconnect__Invitation__c WHERE fluidoconnect__Survey__c=:event.Id]);
            for(fluidoconnect__Invitation__c evMem:memberList){
                if(evMem.fluidoconnect__User__c != null && String.valueOf(evMem.fluidoconnect__User__c).subString(0,15) == currentUserId.subString(0,15)){
                    isMemberPresent = true;
                    eventMemberId = evMem.Id;
                    break;
                }
            }
            //insert event member
            if(!isMemberPresent){
                eventMember.fluidoconnect__Survey__c = event.Id;
                eventMember.fluidoconnect__User__c = currentUserId;
                eventMember.fluidoconnect__Language__c = 'en_US';
                insert eventMember;
                eventMemberId = String.valueOf(eventMember.Id).substring(0,15);
                system.debug('--if Id--'+eventMemberId);
            }
            for(questionsOptionsWrapper cffWrapItem : mapHoldingSelectedRecords.values()){
                Customer_Feedback__c custFeed = new Customer_Feedback__c();
                boolean isInsert = false;
                custFeed.Company_to__c = cffWrapItem.acc.Id;
                String cfResponse = String.ValueOf('Feedback from claim handler : '+ UserInfo.getName() + '\r\n' + '\r\n');
                if(!customerFeedbackMap.containsKey(cffWrapItem.acc.Id))
                    isInsert = true;
                system.debug('--isInsert--'+isInsert+'--=cffWrapItem.acc.Id=--'+cffWrapItem.acc.Id);
                if(questionResponseMap.get(String.valueOf(cffWrapItem.acc.Id).subString(0,15)) != null && questionResponseMap.get(String.valueOf(cffWrapItem.acc.Id).subString(0,15)).size() > 0){
                    //isInsert = false;
                    for(questionsAnswers res: cffWrapItem.quesAnsList){
                        for(fluidoconnect__Response__c response:questionResponseMap.get(String.valueOf(cffWrapItem.acc.Id).subString(0,15))){
                            if(String.valueOf(res.question.Id).subString(0,15) == String.valueOf(response.fluidoconnect__Question__c).subString(0,15)){
                                if(res.question.fluidoconnect__Type__c == 'Radio (Custom)'){
                                    response.fluidoconnect__Response__c = res.answer;
                                    system.debug('--res.question--'+res.question+'--'+res.answer);
                                }
                                else if(res.question.fluidoconnect__Type__c == 'TextArea'){
                                    response.fluidoconnect__Text_Area__c = res.answer;
                                    system.debug('--res.question--'+res.question+'--'+res.answer);
                                }
                            }
                            setToBeUpdated.add(response);
                            system.debug('--------response---------'+response);
                        }
                        cfResponse+= String.valueOf(res.question.fluidoconnect__Question_Description__c).replaceAll('(\\r|\\n)+', ' ')  + ' : ' +  res.answer + '\r\n' + '\r\n';
                    }
                    system.debug('--cfResponse--'+cfResponse);
                    
                    system.debug('--is Insert - if--'+isInsert);
                    if(isInsert){
                        custFeed.Description__c = cfResponse;
                        //custFeed.Title__c = Label.Customer_feedback_Record_Title + ' on '+gchMap.get(selectedProdArea) + ' Customer';
                        custFeed.Title__c = event.Name;
                        custFeed.Date_feedback_received__c = Date.Today();
                        custFeed.Feedback_for_Gard_Department__c = 'UWR';
                        custFeed.Source__c = 'Surveys';
                        custFeed.Type_of_feedback__c = 'Suggestion';
                        custFeed.Status__c = 'Closed';
                        custFeed.Customer_feedback_type__c = 'Open';
                        custFeed.Key_feedback_issue_area__c = feedbackIssueArea;
                        custFeed.Related_event_id__c = eventId.substring(0,15);
                        lstCFTobeInserted.add(custFeed);
                    }
                    else{
                        for(Customer_Feedback__c cf:customerFeedbackList){
                            //if(customerFeedbackMap != null && customerFeedbackMap.containsKey(cf.Company_to__c) && customerFeedbackMap.get(cf.Company_to__c) != null && customerFeedbackMap.get(cf.Company_to__c).Company_to__c == custFeed.Company_to__c && customerFeedbackMap.get(cf.Company_to__c).CreatedById == currentUserId){
                            if(cf.Company_to__c == cffWrapItem.acc.Id){
                                cf.Description__c = cfResponse;
                                cf.Title__c = event.Name;
                                lstCFTobeUpdated.add(cf);
                            }
                        }
                    }
                    system.debug('--------response-set--------'+setToBeUpdated);
                }
                else{
                    //isInsert = true;
                    for(questionsAnswers res: cffWrapItem.quesAnsList){
                        fluidoconnect__Response__c fluidoResponse = new fluidoconnect__Response__c();
                        for(fluidoconnect__Response__c response:questionResponseMap.get(String.valueOf(cffWrapItem.acc.Id).subString(0,15))){
                            if(res.question.fluidoconnect__Type__c == 'Radio (Custom)')
                                response.fluidoconnect__Response__c = res.answer;
                            else if(res.question.fluidoconnect__Type__c == 'TextArea')
                                response.fluidoconnect__Text_Area__c = res.answer;
                        }
                    
                        fluidoResponse.fluidoconnect__Question_text__c = res.question.Id;
                        fluidoResponse.fluidoconnect__Question__c = res.question.Id;
                        fluidoResponse.Company__c = cffWrapItem.acc.Id;
                        system.debug('--fluidoResponse.fluidoconnect__Question__c--'+fluidoResponse.fluidoconnect__Question__c+'-res.question-'+res.question);
                        if(res.question.fluidoconnect__Type__c == 'Radio (Custom)')
                            fluidoResponse.fluidoconnect__Response__c = res.answer;
                        else if(res.question.fluidoconnect__Type__c == 'TextArea')
                            fluidoResponse.fluidoconnect__Text_Area__c = res.answer;
                        fluidoResponse.fluidoconnect__Invitation__c = eventMemberId;
                        responseList.add(fluidoResponse);
                        system.debug('--responseList--'+responseList+'--eventMemberId--'+eventMemberId);
                        //-----------concat the strings to form the body of the feedback--------------
                        cfResponse+= String.valueOf(res.question.fluidoconnect__Question_Description__c).replaceAll('(\\r|\\n)+', ' ')  + ' : ' +  res.answer + '\r\n' + '\r\n';
                    }
                    system.debug('--is Insert - else--'+isInsert);
                    system.debug('--cfResponse--'+cfResponse);
                    
                    if(isInsert){
                        custFeed.Description__c = cfResponse;
                        //custFeed.Title__c = Label.Customer_feedback_Record_Title + ' on '+gchMap.get(selectedProdArea) + ' Customer';
                        custFeed.Title__c = event.Name;
                        custFeed.Date_feedback_received__c = Date.Today();
                        custFeed.Feedback_for_Gard_Department__c = 'UWR';
                        custFeed.Source__c = 'Surveys';
                        custFeed.Type_of_feedback__c = 'Suggestion';
                        custFeed.Status__c = 'Closed';
                        custFeed.Customer_feedback_type__c = 'Open';
                        custFeed.Key_feedback_issue_area__c = feedbackIssueArea ;
                        custFeed.Related_event_id__c = eventId.substring(0,15);
                        lstCFTobeInserted.add(custFeed);
                    }
                    else{
                        for(Customer_Feedback__c cf:customerFeedbackList){
                            //if(customerFeedbackMap != null && customerFeedbackMap.containsKey(cf.Company_to__c) && customerFeedbackMap.get(cf.Company_to__c) != null && customerFeedbackMap.get(cf.Company_to__c).Company_to__c == custFeed.Company_to__c && customerFeedbackMap.get(cf.Company_to__c).CreatedById == currentUserId){
                            if(cf.Company_to__c == cffWrapItem.acc.Id){
                                cf.Description__c = cfResponse;
                                cf.Title__c = event.Name;
                                lstCFTobeUpdated.add(cf);
                            }
                        }
                    }
                    
                }
            }
            listToBeUpdated = new List<fluidoconnect__Response__c>();
            for(fluidoconnect__Response__c res:setToBeUpdated){
                system.debug('---just like that-outside if---');
                if(!listToBeUpdated.contains(res)){
                    listToBeUpdated.add(res);
                    system.debug('---just like that--');
                }
            }
            system.debug('--------response-List--------'+listToBeUpdated);
            //insert customer feedback
            if(lstCFTobeInserted != null && lstCFTobeInserted.size() > 0)
                insert lstCFTobeInserted;
            //update customer feedback
            if(lstCFTobeUpdated != null && lstCFTobeUpdated.size() > 0)
                update lstCFTobeUpdated;
            //insert response in fluido response object from customer feedback form
            if(responseList != null && responseList.size() > 0)
                insert responseList;     
            //update existing responses
            if(listToBeUpdated != null && listToBeUpdated.size() > 0)
                update listToBeUpdated;
            fetchQuestionsAndOptions();
            return null;
        }
        public PageReference cancel() {
            PageReference pg = new PageReference('/apex/CustomerFeedbackForm');
            pg.setRedirect(true);
            return pg; 
        }
        // Wrapper class to populate the table
        public class questionsOptionsWrapper{
            public Account acc{get;set;}
            public List<customerFeedbackFormCtrl.questionsAnswers> quesAnsList{get;set;}
            public boolean isSelected{get;set;}
            public Map<String,List<fluidoconnect__Response__c>> questionResponseMap{get;set;}
            public questionsOptionsWrapper(Account Acc, List<fluidoconnect__Question__c> questList,Map<String,List<fluidoconnect__Response__c>> questionResponseMap, boolean isSelected){
                this.acc = acc;
                this.isSelected = isSelected;
                quesAnsList = new List<questionsAnswers>();
                //system.debug('--in wrapper--'+acc.Id+'--'+questionResponseMap);
                for(fluidoconnect__Question__c ques:questList){
                    //system.debug('--QUES---'+ques+'-acc id-'+String.valueOf(acc.Id).subString(0,15));
                    //system.debug('--questionResponseMap.get(String.valueOf(acc.Id).subString(0,15))--'+questionResponseMap.get(String.valueOf(acc.Id).subString(0,15)));
                    if(questionResponseMap != null && questionResponseMap.get(String.valueOf(acc.Id).subString(0,15)) != null && questionResponseMap.get(String.valueOf(acc.Id).subString(0,15)).size() > 0){
                        //system.debug('--questionResponseMap.get(String.valueOf(acc.Id).subString(0,15)).size()--'+questionResponseMap.get(String.valueOf(acc.Id).subString(0,15)).size());
                        for(fluidoconnect__Response__c response:questionResponseMap.get(String.valueOf(acc.Id).subString(0,15))){
                            //system.debug('--in Wrapper-For--');
                            if(response.fluidoconnect__Question__c == ques.Id){
                                if(ques.fluidoconnect__Type__c == 'TextArea')
                                    quesAnsList.add(new questionsAnswers(ques,response.fluidoconnect__Text_Area__c));
                                else if(ques.fluidoconnect__Type__c == 'Radio (Custom)')
                                    quesAnsList.add(new questionsAnswers(ques,response.fluidoconnect__Response__c));
                            }
                            
                        }
                    }
                    else
                        quesAnsList.add(new questionsAnswers(ques,''));
                }
            }
        }
        public class questionsAnswers{
            public fluidoconnect__Question__c question{get;set;}
            public String answer{get;set;}
            public questionsAnswers(fluidoconnect__Question__c question, String answer){
                this.question=question;
                this.answer=answer;
            }
        }
    }