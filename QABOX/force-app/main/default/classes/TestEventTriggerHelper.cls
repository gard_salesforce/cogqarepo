/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestEventTriggerHelper {
private static List<Valid_Role_Combination__c> validRoles;
      private static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    private List<Opportunity> allOpp = [SELECT ID FROM Opportunity WHERE RecordTypeName__c = 'P&I'];
    private static List<String> strAccId=new List<String>();
    private static String strOppId;
    private static User salesforceUser;
    private static Opportunity_Checklist__c testOppChecklists;
    private static String recordTypeId;
    private static Opportunity opp;
   private static void setupValidRoles() {
        ////For some reason this seems to be throwing an internal Salesforce error /
        ////System.UnexpectedException: Salesforce System Error: 1583762872-13077 (1716107030) (1716107030)
        //validRoles = (List<Valid_Role_Combination__c>)Test.loadData(Valid_Role_Combination__c.sObjectType, 'MDMValidRoles');
        ////END...
        validRoles = new List<Valid_Role_Combination__c>();
        validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 1', Sub_Role__c = 'Sub Role 11'));
        validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 1', Sub_Role__c = 'Sub Role 12'));
        validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 2', Sub_Role__c = 'Sub Role 21'));
        validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 2', Sub_Role__c = 'Sub Role 22'));
        validRoles.add(new Valid_Role_Combination__c(Role__c = 'Other', Sub_Role__c = 'Other Sub Role'));
        insert validRoles;
    }
 //create an account record
    private static void createAccount(String accName,integer i){
        //CustomSetting added by Abhirup
        
        Account acc = new Account(  Name=accName,
        BillingCity = 'Arendal',
      company_Role__c =  validRoles[i].Role__c,
       Sub_Roles__c =   validRoles[i].Sub_Role__c,
        BillingCountry = 'United Kingdom',
        BillingPostalCode = 'BS1 1AD',
        BillingState = 'Avon' ,
        BillingStreet = '1 Elmgrove Road',
       // Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id,
       // Market_Area__c = TestDataGenerator.getMarketArea().Id,
        //Responsible_CUO__c= salesforceUser.Id,
        OwnerId = userinfo.getUserId()
        );
        insert acc;
        strAccId.add(acc.Id); 
    }
 //create opportunity
        private static void createOpp(){
        Id strPIRecId =[SELECT Id FROM RecordType WHERE Name='P&I' AND sObjectType='Opportunity' ].id;
        opp = new Opportunity();
        opp.RecordTypeId = strPIRecId ;
        opp.Name = 'APEXTESTOPP001';
        opp.AccountId = strAccId[0];
        opp.StageName = 'Risk Evaluation';//Renewable Opportunity
        opp.Type = 'New Business';        
        opp.Business_Type__c = 'MOUs';
        opp.Approval_Criteria__c = 'Self Approval';
        opp.Sales_Channel__c = 'Direct';
        opp.CloseDate = Date.Today();
        opp.Confirm_not_on_sanction_list__c = true;
        opp.Amount = 100;
        insert opp;
        strOppId = opp.Id;
        recordTypeId = strPIRecId ;
    }
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
     /*   createUser();
        system.runAs(salesforceUser)
        {
            AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
        insert vrcBroker ;
        }*/
        setupValidRoles();
        createAccount('APEXTESTACC001',1);
        createAccount('testAccount',2);
        createOpp();
        List<Event>EventList=new List<Event>();
        EventList.add(new event(type='Meeting',
                                 Subject='TestMeeting',
                                 whatId=strAccId[0],
                                 StartDateTime=system.now()+(1/24), 
                                 Location__c='External',
                                 OwnerId=userinfo.getuserid(),
                                 CurrencyIsoCode='USD',
                                 DurationInMinutes=60));
          EventList.add(new event(type='Meeting',Subject='TestMeeting',whatId=strOppId,StartDateTime=system.now()+(1/24),   Location__c='External',OwnerId=userinfo.getuserid(),CurrencyIsoCode='USD',DurationInMinutes=60));
        insert EventList;
       List< Meeting_Minute__c> minutesList= new List<Meeting_Minute__c>();
               for(Event ev:EventList){
            minutesList.add(new Meeting_Minute__c(
                                                    Name= 'test Minutes', 
                                                   Event_Id__c=ev.id, 
                                                   Event_Subject__c=ev.subject, 
                                                   Event_StartDateTime__c=ev.StartDateTime, 
                                                   Event_Location__c=ev.location__c, 
                                                   Event_EndDateTime__c=ev.endDateTime,
                                                   Distributed__c=false
                                                    
                                               ));
            ev.description='meeting minutes added';
            ev.Subject='meeting Updated';
            if(ev.whatId.getsObjectType() == Account.sObjectType){
            ev.whatId=strAccId[1];
            }
        }
        insert minutesList;
        update eventList;
          List<EventRelation> evtAtt=new List<EventRelation>([SELECT RelationId FROM EventRelation WHERE EventId IN:eventList ]);
          if(evtAtt.size()>1){
            delete evtAtt;
          }
        delete eventList;
    }
    
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
   
        setupValidRoles();
        createAccount('APEXTESTACC001',1);
        createAccount('testAccount',2);
        createOpp();
        List<Event>EventList=new List<Event>();
        EventList.add(new event(type='Meeting',
                                 Subject='TestMeeting',
                                 whatId=strAccId[0],
                                 StartDateTime=system.now()+(1/24), 
                                 Location__c='External',
                                 OwnerId=userinfo.getuserid(),
                                 CurrencyIsoCode='USD',
                                 DurationInMinutes=60));
          EventList.add(new event(type='Meeting',Subject='TestMeeting',whatId=strOppId,StartDateTime=system.now()+(1/24),   Location__c='External',OwnerId=userinfo.getuserid(),CurrencyIsoCode='USD',DurationInMinutes=60));
        insert EventList;
       List<String>eventId=new List<string>();
        for(Event ev:EventList){
           eventId.add(ev.id);
            ev.description='meeting minutes added';
            ev.Subject='meeting Updated';
            
        }
        List<task> taskList= new List<task>([SELECT id, subject,WhatId,status, activityDate from Task where Meeting_ID__c in: eventId]);
     
       if (taskList.size()>0){
        delete taskList;
       }
          update eventList;
        
    }
    
}