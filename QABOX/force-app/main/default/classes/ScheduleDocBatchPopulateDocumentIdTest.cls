@isTest
public class ScheduleDocBatchPopulateDocumentIdTest {
    @isTest
    public static void testSendCaseRelatedFiles() {
        test.startTest();
        Map<Id, String> fileCorelIdMap = new Map<Id, String>();
        Mule_Web_Service__c mws = new Mule_Web_Service__c();
        mws.Name = 'M-Files endpoint for document storing';
        mws.Endpoint_Url__c = 'http://test.com';
        insert mws;
        
        ContentVersion testContentInsert =new ContentVersion(); 
        testContentInsert.ContentURL='http://test.com'; 
        testContentInsert.Title ='Test'; 
        testContentInsert.Description ='MFiles Description'; 
        insert testContentInsert; 
		
        fileCorelIdMap.put(testContentInsert.Id, 'abcd');
        ScheduleDocBatchPopulateDocumentId schBatch = new ScheduleDocBatchPopulateDocumentId('abcd', fileCorelIdMap, 'Schedule to update Document Id'); 
        String sch ='0 48 * * * ?'; 
        System.schedule('Schedule to update Document Id', sch,schBatch);
        test.stopTest();
    }
}