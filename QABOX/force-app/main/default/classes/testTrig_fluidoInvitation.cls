/**
 * SOC 9/9/13 - Test trigger on fluidoconnect__Invitiation__c which prevents
 * additional members being added to a publish event by anyone other than 
 * the event owner or event organiser
 */
@isTest
private class testTrig_fluidoInvitation {

    static User eventOwner;
    static User eventOrganiser;
    static User unrelatedUser;
    static Profile standardUserProfile = [SELECT Id FROM Profile WHERE Name='Gard Standard User (UWR)' LIMIT 1];
    static fluidoconnect__Survey__c testEvent;
    static Contact[] contacts;

    static void setupUsers()
    {
        eventOwner = new User(  FirstName='Event', 
                                LastName='Owner',
                                Email='eventowner@gard.no.test',
                                UserName='eventowner@gard.no.test',
                                Alias='eOwner',
                                CommunityNickname='eOwner',
                                TimeZoneSidKey='Europe/Amsterdam',
                                LocaleSidKey='no_NO',
                                EmailEncodingKey='ISO-8859-1',
                                LanguageLocaleKey='en_US',
                                ProfileId=standardUserProfile.Id);
        insert eventOwner;
        
        eventOrganiser = new User(  FirstName='Event', 
                                LastName='Organiser',
                                Email='eventorganiser@gard.no.test',
                                UserName='eventorganiser@gard.no.test',
                                Alias='eOrg',
                                CommunityNickname='eOrg',
                                TimeZoneSidKey='Europe/Amsterdam',
                                LocaleSidKey='no_NO',
                                EmailEncodingKey='ISO-8859-1',
                                LanguageLocaleKey='en_US',
                                ProfileId=standardUserProfile.Id);
                                
        insert eventOrganiser;
        
        unrelatedUser = new User(   FirstName='Unrelated', 
                                LastName='User',
                                Email='unrelateduser@gard.no.test',
                                UserName='unrelateduser@gard.no.test',
                                Alias='unrelUsr',
                                CommunityNickname='unrelUsr',
                                TimeZoneSidKey='Europe/Amsterdam',
                                LocaleSidKey='no_NO',
                                EmailEncodingKey='ISO-8859-1',
                                LanguageLocaleKey='en_US',
                                ProfileId=standardUserProfile.Id);
                                
        insert unrelatedUser;
    }
    
    static void setupEvent()
    {
        testEvent = new fluidoconnect__Survey__c(   Name='Test Event',
                                                    Event_Start_Date__c = Date.Today(),
                                                    fluidoconnect__Event_Owner__c = eventOwner.Id,
                                                    fluidoconnect__Event_Organiser__c = eventOrganiser.Id,
                                                    fluidoconnect__Status__c='Draft');
        insert testEvent;
    }
    
    static void setupContacts()
    {
        Account testAccount = new Account(Name='Test');
        insert testAccount;
        
        //Contacts are added as invitees
        contacts = new Contact[]{
            new Contact(FirstName='Test', LastName='TestOne', AccountId=testAccount.Id),
            new Contact(FirstName='Test', LastName='TestTwo', AccountId=testAccount.Id),
            new Contact(FirstName='Test', LastName='TestThree', AccountId=testAccount.Id),
            new Contact(FirstName='Test', LastName='TestFour', AccountId=testAccount.Id),
            new Contact(FirstName='Test', LastName='TestFive', AccountId=testAccount.Id),
            new Contact(FirstName='Test', LastName='TestSix', AccountId=testAccount.Id)
            };
        insert contacts;
    }
    
    static void addContactToEvent(Contact conToAdd)
    {
        system.debug('Adding contact ' + conToAdd.Id + ' as user ' + Userinfo.getUserId());
        try
        {
            fluidoconnect__Invitation__c newInvitee = new fluidoconnect__Invitation__c();
            newInvitee.fluidoconnect__Contact__c = conToAdd.Id;
            newInvitee.fluidoconnect__Survey__c = testEvent.Id;
            insert newInvitee;
        }
        catch(Exception e)
        {
            
        }
    }

    static testMethod void unitTest() {
        
        fluidoconnect__Invitation__c[] invites;
        
        setupUsers();
        setupContacts();
        setupEvent();
        
        testEvent.fluidoconnect__Status__c = 'Open';
        update testEvent;
        
        system.runas(eventOwner)
        {
            addContactToEvent(contacts[0]);
        }
        system.runas(eventOrganiser)
        {
            addContactToEvent(contacts[1]);
        }
        system.runas(unrelatedUser)
        {
            addContactToEvent(contacts[2]);
        }
        
        //While status is Open all users should be able to add
        invites = [SELECT Id FROM fluidoconnect__Invitation__c WHERE fluidoconnect__Survey__c = :testEvent.Id];
        system.assertEquals(invites.size(), 3);
        
        system.runas(unrelatedUser)
        {
            try
            {
                testEvent.Invitations_Locked__c = true;
                update testEvent;
            }
            catch(exception e)
            {
                
            }
        }
       
        //Unrelated user shouldn't have been allowed to save change
        testEvent = [SELECT Id, Invitations_Locked__c FROM fluidoconnect__Survey__c WHERE Id=:testEvent.Id];
        system.assertEquals(testEvent.Invitations_Locked__c, false);
        
        System.runas(eventOwner)
        {
            try
            {
                testEvent.Invitations_Locked__c = true;
                update testEvent;
            }
            catch(exception e)
            {
                
            }
        }
       
        //Event owner should have been allowed to save change
        testEvent = [SELECT Id, Invitations_Locked__c FROM fluidoconnect__Survey__c WHERE Id=:testEvent.Id];
        system.assertEquals(testEvent.Invitations_Locked__c, true);
        
        system.runas(eventOwner)
        {
            addContactToEvent(contacts[3]);
        }
        system.runas(eventOrganiser)
        {
            addContactToEvent(contacts[4]);
        }
        system.runas(unrelatedUser)
        {
            addContactToEvent(contacts[5]);
        }
       
        //While status is Published only owner/organiser should be able to add
        invites = [SELECT Id FROM fluidoconnect__Invitation__c WHERE fluidoconnect__Survey__c = :testEvent.Id];
        system.assertEquals(invites.size(), 5);
        
        fluidoconnect__Invitation__c[] contactInvitedByUnrelatedUser = [SELECT Id FROM fluidoconnect__Invitation__c WHERE fluidoconnect__Contact__c = :contacts[5].Id AND fluidoconnect__Survey__c = :testEvent.Id];
        system.assertEquals(contactInvitedByUnrelatedUser.size(), 0);
        
    }
}