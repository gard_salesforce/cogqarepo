/******************************************************************************************************
* Class Name: showCaseRelatedFilesController
* Created By: CTS
* Date: 17/04/2020
* Description: Used to show the case related files to send to MFiles
*******************************************************************************************************
*  Version      Developer           Date            Description
=======================================================================================================
*  1.0          Anju                17/04/2020      Created
*  1.1          Nagarjuna Kaipu     31/03/2021      SF-6268 Case Documents Storing - Filter SF files 
*  1.2          Arpan Muhuri        12/08/2021      SF-6437 Have a link/button that takes you from the doc/email in SF to the doc stored in M-files

********************************************************************************************************/
public with sharing class showCaseRelatedFilesController {
    @AuraEnabled
    public static String checkMadatoryFields(id caseId){
        String isReady = 'false';
        Case selectedCase = new case();
        selectedCase = [SELECT Category__c,Policy_Year__c FROM Case where Id =: caseId LIMIt 1];
        if(selectedCase.Category__c != null && selectedCase.Policy_Year__c != null){
            isReady = 'true';
        }
        //system.debug('--isReady--'+isReady);
        return isReady;
    }
    @AuraEnabled
    public static void sendCaseFiles(id caseId, List<Id> caseFileIds){
        //system.debug('--Files received--'+caseFileIds);
        SendCaseRelatedFilesWebService.initiateRestCallOut(caseId,caseFileIds);
    }
    
    //JSON approach below
    @AuraEnabled
    public static String fetchCaseFiles(Id caseId, String action) {
        List<Id> linkedEntityIdList = new List<Id>();
        //String caseNumber = '';
        Map<Id,String> emlIdSubjectMap = new Map<Id,String>();
        Map<Id,Id> conDocIdMap = new Map<Id,Id>();
        List<Id> conDocIdList = new List<id>();
        List<Id> emailConDocIdList = new List<id>();
        List<JSONOutput> JSONOutputs = new List<JSONOutput>();
        String queryStr = 'SELECT id, Subject, FromAddress, ToAddress,parent.casenumber from EmailMessage where parentId =: caseId order by createddate desc';
        if(action != 'cloneCase')
            linkedEntityIdList.add(caseId);
        //system.debug('case id :' +caseId);
        //system.debug('queryStr--'+queryStr);
        for(EmailMessage em:(List<EmailMessage>)database.query(queryStr)){//[SELECT id, Subject, FromAddress, ToAddress from EmailMessage where parentId =: caseId order by createddate desc]){
            linkedEntityIdList.add(em.Id);
            //caseNumber = em.parent.casenumber;
            if(action != 'cloneCase')
                emlIdSubjectMap.put(em.Id,em.Subject);
            if(action == 'cloneCase'){
                JSONOutput js = new JSONOutput();
                js.recordId = em.id;
                js.fileType = 'Email';
                js.Title = em.Subject;
                js.fromAdd = em.FromAddress;
                js.conDocId = null;
                js.toAdd = em.ToAddress;
                js.documentId = ''; //SF-6268 added
                JSONOutputs.add(js);
            }
        }
        
        for(ContentDocumentLink cdl:[SELECT ContentDocumentId,LinkedEntityId,Id FROM ContentDocumentLink WHERE LinkedEntityId IN: linkedEntityIdList]){ //IN: caseIdList]){
            if(action == 'cloneCase')
                emailConDocIdList.add(cdl.ContentDocumentId);
            else{
                conDocIdList.add(cdl.ContentDocumentId);
                conDocIdMap.put(cdl.ContentDocumentId,cdl.LinkedEntityId);
            }
        }
        if(action == 'cloneCase'){
            for(ContentDocumentLink cdl:[SELECT ContentDocumentId,Id FROM ContentDocumentLink WHERE LinkedEntityId =: caseId]){ //IN: caseIdList]){
                if(emailConDocIdList != null && (!emailConDocIdList.contains(cdl.ContentDocumentId)))
                    conDocIdList.add(cdl.ContentDocumentId);
            }
        }
        //system.debug('conDocIdList--'+conDocIdList);
        for(ContentVersion cv : [SELECT Id, Title, ContentDocumentId, description FROM ContentVersion  WHERE ContentDocumentId IN : conDocIdList AND ContentSize < 3400000 order by createddate desc]){  //SF-6143 : ContentSize added in Where clause
            JSONOutput js = new JSONOutput();
            //populate the fields for converted email messages
            String[] emlProperties;
            String preDescription = cv.Description; //SF-6268 added
            if(cv.Description != null && cv.Description != '' && cv.Description.contains('#'))
                emlProperties = cv.Description.split('#');
            //system.debug('emlProperties:'+emlProperties);
            //system.debug('cv.Description:'+ cv.Description);
            if(action != 'cloneCase' && emlProperties != null && emlProperties.size() > 4 && linkedEntityIdList.contains(String.ValueOf(emlProperties[4]).split(':')[1])){
                js.fileType = 'Email';
                js.toAdd = emlProperties[0].substringAfter(':');
                js.fromAdd = emlProperties[1].substringAfter(':');
                js.fileCount = emlProperties[2].substringAfter(':');
                js.msgDate = emlProperties[3].substringAfter(':');
                //js.fileSource = caseNumber;
            }
            
            //Ends
            else{
                js.toAdd = '-';
                js.fromAdd = '-';
                js.fileCount = '-';
                js.msgDate = '-';
                js.fileType = 'Attachment';
                /* if(action != 'cloneCase'){
if(conDocIdMap != null && conDocIdMap.size() > 0 && conDocIdMap.containsKey(cv.ContentDocumentId) && emlIdSubjectMap.containsKey(conDocIdMap.get(cv.ContentDocumentId)) )
js.fileSource = emlIdSubjectMap.get(conDocIdMap.get(cv.ContentDocumentId));
else
js.fileSource = caseNumber;
} */
            }
            js.recordId = string.valueOf(cv.id).subString(0,15);
            js.Title = cv.Title;
            js.conDocId = string.valueOf(cv.ContentDocumentId).subString(0,15);
            //SF-6268 added
            if(String.isNotEmpty(preDescription)){
                if(preDescription.startsWith('MFiles')){
                    js.documentId = preDescription.substringBetween('MFilesDocumentId(',')');
                    js.MFilesLink = Label.MFiles_doc_link_for_case_files + js.documentId;    //SF-6437
                }
                else{
                    js.documentId = '';  
                    js.MFilesLink = '';     //SF-6437
                }
            }
            else{
                js.documentId = '';
                js.MFilesLink = '';    //SF-6437
            }
            JSONOutputs.add(js);
        }
        //system.debug('Table data : '+JSONOutputs+' Serialized data :'+JSON.serialize(JSONOutputs));
        //system.debug('Serialized JSON : '+JSON.serialize(JSONOutputs));
        return JSON.serialize(JSONOutputs);
    }
    public class JSONOutput{
        Id recordId;
        String Title;
        String fromAdd;
        String toAdd;
        String fileType;
        Id conDocId;
        String fileCount;
        String msgDate;
        String documentId;
        String MFilesLink;    //SF-6437
        //String fileSource;
    }
    public class JSONOutputCloneCase{
        @AuraEnabled public Id recordId;
        @AuraEnabled public Boolean hasError;
    }
    @AuraEnabled
    public static JSONOutputCloneCase cloneCase(Id caseId, List<Id> conDocumentIdList, List<Id> selectedEmailMsgsIdList){
        try{
            JSONOutputCloneCase JSONOut = new JSONOutputCloneCase();
            Id cloneCaseId = CaseCloneController.cloneCaseWithRelatedRecords(caseId, conDocumentIdList, selectedEmailMsgsIdList);
            JSONOut.recordId = cloneCaseId;
            JSONOut.hasError = false;
            for(emailMessage em:[SELECT htmlBody FROM emailMessage WHERE Id IN: selectedEmailMsgsIdList]){
                if(!String.isBlank(em.htmlBody) && em.htmlBody.length() > 131071){
                    JSONOut.hasError = true;
                    break;
                }
            }
            return JSONOut;
        }catch (Exception err) {
          if ( String.isNotBlank( err.getMessage() ) && err.getMessage().contains( 'error:' ) ) {
                throw new AuraHandledException(err.getMessage().split('error:')[1].split(':')[0] + '.');
            } else {
                throw new AuraHandledException(err.getMessage());
            }
        } 
    }
    //Added for SF-6437 : To fetch files that contains document id
    @AuraEnabled
    public static String fetchCaseFilesSentToMFiles(Id caseId){
        String JSONRecords = showCaseRelatedFilesController.fetchCaseFiles(caseId,'');
        //system.debug('JSONRecords--MFiles--'+JSONRecords);
        List<JSONOutput> JSONOutputs = new List<JSONOutput>();
        JSONOutputs = (List<JSONOutput>)JSON.deserialize(JSONRecords,List<JSONOutput>.class);
        List<JSONOutput> JSONOutputsFiltered = new List<JSONOutput>();
        for(JSONOutput js:JSONOutputs){
            if(js.documentId != null && js.documentId != '')
                JSONOutputsFiltered.add(js);
        }
        return JSON.serialize(JSONOutputsFiltered) ;
    }
}