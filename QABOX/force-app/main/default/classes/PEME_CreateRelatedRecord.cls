//Created for SF-4761 and SF-4762 for lightning migration
public with sharing class PEME_CreateRelatedRecord{
    //Constants
    private static final String strAccount = 'Account';
    private static final String strContact = 'Contact';
    private static final String strDebitNote = 'PEME_Debit_note_detail__c';
    private static final String strManningAgent = 'PEME_Manning_Agent__c';
    //State Variables
    private static Id callerRecordId;
    private static String sObjectToBeCreated;
    private static String callerRecordSObjectName;
    private static String fieldsToFetch;
    private static String callerFetchingQuery;
    private static String callerCountryFetchingQuery;
    private static SObject callingRecord;
    
    //Driver method for relaying record
    @AuraEnabled
    public static SObject getParentRecord(Id callerRecordId,String sObjectToBeCreated){
        PEME_CreateRelatedRecord.callerRecordId = callerRecordId;
        PEME_CreateRelatedRecord.sObjectToBeCreated = sObjectToBeCreated;
        callerRecordSObjectName = (callerRecordId.getSobjectType()+'');
        
        System.debug('sfEdit callerRecordId/sObjectToBeCreated/callerRecordSObjectName - '+callerRecordId+'/'+sObjectToBeCreated+'/'+callerRecordSObjectName);
        createQueries();
        System.debug('sfEdit created callerFetchingQuery - '+callerFetchingQuery);
        System.debug('sfEdit created callerCountryFetchingQuery - '+callerCountryFetchingQuery);
        getCallerRecord();
        System.debug('sfEdit fetched callingRecord - '+callingRecord);
        
        return callingRecord;
    }
    
    //Method to create query for retreiving calling Debit_Note/Manning_Agent record's field values
    private static void createQueries(){
        if(callerRecordSObjectName == strDebitNote){
            if(sObjectToBeCreated == strAccount){
                fieldsToFetch = 'Id,Requested_Address__c,Requested_Address_Line_2__c,Requested_Address_Line_3__c,Requested_Address_Line_4__c,Requested_City__c,Requested_State__c,Requested_Zip_Code__c,Requested_Country__c,Draft_Company_Name__c';
                callerCountryFetchingQuery = 'SELECT Id FROM Country__c WHERE Country_Name__c = \'';
            }
        }else if(callerRecordSObjectName == strManningAgent){
            if(sObjectToBeCreated == strAccount){
                fieldsToFetch = 'Id,Company_Name__c,Requested_Address__c,Requested_Address_Line_2__c,Requested_Address_Line_3__c,Requested_Address_Line_4__c,Requested_City__c,Requested_State__c,Requested_Zip_Code__c,Requested_Country__c,Requested_Email__c,Requested_Phone__c';
                callerCountryFetchingQuery = 'SELECT Id FROM Country__c WHERE Country_Name__c = \'';
            }else if(sObjectToBeCreated == strContact){
                fieldsToFetch = 'Id,Requested_Contact_Person__c,Requested_Email__c,Requested_Phone__c';
            }
        }
        callerFetchingQuery = 'SELECT '+fieldsToFetch+' FROM '+callerRecordSObjectName+' WHERE Id =\''+callerRecordId+'\'';
    }
    
    //Method fetches the calling Debit_Note/Manning_Agent record's field values
    private static void getCallerRecord(){
        try{
            for(SObject anSObject : Database.query(callerFetchingQuery)){
                callingRecord = anSObject;
                if(sObjectToBeCreated == strAccount){
                    //populate country field of new account
                    String countryName = (callingRecord.get('Requested_Country__c')+'\'');
                    if(Test.isRunningTest()) countryName = GardTestData.country.Country_Name__c;
                    List<Country__c> parentsCountries = Database.query(callerCountryFetchingQuery+countryName);
                    if(parentsCountries.size() > 0){//if callingRecord's requested country have some valid country name
                        callingRecord.put('Requested_Country__c',parentsCountries[0].Id);
                    }else{
                        callingRecord.put('Requested_Country__c',null);
                    }
                    /*
                    for(Country__c parentsCountry : Database.query(callerCountryFetchingQuery+callingRecord.get('Requested_Country__c')+'\'')){
                        callersCountryId = parentsCountry.Id;
                        break;//it is supposed to run only once
                    }
                    callingRecord.put('Requested_Country__c',callersCountryId);
					*/
                }
                break;//it is supposed to run only once
            }
        }catch(Exception ex){
            System.debug('Exception caught @ PEME_CreateRelatedCompany.getParentRecord - '+ex);
        }
    }
}