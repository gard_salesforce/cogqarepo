//Test class for ShowClaimDetailsCtrl.
@isTest
Class TestShowClaimDetailsCtrl
{
    public static Document_Metadata__c ViewDoc;
    public static Document_Metadata__c ViewDoc1;
    public static List<Document_Metadata__c> Doclist = new List<Document_Metadata__c>();
    public static Claim_Payment_history__c cph;
    //Test method.........................................................................
    
    public static void coverTestData(){ 
        //test.startTest();
        // Create Test Data...................................................................  
        GardTestData gdInstance = new GardTestData();
        gdInstance.commonRecord(); 
        ClaimPaymentReserveHistoryTabs__c claimpaymentReserveHistTab = new ClaimPaymentReserveHistoryTabs__c(name='value',Enable_Payment__c =true,Enable_Reserve__c = true);
        insert claimpaymentReserveHistTab;
        ViewDoc= new Document_Metadata__c(Client__c= GardTestData.clientAcc.id,
                                          Name='xxyy',
                                          Document_Type__c='Type 1',
                                          Document_Metadata_External_ID__c = 'abc123',
                                          Last_Updated__c=Date.valueOF('2014-01-01'),
                                          Document_Source__c = 'CLAIMS',
                                          Document_Type_Description__c = 'test'
                                         );
        Doclist.add(ViewDoc);
        
        ViewDoc1= new Document_Metadata__c(Client__c= GardTestData.clientAcc.id,
                                           Name='xxyy',
                                           Document_Type__c='Type 1',
                                           Document_Metadata_External_ID__c = 'abc124',
                                           Last_Updated__c=Date.valueOF('2014-01-02'),
                                           Document_Source__c = 'CLAIMS'
                                          );
        Doclist.add(ViewDoc1);
        insert Doclist;
        System.assertequals(Doclist.size()>0,true,true);
        
        GardTestData.brokerClaim.guid__c = '8ce8ad79-a6ex-1837-9e12';
            update GardTestData.brokerclaim;
        CommonVariables__c cvar = new CommonVariables__c(name = 'DefaultProfilePic', value__c ='/mygard/profilephoto/005/T');
        insert cvar;
        DocumentMetadata_Junction_object__c junc_object_broker = new DocumentMetadata_Junction_object__c(Agreement__c= GardTestData.brokercontract_1st.id,
                                                                                                        // Claim__c= GardTestData.brokerClaim.id, 
                                                                                                         Cover__c=GardTestData.brokerAsset_1st.id,
                                                                                                         Document_Metadata__c=ViewDoc.id,
                                                                                                         External_ID__c='2asas123',
                                                                                                         Name='test record',
                                                                                                         Object__c= GardTestData.test_object_1st.id
                                                                                                        );
        cph = new Claim_Payment_history__c(Related_claim_Id__c = GardTestData.brokerClaim.Id,
                                           CLAIM_INCURRED_GROSS_USD__c = 10.00,
                                           CLAIM_PAYMENT_GROSS_ORG__c = 10.00,
                                           CLAIM_PAYMENT_GROSS_USD__c = 10.00,
                                           Claim_Transaction_Id__c = 'CPH-100',
                                           SUB_CLAIM_DETAIL__c = 'test sub claim details'
                                           );
        insert cph ;
    //test.stopTest();
    }
    public static testmethod void brokercover(){ 
        coverTestData();      
        Test.startTest();
        System.runAs(GardTestData.brokerUser){
            
            Pagereference pge = page.exportall;
            system.debug('GardTestData.brokerClaim.guid__c--GG--'+GardTestData.brokerClaim.guid__c);
            pge.getparameters().put('claimid',GardTestData.brokerClaim.guid__c);
            test.setcurrentpage(pge);
            //case csereference = [SELECT ID,contract_for_review__r.Id,contract_for_review__r.client__c FROM Claim__c WHERE guid__c =: GardTestData.brokerCase.guid__c];
            //system.debug('csereference--GG--'+csereference);
            ShowClaimDetailsCtrl showclaim = new ShowClaimDetailsCtrl();
            showclaim.getData();
            showclaim.claimId = GardTestData.brokerClaim.id;// client_Case1.id;
            showclaim.claimData = GardTestData.brokerClaim;// client_Case1.id; 
            system.debug('******** GardTestData.brokerClaim : '+ GardTestData.brokerClaim.Claims_handler__c);
            system.debug('******** showclaim.claimData : '+ showclaim.claimData.ClaimHandler_ContactId__c);            
            showclaim.fetchClaimData();
            showclaim.getSortDirection();
            string value ='ASC';
            showclaim.lstDocuments.add(ViewDoc);
            showclaim.setSortDirection(value);
            showclaim.getData();
            showclaim.claimsHandlerId = GardTestData.brokerUser.id+'#theHome:pageFrame:feedbackPopUpSection1:j_id161:3:cldId';
            showclaim.showClaimsHandler();
            showclaim.exportToExcelForClaimDet();
            showclaim.SaveComments();
            showclaim.currentCompanyId = GardTestData.brokerAcc.Id;
            showclaim.displayDoc();
            showclaim.currentCompanyId = [SELECT Id,company_id__c FROM Account WHERE Id =: GardTestData.brokerAcc.Id].company_id__c ;
            showclaim.pdf = 'test pdf';
            showclaim.displayDoc();
            showclaim.isClaimsHandler = false;            
            showclaim.goToClaimDocument();
            showclaim.createFavourites();
            showclaim.fetchFavourites();
            showclaim.deleteFavourites();
            showclaim.manageDocument();
            showclaim.createFavourites();
            showclaim.goToManageClaimsPage();
            Pagereference pge1 = page.ClaimsDetailSubClaim;             
            pge1.getparameters().put('claimType','Cargo');      
            test.setcurrentpage(pge1);             
            showclaim.goToSubClaim();
            ShowClaimDetailsCtrl.getUserName();
            showclaim.goToClaimDocument();
            showclaim.cloePopUpSuccess();
            showclaim.manageDocument();
            showclaim.cloePopUpSuccess();
            ShowClaimDetailsCtrl.getUserName();
            showclaim.pageType = 'DOCUMENT';
            showclaim.goToManageClaimsPage();
            showclaim.pageType ='SUB_CLAIM';
            showclaim.goToManageClaimsPage();
            showclaim.pageType = 'REIMBURSEMENT';
            showclaim.goToManageClaimsPage();
            Boolean hnext=showclaim.hasNext;
            Boolean hprev=showclaim.hasPrevious;
            Integer pNumber=showclaim.pageNumber;
            showclaim.previous();
            showclaim.next();
            showclaim.setpageNumber();
            showclaim.last();
            showclaim.first();
            showclaim.clearOptionsClaimsDocument();
            showclaim.isClaimsHandler=true;
            //showclaim.downloadVcardClaimDetails();
            showclaim.downloadVcardSubClaimDetails();
            showclaim.getDocTypeDescList();
            showclaim.sorePhotoData(); 
            showclaim.showClaimDetails();
            showclaim.showClaimReserveHistory();
            /*
            showclaim.ViewData();       
            showclaim.getSortDirectionHistory();
            showclaim.setSortDirectionHistory('value');
            showclaim.showClaimPaymentHistory();
            showclaim.hasNextHistory=true;  
            showclaim.hasPreviousHistory=true;
            showclaim.hasNext=true;
            showclaim.hasPrevious=true;
            showclaim.navigate();       
            showclaim.firstHistory();       
            showclaim.previousHistory(); 
            
            showclaim.lastHistory();        
            showclaim.nextHistory();
            showclaim.exportToExcelReserve();
            showclaim.exportToExcelPayment();
            showclaim.noOfReserveRecord=10;
            showclaim.check();
            showclaim.firstRecord();
            showclaim.lastRecord();
            showclaim.nextRecord();
            showclaim.previousRecord();
            showclaim.setPageNumberReserve();
            showclaim.setpageNumberHistory1();
            ShowClaimDetailsCtrl.getGenerateTime();
            ShowClaimDetailsCtrl.getGenerateTimeStamp();
            */
        }  
        Test.stopTest();
    }
    public static testmethod void Clientcover(){ 
        coverTestData();
        
        System.runAs(GardTestData.clientUser){
            Test.startTest();
            Pagereference pge2 = page.exportall;       
            pge2.getparameters().put('claimid',GardTestData.brokerClaim.guid__c);
            test.setcurrentpage(pge2);
            ShowClaimDetailsCtrl showclaim = new ShowClaimDetailsCtrl();
            showclaim.fetchClaimData();
            showclaim.selectedDocType.add('test'); 
            showclaim.getReserveData();
            showclaim.populateClaimHandlerFromMember();
            showclaim.printClaimReserveHistory();
            showclaim.printSubClaim();
            showclaim.printClaimsDetailDocument();
            showclaim.printClaimDetails();
            Test.stopTest();
        }  
    }
    public static testmethod void brokercover2(){ 
        coverTestData();      
        System.runAs(GardTestData.brokerUser){
            Test.startTest();
            Pagereference pge = page.exportall;
            system.debug('GardTestData.brokerClaim.guid__c--GG--'+GardTestData.brokerClaim.guid__c);
            pge.getparameters().put('claimid',GardTestData.brokerClaim.guid__c);
            test.setcurrentpage(pge);
            //case csereference = [SELECT ID,contract_for_review__r.Id,contract_for_review__r.client__c FROM Claim__c WHERE guid__c =: GardTestData.brokerCase.guid__c];
            //system.debug('csereference--GG--'+csereference);
            ShowClaimDetailsCtrl showclaim = new ShowClaimDetailsCtrl();
            showclaim.getData();
            showclaim.claimId = GardTestData.brokerClaim.id;// client_Case1.id;
            showclaim.claimData = GardTestData.brokerClaim;// client_Case1.id; 
            system.debug('******** GardTestData.brokerClaim : '+ GardTestData.brokerClaim.Claims_handler__c);
            system.debug('******** showclaim.claimData : '+ showclaim.claimData.ClaimHandler_ContactId__c);            
            showclaim.fetchClaimData();
            showclaim.ViewData();
            showclaim.showClaimPaymentHistory();
            showclaim.getSortDirectionHistory();
            showclaim.setSortDirectionHistory('value');
            showclaim.showClaimPaymentHistory();
            showclaim.hasNextHistory=true;  
            showclaim.hasPreviousHistory=true;
            showclaim.hasNext=true;
            showclaim.hasPrevious=true;
            showclaim.getRecords();
            showclaim.navigate();       
            showclaim.firstHistory();       
            showclaim.previousHistory();
            showclaim.lastHistory();        
            showclaim.nextHistory();
            showclaim.exportToExcelReserve();
            showclaim.exportToExcelPayment();
            showclaim.noOfReserveRecord=10;
            showclaim.check();
            showclaim.firstRecord();
            showclaim.lastRecord();
            showclaim.nextRecord();
            showclaim.previousRecord();
            showclaim.setPageNumberReserve();
            showclaim.setpageNumberHistory1();
            ShowClaimDetailsCtrl.getGenerateTime();
            ShowClaimDetailsCtrl.getGenerateTimeStamp();
            showclaim.printClaimPaymentHistory();
            showclaim.updateClaimHandler();
            ShowClaimDetailsCtrl.reserveDataWrapper rdw = new ShowClaimDetailsCtrl.reserveDataWrapper(cph,10,10);
            
            Test.stopTest();
        }
    }
  
}