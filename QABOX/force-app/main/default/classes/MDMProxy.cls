/**************************************************************************
* Author  : Phil Spenceley
* Company : cDecisions
* Date    : 30/07/2013
***************************************************************************/
    
/// <summary>
///  A proxy base class to access the MDM Services
/// </summary>
public abstract without sharing class MDMProxy{

    protected String EndPoint { 
        get {
            if (Test.isRunningTest()) {
                return 'https://soa-dev.gard.no/MDM/Notification';
            } else {
                return MDMConfig__c.getInstance().Endpoint__c;
            }
        }
    }
    protected String ServiceNS { 
        get {
            if (Test.isRunningTest()) {
                return 'http://www.gard.no/mdm/v1_0/mdmpartner';
            } else {
                return MDMConfig__c.getInstance().Namespace__c;
            }
        }
    }
    protected String ServiceNS_Prefix { 
        get {
            if (Test.isRunningTest()) {
                return 'mdm';
            } else {
                return MDMConfig__c.getInstance().Prefix__c;
            }
        }
    }
    public Static String SOAPNS { 
        get {
            if (Test.isRunningTest()) {
                return 'http://schemas.xmlsoap.org/soap/envelope/';
            } else {
                return MDMConfig__c.getInstance().SOAPNS__c;
            }
        }
    }
    public static String SOAPNS_Prefix { 
        get {
            if (Test.isRunningTest()) {
                return 'soapenv';
            } else {
                return MDMConfig__c.getInstance().SOAPPrefix__c;
            }
        }
    }
    //TODO: put these all back into custom settings...
    protected String SOAP_Body {
        get {
            return 'Body';
        }
    }
    protected String MDMChangeResponse_Label {
        get {
            return 'mdmChangeResponse';
        }
    }
    protected String MDMChangeResponse_Namespace {
        get {
            return 'http://www.gard.no/mdm/v1_0/mdmpartner';
        }
    }
    
    
    protected DOM.Document sendRequest(MDMRequest request){
        DOM.Document responseDoc = new DOM.Document();
        String requestString = buildRequest(request.getSoapBody().getRootElement());
        System.Debug('Request: ' + requestString);
        //Call service
        system.debug('endpoint from MDMProxy-->'+endPoint);
        if(requestString.contains('mdm:upsertContactRequest')|| requestString.contains('mdm:changeContactsRequest')){ //SF-4227 mdm:changeContactsRequest is added
            responseDoc = WebServiceUtil.processRequest(MDMConfig__c.getInstance().Contact_Endpoint__c, requestString, null);
            system.debug('Object---> contact'+MDMConfig__c.getInstance().Contact_Endpoint__c);
        }
        else{
            responseDoc = WebServiceUtil.processRequest(endPoint, requestString, null);
        }
        if(responseDoc!=null){
            System.Debug('Response: ' + responseDoc.toXMLString());
        }
        
        return responseDoc;
    }
    protected DOM.XMLNode sendRequestMerge(MDMRequest request){
        DOM.Document responseDoc = new DOM.Document();        
        DOM.XMLNode eachXML = newBuildRequest(request.getSoapBody().getRootElement());
        System.Debug('Request: ' + eachXML);
        //Call service
        /*responseDoc = WebServiceUtil.processRequest(endPoint, requestString, null);
        if(responseDoc!=null){
            System.Debug('Response: ' + responseDoc.toXMLString());
        }*/
        
        return eachXML;
    }
    public DOM.XMLNode newBuildRequest(Dom.XmlNode request){
    DOM.Document xmlDoc = new DOM.Document();
        
        //soapHeader.addChildElement('SessionHeader', ServiceNS, null).addChildElement('sessionId', ServiceNS, null).addTextNode(WebServiceUtil.getMDMTestSession());

        //DOM.XMLNode soapBody = envelope.addChildElement('Body', SOAPNS, null).addChildElement(request.getName(), ServiceNS, null);
        DOM.XMLNode soapBody = xmlDoc.createRootElement('MyBody', null, null);
        XmlUtils.copyXmlNode(request, soapBody);
        system.debug('*** soapBody : ' + soapBody);
        return soapBody;
       // return xmlDoc.toXMLString();
    }
    // Use to prepare final XML request
    public string finalBuildRequest(Dom.XmlNode request){
    DOM.Document xmlDoc = new DOM.Document();

        DOM.XmlNode envelope = xmlDoc.createRootElement('Envelope', SOAPNS, SOAPNS_Prefix);
        for(MDM_Namespaces__c n : MDM_Namespaces__c.getAll().values()){
            envelope.setNamespace(n.name,n.namespace__c);
        }           
        DOM.XMLNode soapHeader = envelope.addChildElement('Header', SOAPNS, null);
        DOM.XMLNode soapBody = envelope.addChildElement('Body', SOAPNS, null);
        XmlUtils.copyXmlNode(request, soapBody);
        system.debug('*** soapBody : ' + soapBody);
        
        return xmlDoc.toXMLString();
    }
    public Static String buildRequest(Dom.XmlNode request){
        DOM.Document xmlDoc = new DOM.Document();

        DOM.XmlNode envelope = xmlDoc.createRootElement('Envelope', SOAPNS, SOAPNS_Prefix);
        for(MDM_Namespaces__c n : MDM_Namespaces__c.getAll().values()){
            envelope.setNamespace(n.name,n.namespace__c);
        }           
        DOM.XMLNode soapHeader = envelope.addChildElement('Header', SOAPNS, null);
        
        //soapHeader.addChildElement('SessionHeader', ServiceNS, null).addChildElement('sessionId', ServiceNS, null).addTextNode(WebServiceUtil.getMDMTestSession());

        //DOM.XMLNode soapBody = envelope.addChildElement('Body', SOAPNS, null).addChildElement(request.getName(), ServiceNS, null);
        DOM.XMLNode soapBody = envelope.addChildElement('Body', SOAPNS, null);
        XmlUtils.copyXmlNode(request, soapBody);
        system.debug('*** soapBody : ' + soapBody);

        return xmlDoc.toXMLString();
    }
    
    private String getFieldsString(Set<String> fields) {
        String soqlFields = null;
        for(string field : fields) {
            if (soqlFields == null) {
                soqlFields = field;
            } else {
                soqlFields += ', ' + field;
            }
        }
        if (soqlFields == null) {
            soqlFields = 'id ';
        }
        
        return soqlFields;
    }
    
    private String getIdsString(List<id> ids) {
        String idsString = null;
        for (id id : ids) {
            if (idsString  == null) {
                //idsString = '\'\'{0}\'\'' + id;
                idsString = '\'' + id + '\'';
            } else {
                //idsString += ', \'\'{0}\'\'' + id;
                idsString += ', \'' + id + '\'';
            }
        }
        return idsString;
    }
    
    public static string stripWhitespace(string inString){
        system.debug('*** inString : '+inString);
        string outString = inString.replaceAll( '\\s+', '');
        system.debug('*** outString : '+outString);
        return outString;
    }
    
    public static string stripSpecial(string inString){
        system.debug('*** inString : '+inString);
        string outString = inString.replaceAll( '[{}()^$&._%#!@=<>:;,~\'*?/\\+|-]+', '');
        system.debug('*** outString : '+outString);
        return outString;       
    }
    
    public string stripSpecialAndWhite(string inString){
        return(stripWhitespace(stripSpecial(inString)));

    }
    
    protected String buildSoqlRequest(Set<String> fields, String objectName, id id) {
        string query = string.format('SELECT {0} FROM {1} WHERE id = \'\'{2}\'\'', new List<String>{getFieldsString(fields), objectName, id});
        System.debug('Dynamic Query: ' + query);
        return query;
    }
    
    protected String buildSoqlRequest(Set<String> fields, String objectName, List<id> ids) {
        string query = string.format('SELECT {0} FROM {1} WHERE id IN ({2})', new List<String>{getFieldsString(fields), objectName, getIdsString(ids)});
        System.debug('Dynamic Query: ' + query);
        return query;
    }
    
    public abstract class MDMRequest {
        public abstract Dom.Document getSoapBody();
        public abstract String getName();
        
        public virtual void appendSObjectXml(Dom.XmlNode root, sObject sObj, List<MDM_Service_Fields__c> serviceFields){
            map<string, dom.xmlnode> xmlnodemap = new map<string, dom.xmlnode>();
            //get Namespaces from custom setting
            map<string, MDM_Namespaces__c> namespaceMap = MDM_Namespaces__c.getAll();
            
            for (MDM_Service_Fields__c field : serviceFields) {
                if(field.type__c == 'XmlNode'){
                    if (field.parent_Node__c == 'root'){
                        xmlnodemap.put(field.XmlLabel__c, root);                
                    } else {
                        xmlnodemap.put(field.XmlLabel__c, xmlnodemap.get(field.parent_Node__c).addChildelement(field.XmlLabel__c, namespacemap.get(field.Namespace_Prefix__c).namespace__c, field.Namespace_Prefix__c));
                    }   
                }
                else if(field.type__c == 'Field'){
                    string innerText = '';
                    boolean isNull = false;
                    //Start
                    List<String> fieldNames = field.Field_Name__c.split('\\.');
                    object value;
                    sObject sObj2 = sObj;
                    for (String s : fieldNames) {
                        if (s.endsWithIgnoreCase('__r')) {
                            sObj2 = sObj2.getsObject(s);
                        } else {
                            value = sObj2.get(s);
                        }
                    }
                    
                    if (value != null) {
                        if (value instanceOf Date) {
                            innerText = Datetime.newInstance(((Date)value).year(), ((Date)value).month(), ((Date)value).day()).format('yyyy-MM-dd');
                        } else if (value instanceOf Datetime){
                            innerText = ((Datetime)value).format('yyyy-MM-dd');
                        } else {
                            innerText = string.valueof(value);
                        }
                        if(field.Strip_Special_Characters__c){
                            innerText = stripSpecial(innerText);
                        }
                        if(field.Strip_Whitespaces__c){
                            innerText = stripWhitespace(innerText);
                        }
                        
                    } else {
                        isNull = true;
                    }
                    //End
                    /*
                    if (sObj.get(field.Field_Name__c) != null) {
                        object value = sObj.get(field.Field_Name__c);
                        
                        if (value instanceOf Date) {
                            innerText = Datetime.newInstance(((Date)value).year(), ((Date)value).month(), ((Date)value).day()).format('yyyy-MM-dd');
                        } else if (value instanceOf Datetime){
                            innerText = ((Datetime)value).format('yyyy-MM-dd');
                        } else {
                            innerText = string.valueof(value);
                        }
                    } else {
                        isNull = true;
                    }
                    */
                    if (!(isNull && field.Omit_Node_If_Null__c)) {
                        DOM.XmlNode xmlField = xmlnodemap.get(field.parent_Node__c).addChildElement(field.xmlLabel__c, namespacemap.get(field.Namespace_Prefix__c).namespace__c, field.Namespace_Prefix__c);                                
                        if (!isNull) 
                            xmlField.addTextNode(innerText); 
                    }
                }
                else if(field.type__c == 'Constant'){
                    DOM.XmlNode xmlConstant = xmlnodemap.get(field.parent_Node__c).addChildElement(field.xmlLabel__c, namespacemap.get(field.Namespace_Prefix__c).namespace__c, field.Namespace_Prefix__c);                             
                    xmlConstant.addTextNode(field.Field_Name__c);   
                }
            }
        }
    }
    
    public class MDMDeserializationException extends Exception {
        public DOM.Document xmlDoc { get; private set; }
        public String xmlString { get { return xmlDoc != null ? xmlDoc.toXmlString() : ''; } }
        
        public MDMDeserializationException(Dom.Document xmlDocument, String message, Exception innerEx) {
            this(message, innerEx);
            this.xmlDoc = xmlDocument;
        }
        
        public MDMDeserializationException(Dom.Document xmlDocument, String message) {
            this(xmlDocument, message, null);
        }
        
    }
    
}