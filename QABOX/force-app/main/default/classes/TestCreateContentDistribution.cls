@isTest
public class TestCreateContentDistribution{
     static testMethod void createContentDistribution(){
         ContentVersion content=new ContentVersion(); 
         List<ContentDistribution> test_contentDistributionList = new List<ContentDistribution>();
            content.Title='Testing';
            content.PathOnClient='/' + content.Title + '.jpg'; 
            Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
            content.VersionData=bodyBlob; 
            //content.LinkedEntityId=sub.id;
            content.origin = 'H';
        insert content;
        if(content != null){
        ContentDistribution distribution = new ContentDistribution();
        distribution.Name = content.Title;
        distribution.ContentVersionId = content.Id;
        distribution.PreferencesAllowViewInBrowser= true;
        distribution.PreferencesLinkLatestVersion=true;
        distribution.PreferencesNotifyOnVisit=false;
        distribution.PreferencesPasswordRequired=false;
        distribution.PreferencesAllowOriginalDownload= true;
        test_contentDistributionList.add(distribution);
        //insert test_contentDistributionList;
        if(test_contentDistributionList != null && test_contentDistributionList.size() >0)
            Database.insert(test_contentDistributionList,false);
        }
     }
}