global class BatchDuplicateClaimDelete implements Database.Batchable<sObject> {
   global String query;
   
   
   global BatchDuplicateClaimDelete() {
   query=  'SELECT  MyGard_Claim_ID__c  FROM Case where Claim_Reference_Number__c <>NULL and (NOT  SFDC_Claim_Ref_ID__c like \'E%\') and MyGard_Claim_ID__c like\'E%\'  and parentid=null  and createdby.name=\'integration\''; 
   
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<case> scope){
  system.debug('scope'+scope.size());
  
  list<string> mygardclaimid=new list<string>() ; 
  list<case> delclmlst=new list<case>();
  
  try{
  FOR (case s : scope) {
system.debug('MyGard_Claim_ID__c '+s.MyGard_Claim_ID__c); 
mygardclaimid.add(s.MyGard_Claim_ID__c); 

system.debug('*****mygardclaimid'+mygardclaimid);

}
}catch (exception ex){}

delclmlst=[SELECT ID FROM CASE WHERE MyGard_Claim_ID__c in :mygardclaimid and Claim_Reference_Number__c =NULL and SFDC_Claim_Ref_ID__c like 'E%' and MyGard_Claim_ID__c like'E%'  and parentid=null  and createdby.name<>'integration'];
system.debug('*****delclmlst'+delclmlst);
      delete delclmlst;
     // DataBase.emptyRecycleBin(dellst);
   }

   global void finish(Database.BatchableContext BC){
   }
}