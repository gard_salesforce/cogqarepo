/*************************************************************************************************
    Author      : Martin Gardner
    Company     : cDecisions
    Date Created: 
    Description : This class provides support for overriding the edit function in certain circumstances
    Modified    :   
***************************************************************************************************/
public with sharing class CheckEditAccountExt {

    public account acc{get;set;}
    private ApexPages.StandardController stdCtrl {get; set;}

    public checkEditAccountExt(ApexPages.StandardController std) {
		if (!Test.isRunningTest()) {
			std.addFields(new String[]{
				'Deleted__c',
				'Account_Sync_Status__c'
			});
		}
        acc = (Account)std.getRecord();
    }
	
	public Boolean EditSyncFailuresEnabled { 
		get { 
			MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
			return (mdmSettings.Edit_Sync_Failure_Records__c && acc.Account_Sync_Status__c == 'Sync Failed' && !acc.Deleted__c);
		} 
	}
    
    public pagereference checkAndRedirect() {
		if (!acc.Deleted__c && (EditSyncFailuresEnabled || acc.Account_Sync_Status__c == 'Synchronised')) {
			editLink = new pageReference('/001/e');//?id=' + acc.Id + '&retURL=' + acc.id);
            editLink.getParameters().putAll(ApexPages.currentPage().getParameters());
            //Check the parameters
            System.Debug('*** stdEdit.getParameters = ' + editLink.getParameters());
			if (acc.Account_Sync_Status__c == 'Synchronised') {
				return editLink;
			}
		} 
		return null;
    }
	
	private pagereference editLink;
	
	public pagereference ignoreWarningAndEdit() {
        return editLink;
    }
	
    public pagereference goBack() {
        pagereference stdDetail = new pageReference('/' + acc.Id);
        return stdDetail;
    }
    
}