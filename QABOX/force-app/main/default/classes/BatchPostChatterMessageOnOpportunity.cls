/******************************************************************************************************
* Class Name: BatchPostChatterMessageOnOpportunity
* Created By: Nagarjuna Kaipu
* Date: 08-04-2021
* Description: SF-6270 - Used to notify via chatter when there are 2 months left for the opportunity inception date
*******************************************************************************************************
*  Version		Developer			Date			Description
=======================================================================================================
*  1.0          Nagarjuna Kaipu		08/04/2021  	Created
*  1.1          Nagarjuna Kaipu     07/05/2021      SF-6565 Improve opp chatter post text 
********************************************************************************************************/
global class BatchPostChatterMessageOnOpportunity Implements Database.batchable<sObject>{
    
    global Database.queryLocator start(Database.BatchableContext BC)
    {
        Date expDate = date.today().adddays(60);
        if(Test.isRunningTest()){
            return Database.getqueryLocator([Select Id, Name, Owner.Name, Expiry_Date__c, Account.Name, Account.Claim_Adjuster_Marine_lk__c, Account.Claim_handler_Marine__c, RecordType.Name, Type
                                             From Opportunity where RecordType.Name = 'Marine' AND Type = 'Renewal' Limit 1]);
        }
        else{
            return Database.getqueryLocator([Select Id, Name, Owner.Name, Expiry_Date__c, Account.Name, Account.Claim_Adjuster_Marine_lk__c, Account.Claim_handler_Marine__c, RecordType.Name, Type
                                             From Opportunity where Expiry_Date__c = :expDate AND RecordType.Name = 'Marine' AND Type = 'Renewal']);   
        }
    }
    
    global void execute(Database.BatchableContext BC,List<Opportunity> oppList)
    {
        List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
        
        for(Opportunity opp : oppList){
            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MentionSegmentInput mentionSegmentInput2 = new ConnectApi.MentionSegmentInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
            ConnectApi.TextSegmentInput textSegmentInput2 = new ConnectApi.TextSegmentInput();
            
            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            //Mention user here
            if(opp.account.Claim_Adjuster_Marine_lk__c != null){
                mentionSegmentInput.id = opp.account.Claim_Adjuster_Marine_lk__c;
                textSegmentInput2.text = ' ';
                messageBodyInput.messageSegments.add(mentionSegmentInput);  
                messageBodyInput.messageSegments.add(textSegmentInput2);
            }
            if(opp.account.Claim_handler_Marine__c != null && opp.account.Claim_handler_Marine__c != opp.account.Claim_Adjuster_Marine_lk__c){
                mentionSegmentInput2.id = opp.account.Claim_handler_Marine__c;
                messageBodyInput.messageSegments.add(mentionSegmentInput2);
            }
            Datetime dt = opp.Expiry_Date__c;
            String dateStr = dt.format('dd/MM/yyyy');
            //SF-6565 modified
            textSegmentInput.text = '\n'+'FYI the ongoing Marine policy for '+opp.Account.Name+' is due to renew on the '+ dateStr+'.'
                +'\n'+'Responsible Underwriter, '+ opp.Owner.Name +', will appreciate your soonest input with information for the renewal covering the following topics (as applicable):'
                +'\n\n\u2022'+' 3+1 year statistics'
                +'\n\u2022'+' Timely notification of new claims to us'
                +'\n\u2022'+' Specific comments on closed and open cases'
                +'\n\u2022'+' Difficulties in the communication with Owners/Broker'
                +'\n\u2022'+' Workload on this account'
                +'\n\u2022'+' Suggestion to revised policy wording (check with the adjuster)'
                +'\n\u2022'+' Feedback from surveyors (working environment, access to documents and info, communication with the crew etc)'
                +'\n\u2022'+' AOB';
            messageBodyInput.messageSegments.add(textSegmentInput);
            
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = opp.Id;
            
            ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);
            batchInputs.add(batchInput);
        }
        ConnectApi.ChatterFeeds.postFeedElementBatch(null, batchinputs);
    }
    
    global void finish (Database.BatchableContext BC)
    {
        
    }
}