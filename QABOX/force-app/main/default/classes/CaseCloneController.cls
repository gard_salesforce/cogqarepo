/**
    * Created by ohuuse on 29/10/2019.
    */
    
    public with sharing class CaseCloneController {
        // will clone the Case and the related EmailMessages and create new
        // contentDocumentLink which relates the Files/Attachments to the cloned EmailMessages
        /*
        Declared a Global Case List to accept Cloned Case to Avoid Multiple DML's 
        */
        
        public static Boolean afterInsertRunOnce = false;
        //To hold cloned case id, selected content documents and email messages
        public static Id clonedCaseId;
        public static List<Id> selectedContentDocIdList;
        public static List<Id> selectedEmailMessagesIdList;
        // Not tested but demonstrates the general idea:
        @InvocableMethod
        public static void cloneCaseWithRelatedRecordsFromProcess(List<Id> caseIds){
            Id returnId = CaseCloneController.cloneCaseWithRelatedRecords(caseIds[0],null,null);
        }
        
        @AuraEnabled
        public static Id cloneCaseWithRelatedRecords(Id caseId,List<Id> selectedConDocIdList,List<Id> selectedEmailMsgsIdList) 
        {
            system.debug('inside cloneCaseWithRelatedRecords method *******');
            // Preface to soql all fields from the caseId
            Set<String> caseExceptionStrings = new Set<String>();
            //List<String> emailList = new List<String>();
            caseExceptionStrings.add('SFDC_Claim_Ref_ID__c');
            caseExceptionStrings.add('GUID__c');
            String caseWhereQuery = 'Id = \'' + caseId + '\'';
            // create soql query with all editable fields and run query
            Case originalCase = database.query(CaseCloneController.queryStringAllFields('Case', caseWhereQuery, caseExceptionStrings));
            // clone case
            Case clonedCase = originalCase.clone();
            clonedCase.ParentId = caseId;
            clonedCase.Status = 'New';
            clonedCase.I_Am_A_Clone__c = TRUE; 
            clonedCase.Category__c=''; 
            clonedCase.Sub_Category__c='';       
            clonedCase.Reasons_for_Waiting_for_Customer__c='';
            if(!String.isBlank(clonedCase.Description) && clonedCase.Description.length() > 32000)
                clonedCase.Description = '';
            insert clonedCase;
            system.debug('--cloned case id--'+clonedCase.Id);
            //populate global variables with cloned case id and selected files id from clone case button in SF UI
            populateSelectedConDocIdList(selectedConDocIdList);
            populateCloneCaseId(clonedCase.Id);
            populateSelectedEmailMsgs(selectedEmailMsgsIdList);
            Map<Id, List<EmailMessage>> clonedEmailMessagesByorigEmailMessageIds = new Map<Id, List<EmailMessage>>();
            if((selectedEmailMessagesIdList != null && selectedEmailMessagesIdList.size() >0) || ((selectedContentDocIdList == null || selectedContentDocIdList.size() == 0) && (selectedEmailMessagesIdList ==null || selectedEmailMessagesIdList.size() == 0)))
                clonedEmailMessagesByorigEmailMessageIds = CaseCloneController.cloneAndInsertEmailMessages( new Map<id,List<Case>>{caseId=>new List<Case>{clonedCase}});
            // get related ContentenDocumentLinks by their orig emailMessage
            Map<Id, List<ContentDocumentLink>> origContentDocumentLinksByorigEmailIds = getRelatedContentDocumentLinks(clonedEmailMessagesByorigEmailMessageIds);
            system.debug('origContentDocumentLinksByorigEmailIds--'+origContentDocumentLinksByorigEmailIds);
            // Create  new ContentDocumentLinks relating attachment to the cloned EmailMessages
            List<ContentDocumentLink> newContentDocumentLinks = new List<ContentDocumentLink>();
            newContentDocumentLinks.addAll(CaseCloneController.createAndInsertContentDocumentLinksRelatedToEmailAndCase(origContentDocumentLinksByorigEmailIds, clonedEmailMessagesByorigEmailMessageIds));
            system.debug('newContentDocumentLinks--'+newContentDocumentLinks);
            // Create new ContentDocumentLinks related to the orig Case (files related directly to Case - not to EmailMessages)
            //if((selectedContentDocIdList == null || selectedContentDocIdList.size() == 0) && (selectedEmailMessagesIdList ==null || selectedEmailMessagesIdList.size() == 0))
            newContentDocumentLinks.addAll(CaseCloneController.createAndInsertContentDocumentLinksRelatedToCase(new Map<id,List<Case>>{caseId=>new List<case>{clonedCase}}, newContentDocumentLinks));
            system.debug('newContentDocumentLinks--'+newContentDocumentLinks);
            // insert the new ContentDocumentLinks
            if (newContentDocumentLinks.size() > 0)
            {
                insert newContentDocumentLinks;
            }
            return clonedCase.Id;
            
        }
        public static void populateSelectedConDocIdList(List<Id> selectedConDocIdList){
            selectedContentDocIdList = new List<Id>();
            if(selectedConDocIdList != null && selectedConDocIdList.size() >0)
                selectedContentDocIdList.addAll(selectedConDocIdList);
        }
        public static void populateCloneCaseId(Id cloneCaseId){
            clonedCaseId = cloneCaseId;
        }
        public static void populateSelectedEmailMsgs(List<Id> selectedEmailMsgsIdList){
            selectedEmailMessagesIdList = new List<Id>();
            if(selectedEmailMsgsIdList != null && selectedEmailMsgsIdList.size() >0)
                selectedEmailMessagesIdList.addAll(selectedEmailMsgsIdList);
        }
        public static Map<Id, List<ContentDocumentLink>> getRelatedContentDocumentLinks( Map<Id, List<EmailMessage>> clonedEmailMessagesByorigEmailMessageIds){
            Map<Id, List<ContentDocumentLink>> origContentDocumentLinksByorigEmailIds = new Map<Id, List<ContentDocumentLink>>();
            
            if(!clonedEmailMessagesByorigEmailMessageIds.keySet().isEmpty()){
                for (ContentDocumentLink cdl : [SELECT Id, LinkedEntityId, ContentDocument.Title, ContentDocumentId, ShareType, Visibility FROM ContentDocumentLink WHERE LinkedEntityId IN :clonedEmailMessagesByorigEmailMessageIds.keySet()]){
                    List<ContentDocumentLink> cdlList = origContentDocumentLinksByorigEmailIds.containsKey(cdl.LinkedEntityId) ? origContentDocumentLinksByorigEmailIds.get(cdl.LinkedEntityId) : new List<ContentDocumentLink>();
                    cdlList.add(cdl);
                    origContentDocumentLinksByorigEmailIds.put(
                        cdl.LinkedEntityId,
                        cdlList
                    );
                }
            }
            return origContentDocumentLinksByorigEmailIds;
        } 
        
        /*Commented for Arpan's code ,migration to UAT, later, discuss what need to be doen to this code wiht Yem. Preethika..
            public static void cloneCaseforIncomingEmails( Map<Id, EmailMessage> emailMessagesByCaseIds){
            Set<String> caseExceptionStrings = new Set<String>{'SFDC_Claim_Ref_ID__c','GUID__c'};
            Set<Id>caseIds = new set<id>();
            caseIds.addAll(emailMessagesByCaseIds.keySet());
            String recordTypeName='Customer_Transactions';
            String caseWhereQuery = 'Id IN :caseIds AND RecordType.DeveloperName=:recordTypeName';
            String origCaseRefid; //Added for SF-5425
            // create soql query with all editable fields and run query
            List<Case> originalCaseList = database.query(CaseCloneController.queryStringAllFields('Case', caseWhereQuery, caseExceptionStrings));
            
            Map<String,QueueInEmail__c> emailMap = new Map<String,QueueInEmail__c>();
            for(QueueInEmail__c queue: QueueInEmail__c.getAll().values()){
                emailMap.put(queue.email__c,queue);
            }
            
            Id caseOwnerId;                
            Map<id,List<Case>>clonedCaseMap=new Map<id,List<Case>>();
            
            if(!originalCaseList.isEmpty()){
                for(Case originalCase : originalCaseList){
                    EmailMessage mappedEmailMessage = emailMessagesByCaseIds.get(originalCase.id);
                    origCaseRefid = originalCase.Case_Ref_Id__c.substringBetween('[ ',' ]');
                    /*if((emailMessagesByCaseIds.get(originalCase.id).subject!=null && emailMessagesByCaseIds.get(originalCase.id).subject.Contains(originalCase.Case_Ref_Id__c)) || 
                        (emailMessagesByCaseIds.get(originalCase.id).HtmlBody!=null && emailMessagesByCaseIds.get(originalCase.id).HtmlBody.Contains(origCaseRefid)) || 
                        (emailMessagesByCaseIds.get(originalCase.id).TextBody!=null && emailMessagesByCaseIds.get(originalCase.id).TextBody.Contains(origCaseRefid))){//Added for SF-5425*//*comm. cont.
                    if((mappedEmailMessage.subject != null && mappedEmailMessage.subject.Contains(originalCase.Case_Ref_Id__c))
                       || (mappedEmailMessage.HtmlBody != null && mappedEmailMessage.HtmlBody.Contains(origCaseRefid))
                       || (mappedEmailMessage.TextBody != null && mappedEmailMessage.TextBody.Contains(origCaseRefid))){//Added for SF-5425
                           for(String email : emailMap.keySet()){
                               if(originalCase.team__c.equalsIgnoreCase(emailMap.get(email).team__c)){
                                   caseOwnerId = emailMap.get(email).id__c;
                               }
                               //if(emailMessagesByCaseIds.get(originalCase.id).ccAddress!=null && emailMessagesByCaseIds.get(originalCase.id).ccAddress.contains(email) && !(originalCase.team__c.equalsIgnoreCase(emailMap.get(email).team__c))){
                               if(mappedEmailMessage.ccAddress != null && mappedEmailMessage.ccAddress.contains(email) && !(originalCase.team__c.equalsIgnoreCase(emailMap.get(email).team__c))){
                                   Case clonedCase = originalCase.clone();
                                   clonedCase.ParentId = originalCase.id;
                                   clonedCase.Status = 'New';
                                   clonedCase.OwnerId = emailMap.get(email).Id__c;
                                   clonedCase.I_Am_A_Clone__c = TRUE; 
                                   //clonedCase.New_incoming_email__c= true;
                                   if(clonedCaseMap.containsKey(originalCase.id)){
                                       clonedCaseMap.get(originalCase.id).add(clonedCase);
                                   }else{
                                       clonedCaseMap.put(originalCase.id,new List<case>{clonedCase});
                                   }
                               }
                               // bcc is not supported by SF for inbound email
                               /*if(emailMessagesByCaseIds.get(originalCase.id).BccAddress!=null && emailMessagesByCaseIds.get(originalCase.id).BccAddress.contains(email)){
                                Case clonedCase = originalCase.clone();
                                clonedCase.ParentId = originalCase.id;
                                clonedCase.Status = 'New';
                                clonedCase.OwnerId = emailMap.get(email).Id__c;
                                clonedCase.I_Am_A_Clone__c = TRUE; 
                                if(clonedCaseMap.containsKey(originalCase.id)){
                                clonedCaseMap.get(originalCase.id).add(clonedCase);
                                }else{
                                clonedCaseMap.put(originalCase.id,new List<case>{clonedCase});
                                }
                                }*//*comm. cont.
                               //Added for SF-5430
                               //if(emailMessagesByCaseIds.get(originalCase.id).ToAddress!=null && emailMessagesByCaseIds.get(originalCase.id).ToAddress.contains(email) && !(originalCase.team__c.equalsIgnoreCase(emailMap.get(email).team__c))){
                               if(mappedEmailMessage.ToAddress != null && mappedEmailMessage.ToAddress.contains(email) && !(originalCase.team__c.equalsIgnoreCase(emailMap.get(email).team__c))){
                                   Case clonedCase = originalCase.clone();
                                   clonedCase.ParentId = originalCase.id;
                                   clonedCase.Status = 'New';
                                   clonedCase.OwnerId = emailMap.get(email).Id__c;
                                   clonedCase.I_Am_A_Clone__c = TRUE; 
                                   //clonedCase.New_incoming_email__c= true;
                                   if(clonedCaseMap.containsKey(originalCase.id)){
                                       clonedCaseMap.get(originalCase.id).add(clonedCase);
                                   }else{
                                       clonedCaseMap.put(originalCase.id,new List<case>{clonedCase});
                                   }
                               }
                           }
                           
                           if(originalCase.status.equalsIgnoreCase('Closed')){
                               Case clonedCase = originalCase.clone();
                               clonedCase.ParentId = originalCase.id;
                               clonedCase.Status = 'New';
                               clonedCase.OwnerId=caseOwnerId!=null?caseOwnerId:originalCase.ownerId;
                               clonedCase.I_Am_A_Clone__c = TRUE; 
                               //clonedCase.New_incoming_email__c= true;
                               if(clonedCaseMap.containsKey(originalCase.id)){
                                   clonedCaseMap.get(originalCase.id).add(clonedCase);
                               }else{
                                   clonedCaseMap.put(originalCase.id,new List<case>{clonedCase});
                               }
                               
                           }
                       }
                }
                
                List<case>ClonedCaseList=new List<Case>();
                if(clonedCaseMap.size()>0){
                    for(Id key :clonedCaseMap.keyset()){
                        ClonedCaseList.addAll(clonedCaseMap.get(key));
                    }
                }
                insert ClonedCaseList;
                Map<Id, List<EmailMessage>> clonedEmailMessagesByorigEmailMessageIds = CaseCloneController.cloneAndInsertEmailMessages(clonedCaseMap);
                Map<Id, List<ContentDocumentLink>> origContentDocumentLinksByorigEmailIds = getRelatedContentDocumentLinks(clonedEmailMessagesByorigEmailMessageIds);
                // get related ContentenDocumentLinks by their orig emailMessage
                cloneAndInsertAttachment(origContentDocumentLinksByorigEmailIds,clonedEmailMessagesByorigEmailMessageIds,clonedCaseMap);
            }
        }
        
        public static void cloneAndInsertAttachment(Map<Id, List<ContentDocumentLink>> origContentDocumentLinksByorigEmailIds,Map<Id, List<EmailMessage>> clonedEmailMessagesByorigEmailMessageIds, Map<id,List<Case>>clonedCaseMap){
            // new
            //Create  new ContentDocumentLinks relating attachment to the cloned EmailMessages
            List<ContentDocumentLink> newContentDocumentLinks = new List<ContentDocumentLink>();
            newContentDocumentLinks.addAll(CaseCloneController.createAndInsertContentDocumentLinksRelatedToEmailAndCase(origContentDocumentLinksByorigEmailIds, clonedEmailMessagesByorigEmailMessageIds));
            //Create new ContentDocumentLinks related to the orig Case (files related directly to Case - not to EmailMessages)
            newContentDocumentLinks.addAll(CaseCloneController.createAndInsertContentDocumentLinksRelatedToCase(clonedCaseMap, newContentDocumentLinks));
            //insert the new ContentDocumentLinks
            //  List<ContentDocumentLink> lstContentDocumentlink=[SELECT ContentDocumentId, LINKEDENTITYID FROM ContentDocumentLink where LINKEDENTITYID IN:EmailMessageList];
            
            if (newContentDocumentLinks.size() > 0){
                upsert newContentDocumentLinks;
                
            }
        }
        */
        public static String queryStringAllFields(String ObjectName, String whereQuery, Set<String> exceptionFields) {
            /*
            * @description: A code snippet that mimics the popular Select * SQL syntax in force.com's Apex language.
            */
            // Initialize setup variables
            Set<String> exceptionFieldsLCase = new Set<String>();
            for(String exceptionField : exceptionFields)exceptionFieldsLCase.add(exceptionField.toLowerCase());
            
            String query = 'SELECT';
            Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
            // Grab the fields from the describe method and append them to the queryString one by one.
            Boolean exceptionBlock;
            
            if(ObjectName.equalsIgnoreCase('EmailMessage')){
                //query+=' MessageSize__c,'; 
            }else if(ObjectName.equalsIgnoreCase('Case')){
                query+=' Case_Ref_Id__c,';
            }
            
            for(String objectFieldName : objectFields.keySet()){           
                /*
                exceptionBlock = False;
                
                for (String ef : exceptionFields){
                if(ef.containsIgnoreCase(objectFieldName)){
                exceptionBlock = True;
                }
                }*/
                exceptionBlock = exceptionFieldsLCase.contains(objectFieldName.toLowerCase());
                
                if(objectFields.get(objectFieldName).getDescribe().isCreateable() && !exceptionBlock){ // changed isUpdateable to isCreatable() // SF-5431
                    query += ' ' + objectFieldName + ',';
                }
            }
            
            // Manually add related object's fields that are needed.
            // Strip off the last comma if it exists.
            if (query.subString(query.Length() - 1, query.Length()) == ','){
                query = query.subString(0, query.Length() - 1);
            }
            
            // Add FROM statement
            query += ' FROM ' + objectName;
            // Add on a WHERE/ORDER/LIMIT statement as needed
            query += ' WHERE ' + whereQuery; // modify as needed
           
            return query;
        }
        
        //Insert email messages
        public static Map<Id, List<EmailMessage>> cloneAndInsertEmailMessages(map<id,List<Case>>ClonedCaseMap ){
            // Map<Id,emailMessage>ClonedEmailMap=new Map<id,emailMessage>();
            Gard_RecursiveBlocker.blocker = true; //To retain the old email subject with case number. 'blocker' checking done in emailMessageTrigger
            Set<String> emailMessageExceptionStrings = new Set<String>();
            emailMessageExceptionStrings.add('ValidatedFromAddress');
            String emailMessageWhereQuery;
            Map<Id, List<EmailMessage>> clonedEmailMessagesByorigEmailMessageIds = new Map<Id, List<EmailMessage>>();
            List<EmailMessage> emailMessages;
            Set<id>keys= ClonedCaseMap.keySet();
            if(selectedEmailMessagesIdList != null && selectedEmailMessagesIdList.size() >0)
                emailMessageWhereQuery = 'Id IN : selectedEmailMessagesIdList ';
            else
                emailMessageWhereQuery = 'ParentId IN : keys ';
            emailMessages=( List<EmailMessage>)database.query(CaseCloneController.queryStringAllFields('EmailMessage', emailMessageWhereQuery, emailMessageExceptionStrings));
            system.debug('emailMessages*****'+emailMessages);
            system.debug('ClonedCaseMap****'+ClonedCaseMap);
            /*  for(emailMessage email:emailMessages){
            if(email.clonedParent__c!=null){
            ClonedEmailMap.put(email.clonedParent__c,email);
            }
            }*/
            
            for (EmailMessage em : emailMessages)
            {
                if(ClonedCaseMap.get(em.parentid).size()>0)
                {
                    for(Case caseObj:ClonedCaseMap.get(em.parentid))
                    {
                        //   if(!ClonedEmailMap.containsKey(em.id)){
                       // system.debug(caseObj.id);
                        EmailMessage clonedEmailMessage = em.clone();
                        clonedEmailMessage.ParentId = caseObj.id;
                        clonedEmailMessage.Incoming = em.incoming;//SF-5431
                        clonedEmailMessage.IsCloned__c = True; // SF-5431
                        clonedEmailMessage.Converted__c=false;// SF-6141
                        clonedEmailMessage.relatedToId=caseObj.id;
                        if(!String.isBlank(clonedEmailMessage.htmlBody) && clonedEmailMessage.htmlBody.length() > 131071)
                            clonedEmailMessage.htmlBody = clonedEmailMessage.htmlBody.subString(0,131072);
                        //clonedEmailMessage.clonedParent__c=em.id;
                        //  }
                       // system.debug('clonedEmailMessage***'+clonedEmailMessage.messageSize__c);
                        if(clonedEmailMessagesByorigEmailMessageIds.containsKey(em.Id))
                        {
                            clonedEmailMessagesByorigEmailMessageIds.get(em.Id).add(clonedEmailMessage);
                        }
                        else
                        {
                            clonedEmailMessagesByorigEmailMessageIds.put(em.Id,new List<EmailMessage>{ clonedEmailMessage});
                        }
                    }
                }
            }
            List<EmailMessage>clonedEmailList=new List<EmailMessage>();
            for(Id key:clonedEmailMessagesByorigEmailMessageIds.keySet())
            {
                
                clonedEmailList.addAll(clonedEmailMessagesByorigEmailMessageIds.get(key));
            }
            //try{
            if (clonedEmailList.size() > 0) 
            {
               //system.debug('clonedEmailList*****'+clonedEmailList);
                insert clonedEmailList;
            }
            return clonedEmailMessagesByorigEmailMessageIds;
            /*}catch(exception ex){
               return null;
            }*/
        }
       
        public static List<ContentDocumentLink> createAndInsertContentDocumentLinksRelatedToEmailAndCase(Map<Id, List<ContentDocumentLink>> origContentDocumentLinksByorigEmailIds, Map<Id, List<EmailMessage>> clonedEmailMessagesByorigEmailMessageIds)
        {
            List<ContentDocumentLink> newContentDocumentLinks = new List<ContentDocumentLink>();
            // set contentDocumentIds is only used to make sure that duplicate ContentDocumentLinks are not created between
            // case and same ContentDocument
            Map<Id,set<id>> contentDocumentIdsRelatedToCase = new Map<Id,set<id>>();
            Set<Id> contentDocumentIdsRelatedEmailMessage = new Set<Id>();
            for (Id origEmailId : origContentDocumentLinksByorigEmailIds.keySet())
            {
                for (ContentDocumentLink origCdl : origContentDocumentLinksByorigEmailIds.get(origEmailId)) 
                {
                    for(EmailMessage email:clonedEmailMessagesByorigEmailMessageIds.get(origEmailId))
                    {
                        
                        
                        //    if(origCdl.LinkedEntityId!= email.Id){ //new
                        ContentDocumentLink newCdl = new ContentDocumentLink();
                        newCdl.LinkedEntityId = email.Id;
                        
                        newCdl.ContentDocumentId = origCdl.ContentDocumentId;
                        newCdl.ShareType = origCdl.ShareType;
                        newCdl.Visibility = origCdl.Visibility;
                        newContentDocumentLinks.add(newCdl);
                        
                        // add to Case too (only once for each ContentDocument):
                        if (contentDocumentIdsRelatedToCase.containsKey(email.ParentId) && !contentDocumentIdsRelatedToCase.get(email.ParentId).contains(origCdl.ContentDocumentId)) 
                        {
                            contentDocumentIdsRelatedToCase.get(email.ParentId).add(origCdl.ContentDocumentId);
                            ContentDocumentLink clonedCdl = newCdl.clone();
                            clonedCdl.LinkedEntityId = email.ParentId;
                            newContentDocumentLinks.add(clonedCdl);
                        }
                        else if(!contentDocumentIdsRelatedToCase.containsKey(email.ParentId))
                        {
                            contentDocumentIdsRelatedToCase.put(email.ParentId,new set<Id>{origCdl.ContentDocumentId});
                            ContentDocumentLink clonedCdl = newCdl.clone();
                            clonedCdl.LinkedEntityId = email.ParentId;
                            newContentDocumentLinks.add(clonedCdl);
                        }
                        //  } // new
                    }
                }
            }
            return newContentDocumentLinks;
        }
        
        public static List<ContentDocumentLink> createAndInsertContentDocumentLinksRelatedToCase(map<id,List<Case>>ClonedCaseMap, List<ContentDocumentLink> newContentDocumentLinks)
        {
            List<ContentDocumentLink> contentDocumentLinksRelatedToOrigCase = new List<ContentDocumentLink>();
            List<Id> contentDocumentIdAllReadyOnCase = new List<Id>();
            Set<Id> caseIdSet = new Set<ID>();
            caseIdSet = ClonedCaseMap.keySet();
            if((selectedContentDocIdList == null || selectedContentDocIdList.size() == 0) && (selectedEmailMessagesIdList ==null || selectedEmailMessagesIdList.size() == 0)){
                system.debug('--cloning all--');
                for (ContentDocumentLink cdExists : newContentDocumentLinks)
                {
                    for(Id key:ClonedCaseMap.keySet())
                    {
                        for(Case caseObj:ClonedCaseMap.get(key))
                        {
                            if (cdExists.LinkedEntityId ==caseObj.id)
                            {
                                contentDocumentIdAllReadyOnCase.add(cdExists.ContentDocumentId);
                            }
                        }
                    }
                }
                if(ClonedCaseMap!=null && !ClonedCaseMap.keySet().isEmpty() )
                {
                    for (ContentDocumentLink cdlCase : [SELECT Id, LinkedEntityId, ContentDocument.Title, ContentDocumentId, ShareType, Visibility FROM ContentDocumentLink WHERE LinkedEntityId = :ClonedCaseMap.keySet()])
                    {
                        
                        for(Case caseObj:ClonedCaseMap.get(cdlCase.LinkedEntityId)) {
                            if (!contentDocumentIdAllReadyOnCase.contains(cdlCase.ContentDocumentId)) 
                            {
                                ContentDocumentLink newcdl = new ContentDocumentLink();
                                newcdl.LinkedEntityId = caseObj.id;
                                newCdl.ContentDocumentId = cdlCase.ContentDocumentId;
                                newCdl.ShareType = cdlCase.ShareType;
                                newCdl.Visibility = cdlCase.Visibility;
                                contentDocumentLinksRelatedToOrigCase.add(newCdl);
                            }
                        }
                    }
                }
            }
            else if((selectedContentDocIdList != null && selectedContentDocIdList.size() > 0)){
                system.debug('selected con docs:'+selectedContentDocIdList);
                for(ContentDocumentLink cdl : [SELECT Id, LinkedEntityId, ContentDocument.Title, ContentDocumentId, ShareType, Visibility FROM ContentDocumentLink WHERE LinkedEntityId IN: caseIdSet AND ContentDocumentId IN: selectedContentDocIdList]){
                    ContentDocumentLink newcdl = new ContentDocumentLink();
                    newcdl.LinkedEntityId = clonedCaseId;
                    newCdl.ContentDocumentId = cdl.ContentDocumentId;
                    newCdl.ShareType = cdl.ShareType;
                    newCdl.Visibility = cdl.Visibility;
                    contentDocumentLinksRelatedToOrigCase.add(newCdl);
                }
            }
            system.debug('-- new con doc links to be inserted while cloning from ui--'+contentDocumentLinksRelatedToOrigCase);
            return contentDocumentLinksRelatedToOrigCase;
        }
        
    }
    //}