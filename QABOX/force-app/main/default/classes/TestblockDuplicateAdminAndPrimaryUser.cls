@isTest(SeeAllData=true) 
public class TestblockDuplicateAdminAndPrimaryUser
{
    static testMethod void blockDuplicateAdminAndPrimaryUser() 
    {
       Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');          
       insert Markt;
        
        string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
        
        User salesforceLicUser = new User(Alias = 'standt',                                             
        profileId = salesforceLicenseId ,                                            
        Email='standarduser@testorg.com',                                            
        EmailEncodingKey='UTF-8',                                            
        CommunityNickname = 'test13',                                            
        LastName='Testing',                                            
        LanguageLocaleKey='en_US',                                            
        LocaleSidKey='en_US',                                              
        TimeZoneSidKey='America/Los_Angeles',                                            
        UserName='test008@testorg.com'                                           
        );        
        insert salesforceLicUser;
        
        Account clientAcc = new Account( Name = 'Test_1', 
                                            Site = '_www.test_1.se', 
                                            Type = 'Client', 
                                            BillingStreet = 'Gatan 1',
                                            BillingCity = 'Stockholm', 
                                            BillingCountry = 'SWE', 
                                            BillingPostalCode = 'BS1 1AD',
                                            recordTypeId=System.Label.Client_Contact_Record_Type,
                                            Market_Area__c = Markt.id,
                                            Area_Manager__c = salesforceLicUser.id                                                                       
                                            );
        insert clientAcc;
        
        Contact clientContact = new Contact( FirstName='Raan',
                                                Synchronisation_Status__c = 'Synchronised',
                                                 LastName='Baan',
                                                 MailingCity = 'London',
                                                 MailingCountry = 'United Kingdom',
                                                 MailingPostalCode = 'SE1 1AD',
                                                 MailingState = 'London',
                                                 MailingStreet = '1 London Road',
                                                 AccountId = clientAcc.Id,
                                                 Email = 'test321@gmail.com',                                                 
                                                 Primary_Contact__c = False
                                            );
        insert clientContact;
        
         
        Contact clientContact_1 = new Contact( FirstName='Raan_1',
                                                 LastName='Baan_1',
                                                 Synchronisation_Status__c = 'Synchronised',
                                                 MailingCity = 'London_1',
                                                 MailingCountry = 'United Kingdom_1',
                                                 MailingPostalCode = 'SE1 1AD_1',
                                                 MailingState = 'London_1',
                                                 MailingStreet = '1 London Road_1',
                                                 AccountId = clientAcc.Id,
                                                 Email = 'test321_1@gmail.com',                                                 
                                                 Primary_Contact__c = true
                                            );
        insert clientContact_1;
        system.debug('************clientContact_1'+clientContact.id);
        
        system.debug('************clientContact_1'+clientContact_1.id);
                
        clientContact.Synchronisation_Status__c = 'Synchronised';
        update clientContact;
        system.debug('*****************UPDATING FOR PRIMARY CONTACT***********');
        clientContact.Synchronisation_Status__c = 'Synchronised';
        Gard_RecursiveBlocker.blocker = false;
        clientContact.Primary_Contact__c = true;
        update clientContact; 
        
        
   
        
    }
}