/*************************************************************************************************
    Author      : Martin Gardner
    Company     : cDecisions
    Date Created: 13/09/2013
    Description : This class manages the creation and update of Deleted_Record__c records
    			  to record a history of records that have been deleted.
    Modified    : MG 16/09/2013 Amended so that when a record is Deleted -> UnDeleted -> Deleted the
    				deleted date is updated and the undelete flag is cleared.  
***************************************************************************************************/
public with sharing class ManageDeletedRecord {
	public static boolean testFailure = false;
	public static string TestExceptionMessage = 'Test Exception';
	
	
	public static void RememberDeletedRecords(Set<Id> deletedIds, String objectName){
		
		System.Debug('*** testFailure = ' + testFailure);
		List<Deleted_Record__c> delrecs = new List<Deleted_Record__c>();
		DateTime deleteddate = Datetime.now();
		for(Id id:deletedIds){
			delrecs.add(new Deleted_Record__c(RecordId__c = id, Object__c = objectName, Deleted_Date__c = deleteddate, UnDeleted__c=false));
		}
		try{
			upsert delrecs RecordId__c;
			if(testFailure){
				throw new SetupTestData.TestException(ManageDeletedRecord.TestExceptionMessage);	
			}
		} catch(Exception e) {
			System.debug('*** ManageDeletedRecord - Exception - ' + e.getMessage());
		}
	}
	
	//This version of the method allows for recording the dwh id as well
	public static void RememberDeletedRecords(Map<Id, String> deletedRows, String objectName){
		System.Debug('*** testFailure = ' + testFailure);
		List<Deleted_Record__c> delrecs = new List<Deleted_Record__c>();
		DateTime deleteddate = Datetime.now();
		for(Id id:deletedRows.keySet()){
			delrecs.add(new Deleted_Record__c(RecordId__c = id, DWH_Id__c = deletedRows.get(id), Object__c = objectName, Deleted_Date__c = deleteddate, UnDeleted__c=false));
		}
		try{
			upsert delrecs RecordId__c;
			if(testFailure){
				throw new SetupTestData.TestException(ManageDeletedRecord.TestExceptionMessage);	
			}
		} catch(Exception e) {
			System.debug('*** ManageDeletedRecord - Exception - ' + e.getMessage());
		}		
	}
	
	public static void UpdateUnDeletedRecords(Set<Id> undeletedIds, String objectName){
		System.Debug('*** testFailure = ' + testFailure);
		List<Deleted_Record__c> delrecs = new List<Deleted_Record__c>();
		Schema.SObjectField externalId = Deleted_Record__c.RecordId__c;
		DateTime deleteddate = Datetime.now();
		for(Id id:undeletedIds){
			delrecs.add(new Deleted_Record__c(RecordId__c = id, UnDeleted__c = true));
		}
		try{
			upsert delrecs RecordId__c;
			if(testFailure){
				throw new SetupTestData.TestException(ManageDeletedRecord.TestExceptionMessage);
			}
		} catch(Exception e) {
			System.debug('*** ManageDeletedRecord - Exception - ' + e.getMessage());
		}
	}
		
}