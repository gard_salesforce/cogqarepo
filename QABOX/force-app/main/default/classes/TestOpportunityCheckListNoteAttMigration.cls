@isTest
public class TestOpportunityCheckListNoteAttMigration{
@isTest static void TestOTMNotesAtt(){
        OpportunityCheckListNoteAttMigration OTMOppAtt = new OpportunityCheckListNoteAttMigration();
        OTMOppAtt.doOperation();
    }
/*
    public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public List<Opportunity> allOpp = [SELECT ID FROM Opportunity WHERE RecordTypeName__c = 'P_I'];
    public static String strAccId;
    public static String strOppId;
    public static Blob b = Blob.valueOf('Test Data');  
    public static User salesforceUser;
    public static Opportunity_Checklist__c testOppChecklists1,testOppChecklists2;

    public static void createRecords(){
//Create a user
        salesforceUser = new User(
        Alias = 'standt', 
        profileId = salesforceLicenseId ,
        Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8',
        CommunityNickname = 'test13',
        LastName='Testing',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',  
        TimeZoneSidKey='America/Los_Angeles',
        UserName='test008@testorg.com'
        );
        insert salesforceUser; 
//Create Account
        Account acc = new Account(  Name='APEXTESTACC001',
        BillingCity = 'Bristol',
        BillingCountry = 'United Kingdom',
        BillingPostalCode = 'BS1 1AD',
        BillingState = 'Avon' ,
        BillingStreet = '1 Elmgrove Road',
        Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id,
        Market_Area__c = TestDataGenerator.getMarketArea().Id,
        OwnerId = salesforceUser.id
        );
        insert acc;
        strAccId = acc.Id; 

//create opportunity
        Id strRecId =[SELECT Id FROM RecordType WHERE Name='P&I' AND sObjectType='Opportunity' ].id;
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = strRecId ;
        opp.Name = 'APEXTESTOPP001';
        opp.AccountId = strAccId;
        opp.StageName = 'Risk Evaluation';//Renewable Opportunity
        opp.Type = 'New Business';        
        opp.Business_Type__c = 'MOUs';
        opp.Approval_Criteria__c = 'Self Approval';
        opp.Sales_Channel__c = 'Direct';
        opp.CloseDate = Date.Today();
        opp.Confirm_not_on_sanction_list__c = true;
        opp.Amount = 100;
        insert opp;
        strOppId = opp.Id;
    
   
//Create Opportunity checklist
        Id newPIRecId =[SELECT Id FROM RecordType WHERE Name='P&I' AND sObjectType='Opportunity_Checklist__c'].id;
        testOppChecklists1 = new Opportunity_Checklist__c (Charterers_Completed__c = true,Opportunity__c = strOppId ,
                                       Checklist_Completed__c = true,No_change__c = true ,
                                       Capital_Provider1__c = true,Capital_Provider_Comments__c='test',
                                       Broker__c=true,Broker_details_including_contact__c='test',
                                       Broker_Commission__c=true,Commission_Comments__c='test',
                                       Charterers_Deductiblesv2__c= true, Charterers_Deductibles_Commentsv2__c='test',
                                       RecordTypeId=newPIRecId );
                                       
        insert testOppChecklists1 ;
        
        Id charterersRecId =[SELECT Id FROM RecordType WHERE Name='Charterers' AND sObjectType='Opportunity_Checklist__c'].id;
        testOppChecklists2 = new Opportunity_Checklist__c (Charterers_Completed__c = true,Opportunity__c = strOppId ,
                                       Checklist_Completed__c = true,No_change__c = true ,
                                       Capital_Provider1__c = true,Capital_Provider_Comments__c='test',
                                       Broker__c=true,Broker_details_including_contact__c='test',
                                       Broker_Commission__c=true,Commission_Comments__c='test',
                                       Charterers_Deductiblesv2__c= true, Charterers_Deductibles_Commentsv2__c='test',
                                       RecordTypeId=charterersRecId );
                                       
        insert testOppChecklists2 ;
        
//Insert an attachment
        Attachment attachment = new Attachment();  
        attachment.ParentId = testOppChecklists1.Id;  
        attachment.Name = 'Test Attachment for Parent';  
        attachment.description = '::ITEMNO: ***+*** ::VOUCHERNO:';
        attachment.Body = b;  
        //ContentType='application/pdf ';
        insert(attachment);
       
        
    } 
//Test Method
    @isTest static void TestOTMNotesAtt(){
        OpportunityCheckListNoteAttMigration OTMOppAtt = new OpportunityCheckListNoteAttMigration();
        createRecords();
        Test.startTest();
        OTMOppAtt.doOperation();
        Test.stopTest();
    }
    */
}