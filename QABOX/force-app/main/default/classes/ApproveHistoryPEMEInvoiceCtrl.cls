public class ApproveHistoryPEMEInvoiceCtrl
{
public string pemeInvoiceId{get;set;}
public PEME_Invoice__c pemeInvoiceInstance;

public ApproveHistoryPEMEInvoiceCtrl(ApexPages.StandardController controller)
{
    pemeInvoiceInstance = (PEME_Invoice__c)controller.getRecord();
    pemeInvoiceId = String.valueOf(pemeInvoiceInstance.id).substring(0,15);
    
}

}