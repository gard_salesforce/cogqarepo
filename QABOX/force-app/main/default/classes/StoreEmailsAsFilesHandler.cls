public class StoreEmailsAsFilesHandler {
    
    public static transient String emlbody;
    @AuraEnabled public string limitExceeded{get;set;}
    @AuraEnabled public String emailsAvailable{get;set;}
    
    @AuraEnabled
    public static String FetchEmailsFromCase(Id caseId){
        List<Id> emailIdList = new List<Id>();
        List<Id> emailIdListNotToBeConverted = new List<Id>();
        List<Id> emailIdListToBeConverted = new List<Id>();
        List<JSONOutput> JSONOutputs = new List<JSONOutput>();
        Map<Id,EmailMessage> emlMsgMap = new Map<Id,EmailMessage>();
        for(EmailMessage em:[SELECT Id,ToAddress,Subject,FromAddress,MessageDate FROM EmailMessage WHERE parentId =: caseId AND Converted__c = false AND Status != '5']){
            emailIdList.add(em.Id);
            emlMsgMap.put(em.Id,em);
        }
        if(emailIdList != null && emailIdList.size() > 0){
            for(AggregateResult aggr:[SELECT sum(ContentDocument.ContentSize)FileSize, count(ContentDocumentId), linkedEntityId FROM ContentDocumentLink where linkedEntityId IN: emailIdList group by linkedEntityId]){
                if(Integer.ValueOf(aggr.get('FileSize')) > 3400000){
                    emailIdListNotToBeConverted.add((Id)aggr.get('linkedEntityId'));
                    system.debug('Exceeded email : '+aggr.get('linkedEntityId'));
                }
            }
        }
        for(Id emId:emailIdList){
            if(!emailIdListNotToBeConverted.contains(emId))
                emailIdListToBeConverted.add(emId);
        }
        system.debug('emailIdListToBeConverted--'+emailIdListToBeConverted);
        for(Id emId : emailIdListToBeConverted){
            JSONOutput js = new JSONOutput();
            EmailMessage em = emlMsgMap.get(emId);
            js.recordId = em.Id;
            js.Subject = em.Subject;
            js.fromAdd = em.FromAddress;
            js.toAdd = em.ToAddress;
            js.msgDate = String.valueOf(em.MessageDate);
            JSONOutputs.add(js);
        }
        return JSON.serialize(JSONOutputs);
    }
    @AuraEnabled
    public static StoreEmailsAsFilesHandler convertEmailToFile(Id caseId, List<Id> emailIdListToBeConverted){
        for(Id eml:emailIdListToBeConverted){
            convertEmails(caseId, eml);
        }
        return null;
    }
    @future
    public static void convertEmails(id caseId,id emailId){// List<Id> emailIdList){
        List<ContentVersion> emContentVersionList = new List<ContentVersion>();
        Map<Id,List<Id>> emlCdlMap = new Map<Id,List<Id>>();
        List<Id> cdIdList = new List<Id>();
        EmailMessage emailMsg = new EmailMessage();
        emailMsg = [SELECT Id,ParentId,ToAddress,Subject,FromAddress,CreatedDate,HtmlBody,TextBody,Converted__c,MessageDate FROM EmailMessage WHERE Id =: emailId AND Converted__c = false];
        system.debug('heap size before fetching CDL :'+Limits.getHeapSize());
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>([SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: emailId Order By LinkedEntityId desc]);  //commented to reduce heap size.
        system.debug('heap size after fetching CDL :'+Limits.getHeapSize());
        for(ContentDocumentLink cdl:cdlList){
            cdIdList.add(cdl.ContentDocumentId);
        }
        
        //for(ContentVersion cv:[SELECT ContentDocumentId,FileExtension,Id,Title,VersionData FROM ContentVersion WHERE ContentDocumentId IN: ){
        
        //}
        //Map to store email msg id and related contentdDocument Id - ENDS
        try{
            //for(EmailMessage em:emailMsgsList){
            //system.debug('condoc List : '+emlCdlMap.get(emailMsg.Id));
            
            List<ContentVersion> cvList = new List<ContentVersion>([SELECT ContentDocumentId,FileExtension,Id,Title,VersionData FROM ContentVersion WHERE ContentDocumentId IN: cdIdList]);
            system.debug('heap size after fetching content version :'+Limits.getHeapSize());
            //system.debug('email body : '+emailMsg.HtmlBody);
            //system.debug('contentVersion List : '+cvList);
            //String 
            emlbody = '';
            emlbody += 'To: '+emailMsg.ToAddress+'\n';
            emlbody += 'Subject: '+emailMsg.Subject+'\n';
            emlbody += 'From: '+emailMsg.FromAddress+'\n';
            emlbody += 'Reply-To: '+emailMsg.FromAddress+'\n';
            emlbody += 'Sender: '+emailMsg.FromAddress+'\n';
            emlbody += 'MIME-Version: 1.0\n';
            emlbody += 'Message-ID: <20050430192829.0489.test@example.test>\n';
            //emlbody += 'Date: '+emailMsg.CreatedDate+'\n';
            emlbody += 'Date:'+emailMsg.MessageDate.formatGMT('dd-MMM-yyyy hh:mm:ss a')+'\n';
            //emlbody += 'Content-Type: text/html\n\n';
            emlbody += 'Content-Type: multipart/mixed; boundary="----=_Part_2192_32400445.1115745999735"\n\n';
            //emlbody += 'Content-Transfer-Encoding: quoted-printable\n\n';
            
            emlbody += '------=_Part_2192_32400445.1115745999735\n';
            emlbody += 'Content-Type: text/html; charset=utf-8\n';//ISO-8859-1\n';
            emlbody += 'Content-Transfer-Encoding: quoted-printable\n';
            emlbody += 'Content-Disposition: inline\n\n';
            if(emailMsg.HtmlBody != null && emailMsg.HtmlBody != '')
                emlbody += '<html><head></head>'+emailMsg.HtmlBody.substringAfterLast('</head>')+'\n\n';
            else if(emailMsg.TextBody != null && emailMsg.TextBody != '')
                emlbody += '<html><head></head><body>'+emailMsg.TextBody+'</body></html>\n\n';
            system.debug('heap size before adding files :'+Limits.getHeapSize());
            if(cvList != null && cvList.size() > 0){
                for(ContentVersion cv:cvList){
                    emlbody += '------=_Part_2192_32400445.1115745999735\n';
                    emlbody += 'Content-Type: text/html; name='+cv.Title;
                    if(cv.FileExtension != null && cv.FileExtension != '' )
                        emlbody += '.'+cv.FileExtension;
                    emlbody += '\n';
                    emlbody += 'Content-Transfer-Encoding: base64\n';
                    emlbody += 'Content-Disposition: attachment; filename='+cv.Title;
                    if(cv.FileExtension != null && cv.FileExtension != '' )
                        emlbody += '.'+cv.FileExtension;
                    emlbody += +'\n\n';
                    emlbody += EncodingUtil.base64Encode(cv.VersionData)+'==\n';
                    system.debug('heap size after adding files :'+Limits.getHeapSize());
                    //system.debug('Version data of attachment:'+EncodingUtil.base64Encode(cv.VersionData));
                }
            }
            emlbody += '------=_Part_2192_32400445.1115745999735--\n';
            //system.debug('email body after customizing : '+emlbody);
            Blob fileBody = Blob.valueOf(emlbody);
            //system.debug('fileBody:'+fileBody);
            ContentVersion convertedEml =  new ContentVersion();
            convertedEml.description = 'To:'+emailMsg.ToAddress+'#From:'+emailMsg.FromAddress+'#FileCount:'+String.valueOf(cvList.size())+'#CreatedDate:'+String.valueOf(emailMsg.CreatedDate)+'#ParentId:'+emailId;
            convertedEml.ContentLocation = 'S'; //S specify this document is in SF, use E for external files
            convertedEml.PathOnClient = 'Email File'; //+ att.attIns.FileExtension;
            convertedEml.Title = emailMsg.Subject+'.eml';
            convertedEml.VersionData = filebody;
            
            //emContentVersionList.add(cv);
            //}
            //Insert converted email files(.eml files)
            insert convertedEml;
            emailMsg.Converted__c = true; //Mark the email as converted.
            List<ContentDocumentLink> emlTocdLList = new List<ContentDocumentLink>();
            
            Id conDocId = [SELECT id,ContentDocumentId FROM contentVersion WHERE Id =: convertedEml.Id].ContentDocumentId;
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.ContentDocumentId = conDocId;
            cdl.LinkedEntityId = caseId;// parent id assigned
            cdl.ShareType = 'I'; // Inferred permission
            cdl.Visibility = 'AllUsers';
            insert cdl;
            system.debug('--Files inserted--');
            update emailMsg;
            system.debug('Number of queries issued : '+Limits.getQueries()+'--can be issued--'+Limits.getLimitQueries());
        }catch(Exception e){system.debug('Exception--');}//throw new AuraHandledException(e.getMessage());}
    }
    public interface JSONOutputChildMarker {
    // empty interface to allow for using inner class as component attributes.
    }
    public class JSONOutput implements JSONOutputChildMarker{
        Id recordId;
        String Subject;
        String fromAdd;
        String toAdd;
        String msgDate;
        //String fileSource;
    }
    //SF-6432
    @AuraEnabled
    public static String checkMadatoryFields(id caseId){
        String isReady = 'false';
        Case selectedCase = new case();
        selectedCase = [SELECT Category__c,Policy_Year__c FROM Case where Id =: caseId LIMIt 1];
        if(selectedCase.Category__c != null && selectedCase.Policy_Year__c != null){
            isReady = 'true';
        }
        system.debug('--isReady--'+isReady);
        return isReady;
    }
}