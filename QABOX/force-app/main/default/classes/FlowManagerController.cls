public class FlowManagerController {

    private static final string RESOURCE_TOOLING_QUERY_FLOW = '/tooling/query?q=Select+Id,ActiveVersion.VersionNumber,LatestVersion.VersionNumber,DeveloperName+From+FlowDefinition+ORDER+BY+DeveloperName';
    private static final string RESOURCE_TOOLING_SOBJECT_FLOW = '/tooling/sobjects/FlowDefinition/';

    @AuraEnabled
    public static String getFlows() {

        HttpResponse response = executeCallout('GET', RESOURCE_TOOLING_QUERY_FLOW, null);

        System.debug('response is: '+response);
        System.debug('response body is :' +response.getBody());

        if (response.getStatusCode() == 200) {
            return response.getBody();
        }

        throw new AuraHandledException(response.toString());
    }

    @AuraEnabled
    public static boolean updateFlow(String flowId, String metadata) {

        HttpResponse response = executeCallout('PATCH', RESOURCE_TOOLING_SOBJECT_FLOW + flowId + '/?_HttpMethod=PATCH', metadata);

        System.debug(response);

        if (response.getStatusCode() == 200 || response.getStatusCode() == 204) {
            return true;
        }

        return false;
    }

    public static HttpResponse executeCallout(String method, String resourceName, String requestBody) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse res = new HttpResponse();
        if (method == 'PATCH') {
            request.setMethod('POST');    
        } else {
            request.setMethod(method);    
        }
        //String baseURL     = URL.getSalesforceBaseUrl().toExternalForm();
        //System.debug('baseURL is: '+baseURL);
        System.debug('URL is: '+getUrl() + resourceName);
        request.setEndpoint(getUrl() + '/services/data/v51.0'+ resourceName);
        //System.debug('full URL is: '+baseURL + '/services/data/v51.0' + resourceName);
        //request.setEndpoint(baseURL + '/services/data/v51.0' + resourceName);
        
        request.setHeader('Authorization', 'OAuth {!$Credential.OAuthToken}');
        //request.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
        request.setHeader('Accept', 'application/json');
        request.setHeader('X-PrettyPrint', '1');
        request.setHeader('Content-Type', 'application/json');
        
        if (String.isNotBlank(requestBody)) {
            request.setBody(requestBody);
        }
        
        System.debug('request is: '+request);

        res = http.send(request);
        boolean redirect = false;
        if(res.getStatusCode() >=300 && res.getStatusCode() <= 307 && res.getStatusCode() != 306) {
            do {
                System.debug('inside');
                redirect = false; // reset the value each time
                String loc = res.getHeader('Location'); // get location of the redirect
                if(loc == null) {
                    redirect = false;
                    continue;
                }
                request = new HttpRequest();
                request.setEndpoint(loc);
                request.setMethod('GET');
                res = http.send(request);
                if(res.getStatusCode() != 500) { // 500 = fail
                    if(res.getStatusCode() >=300 && res.getStatusCode() <= 307 && res.getStatusCode() != 306) {
                        redirect= true;
                    }
                    // I do special handling here with cookies
                    // if you need to bring a session cookie over to the
                    // redirected page, this is the place to grab that info
                }
            } while (redirect && Limits.getCallouts() != Limits.getLimitCallouts());
        }
        //congratulations you're outside of the redirects now
        //read what you need from the res object
        system.debug('res body is: '+res.getBody());
        return res;
        
    }

    private static String getUrl(){
        //return 'callout:Named_Credential'; // CHANGE THIS
        return 'callout:Flow_Manager';
    }
}