@isTest(seeAllData = true)
public class TestWsdlGardNoV10Reportsservice {
    public static testMethod void testExecute(){
        String response = new wsdlGardNoV10Reportsservice.execute_pttPort().execute(null,null,null,null,null,null,null);
        System.assertEquals('Test Response', response);
    }
    
    public static testMethod void testExecuteMarine(){
        String response = new wsdlGardNoV10Reportsservice.execute_pttPort().executeMarine(null,null,null,null,null,null);
        System.assertEquals('Test Response', response);
    }
}