@isTest
public class testGardForgotPasswordController{
    public static User salesforceUser;
    public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    //Create a user
    public static void createUser(){
        salesforceUser = new User(
            Alias = 'standt', 
            profileId = salesforceLicenseId ,
            Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8',
            CommunityNickname = 'test13',
            LastName='Testing',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',  
            TimeZoneSidKey='America/Los_Angeles',
            // UserName='test_user_123@salesforce.com'
            UserName='testGard1@salesforce.com.mygard' //Aritra 12.9
        );
        insert salesforceUser; 
    }
    
    @isTest static void  testGardForgotPsswrdMethod(){
        createUser();
        test.StartTest();
        AdminUsers__c adminUsersCS = new AdminUsers__c();
        adminUsersCS.Name = 'Number of users';  
        adminUsersCS.Value__c = 3;
        insert adminUsersCS;
        GardForgotPasswordController  GFPC = new GardForgotPasswordController();
        GFPC.username_1 = 'testGard1@salesforce.com.mygard';
        GFPC.Submit();
        test.StopTest();
    }
    @isTest static void  testGardForgotPsswrdMethod2(){
        createUser();
        test.StartTest();
        AdminUsers__c adminUsersCS = new AdminUsers__c();
        adminUsersCS.Name = 'Number of users';  
        adminUsersCS.Value__c = 3;
        insert adminUsersCS;
        GardForgotPasswordController  GFPC = new GardForgotPasswordController();
        GFPC.username_1 = 'test _1@salesforce.com';
        GFPC.Submit();
        test.StopTest();
    }
    @isTest static void  testGardForgotPsswrdMethod3(){
        GardTestData test_rec = new GardTestData();    
        test.StartTest();
        test_rec.commonrecord();
        AdminUsers__c adminUsersCS = new AdminUsers__c();
        adminUsersCS.Name = 'Number of users';  
        adminUsersCS.Value__c = 3;
        insert adminUsersCS;
        GardForgotPasswordController  GFPC = new GardForgotPasswordController();
        GFPC.username_1 = 'mygardtestbrokerUser@testorg.com.mygard';
        GFPC.Submit();
        test.StopTest();
    }
}