/****************************************************************************************************************************
*    Deveployed By   :    Cognizant Technology Solution
*    Created Date    :    6/03/2015 
*    Descriptions    :    MyGard portal login has been controll from this class.
*    Modification Log
*    --------------------------------------------------------------------------------------------------------
*    Developer                                Date                        Description
*    --------------------------------------------------------------------------------------------------------
*    Arindam Ganguly                          6/03/2015                   Custom login for MyGard(portal) user
*    Arindam Ganguly                          17/11/2016                  Empty space in last char not accepted in username SF-259 
*    Arpan Muhuri                             16/11/2017                  Implementation of Remember me feature
*****************************************************************************************************************************/
Global class GardLoginCtrl{
    public boolean isRemember{get;set;}
    public String username{get;set;}
    public String password{get;set;}
    public String topMessage{get;set;}
    public String detailMessage{get;set;}
    public String errorMessage{get;set;}
    public String verCodeMessage{get;set;}
    
    public String BrowsertopMessage{get;set;}
    public String BrowserDetailMessage{get;set;}
    public string rememberFlag; //SF-3897
    
    public GardLoginCtrl(){
        resetErrorMessage();
        showMessage();
        showBrowserMessage();
        //Start SF-3897
        isRemember = false;
        rememberFlag = '';
        Cookie existingLogInCredDetails = ApexPages.currentPage().getCookies().get('GardLoginCredentials');
        if(existingLogInCredDetails != null){
            String[] existingUsrId = existingLogInCredDetails.getValue().split('##');
            system.debug('user id  '+existingUsrId[0]+' -- Password : '+existingUsrId[1]);
            Blob UsrIdStr = EncodingUtil.base64Decode(existingUsrId[0].trim());
            username = UsrIdStr.toString();
            if(existingUsrId[1] == 'true'){
                isRemember = true;
            }
            system.debug('username : '+username);
        }else{
            username = '';
            password = '';
            isRemember = false;
        }
        
        //End SF-3897
        
        formNumber = 1;
        isPilotUser = false;
        verificationCode = '';
    }
    
    public PageReference forwardToCustomAuthPage() {
        if(UserInfo.getUserType() == 'Guest'){
            return new PageReference('/GardLogin');
        }else{
            return null;
        }
    }
    
    global PageReference forgotPassword(){
        PageReference page = new PageReference('/GardForgotPassword');
        page.setRedirect(true);
        return page;
    }
    
    public void showMessage(){
        Map<String, LogInDownTime__c> allMessages = LogInDownTime__c.getAll();
        for(String singleKey : allMessages.keySet()){
            if(singleKey.contains('TopMessage')){
                LogInDownTime__c singleMessageField = allMessages.get(singleKey);
                topMessage = singleMessageField.Message__c; 
            }else if(singleKey.contains('DetailMessage')){
                LogInDownTime__c singleMessageField = allMessages.get(singleKey);
                detailMessage = singleMessageField.Message__c; 
            }
        }
    }
    
    public void showBrowserMessage(){
        Map<String, Browser_Worning__c> allMessages = Browser_Worning__c.getAll();
        for(String singleKey : allMessages.keySet()){
            if(singleKey.contains('TopMessage')){
                Browser_Worning__c singleMessageField = allMessages.get(singleKey);
                BrowsertopMessage = singleMessageField.Message__c; 
            }else if(singleKey.contains('DetailMessage')){
                Browser_Worning__c singleMessageField = allMessages.get(singleKey);
                BrowserDetailMessage = singleMessageField.Message__c; 
            }
        }
    }
    
    //Added for SF-6019 MultiFactorAuth
    
    public Integer formNumber{get; private set{formNumber = (value > 3 || value < 1) ? 1 : value;}}
    public Boolean isFinalForm{get;private set;}
    public Boolean isPilotUser{get;private set;}
    public String verificationCode{get;set;}
    public String attemptId{get;set;}
    public Id loggingInUserId;
    private static List<Id> pilotProfileIds{//the profile ids for those that are to be multifactor authenticated
        get{
            if(pilotProfileIds == null) populateAccessControlLists();
            return pilotProfileIds;
        }
        set;
    }
    private static List<Id> pilotUserIds{//the profile ids for those that are to be multifactor authenticated
        get{
            if(pilotUserIds == null) populateAccessControlLists();
            return pilotUserIds;
        }
        set;
    }
    
    
    public PageReference form1Next(){
        resetErrorMessage();
        username = username.trim();
        System.debug('username - '+username);
        List<User> userDetails = [SELECT Id,Username,Email,MobilePhone,ProfileId FROM User WHERE username = :username AND isActive = true AND User.Profile.UserLicense.Name like '%Partner Community%']; //SF-5777 Modified
        if(!Site.isValidUsername(username) || userDetails.isEmpty()){
            errorMessage = 'Please enter a valid user name. (User names in MyGard are email address plus “.mygard“)';
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please enter a valid user name. (User names in MyGard are email address plus “.mygard“)'));
        }else{
            isPilotUser = (pilotProfileIds.contains(userDetails[0].ProfileId) || pilotUserIds.contains(userDetails[0].Id));
            if(isPilotUser){
                //System.UserManagement.initRegisterVerificationMethod(Auth.VerificationMethod.EMAIL);
                loggingInUserId = userDetails[0].Id;
                isFinalForm = false;
            }else{
                isFinalForm = true;
            }
            formNumber = 2;
        }
        
        return null;
    }
    
    public PageReference form2Next(){
        resetErrorMessage();
        Boolean passAuthRes = authenticatePassword();
        if(passAuthRes){
            if(initiateFactorVerification()){
                formNumber = 3;
                isFinalForm = true;
                verCodeMessage = getVerCodeMessage(loggingInUserId);
            }
        }else{
            errorMessage = 'Your login attempt has failed. Make sure the username and password are correct.';
        }
        return null;
    }
    
    private static void populateAccessControlLists(){//fetches list of which Profiles are to be required MFA
        pilotProfileIds = new List<Id>();
        pilotUserIds = new List<Id>();
        
        for(MFA_Login__c mfaLoginCS : MFA_Login__c.getAll().values()){
            if(mfaLoginCS.Entity_Id__c != null){
                try{//to prevent non-Id values in the lists
                    Id entityId = Id.valueOf(mfaLoginCS.Entity_Id__c);
                    if(entityId.getSobjectType() == Profile.SObjectType) pilotProfileIds.add(entityId);
                    if(entityId.getSobjectType() == User.SObjectType) pilotUserIds.add(entityId);
                }catch(Exception ex){continue;}
            }
        }
    }
    
    private Boolean authenticatePassword(){
        return ((Site.login(username, password, '/mygard/apex/MultipleCompany')) != null);
    }
    
    @TestVisible private Boolean initiateFactorVerification(){
        attemptId = System.UserManagement.initPasswordlessLogin(loggingInUserId,Auth.VerificationMethod.EMAIL);
        // attemptId = System.UserManagement.initPasswordlessLogin(loggingInUserId,Auth.VerificationMethod.SMS);
        System.debug('attemptId-->'+attemptId);
        return (attemptId != null);
    }
    
    global PageReference login(){
        resetErrorMessage();
        if(isRemember && !String.isEmpty(username) && (!String.isEmpty(password) || isPilotUser)){//SF-3897
            Blob user_Id = Blob.valueOf(username.trim());
            String u_Id = EncodingUtil.base64Encode(user_Id);
            rememberFlag = 'true';
            Cookie logInCred = new Cookie('GardLoginCredentials',u_Id+'##'+rememberFlag,null,86400,true);
            ApexPages.currentPage().setCookies(new Cookie[]{logInCred});
        }
        
        PageReference nextPage;
        if(isPilotUser){
            Auth.VerificationResult authVerRes = System.UserManagement.verifyPasswordlessLogin(loggingInUserId,Auth.VerificationMethod.EMAIL,attemptId,verificationCode,'/mygard/apex/MultipleCompany');
            // Auth.VerificationResult authVerRes = System.UserManagement.verifyPasswordlessLogin(loggingInUserId,Auth.VerificationMethod.SMS,attemptId,verificationCode,'/mygard/apex/MultipleCompany');
            System.debug('authVerRes-->'+authVerRes);
            if(authVerRes.success){
                nextPage = authVerRes.redirect;
            }else{
                errorMessage = 'Incorrect verification code entered. Please enter the valid verification code.';
                nextPage = null;
            }
        }else{
            nextPage = Site.login(username,password, '/mygard/apex/MultipleCompany');
            if(nextPage == null) errorMessage = 'Your login attempt has failed. Make sure the username and password are correct.';
        }
        
        return nextPage;
    }
    
    public PageReference resetForm(){
        formNumber = 1;
        username = '';
        password = '';
        verificationCode = '';
        isFinalForm = false;
        resetErrorMessage();
        return null;
    }
    
    private void resetErrorMessage(){
        errorMessage = '';
    }
    
    @TestVisible private static String getVerCodeMessage(Id loggingInUserId){
        String verCodeErrorMessage = 'Enter the verification code sent';
        try{
            String maskedEmail;
            User loggingInUser = [SELECT Id,Email FROM User WHERE Id = :loggingInUserId];
            String userEmail = loggingInUser.email;
            Integer indexOfAmp = userEmail.indexOf('@');
            Integer lastIndexOfDot = userEmail.lastIndexOf('.');
            if(indexOfAmp > 0 &&  lastIndexOfDot > 0 && lastIndexOfDot > indexOfAmp+2){
                
                String emailId = userEmail.substring(0,userEmail.indexOf('@'));
                userEmail = userEmail.replace(emailId,'');
                emailId = getMaskedString(emailId);
                String emailHost = userEmail.substring(userEmail.indexOf('@')+1,userEmail.indexOf('.'));
                userEmail = userEmail.replace('@'+emailHost,'');
                emailHost = getMaskedString(emailHost);
                String emailDomain = userEmail.substring(userEmail.indexOf('.'),userEmail.length());
                verCodeErrorMessage += (' to '+(emailId+'@'+emailHost+emailDomain));
            }
        }catch(Exception ex){}
        return verCodeErrorMessage;
    }
    
    private static String getMaskedString(String sourceString){
        String maskedString = '';
        if(sourceString.length() > 2){
            maskedString = sourceString.substring(0,2);
            for(Integer i = 0 ; i < sourceString.length()-2 ; i++) maskedString+='*';
        }else{
            for(Integer i = 0 ; i < sourceString.length() ; i++) maskedString+='*';
        }
        return maskedString;
    }
}