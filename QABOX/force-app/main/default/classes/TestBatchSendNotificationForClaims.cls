@isTest
private class TestBatchSendNotificationForClaims {

@istest
    static void TestBatchSendNotificaiton(){
        Test.startTest();
        CommonVariables__c cvar = new CommonVariables__c(name = 'ReplyToMailId', value__c ='Test123@gmail.com');
        insert cvar;
        GardTestData gtd = new GardTestData();
        gtd.commonRecord();
        List <case> pendingCases = new List<Case>();
        pendingCases.add(GardTestData.claimCase);     
        
        BatchSendNotificationForClaims job = new BatchSendNotificationForClaims();
        Database.QueryLocator ql = job.start(null);
        job.execute(null,pendingCases);
        job.Finish(null);
        //Database.executeBatch(new BatchSendNotificationForClaims());
        Test.stopTest();
    }
    
    public static TestMethod void testBatchSendNotificationForClaims() {
        
        
        test.StartTest();
        User adminUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()]; //select an active user that has a valid admin profile
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        System.runAs(adminUser) { 
        //run as the selected User
            
            List<CronTrigger> cronTriggersReadyToFireAgain = [SELECT Id FROM CronTrigger WHERE NextFireTime != null]; //create a list of CronTrigger records that are scheduled to fire again
            if (!cronTriggersReadyToFireAgain.isEmpty()) { //if the list is not empty
                for (CronTrigger t : cronTriggersReadyToFireAgain) { //for each record
                    System.abortJob(t.Id); //abort the job
                }
            }
        }
        //Scheduler_batchCorrespodentPIHullFlag sbc = new Scheduler_batchCorrespodentPIHullFlag ();
        String jobId = System.schedule('Test my class',
                        CRON_EXP, 
                        new Scheduler_BatchSendClaimNotification());
        test.StopTest();
    }
}