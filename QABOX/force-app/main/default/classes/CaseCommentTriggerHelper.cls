public class CaseCommentTriggerHelper{
    
    @future public static void processCaseComments(List<Id> parentCaseIds){
        List<Case> parentCases = Database.query('SELECT Id,Type,recordTypeId,recordType.Name,ownerId FROM Case WHERE Id IN :parentCaseIds');
        for(Case parentCase : parentCases){
            parentCase.Last_Activity__c = 'A case comment was added. ';
            parentCase.Last_Activity_DateTime__c = System.now();
           // system.debug('in Trigger');
            if(userInfo.getUserId() !=parentCase.ownerId){
              parentCase.New_incoming_email__c =true;
            }
        }
        
        try{
            if(!parentCases.isEmpty()) update parentCases;
        }catch(DmlException dmlEx){}
    }
}