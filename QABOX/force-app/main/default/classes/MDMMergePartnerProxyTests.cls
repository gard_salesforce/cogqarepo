@isTest
private class MDMMergePartnerProxyTests {
	private static testmethod void MDMMergePartnerAsync_Success() {
		///Arrange...
		MDMProxyTests.setupMDMConfigSettings();
		Account a1 = new Account (Name = 'Unit Test Account 1');
		Account a2 = new Account (Name = 'Unit Test Account 2');
		insert a1;
		insert a2;
		
		MDM_Account_Merge__c m = new MDM_Account_Merge__c(From_Account__c = a1.Id, To_Account__c = a2.Id);
		insert m;
		
		id id = m.id;
		
		WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(m.From_Account__c + ',' + m.To_Account__c, true);
				
		///Act...
		Test.StartTest();
		// manually call the web service
		MDMMergePartnerProxy.MDMMergePartnerAsync(new List<id> { id });
		Test.StopTest();
		
		///Assert...
		// ensure the sync status has been updated on the Contact
		m = [SELECT id, Synchronisation_Status__c FROM MDM_Account_Merge__c WHERE id = :id];
		System.assertEquals('Synchronised', m.Synchronisation_Status__c, 'MDM_Account_Merge__c Sync Status = Synchronised');
		
	}
	
	private static testmethod void MDMMergePartnerAsync_Failure() {
		///Arrange...
		MDMProxyTests.setupMDMConfigSettings();
		Account a1 = new Account (Name = 'Unit Test Account 1');
		Account a2 = new Account (Name = 'Unit Test Account 2');
		insert a1;
		insert a2;
		
		MDM_Account_Merge__c m = new MDM_Account_Merge__c(From_Account__c = a1.Id, To_Account__c = a2.Id);
		insert m;
		
		id id = m.id;
		
		WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(m.From_Account__c + ',' + m.To_Account__c, false);
				
		///Act...
		Test.StartTest();
		// manually call the web service
		MDMMergePartnerProxy.MDMMergePartnerAsync(new List<id> { id });
		Test.StopTest();
		
		///Assert...
		// ensure the sync status has been updated on the Contact
		m = [SELECT id, Synchronisation_Status__c FROM MDM_Account_Merge__c WHERE id = :id];
		System.assertEquals('Sync Failed', m.Synchronisation_Status__c, 'MDM_Account_Merge__c Sync Status = Sync Failed');
		
	}
	
	private static testmethod void MDMMergePartnerAsync_NullResponse() {
		///Arrange...
		MDMProxyTests.setupMDMConfigSettings();
		Account a1 = new Account (Name = 'Unit Test Account 1');
		Account a2 = new Account (Name = 'Unit Test Account 2');
		insert a1;
		insert a2;
		
		MDM_Account_Merge__c m = new MDM_Account_Merge__c(From_Account__c = a1.Id, To_Account__c = a2.Id);
		insert m;
		
		id id = m.id;
		
		WebServiceUtilTests.Instance.MockWebServiceResponse = null;
				
		///Act...
		Test.StartTest();
		// manually call the web service
		MDMMergePartnerProxy.MDMMergePartnerAsync(new List<id> { id });
		Test.StopTest();
		
		///Assert...
		// ensure the sync status has been updated on the Contact
		m = [SELECT id, Synchronisation_Status__c FROM MDM_Account_Merge__c WHERE id = :id];
		System.assertEquals('Sync Failed', m.Synchronisation_Status__c, 'MDM_Account_Merge__c Sync Status = Sync Failed');
		
	}
}