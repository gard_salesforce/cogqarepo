/**
 * Created by ohuuse on 04/11/2019.
 */

@isTest(seeAlldata=true)
public with sharing class CaseCloneControllerTest {

    @isTest
    public static void testCaseCloneController() {
        /*QueueInEmail__c qe = new QueueInEmail__c(name='Marine Support North', Email__c = 'testcc@test.com', Team__c='CT North');
        insert qe;   
        */
        
        Case caseOne = listBuilderModalControllerTest.createCases(1, false, true)[0];
        test.startTest();
        EmailMessage emailToCase = new EmailMessage();
        emailToCase.Subject = 'test';
        emailToCase.TextBody = 'test body';
        emailToCase.htmlbody = '<html><head></head><body><div>test body</div></body></html>\n\n';
        emailToCase.ParentId = caseOne.Id;
        insert emailToCase;
        
        //insert files
        List<ContentVersion> cvList= new List<ContentVersion>();
        ContentVersion contentVersion1 = new ContentVersion(Title = 'Penguins',
                                                           PathOnClient = 'Penguins.jpg',
                                                           VersionData = Blob.valueOf('Test Content1'),
                                                           IsMajorVersion = true
                                                           );
        ContentVersion contentVersion2 = new ContentVersion(Title = 'Penguins',
                                                           PathOnClient = 'Penguins.jpg',
                                                           VersionData = Blob.valueOf('Test Content2'),
                                                           IsMajorVersion = true
                                                           );
        ContentVersion contentVersion3 = new ContentVersion(Title = 'ForwardedAttachment_1',
                                                           PathOnClient = 'Penguins.txt',
                                                           VersionData = Blob.valueOf('Test Content3'),
                                                           IsMajorVersion = true
                                                           );
        cvList.add(contentVersion1);
        cvList.add(contentVersion2);
        cvList.add(contentVersion3);
        insert cvList;
        List<Id> idlist1=new List<Id>();
        for(ContentVersion cv:cvList){
            idlist1.add(cv.Id);
        }
        List<ContentVersion> documents = [SELECT Id, ContentDocumentId  FROM ContentVersion where Id IN: idlist1 limit 10]; 
        
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        
        ContentDocumentLink caseContentLink = new ContentDocumentLink();
        caseContentLink.LinkedEntityId = caseOne.id;
        caseContentLink.ShareType = 'I';
        caseContentLink.ContentDocumentId = documents[0].ContentDocumentId;
        caseContentLink.Visibility = 'AllUsers'; 
        
        ContentDocumentLink emailContentLink = new ContentDocumentLink();
        emailContentLink.LinkedEntityId = emailToCase.id;
        emailContentLink.ShareType = 'V';
        emailContentLink.ContentDocumentId = documents[1].ContentDocumentId;
        emailContentLink.Visibility = 'AllUsers'; 
        
        cdlList.add(caseContentLink);
        cdlList.add(emailContentLink);
        insert cdlList;
        
        listBuilderModalControllerTest.createContentDocumentsLinkedToRecord(1, emailToCase.Id);
        
        List<id> conDocId = new List<Id>{(contentVersion1.id),(contentVersion2.Id)};
        List<Id> emailId = new List<Id>{(emailToCase.Id)};
        CaseCloneController.cloneCaseWithRelatedRecords(caseOne.Id,conDocId,emailId);
        CaseCloneController.cloneCaseWithRelatedRecords(caseOne.Id,null,null);
        
        //Running test for Store emails as files.
        StoreEmailsAsFilesHandler.convertEmailToFile(caseOne.Id);
        List<Id> idlist= new List<id>();
        idlist1 = new list<id>();
        for(ContentVersion cv:documents){
            idlist1.add(cv.ContentDocumentId);
        }
        DeleteFowardedEmlFiles.deleteFiles(idlist1);
        test.stopTest();
    }
}