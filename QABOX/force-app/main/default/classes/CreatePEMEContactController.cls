public with sharing class CreatePEMEContactController {
	private String manningAgentId{get;set;}
	private String retURL{get;set;}
	private String RecordType{get;set;}
	private String name_lastcon2{get;set;}
	private String con15{get;set;}
	private String con12{get;set;}
	private String saveURL{get;set;}
	
	public pageReference onLoad(){
		if(String.isNotBlank(manningAgentId)){
            system.debug('userInfo.getUserId()--GG--'+userInfo.getUserId()+'--GG--'+ManningAgentID__c.getInstance(userInfo.getUserId()));
		ManningAgentID__c manningAgent=ManningAgentID__c.getInstance(userInfo.getUserId());
		system.debug('manningAgent*****'+manningAgent);
		if(manningAgent.ManningAgentID__c!=null){
			delete manningAgent;
		}
	    	 manningAgent=new ManningAgentID__c();
	    	 manningAgent.SetupOwnerId=userInfo.getUserId();
	    	 manningAgent.ManningAgentID__c=String.valueOf(manningAgentId);
	    	 upsert manningAgent;
	    	
	    }
		return new PageReference('/003/e?retURL='+retURL+'&RecordType='+RecordType+'&ent=Contact&name_lastcon2='+name_lastcon2+'&con15='+con15+'&con12='+con12+'&Chosen=43&saveURL='+saveURL);
	}
	public CreatePEMEContactController(){
		manningAgentId=apexpages.currentPage().getParameters().get('00ND00000035lm3');
		retURL=apexpages.currentPage().getParameters().get('retURL');
		RecordType=apexpages.currentPage().getParameters().get('RecordType');
		name_lastcon2=apexpages.currentPage().getParameters().get('name_lastcon2');
		con15=apexpages.currentPage().getParameters().get('con15');
		con12=apexpages.currentPage().getParameters().get('con12');
		saveURL=apexpages.currentPage().getParameters().get('saveURL');
	    system.debug('manningAgentId******'+manningAgentId);
	    
		
	}
    
}