@istest
private class TestBatch_AccountContactRelation {
    private static void setupTestData() {
        List<AccountContactRole> aCRoleLst = new List<AccountContactRole>();
        List<AccountContactRelation> aCRelLst = new List<AccountContactRelation>();
        GardTestData gdInstance = new GardTestData();
        gdInstance.commonRecord();
        AccountContactRole aCRole = new AccountContactRole();
        aCRole.AccountId = GardTestData.clientAcc.Id;
        aCRole.ContactId = GardTestData.clientContact.Id;
        aCRole.Role = 'Accounting';
        AccountContactRole aCRole1 = new AccountContactRole();
        aCRole1.AccountId = GardTestData.clientAcc.Id;
        aCRole1.ContactId = GardTestData.clientContact.Id;
        aCRole1.Role = 'Accounting Secondary P&I';
        aCRoleLst.add(aCRole1);
        AccountContactRole aCRole2 = new AccountContactRole();
        aCRole2.AccountId = GardTestData.brokerAcc.Id;
        aCRole2.ContactId = GardTestData.brokerContact.Id;
        aCRole2.Role = 'Accounting Secondary P&I';
        aCRoleLst.add(aCRole2);
        Insert aCRoleLst;
        system.assert(aCRoleLst.size()>0,'AccountContactRole Inserted Successfully');
    }
    static testmethod void UpsertAccountContactRelation() {
        setupTestData();
        Test.startTest();
        Batch_AccountContactRelation batch = new Batch_AccountContactRelation();
        Database.executeBatch(batch);
        Test.stopTest();
    }
}