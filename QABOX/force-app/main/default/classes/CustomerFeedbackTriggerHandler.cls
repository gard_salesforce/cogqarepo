public class CustomerFeedbackTriggerHandler 
{    
    public CustomerFeedbackTriggerHandler(){
    }
    
    public void OnBeforeInsert(List<Customer_Feedback__c> newCFs, List<Customer_Feedback__c> oldCFs, Map<ID, Customer_Feedback__c> newCFsMap , Map<ID, Customer_Feedback__c> oldCFsMap)
    {
        system.debug('Account Trigger On Before Insert');
        helper.updateCompanyOwner(newCFs);
    }
    public void OnAfterInsert(List<Customer_Feedback__c> newCFs, List<Customer_Feedback__c> oldCFs, Map<ID, Customer_Feedback__c> newCFsMap , Map<ID, Customer_Feedback__c> oldCFsMap)
    {
        system.debug('Account Trigger On After Insert');
        helper.postChatterFeed(newCFs, oldCFsMap);
        helper.accessToHandlerResp(newCFs);
    }
    public void OnAfterUpdate( List<Customer_Feedback__c> newCFs, List<Customer_Feedback__c> oldCFs, Map<ID, Customer_Feedback__c> newCFsMap , Map<ID, Customer_Feedback__c> oldCFsMap )
    {
        system.debug('Account Trigger On After Update ');
        helper.updateTaskStatus(newCFs);
        helper.postChatterFeedOnUpdate(newCFs, oldCFsMap);
        helper.accessToHandlerResp(newCFs);
        helper.removeOldHandlerResp_Share(newCFs, oldCFsMap);
    }
    public void OnBeforeUpdate( List<Customer_Feedback__c> newCFs, List<Customer_Feedback__c> oldCFs, Map<ID, Customer_Feedback__c> newCFsMap , Map<ID, Customer_Feedback__c> oldCFsMap )
    {
        system.debug('Account Trigger On Before Update ');
        helper.updateCompanyOwner(newCFs);
    }
    
    CustomerFeedbackTriggerHelper helper = new CustomerFeedbackTriggerHelper();
}