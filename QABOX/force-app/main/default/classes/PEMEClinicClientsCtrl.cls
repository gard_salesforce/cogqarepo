/*
    Name of Developer:   Debosmeeta Paul
    Name of Company:     CTS
    
    Purpose of Class:    This class is used for Clinic login-List of clients enolled.
*/
public class PEMEClinicClientsCtrl{

    private static final Integer SMARTQUERY_PAGE_SIZE   = 10;
    public set<String> lstClientOptions {get;set;}
    public list<PEME_Enrollment_Form__c> lstEnrollments {get;set;}
    public list<PEME_Enrollment_Form__c> lstEnrollPrint {get;set;}
    public list<String> selectedClient {get;set;}
    //pagination variables
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    public String sSoqlQuery;
    private String nameOfMethod;
    Public Integer intStartrecord { get ; set ;}
    Public Integer intEndrecord { get ; set ;}
    public Integer pageNum{get;set;}//{pageNum = 1;}
    public String jointSortingParam { get;set; }
  //  private String sortDirection = Ascending;
  //  private String sortExp = 'ClientName__r.Name';
    public Integer noOfData{get;set;}
    set<string> enrollStatus;
    public String userName{get;set;}
    //pagination variables
    public String orderBy{get;set;}//{orderBy = '';}
    private String sortDirection = 'ASC';
    private String sortExp = '';  
    public string loggedInContactId;
    public List<Contact> lstContact{get;set;}
    private static String Ascending='ASC';
    public String orderByNew
    {
     get
         {
            return sortExp;
         }
         set
         {
           //if the column is clicked on then switch between Ascending and Descending modes
           if (value == sortExp)
             sortDirection = (sortDirection.contains('DESC')? Ascending : 'DESC');
           else
             sortDirection = Ascending;
             sortExp = value;
         }
     }
     public String getSortDirection()
     {
        //if not column is selected 
        if (orderByNew == null || orderByNew == '')
          return Ascending;
        else
         return sortDirection;
     }
    
     public void setSortDirection(String value)
     {  
       sortDirection = value;
     }
    public ApexPages.StandardSetController submissionSetConOpen{
    get{ 
           return submissionSetConOpen;         
    }
    set; }
    public List<SelectOption> getClientOptions() {
        List<SelectOption> Options = new List<SelectOption>();
        if(lstClientOptions!= null && lstClientOptions.size()>0){
            for(String strOp:lstClientOptions){
                if(strOp != null )
                Options.add(new SelectOption(strOp,strOp));
            }
            Options.sort();
            
        }
        return Options;
    }
    
    public String sortExpression{
         get{
            return sortExp;
         }
         set{          
           if (value == sortExp)
             sortDirection = (sortDirection == Ascending)? 'DESC' : Ascending;
           else
             sortDirection = Ascending;
             sortExp = value;
         }
     }
    
 /*    public String getSortDirection(){        
        if (sortExpression == null || sortExpression == '')
          return Ascending;
        else
         return sortDirection;
     }
    
     public void setSortDirection(String value){  
       sortDirection = value;
     }
    */
    public PEMEClinicClientsCtrl(){ 
        pageNum = 1;
        orderBy = '';
        selectedClient = new list<String>(); 
        lstClientOptions = new set<String>();    
        lstEnrollments = new list<PEME_Enrollment_Form__c>(); 
        orderByNew = 'ClientName__r.Name';
        List<User> lstUser = new List<User>([select firstName,lastName,ContactId,Contact.Name,Contact.Account.Name from User where id=:UserInfo.getUserId()]);
        userName = lstUser[0].Contact.Name;
        generateClients();            
    }
    
    public void generateClients(){
        
         List<User> lstUser = new List<User>([select id,Contactid,name,Contact.Name,Email from User where id=:UserInfo.getUserId()]);
        if(lstUser!=null && lstUser.size()>0 && lstUser[0].Contactid!=null)
        {
            loggedInContactId = lstUser[0].Contactid;
            lstContact = new List<Contact>([select name, account.id,account.name,accountid, account.recordtypeid,account.company_Role__c, /*IsPeopleLineUser__c,*/account.BillingPostalcode__c,account.BillingCountry__c,account.BillingState__c,account.Billing_Street_Address_Line_1__c,account.Billing_Street_Address_Line_2__c,account.Billing_Street_Address_Line_3__c,account.Billing_Street_Address_Line_4__c 
                                          from Contact 
                                          where id=:loggedInContactId]);           
            if(lstContact!=null && lstContact.size()>0 && lstContact[0].accountId!=null)
            {
                if(lstContact[0].account.company_Role__c.contains('External Service Provider')) //Added for MYG-2948
                {        
                    pageNum = 1;
                    lstEnrollments= new List<PEME_Enrollment_Form__c>();
                    
                  /*  if (jointSortingParam != null){
                        system.debug('******jointSortingParam**************'+jointSortingParam);
                        String[] arrStr = jointSortingParam .split('##');
                        sortDirection = arrStr[0];
                        sortExp = arrStr[1];            
                    } */
                    
                    String strCondition ='';
             
                    if(selectedClient!= null && selectedClient.size()>0){
                        strCondition = ' AND ClientName__r.Name IN:selectedClient';
                    }
                    
                    string sortFullExp = sortExpression  + ' ' + sortDirection;
                    enrollStatus = new set<String>{'Enrolled','Disenrolled'};
                              
                    sSoqlQuery = 'Select Id,ClientName__c, ClientName__r.GUID__c,GUID__c, ClientName__r.Name, Enrollment_Status__c,LastModifiedDate,'+
                                ' Last_Status_Change_Date__c from PEME_Enrollment_Form__c where'+
                                ' Enrollment_Status__c IN :enrollStatus'+strCondition;
                    orderBy = ' ORDER BY ' + orderByNew + ' ' + sortDirection; //For new sorting 
                    sSoqlQuery = sSoqlQuery+ ' ' + orderBy;                      
                   // sSoqlQuery = sSoqlQuery+strCondition+' ORDER BY '+sortFullExp+' ';
                    submissionSetConOpen = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery + ' limit 10000')); 
                    submissionSetConOpen.setPageSize(10);
                    createOpenUserList();        
                  //  searchAssets();
                    
                    for(PEME_Enrollment_Form__c objEnroll:[Select Id, ClientName__c,ClientName__r.GUID__c,ClientName__r.Name,GUID__c, Enrollment_Status__c,LastModifiedDate, 
                    Last_Status_Change_Date__c from PEME_Enrollment_Form__c where Enrollment_Status__c IN ('Enrolled','Disenrolled') order by ClientName__r.Name asc]){
                        lstClientOptions.add(objEnroll.ClientName__r.Name); 
                    }
                }
            }
        }
        
    }
    Public void createOpenUserList()
    {
        if(submissionSetConOpen!=null)
        {
            lstEnrollments.clear(); 
            for(PEME_Enrollment_Form__c enr : (List<PEME_Enrollment_Form__c>)submissionSetConOpen.getRecords())             
                lstEnrollments.add(enr);
                System.debug('********'+lstEnrollments.size());         
        } 
    }
    //Returns to the first page of records
    public void firstOpen() {
        submissionSetConOpen.first(); 
        pageNum = submissionSetConOpen.getPageNumber();
        
        createOpenUserList();
        
    }
    public Boolean hasPreviousOpen   
    {   
        get   
        {   
            return submissionSetConOpen.getHasPrevious();   
        }   
        set;   
    } 
     //Returns the previous page of records   
    public void previousOpen()   
    {   
        submissionSetConOpen.previous();
        pageNum = submissionSetConOpen.getPageNumber();
        createOpenUserList();  
    } 
    //to go to a specific page number
    public void setpageNumberOpen()
    {
        submissionSetConOpen.setpageNumber(pageNum); 
        createOpenUserList();
    }
    public Boolean hasNextOpen   
    {   
        get   
        {   
            return submissionSetConOpen.getHasNext();   
        }   
        set;   
    } 
    //Returns the next page of records   
    public void nextOpen()   
    {   
        submissionSetConOpen.next(); 
        pageNum = submissionSetConOpen.getPageNumber();
        createOpenUserList();
        
    }
     //Returns to the last page of records
    public void lastOpen(){
        submissionSetConOpen.last(); 
        pageNum = submissionSetConOpen.getPageNumber();
        
        createOpenUserList();
        
    }
   /* public void searchAssets(){
        if(sSoqlQuery.length()>0){
            System.debug('selectedClient--->'+selectedClient);            
            System.debug('Soql Query--->'+sSoqlQuery);  
            setCon = null;            
            
            lstEnrollments=(List<PEME_Enrollment_Form__c>)setCon.getRecords();
            system.debug('****setCon.getResultSize()**********'+setCon.getResultSize());
            if(setCon.getPageNumber() == 1)
                intStartrecord = 1;
            else
                intStartrecord  = ((setCon.getPageNumber() - 1) * SMARTQUERY_PAGE_SIZE) + 1;
          
            intEndrecord = setCon.getPageNumber() * SMARTQUERY_PAGE_SIZE;
            if( intEndrecord > noOfRecords ){
                intEndrecord = noOfRecords ;
            }
        }
                                                              
    }
        
    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                size = SMARTQUERY_PAGE_SIZE;
                System.debug('Soql Query--->'+sSoqlQuery);
                if(sSoqlQuery!=null && sSoqlQuery!=''){             
                    setCon = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery+' LIMIT 10000'));
                    setCon.setPageSize(size);
                    noOfRecords = setCon.getResultSize();
                    system.debug('****Total records****'+noOfRecords);
                    if (noOfRecords == null || noOfRecords == 0){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No search results found.'));
                    }else if (noOfRecords == 10000){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The search returned 10000 records (maximum allowable limit).'));
                    }
                }                
            }
            return setCon;
        }set;
    }  
    
    public void setpageNumber(){
        if(setCon!=null){
            setCon.setpageNumber(pageNum);
            searchAssets();
        }
    }
        
    public Boolean hasNext {
        get {
            if(setCon!=null)
            return setCon.getHasNext();
            else return false;
        }
        set;
    }
    
    public Boolean hasPrevious {
        get {
            if(setCon!=null)
            return setCon.getHasPrevious();
            else return false;
        }
        set;
    }
    
    public Integer pageNumber {
        get {
            if(setCon!=null)
            return setCon.getPageNumber();
            else return 0;
        }
        set;
    }
       
    public void navigate(){
        if(nameOfMethod=='previous'){
             setCon.previous();
             pageNum=setCon.getpageNumber();
             searchAssets();
        }else if(nameOfMethod=='last'){
             setCon.last();
             searchAssets();
        }else if(nameOfMethod=='next'){
             setCon.next();
             pageNum=setCon.getpageNumber();
             searchAssets();
        }else if(nameOfMethod=='first'){
             setCon.first();
             searchAssets();
        }
    }
        
    public void first() {
        nameOfMethod='first';
        navigate();
    }
    
    public void next(){
        nameOfMethod='next';
        pageNum = setCon.getPageNumber();
        navigate();        
    }
   
    public void last(){
        nameOfMethod='last';
        navigate();
    }
  
    public void previous(){
        nameOfMethod='previous';
         pageNum = setCon.getPageNumber();
        navigate();
    } */
    
    public pageReference clearOptions(){
        selectedClient.clear();
        generateClients();
        return null;
    }
    public PageReference print(){
        lstEnrollPrint = new List<PEME_Enrollment_Form__c>();
        Integer count = 1;
        noOfData = 0;
        for(PEME_Enrollment_Form__c mcl : Database.Query(sSoqlQuery+' LIMIT 10000')){               
            if(count<1000){
                lstEnrollPrint.add(mcl);                    
                count++;
            }else{                   
                count = 1;
            }   
            noOfData++;                 
        }
        return page.PrintForPEMEClients;
    }
        
     public PageReference exportToExcel(){
        lstEnrollPrint = new List<PEME_Enrollment_Form__c>();
        Integer count = 1;
        noOfData = 0;
        for(PEME_Enrollment_Form__c mcl : Database.Query(sSoqlQuery+' LIMIT 10000')){               
            if(count<1000){
                lstEnrollPrint.add(mcl);                    
                count++;
            }else{                   
                count = 1;
            }   
            noOfData++;                 
        }
        return page.GenerateExcelForPEMEClients;
    }
}