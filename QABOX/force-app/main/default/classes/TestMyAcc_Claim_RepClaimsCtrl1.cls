//Test class for ReportedClaimsCtrl .
@isTest
public class TestMyAcc_Claim_RepClaimsCtrl1{    

    public static List<Account> AccountList=new List<Account>();
    public static List<Contact> ContactList=new List<Contact>();
    public static List<Contract> ContractList=new List<Contract>();
    public static List<User> UserList=new List<User>();
    public static List<Asset> AssetList=new List<Asset>();
    public static List<Case> CaseList =new List<Case>();
    public static List<Market_Area__c> Market_AreaList=new List<Market_Area__c>();
    public static List<Object__c> ObjectList=new List<Object__c>();
    public static List<Gard_Contacts__c> Gard_ContactsList=new List<Gard_Contacts__c>();
    public static string PartnerLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Partner Community' and name='Partner Community Login User Custom' limit 1].id;
    public static string SalesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Account ClientAcc;
    public static Account BrokerAcc;
    public static Contact BrokerContact;
    public static Contact ClientContact;
    public static User BrokerUser;
    public static User ClientUser;
    public static Contract ClientContract;
    public static Contract BrokerContract;
    public static Object__c Obj;
    public static Asset BrokerAsset;
    public static Asset ClientAsset;
    public static Case BrokerCase,BrokerCase_2,BrokerCase_1;
    public static Case ClientCase, ClientCasePend,ClientCase_1;
    public static Market_Area__c Markt;
    public static Gard_Contacts__c GContacts;
    public static User SalesforceLicUser;
    public static Extranet_Favourite__c exfav;
    public static AccountToContactMap__c Map_Broker, Map_Client;
    public static Account_Contact_Mapping__c AcMap1 , AcMap2;
    public static list<Account_Contact_Mapping__c> AcMaplist = new list<Account_Contact_Mapping__c>();   
    public static List<ContactSubscriptionFields__c> conFieldsList = new List<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    //************************variables added for duplicate literals*************************************
    public static string enUsrStr = 'en_US';
    public static string mailngStateStr =  'London';
    public static string draftStr =  'Draft';
    public static string piStr =  'P&I';
    public static string expDateStr =  '2015-01-01';
    public static string incptnDateStr =  '2012-01-01';
    public static string originStr = 'Community';
    public static string eventDtStr = '2014-01-03';
    public static string cargoStr =  'Cargo';
    public static string tabnameStr = 'ABC';
    
    //**************************************end**********************************************************
// Create test data...............................................    
    public Static void data(){
    
    Map<String, Topics__c> TopicTotal = Topics__c.getAll();
    List<String> topicOptions = new List<String>();  
    GardUtils.inUpdateUserFirst = false; 
    Markt = new Market_Area__c(Market_Area_Code__c = 'abcd');
    Market_AreaList.add(Markt);
    insert Market_AreaList;  
     AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
    insert numberofusers;
    conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
    conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
    conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
    conFieldsList.add(conSubFields);
    conFieldsList.add(conSubFields2);
    conFieldsList.add(conSubFields3);
    insert conFieldsList;
       
    GardTestData gtdInstance = new GardTestData();
        gtdInstance.commonRecord();   /*    
    SalesforceLicUser = new User(Alias = 'standt', 
                                    profileId = SalesforceLicenseId ,
                                    Email='standarduser@testorg.com',
                                    EmailEncodingKey='UTF-8',
                                    CommunityNickname = 'test1350',
                                    LastName='Testing',
                                    LanguageLocaleKey=enUsrStr ,
                                    LocaleSidKey=enUsrStr ,  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='test008@testorg.com.mygard'
                                   );
    insert SalesforceLicUser;
    
    //Accounts............
    BrokerAcc = new Account(  Name='testre',
                            BillingCity = 'Bristol',
                            BillingCountry = 'United Kingdom',
                            BillingPostalCode = 'BS1 1AD',
                            BillingState = 'Avon' ,
                            BillingStreet = '1 Elmgrove Road',
                            recordTypeId=System.Label.Broker_Contact_Record_Type,
                            Site = '_www.cts.se',
                            Type = 'Broker',
                            Sub_Roles__c = 'Broker - Reinsurance Broker', 
                            Market_Area__c = Markt.id,
                            Area_Manager__c = SalesforceLicUser.id                                                                       
                         );
    AccountList.add(BrokerAcc);
    
    
    ClientAcc = new Account( Name = 'Test_1', 
                                Site = '_www.test_1.se', 
                                Type = 'Client', 
                                BillingStreet = 'Gatan 1',
                                BillingCity = 'Stockholm', 
                                BillingCountry = 'SWE', 
                                BillingPostalCode = 'BS1 1AD',
                                recordTypeId=System.Label.Client_Contact_Record_Type,
                                Market_Area__c = Markt.id,
                            	Sub_Roles__c = '',
                                Area_Manager__c = SalesforceLicUser.id 
                               );
                
    AccountList.add(ClientAcc); 
    insert AccountList;
    System.assertequals(AccountList.size()>0,true,true);
    
    //Contacts.................
    BrokerContact = new Contact(FirstName='Raan',
                                 LastName='Baan',
                                 MailingCity = mailngStateStr ,
                                 MailingCountry = 'United Kingdom',
                                 MailingPostalCode = 'SE1 1AD',
                                 MailingState = mailngStateStr ,
                                 MailingStreet = '1 London Road',
                                 OtherStreet='Baker street',
                                 AccountId = BrokerAcc.Id,
                                 Email = 'test321@gmail.com'
                               );
    ContactList.add(BrokerContact) ;
    
    ClientContact= new Contact( FirstName='tsat_1',
                                  LastName='chak',
                                  MailingCity = mailngStateStr ,
                                  MailingCountry = 'United Kingdom',
                                  MailingPostalCode = 'SE1 1AE',
                                  MailingState = mailngStateStr ,
                                  MailingStreet = '4 London Road',
                                  OtherStreet= 'Baker street',
                                  AccountId = ClientAcc.Id,
                                  Email = 'test321@gmail.com'
                              );
    ContactList.add(ClientContact);
    insert ContactList;
    System.assertequals(ContactList.size()>0,true,true);
    
    GContacts = new Gard_Contacts__c(FirstName__c = 'test_first_name',
                                   LastName__c = 'test_last_name',
                                   Email__c = 'abc@gmail.com',
                                   MobilePhone__c = '78963214',
                                   Portal_Image__c = 'test_image',
                                   office_city__c = 'test_office'
                                 );
    Gard_ContactsList.add(GContacts);
    insert Gard_ContactsList; 
    System.assertequals(Gard_ContactsList.size()>0,true,true);                                
    
    //Users..................
    BrokerUser = new User(Alias = 'standt', 
                        profileId = partnerLicenseId,
                        Email='mdjawedm@gmail.com',
                        EmailEncodingKey='UTF-8',
                        LastName='estTing',
                        LanguageLocaleKey=enUsrStr ,
                        CommunityNickname = 'BrokerUser100',
                        LocaleSidKey=enUsrStr ,  
                        TimeZoneSidKey='America/Los_Angeles',
                        UserName='testBrokerUser@testorg.com.mygard',
                        ContactId = BrokerContact.Id,
                        contactId__c = GContacts.id
                       );
    UserList.add(BrokerUser);
               
    ClientUser = new User(Alias = 'alias_1',
                        CommunityNickname = 'ClientUser100',
                        Email='test_11@testorg.com', 
                        profileid = partnerLicenseId, 
                        EmailEncodingKey='UTF-8',
                        LastName='tetst_006',
                        LanguageLocaleKey=enUsrStr ,
                        LocaleSidKey=enUsrStr ,
                        TimeZoneSidKey='America/Los_Angeles',
                        UserName='testClientUser@testorg.com.mygard',
                        ContactId = ClientContact.id,
                        contactId__c = GContacts.id
                       );
    
    UserList.add(ClientUser) ;
    insert UserList;
    System.assertequals(UserList.size()>0,true,true);    
    
    AcMap1 = new Account_Contact_Mapping__c(Account__c = BrokerAcc.id,
                               Active__c = true,
                               Administrator__c = 'Admin',
                               Contact__c = BrokerContact.id,
                               IsPeopleClaimUser__c = false,
                               Marine_access__c = true,
                               PI_Access__c = true
                               //Show_Claims__c = false,
                               //Show_Portfolio__c = false 
                             );
    
    AcMap2 = new Account_Contact_Mapping__c(Account__c =  ClientAcc.id,
                           Active__c = true,
                           Administrator__c = 'Admin',
                           Contact__c = ClientContact.id,
                           IsPeopleClaimUser__c = false,
                           Marine_access__c = true,
                           PI_Access__c = true
                           //Show_Claims__c = false,
                           //Show_Portfolio__c = false 
                         );
    AcMaplist.add(AcMap1);
    AcMaplist.add(AcMap2);
    Insert AcMaplist;                            
    
    
    Map_Broker = new AccountToContactMap__c(AccountId__c=BrokerAcc.id,Name=BrokerContact.id);
    Map_Client = new AccountToContactMap__c(AccountId__c= ClientAcc.id,Name=ClientContact.id);
    
    Insert Map_Broker;
    Insert Map_Client; 
    
    BrokerContract = New Contract(Accountid = BrokerAcc.id, 
                    Account = BrokerAcc, 
                    Status = draftStr ,
                    CurrencyIsoCode = 'SEK', 
                    StartDate = Date.today(),
                    ContractTerm = 2,
                    Agreement_Type__c = piStr , 
                    Broker__c = BrokerAcc.id,
                    Client__c = ClientAcc.id,
                    Broker_Name__c = BrokerAcc.id,
                    Expiration_Date__c = date.valueof(expDateStr ),
                    Inception_date__c = date.valueof(incptnDateStr ),
                    Agreement_Type_Code__c = 'Agreement123',
                    Agreement_ID__c = 'Agreement_ID_879',
                    Business_Area__c=piStr ,
                    Accounting_Contacts__c = BrokerUser.id,
                    Contract_Reviewer__c = BrokerUser.id,
                    Contracting_Party__c = BrokerAcc.id,
                    Policy_Year__c='2015'                                            
                    );
    
    ContractList.add(BrokerContract);
    
    ClientContract = New Contract(Accountid = ClientAcc.id, 
                    Account = ClientAcc, 
                    Status = draftStr ,
                    CurrencyIsoCode = 'SEK', 
                    StartDate = Date.today(),
                    ContractTerm = 2,
                    Broker__c = BrokerAcc.id,
                    Client__c = ClientAcc.id,
                    Expiration_Date__c = date.valueof(expDateStr ),
                    Agreement_Type__c = piStr ,
                    Agreement_Type_Code__c = 'Agreement123',
                    Inception_date__c = date.valueof(incptnDateStr ),
                    Business_Area__c=piStr ,
                    Policy_Year__c='2014'                                        
                    );
    
    ContractList.add(ClientContract);
    insert ContractList ;
    System.assertequals(ContractList .size()>0,true,true);   
    
    Obj = New Object__c( Dead_Weight__c= 20,Object_Unique_ID__c = '1234lkolko');
    ObjectList.add(Obj);
    insert ObjectList ;
    System.assertequals(ObjectList.size()>0,true,true);   
    
    BrokerAsset = new Asset(Name= 'Asset 1',
                             accountid =BrokerAcc.id,
                             contactid = BrokerContact.Id,  
                             Agreement__c = BrokerContract.id, 
                             Cover_Group__c = piStr ,
                             CurrencyIsoCode='USD',
                             product_name__c = piStr ,
                             Object__c = Obj.id,
                             Underwriter__c = BrokerUser.id,
                             Risk_ID__c = '123456ris',
                             Expiration_Date__c = date.valueof(expDateStr ),
                             Inception_date__c = date.valueof(incptnDateStr )
                             ); 
    AssetList.add(BrokerAsset);
    
    ClientAsset = new Asset(Name= 'Asset 1',
                             accountid =ClientAcc.id,
                             contactid = ClientContact.Id,  
                             Agreement__c = BrokerContract.id, 
                             Cover_Group__c = piStr ,
                             CurrencyIsoCode='USD',
                             product_name__c = 'War',
                             Object__c = Obj.id,
                             Underwriter__c = ClientUser.id,
                             Expiration_Date__c = date.valueof(expDateStr ),
                             Inception_date__c = date.valueof(incptnDateStr ),
                             Risk_ID__c = '123456ri'
                             ); 
    AssetList.add(ClientAsset);   
    insert AssetList;
    System.assertequals(AssetList.size()>0,true,true); 
    
    BrokerCase = new Case(Accountid = BrokerAcc.id,
                        ContactId = BrokerContact.id,
                        Origin = originStr ,
                        //Sub_Claim_Handler__c = BrokerContact.id,
                        //Registered_By__c = BrokerContact.id, 
                        Object__c = Obj.id,
                       // Version_Key__c = 'kolka2342',
                        Claim_Incurred_USD__c= 500,
                        Claim_Reference_Number__c='kol23232',
                        Reserve__c = 350,
                        Paid__c =50000,
                        Total__c= 750,
                        Event_Date__c=date.valueof(eventDtStr ),
                        //Voyage_To__c = 'Voyage_To',
                        Member_reference__c = 'ash1232',
                        //Voyage_From__c = 'Voyage_From',
                        Claim_Type__c = cargoStr,
                        Risk_Coverage__c = BrokerAsset.id,
                        Status = 'open',
                        parentId = null,
                        Contract_for_Review__c = BrokerContract.id                                                  
                        );
    caseList.add(BrokerCase);
    
    ClientCase = new Case(Accountid = ClientAcc.id,
                         ContactId = ClientContact.id, 
                         //Registered_By__c = ClientContact.id,
                         Origin = originStr ,
                         Object__c = Obj.id, 
                         Claim_Incurred_USD__c = 50000,
                         //Claims_handler__c = ClientContact.id,
                         Event_details__c = 'Testing ReportedClaimsCtrl',
                         Paid__c = 50000, 
                         Event_Type__c = 'Incident',
                         Event_Date__c=date.valueof(eventDtStr ),
                         //Version_Key__c = 'hgdsjhdAs232',
                         Claim_Type__c = cargoStr,
                         //Event_Date__c = Date.valueof('2014-12-12'),
                         Claim_Reference_Number__c = 'ClaiMRef1234', 
                         //Claims_Currency__c = 'USD',
                         //Voyage_To__c = 'Voyage_To',
                         //Voyage_From__c = 'Voyage_From',
                         parentId = null,
                         Risk_Coverage__c = ClientAsset.id,
                         Status = draftStr ,
                         Contract_for_Review__c = ClientContract.id 
                          );
    caseList.add(ClientCase);
    
    ClientCasePend = new Case(Accountid = ClientAcc.id,
                             ContactId = ClientContact.id, 
                             //Registered_By__c = ClientContact.id,
                             Origin = originStr ,
                             Object__c = Obj.id, 
                             Claim_Incurred_USD__c = 50000,
                             //Claims_handler__c = ClientContact.id,
                             Event_details__c = 'Testing ReportedClaimsCtrl',
                             Paid__c = 50000, 
                             Event_Type__c = 'Incident',
                             Event_Date__c=date.valueof(eventDtStr ),
                             //Version_Key__c = 'hgdsjhdAs232',
                             Claim_Type__c = cargoStr,
                             //Event_Date__c = Date.valueof('2014-12-12'),
                             Claim_Reference_Number__c = 'ClaiMRef1234', 
                             //Claims_Currency__c = 'USD',
                             //Voyage_To__c = 'Voyage_To',
                             //Voyage_From__c = 'Voyage_From',
                             parentId = null,
                             Risk_Coverage__c = ClientAsset.id,
                             Status = 'Pending',
                             Contract_for_Review__c = ClientContract.id 
                              );
    caseList.add(ClientCasePend);
    insert caseList;
    caseList.clear();
    BrokerCase_1 = new Case(Accountid = BrokerAcc.id,
                            ContactId = BrokerContact.id,
                            Origin = originStr ,
                            //Sub_Claim_Handler__c = BrokerContact.id,
                            //Registered_By__c = BrokerContact.id, 
                            Object__c = Obj.id,
                           // Version_Key__c = 'kolka2342',
                            Claim_Incurred_USD__c= 500,
                            Claim_Reference_Number__c='kol23232',
                            Reserve__c = 350,
                            Paid__c =50000,
                            Total__c= 750,
                            Event_Date__c=date.valueof(eventDtStr ),
                            //Voyage_To__c = 'Voyage_To',
                            Member_reference__c = 'ash1232',
                            //Voyage_From__c = 'Voyage_From',
                            Claim_Type__c = cargoStr,
                            Risk_Coverage__c = BrokerAsset.id,
                            Status = draftStr ,
                            parentId = BrokerCase.id,
          /*                  Contract_for_Review__c = BrokerContract.id                                                  
                            );
    caseList.add(BrokerCase_1);
    
    BrokerCase_2 = new Case(Accountid = BrokerAcc.id,
                            ContactId = BrokerContact.id,
                            Origin = originStr ,
                            //Sub_Claim_Handler__c = BrokerContact.id,
                            //Registered_By__c = BrokerContact.id, 
                            Object__c = Obj.id,
                           // Version_Key__c = 'kolka2342',
                            Claim_Incurred_USD__c= 500,
                            Claim_Reference_Number__c='kol23232',
                            Reserve__c = 350,
                            Paid__c =50000,
                            Total__c= 750,
                            Event_Date__c=date.valueof(eventDtStr ),
                            //Voyage_To__c = 'Voyage_To',
                            Member_reference__c = 'ash1232',
                            //Voyage_From__c = 'Voyage_From',
                            Claim_Type__c = cargoStr,
                            Risk_Coverage__c = BrokerAsset.id,
                            Status = 'Open',
                            parentId = BrokerCase.id,
                            Contract_for_Review__c = BrokerContract.id                                                  
                            );
    caseList.add(BrokerCase_2);
    ClientCase_1 = new Case(Accountid = ClientAcc.id,
                         ContactId = ClientContact.id, 
                         Origin = originStr ,
                         Object__c = Obj.id, 
                         Claim_Incurred_USD__c = 50000,
                         Event_details__c = 'Testing ReportedClaimsCtrl',
                         Paid__c = 50000, 
                         Event_Type__c = 'Incident',
                         Event_Date__c=date.valueof(eventDtStr ),
                         Claim_Type__c = cargoStr,
                         Claim_Reference_Number__c = 'ClaiMRef1235', 
                         parentId = ClientCase.id,
                         Risk_Coverage__c = ClientAsset.id,
                         Status = 'Open',
                         Contract_for_Review__c = ClientContract.id 
                          );
    caseList.add(ClientCase_1 );
    upsert caseList;
    System.assertequals(caseList.size()>0,true,true); 
    
    
    exfav= new Extranet_Favourite__c(Item_Id__c=Obj.id, Item_Type__c='Saved Search', Item_Name__c='mycovers 1',Favourite_By__c=BrokerUser.id
                                            );
    insert exfav; 
    
    Extranet_Global_Client__c global_Client= new Extranet_Global_Client__c(Selected_Global_Clients__c=ClientAcc.id,Selected_By__c=ClientUser.id, Selected_For_Client__c = ClientAcc.id );
    insert global_Client; 
    
    Extranet_Global_Client__c global_clnt= new Extranet_Global_Client__c(Selected_Global_Clients__c=ClientAcc.id,Selected_By__c=BrokerUser.id, Selected_For_Client__c = ClientAcc.id );
    insert global_clnt;                                                     
    */
    Reimbursement__c reimb = new Reimbursement__c();
    insert reimb;
    
    Reimbursement_Line_Item__c rli = new Reimbursement_Line_Item__c(
                                                         Amount__c = 100,
                                                         Reimbursement__c = reimb.ID
                                                     );
    insert rli;
    
    Case_Reimbursement_Relation__c crr = new Case_Reimbursement_Relation__c(
                                                                 Claim__c = GardTestData.clientCase.ID,
                                                                 Reimbursement__c = reimb.ID      
                                                             );
    insert crr;
    
    } 
    
    //Test method..............................        
    public static testmethod void reportedClaims(){
        Test.startTest();
    TestMyAcc_Claim_RepClaimsCtrl1.data();
    system.runAs(GardTestData.ClientUser){
    

        ApexPages.StandardController sc2 = new ApexPages.StandardController(GardTestData.ClientAcc);
        Test.stopTest();
        ReportedClaimsCtrl reportdclm1 = new ReportedClaimsCtrl(sc2);
        reportdclm1.hasPreviousDraft = true;
        reportdclm1.pageNumberDraft = 100;
        PageReference pg = reportdclm1.printDraft();
        reportdclm1.manageCaseId = String.valueOf(GardTestData.clientCase.ID);
        PageReference pg1 = reportdclm1.goToManageClaimsPage();
        System.assertNotEquals(null,pg1);
        reportdclm1.tabName = tabnameStr ;
        reportdclm1.caseData = new Case();
        reportdclm1.year = tabnameStr ;
        reportdclm1.Objects = tabnameStr ;
        reportdclm1.claimType = tabnameStr ;
        reportdclm1.print();
        reportdclm1.exportToExcel();
        reportdclm1.exportToExcelDraft();
        reportdclm1.firstDraft();
        reportdclm1.lastDraft();
        reportdclm1.nextDraft();
        reportdclm1.previousDraft();
        reportdclm1.setpageNumberDraft();
        reportdclm1.caseId = GardTestData.clientCase.ID;
        reportdclm1.showCaseDetails();
        reportdclm1.getFilterListDraft();
        reportdclm1.filterRecords();
        reportdclm1.getClaimTypeLst();
        reportdclm1.getObjectName();
        reportdclm1.getYearLst();
		reportdclm1.mainClaimId = GardTestData.clientCase.ID;  
        reportdclm1.deleteRecord();
        reportdclm1.getSortDirection();
        reportdclm1.showClaimsHandler();
        reportdclm1.setpageNumber();
        reportdclm1.previous();
        reportdclm1.next();
        reportdclm1.last();
        reportdclm1.first();
        ReportedClaimsCtrl.getGenerateTimeStamp();
        ReportedClaimsCtrl.getGenerateTime();
        ReportedClaimsCtrl.getUserName();
        }
    }
    public static testmethod void reportedClaims_Broker(){
        Test.startTest();
        TestMyAcc_Claim_RepClaimsCtrl1.data();
    system.runAs(GardTestData.brokerUser){
    
        ApexPages.StandardController sc2 = new ApexPages.StandardController(GardTestData.ClientAcc);
        Test.stopTest();
        ReportedClaimsCtrl reportdclm1 = new ReportedClaimsCtrl(sc2);
        reportdclm1.hasPreviousDraft = true;
        reportdclm1.pageNumberDraft = 100;
        PageReference pg = reportdclm1.printDraft();
        reportdclm1.manageCaseId = String.valueOf(GardTestData.brokerCase.Id);
        PageReference pg1 = reportdclm1.goToManageClaimsPage();
        System.assertNotEquals(null,pg1);
        reportdclm1.tabName = tabnameStr ;
        reportdclm1.caseData = new Case();
        reportdclm1.year = tabnameStr ;
        reportdclm1.Objects = tabnameStr ;
        reportdclm1.claimType = tabnameStr ;
        reportdclm1.print();
        reportdclm1.exportToExcel();
        reportdclm1.exportToExcelDraft();
        reportdclm1.firstDraft();
        reportdclm1.lastDraft();
        reportdclm1.nextDraft();
        reportdclm1.previousDraft();
        reportdclm1.setpageNumberDraft();
        reportdclm1.caseId = GardTestData.brokerCase.ID;
        reportdclm1.showCaseDetails();
        reportdclm1.getFilterListDraft();
        reportdclm1.filterRecords();
        reportdclm1.getClaimTypeLst();
        reportdclm1.getObjectName();
        reportdclm1.getYearLst(); 
        reportdclm1.mainClaimId = GardTestData.brokerCase.ID;
        update GardTestData.brokerCase;
        
        reportdclm1.deleteRecord();
        reportdclm1.getSortDirection();
        reportdclm1.showClaimsHandler();
        reportdclm1.setpageNumber();
        reportdclm1.previous();
        reportdclm1.next();
        reportdclm1.last();
        reportdclm1.first();
        ReportedClaimsCtrl.getGenerateTimeStamp();
        ReportedClaimsCtrl.getGenerateTime();
        ReportedClaimsCtrl.getUserName();
        reportdclm1.claimsHandlerId = GardTestData.brokerUser.id+'#'+'1';
        reportdclm1.showClaimsHandler();
        reportdclm1.lstCaseForExcel = new list<case>();
        reportdclm1.lstCaseForExcel.add(GardTestData.brokerCase); 
        reportdclm1.getFilterList();
        reportdclm1.lstDraftClaims = new list<case>();
        reportdclm1.lstDraftClaims.add(GardTestData.brokerCase);
        reportdclm1.getFilterListDraft();
        string drft = GardTestData.brokerCase.Status;
        GardTestData.brokerCase.Status = 'Draft';
        update GardTestData.brokerCase;
        reportdclm1.goToManageClaimsPage();
        reportdclm1.manageCaseId = GardTestData.clientCase.id;
        GardTestData.brokerCase.Status = drft;
         //GardTestData.brokerCase.ParentId =  GardTestData.clientCase.id;
        update GardTestData.brokerCase;
         reportdclm1.goToManageClaimsPage();
        System.debug('case>>>>'+[select status,id from case]+'>>>>'+GardTestData.brokerCase.status + '>>>>>'+GardTestData.clientCase.Status);
        reportdclm1.mainClaimId = GardTestData.brokerCase.Id;
        GardTestData.brokerCase.Status = 'Draft';
        update GardTestData.brokerCase;
        reportdclm1.deleteRecord();
        system.debug('origin>>>>'+GardTestData.brokerCase.Origin);
        //GardTestData.brokerCase.Status = 'New';
        //update GardTestData.brokerCase;
        }
    }
}