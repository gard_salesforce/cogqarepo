@isTest
private class TestScheduler_BatchMyCoverHardDelete {
     	public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
     	public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
  public static TestMethod void testScheduler_BatchMyCoverHardDelete() {
       	conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        
        test.StartTest();
        
        // schedule a new job
        String jobId = Scheduler_BatchMyCoverHardDelete.scheduleMe();
        
        // get the newly created trigger record
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, 
            NextFireTime
            FROM CronTrigger WHERE id = :jobId];
        
        // assert that the values in the job are correct
        System.assertEquals(0, ct.TimesTriggered);
        
        test.StopTest();
    }
}