@isTest private class TestEmailMessageReparenting {
	private static Case referenceCase;
    private static List<EmailMessage> duplicateEms = new List<EmailMessage>();
    private static final String fromAddress;
    private static final List<CaseTeam> caseTeams;
    private static final List<String> emailAddresses;
    private static final List<String> ccAddresses;
    private static final Id ctRecTypeId;
    static{
        fromAddress = 'fromAddress@example.com';
        caseTeams = new List<CaseTeam>();
        
        List<Id> caseQueueIds = new List<Id>();
        for(QueueSobject qSO : [SELECT QueueId FROM QueueSobject WHERE sObjectType = 'Case']) caseQueueIds.add(qSo.queueId);
        
        List<String> validCaseTeamNames = new List<String>();
        for(Schema.PickListEntry ple : Case.team__c.getDescribe().getPicklistValues()){
            validCaseTeamNames.add(ple.getValue());
        }
        
        Integer iterationIndex = 0;
        Set<String> existingEmailIds = new Set<String>();
        for(Group queue : [SELECT id,name,email FROM Group WHERE Id IN :caseQueueIds AND email != NULL]){
            existingEmailIds.add(queue.email);
            queue.email = existingEmailIds.contains(queue.email) ? queue.email+''+iterationIndex : queue.email;
            caseTeams.add(new caseTeam(queue.Id,queue.name,validCaseTeamNames[iterationIndex],queue.email));
            iterationIndex++;
            if(iterationIndex == validCaseTeamNames.size()) break;
        }
        ctRecTypeId = [SELECT id,name FROM RecordType WHERE Name LIKE '%customer transaction%'].id;
    }
    
    @TestVisible private static void createData(){
        //QueueCS data creation
        List<QueueInEmail__c> qies = new List<QueueInEmail__c>();
        for(CaseTeam cTeam : caseTeams)
        qies.add(
            new QueueInEmail__c(
                Name = cTeam.teamName,
                Team__c = cTeam.caseTeamName,
                Email__c = cTeam.teamEmail,
                Id__c = cTeam.teamId
            )
        );
        insert qies;
        
        //Parent Case creation
        referenceCase = new Case();
        referenceCase.ownerId = caseTeams.get(0).teamId;
        referenceCase.Team__c = caseTeams.get(0).caseTeamName;
        referenceCase.recordTypeId = ctRecTypeId;
        insert referenceCase;
        
        //EmailMessage creation
        for(Integer index = 0 ; index < caseTeams.size() ; index++){
            duplicateEms.add(
                new EmailMessage(
                    Incoming = true,
                    fromAddress = fromAddress,
                    toAddress = caseTeams.get(0).teamEmail,
                    ccAddress = caseTeams.get(1).teamEmail+';'+caseTeams.get(2).teamEmail,
                    Subject = 'someSubject',
                    textBody = 'someBody',
                    parentId = referenceCase.Id,
                    messageDate = System.now().date()
                )
            );
        }
        insert duplicateEms;
    }
    
    @isTest private static void testEmScheduler(){
        //if(true)return;
        createData();
        EmailMessageJobQueueingBatchScheduler.startDelayedQueue(duplicateEms[0].parentId,duplicateEms[0].fromAddress,duplicateEms[0].messageDate+'');
    }
    
    @isTest private static void testEmBatch(){
        //if(true)return;
        createData();
        String jobName = duplicateEms[0].parentId+EmailMessageReparentingJobs.delimiter+
            +duplicateEms[0].fromAddress+EmailMessageReparentingJobs.delimiter+
            +duplicateEms[0].messageDate+EmailMessageReparentingJobs.delimiter+1;
        Database.executeBatch(new EmailMessageJobQueueingBatchScheduler(jobName));
    }
    
    @isTest private static void testEmJobReparentingQueue(){
        createData();
        String jobName = duplicateEms[0].parentId+EmailMessageReparentingJobs.delimiter+
            +duplicateEms[0].fromAddress+EmailMessageReparentingJobs.delimiter+
            +duplicateEms[0].messageDate+EmailMessageReparentingJobs.delimiter+1;
        System.enqueueJob(new EmailMessageReparentingJobs(jobName));
    }
    
    @isTest private static void testEmJobReparentingQueue2(){
        createData();
        String jobName = duplicateEms[0].parentId+EmailMessageReparentingJobs.delimiter+
            +duplicateEms[0].fromAddress+EmailMessageReparentingJobs.delimiter+
            +duplicateEms[0].messageDate+EmailMessageReparentingJobs.delimiter+1;
        EmailMessageReparentingJobs emrj = new EmailMessageReparentingJobs(jobName);
        emrj.createLists();
        emrj.createChildCloneCases();
        emrj.cloneEMsAndReparentFiles();
    }
    
    class CaseTeam{
        String teamId;
        String teamName;
        String caseTeamName;
        String teamEmail;
        caseTeam(String teamId,String teamName,String caseTeamName,String teamEmail){
            this.teamId = teamId;
            this.teamName = teamName;
            this.caseTeamName = caseTeamName;
            this.teamEmail = teamEmail;
        }
    }    
}