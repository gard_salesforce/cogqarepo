@isTest
public class Test_New_OppChecklist_btn_Ctrl{
public static testMethod void newoppchklistmethod(){
        GardTestData testRecs = new GardTestData();
        testRecs.commonRecord();
        test.startTest();
        //Id RecordTypeIdOpportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('P_I').getRecordTypeId();
        Opportunity opportunity = new Opportunity();
        //opportunity.RecordTypeId = RecordTypeIdOpportunity;
        opportunity.Type = 'New Business';
        opportunity.Name = 'Test opportunity';
        opportunity.AccountId = GardTestData.clientAcc.Id;
        opportunity.StageName = 'Quote';
        opportunity.CloseDate = Date.today();
        opportunity.Business_Type__c = 'Ship Owners';
        opportunity.Amount = 150000;
        opportunity.Agreement_Market_Area__c = GardTestData.Markt.Id;
        opportunity.Area_Manager__c = GardTestData.salesforceLicUser.id;
        opportunity.Approval_Criteria__c = '';
        opportunity.Senior_Approver__c = null;      
        insert opportunity;
        
        Opportunity_Checklist__C cklist = new Opportunity_Checklist__C();
        cklist.Opportunity__c = opportunity.id;
        insert cklist;
        //New_OppChecklist_btn_Ctrl oppChecklistObj = new New_OppChecklist_btn_Ctrl();
        New_OppChecklist_btn_Ctrl.getOpportunites(opportunity.Id);
        New_OppChecklist_btn_Ctrl.getOppChecklists(opportunity.Id);
        New_OppChecklist_btn_Ctrl.getRecordTypes();
        New_OppChecklist_btn_Ctrl.fetchUserInfo();
        New_OppChecklist_btn_Ctrl.deleteRecord(cklist.Id);
        test.stopTest();
    }
}