@isTest
public class testBatchMyCoverHardDelete{
    Public static Account brokerAcc, clientAcc;
    Public static User brokerUser;
    Public static MyCoverList__c coverlist;
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
   // Public static MyCoverList__c coverQuery = [Select id from MyCoverList__c where IsDeleted = true ALL ROWS];
    Public static string coverQuery = 'Select id from MyCoverList__c where IsDeleted = true ALL ROWS';
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Partner Community' and name='Partner Community Login User Custom' limit 1].id;
    Public static Contact clientContact, brokerContact; 
    Public static Gard_Contacts__c grdobj ; 
    public static User salesforceUser;
    public static void commonRecord(){
    
        salesforceUser = new User(
                    Alias = 'standt', 
                    profileId = salesforceLicenseId ,
                    Email='standarduser@testorg.com',
                    EmailEncodingKey='UTF-8',
                    CommunityNickname = 'test13',
                    LastName='Testing',
                    LanguageLocaleKey='en_US',
                    LocaleSidKey='en_US',  
                    TimeZoneSidKey='America/Los_Angeles',
                    UserName='test008@testorg.com',
                    City= 'Arendal'
                    );
        insert salesforceUser; 
        grdobj  =    new  Gard_Contacts__c(
                                                FirstName__c = 'Testreqchng',
                                                LastName__c = 'Baann',
                                                Email__c = 'WR_gards.12@Test.com',
                                                MobilePhone__c = '548645', 
                                                Nick_Name__c  = 'NilsPeter', 
                                                Office_city__c = 'Arendal', 
                                                Phone__c = '5454454',
                                                Portal_Image__c ='<img alt="User-added image" src="https://c.cs8.content.force.com/servlet/rtaImage?eid=a1hL0000000kSyZ&amp;feoid=00NL0000003OySC&amp;refid=0EML00000008Zjs"></img>', 
                                                Title__c = 'test'
                                               );
            insert grdobj ;
        
      
    brokerAcc = new Account(  Name='testBroker',
                                BillingCity = 'Southampton',
                                BillingCountry = 'United Kingdom',
                                BillingPostalCode = 'BS2 AD!',
                                BillingState = 'Avon LAng' ,
                                BillingStreet = '1 Mangrove Road',
                                company_Role__c = 'broker',
                                guid__c = '8ce8ad89-a6ed-1836-9e17',
                                Role_Broker__c = true,
                                Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id,
                                Market_Area__c = TestDataGenerator.getMarketArea().Id,
                                OwnerId = salesforceUser.id
                                );
     insert brokerAcc ;
      brokerContact = new Contact( 
                                        FirstName='Yoo',
                                        LastName='Baooo',
                                        MailingCity = 'Kingsville',
                                        OtherPhone = '1112223356',
                                        mobilephone = '1112223334',
                                        MailingCountry = 'United Kingdom',
                                        MailingPostalCode = 'SE3 1AD',
                                        MailingState = 'London',
                                        MailingStreet = '1 Eastwood Road',
                                        AccountId = brokerAcc.Id,
                                        Email = 'test32@gmail.com'
                                   );
        insert brokerContact  ;
       /*   brokerUser = new User(  Alias = 'standt', 
                                    profileId = partnerLicenseId ,
                                    Email='Claim@gard.no',
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_005',
                                    LanguageLocaleKey='en_US',
                                    CommunityNickname = 'brokerUser',
                                    LocaleSidKey='en_US',  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='mygardtestbrokerUser@testorg.com.mygard',
                                    ContactId = BrokerContact.Id,
                                    contactId__c = grdobj.id,
                                    IsActive = true
                             );
                              
        insert brokerUser; */
       
    
        
     clientAcc = new Account(  Name = 'TestClient', 
                                    Site = '_www.test_1.s', 
                                    Type = 'prospect', 
                                    BillingStreet = 'Gatanfol 1',
                                    BillingCity = 'phuket', 
                                    BillingCountry = 'SWE', 
                                    BillingPostalCode = 'BS1 1AD',
                                    company_Role__c = 'client',
                                    Role_Client__c = true,
                                    Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id,
                                    Market_Area__c = TestDataGenerator.getMarketArea().Id,
                                    OwnerId = salesforceUser.id
                                    );
     insert clientAcc;      
     clientContact= new Contact(   FirstName='sat_1',
                                      LastName='hak',
                                      MailingCity = 'London',
                                      OtherPhone = '1112223356',
                                      mobilephone = '1112223334',
                                      MailingCountry = 'United Kingdom',
                                      MailingPostalCode = 'SE1 1AE',
                                      MailingState = 'London',
                                      MailingStreet = '4 London Road',
                                      AccountId = clientAcc.Id,
                                      Email = 'test321@gmail.com'
                                  );
        insert clientContact; 
    coverlist=new MyCoverList__c(broker_id__c=brokerAcc.id,
                                                 broker__c  =brokerAcc.id,
                                                 //sclaimslead__c='Y',
                                                 client_id__c=clientAcc.id,
                                                 client__c=clientAcc.id,
                                                 expirydate__c='2015-02-19',
                                                 gardshare__c=100,
                                                 onrisk__c=false,
                                                 param__c='abc',
                                                 policyyear__c=2008,
                                                 productarea__c='P&I',
                                                 product__c='P&I Cover',
                                                 UnderwriterId__c=salesforceUser.id,
                                                 underwriter__c=salesforceUser.id,
                                                 userId__c=salesforceUser.id,
                                                 claimslead__c='N',
                                                 viewobject__c=3
                                                // IsDeleted = true
                                                 );
    insert coverlist;
    }
    
    @isTest static void testHardDelete(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        commonRecord();
        MyCoverList__c coverlistTest = [Select id from MyCoverList__c];
        delete coverlistTest ;
        Database.BatchableContext BC;
        Database.QueryLocator QL;
        BatchMyCoverHardDelete BMCDel = new BatchMyCoverHardDelete();
        ID batchprocessid = Database.executeBatch(BMCDel);
        QL = BMCDel.start(BC);
        Database.executeBatch(BMCDel);
       // BMCDel.execute(BC,coverQuery );
        BMCDel.finish(BC); 
    }
}