@IsTest
private class MDMAddressProxyTests {

    private static testmethod void UpsertBillingAddressAsync_Success() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account', billingStreet = 'Test Road', BillingState = 'Test State', BillingPostalCode = 'TE12 3ST', BillingCountry = 'UK');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, true);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMAddressProxy.MDMUpsertBillingAddressAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Billing_Address_Sync_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Synchronised', a.Billing_Address_Sync_Status__c, 'Account Sync Status = Synchronised');
        
    }
    
    private static testmethod void UpsertBillingAddressAsync_Failure() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account', billingStreet = 'Test Road', BillingState = 'Test State', BillingPostalCode = 'TE12 3ST', BillingCountry = 'UK');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, false);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMAddressProxy.MDMUpsertBillingAddressAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Billing_Address_Sync_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Sync Failed', a.Billing_Address_Sync_Status__c, 'Account Sync Status = Sync Failed');
        
    }
    
    private static testmethod void UpsertBillingAddressAsync_NullResponse() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account', billingStreet = 'Test Road', BillingState = 'Test State', BillingPostalCode = 'TE12 3ST', BillingCountry = 'UK');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = null;
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMAddressProxy.MDMUpsertBillingAddressAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Billing_Address_Sync_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Sync Failed', a.Billing_Address_Sync_Status__c, 'Account Sync Status = Sync Failed');
        
    }
    
    private static testmethod void DeletetBillingAddressAsync_Success() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account', billingStreet = 'Test Road', BillingState = 'Test State', BillingPostalCode = 'TE12 3ST', BillingCountry = 'UK');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, true);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMAddressProxy.MDMDeleteBillingAddressAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Billing_Address_Sync_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Synchronised', a.Billing_Address_Sync_Status__c, 'Account Sync Status = Synchronised');
        
    }
    
    private static testmethod void DeleteBillingAddressAsync_Failure() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account', billingStreet = 'Test Road', BillingState = 'Test State', BillingPostalCode = 'TE12 3ST', BillingCountry = 'UK');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, false);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMAddressProxy.MDMDeleteBillingAddressAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Billing_Address_Sync_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Sync Failed', a.Billing_Address_Sync_Status__c, 'Account Sync Status = Sync Failed');
        
    }
    
    private static testmethod void DeleteBillingAddressAsync_NullResponse() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account', billingStreet = 'Test Road', BillingState = 'Test State', BillingPostalCode = 'TE12 3ST', BillingCountry = 'UK');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = null;
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMAddressProxy.MDMDeleteBillingAddressAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Billing_Address_Sync_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Sync Failed', a.Billing_Address_Sync_Status__c, 'Account Sync Status = Sync Failed');
        
    }
    
    private static testmethod void UpsertShippingAddressAsync_Success() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account', ShippingStreet = 'Test Road', ShippingState = 'Test State', ShippingPostalCode = 'TE12 3ST', ShippingCountry = 'UK');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, true);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMAddressProxy.MDMUpsertShippingAddressAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Shipping_Address_Sync_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Synchronised', a.Shipping_Address_Sync_Status__c, 'Account Sync Status = Synchronised');
        
    }
    
    private static testmethod void UpsertShippingAddressAsync_Failure() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account', ShippingStreet = 'Test Road', ShippingState = 'Test State', ShippingPostalCode = 'TE12 3ST', ShippingCountry = 'UK');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, false);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMAddressProxy.MDMUpsertShippingAddressAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Shipping_Address_Sync_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Sync Failed', a.Shipping_Address_Sync_Status__c, 'Account Sync Status = Sync Failed');
        
    }
    
    private static testmethod void UpsertShippingAddressAsync_NullResponse() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account', ShippingStreet = 'Test Road', ShippingState = 'Test State', ShippingPostalCode = 'TE12 3ST', ShippingCountry = 'UK');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = null;
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMAddressProxy.MDMUpsertShippingAddressAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Shipping_Address_Sync_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Sync Failed', a.Shipping_Address_Sync_Status__c, 'Account Sync Status = Sync Failed');
        
    }
    
    private static testmethod void DeletetShippingAddressAsync_Success() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account', ShippingStreet = 'Test Road', ShippingState = 'Test State', ShippingPostalCode = 'TE12 3ST', ShippingCountry = 'UK');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, true);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMAddressProxy.MDMDeleteShippingAddressAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Shipping_Address_Sync_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Synchronised', a.Shipping_Address_Sync_Status__c, 'Account Sync Status = Synchronised');
        
    }
    
    private static testmethod void DeleteShippingAddressAsync_Failure() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account', ShippingStreet = 'Test Road', ShippingState = 'Test State', ShippingPostalCode = 'TE12 3ST', ShippingCountry = 'UK');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, false);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMAddressProxy.MDMDeleteShippingAddressAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Shipping_Address_Sync_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Sync Failed', a.Shipping_Address_Sync_Status__c, 'Account Sync Status = Sync Failed');
        
    }
    
    private static testmethod void DeleteShippingAddressAsync_NullResponse() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account', ShippingStreet = 'Test Road', ShippingState = 'Test State', ShippingPostalCode = 'TE12 3ST', ShippingCountry = 'UK');
        insert a;
        id id = a.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = null;
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMAddressProxy.MDMDeleteShippingAddressAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        a = [SELECT id, Shipping_Address_Sync_Status__c FROM Account WHERE id = :id];
        //System.assertEquals('Sync Failed', a.Shipping_Address_Sync_Status__c, 'Account Sync Status = Sync Failed');
        
    }
}