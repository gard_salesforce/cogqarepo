@isTest (seeAllData = false)
private class testTrigEventAIAU {
    /*public static void createEvent(Event e)
    {
         List<Event> evntLst=new List<Event>();
    }*/
    static testMethod void testTrigEventAIAU(){
        test.startTest();
        
        // create a new event record, this will trigger and test the insert section of the class
      // User user1 = [SELECT id from user LIMIT 1];
      AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
                insert numberofusers;
        String salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
         Gard_Contacts__c grdobj  =    new  Gard_Contacts__c(
                                                        FirstName__c = 'Testreqchng',
                                                        LastName__c = 'Baann',
                                                        Email__c = 'WR_gards.12@Test.com', 
                                                        MobilePhone__c = '548645', 
                                                        Nick_Name__c  = 'NilsPeter', 
                                                        Office_city__c = 'Arendal', 
                                                        Phone__c = '5454454',
                                                        Portal_Image__c ='<img alt="User-added image" src="https://c.cs8.content.force.com/servlet/rtaImage?eid=a1hL0000000kSyZ&amp;feoid=00NL0000003OySC&amp;refid=0EML00000008Zjs"></img>', 
                                                        Title__c = 'testStr' 
                                                       );
                    insert grdobj ;
        User user1 = new User(
                                                Alias = 'standt' , 
                                                profileId = salesforceLicenseId ,
                                                Email='standarduser@testorg.com' ,
                                                EmailEncodingKey='UTF-8',
                                                CommunityNickname = 'test13Str' ,
                                                LastName='testingStr' ,
                                                LanguageLocaleKey= 'en_US',
                                                LocaleSidKey= 'en_US' , 
                                                //contactID = con.id, 
                                                TimeZoneSidKey= 'America/Los_Angeles' ,
                                                UserName='mygardtest008@testorg.com.mygard',
                                                contactId__c = grdobj.id,
                                                City= 'Arendal'

                                           );
                    insert user1;
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
               insert vrcClient;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
                insert Markt;
        Gard_Team__c gt = new Gard_Team__c();
        gt.Active__c = true;
        gt.Office__c = 'test';
        gt.Name = 'GTeam';
        gt.Region_code__c = 'TEST';
        insert gt;
        Country__c country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA',Gard_Team__c=gt.Id,Claims_Support_Team__c=gt.Id);
        insert country;
        Account ac = new Account(  Name = 'TestuuName', 
                                                    Role_Client__c = true,
                                                    Underwriter_4__c = user1.id ,
                                                    Underwriter_main_contact__c=user1.id,
                                                    UW_Assistant_1__c=user1.id,
                                                    U_W_Assistant_2__c=user1.id,
                                                    Claim_handler_Crew__c = user1.id,
                                                    Company_Status__c='Active',
                                                    country__c = country.Id);                                    
                insert ac;
        Account acc = new Account(  Name = 'Testuu', 
                                                    //recordTypeId=System.Label.Client_Contact_Record_Type,
                                                    //Market_Area__c = Markt.id,                                                    Claim_handler_Marine_2_lk__c = salesforceLicUser.id ,
                                                   // X2nd_UWR__c = user1.id ,
                                                    //X3rd_UWR__c = user1.id ,
                                                    Role_Client__c = true,
                                                    //Country__c = 'UK',
                                                    Underwriter_4__c = user1.id ,
                                                    Underwriter_main_contact__c=user1.id,
                                                    UW_Assistant_1__c=user1.id,
                                                    U_W_Assistant_2__c=user1.id,
                                                    Claim_handler_Crew__c = user1.id,
                                                    Company_Status__c='Active',
                                            parentId = ac.id,
                                                    country__c = country.Id);                                    
                insert acc; 
        Account acc1 = new Account(  Name = 'Testuu21', 
                                                    //recordTypeId=System.Label.Client_Contact_Record_Type,
                                                    //Market_Area__c = Markt.id,                                                    Claim_handler_Marine_2_lk__c = salesforceLicUser.id ,
                                                   // X2nd_UWR__c = user1.id ,
                                                    //X3rd_UWR__c = user1.id ,
                                                    Role_Client__c = true,
                                                    //Country__c = 'UK',
                                                    Underwriter_4__c = user1.id ,
                                                    Underwriter_main_contact__c=user1.id,
                                                    UW_Assistant_1__c=user1.id,
                                                    U_W_Assistant_2__c=user1.id,
                                                    Claim_handler_Crew__c = user1.id,
                                                    Company_Status__c='Active',
                                                    country__c = country.Id);                                    
                insert acc1; 
        //Account acc = [SELECT Id from Account where Type='Customer' LIMIT 1];
        //Account acc1 = [SELECT Id from Account where Type='Customer'  and id !=:acc.id LIMIT 1];
        Contact con = new Contact( 
                                                FirstName='Yoo',
                                                LastName='Baooo',
                                                MailingCity = 'Kingsville',
                                                OtherPhone =  '122323' ,
                                                mobilephone =  '2122323' ,
                                                MailingCountry =  'bilCntryStr' ,
                                                MailingPostalCode = 'SE3 1AD',
                                                MailingState =  'mailngStateStr' ,
                                                MailingStreet = '1 Eastwood Road',
                                                AccountId = acc.Id,
                                                Email = 'test32@gmail.com',
                                    Synchronisation_Status__c ='Synchronised'
                                           );
                insert con;
        Contact  con1= new Contact(   FirstName='sat_1',
                                              LastName='hak',
                                              MailingCity =  'London' ,
                                              OtherPhone =  '1112223356' ,
                                              mobilephone =  '1112223334',
                                              MailingCountry =  'SWE',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState =  'London' ,
                                              MailingStreet = '4 London Road',
                                              AccountId = acc1.Id,
                                              Email =  'test_new321@gmail.com',
                                              Primary_Contact__c = true,
                                               Synchronisation_Status__c  ='Synchronised'
                                          );
                insert con1;
        
        //Contact con = [select id from Contact where AccountId = :acc.id LIMIT 1];
        //Contact con1 = [select id from Contact where AccountId != :acc.id LIMIT 1];
        
        Event ev = new Event();
        ev.EndDateTime = system.now()+1;
        ev.Type = 'meeting';
        ev.Subject = 'test activity';
        ev.StartDateTime = system.now();
        ev.WhoId = con.id;
        ev.whatId = acc.Id ;
        ev.Minutes_of_Meeting__c = 'test';
        ev.description = 'testing';
        ev.OwnerId = user1.id;
        insert(ev);
        String evID = ev.Id;
        Meeting_Minute__c meetMin = new Meeting_Minute__c(Event_Id__c = evID , Event_Subject__c = 'Test Subject');
        insert meetMin ;
        // assert that the related meeting minute record has been created
        system.assert([select count() from Meeting_Minute__c where Event_Id__c = :evID] == 1);
        string subj = [Select Event_Subject__c from Meeting_Minute__c where Event_Id__c = :evID].Event_Subject__c;
        
        // update the event record created above, this will trigger and test the update section of the class
       // Event ev2 = [Select Id, Subject, description from Event where Id = :evID LIMIT 1];
        list<task> tskList = [select id FROM task where Meeting_ID__c =: evID];
        delete tskList;
       /* ev2.Subject = 'updated test activity';
        ev2.Type = 'meeting';
        ev2.description = 'test';
        ev2.Meeting_ID__c = ev.id;
        ev2.OwnerId = user1.id;
        ev2.IsDuplicate__c = true;
        ev2.whatId = acc.id;
        ev2.Minutes_of_Meeting__c = 'testing';
        ev2.IsDuplicate__c = false;
        */
        Event ev3 = new Event();
        ev3.Subject = 'updated test activity';
        ev3.Type = 'meeting';
        ev3.description = 'test';
        ev3.Meeting_ID__c = ev.id;
        ev3.OwnerId = user1.id;
        ev3.IsDuplicate__c = true;
        ev3.whatId = acc.id;
        ev3.WhoId = con.id;
        ev3.Minutes_of_Meeting__c = 'testing';
        //ev3.IsDuplicate__c = true;
        ev3.DurationInMinutes = 8;
        ev3.ActivityDateTime = system.now();
        insert ev3;
        EventRelation evtRel = new EventRelation();
        evtRel.isWhat = false;
        evtRel.status = 'new';
        evtRel.EventId = ev.id;
        evtRel.RelationId = con1.id;
        evtRel.IsInvitee = true;
        evtRel.isParent = true;
        insert evtRel;
        //ev3.Description = 'Done';
        //update ev3;
        //update(ev2);
        ev.description = 'test2';
        update ev;
        EventRelationHelper.UpdateChildRecordInsert(ev.Id);
        test.stopTest();
        
        
        meetMin.Event_Subject__c = 'updated test activity';
        update meetMin;
        // assert that the meeting minute record has been updated
        system.assert(subj != [Select Event_Subject__c from Meeting_Minute__c where Event_Id__c = :evID].Event_Subject__c);
        
        //insert opportunity
        Id strRecId =[SELECT Id FROM RecordType WHERE Name='P&I' AND sObjectType='Opportunity' ].id;
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = strRecId ;
        opp.Name = 'APEXTESTOPP001';
        opp.AccountId = acc.Id;
        opp.StageName = 'Risk Evaluation';//Renewable Opportunity
        opp.Type = 'New Business';        
        opp.Business_Type__c = 'MOUs';
        opp.Approval_Criteria__c = 'Self Approval';
        opp.Sales_Channel__c = 'Direct';
        opp.CloseDate = Date.Today();
        opp.Confirm_not_on_sanction_list__c = true;
        opp.Amount = 100;
        insert opp;
        Event evOpp = new Event();
        evOpp.EndDateTime = system.now()+1;
        evOpp.Type = 'meeting';
        evOpp.Subject = 'test activity';
        evOpp.StartDateTime = system.now();
        evOpp.whatId = opp.Id ;
        evOpp.Minutes_of_Meeting__c = 'test';
        evOpp.description = 'testing';
        insert(evOpp);
        Task t = new Task();
        t.Meeting_ID__c  = ev.id;
        t.status = 'New';
        t.subject = ev.subject;
        insert t;
        ev.Whatid = acc1.id;
        update ev;
        Event evOppUpdate = [SELECT id,Subject FROM Event WHERE Id = :evOpp.Id];
        evOppUpdate.Subject = 'opp updated';
        update evOppUpdate;
        Event evOppUpdateDesc = [SELECT id,Subject,description FROM Event WHERE Id = :evOpp.Id];
        evOppUpdateDesc.description = 'test';
        update evOppUpdateDesc;
        Event evtDel = [SELECT id FROM Event WHERE Id =: evOpp.Id];
        delete evtDel;
    }
}