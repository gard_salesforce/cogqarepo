/*************************************************************************************************
    Author      : Martin Gardner
    Company     : cDecisions
    Date Created: 13/09/2013
    Description : Provides a wrapper for the Data Quality settings that can be used from test cases
    Modified    :   
***************************************************************************************************/
public with sharing class DataQualitySupport {
  private static DQ__c testconfig = null;
  
  public static DQ__c GetSetting(String UserId){
    if(Test.isRunningTest() && testconfig != null ){return testconfig;      
    }else{
      DQ__c theobject = DQ__c.getInstance(UserId);
      if(theobject==null){ theobject = new DQ__c();}
      
      if(Test.isRunningTest()){
      	theobject.Enable_Validation__c = true;
        theobject.Enable_Workflow__c = true;
        theobject.Enable_Opportunity_Delete_History__c = true;
        theobject.Enable_Opportunity_Line_Delete_History__c = true;
        theobject.Enable_Contact_Type_Validation__c = true;
        theobject.Enable_Role_Governance__c = true;
		theobject.Enable_Account_Market_Area_Manager__c = true;
        theobject.SetupOwnerId = UserId;
        theobject.Enable_Validation_for_Processing_PSC__c = true;
      }
      return theobject;
    }
  }
  
  public static DQ__c GetSettingForTest(String UserId){
    if(Test.isRunningTest() && testconfig != null ){return testconfig;      
    }else{
      DQ__c theobject = DQ__c.getInstance(UserId);
      
      if(theobject==null){
        theobject = new DQ__c();
        theobject.Enable_Validation__c = true;
        theobject.Enable_Workflow__c = true;
        theobject.Enable_Opportunity_Delete_History__c = true;
        theobject.Enable_Opportunity_Line_Delete_History__c = true;
        theobject.Enable_Contact_Type_Validation__c = true;
        theobject.Enable_Role_Governance__c = true;
		theobject.Enable_Account_Market_Area_Manager__c = true;
        theobject.SetupOwnerId = UserId;
        theobject.Enable_Validation_for_Processing_PSC__c = true;
      }
      return theobject;
    }
  }  
  
  public static DQ__c DisableRoleGovernance(String UserId){
  	DQ__c theobject = DQ__c.getInstance(UserId);
      
      if(theobject == null || Test.isRunningTest()){
		if (theobject == null) {
      		theobject = new DQ__c();
		}
        theobject.Enable_Validation__c = true;
        theobject.Enable_Workflow__c = true;
        theobject.Enable_Opportunity_Delete_History__c = true;
        theobject.Enable_Opportunity_Line_Delete_History__c = true;
        theobject.Enable_Contact_Type_Validation__c = true;
		theobject.Enable_Account_Market_Area_Manager__c = true;
        theobject.SetupOwnerId = UserId;
        theobject.Enable_Validation_for_Processing_PSC__c = true;
      }
      theobject.Enable_Role_Governance__c = false;
      System.Debug('New DQ__C =' + theobject);
      upsert theobject;
      return theobject;
  } 
  
  public static DQ__c GetSetting(){
    if(Test.isRunningTest() && testconfig != null ){return testconfig;      
    }else{
      DQ__c theobject = DQ__c.getInstance();
      
      if(theobject==null){
      	 theobject = new DQ__c();
      }
      if(Test.isRunningTest()){
       
        theobject.Enable_Validation__c = true;
        theobject.Enable_Workflow__c = true;
        theobject.Enable_Opportunity_Delete_History__c = true;
        theobject.Enable_Opportunity_Line_Delete_History__c = true;
        theobject.Enable_Contact_Type_Validation__c = true;
		theobject.Enable_Account_Market_Area_Manager__c = true;
        theobject.Enable_Role_Governance__c = true;
        theobject.Enable_Validation_for_Processing_PSC__c = true;
      }
      return theobject;
    }    
  } 
  

}