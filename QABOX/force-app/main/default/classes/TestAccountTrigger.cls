@isTest
private class TestAccountTrigger {
    public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Market_Area__c Markt;
    public static User salesforceLicUser;
    public static Account clientAcc;
    public static Account nonClientAcc;
    public static Contact clientPrimaryContact;
    public static Contact clientNonPrimaryContact;
    private static List<Valid_Role_Combination__c> validRoles;
    //public static ContactSubscriptionFields__c conSubFields , conSubFields2 , conSubFields3;
    //public static List<ContactSubscriptionFields__c> conFieldsList = new List<ContactSubscriptionFields__c>();
    /*
    public static void createCustomSettingsValue(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList;
    }
    */
    private static void setupValidRoles() {
        validRoles = new List<Valid_Role_Combination__c>();
        validRoles.add(new Valid_Role_Combination__c(Role_Key__c='1', Role__c = 'Client', Sub_Role__c = ''));
        validRoles.add(new Valid_Role_Combination__c(Role_Key__c='2', Role__c = 'Correspondent', Sub_Role__c = ''));
        validRoles.add(new Valid_Role_Combination__c(Role_Key__c='3', Role__c = 'Broker', Sub_Role__c = 'Broker - Insurance Broker'));
        validRoles.add(new Valid_Role_Combination__c(Role_Key__c='4', Role__c = 'External Service Provider', Sub_Role__c = 'ESP - Agent'));
        validRoles.add(new Valid_Role_Combination__c(Role_Key__c='5', Role__c = 'Other', Sub_Role__c = 'Other - Society'));
        insert validRoles;
    }
    
    public static void createCustomSettingsValue(){
        List<ContactSubscriptionFields__c> conFieldsList = new List<ContactSubscriptionFields__c>();
        ContactSubscriptionFields__c conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        ContactSubscriptionFields__c conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        ContactSubscriptionFields__c conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList;
    }
    private static void createUsers(){
        salesforceLicUser = new User(
                                Alias = 'standt', 
                                profileId = salesforceLicenseId ,
                                Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8',
                                CommunityNickname = 'test13',
                                LastName='Testing',
                                LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US',  
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName='test008007@testorg.com'
                                     );
        
        insert salesforceLicUser; 
    }
    
    private static void createClientCompany(){
        Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt; 
        createUsers();        
        
        clientAcc = new Account( Name = 'Test_1', 
                                Site = '_www.test_1.se', 
                                Type = 'Client', 
                                BillingStreet = 'Gatan 1',
                                BillingCity = 'Stockholm', 
                                BillingCountry = 'SWE', 
                                BillingPostalCode = 'BS1 1AD',
                                recordTypeId=System.Label.Client_Contact_Record_Type,
                                Company_Role_Text__c='Client',
                                Market_Area__c = Markt.id,
                                Area_Manager__c = salesforceLicUser.id,
                                OwnerId = salesforceLicUser.id
                               );
        insert clientAcc;
    }
    
    private static void createNonClientCompany(){
        Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt; 
        createUsers();        
        
        nonClientAcc = new Account( Name = 'Test_Non_Client_1', 
                                Site = '_www.test_1.se', 
                                Type = 'Client', 
                                BillingStreet = 'Gatan 1',
                                BillingCity = 'Stockholm', 
                                BillingCountry = 'SWE', 
                                BillingPostalCode = 'BS1 1AD',
                                recordTypeId=System.Label.Broker_Contact_Record_Type,
                                Company_Role_Text__c='Broker',
                                Sub_Roles__c = 'Broker - Insurance Broker',
                                Company_Role__c = 'Broker',
                                Market_Area__c = Markt.id,
                                Area_Manager__c = salesforceLicUser.id,
                                OwnerId = salesforceLicUser.id
                               );
        insert nonClientAcc;
    }
    
    private static void createContactsWithValues(Account createdAccount){
        List<Contact> allContacts = new List<Contact>();
        createCustomSettingsValue();
        clientPrimaryContact= new Contact( FirstName='primary_1',
                              LastName='chak',
                              MailingCity = 'London',
                              MailingCountry = 'United Kingdom',
                              MailingPostalCode = 'SE1 1AE',
                              MailingState = 'London',
                              MailingStreet = '4 London Road',
                              AccountId = createdAccount.Id,
                              Email = 'test321@gmail.com',
                              Primary_Contact__c = true,
                              Publication_Member_circulars__c=false,
                              Publications_Gard_Rules__c=false,
                              Publication_Guidance_to_Master__c=false,
                              Publication_Rules_Number_of_copies__c='5',
                              Publication_GtM_Number_of_Copies__c='10'
                               );
        allContacts.add(clientPrimaryContact);                       
        clientNonPrimaryContact= new Contact( FirstName='nonPrimary_1',
                              LastName='chak',
                              MailingCity = 'London',
                              MailingCountry = 'United Kingdom',
                              MailingPostalCode = 'SE1 1AE',
                              MailingState = 'London',
                              MailingStreet = '4 London Road',
                              AccountId = createdAccount.Id,
                              Email = 'test156@gmail.com',
                              Publication_Member_circulars__c=false,
                              Publications_Gard_Rules__c=false,
                              Publication_Guidance_to_Master__c=false
                               );
        allContacts.add(clientNonPrimaryContact);
                              
        insert allContacts;
    }
    
    private static void createContacts(Account createdAccount){
        List<Contact> allContacts = new List<Contact>();
        createCustomSettingsValue();
        clientPrimaryContact= new Contact( FirstName='primary_1',
                              LastName='chak',
                              MailingCity = 'London',
                              MailingCountry = 'United Kingdom',
                              MailingPostalCode = 'SE1 1AE',
                              MailingState = 'London',
                              MailingStreet = '4 London Road',
                              AccountId = createdAccount.Id,
                              Email = 'test321@gmail.com',
                              Primary_Contact__c = true,
                              Publication_Member_circulars__c=false,
                              Publications_Gard_Rules__c=false,
                              Publication_Guidance_to_Master__c=false
                               );
        allContacts.add(clientPrimaryContact);                       
        clientNonPrimaryContact= new Contact( FirstName='nonPrimary_1',
                              LastName='chak',
                              MailingCity = 'London',
                              MailingCountry = 'United Kingdom',
                              MailingPostalCode = 'SE1 1AE',
                              MailingState = 'London',
                              MailingStreet = '4 London Road',
                              AccountId = createdAccount.Id,
                              Email = 'test156@gmail.com',
                              Publication_Member_circulars__c=false,
                              Publications_Gard_Rules__c=false,
                              Publication_Guidance_to_Master__c=false
                               );
        allContacts.add(clientNonPrimaryContact);
                              
        insert allContacts;
    }
    
    private static void createAccount(){
        createClientCompany();
    }
    private static void createClientCompanyWithContacts(){
        createClientCompany();
        createContacts(clientAcc);
    }
    
    private static void createNonClientCompanyWithContacts(){
        createNonClientCompany();
        createContacts(nonClientAcc);
    }
    
    private static void createClientCompanyWithContactsWithInitialValues(){
        createClientCompany();
        createContactsWithValues(clientAcc);
    }
    
    static testMethod void testClientOnP_I_MemberBehavior(){
        TestAccountTrigger.createClientCompanyWithContacts();
        createCustomSettingsValue();
        setupValidRoles();
        Account fetchedAccount = [SELECT ID,P_I_Member_Flag__c FROM ACCOUNT WHERE Name = 'Test_1' LIMIT 1];
        fetchedAccount.P_I_Member_Flag__c = true;
        
        update fetchedAccount;
        
        Contact fetchedPrimaryContact = [SELECT ID, Publication_Member_circulars__c, Publications_Gard_Rules__c, Publication_Guidance_to_Master__c,
                                    Publication_Rules_Number_of_copies__c,Publication_GtM_Number_of_Copies__c FROM CONTACT
                                    WHERE AccountId =: fetchedAccount.Id AND FirstName='primary_1' LIMIT 1];
        System.assert(fetchedPrimaryContact.Publication_Member_circulars__c);
        System.assert(fetchedPrimaryContact.Publications_Gard_Rules__c);
        System.assert(fetchedPrimaryContact.Publication_Guidance_to_Master__c);
        System.assertEquals(String.valueof(1),fetchedPrimaryContact.Publication_Rules_Number_of_copies__c,1);
        System.assertEquals(String.valueof(1),fetchedPrimaryContact.Publication_GtM_Number_of_Copies__c,1);
        
        Contact fetchedNonPrimaryContact = [SELECT ID, Publication_Member_circulars__c, Publications_Gard_Rules__c, Publication_Guidance_to_Master__c FROM CONTACT
                                    WHERE AccountId =: fetchedAccount.Id AND FirstName='nonPrimary_1' LIMIT 1];
        System.assert(!fetchedNonPrimaryContact.Publication_Member_circulars__c);
        System.assert(!fetchedNonPrimaryContact.Publications_Gard_Rules__c);
        System.assert(!fetchedNonPrimaryContact.Publication_Guidance_to_Master__c);
        
        fetchedAccount.P_I_Member_Flag__c = false;
        
        update fetchedAccount;
        
        fetchedPrimaryContact = [SELECT ID, Publication_Member_circulars__c, Publications_Gard_Rules__c, Publication_Guidance_to_Master__c,
                                    Publication_Rules_Number_of_copies__c,Publication_GtM_Number_of_Copies__c FROM CONTACT
                                    WHERE AccountId =: fetchedAccount.Id AND FirstName='primary_1' LIMIT 1];
        System.assert(fetchedPrimaryContact.Publication_Member_circulars__c);
        System.assert(!fetchedPrimaryContact.Publications_Gard_Rules__c);
        System.assert(!fetchedPrimaryContact.Publication_Guidance_to_Master__c);
        System.assertEquals(String.valueof(1),fetchedPrimaryContact.Publication_Rules_Number_of_copies__c,1);
        System.assertEquals(String.valueof(1),fetchedPrimaryContact.Publication_GtM_Number_of_Copies__c,1);
        
        fetchedNonPrimaryContact = [SELECT ID, Publication_Member_circulars__c, Publications_Gard_Rules__c, Publication_Guidance_to_Master__c FROM CONTACT
                                    WHERE AccountId =: fetchedAccount.Id AND FirstName='nonPrimary_1' LIMIT 1];
        System.assert(!fetchedNonPrimaryContact.Publication_Member_circulars__c);
        System.assert(!fetchedNonPrimaryContact.Publications_Gard_Rules__c);
        System.assert(!fetchedNonPrimaryContact.Publication_Guidance_to_Master__c);                     
    } 
    
    static testMethod void testNonClientOnP_I_MemberBehavior(){
        TestAccountTrigger.createNonClientCompanyWithContacts();
        createCustomSettingsValue();
        setupValidRoles();
        Account fetchedAccount = [SELECT ID,P_I_Member_Flag__c FROM ACCOUNT WHERE Name = 'Test_Non_Client_1' LIMIT 1];
        fetchedAccount.P_I_Member_Flag__c = true;
        
        update fetchedAccount;
        
        Contact fetchedPrimaryContact = [SELECT ID, Publication_Member_circulars__c, Publications_Gard_Rules__c, Publication_Guidance_to_Master__c FROM CONTACT
                                    WHERE AccountId =: fetchedAccount.Id AND FirstName='primary_1' LIMIT 1];
        System.assert(!fetchedPrimaryContact.Publication_Member_circulars__c);
        System.assert(!fetchedPrimaryContact.Publications_Gard_Rules__c);
        System.assert(!fetchedPrimaryContact.Publication_Guidance_to_Master__c);
        
        Contact fetchedNonPrimaryContact = [SELECT ID, Publication_Member_circulars__c, Publications_Gard_Rules__c, Publication_Guidance_to_Master__c FROM CONTACT
                                    WHERE AccountId =: fetchedAccount.Id AND FirstName='nonPrimary_1' LIMIT 1];
        System.assert(!fetchedNonPrimaryContact.Publication_Member_circulars__c);
        System.assert(!fetchedNonPrimaryContact.Publications_Gard_Rules__c);
        System.assert(!fetchedNonPrimaryContact.Publication_Guidance_to_Master__c);
        
        fetchedAccount.P_I_Member_Flag__c = false;
        
        update fetchedAccount;
        
        fetchedPrimaryContact = [SELECT ID, Publication_Member_circulars__c, Publications_Gard_Rules__c, Publication_Guidance_to_Master__c FROM CONTACT
                                    WHERE AccountId =: fetchedAccount.Id AND FirstName='primary_1' LIMIT 1];
        System.assert(!fetchedPrimaryContact.Publication_Member_circulars__c);
        System.assert(!fetchedPrimaryContact.Publications_Gard_Rules__c);
        System.assert(!fetchedPrimaryContact.Publication_Guidance_to_Master__c);
        
        fetchedNonPrimaryContact = [SELECT ID, Publication_Member_circulars__c, Publications_Gard_Rules__c, Publication_Guidance_to_Master__c FROM CONTACT
                                    WHERE AccountId =: fetchedAccount.Id AND FirstName='nonPrimary_1' LIMIT 1];
        System.assert(!fetchedNonPrimaryContact.Publication_Member_circulars__c);
        System.assert(!fetchedNonPrimaryContact.Publications_Gard_Rules__c);
        System.assert(!fetchedNonPrimaryContact.Publication_Guidance_to_Master__c);                     
    } 
    
    static testMethod void testClientOnP_I_MemberBehaviorWithInitialValues(){
        TestAccountTrigger.createClientCompanyWithContactsWithInitialValues();
        createCustomSettingsValue();
        setupValidRoles();
        Account fetchedAccount = [SELECT ID,P_I_Member_Flag__c FROM ACCOUNT WHERE Name = 'Test_1' LIMIT 1];
        fetchedAccount.P_I_Member_Flag__c = true;
        
        update fetchedAccount;
        
        Contact fetchedPrimaryContact = [SELECT ID, Publication_Member_circulars__c, Publications_Gard_Rules__c, Publication_Guidance_to_Master__c,
                                    Publication_Rules_Number_of_copies__c,Publication_GtM_Number_of_Copies__c FROM CONTACT
                                    WHERE AccountId =: fetchedAccount.Id AND FirstName='primary_1' LIMIT 1];
        System.assert(fetchedPrimaryContact.Publication_Member_circulars__c);
        System.assert(fetchedPrimaryContact.Publications_Gard_Rules__c);
        System.assert(fetchedPrimaryContact.Publication_Guidance_to_Master__c);
        System.assertEquals(String.valueof(5),fetchedPrimaryContact.Publication_Rules_Number_of_copies__c,5);
        System.assertEquals(String.valueof(10),fetchedPrimaryContact.Publication_GtM_Number_of_Copies__c,10);
        
        Contact fetchedNonPrimaryContact = [SELECT ID, Publication_Member_circulars__c, Publications_Gard_Rules__c, Publication_Guidance_to_Master__c FROM CONTACT
                                    WHERE AccountId =: fetchedAccount.Id AND FirstName='nonPrimary_1' LIMIT 1];
        System.assert(!fetchedNonPrimaryContact.Publication_Member_circulars__c);
        System.assert(!fetchedNonPrimaryContact.Publications_Gard_Rules__c);
        System.assert(!fetchedNonPrimaryContact.Publication_Guidance_to_Master__c);
        
        fetchedAccount.P_I_Member_Flag__c = false;
        
        update fetchedAccount;
        
        fetchedPrimaryContact = [SELECT ID, Publication_Member_circulars__c, Publications_Gard_Rules__c, Publication_Guidance_to_Master__c,
                                    Publication_Rules_Number_of_copies__c,Publication_GtM_Number_of_Copies__c FROM CONTACT
                                    WHERE AccountId =: fetchedAccount.Id AND FirstName='primary_1' LIMIT 1];
        System.assert(fetchedPrimaryContact.Publication_Member_circulars__c);
        System.assert(!fetchedPrimaryContact.Publications_Gard_Rules__c);
        System.assert(!fetchedPrimaryContact.Publication_Guidance_to_Master__c);
        System.assertEquals(String.valueof(5),fetchedPrimaryContact.Publication_Rules_Number_of_copies__c,5);
        System.assertEquals(String.valueof(10),fetchedPrimaryContact.Publication_GtM_Number_of_Copies__c,10);
        
        fetchedNonPrimaryContact = [SELECT ID, Publication_Member_circulars__c, Publications_Gard_Rules__c, Publication_Guidance_to_Master__c FROM CONTACT
                                    WHERE AccountId =: fetchedAccount.Id AND FirstName='nonPrimary_1' LIMIT 1];
        System.assert(!fetchedNonPrimaryContact.Publication_Member_circulars__c);
        System.assert(!fetchedNonPrimaryContact.Publications_Gard_Rules__c);
        System.assert(!fetchedNonPrimaryContact.Publication_Guidance_to_Master__c);
    }
    
    static testMethod void testAccountGUID(){
        TestAccountTrigger.createClientCompanyWithContacts();
        createCustomSettingsValue();
    }
    public static TestMethod void testAccountTriggerMethod() {
        test.StartTest();
        setupValidRoles();
        PEME_Enrollment_Form__c pef = new PEME_Enrollment_Form__c();
        pef.Enrollment_Status__c = 'Enrolled';
        insert pef;
        
        PEME_Manning_Agent__c pma = new PEME_Manning_Agent__c();
        pma.PEME_Enrollment_Form__c = pef.id;
        insert pma;
        
        PEME_Debit_note_detail__c pbnd = new PEME_Debit_note_detail__c();
        pbnd.PEME_Enrollment_Form__c = pef.id;
        insert pbnd;
        
        //Insert Account
        Account account = new Account();    
        account.Name = 'Customer Test Account';
        account.Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().id;
        account.Market_Area__c = TestDataGenerator.getMarketArea().Id;    
        account.Synchronisation_Status__c = 'Synchronised';
        account.Billing_Address_Sync_Status__c = 'Synchronised';
        account.Shipping_Address_Sync_Status__c = 'Synchronised';
        account.Roles_Sync_Status__c  = 'Synchronised'; 
        account.Company_Status__c  = 'Active';
        account.Company_Role__c = 'Broker;'; 
        account.Sub_Roles__c = 'Broker - Insurance Broker';
        account.Manning_Agent_or_Debit_Note_ID__c = pma.id; 
        insert account;
        
        Contact con = new Contact();
        con = TestDataGenerator.createContactFor(account);
        con.Primary_Contact__c = true;
        update con;
        
        //Update Account
        account.notifyAdmin__c = true;
        account.Company_Status__c = 'Inactive';
        account.KYC_exception_DMS_ref__c ='ONE';
        account.Company_Role__c = 'Client;';
        //update account;
        
        //account.Account_Sync_Status__c = 'Sync Failed';
        account.Company_Role__c = 'External Service Provider;';
        account.Sub_Roles__c = 'ESP - Agent';
        account.notifyAdmin__c = false;
        update account;
        
        account.Company_Role__c = 'Correspondent;';
        account.notifyAdmin__c = false;
        update account;
        
        //Insert Account
        Account account2 = new Account();    
        account2.Name = 'Customer Test Account';
        account2.Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().id;
        account2.Market_Area__c = TestDataGenerator.getMarketArea().Id;    
        account2.Synchronisation_Status__c = 'Synchronised';
        account2.Billing_Address_Sync_Status__c = 'Synchronised';
        account2.Shipping_Address_Sync_Status__c = 'Synchronised';
        account2.Roles_Sync_Status__c  = 'Synchronised'; 
        account2.Company_Status__c  = 'Active';
        account2.Company_Role__c = 'Other'; 
        account2.Sub_Roles__c = 'Other - Society';
        account2.Manning_Agent_or_Debit_Note_ID__c = pbnd.id;   
        insert account2;
        //update account
        con.accountID = account2.Id;
        update con;
        account2.Billing_Street_Address_Line_1__c = 'test address';
        update account2;
        
        test.StopTest();
    }
}