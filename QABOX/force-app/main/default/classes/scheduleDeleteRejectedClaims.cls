global class scheduleDeleteRejectedClaims implements schedulable
{
    global void execute(SchedulableContext sc)
    {
        BatchDeleteRejectedClaims bdr = new BatchDeleteRejectedClaims();
        database.executebatch(bdr);
    }
}