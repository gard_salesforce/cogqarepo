global class reconcileCompanies implements Database.Batchable<sObject> {
    global String query;
    global String AccFields;
    global String PSCFields;
    global List<Reconciliation_Mapping__c> Mappings;
    public boolean IsTest {get;set;}
                    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        //check whether records need to be processed
        if (scope.size()>0) {
            //get parameters from custom settings
            boolean bProcessAll;       // stores whether we want to process all differences, regardless of whether there's a Salesforce history record for the mapping field
            date dProcessAfterDate;    // stores the earliest date we want to process Salesforce history records for
            boolean bAddRecons;        // whether to add the temp reconciliations collection to the collection which will be written to SF
            CompanyReconciliation__c customsettings = CompanyReconciliation__c.getInstance();
            bProcessAll = customsettings.ProcessAllDifferences__c;
            dProcessAfterDate = customsettings.ProcessAfterDate__c;
            if (IsTest==true) {bProcessAll = true;}  // overrides custom setting if we're running a test
            
            //create output collection for DML insert
            List<Company_Reconciliation__c> lsRecon = new List<Company_Reconciliation__c>();
            //create collection to temporarily store all reconciliation differences
            List<Company_Reconciliation__c> lsReconTemp = new List<Company_Reconciliation__c>();
            
            //get the SF Company records
            String strAccIds = '';
            for (sObject ascrec: scope) {strAccIds+='\''+ascrec.get('Salesforce_Company__c')+'\',';}
            strAccIds = strAccIds.substring(0, strAccIds.length()-1);
            System.debug('### AccountIds:'+strAccIds);
            String strAccQuery = 'SELECT Id, ' + AccFields
                                 +'FROM Account '
                                 +'WHERE Id IN ('+strAccIds+')';
            //System.debug('### Account query: '+strAccQuery);
            List<sObject> lsAcc = Database.query(strAccQuery);
            //check through all ASC records
            Account comprec;
            for (sObject ascrec: scope) {
                bAddRecons = false;
                //find the company record to compare to
                for (integer i=0; i<lsAcc.size(); i++) {
                    if (lsAcc[i].get('Id')==ascrec.get('Salesforce_Company__c')) {comprec = (Account)lsAcc[i];}
                }
                System.debug('### Comparing record '+ascrec.get('Id')+':'+ascrec.get('Name')+' to '+comprec.Id+':'+comprec.Name);
                //compare the asc fields to the company fields
                for (Reconciliation_Mapping__c mp: Mappings) {
                    if (ascrec.get(mp.Admin_System_Field__c)!=comprec.get(mp.Company_Field__c)) {
                        //if fields different add to temporary collection
                        lsReconTemp.add(new Company_Reconciliation__c(
                                        Admin_System_Company__c=String.valueOf(ascrec.get('Id')), 
                                        ASC_Field__c=String.valueOf(mp.Admin_System_Field__c), 
                                        ASC_Value__c=String.valueOf(ascrec.get(mp.Admin_System_Field__c)), 
                                        Company__c=String.valueOf(comprec.get('Id')), 
                                        Company_Field__c=String.valueOf(mp.Company_Field__c), 
                                        Company_Value__c=String.valueOf(comprec.get(mp.Company_Field__c))));
                        // if ProcessAll is true OR
                        // if ProcessAll is false AND a history record exists for the salesforce company, after the earliest date, for a matching field
                        // then set the flag to indicate all differences between mapped fields should be added to the reconciliation report.
                        system.debug('##### exist hist rec0:'+String.valueOf(comprec.get('Id')));
                        system.debug('##### exist hist rec1:'+dProcessAfterDate);
                        system.debug('##### exist hist rec2:'+String.valueOf(mp.Company_Field__c));
                        system.debug('##### exist hist rec2:'+existsHistoryRecord(String.valueOf(comprec.get('Id')),dProcessAfterDate,String.valueOf(mp.Company_Field__c)));
                        if (bProcessAll == true || (bProcessAll == false && existsHistoryRecord(String.valueOf(comprec.get('Id')),dProcessAfterDate,String.valueOf(mp.Company_Field__c)))) {
                            bAddRecons = true;
                        }
                    }
                }
                // Once all reconciliation fields are checked see:
                // - whether at least one of the mapped fields has changed in Salesforce since the date specified in the custom settings
                // - or the process all flag is set
                // if either of the above are true (ie the bAdRecons flag is set) then add all mapped field differences to the collection
                // which will be written to Salesforce
                if (bAddRecons == true){
                    for (Company_Reconciliation__c rec: lsReconTemp) {
                        lsRecon.add(rec);
                    }
                }
                lsReconTemp.clear();
            }
            if (lsRecon.size()>0) {insert lsRecon;}
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        //ASSERT: do nothing
    }
    
    boolean existsHistoryRecord(string idAccount, date dStartDate, string sField)
    {
        boolean bRetVal = false;
        AccountHistory[] his;
        if (dStartDate != null) {    // if we have a date field use that to restrict how back back we look for history records
            his = [Select AccountId from accountHistory WHERE 
                    AccountId = :idAccount AND
                    Field = :sField AND
                    CreatedDate >= :dStartDate
                    LIMIT 1];
        }
        else {   // if no date field do a search over all history records, regardless of date
            his = [Select AccountId from accountHistory WHERE 
                    AccountId = :idAccount AND
                    Field = :sField
                    LIMIT 1];        
        }
        if (his.size() > 0)
            bRetVal = true;
        else
            bRetVal = false;

        return bRetVal;    
    }
    
    public static testmethod void runTest() {
        List<Reconciliation_Mapping__c> lsMap = [SELECT Id, Name, Company_Field__c, Admin_System_Field__c 
                                                 FROM Reconciliation_Mapping__c 
                                                 ORDER BY Company_Field__c];
        delete lsMap;
        insert new Reconciliation_Mapping__c (Company_Field__c='Website',Admin_System_Field__c='Company_Website__c');
        lsMap = [SELECT Id, Name, Company_Field__c, Admin_System_Field__c 
                                                 FROM Reconciliation_Mapping__c 
                                                 ORDER BY Company_Field__c];
                                                 
        // create test salesforce account
        Account testacc = TestDataGenerator.getClientAccount();//new Account(Name='testaccount');
        //insert(testacc);
        // add a test value to the mapped field
        testacc.Website = 'sfwebsite.com';
        update(testacc);
        string idAcc = testacc.Id;
        
        system.debug('######Salesforce Company ID:'+idAcc);        

        // create test production system company
        Admin_System_Company__c testasc = new Admin_System_Company__c(Name='testasc',Salesforce_Company__c=idAcc,Company_Website__c='pscwebsite.com');
        insert(testasc);
        string idAsc = testasc.Id;
        
        String strAccFields = 'Name, ';
        String strPSCFields = 'Name, ';
        for (Reconciliation_Mapping__c rec: lsMap) {
            strAccFields+=rec.Company_Field__c+', ';
            strPSCFields+=rec.Admin_System_Field__c+', ';
        }
        strAccFields = strAccFields.substring(0, strAccFields.length()-2)+' ';
        strPSCFields = strPSCFields.substring(0, strPSCFields.length()-2)+' ';
        
        reconcileCompanies rc = new reconcileCompanies();
        rc.query = 'SELECT Id, Salesforce_Company__c, ' + strPSCFields
                  +'FROM Admin_System_Company__c '
                  +'WHERE Salesforce_Company__c = \'' + idAcc + '\' '
                  +'ORDER BY Name LIMIT 10';
        rc.AccFields = strAccFields;
        rc.PSCFields = strPSCFields;
        rc.Mappings = lsMap;
        //need to override standard behaviour as test classes appear not to create field update history records
        rc.IsTest = true;
        
        Test.startTest();        
        Database.executeBatch(rc);
        Test.stopTest();
        
        // assert that the company reconciliation record has been created
        system.assert([Select count() from Company_Reconciliation__c where 
            Admin_System_Company__c = :idAsc AND 
            Company__c = :idAcc AND
            ASC_Field__c = :lsMap[0].Admin_System_Field__c AND
            ASC_Value__c = 'pscwebsite.com' AND
            Company_Field__c = :lsMap[0].Company_Field__c AND
            Company_Value__c = 'sfwebsite.com'] == 1);
    }
    
}