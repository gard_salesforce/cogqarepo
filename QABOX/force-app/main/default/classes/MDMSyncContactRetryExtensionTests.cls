@isTest
private class MDMSyncContactRetryExtensionTests {
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
	private static List<Valid_Role_Combination__c> validRoles;
	
	private static Account setupTestAccount() {
		return new Account (Name = 'Unit Test Account', Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
	}
	
	private static Contact setupTestContact(id AccountId) {
		return new Contact (FirstName = 'Bill', LastName = 'Smith', AccountId = AccountId);
	}
	
	private static void setupValidRoles() {
		////For some reason this seems to be throwing an internal Salesforce error /
		////System.UnexpectedException: Salesforce System Error: 1583762872-13077 (1716107030) (1716107030)
		//validRoles = (List<Valid_Role_Combination__c>)Test.loadData(Valid_Role_Combination__c.sObjectType, 'MDMValidRoles');
		////END...
		validRoles = new List<Valid_Role_Combination__c>();
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 1', Sub_Role__c = 'Sub Role 11'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 1', Sub_Role__c = 'Sub Role 12'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 2', Sub_Role__c = 'Sub Role 21'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 2', Sub_Role__c = 'Sub Role 22'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Other', Sub_Role__c = 'Other Sub Role'));
		insert validRoles;
	}
	
	static testMethod void MDMSyncAccountRetryExtension_Coverage() {
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
		///ARRANGE...
		setupValidRoles();
		Account a = setupTestAccount();
		a.Synchronisation_Status__c = 'Sync Failed';
		a.Billing_Address_Sync_Status__c = 'Sync Failed';
		a.Shipping_Address_Sync_Status__c = 'Sync Failed';
		insert a;
		
		Contact c = setupTestContact(a.Id);
		c.Synchronisation_Status__c = 'Sync Failed';
		insert c;
		
		Apexpages.currentPage().getParameters().put('id',c.Id);     
		ApexPages.StandardController sc = new ApexPages.standardController(c);
        MDMSyncContactRetryExtension retryExt = new MDMSyncContactRetryExtension(sc);
		
		///ACT...
		Test.startTest();
		boolean b = retryExt.IsAdmin;
		string s = retryExt.redirectUrl;
		boolean b2 = retryExt.shouldRedirect;
		retryExt.RetryContactSync();
		Test.stopTest();
		
		///ASSERT...
		c = [SELECT id, Synchronisation_Status__c FROM Contact WHERE id = :c.id];
		System.assertEquals('Sync In Progress', c.Synchronisation_Status__c, 'Contact sync status should be sync in progress');
	}
}