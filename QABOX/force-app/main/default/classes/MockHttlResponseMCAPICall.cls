@isTest
global class MockHttlResponseMCAPICall implements HttpCalloutMock {
   
    global HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        
        if(req.getBody().contains('clientId'))
        {
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"accessToken": "YOUR_ACCESS_TOKEN","expiresIn": 3600}');            
            res.setStatusCode(200);
            return res;
        }
        else if (req.getBody().contains('Address'))
        {
            res.setHeader('Content-Type', 'application/json');      
            res.setBody('{"To": {"Address": "a@c.com","SubscriberKey": "a@c.com"},"ContactAttributes": {"SubscriberAttributes": {"fromAddress": "b@a.com", "fromName": "test","Title": "testTitle", "LinkText": "testLinkText", "Organiser": "testName", "date": "testDate", "EventType": "testType"}}},"OPTIONS": {"RequestType": "SYNC"}}');
            res.setStatus('Accepted');
            res.setStatusCode(202);
            return res;
        }      
        else return res;
    }
}