public without sharing class ViewDocCtrlHelper {
    public static boolean hasPIAccess{get;set;}
    public static boolean hasMarineAccess{get;set;}
    public ViewDocCtrlHelper() {
     
    }
    public static Map<String,List<Contract>> getClientwithMultipleBroker(String pdClient,String pdBroker,List<String> policyYear,List<string> prodArea)
    {
       
        
        Map<String,List<Contract>>agreementMap=new Map<String,List<contract>>();
       /*   system.debug('pdClient ****'+pdClient );
        system.debug('policyYear***********'+policyYear);
        system.debug('pdBroker***********'+pdBroker);
         system.debug('prodArea***********'+prodArea);
              List<Asset>coverList=new List<Asset> ([Select agreement__c,agreement_r.broker__r.company_id__c,object__r.Object_Unique_ID__c,On_risk_indicator__c From Asset 
                                                               where agreement__r.client__r.company_id__c=:pdClient
                                                               and  agreement__r.broker__r.company_id__c=: pdBroker
                                                                AND Policy_Year__c IN:policyYear AND Business_Area__c IN : prodArea ]*/
         
  
          List<Contract>agreementList=new List<Contract>([select id,Business_Area__c ,broker__r.company_id__c,client__c,Policy_Year__c ,(SELECT object__r.Object_Unique_ID__c,object__r.name,On_risk_indicator__c FROM Assets__r)  FROM Contract  
                                                          WHERE Client__r.company_id__c=: pdClient
                                                          AND broker__r.company_id__c=: pdBroker
                                                          AND Policy_Year__c IN:policyYear AND Business_Area__c IN : prodArea
                                                          AND   Revoke_Broker_Access__c = false ]);
       
           system.debug('agreementList****'+agreementList);
        for(Contract agreement:agreementList )
                                                                                                     
                                                                                                    
        {   
             system.debug('agreement.broker__c*****'+agreement.broker__c);  
             system.debug('agreement.broker__r.company_id__c*****'+agreement.broker__r.company_id__c);   
            
             
              if(agreement.broker__c!=null && agreementMap.containsKey(agreement.broker__r.company_id__c))
              {
                agreementMap.get(agreement.broker__r.company_id__c).add(agreement);
              }
              else if(agreement.broker__c!=null)
              {
                agreementMap.put(agreement.broker__r.company_id__c,new List<Contract>{agreement});
              }       
         }
         system.debug('agreementMap******'+agreementMap);
         return agreementMap;
    }
    public static DocumentResponseWrapper GetAgreementDetailsForClientandBroker(DocumentResponseWrapper documentResponseWrapper,Map<String,List<Contract>>agreementMap,String Pd_broker,String  UserType,String pdClient)
    {
        hasPIAccess=false;
        hasMarineAccess=false;
        List<DocumentResponseWrapper.ResultObject> resultObjectList=new  List<DocumentResponseWrapper.ResultObject>();
         Set<string>objectSet=new Set<String>();
         DocumentResponseWrapper clientwithMultiBrokerResponseWrapper=new DocumentResponseWrapper(); // new instance of DocumentResponseWrapper
         clientwithMultiBrokerResponseWrapper.resultObjects=new List<DocumentResponseWrapper.ResultObject>();
        List<String>policyYearList=new List<String>();
        
        if(userType.equalsIgnoreCase(Label.Broker)  && Pd_broker!=null)
        {
          //  Map<id,List<Contract>>agreementMap=getClientwithMultipleBroker(pdClient);
            if(agreementMap!=null && agreementMap.size() > 0)
            {
                
                if(agreementMap.containsKey(Pd_broker))
                {
                    for(Contract agreement:agreementMap.get(Pd_broker))
                    {
                        
                       /* if(String.isEmpty(agreement.Policy_Year__c )){
                        policyYearList.add(Policy_Year__c );
                        }*/
                        if(agreement.Business_Area__c.equalsIgnoreCase('Marine')){
                             hasMarineAccess=true;
                         }
                         if(agreement.Business_Area__c.equalsIgnoreCase('P&I')){
                             hasPIAccess=true;
                         }        
                        for(Asset cover:agreement.assets__r)
                        {
                           if(cover.object__c!=null /*&& cover.On_risk_indicator__c*/){
                            objectSet.add((cover.object__r.name).toUpperCase());
                            }
                        }
                                     
                    }
                }
                system.debug('objectSet*****'+objectSet);
                Map<String,MyGardDocumentType__c >docTypeMap=new Map<String,MyGardDocumentType__c>();
                 List<MyGardDocumentType__c> allDocTypes = MyGardDocumentType__c.getall().values();
                 for(MyGardDocumentType__c mgDoc : allDocTypes){
                 docTypeMap.put(mgDoc.Document_Name__c,mgDoc);
                 }
                if(documentResponseWrapper!=null )
                {
                    //filter the object from the response
                    for(DocumentResponseWrapper.ResultObject result:documentResponseWrapper.ResultObjects)
                    {
                       /* system.debug('result.ObjectProperties***'+result.ObjectProperties);
                        system.debug('P&I access***'+hasPIAccess);
                        system.debug('objectSet******************'+objectSet);
                        system.debug('result.ObjectProperties.pd_objectnameondocument**'+result.ObjectProperties.pd_objectnameondocument);
                         system.debug('policy doc type ****'+docTypeMap.get(result.ObjectProperties.pd_policydocumenttype));*/
                         if(result.ObjectProperties != null && 
                            ((result.ObjectProperties.pd_certificatetype !=null && 
                            result.ObjectProperties.pd_object!=null &&
                            objectSet.contains((result.ObjectProperties.pd_object).toUpperCase())&& docTypeMap.get(result.ObjectProperties.pd_certificatetype)!=null &&
                              ((docTypeMap.get(result.ObjectProperties.pd_certificatetype).P_I_Document__c && hasPIAccess) || (docTypeMap.get(result.ObjectProperties.pd_certificatetype).Marine_Document__c && hasMarineAccess)  ))
                             || (result.ObjectProperties.pd_policydocumenttype !=null  && !String.isEmpty(result.ObjectProperties.pd_objectnameondocument) && 
   objectSet.contains((result.ObjectProperties.pd_objectnameondocument).toUpperCase()) && ((docTypeMap.get(result.ObjectProperties.pd_policydocumenttype).P_I_Document__c && hasPIAccess) || 
 (docTypeMap.get(result.ObjectProperties.pd_policydocumenttype).Marine_Document__c && hasMarineAccess)  ) )
                             
                             
                             )){
                            resultObjectList.add(result);
                         }
                      
                    }
                    clientwithMultiBrokerResponseWrapper.result=documentResponseWrapper.Result;
                    clientwithMultiBrokerResponseWrapper.Count=resultObjectList.size(); //need to check the count value
                    clientwithMultiBrokerResponseWrapper.MoreResultsAvailable=documentResponseWrapper.MoreResultsAvailable;
                    clientwithMultiBrokerResponseWrapper.ObjectType=documentResponseWrapper.ObjectType;
                    clientwithMultiBrokerResponseWrapper.ObjectTypeAlias=documentResponseWrapper.ObjectTypeAlias;
                    system.debug('documentResponseWrapper.ObjectTypeAlias****'+documentResponseWrapper.ObjectTypeAlias);
                    system.debug('clientwithMultiBrokerResponseWrapper.ResultObjects****'+clientwithMultiBrokerResponseWrapper.ResultObjects);
                    clientwithMultiBrokerResponseWrapper.ResultObjects.addAll(resultObjectList);
                }
            }
        } 
        return  clientwithMultiBrokerResponseWrapper;   
    }
}