@isTest(seeAllData=true)

private class Test_idea_chatter_post_Trigger 
{
  private static testMethod void createIdea()
  {
    Test.startTest();
    // Create an Idea
    Idea myIdea = new Idea(CommunityId = '09a20000000M3QWAA0');
    {
      myIdea.title = 'Test Idea Title';
      myIdea.body = ' test idea body';
      myIdea.categories = 'Salesforce';
      myIdea.CurrencyIsoCode = 'USD';
      myIdea.Status = 'Not Yet Reviewed';
      insert myIdea;
    }
    // Verification
    System.assertEquals(myIdea.Status, 'Not Yet Reviewed'); 
    // Update an Idea
    myIdea.Status = 'Completed';
    update myIdea;
    // Verification
    System.assertEquals(myIdea.Status, 'Completed'); 
    FeedItem ChatterFeed = [Select body From FeedItem where type = 'TextPost' and visibility = 'allusers' and createddate >= today ORDER BY CreatedDate desc limit 1];
    System.assertEquals(ChatterFeed.body.startswith('Your Idea'),True); 

    Test.stopTest();
  }
}