public class ContactOperations{
	public void doInsertContacts(List<Contact> allContacts){
		Database.insert(allContacts);
	}
	
	public void doUpdateContacts(List<Contact> allContacts){
		Database.update(allContacts);
	}
	
	public void doDeleteContacts(List<Contact> allContacts){
		Database.delete(allContacts);
	}
	
	public void doUpsertContacts(List<Contact> allContacts){
		Database.upsert(allContacts);
	}
}