global class BatchCorrespodentPIHullFlagUpdate implements Database.Batchable<sObject> {
    public String query;
    /* global batchCorrespodentPIHullFlagUpdate(){
        query = 'SELECT Id,Company__c,Stop_Date_P_I__c ,Stop_Date_Hull__c FROM CorrespondentPort__c';
    } */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        query = 'SELECT Id,Company__c,Stop_Date_P_I__c ,Stop_Date_Hull__c FROM CorrespondentPort__c';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<CorrespondentPort__c> scope) {
        system.debug('--batch class called--');
        //list<CorrespondentPort__c> allCorresPortsForBatch = [SELECT Id,Company__c,Stop_Date_P_I__c ,Stop_Date_Hull__c FROM CorrespondentPort__c];
        Set<CorrespondentPort__c> setToBeUpdated = new Set<CorrespondentPort__c>();
        for(CorrespondentPort__c corr:scope){
            /*
            if(corr.Stop_Date_P_I__c != null && corr.Stop_Date_P_I__c < date.today()){
                setToBeUpdated.add(corr);
            }
            if(corr.Stop_Date_Hull__c != null && corr.Stop_Date_Hull__c < date.today()){
                setToBeUpdated.add(corr);
            }
            */
            setToBeUpdated.add(corr);
        }
        system.debug('list to be updated --- > '+setToBeUpdated);
        system.debug('list size to be updated --- > '+setToBeUpdated.size());
        List<CorrespondentPort__c> listToBeUpdated = new List<CorrespondentPort__c>();
        listToBeUpdated.addAll(setToBeUpdated);
        system.debug('to be updated--> '+listToBeUpdated);
        if(listToBeUpdated.size()>0){
            update listToBeUpdated;
        }
    }
    global void finish(Database.BatchableContext BC) {
        //ASSERT: do something here
    }
}