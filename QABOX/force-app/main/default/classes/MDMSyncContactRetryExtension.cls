public with sharing class MDMSyncContactRetryExtension {

	private final Contact contact;
	
	public String redirectUrl {public get; private set;}
    public Boolean shouldRedirect {public get; private set;}
	
	public Boolean IsAdmin { 
		get { 
			MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
			return mdmSettings.Retry_Buttons_Enabled__c;
		} 
	}
	
	public MDMSyncContactRetryExtension (ApexPages.StandardController stdController) {
    	this.contact = (Contact)stdController.getRecord();
		redirectUrl = stdController.view().getUrl();
		shouldRedirect = false;
    }
	
	public ApexPages.PageReference RetryContactSync() {
		shouldRedirect = true;
		//Hopefully a better way of doing this than below as needs no logic here and will lock the account until the future method is completed...
		contact.Synchronisation_Status__c = 'Sync In Progress';
		update contact;
		/*
		if (!contact.Deleted__c && contact.Synchronisation_Status__c == 'Sync Failed') {
			MDMContactProxy.MDMUpsertContactAsync(new List<id> { contact.Id });	
		} else if (contact.Deleted__c && contact.Synchronisation_Status__c == 'Sync Failed') {
			MDMContactProxy.MDMDeleteContactAsync(new List<id> { contact.Id });
		}
		*/
		return null;
	}

}