public class UtilityData{
    //Job Identifiers
    public String sObjectName;
    public String service;
    
    //Job-specific fields
    public String maskEmailId;
    public Integer batchSize;
    
    //Default fields
    private static final Map<String,List<String>> sObjectToServicesMap = new Map<String,List<String>>{
        'account' => new List<String>{'emailmasking'},
        'case' => new List<String>{'emailmasking'},
        'contact' => new List<String>{'emailmasking'},
        'gard_contacts__c' => new List<String>{'emailmasking'},
        'gard_team__c' => new List<String>{'emailmasking'},
        'contract' => new List<String>{'onrisking'},
        'asset' => new List<String>{'onrisking'}
    };
    private static final String defaultMaskEmailId = 'defaultMaskingEmail@example.com';
    
    
    //Constructor to initialize with default values - prevents null exception
    public UtilityData(){
        batchSize = 100;
        maskEmailId = defaultMaskEmailId;
    }
    
    //if atleast minimal data is given to run a Job
    public Boolean isValid(){
        return sObjectToServicesMap.get(this.sObjectName.toLowerCase()).contains(this.service.toLowerCase());
    }
}