/*
    Name of Developer:   Debosmeeta Paul
    Name of Company:     CTS
    
    Purpose of Class:    This class is used for Clinic login-Manning Agent details,list of objects and address of the client enrolled.
*/
public class PEMEClinicClientDetailsCtrl{
    
    private static final Integer SMARTQUERY_PAGE_SIZE   = 10;    
    public String selectedClient {get;set;}
    public list<PEME_Manning_Agent__c> lstManningAgents {get;set;}
    public list<Object__c> lstClientObjects {get;set;}
    public list<Asset> lstClientCovers {get;set;}
    public list<Asset> lstobj {get;set;}
    public set<String> lstObjects {get;set;}
    public list<String> selectedObject {get;set;}
    public String clientName {get;set;}
    //pagination variables
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    public String sSoqlQuery;
    private String nameOfMethod;
    Public Integer intStartrecord { get ; set ;}
    Public Integer intEndrecord { get ; set ;}
    public Integer pageNum{get;set;}//{pageNum = 1;}
    public String jointSortingParam { get;set; }
  //  private String sortDirection = Ascending;
  //  private String sortExp = 'Object__r.Name';
    public Integer noOfData{get;set;}
    public Account objAcc{get;set;}
    public String userName{get;set;}
    //pagination variables
    set<string> coverType;
    public String orderBy{get;set;}//{orderBy = '';}
    private String sortDirection = 'ASC';
    private String sortExp = '';  
    public string loggedInContactId;        
    public List<Contact> lstContact{get;set;}
    private static String Ascending='ASC';
    public String orderByNew
    {
     get
         {
            return sortExp;
         }
         set
         {
           //if the column is clicked on then switch between Ascending and Descending modes
           if (value == sortExp)
             sortDirection = (sortDirection.contains('DESC')? Ascending : 'DESC');
           else
             sortDirection = Ascending;
             sortExp = value;
         }
     }
     public String getSortDirection()
     {
        //if not column is selected 
        if (orderByNew == null || orderByNew == '')
          return Ascending;
        else
         return sortDirection;
     }
    
     public void setSortDirection(String value)
     {  
       sortDirection = value;
     }
    public ApexPages.StandardSetController submissionSetConOpen{
    get{ 
           return submissionSetConOpen;         
    }
    set; }
    public List<SelectOption> getObjectOptions() {
        List<SelectOption> Options = new List<SelectOption>();
        if(lstObjects!= null && lstObjects.size()>0){
            for(String strOp:lstObjects){
                if(strOp != null )
                Options.add(new SelectOption(strOp,strOp));
            }
            Options.sort();
            
        }
        return Options;
    }
    
    public String sortExpression{
         get{
            return sortExp;
         }
         set{          
           if (value == sortExp)
             sortDirection = (sortDirection == Ascending)? 'DESC' : Ascending;
           else
             sortDirection = Ascending;
             sortExp = value;
         }
     }
    
   /*  public String getSortDirection(){        
        if (sortExpression == null || sortExpression == '')
          return Ascending;
        else
         return sortDirection;
     }
    
     public void setSortDirection(String value){  
       sortDirection = value;
     }
     */
    public PEMEClinicClientDetailsCtrl(){ 
        pageNum = 1;
        orderBy = '';
        List<User> lstUser = new List<User>([select id,Contactid,name,Contact.Name,Email from User where id=:UserInfo.getUserId()]);
        if(lstUser!=null && lstUser.size()>0 && lstUser[0].Contactid!=null)
        {
            loggedInContactId = lstUser[0].Contactid;
            lstContact = new List<Contact>([select name, account.id,account.name,accountid, account.recordtypeid,account.company_Role__c, /*IsPeopleLineUser__c,*/account.BillingPostalcode__c,account.BillingCountry__c,account.BillingState__c,account.Billing_Street_Address_Line_1__c,account.Billing_Street_Address_Line_2__c,account.Billing_Street_Address_Line_3__c,account.Billing_Street_Address_Line_4__c 
                                          from Contact 
                                          where id=:loggedInContactId]);     
            userName = lstUser[0].Contact.Name;      
        }          
    }
    public pageReference loadData()
    {
       if(lstContact!=null && lstContact.size()>0 && lstContact[0].accountId!=null)
            {
                if(lstContact[0].account.company_Role__c.contains('External Service Provider')) //Added for MYG-2948
                {
                    lstManningAgents = new list<PEME_Manning_Agent__c>(); 
                    objAcc = new Account(); 
                    lstClientObjects = new list<Object__c>();
                    lstClientCovers = new list<Asset>();
                    lstObjects = new set<String>();
                    selectedObject = new list<String>();
                    orderByNew = 'Object__r.Name';
                    selectedClient = ApexPages.currentPage().getParameters().get('client');
                   // system.debug('====selectedClient ===='+selectedClient);
                    if(selectedClient!=null && selectedClient!=''){
                        objAcc = [select Id,Name,Billing_Street_Address_Line_1__c,Billing_Street_Address_Line_2__c,Billing_Street_Address_Line_3__c,Billing_Street_Address_Line_4__c,BillingCity__c,BillingPostalCode__c,BillingState__c,BillingCountry__c,Shipping_Street_Address_Line_1__c,Shipping_Street_Address_Line_2__c,Shipping_Street_Address_Line_3__c,Shipping_Street_Address_Line_4__c,ShippingCity__c,ShippingPostalCode__c,ShippingState__c,ShippingCountry__c,Phone,Fax,Website,Corporate_email__c,Mobile_Phone_1__c,GUID__c from Account where GUID__c=:selectedClient];
                        clientName = objAcc.Name;
                       // system.debug('====clientName ===='+clientName);     
                        for(PEME_Manning_Agent__c objManning:[SELECT Client__c,Client_Address__c,PEME_Enrollment_Form__r.ClientName__r.GUID__c,Client__r.Name,Client__r.BillingCity__c,Client__r.BillingPostalcode__c,Client__r.BillingCountry__c,Client__r.BillingState__c,Client__r.Billing_Street_Address_Line_1__c,Client__r.Billing_Street_Address_Line_2__c,Client__r.Billing_Street_Address_Line_3__c,Client__r.Billing_Street_Address_Line_4__c,Company_Name__c,Contact_Point__c,Contact_Point__r.name,Contact_Person_Email__c,Id,PEME_Enrollment_Form__c,
                        PEME_Enrollment_Form__r.ClientName__c,Contact_Person_Phone__c,Status__c,Client_Address_Line_2__c,Client_Address_Line_3__c,Client_Address_Line_4__c,Client_City__c,Client_State__c,Client_Zip_Code__c,Client_Country__c
                        FROM PEME_Manning_Agent__c where PEME_Enrollment_Form__r.ClientName__r.GUID__c=:selectedClient and Status__c = 'Approved' order by Client__r.Name asc]){
                            lstManningAgents.add(objManning); 
                        }
                                    
                    }
                    system.debug('lstManningAgents.size() '+lstManningAgents.size()); 
                    generateClientDetails(); 
                }
                else
                {
                    PageReference pg = new PageReference('/apex/HomePage');          
                    return pg;
                }
            } 
        return null;
    }
    
    public void generateClientDetails(){
                
        pageNum = 1;
        lstClientCovers= new List<Asset>();
        orderByNew = 'Object__r.Name';
        
    /*    if (jointSortingParam != null){
            system.debug('******jointSortingParam**************'+jointSortingParam);
            String[] arrStr = jointSortingParam .split('##');
            sortDirection = arrStr[0];
            sortExp = arrStr[1];            
        }
        */
        String strCondition ='';
        
        if(selectedObject!= null && selectedObject.size()>0){
            strCondition = ' AND Object__r.Name IN:selectedObject';
        }
        
        string sortFullExp = sortExpression  + ' ' + sortDirection;
        
        coverType = new set<String>{'Crew Cover','P&I Cover'};       
        sSoqlQuery = 'SELECT Object__r.Id,Object__r.Name,Name,Agreement__r.Client__r.GUID__c,Expiration_Date__c,Agreement__r.Policy_Year__c,Object__r.Imo_Lloyds_No__c,On_risk_indicator__c FROM Asset'+
                    ' where Agreement__r.Client__r.GUID__c =:selectedClient AND Name IN:coverType and (on_risk_indicator__c in(true,false) or inception_date__c> today) and (Expiration_Date__c = LAST_N_DAYS:365 or Expiration_Date__c > LAST_N_DAYS:365)'+strCondition;
        orderBy = ' ORDER BY ' + orderByNew + ' ' + sortDirection; //For new sorting 
        sSoqlQuery = sSoqlQuery+ ' ' + orderBy;  
       // sSoqlQuery = sSoqlQuery+strCondition+' ORDER BY '+sortFullExp+' ';
        submissionSetConOpen = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery + ' limit 10000')); 
        submissionSetConOpen.setPageSize(10);
        createOpenUserList();      
       // searchAssets();
               
        for(Asset objAsset:[SELECT Object__r.Id,Agreement__r.Client__r.GUID__c,Object__r.Name,Name,Expiration_Date__c,Agreement__r.Policy_Year__c,Object__r.Imo_Lloyds_No__c,On_risk_indicator__c FROM Asset
        where Agreement__r.Client__r.GUID__c =:selectedClient AND Name IN ('Crew Cover','P&I Cover') and (on_risk_indicator__c in(true,false) or inception_date__c> today) and (Expiration_Date__c = LAST_N_DAYS:365 or Expiration_Date__c > LAST_N_DAYS:365) ORDER BY Object__r.Name LIMIT 1000]){                
            lstObjects.add(objAsset.Object__r.Name);                
        }   
               
    }
    Public void createOpenUserList()
    {
        if(submissionSetConOpen!=null)
        {
            lstClientCovers.clear(); 
            for(Asset ast : (List<Asset>)submissionSetConOpen.getRecords())             
                lstClientCovers.add(ast);
             //   System.debug('********'+lstClientCovers.size());         
        } 
    }
    //Returns to the first page of records
    public void firstOpen() {
        submissionSetConOpen.first(); 
        pageNum = submissionSetConOpen.getPageNumber();
        
        createOpenUserList();
        
    }
    public Boolean hasPreviousOpen   
    {   
        get   
        {   
            return submissionSetConOpen.getHasPrevious();   
        }   
        set;   
    } 
     //Returns the previous page of records   
    public void previousOpen()   
    {   
        submissionSetConOpen.previous();
        pageNum = submissionSetConOpen.getPageNumber();
        createOpenUserList();  
    } 
    //to go to a specific page number
    public void setpageNumberOpen()
    {
        submissionSetConOpen.setpageNumber(pageNum); 
        createOpenUserList();
    }
    public Boolean hasNextOpen   
    {   
        get   
        {   
            return submissionSetConOpen.getHasNext();   
        }   
        set;   
    } 
    //Returns the next page of records   
    public void nextOpen()   
    {   
        submissionSetConOpen.next(); 
        pageNum = submissionSetConOpen.getPageNumber();
        createOpenUserList();
        
    }
     //Returns to the last page of records
    public void lastOpen(){
        submissionSetConOpen.last(); 
        pageNum = submissionSetConOpen.getPageNumber();
        
        createOpenUserList();
        
    }
   /* public void searchAssets(){
        if(sSoqlQuery.length()>0){            
            System.debug('Soql Query--->'+sSoqlQuery);  
            setCon = null;            
            
            lstClientCovers=(List<Asset>)setCon.getRecords();
            system.debug('****setCon.getResultSize()**********'+setCon.getResultSize());
            if(setCon.getPageNumber() == 1)
                intStartrecord = 1;
            else
                intStartrecord  = ((setCon.getPageNumber() - 1) * SMARTQUERY_PAGE_SIZE) + 1;
          
            intEndrecord = setCon.getPageNumber() * SMARTQUERY_PAGE_SIZE;
            if( intEndrecord > noOfRecords ){
                intEndrecord = noOfRecords ;
            }
        }
                                                              
    }
        
    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                size = SMARTQUERY_PAGE_SIZE;
                System.debug('Soql Query--->'+sSoqlQuery);
                if(sSoqlQuery!=null && sSoqlQuery!=''){             
                    setCon = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery+' LIMIT 10000'));
                    setCon.setPageSize(size);
                    noOfRecords = setCon.getResultSize();
                    system.debug('****Total records****'+noOfRecords);
                    if (noOfRecords == null || noOfRecords == 0){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No search results found.'));
                    }else if (noOfRecords == 10000){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The search returned 10000 records (maximum allowable limit).'));
                    }
                }                
            }
            return setCon;
        }set;
    }  
    
    public void setpageNumber(){
        if(setCon!=null){
            setCon.setpageNumber(pageNum);
            searchAssets();
        }
    }
        
    public Boolean hasNext {
        get {
            if(setCon!=null)
            return setCon.getHasNext();
            else return false;
        }
        set;
    }
    
    public Boolean hasPrevious {
        get {
            if(setCon!=null)
            return setCon.getHasPrevious();
            else return false;
        }
        set;
    }
    
    public Integer pageNumber {
        get {
            if(setCon!=null)
            return setCon.getPageNumber();
            else return 0;
        }
        set;
    }
       
    public void navigate(){
        if(nameOfMethod=='previous'){
             setCon.previous();
             pageNum=setCon.getpageNumber();
             searchAssets();
        }else if(nameOfMethod=='last'){
             setCon.last();
             searchAssets();
        }else if(nameOfMethod=='next'){
             setCon.next();
             pageNum=setCon.getpageNumber();
             searchAssets();
        }else if(nameOfMethod=='first'){
             setCon.first();
             searchAssets();
        }
    }
        
    public void first() {
        nameOfMethod='first';
        navigate();
    }
    
    public void next(){
        nameOfMethod='next';
        pageNum = setCon.getPageNumber();
        navigate();        
    }
   
    public void last(){
        nameOfMethod='last';
        navigate();
    }
  
    public void previous(){
        nameOfMethod='previous';
         pageNum = setCon.getPageNumber();
        navigate();
    } */
    
    public pageReference clearOptions(){
        selectedObject.clear();
        generateClientDetails();
        return null;
    }
    public PageReference print(){
        lstobj = new List<Asset>();
        Integer count = 1;
        noOfData = 0;
        for(Asset mcl : Database.Query(sSoqlQuery+' LIMIT 10000')){               
            if(count<1000){
                lstobj.add(mcl);                    
                count++;
            }else{                   
                count = 1;
            }   
            noOfData++;                 
        }
        return page.PrintForPemeObjects;
    }
        
     public PageReference exportToExcel(){
        lstobj = new List<Asset>();
        Integer count = 1;
        noOfData = 0;
        //System.debug('checkinggggggggggggggggg'+sSoqlQuery);
        for(Asset mcl : Database.Query(sSoqlQuery+' LIMIT 10000')){               
            if(count<1000){
                lstobj.add(mcl);                    
                count++;
            }else{                   
                count = 1;
            }   
            noOfData++;                 
        }
        return page.GenerateExcelForPemeObject;
    }
}