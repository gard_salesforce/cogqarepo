public class PEMEEnrollmentAssignToME{
    public static String currentLoggedInUserId = UserInfo.getUserId();
    //private static User adminUser = [SELECT id FROM User WHERE profileId = '00e20000001O7Hl' AND isActive = true LIMIT 1];
    @AuraEnabled
    public static Map<String,String> assignToMe(Id pef_Id){
        Map<String,String> msg = new Map<String,String>(); 
        System.debug('pef_Id - '+pef_Id);
        setLoggedInUserAsOwner(pef_Id);
        //updateProcessInstanceWorkitem(pef_Id);//Commented by Geetham as the same code is written in the trigger
        msg.put('status','0');
        return msg;
    }
    
    @AuraEnabled
    public static List<PEME_Enrollment_Form__c> setLoggedInUserAsOwner(Id pef_Id){
        System.debug('PEMEEnrollmentAssignToME.setLoggedInUserAsOwner() STARTS');
        List<PEME_Enrollment_Form__c> Pef;
        try{
            System.debug('PEMEEnrollmentAssignToME.setLoggedInUserAsOwner() point 1 pef_Id/pef - '+pef_Id+' / '+pef);
            pef = [Select OwnerId,id From PEME_Enrollment_Form__c Where Id =:pef_Id]; 
            System.debug('PEMEEnrollmentAssignToME.setLoggedInUserAsOwner() point 2 pef - '+pef);
            System.debug('PEMEEnrollmentAssignToME.setLoggedInUserAsOwner() at point 3 currentLoggedInUserId - '+currentLoggedInUserId);
            if(pef.size() > 0){
                pef[0].OwnerId = currentLoggedInUserId;
                update Pef;
            }
            System.debug('No Exception caught @ PEMEEnrollmentAssignToME.setLoggedInUserAsOwner() - '+pef);
        }catch(Exception ex){
            System.debug('Exception caught @ PEMEEnrollmentAssignToME.setLoggedInUserAsOwner() - '+ex.getMessage());
            System.debug('Exception caught @ PEMEEnrollmentAssignToME.setLoggedInUserAsOwner() - '+ex);
        }
        System.debug('PEMEEnrollmentAssignToME.setLoggedInUserAsOwner() ENDS');
        return pef;
    }
    /*Commented by Geetham as the same code is written in the trigger
    @AuraEnabled
    public static List<ProcessInstanceWorkitem> updateProcessInstanceWorkitem(Id pef_Id){
        System.debug('PEMEEnrollmentAssignToME.updateProcessInstanceWorkitem() STARTS');
        List<ProcessInstanceWorkitem> processInstance;
        try{
            System.debug('PEMEEnrollmentAssignToME.updateProcessInstanceWorkitem() at point 1 pef_Id - '+pef_Id);
            processInstance = [SELECT Id,ActorId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =:pef_Id AND ProcessInstance.Status = 'Pending' LIMIT 1];
            System.debug('PEMEEnrollmentAssignToME.updateProcessInstanceWorkitem() at point 2 processInstance - '+processInstance);
            System.debug('PEMEEnrollmentAssignToME.updateProcessInstanceWorkitem() at point 3 currentLoggedInUserId - '+currentLoggedInUserId);
            if(processInstance.size() > 0){
                processInstance[0].ActorId = currentLoggedInUserId;
                update processInstance;
                System.debug('No Exception caught @ PEMEEnrollmentAssignToME.updateProcessInstanceWorkitem() - '+processInstance);
            }
        }catch(Exception ex){
            System.debug('Exception caught @ PEMEEnrollmentAssignToME.updateProcessInstanceWorkitem() - '+ex.getMessage());
        }
        System.debug('PEMEEnrollmentAssignToME.updateProcessInstanceWorkitem() ENDS');
        return processInstance;
    }*/
}