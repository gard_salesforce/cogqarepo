Public class DocumentRequestWrapper  {
    //public List<String> documentClass {get;set;}
    Public List<String> propertyResult {get;set;}
    
    Public List<Object> filter {get;set;}//new
    //Public String SearchText {get;set;}//new
    //public List<String> ObjectClass {get;set;} //new  
    //Public List<String> propertyResult1 {get;set;}//new

    
    public DocumentRequestWrapper(String pd_clientOrObj, String Pd_broker, String userType, Boolean pd_publishToExtranet, Integer pd_years, List<String> documentTypes, String searchText, Boolean isObjectMode, String PageType){
        //List<String> documentClasses = new List<String>();
        filter = new List<Object>();
        
        MyGardDocumentType__c myGardDocumentObject = new MyGardDocumentType__c(); // N/A
        //Create a map of each class and its respective doc types - Start
        Map<String,String> docTypeClassMap = new Map<String,String>();
        Map<String,List<String>> docClassTypeMap = new Map<String,List<String>>();
        List<String> docClassList = new List<String>();
        List<String> docClassForObjList;
        List<String> docTypeList;
        Boolean isDocSelected = true;
        Map<String,String> docClassTypefieldMap = new Map<String,String>();
        Map<String,Map<String,Object>> pd_brokerMap = new Map<String,Map<String,Object>>();  //Added for sending broker id as  parameter
        //----added---
        List<MyGardDocumentType__c> allDocTypes;
         Boolean docTypeAvailable = true;
       // List<String>policy_Year=new List<String>{'2020','2019','2018','2017'};
        List<String>myGardYrList=new List<String>();
        for( MyGardDocumentYear__c myGardYear : MyGardDocumentYear__c.getAll().values()){
                       myGardYrList.add(myGardYear.Year__c);
              }
          
        if(documentTypes == null || documentTypes.size() == 0){
             docTypeAvailable=false;
            if(MyGardHelperCtrl.ACCOUNT_ID_SET != null && MyGardHelperCtrl.ACCOUNT_ID_SET.size () == 0){        
              MyGardHelperCtrl.CreateCommonData();
            }
            Account_Contact_Mapping__c acmJuncObj=MyGardHelperCtrl.ACM_JUNC_OBJ;
            isDocSelected = false;
            allDocTypes = MyGardDocumentType__c.getall().values();
            for(MyGardDocumentType__c mgDoc : allDocTypes){
                if((mgDoc.P_I_Document__c && acmJuncObj.PI_access__c) ||(mgDoc.Marine_Document__c && acmJuncObj.Marine_access__c)){
                if(PageType.equalsIgnoreCase('PiRenewalPolicies') && mgDoc.Document_Type_Certificate__c == true)
                    documentTypes.add(mgDoc.Document_Type__c);
                if(PageType.equalsIgnoreCase('PiRenewalBlueCard') && mgDoc.Document_Type_Blue_Card__c == true)
                    documentTypes.add(mgDoc.Document_Type__c);
                if(!(PageType.equalsIgnoreCase('PiRenewalPolicies')) && !(PageType.equalsIgnoreCase('PiRenewalBlueCard')))
                    documentTypes.add(mgDoc.Document_Type__c);
                system.debug('PageType--'+PageType+'--mgDoc-Document_Type_Certificate__c--'+mgDoc.Document_Type_Certificate__c+'--mgDoc-Document_Type_Blue_Card__c--'+mgDoc.Document_Type_Blue_Card__c);
              }
            }
        }
        system.debug('PageType--'+PageType+'--documentTypes--'+documentTypes);
        system.debug('queryresult*******'+[SELECT Document_Class__c, Document_Type__c, Document_Type_Field__c FROM MyGardDocumentType__c where Document_Type__c IN: documentTypes]);
        for(MyGardDocumentType__c allDocType : [SELECT Document_Class__c, Document_Type__c, Document_Type_Field__c FROM MyGardDocumentType__c where Document_Type__c IN: documentTypes]){ //(BBC, 20, MLC, WRC)
            system.debug('---allDocType in loop--'+allDocType);
            docTypeClassMap.put(allDocType.Document_Type__c, allDocType.Document_Class__c);
            if(!docClassList.contains(allDocType.Document_Class__c)){
                docClassList.add(allDocType.Document_Class__c);
                docClassTypefieldMap.put(allDocType.Document_Class__c,allDocType.Document_Type_Field__c);
            }
        }
        for(String docClassListObjs : docClassList){
            docTypeList = new List<String>();
            for(String docTypeClassMapKeys : docTypeClassMap.KeySet()){
                if(docTypeClassMap.get(docTypeClassMapKeys) == docClassListObjs){
                    docTypeList.add(docTypeClassMapKeys);
                }
            }
            docClassTypeMap.put(docClassListObjs,docTypeList);
        }
        system.debug('docClassTypeMap---'+docClassTypeMap);
        //Create a map of each class and its respective doc types - End
        //-------------------------------------------------------------------------------------------------
        /*
        if(!documentClasses.contains(string.valueOf(myGardDocumentObject.Document_Class__c))){
            documentClasses.add(string.valueOf(myGardDocumentObject.Document_Class__c));
        } 
        //this.documentClass = documentClasses;
        this.documentClass = documentClasses;
        */
        //-------------------------------------------------------------------------------------------------
        //filter Parameter
        //Map<String,String> tempSearchMap;
        //Map<String,String> tempObjectClassMap;
        List<Map<String,Object>> propertyFilterGIC;
        List<Map<String,Object>> propertyFilterPARIS;
        Map<String,Object> tempPubExtMap;
        Map<String,Object> tempYearMap;
        Map<String,Object> tempSelClientMap;
        Map<String,Object> filterObjMap;
        Map<String,Object> tempClasTypeMap;
        Map<String,Object> tempBrokerMap;
        
        //Variables created by Pulkit STARTS
        Map<String,Object> pd_agreement;
        Map<String,String> pd_brokerObj;
        //Variables created by Pulkit ENDS
        /*Filter Parameters for paris Doc*/
        Map<String,Map<String,Object>> pd_agreementBrokerMap = new Map<String,Map<String,Object>>();
        Map<String,Map<String,Object>> pd_agreementClientMap = new Map<String,Map<String,Object>>();
        Map<String,Map<String,Object>> pd_agreementYearMap = new Map<String,Map<String,Object>>();
        Map<String,Object> parisFilterObjMap;
        Map<String,Map<String,Object>> pd_riskObjectMap ;
        system.debug('isObjectMode---'+isObjectMode);
        if(isObjectMode == false){
            for(String docClassObj : docClassTypeMap.KeySet()){
                system.debug('docClassTypeMap---'+docClassObj);
                filterObjMap =  new Map<String,Object>();
               
                if(!string.isBlank(SearchText))
                    filterObjMap.put('SearchText', searchText);
                
               
                docClassList = new List<String>{docClassObj};
                filterObjMap.put('ObjectClass',docClassList);
                
                tempPubExtMap = new Map<String,Object>();
                tempPubExtMap.put('pd_publishtoextranet',pd_publishToExtranet);
                
                tempYearMap = new Map<String,Object>();
               
                
                 tempYearMap.put('pd_years',string.ValueOf(pd_years));
               //tempYearMap.put('pd_years',policy_Year);
                
                tempSelClientMap = new Map<String,Object>();

                tempSelClientMap.put('pd_client',pd_clientOrObj.trim());
                
                system.debug('tempClasTypeMap---'+tempClasTypeMap);
                tempClasTypeMap = new Map<String,Object>();
                
                system.debug('tempClasTypeMap---'+tempClasTypeMap);
                tempBrokerMap = new Map<String,Object>();
                pd_brokerMap=new Map<String,Map<String,Object>>();
                propertyFilterGIC = new List<Map<String,Object>>();
                if(userType == 'Broker'){
                    if(!docClassObj.equalsIgnoreCase(Label.tradingCertificate)){
                        tempBrokerMap.put('pd_broker',Pd_broker);
                        pd_brokerMap.put('pd_risk',tempBrokerMap);
                        propertyFilterGIC.add(pd_brokerMap);
                      
                    }
                   
                   // tempBrokerMap.put('pd_broker',Pd_broker);
                   
                    
                }
                 if(docClassObj.equalsIgnoreCase(Label.tradingCertificate)){
                  
                  propertyFilterGIC.add(tempSelClientMap);
                  }
                  else{
                  propertyFilterGIC.add(new Map<String,Object>{'pd_risk' => tempSelClientMap});
                  }
                   propertyFilterGIC.add(tempYearMap);
                propertyFilterGIC.add(tempPubExtMap);
               
               // propertyFilterGIC.add(tempSelClientMap);
                
                system.debug('isDocSelected---'+isDocSelected);
                //if(isDocSelected)
               //  if(docTypeAvailable){
                tempClasTypeMap.put(docClassTypefieldMap.get(docClassObj),docClassTypeMap.get(docClassObj));
                propertyFilterGIC.add(tempClasTypeMap);
               // }
                filterObjMap.put('PropertyFilter',propertyFilterGIC);
                
                filter.add(filterObjMap);

                /* code start for PARIS Added by Anju */
                if(docClassObj.equalsIgnoreCase('cl_policydocument')){
                    parisFilterObjMap=new Map<String,object>();
                    parisFilterObjMap.put('ObjectClass',new List<String>{docClassObj})  ;
                    if(!string.isBlank(SearchText))
                    parisFilterObjMap.put('SearchText', searchText);
                    propertyFilterPARIS = new List<Map<String,Object>>();
                    if(userType == 'Broker'){
                        //  pd_agreementBrokerMap = new Map<String,Map<String,Object>>();
                            tempBrokerMap.put('pd_broker',Pd_broker);
                           // pd_agreementBrokerMap.put('pd_agreement',tempBrokerMap);
                            propertyFilterPARIS.add(tempBrokerMap);
                    }
                    pd_agreementClientMap = new Map<String,Map<String,Object>>();
                    pd_agreementYearMap = new Map<String,Map<String,Object>>();
                   //pd_agreementYearMap.put('pd_agreement', new Map<String,Object>{'pd_years' => String.valueOf(pd_years)});
                  // pd_agreementYearMap.put('pd_agreement', new Map<String,Object>{'pd_underwritingyear' => policy_Year});
                    
                    
                   // pd_agreementClientMap.put('pd_agreement',new Map<String,Object>{'pd_client'=>pd_clientOrObj});
                    propertyFilterPARIS.add(new Map<String,Object>{'pd_client'=>pd_clientOrObj});
                    propertyFilterPARIS.add(new Map<String,Object>{'pd_years' => String.valueOf(pd_years)});
                    propertyFilterPARIS.add(new Map<String,object>{'pd_publishtoextranet'=>pd_publishtoextranet});
                   // if(docTypeAvailable){
                    propertyFilterPARIS.add(new Map<String,object>{docClassTypefieldMap.get(docClassObj)=>docClassTypeMap.get(docClassObj)});
                  //  }
                    parisFilterObjMap.put('PropertyFilter',propertyFilterPARIS);
                    filter.add(parisFilterObjMap);
                }

            }
        }
        else if(isObjectMode == true){
            system.debug('pd_clientOrObj***'+pd_clientOrObj);
            List<String>clientOrObject=pd_clientOrObj.split('#');
            system.debug('clientOrObject***'+clientOrObject);
            String pd_client='';
            String pd_object='';
            if(clientOrObject.size()>0){
             pd_object=clientOrObject[0];
             pd_client=clientOrObject[1];
            }
            for(MyGardDocumentType__c allDocType : [SELECT Document_Class__c, Document_Type__c, Document_Type_Field__c FROM MyGardDocumentType__c]){
                if(!docClassList.contains(allDocType.Document_Class__c))
                    docClassList.add(allDocType.Document_Class__c);
            }
             if(pd_object.startsWithIgnoreCase('p')){
                pd_object=pd_object.mid(1,pd_object.length()-1);
                
               }
                 
                   filterObjMap =  new Map<String,Object>(); 
                    
            for(String docClass : docClassList){
                //system.debug('--docClass--'+docClass);
                filterObjMap =  new Map<String,Object>();
                
                docClassForObjList = new List<String>{docClass};
                filterObjMap.put('ObjectClass',docClassForObjList);
                // filterObjMap.put('ObjectClass',docClassList);
                if(!string.isBlank(SearchText)){
                filterObjMap.put('SearchText', searchText);
                }
                //if(docClass.equalsIgnoreCase('cl_certificate')){
                tempPubExtMap = new Map<String,Object>();
                tempPubExtMap.put('pd_publishtoextranet',pd_publishToExtranet);
                
                tempSelClientMap = new Map<String,Object>();
               
                 tempSelClientMap.put('pd_object',pd_object);
                
                propertyFilterGIC = new List<Map<String,Object>>();
                propertyFilterGIC.add(tempSelClientMap);
                propertyFilterGIC.add(tempPubExtMap);
                propertyFilterGIC.add(new map<String,Object>{'pd_years'=>myGardYrList});
                 propertyFilterGIC.add(new map<String,Object>{'pd_client'=>pd_client});
                if(userType.equalsIgnoreCase(Label.Broker) && !docClass.equalsIgnoreCase(Label.tradingCertificate)){
                   
                propertyFilterGIC.add(new map<String,Object>{'pd_broker'=>Pd_broker});
                }
                //pd_riskObjectMap=new Map<String,Map<String,Object>>();
                 
                filterObjMap.put('PropertyFilter',propertyFilterGIC);
               
                filter.add(filterObjMap);
               // }
                if(docClass.equalsIgnoreCase('cl_policydocument')){
                    parisFilterObjMap=new Map<String,Object>();
                    propertyFilterPARIS = new List<Map<String,Object>>(); 
                    parisFilterObjMap.put('ObjectClass',docClassList);
                    pd_riskObjectMap=new Map<String,Map<String,Object>>();
                    pd_riskObjectMap.put('pd_risk', new Map<String,Object>{'pd_object' =>pd_object});
                   
                     propertyFilterPARIS.add(new map<String,Object>{'pd_years'=>myGardYrList});
                    propertyFilterPARIS.add(pd_riskObjectMap);
                    propertyFilterPARIS.add(new map<String,Object>{'pd_publishtoextranet' =>pd_publishToExtranet});
                     propertyFilterPARIS.add(New Map<String,Map<String,Object>>{'pd_risk' => new Map<String,Object>{'pd_client' =>Pd_client}});
                
                   if(userType.equalsIgnoreCase(Label.broker)){
                   propertyFilterPARIS.add(New Map<String,Map<String,Object>>{'pd_risk' => new Map<String,Object>{'pd_broker' =>Pd_broker}});
                 
                   }
                    parisFilterObjMap.put('PropertyFilter',propertyFilterPARIS);
                    if(!string.isBlank(SearchText)){
                    parisFilterObjMap.put('SearchText', searchText);
                    }
                     filter.add(parisFilterObjMap);
                }
                system.debug('--Object filter--'+filter);
           }
        }
        //
        
        /*
        
        Map<String, list<Object>> tempStringObjectsMap = new Map<String, list<Object>>();
        List<String> documentType ;  
        
        for(string documentClass : documentClasses){
            String documentTypeField='';
            System.debug('mapping : ' + documentClass);
            documentType = new List<String>(); 
            tempStringObjectsMap = new Map<String, list<Object>>();
            
            System.debug('documentTypes : ' + documentTypes); //(BBC, 20, MLC, WRC)
           
            for(string allDocType : documentTypes){ 
                MyGardDocumentType__c allDocumentType = [SELECT Document_Class__c, Document_Type__c, Document_Type_Field__c FROM MyGardDocumentType__c where Document_Type__c=:allDocType];                 
                System.debug( 'Document Type :  ' + allDocType + '  Document Class :  ' + string.valueOf(allDocumentType.Document_Class__c));                        
                               
                if(string.valueOf(documentClass) == string.valueOf(allDocumentType.Document_Class__c)){
                    documentType.add(string.valueOf(allDocType));  
                    System.debug( 'documentType  :  ' + documentType);
                    if(documentTypeField == '')
                        documentTypeField = string.valueOf(allDocumentType.Document_Type_Field__c);
                }
                System.debug( 'documentType  :  ' + documentType); 
            }            
             
            System.debug( 'tempStringObjectsMap  :  ' + tempStringObjectsMap);
              
            if(documentType.size() > 0)
            {
                tempStringObjectsMap.put(documentTypeField, documentType);
                System.debug( 'tempStringObjectsMap  :  ' + tempStringObjectsMap);  
                System.debug( 'propertyFilter:  ' + propertyFilter); 
                
                propertyFilter.add(tempStringObjectsMap);
            }           
        }
        */
        System.debug( 'propertyFilter:  ' + filter); 
        
        propertyResult = new List<String>(); 
        if(!filter.isEmpty()){       
        propertyResult.add('pd_client');
        propertyResult.add('pd_years');
        propertyResult.add('pd_productarea');
        propertyResult.add('pd_object');
        propertyResult.add('pd_certificatetype');
        propertyResult.add('pd_policydocumenttype');
        propertyResult.add('pd_covercovertypes');
        propertyResult.add('pd_documentid');
        propertyResult.add('100');        
        propertyResult.add('0');
        propertyResult.add('pd_author');
        propertyResult.add('pd_creator');
        propertyResult.add('21');
        propertyResult.add('pd_underwritingyear');
        propertyResult.add('pd_validfrom');
        propertyResult.add('pd_validto');
        propertyResult.add('pd_objectnameondocument');
        
        System.debug('\nsfEdit propertyFilterGIC - '+propertyFilterGIC);
        System.debug('\nsfEdit Json.serialize(propertyFilterGIC) - '+Json.serialize(propertyFilterGIC));
        System.debug('\nsfEdit propertyResult - '+propertyResult);
        }
    }
    
}



/*
Public class DocumentRequestWrapper  {
    public List<String> documentClass {get;set;}
    public List<Map<String,Object>> propertyFilter;
    Public List<String> propertyResult {get;set;}
    
    public DocumentRequestWrapper(String pd_client, Boolean pd_publishToExtranet, Integer pd_years, List<String> documentTypes){
        List<String> documentClasses = new List<String>();
        MyGardDocumentType__c myGardDocumentObject = new MyGardDocumentType__c();
        
        for(string allDocType : documentTypes){ //(BBC, 20, MLC, WRC)
            myGardDocumentObject = [SELECT Document_Class__c, Document_Type__c, Document_Type_Field__c FROM MyGardDocumentType__c where Document_Type__c=:allDocType];
            System.debug('myGardDocumentObject : '+ myGardDocumentObject);
            
            if(!documentClasses.contains(string.valueOf(myGardDocumentObject.Document_Class__c))){
                documentClasses.add(string.valueOf(myGardDocumentObject.Document_Class__c));
            } 
            //this.documentClass = documentClasses;
        }
        this.documentClass = documentClasses;
        
        propertyFilter = new List<Map<String,Object>>();
        Map<String,Object> tempStringObjectMap1 = new Map<String,Object>();
        tempStringObjectMap1.put('pd_publishtoextranet',pd_publishToExtranet);
        Map<String,Object> tempStringObjectMap2 = new Map<String,Object>();
        tempStringObjectMap2.put('pd_years',pd_years);
        Map<String,Object> tempStringObjectMap3 = new Map<String,Object>();
        tempStringObjectMap3.put('pd_client',pd_client);
        
        propertyFilter.add(tempStringObjectMap1);
        propertyFilter.add(tempStringObjectMap2);
        propertyFilter.add(tempStringObjectMap3);
        
        Map<String, list<Object>> tempStringObjectsMap = new Map<String, list<Object>>();
        List<String> documentType ;  
        
        for(string documentClass : documentClasses){
            String documentTypeField='';
            System.debug('mapping : ' + documentClass);
            documentType = new List<String>(); 
            tempStringObjectsMap = new Map<String, list<Object>>();
            
            System.debug('documentTypes : ' + documentTypes); //(BBC, 20, MLC, WRC)
           
            for(string allDocType : documentTypes){ 
                MyGardDocumentType__c allDocumentType = [SELECT Document_Class__c, Document_Type__c, Document_Type_Field__c FROM MyGardDocumentType__c where Document_Type__c=:allDocType];                 
                System.debug( 'Document Type :  ' + allDocType + '  Document Class :  ' + string.valueOf(allDocumentType.Document_Class__c));                        
                               
                if(string.valueOf(documentClass) == string.valueOf(allDocumentType.Document_Class__c)){
                    documentType.add(string.valueOf(allDocType));  
                    System.debug( 'documentType  :  ' + documentType);
                    if(documentTypeField == '')
                        documentTypeField = string.valueOf(allDocumentType.Document_Type_Field__c);
                }
                System.debug( 'documentType  :  ' + documentType); 
            }            
             
            System.debug( 'tempStringObjectsMap  :  ' + tempStringObjectsMap);
              
            if(documentType.size() > 0)
            {
                tempStringObjectsMap.put(documentTypeField, documentType);
                System.debug( 'tempStringObjectsMap  :  ' + tempStringObjectsMap);  
                System.debug( 'propertyFilter:  ' + propertyFilter); 
                
                propertyFilter.add(tempStringObjectsMap);
            }           
        }
        
        System.debug( 'propertyFilter:  ' + propertyFilter); 
        
        propertyResult = new List<String>();        
        propertyResult.add('pd_client');
        propertyResult.add('pd_years');
        propertyResult.add('pd_productarea');
        propertyResult.add('pd_object');
        propertyResult.add('pd_certificatetype');
        propertyResult.add('pd_policydocumenttype');
        propertyResult.add('pd_covercovertypes');
        propertyResult.add('pd_documentid');
        propertyResult.add('100');        
        propertyResult.add('0');
        propertyResult.add('pd_author');
        propertyResult.add('pd_creator');
        propertyResult.add('21');
        propertyResult.add('pd_underwritingyear');
        propertyResult.add('pd_validfrom');
        propertyResult.add('pd_validto');
        propertyResult.add('pd_objectnameondocument');
        
        
    }
    
} 
*/