//Generated by wsdl2apex

public class messagesGardNoV10Subclaim {
    public class SubClaimBase {
        public String SubClaimTitle;
        public messagesGardNoV10Claimtype.ClaimTypeBase SubClaimType;
        public String MemberSubClaimReference;
        public DateTime SubClaimDate;
        public String SubClaimText;
        public String SubClaimBriefDescription;
        public String SubClaimDetailText;
        public messagesGardNoV10Subclaimdetails.SubClaimDetailsBase SubClaimDetails;
        private String[] SubClaimTitle_type_info = new String[]{'SubClaimTitle','http://messages.gard.no/v1_0/SubClaim',null,'0','1','false'};
        private String[] SubClaimType_type_info = new String[]{'SubClaimType','http://messages.gard.no/v1_0/SubClaim',null,'1','1','false'};
        private String[] MemberSubClaimReference_type_info = new String[]{'MemberSubClaimReference','http://messages.gard.no/v1_0/SubClaim',null,'0','1','false'};
        private String[] SubClaimDate_type_info = new String[]{'SubClaimDate','http://messages.gard.no/v1_0/SubClaim',null,'1','1','false'};
        private String[] SubClaimText_type_info = new String[]{'SubClaimText','http://messages.gard.no/v1_0/SubClaim',null,'0','1','false'};
        private String[] SubClaimBriefDescription_type_info = new String[]{'SubClaimBriefDescription','http://messages.gard.no/v1_0/SubClaim',null,'0','1','false'};
        private String[] SubClaimDetailText_type_info = new String[]{'SubClaimDetailText','http://messages.gard.no/v1_0/SubClaim',null,'0','1','false'};
        private String[] SubClaimDetails_type_info = new String[]{'SubClaimDetails','http://messages.gard.no/v1_0/SubClaim',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://messages.gard.no/v1_0/SubClaim','true','false'};
        private String[] field_order_type_info = new String[]{'SubClaimTitle','SubClaimType','MemberSubClaimReference','SubClaimDate','SubClaimText','SubClaimBriefDescription','SubClaimDetailText','SubClaimDetails'};
    }
}