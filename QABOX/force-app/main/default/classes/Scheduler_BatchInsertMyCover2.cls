global class Scheduler_BatchInsertMyCover2 implements Schedulable
{
    public static String sched = '0 00 01 * * ?';  //4 times a day 

    global static String scheduleMe() 
    {
        Scheduler_class SC = new Scheduler_class(); 
        return System.schedule('My batch Job', sched, SC);
    }

    global void execute(SchedulableContext sc) 
    {    
        if(!Test.isRunningTest())  
        {
            BatchInsertMyCover2  prodBatch_false = new BatchInsertMyCover2('false', null);     
            database.executebatch(prodBatch_false, 1);
            BatchInsertMyCover2  prodBatch_true = new BatchInsertMyCover2('true', null);
            database.executebatch(prodBatch_true, 20);
        }
    }
}