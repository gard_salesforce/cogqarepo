public class CustomerFeedbackEventCtrl {
    public List<Customer_Feedback__c> customerfeedbacklst {get;set;}
    string userid = UserInfo.getUserId();
    private final Event evt;
    public String domain {get;set;}
    public Id CustNo {get;set;}
    public String eventName {get;set;}     
    public CustomerFeedbackEventCtrl(ApexPages.StandardController controller){
        this.evt = (Event)controller.getRecord();
        CustNo = evt.Id;
        eventName = [select subject from event where id=:CustNo].Subject;
        domain = ApexPages.currentPage().getHeaders().get('Host');       
        customerfeedbacklst = [SELECT id,Name,status__c,Source__c,Responsible__c,Type_of_feedback__c,Response__c,CreatedDate,Title__c FROM Customer_Feedback__c WHERE MeetingId__c=:CustNo];
    }
}