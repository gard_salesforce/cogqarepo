global class Scheduler_BatchMyCoverHardDelete implements Schedulable{
    
    public static String sched = '0 00 01 * * ?';

    global static String scheduleMe() {
        Scheduler_BatchMyCoverHardDelete SC = new Scheduler_BatchMyCoverHardDelete(); 
        return System.schedule('My batch Job', sched, SC);
    }
    global void execute(SchedulableContext sc) {
        if(!Test.isRunningTest())  
        {
            BatchMyCoverHardDelete bhd= new BatchMyCoverHardDelete();
            ID batchprocessid = Database.executeBatch(bhd,50);   
        }        
      }
}