public class CompanyOnRiskInfoResponseWrapper {  
    public integer crmId;
    public String partnerName;
    public integer partnerId;
    public integer partnerTypeCode; 
    public String partnerTypeDescription;
    public boolean stopUse;
    public boolean companyOnRisk;
    public boolean clientOnRisk;
    public boolean brokerOnRisk;
    public boolean insurancecompanyOnRisk;
    public boolean assuredOnRisk;
    public boolean protectedCoassuredOnRisk;
    public boolean coassuredOnRisk;
    public boolean pandiOwnersOnRisk;
    public boolean pandiCharterersOnRisk;
    public boolean pandiMOUOnRisk;
    public boolean marineOnRisk;
    public boolean energyOnRisk;
    public boolean buildersOnRisk;
    public boolean originalInsuredMutualOnRisk;
    public boolean originalInsuredSmallCraftOnRisk;
}