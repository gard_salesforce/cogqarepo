@isTest
public class TestBatchInsertMyCoverList {
    @isTest static void testBatchConstructor1(){
        //Create Data
        GardTestData gtd = new GardTestData();
        gtd.commonRecord();
            Database.executeBatch(new BatchInsertMyCoverList(true));
        	Database.executeBatch(new BatchInsertMyCoverList(false));
    }
    
    @isTest static void testBatchConstructor2(){
        //Create Data
        GardTestData gtd = new GardTestData();
        gtd.commonRecord();
        List<Id> accountIds = new List<Id>();
        for(Account account : [SELECT id fROM Account]){
            accountIds.add(account.id);
        }
        Database.executeBatch(new BatchInsertMyCoverList(true,accountIds));
        Database.executeBatch(new BatchInsertMyCoverList(false,accountIds));
    }
}