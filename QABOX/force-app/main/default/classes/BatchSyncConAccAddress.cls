global class BatchSyncConAccAddress implements Database.Batchable<sObject>{
    public String query;
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        // Call the method in the processContacts class which syncs addresses
        Set<Id> conIds = new Set<Id>();
        for (sObject s: scope) {
            conIds.add(s.Id);
        }
        List<Contact> lstConToUpdate = new List<Contact>();
        for (contact con : [SELECT Id, MailingStreet, MailingCity, MailingPostalCode, MailingState, MailingCountry,
                            AccountId, Account.BillingStreet, Account.BillingCity, Account.BillingPostalCode, Account.BillingState, Account.BillingCountry
                            FROM Contact WHERE Id IN :conIds]) {
            if(con.MailingStreet!=con.Account.BillingStreet || con.MailingCity!=con.Account.BillingCity || con.MailingPostalCode!=con.Account.BillingPostalCode || con.MailingState!=con.Account.BillingState || con.MailingCountry!=con.Account.BillingCountry) {
                lstConToUpdate.add(con);
            }
        }
        if(lstConToUpdate.size() > 0) {
            processContacts pc = new processContacts();
            pc.syncConAccAddress(lstConToUpdate);
            update lstConToUpdate;
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        //ASSERT: do something here
    }
}