@isTest
global class MuleWebServiceTestMock implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest req) {

        HttpResponse respost = new HttpResponse();
        respost.setHeader('Content-Type', 'application/json');
        String endPoint = String.valueOf(req.getEndpoint());
        if(endPoint.contains('https://gard-oauth-provider-front.eu.cloudhub.io'))  
        {
            respost.setBody('{"access_token":"wxCZ_yae-VTKpNpldKexrIEf_DGOBF1bR75yx_XqThgKkuwutDQs2wQxM8LNnKCkU-6SzHrBOhdAEh1K4tiqXQ", "scope": "READ WRITE", "token_type":"Bearer", "expires_in":3600}');
            respost.setStatusCode(200);
        }
        else if(endPoint.contains('api/documents'))
        {
            respost.setBody('{"status": "success","message": "Document successfully received and in progress of being stored in DMS","correlationId": "2f35e7b0-c5f6-11ea-b784-06b93c5853fc"}');
            respost.setStatusCode(202);
        }
        else if(endPoint.contains('api/legalentities/onrisk'))
        {
            respost.setStatusCode(200);
            respost.setBody('[{'
                +'"crmid":'+[SELECT company_id__c FROM Account where Name = 'Customer Test Account'].company_id__c+','
                +'"partnerName": "Apex Bulk",'
                +'"partnerId": 1021,'
                +'"partnerTypeCode": 5,'
                +'"partnerTypeDescription": "CUSTOMER_DIR",'
                +'"partnerFullName": "Apex Bulk Carriers LLC",'
                +'"partnerLongName": "Apex Bulk Carriers LLC",'
                +'"partnerCountryCode": "USA",'
                +'"partnerCountryDescription": "United States o",'
                +'"stopUse": false,'
                +'"companyOnRisk": true,'
                +'"clientOnRisk": true,'
                +'"brokerOnRisk": false,'
                +'"insurancecompanyOnRisk": false,'
                +'"assuredOnRisk": false,'
                +'"coassuredOnRisk": false,'
                +'"protectedCoassuredOnRisk": false,'
                +'"pandiOwnersOnRisk": false,'
                +'"pandiCharterersOnRisk": false,'
                +'"pandiMOUOnRisk": false,'
                +'"marineOnRisk": true,'
                +'"energyOnRisk": false,'
                +'"buildersOnRisk": false,'
                +'"originalInsuredMutualOnRisk": false,'
                +'"originalInsuredSmallCraftOnRisk": false'
                +'}]'
                );
            }
        System.debug('------success');
        return respost;
    }
}