global with sharing  class ScheduleBatchSendClaimsSurveyNotifctn implements Schedulable{
    global List<Id> userList = new List<Id>();
    global Customer_Feedback__c feedback;
    public String jobName;
    public ScheduleBatchSendClaimsSurveyNotifctn(List<Id> userList,Customer_Feedback__c feedback,String jobName){
        this.userList = userList;
        this.feedback = feedback;
        this.jobName = jobName;
    }
    global void execute(SchedulableContext sc) {
        system.debug('--Send claims customer feedback scheduler fired--');
        ID BatchId = Database.executeBatch(new BatchSendClaimsSurveyNotification(userList, feedback, jobName), 100);
    }
}