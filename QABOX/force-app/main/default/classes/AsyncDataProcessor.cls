global class AsyncDataProcessor {
    @future
    //public method to update Quote Area Managers when the parent Account Area Manager changes
    public static void UpdateOpptyAreaManagers(String strQuery) {
        List<Opportunity> lsOppty = new List<Opportunity>();
        for (Opportunity opp: Database.query(strQuery)) {
            if (opp.Account_Area_Manager__c!=null && (opp.Area_Manager__c!=opp.Account_Area_Manager__c)) {
                opp.Area_Manager__c = opp.Account_Area_Manager__c;
                lsOppty.add(opp);
            }
        }
        if (lsOppty.size()>0) {update lsOppty;}
    }
    
    @future
    //public method to update the Country in Opportunity
    public static void UpdateOpptyCountry(String strQuery) {
        System.debug('###UpdateOpptyCnt1: ');

        List<Opportunity> lsOppty = new List<Opportunity>();
        for (Opportunity opp: Database.query(strQuery)) {
            if (opp.Country_v2__c!= opp.Account.Country__c) {
                System.debug('###UpdateOpptyCnt2: ');
                opp.Country_v2__c = opp.Account.Country__c;
                lsOppty.add(opp);
            }
        }
        if (lsOppty.size()>0) {update lsOppty;}
    }
    
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id; 
    Public static User salesforceLicUser;
    
    @isTest
    public static void runTest() {
        system.debug('from run test ');
        // create a new account
        Account acc = TestDataGenerator.getClientAccount();//new Account(Name = 'test account',Area_Manager__c = userinfo.getuserid(), Market_Area__c = TestDataGenerator.getMarketArea().id);
       // insert acc;
        salesforceLicUser = new User(
                                        Alias = 'standt', 
                                        profileId = salesforceLicenseId ,
                                        Email='standarduser@testorg.com',
                                        EmailEncodingKey='UTF-8',
                                        CommunityNickname = 'test13',
                                        LastName='Testing',
                                        LanguageLocaleKey='en_US',
                                        LocaleSidKey='en_US', 
                                        //contactID = brokercontact.id, 
                                        TimeZoneSidKey='America/Los_Angeles',
                                        UserName='mygardtest008@testorg.com',
                                        City='Arendal'
                                   );
         insert salesforceLicUser;
        // create a new opportunity
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.Name = 'test opportunity';
        opp.StageName = 'test';
        opp.CloseDate = system.today();
        opp.Area_Manager__c = userinfo.getuserid();
      //  opp.Account_Area_Manager__c = salesforceLicUser.ID;
        opp.Type = 'New Business';
        insert opp;
        
        // run a simple test of class
        test.startTest();
        acc.Country__c = TestDataGenerator.getCountry('TESTCOUNTRY').Id;
        update acc;
        
        String queryOpptCntry = 'SELECT Country_v2__c, Account.Country__c FROM Opportunity where Account.Id ='+'\''+acc.id+'\''; 
        UpdateOpptyAreaManagers('SELECT Id, Name, Account_Area_Manager__c, Area_Manager__c FROM Opportunity WHERE Id = \'' + opp.Id + '\'');
        UpdateOpptyCountry(queryOpptCntry);
        test.stopTest();
    }
}