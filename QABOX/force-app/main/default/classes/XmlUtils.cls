/**************************************************************************
* Author  : Phil Spenceley
* Company : cDecisions
* Date    : 26/07/2013
***************************************************************************/
    
/// <summary>
///  Xml Utility class providing some useful helper functions
/// </summary>
public without sharing class XmlUtils {

	public static final String XSI_NS = 'http://www.w3.org/2001/XMLSchema-instance';
	public static final String XSI_PREFIX = 'xsi';
	
    /// <summary>
    ///  Copies one XmlNode (and its children) to another XmlNode
    ///   Caution - The recursive nature of this call use large quantites of script statements on larger Xml Documents
    /// </summary>
    /// <param name="fromNode"></param>
    /// <param name="toNode"></param>
    public static void copyXmlNode(DOM.XMLNode fromNode, DOM.XMLNode toNode) {
        string NS = fromNode.getNamespace();
        string NSpreFix = null;
        if (NS != null) {
            NSpreFix = fromNode.getPrefixFor(fromNode.getNamespace());
        } 
        if (fromNode.getName() != null) {
            DOM.XmlNode newTo = toNode.addChildElement(fromNode.getName(), NS, NSpreFix);
            copyXmlChildren(fromNode, newTo);
            copyXmlAttributes(fromNode, newTo);
        }
    }
	
    /// <summary>
    ///  Copies one XmlNodes children (recursively) to another XmlNode
    ///   Caution - The recursive nature of this call use large quantites of script statements on larger Xml Documents
    /// </summary>
    /// <param name="fromNode"></param>
    /// <param name="toNode"></param>
    public static void copyXmlChildren(DOM.XMLNode fromNode, DOM.XMLNode toNode) {
        for (DOM.XmlNode child : fromNode.getChildren()) {
            string NS = child.getNamespace();
            string NSpreFix = null;
            if (NS != null) {
                NSpreFix = child.getPrefixFor(child.getNamespace());
            } 
            if (child.getName() != null) {
                DOM.XmlNode newTo = toNode.addChildElement(child.getName(), NS, NSpreFix);
                if (child.getChildren() != null) {
                    copyXmlChildren(child, newTo);
                }
                if (child.getText() != null) {
                    newTo.addTextNode(child.getText());
                }
                copyXmlAttributes(child, newTo);
            }            
        }
    }
	
	/// <summary>
    ///  Copies one XmlNodes attributes to another XmlNode
    /// </summary>
    /// <param name="fromNode"></param>
    /// <param name="toNode"></param>
	public static void copyXmlAttributes(DOM.XMLNode fromNode, DOM.XMLNode toNode) {
        for (integer i = 0; i < fromNode.getAttributeCount(); i++) {
            string key = fromNode.getAttributeKeyAt(i);
            string keyNS = fromNode.getAttributeKeyNsAt(i);
            string value = fromNode.getAttributeValue(key, keyNS);
            string valueNS = fromNode.getAttributeValueNs(key, keyNS);
            toNode.setAttributeNs(key, value, keyNS, valueNS);
        }
    }
    
}