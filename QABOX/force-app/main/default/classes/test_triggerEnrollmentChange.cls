@isTest

public class test_triggerEnrollmentChange{
    
    static testMethod void triggerrecentchangeEnrollment(){
        string partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
        
        Account clientAcc = new Account();//[select name,company_id__c from Account where company_id__c='22384'];  
        /*
        AccountFollwer__c afCS = new AccountFollwer__c();
        afCS.name = 'test';
        afCS.check_account_follwer__c = true;
        insert afCS;
        */
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt;
        CommonVariables__c cv= new CommonVariables__c(Name='ReplyToMailId',Value__c='testmail@test.com');
        insert cv;
        clientAcc.Name = 'Testt';
        //clientAcc.company_id__c='22384';
        clientAcc.recordTypeId = System.Label.Client_Contact_Record_Type;
        clientAcc.Site = '_www.test.se';
        clientAcc.Type = 'Customer';
        clientAcc.BillingStreet = 'Gatan 1';
        clientAcc.BillingCity = 'Stockholm';
        clientAcc.BillingCountry = 'SWE';    
        clientAcc.Area_Manager__c = userInfo.getUserId();        
        clientAcc.Market_Area__c = Markt.id;
        clientAcc.PEME_Enrollment_Status__c = 'Enrolled';   
        
        insert clientAcc;
        
        Contact clientContact= new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity = 'London',
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState = 'London',
                                              MailingStreet = '4 London Road',
                                              AccountId = clientAcc.Id,
                                              Email = 'test321@gmail.com'
                                          );
        
        insert clientContact;

        User clientUser = new User( Alias = 'alias_1',
                                    CommunityNickname = 'clientUser',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey='en_US',
                                    LocaleSidKey='en_US',
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testclientUser@testorg.com.mygard',
                                    ContactId = clientContact.id
                                   );
            
        insert clientUser;
        
        PEME_Enrollment_Form__c pefClient = new PEME_Enrollment_Form__c();
        pefClient.ClientName__c=clientAcc.ID;
        pefClient.Enrollment_Status__c='Draft';
        pefClient.Submitter__c= clientContact.id;
        insert pefClient;
        
        PEME_Manning_Agent__c pmgClient=new PEME_Manning_Agent__c();
        pmgClient.Client__c=clientAcc.ID;
        pmgClient.PEME_Enrollment_Form__c=pefClient.ID;
        pmgClient.Company_Name__c='Testc Company';
        pmgClient.Client__c = clientAcc.id;
        pmgClient.Contact_Point__c=clientContact.Id;
        pmgClient.Status__c='Approved';
        pmgClient.Requested_Address__c='testcaddr1';
        pmgClient.Requested_Address_Line_2__c='testcaddr2';
        pmgClient.Requested_Address_Line_3__c='testcaddr3';
        pmgClient.Requested_Address_Line_4__c='testcaddr4';
        pmgClient.Requested_City__c='testcCity1';
        pmgClient.Requested_Contact_Person__c='testc person1';
        pmgClient.Requested_Country__c='testccountry1';
        pmgClient.Requested_Email__c='testc1@mail.com';
        pmgClient.Requested_Phone__c='1154228855';
        pmgClient.Requested_State__c='testcstate1';
        pmgClient.Requested_Zip_Code__c='testczip';
        
        insert pmgClient;
        
        PEME_Debit_note_detail__c pefDebitClient = new PEME_Debit_note_detail__c();
        pefDebitClient.PEME_Enrollment_Form__c = pefClient.id;
        pefDebitClient.Company_Name__c = clientAcc.id;
        pefDebitClient.Status__c = 'Approved';
        insert pefDebitClient;
        
        pefClient.Enrollment_Status__c='Under Approval';
        update pefClient;
        
        pefClient.Enrollment_Status__c='Enrolled';
        update pefClient;
        
        pefClient.Enrollment_Status__c='Disenrolled';     //SF-4176
        update pefClient;
        
    }
    
    static testmethod void triggerrecentChangeOnManningAgent(){
        /*
        AccountFollwer__c afCS = new AccountFollwer__c();
        afCS.name = 'test 1';
        afCS.check_account_follwer__c = true;
        insert afCS;
        */
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt;
        
        Account clientAcc= New Account();
        clientAcc.Name = 'Testt';
        //clientAcc.company_id__c=22384;
        clientAcc.recordTypeId = System.Label.Client_Contact_Record_Type;
        clientAcc.Site = '_www.test.se';
        clientAcc.Type = 'Customer';
        clientAcc.BillingStreet = 'Gatan 1';
        clientAcc.BillingCity = 'Stockholm';
        clientAcc.BillingCountry = 'SWE';    
        clientAcc.Area_Manager__c = userInfo.getUserId();        
        clientAcc.Market_Area__c = Markt.id;
        clientAcc.PEME_Enrollment_Status__c = 'Enrolled';   
        
        insert clientAcc;
        
        Account clientAcc1= New Account();
        clientAcc1.Name = 'Testt';
        //clientAcc.company_id__c=22384;
        clientAcc1.recordTypeId = System.Label.Client_Contact_Record_Type;
        clientAcc1.Site = '_www.test.se';
        clientAcc1.Type = 'Customer';
        clientAcc1.BillingStreet = 'Gatan 1';
        clientAcc1.BillingCity = 'Stockholm';
        clientAcc1.BillingCountry = 'SWE';    
        clientAcc1.Area_Manager__c = userInfo.getUserId();        
        clientAcc1.Market_Area__c = Markt.id;
        clientAcc1.PEME_Enrollment_Status__c = 'Enrolled';   
        
        insert clientAcc1;
        
        Contact clientContact= new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity = 'London',
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState = 'London',
                                              MailingStreet = '4 London Road',
                                              AccountId = clientAcc.Id,
                                              Email = 'test321@gmail.com'
                                          );
        
        insert clientContact;
        
        List<Group> queue_list = [select Id from Group where Name = 'PEME GARD ARENDAL QUEUE' and Type = 'Queue' limit 1];
        
        PEME_Enrollment_Form__c pefClient = new PEME_Enrollment_Form__c();
        pefClient.ClientName__c=clientAcc.ID;
        pefClient.Enrollment_Status__c='Draft';
        pefClient.Submitter__c= clientContact.id;
        //fClient.ownerId = queue_list[0].id;
        insert pefClient;
        
       
        
        PEME_Manning_Agent__c pmgClient=new PEME_Manning_Agent__c();
        pmgClient.Client__c=clientAcc.ID;
        pmgClient.PEME_Enrollment_Form__c=pefClient.ID;
        pmgClient.Company_Name__c='Testc Company';
        pmgClient.Client__c = clientAcc.id;
        pmgClient.Contact_Point__c=clientContact.Id;
        pmgClient.Status__c='Approved';
        pmgClient.Requested_Address__c='testcaddr1';
        pmgClient.Requested_Address_Line_2__c='testcaddr2';
        pmgClient.Requested_Address_Line_3__c='testcaddr3';
        pmgClient.Requested_Address_Line_4__c='testcaddr4';
        pmgClient.Requested_City__c='testcCity1';
        pmgClient.Requested_Contact_Person__c='testc person1';
        pmgClient.Requested_Country__c='testccountry1';
        pmgClient.Requested_Email__c='testc1@mail.com';
        pmgClient.Requested_Phone__c='1154228855';
        pmgClient.Requested_State__c='testcstate1';
        pmgClient.Requested_Zip_Code__c='testczip';
        
        insert pmgClient;
        
        PEME_Debit_note_detail__c pefDebitClient = new PEME_Debit_note_detail__c();
        pefDebitClient.PEME_Enrollment_Form__c = pefClient.id;
        pefDebitClient.Company_Name__c = clientAcc.id;
        pefDebitClient.Status__c = 'Approved';
        insert pefDebitClient;
        
        pmgClient.Client__c = clientAcc.id;
        update pmgClient;
        
        pmgClient.Client__c = clientAcc1.id;
        update pmgClient;//clientAcc1
        
         pefClient.Enrollment_Status__c='Enrolled';
        pefClient.ownerId = queue_list[0].id;
        update pefClient;
        
        pefClient.Enrollment_Status__c='Disenrolled';     //SF-4176
        update pefClient; 

        Delete pefClient;
    }
}