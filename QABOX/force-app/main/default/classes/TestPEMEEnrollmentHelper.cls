/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class TestPEMEEnrollmentHelper {
    
    @TestSetup
    
    static void testData(){
        /*CommonVariables__c cvar = new CommonVariables__c(name = 'ReplyToMailId', value__c = 'Test123@gmail.com');
        insert cvar;*/
        test.startTest();
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c = '');
        insert vrcClient;
        Country__c country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA');
        insert country;
        CommonVariables__c cvar = new CommonVariables__c(name = 'ReplyToMailId', value__c ='Test123@gmail.com');
        insert cvar;
        Account clientAcc = new Account(  Name = 'Testuu', 
                                                    Site = '_www.test_1.s', 
                                                    BillingStreet = 'Gatanfol 1',
                                                    BillingCity = 'phuket',  
                                                    Client_On_Risk_Flag__c = true,
                                                    company_Role__c =  'Client' ,
                                                    recordTypeId = System.Label.Client_Contact_Record_Type,
                                                    guid__c = '8ce8ad66-a6ec-1834-9e21',
                                                    Role_Client__c = true,
                                                    Company_Status__c = 'Active',
                                                    P_I_Member_Flag__c = true,
                                                    PEME_Enrollment_Status__c = 'Enrolled',
                                        			Country__c = country.id
                                       );
        insert clientAcc;
        Contact clientContact = new Contact(lastName = 'Submitter',accountId = clientAcc.id);
        insert clientContact;
        PEME_Enrollment_Form__c eForm = new PEME_Enrollment_Form__c(enrollment_status__c = 'Draft',ClientName__c = clientAcc.id,Submitter__c = clientContact.id);
        insert eForm;
        PEME_Debit_note_detail__c debitNote = new PEME_Debit_note_detail__c(PEME_Enrollment_Form__c = eForm.id,Company_Name__c = clientAcc.id,Status__c = 'Approved');
        insert debitNote;
        PEME_Manning_Agent__c manningAgent = new PEME_Manning_Agent__c(PEME_Enrollment_Form__c = eForm.id,Client__c = clientAcc.id,Contact_Point__c = clientContact.id,status__c = 'Approved');
        insert manningAgent;
        test.stopTest();
    }
    static testMethod void myUnitTest() {
        PEME_Enrollment_Form__c eForm = [SELECT id,enrollment_status__c FROM PEME_Enrollment_Form__c WHERE enrollment_status__c = 'Draft'];
        test.startTest();
        system.debug('before if eForm:::::::::::'+eForm.Id);
        if(eForm != null){
            eForm.enrollment_status__c = 'Under Approval';
            system.debug('within if eForm:::::::::::'+eForm.Id);
            update eForm;
            eForm.enrollment_status__c = 'Draft';
            //PEMEEnrollmentHelper.afterDelete(new List<PEME_Enrollment_Form__c>{eForm});
            test.stopTest();
            delete eForm;
        }
    }
}