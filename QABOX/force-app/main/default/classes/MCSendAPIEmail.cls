//This class is used to send Marketing cloud emails Triggered as per fluidoSurveyInvitation_BeforeInsertBeforeDeleteAfterInsertAfterUpdate written on Kysely object
global class MCSendAPIEmail{
    public static string strAuthorizationHeader;
    public static string strAccessToken;
    public static string strRequestbody;   
    public static string strToAddress;
    public static string strSubcriberKey;
    public static string strEventTitle;
    public static string strLinkText;
    public static string strOrganizerName;
    public static string strTSDExternalKey;
    public static Boolean isSuccess;
    public static List<MarketingCloudAPICall__c> lstDetails;
    
    public MCSendAPIEmail()
    {
        
        strAuthorizationHeader = '';
        strAccessToken = '';
        strRequestbody = '';       
        strToAddress = '';
        strSubcriberKey = '';
        strTSDExternalKey = '';
        strEventTitle = '';
        strLinkText = '';
        strOrganizerName = '';
        lstDetails = new List<MarketingCloudAPICall__c>();
        isSuccess = false;
    }
    
    
    @future(callout=true)
    public static void sendEmail(String eventMemberId,String toAdd, String extKey, String eventTitle, String linkText, String organizerName, String organizerEmail, String evDate, String eventType,String urlStr)        
    {
         // passing eventMemberId extram parameter for SF-4359
        // variable assignment   
        //toadd='krishna.kundan@hotmail.com';     
        strToAddress = toAdd;
        //strSubcriberKey = toAdd; commented for SF-4359
        strSubcriberKey = eventMemberId;
        strTSDExternalKey = extKey;
        strEventTitle = eventTitle;
        strLinkText = linkText;
        strOrganizerName = organizerName;
        String fromName = organizerName;
        String fromAdd = organizerEmail;
        String dtEvent = evDate;
        String urlEvent = urlStr;
        lstDetails = [SELECT Id, ClientId__c , AccessToken__c , ClientSecret__c,EndPoint_SendEmail__c, EndPoint__c FROM MarketingCloudAPICall__c WHERE Name = 'APICallDetail'];
        //system.debug('Name:'+Name);
        try{
            isSuccess = false;
            Http http = new Http();
            HttpRequest req = new HttpRequest();            
            //system.debug('AccessToken::'+lstDetails[0].AccessToken__c);
            //in case access token is blank
            if(lstDetails[0].AccessToken__c == '' || lstDetails[0].AccessToken__c == null)
            {
                isSuccess = generateAccessToken();
                //if(isSuccess)
                //lstDetails = [SELECT Id, AccessToken__c FROM MarketingCloudAPICall__c WHERE Name = 'APICallDetail'];
            }
            //system.debug('isSuccess::'+isSuccess);
            //system.debug('lstDetails::'+lstDetails);
            //system.debug('lstDetails size::'+lstDetails.size());
            //system.debug('isSuccess::'+isSuccess);
            if((isSuccess)||(lstDetails != null && lstDetails.size() > 0 && lstDetails[0].AccessToken__c != ''))
            {
                //populateRequiredDetails();                               
                strAccessToken = lstDetails[0].AccessToken__c;
                string authorizationHeader = 'Bearer '+ strAccessToken;
                strRequestBody ='{"To": {"Address": "'+strToAddress+'","SubscriberKey": "'+strSubcriberKey+'","ContactAttributes": {"SubscriberAttributes": {"fromAddress": "'+fromAdd+'", "fromName": "'+fromName+'" ,"Title": "'+eventTitle+'", "LinkText": "'+linkText+'", "Organiser": "'+organizerName+'", "date": "'+dtEvent+'", "EventType": "'+eventType+'" , "CalenderLink": "'+urlStr+'"}}},"OPTIONS": {"RequestType": "SYNC"}}';      
                System.debug('@@@@@@@@@@@@@@================>'+strRequestBody);            
                req.setBody(strRequestBody);
                req.setEndpoint(lstDetails[0].EndPoint_SendEmail__c+'key:'+strTSDExternalKey+'/send');
                req.setMethod('POST');
                req.setHeader('Content-Type', 'application/json');
                req.setHeader('Authorization', authorizationHeader);
                HttpResponse res = http.send(req); 
                //System.debug('@@@@@@@@@@@@@@================>'+res);
                if(res.getStatus() == 'Unauthorized' || res.getStatusCode() == 401)
                {
                    isSuccess = generateAccessToken();
                    if(isSuccess)
                    {
                        //System.debug('@@@@@@@@@@@@@@================strAccessToken in Send Email method>'+strAccessToken);
                        authorizationHeader = 'Bearer '+ strAccessToken;
                        req.setHeader('Authorization', authorizationHeader);
                        res = http.send(req); 
                    }
                }
                if(res.getStatus() == 'Accepted' && res.getStatusCode() == 202)
                {
                    update lstDetails;
                    //System.debug('Response:::'+res.getBody());
                    JSONParser parser = JSON.createParser(res.getBody());
                    while (parser.nextToken() != null) {
                        if(parser.getText() == 'hasErrors') {
                            parser.nextToken();
                            if(parser.getText() == 'false')
                                System.debug('Email Sent!!');                            
                        }
                    }
                }
                
            }
        }         
        catch(Exception e)
        {
            System.debug('Exception:'+e);
        }
    }
    
    /*to get the details required to send email
    public static void populateRequiredDetails(String fromAdd, String frName, String toAdd, String extKey, String eventTitle, String linkText, String organizerName)
    {
        strFromEmailAddress = fromAdd;
        strFromName = frName;
        strToAddress = toAdd;
        strSubcriberKey = fromAdd;
        strTSDExternalKey = extKey;
        strEventTitle = eventTitle;
        strLinkText = linkText;
        strOrganizerName = organizerName;
    }*/
    
    //generate access token
    public static Boolean generateAccessToken(){
        try{
            Http http = new Http();
            HttpRequest req = new HttpRequest();        
            //lstDetails = [SELECT Id, ClientId__c , AccessToken__c , ClientSecret__c,EndPoint_SendEmail__c, EndPoint__c FROM MarketingCloudAPICall__c WHERE Name = 'APICallDetail'];
            if(lstDetails != null && lstDetails.size()>0)
            {
                String strClientId = lstDetails[0].ClientId__c;
                String strClientSecret = lstDetails[0].ClientSecret__c;
                if(strClientId != null && strClientId != '' && strClientSecret != '' && strClientSecret != null)
                {
                    String strBody = 'clientId='+strClientId+'&clientSecret='+strClientSecret;
                    req.setEndpoint(lstDetails[0].EndPoint__c);
                    req.setMethod('POST');
                    req.setBody(strBody);
                    HttpResponse res = http.send(req);
                    if(res.getStatusCode() == 200) //status = ok
                    {
                        JSONParser parser = JSON.createParser(res.getBody());
                        while (parser.nextToken() != null) {
                            if(parser.getText() == 'accessToken') {
                                parser.nextToken();
                                lstDetails[0].AccessToken__c = parser.getText();  
                                strAccessToken = parser.getText();
                                System.debug('@@@@@@@@@@@@@@================parser text>'+parser.getText()); 
                                System.debug('@@@@@@@@@@@@@@================strAccessToken in Generate AccessToken method>'+strAccessToken);
                            }
                        }
                        //update lstDetails;
                        return true;
                        //calling send email method
                        //sendEmail();
                    }
                    else return false;
                }
                else return false;
            }
            else return false;
        }
        catch(Exception e)
        {
            System.debug('Exception:'+e);
            return false;
        }
        
    }
}