public with sharing class EventTriggerHelper {
    
    public static void afterInsert(List<Event>eventList){
        createTask(eventList);
        createEventChatterPost(eventList);
    }
    public static void afterUpdate(List<Event>newEvtList,Map<id,Event>oldEvtMap){
        List<FeedItem> feedItemList = new List<FeedItem>();
        For(Event e_new:newEvtList){
            if(e_new.WhatId != null && e_new.Type != null && e_new.Type.equalsignoreCase('Meeting') && e_new.description!=oldEvtMap.get(e_new.id).description){
                if(e_new.WhatId.getsObjectType() == Opportunity.sObjectType){
                    feedItemList.add(addChatterPost(e_new,e_new.whatId,'','Meeting Update'));
                    Id accId = [select id,AccountId from Opportunity where id=:e_new.WhatId].AccountId;
                    feedItemList.add(addChatterPost(e_new,accId,'','Meeting Update'));
                }
                else if(e_new.WhatId.getsObjectType() == Account.sObjectType){
                    {//added for SF-5223
                            EventRelationHelper.HierarchyEventValues evrHev = EVentRelationHelper.getTopLevelElement(e_new.WhatId);
                            if(evrHev != null){
                                String siteCountry;
                                if(!String.isBlank(evrHev.initiatingAccount.site) && evrHev.initiatingAccount.country__c != null && !String.isBlank(evrHev.initiatingAccount.site)) siteCountry = ' ('+evrHev.initiatingAccount.site+', '+evrHev.initiatingAccount.country__r.Country_Name__c+')';
                                else if(!String.isBlank(evrHev.initiatingAccount.site)) siteCountry = ' ('+evrHev.initiatingAccount.site+')';
                                else if(evrHev.initiatingAccount.country__c != null && !String.isBlank(evrHev.initiatingAccount.country__r.Country_Name__c)) siteCountry = ' ('+evrHev.initiatingAccount.country__r.Country_Name__c+')';
                                else siteCountry = '';
                                
                                String relatedToStr = '\nRelated To: '+evrHev.initiatingAccount.name+siteCountry;
                                    
                                if(evrHev.initiatingAccount != null)feedItemList.add(addChatterPost(e_new,evrHev.initiatingAccount.Id,relatedToStr,'Meeting Update'));
                                if(evrHev.rootAccount != null && evrHev.initiatingAccount.Id != evrHev.rootAccount.Id) feedItemList.add(addChatterPost(e_new,evrHev.rootAccount.Id,relatedToStr,'Meeting Update'));
                            }
                    }
                    //feedItemList.add(addChatterPost(e_new,e_new.whatId,'Meeting Update'));//commented for SF-5223
                }
            }
        }
        try{
            if(feedItemList.size()>0){
                insert feedItemList;
            }
        }catch(exception ex){}
    }
    private static void createEventChatterPost(List<Event>eventList){
        List<feedItem>feedItemList=new List<feedItem>();
        for(Event e_new:eventList){ 
            if(e_new.WhatId != null && (e_new.Type!= null && e_new.Type.equalsignoreCase('meeting'))){
                if(e_new.WhatId != null ){//#7&&
                    if(e_new.WhatId.getsObjectType() == Account.sObjectType){
                        //FeedItem fitem = addChatterPost(e_new,e_new.whatId,'New Meeting');//Commented for SF-5223
                        {//added for SF-5223
                            EventRelationHelper.HierarchyEventValues evrHev = EVentRelationHelper.getTopLevelElement(e_new.WhatId);
                            if(evrHev != null){
                                String siteCountry;
                                if(!String.isBlank(evrHev.initiatingAccount.site) && evrHev.initiatingAccount.country__c != null && !String.isBlank(evrHev.initiatingAccount.site)) siteCountry = ' ('+evrHev.initiatingAccount.site+', '+evrHev.initiatingAccount.country__r.Country_Name__c+')';
                                else if(!String.isBlank(evrHev.initiatingAccount.site)) siteCountry = ' ('+evrHev.initiatingAccount.site+')';
                                else if(evrHev.initiatingAccount.country__c != null && !String.isBlank(evrHev.initiatingAccount.country__r.Country_Name__c)) siteCountry = ' ('+evrHev.initiatingAccount.country__r.Country_Name__c+')';
                                else siteCountry = '';
                                
                                String relatedToStr = '\nRelated To: '+evrHev.initiatingAccount.name+siteCountry;
                                if(evrHev.initiatingAccount != null)feedItemList.add(addChatterPost(e_new,evrHev.initiatingAccount.Id,relatedToStr,'New Meeting'));
                                if(evrHev.rootAccount != null && evrHev.initiatingAccount.Id != evrHev.rootAccount.Id) feedItemList.add(addChatterPost(e_new,evrHev.rootAccount.Id,relatedToStr,'New Meeting'));
                            }
                        }
                    }
                    if(e_new.WhatId.getsObjectType() == Opportunity.sObjectType){
                        feedItemList.add(addChatterPost(e_new,e_new.whatId,'','New Meeting'));
                        Id accId = [select id,AccountId from Opportunity where id=:e_new.WhatId].AccountId;
                        feedItemList.add(addChatterPost(e_new,accId,'','New Meeting'));
                    }
                }    
            }
        }
        try{
            if(feedItemList.size()>0){
                insert feedItemList;
            }
        }catch(exception ex){}
    }
    
    private static feedItem addChatterPost(event e_new,Id parentId,String relatedToStr, String evtType){
        String evtAttendees = fetchEventAttendees(e_new);
        FeedItem fitem = new FeedItem(); 
        //******If, event is linked with an oppty then it will link the chatter post to opportunity's Company******4072.. Date - 19/01/2018
        fitem.type = 'LinkPost';
        fitem.ParentId = parentId;
        
        String LEXUrl = Label.InternalLightningOrgURL.substring(0,Label.InternalLightningOrgURL.lastIndexOf('.com')) + '.com/lightning/r/Event/'+e_new.id+'/view';
        //String LEXUrl = Label.InternalLightningOrgURL.substring(0,Label.InternalLightningOrgURL.lastIndexOf('.com')) + '.com/lightning/r/'+e_new.id+'/view';
        fitem.LinkUrl = LEXUrl;
        fitem.Title = 'View Meeting'; 
        fitem.Body = evtType + ((e_new.Location__c) != null ? ', ' + e_new.Location__c : '')
            + relatedToStr
            + '\nStart: ' + e_new.StartDateTime.format() + ',  End:' + e_new.EndDateTime.format()
            + '\nSubject: ' + e_new.Subject
        	+ '\nAttendees: '+ evtAttendees;
            //+ '\nMeeting Link: '+LEXUrl;
        
        return fitem;
    }
    
    
    private static void createTask(List<Event>eventList){
        
        List<task>taskList=new List<Task>();
        for(Event e_new:eventList){
            if(e_new.WhatId != null && (e_new.Type!= null && e_new.Type.equalsignoreCase('meeting'))){//#4
                if(!e_new.IsDuplicate__c)
                {//#5
                    Task t = new Task();
                    //added for SF-5146 -Naren
                    /*When meeting is created with description field Null value ,task created automatically 
with status 'Not Started'and if Event description field has an Value,then task will not create*/
                    if(e_new.Description == null || e_new.Description == '') 
                    {//#6
                        t.whatId = e_new.WhatId;
                        t.meeting_id__c = e_new.id;
                        t.meeting_name__c = e_new.subject;
                        t.OwnerId = e_new.OwnerId;
                        t.Subject = 'Complete Meeting minutes - ' + e_new.subject;
                        t.Priority = 'Normal';                
                        t.ActivityDate = date.newinstance(e_new.EndDateTime.year(), e_new.EndDateTime.month(), e_new.EndDateTime.day()); //SF 3863
                        t.Status = 'Not Started';
                        // added for SF-3853
                        t.isComplete__c= true;
                        taskList.add(t);
                        
                        // End of SF-5146 -Naren
                    }//#6
                }//#5
            }
        } 
        try{
            if(taskList.size()>0){
                insert taskList;
            }
        }catch (exception ex){}
    }
    private static String fetchEventAttendees(Event e_new){
        String attendeesStr = '';
        for(EventRelation er : [SELECT AccountId,EventId,Id,IsInvitee,IsParent,Relation.name,Status FROM EventRelation WHERE EventId =: e_new.Id AND IsWhat = false]){
        	attendeesStr = attendeesStr + er.Relation.name + ', ';
        }
        if(!String.isBlank(attendeesStr) && attendeesStr.contains(','))
            attendeesStr = attendeesStr.removeEnd(', ');
        return attendeesStr;
    }
}