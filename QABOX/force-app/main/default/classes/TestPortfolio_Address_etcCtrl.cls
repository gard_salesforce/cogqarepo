@isTest(seeAllData=false)
Class TestPortfolio_Address_etcCtrl
{
      /*  Public static List<Account> AccountList=new List<Account>();
        Public static List<Contact> ContactList=new List<Contact>();
        Public static List<Contract> ContractList=new List<Contract>();
        Public static List<User> UserList=new List<User>();
        Public static List<Asset> AssetList=new List<Asset>();
        Public static List<Case> caseList =new List<Case>();
        Public static List<Market_Area__c> Market_AreaList=new List<Market_Area__c>();
        Public static List<Object__c> ObjectList=new List<Object__c>();
        Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Partner Community' and name='Partner Community Login User Custom' limit 1].id;
        Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
        public static Account clientAcc;
        public static Account brokerAcc;
        public static Contact brokerContact;
        public static Contact clientContact;
        public static User brokerUser;
        public static User clientUser;
        public static Contract clientContract;
        public static Contract brokerContract;
        public static Object__c obj;
        public static Asset brokerAsset;
        public static Asset clientAsset;
        public static Market_Area__c Markt;
        public static User salesforceLicUser;
        public static Case Client_Case;
        public static Case ClientCase_new;
        public static Case clientCase;
        public static Case clientCase_1;
        public static Case clientCase_2;
        public static Gard_Contacts__c grdobj;
        public static AccountToContactMap__c map_broker, map_client;
        
        public static LogInDownTime__c LogInDownTime, LogInDownTime1;
        
    public static void cover()
    { 
          
          LogInDownTime = new LogInDownTime__c(Name ='DetailMessage', Message__c = 'We will be back soon.');          
          LogInDownTime1 = new LogInDownTime__c(Name ='TopMessage', Message__c = 'We will be back soon.');
          insert LogInDownTime;
          insert LogInDownTime1;
          
          Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
          Market_AreaList.add(Markt);
          insert Market_AreaList;  
          
          salesforceLicUser = new User(
                                            Alias = 'standt', 
                                            profileId = salesforceLicenseId ,
                                            Email='standarduser@testorg.com',
                                            EmailEncodingKey='UTF-8',
                                            CommunityNickname = 'test13',
                                            LastName='Testing',
                                            LanguageLocaleKey='en_US',
                                            LocaleSidKey='en_US',  
                                            TimeZoneSidKey='America/Los_Angeles',
                                            UserName='test008@testorg.com.mygard'
                                           );
        insert salesforceLicUser;
        
         grdobj =new  Gard_Contacts__c(
                         FirstName__c = 'Test',
                         LastName__c = 'Baann',
                         //Company_Type__c = 'Grd', 
                         Email__c = 'WR_gards.12@Test.com',
                         //MailingCity__c = 'Arendal', 
                         //MailingCountry__c = 'Norway', 
                         //MailingPostalCode__c =  '487155',
                         //MailingState__c = 'Oslo', 
                         //MailingStreet__c = 'Rd Road', 
                         //Mobile__c = '98323322', 
                         MobilePhone__c = '548645', 
                         Nick_Name__c  = 'NilsPeter', 
                         Office_city__c = 'Arendal', 
                         Phone__c = '5454454',
                         Portal_Image__c ='kjhjkdfhjifh.src.amg;jkhsd12333.grd', 
                         Title__c = 'test'
                         );
        insert grdobj ; 
          
       //Accounts............
       brokerAcc = new Account(  Name='Gardcon 1',
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',
                                    recordTypeId=System.Label.Broker_Contact_Record_Type,
                                    Site = '_www.cts.se',
                                    Type = 'Broker',
                                    Market_Area__c = Markt.id,
                                    guid__c = '8ce8ad66-a6ec-1834-9e21',
                                    Area_Manager__c = salesforceLicUser.id                                                                       
                                 );
       AccountList.add(brokerAcc);
    
      
       clientAcc = new Account( Name = 'Gardcon', 
                                        Site = '_www.test_1.se', 
                                        Type = 'Client', 
                                        BillingStreet = 'Gatan 1',
                                        BillingCity = 'Stockholm', 
                                        BillingCountry = 'SWE', 
                                        BillingPostalCode = 'BS1 1AD',
                                        guid__c = '8ce8ad66-a6ec-1834-9e21',
                                        recordTypeId=System.Label.Client_Contact_Record_Type,
                                        Market_Area__c = Markt.id,
                                        Area_Manager__c = salesforceLicUser.id 
                                       );
                                    
        AccountList.add(clientAcc); 
        insert AccountList; 
        //Contacts.................
        brokerContact = new Contact( 
                                             FirstName='Raan',
                                             LastName='Baan',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState = 'London',
                                             MailingStreet = '1 London Road',
                                             AccountId = brokerAcc.Id,
                                             No_Longer_Employed_by_Company__c = false,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(brokerContact) ;

        clientContact= new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity = 'London',
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState = 'London',
                                              MailingStreet = '4 London Road',
                                              No_Longer_Employed_by_Company__c = false,
                                              AccountId = clientAcc.Id,
                                              Email = 'test321@gmail.com'
                                          );
        ContactList.add(clientContact);
        insert ContactList;
        
        //Users..................
        brokerUser = new User(
                                    Alias = 'standt', 
                                    profileId = partnerLicenseId,
                                    Email='mdjawedm@gmail.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='estTing',
                                    LanguageLocaleKey='en_US',
                                    CommunityNickname = 'brokerUser',
                                    LocaleSidKey='en_US',  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testbrokerUser@testorg.com.mygard',
                                    ContactId = BrokerContact.Id,
                                    ContactId__c = grdobj.id
                                   );
        UserList.add(brokerUser);
                                   
        clientUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'clientUser',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey='en_US',
                                    LocaleSidKey='en_US',
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testclientUser@testorg.com.mygard',
                                    ContactId = clientContact.id,
                                    ContactId__c = grdobj.id
                                   );
                
         UserList.add(clientUser) ;
         insert UserList;
         
         map_broker = new AccountToContactMap__c(AccountId__c=brokerAcc.id,Name=brokerContact.id);
         map_client = new AccountToContactMap__c(AccountId__c= clientAcc.id,Name=clientContact.id);
         
         Insert map_broker;
         Insert map_client;
         
        brokerContract = New Contract(Accountid = brokerAcc.id, 
                                        Account = brokerAcc, 
                                        Status = 'Draft',
                                        CurrencyIsoCode = 'SEK', 
                                        StartDate = Date.today(),
                                        ContractTerm = 2,
                                        Agreement_Type__c = 'Mou Operators', 
                                        Broker__c = brokerAcc.id,
                                        Client__c = clientAcc.id,
                                        Broker_Name__c = brokerAcc.id,
                                        Expiration_Date__c = date.valueof('2015-03-01'),
                                        Inception_date__c = date.valueof('2012-01-01'),
                                        Agreement_Type_Code__c = 'Agreement123',
                                        Agreement_ID__c = 'Agreement_ID_879',
                                        Business_Area__c='Marine',
                                        Accounting_Contacts__c = brokerUser.id,
                                        Contract_Reviewer__c = brokerUser.id,
                                        Contracting_Party__c = brokerAcc.id,
                                        Shared_With_Client__c = true                                           
                                        );
         
         ContractList.add(brokerContract);
         
         clientContract = New Contract(Accountid = clientAcc.id, 
                                        Account = clientAcc, 
                                        Status = 'Draft',
                                        CurrencyIsoCode = 'SEK', 
                                        StartDate = Date.today(),
                                        ContractTerm = 2,
                                        Broker__c = brokerAcc.id,
                                        Client__c = clientAcc.id,
                                        Expiration_Date__c = date.valueof('2015-03-21'),
                                        Agreement_Type__c = 'Mou Operators',
                                        Agreement_Type_Code__c = 'Agreement123',
                                        Inception_date__c = date.valueof('2012-01-01'),
                                        Business_Area__c='P&I',
                                        Shared_With_Client__c = True                                        
                                        );
         
         ContractList.add(clientContract);
         insert ContractList ;  
         
         obj = New Object__c( Dead_Weight__c= 20,Object_Unique_ID__c = '1234lkolko', name = 'test_object');
         ObjectList.add(obj);
         insert ObjectList ;   
         
         brokerAsset = new Asset(Name= 'Asset 1',
                                 accountid =brokerAcc.id,
                                 contactid = BrokerContact.Id,  
                                 Agreement__c = brokerContract.id, 
                                 Cover_Group__c = 'P&I',
                                 Cover_Group_Code__c = 'abcde12345',
                                 CurrencyIsoCode='USD',
                                 product_name__c = 'Hull & Machinery',
                                 Object__c = obj.id,
                                 Underwriter__c = brokerUser.id,
                                 Risk_ID__c = '123456ris',
                                 Expiration_Date__c = date.valueof('2015-01-01'),
                                 Inception_date__c = date.valueof('2012-01-01'),
                                 Status = 'Purchased'
                                 ); 
         AssetList.add(brokerAsset);
         
         clientAsset = new Asset(Name= 'Asset 1',
                                 accountid =clientAcc.id,
                                 contactid = clientContact.Id,  
                                 Agreement__c = clientContract.id, 
                                 Cover_Group__c = 'P&I',
                                 Cover_Group_Code__c = 'abcde1234545',
                                 CurrencyIsoCode='USD',
                                 product_name__c = 'Hull Interest',
                                 Object__c = obj.id,
                                 Underwriter__c = clientUser.id,
                                 Expiration_Date__c = date.valueof('2015-01-01'),
                                 Inception_date__c = date.valueof('2012-01-01'),
                                 Risk_ID__c = '123456ri',
                                 Status = 'Registered'
                                 ); 
         AssetList.add(clientAsset);   
         insert AssetList;
         
         Client_Case = new Case(Accountid = clientAcc.id,
                            ContactId = clientContact.id,
                            Origin = 'Community',
                            Object__c = obj.id,
                            //Version_Key__c = 'kolka2342',
                            Claim_Incurred_USD__c= 500,
                            Claim_Reference_Number__c='kol23232',
                            Reserve__c = 350,
                            Paid__c =50000,
                            Total__c= 750,
                            //Voyage_To__c = 'Voyage_To',
                            Member_reference__c = 'ash1232',
                            //Voyage_From__c = 'Voyage_From',
                            Claim_Type__c = 'Cargo',
                            Risk_Coverage__c = clientAsset.id,
                            Status = 'Open',
                            Contract_for_Review__c = brokerContract.id,
                            Total_100__c = 112,
                            Claims_handler__c = brokerUser.id,
                            Status_Change_Date__c = datetime.now()                                                   
                            );
        caseList.add(Client_Case);
        
        ClientCase_new = new Case(Accountid = clientAcc.id,
                            ContactId = clientContact.id,
                            Origin = 'Community',
                            Object__c = obj.id,
                            //Version_Key__c = 'kolka232',
                            Claim_Incurred_USD__c= 1500,
                            Claim_Reference_Number__c='ko8l23232',
                            Reserve__c = 3502,
                            Paid__c =500002,
                            Total__c= 7502,
                            //Voyage_To__c = 'test_Voyage_To',
                            Member_reference__c = 'wash1232',
                            //Voyage_From__c = 'test_Voyage_From',
                            Claim_Type__c = 'Cargo',
                            Risk_Coverage__c = clientAsset.id,
                            Status = 'Open',
                            Contract_for_Review__c = brokerContract.id,
                            Total_100__c = 1128,
                            Claims_handler__c = brokerUser.id,
                            Status_Change_Date__c = datetime.now()                                                  
                            );
        caseList.add(ClientCase_new);
                
        clientCase = new Case(Accountid = clientAcc.id,
                             ContactId = clientContact.id, 
                             Origin = 'Community',
                             Object__c = obj.id, 
                             Claim_Incurred_USD__c = 50000,
                             Event_details__c = 'Testing ReportedClaimsCtrl',
                             Paid__c = 50000, 
                             Event_Type__c = 'Incident', 
                             //Version_Key__c = 'hgdsjhdAs232',
                             Claim_Type__c = 'Cargo',
                             Event_Date__c = Date.valueof('2014-12-12'),
                             Claim_Reference_Number__c = 'ClaiMRef1234', 
                             //Claims_Currency__c = 'USD',
                             //Voyage_To__c = 'Voyage_To',
                             //Voyage_From__c = 'Voyage_From',
                             Risk_Coverage__c = clientAsset.id,
                             Status = 'Open',
                             Contract_for_Review__c = clientContract.id,
                             Total_100__c = 1124,
                             Claims_handler__c = brokerUser.id,
                             Status_Change_Date__c = datetime.now()
                              );
        caseList.add(clientCase);
        
        clientCase_1 = new Case(Accountid = clientAcc.id,
                             ContactId = clientContact.id, 
                             Origin = 'Community',
                             Object__c = obj.id, 
                             Claim_Incurred_USD__c = 50000,
                             Event_details__c = 'Testing ReportedClaimsCtrl',
                             Paid__c = 50000, 
                             Event_Type__c = 'Incident', 
                             //Version_Key__c = 'hgdsjhdAs232',
                             Claim_Type__c = 'Cargo',
                             Event_Date__c = Date.valueof('2014-12-12'),
                             Claim_Reference_Number__c = 'ClaiMRef1234', 
                             //Claims_Currency__c = 'USD',
                             //Voyage_To__c = 'Voyage_To',
                             //Voyage_From__c = 'Voyage_From',
                             Risk_Coverage__c = clientAsset.id,
                             Status = 'Open',
                             Contract_for_Review__c = clientContract.id,
                             Total_100__c = 11245,
                             Claims_handler__c = brokerUser.id,
                             Status_Change_Date__c = datetime.now()
                             );                             
        caseList.add(clientCase_1);
        System.debug('Today Date and time is ' + DateTime.Now());
        
        clientCase_2 = new Case(Accountid = clientAcc.id,
                             ContactId = clientContact.id, 
                             Origin = 'Community',
                             Object__c = obj.id, 
                             Claim_Incurred_USD__c = 50000,
                             Event_details__c = 'Testing ReportedClaimsCtrl',
                             Paid__c = 50000, 
                             Event_Type__c = 'Incident', 
                             //Version_Key__c = 'hgdsjhdAs232',
                             Claim_Type__c = 'Cargo',
                             Event_Date__c = Date.valueof('2014-12-12'),
                             Claim_Reference_Number__c = 'ClaiMRef1234', 
                            // Claims_Currency__c = 'USD',
                             //Voyage_To__c = 'Voyage_To',
                             //Voyage_From__c = 'Voyage_From',
                             Risk_Coverage__c = clientAsset.id,
                             Status = 'Open',
                             Contract_for_Review__c = clientContract.id,
                             Total_100__c = 11214,
                             Claims_handler__c = brokerUser.id,
                             Status_Change_Date__c = datetime.now()
                              );
        caseList.add(clientCase_2);
        insert caseList;
       
        Extranet_Global_Client__c ext_obj = new Extranet_Global_Client__c(Selected_Global_Clients__c = clientAcc.id,Selected_By__c = clientUser.id,Selected_For_Client__c =clientAcc.id ); 
        insert ext_obj; 
        
        
         ***************************Inserting custom settings for seealldata=false*********************
          
          SelectedClientId_AddBook__c addBookCS = new SelectedClientId_AddBook__c();
          addBookCS.Value__c = ClientAcc.id;
          addBookCS.name = BrokerContact.id;
          insert addBookCS;
          
          SelectedClientId_AddBook__c addBookCS1 = new SelectedClientId_AddBook__c();
          addBookCS1.Value__c = ClientAcc.id;
          addBookCS1.name = ClientContact.id;
          insert addBookCS1;
          **************************end*********************
        } */
        
   /* was commented
    public static testmethod void PortfolioReports()
    { 
         TestPortfolio_Address_etcCtrl.cover();  
         System.runAs(brokerUser)
        {
            PageReference vfpage = Page.PortfolioReports;
            ApexPages.CurrentPage().getParameters().put('paramValue',clientAcc.id);
            ApexPages.CurrentPage().getParameters().put('paramClient',clientAcc.id);
            ApexPages.CurrentPage().getParameters().put('paramAgreement','');
            ApexPages.CurrentPage().getParameters().put('paramCoverGroup','');
           // Test.setCurrentPage(vfpage); 
            Test.startTest();
            PortfolioReportsCtrl portfolioReportsObj = new PortfolioReportsCtrl();
            portfolioReportsObj.loggedInContactId = BrokerContact.Id;
            system.debug('!!!!!!!!!!!!clientId-->'+portfolioReportsObj.clientId);
            PortfolioReportsCtrl.isPeopleClaimUser();
            portfolioReportsObj.SelectedAgreementType ='P&I'; 
            //portfolioReportsObj.SelectedClient = '8ce8ad66-a6ec-1834-9e21';
            //portfolioReportsObj.rerenderList();
            portfolioReportsObj.rerenderListCover();
            portfolioReportsObj.SelectedAgreementType ='P&I';
            portfolioReportsObj.getCoverGroupList();
            PortfolioReportsCtrl.ViewDataRemote(portfolioReportsObj.SelectedClient,portfolioReportsObj.SelectedAgreementType,portfolioReportsObj.SelectedCoverGroup);
            PageReference pg =  portfolioReportsObj.getNewReport();
            String strng = portfolioReportsObj.retriveHtmlData();
            //PortfolioReportsCtrl.Data new_obj = new PortfolioReportsCtrl.Data();
            //PortfolioReportsCtrl.getReport(new_obj );
            pageReference pg = portfolioReportsObj.fetchSelectedClients();
            portfolioReportsObj.strSelected = clientAcc.id+';'+clientAcc.id;
            pageReference pg = portfolioReportsObj.fetchSelectedClients();
           // PortfolioReportsCtrl.getAgreementList();
           // brokerContract.Agreement_Type_Code__c = null;
           // update brokerContract;
            //PortfolioReportsCtrl.getAgreementList();
           //PortfolioReportsCtrl.getNewReport();
            
        } 
        System.runAs(clientUser)
        {
            PageReference vfpage = Page.PortfolioReports;
            vfpage.getParameters().put('paramValue',clientAcc.id);
            Test.setCurrentPage(vfpage);
            PortfolioReportsCtrl portfolioReportsObject = new PortfolioReportsCtrl();
            system.debug('!!!!!!!!!!!!clientId1-->'+portfolioReportsObject.clientId);
            portfolioReportsObject.loggedInContactId = clientContact.id;
            portfolioReportsObject.SelectedAgreementType = 'P&I';
            //portfolioReportsObject.SelectedClient = '8ce8ad66-a6ec-1834-9e21';
            portfolioReportsObject.getClientList();
            portfolioReportsObject.SelectedClient = '8ce8ad66-a6ec-1834-9e21';
            //portfolioReportsObject.rerenderList();
            portfolioReportsObject.rerenderListCover();
            portfolioReportsObject.SelectedAgreementType = 'P&I';
            portfolioReportsObject.SelectedClient = '';  
            portfolioReportsObject.getCoverGroupList();
            
            Test.stopTest();            
        }
        
    }
    
   /* public static testmethod void GardExtHome()
    {    
        TestPortfolio_Address_etcCtrl.cover();
         System.runAs(brokerUser)
        {
            GardExtHomeController exhome = new GardExtHomeController();
            exhome.userContact = new Contact (
                                                FirstName='test_f',
                                              LastName='test_l',
                                              MailingCity = 'test_city',
                                              MailingCountry = 'test_country',
                                              MailingPostalCode = 'test_postal_code',
                                              MailingState = 'test_state',
                                              MailingStreet = 'test_street',
                                              AccountId = clientAcc.Id,
                                              Email = 'test_email@gmail.com',
                                              IsPeopleLineUser__c = True,
                                              Phone = '4521478',
                                              MobilePhone = '987451236987'
                                             );
              insert exhome.userContact;                              
            
            exhome.getToDoItems();
            exhome.getRatingItems();
            exhome.getleftImageId();
            exhome.getrightImageId();
            exhome.getuserFeedBack();
            exhome.submitAction();
            exhome.userContact.AccountId = brokerAcc.Id;
            exhome.submitAction();
            exhome.reDirection();
            exhome.getmyCovers();
            exhome.underwrId = 'a1hL0000000kT2HIAU#theHome:pageFrame:feedbackPopUpSection1:j_id186:0:j_id193';
            exhome.underwriterPopup();
            List<Case> caseli = exhome.myClaims;
           // exhome.showCaseDetails();
          //  exhome.getImageUrl('abcde');
            exhome.claimsHandlerId = 'a1hL0000000kSxrIAE#theHome:pageFrame:feedbackPopUpSection1:j_id161:0:cldId';
            exhome.showClaimsHandler();
            exhome.cancelAction();
        } 
        System.runAs(clientUser)
        {
            GardExtHomeController exhome = new GardExtHomeController();
            
            exhome.userContact = new Contact (
                                              FirstName='test_fn',
                                              LastName='test_ln',
                                              MailingCity = 'test_cityk',
                                              MailingCountry = 'test_countryk',
                                              MailingPostalCode = 'test_postal_codek',
                                              MailingState = 'test_statek',
                                              MailingStreet = 'test_streetk',
                                              AccountId = clientAcc.Id,
                                              Email = 'test_emailk@gmail.com',
                                              IsPeopleLineUser__c = True,
                                              Phone = '452147854',
                                              MobilePhone = '9872451236987'
                                             );
              insert exhome.userContact;
            
            exhome.getToDoItems();
            exhome.getRatingItems();
            exhome.getleftImageId();
            exhome.getrightImageId();
            exhome.getuserFeedBack();
            exhome.selectedToDo = 'test_do';
            exhome.selectedRating = 'test_rating';
            exhome.userFeedBack.Comments__c ='test_comments';
            exhome.userFeedBack.What_was_your_main_reason_for_visiting__c = 'test_reason';
            exhome.userFeedBack.Contact__c = clientContact.id;
            exhome.submitAction();
            exhome.reDirection();
            exhome.getmyCovers();
            exhome.underwriterPopup();
            List<Case> caseli = exhome.myClaims;
           // exhome.showCaseDetails();
          //  exhome.getImageUrl('abcde');
            exhome.showClaimsHandler();
            exhome.cancelAction();
            exhome.strParams = 'Audun F. Pettersen##Loss of Hire##Marine##001D000000fxSZ4IAM##2015-02-27 00:00:00##2014##Y##75.00##001D000001LA9UoIAL##true';
            exhome.viewObject();
            exhome.strParams ='null##null##null##null##null##null##null##null##null##null';
            exhome.viewObject();
        }
    } was commented
    */
   /* public static testmethod void Gardcontacts()
    {
        TestPortfolio_Address_etcCtrl.cover();
        system.runAs(brokerUser)
        {
            GardContactsController GrdContactsCntl = new GardContactsController();
            GrdContactsCntl.selectedAccountId=null;
            GrdContactsCntl.getClients();
            GrdContactsCntl.selectedAccountId = brokerAcc.id;
            GrdContactsCntl.showAllContact();
            boolean var1 = true;
             GrdContactsCntl.hasPrevious = var1;
            boolean var2 = true;
            GrdContactsCntl.hasNext = var2;
            Integer var3 = 100;
            GrdContactsCntl.pageNumber = var3;
            GrdContactsCntl.previous();
            GrdContactsCntl.next();
            GrdContactsCntl.first();
            GrdContactsCntl.Last();
            GrdContactsCntl.showPopup();
            GrdContactsCntl.closePopup();
            GrdContactsCntl.descClient();
            GrdContactsCntl.ascClient();
            GrdContactsCntl.printData();
            GrdContactsCntl.pageNum=1;
            GrdContactsCntl.setpageNumber();
        }
        
        system.runAs(clientUser)
        {
            GardContactsController GrdContactsCntl_1 = new GardContactsController();
            GrdContactsCntl_1.selectedAccountId=null;
            GrdContactsCntl_1.getClients();
            GrdContactsCntl_1.selectedAccountId = clientAcc.id;
            GrdContactsCntl_1 .showAllContact();
            boolean var1 = true;
             GrdContactsCntl_1.hasPrevious = var1;
            boolean var2 = true;
            GrdContactsCntl_1.hasNext = var2;
            Integer var3 = 100;
            GrdContactsCntl_1.pageNumber = var3;
            //GrdContactsCntl.previous();
            //GrdContactsCntl.next();
            GrdContactsCntl_1.showPopup();
            GrdContactsCntl_1.closePopup();
            GrdContactsCntl_1.descClient();
            GrdContactsCntl_1.ascClient();
            GrdContactsCntl_1.printData();
            GrdContactsCntl_1.pageNum=1;
            GrdContactsCntl_1.selectedGlobalClient = new list<string>();
            GrdContactsCntl_1.selectedGlobalClient.add(clientAcc.id);
            //GrdContactsCntl_1.strselected = clientAcc.id;
            //pageReference pg = GrdContactsCntl_1.fetchSelectedClients();
            GrdContactsCntl_1.fetchSelectedClients();
            GrdContactsCntl_1.strselected = clientAcc.id+';'+clientAcc.id;
           // pageReference pg = GrdContactsCntl_1.fetchSelectedClients();
           // GrdContactsCntl.setpageNumber();
        }
    }
    public static testmethod void AddressBook()
    {
        TestPortfolio_Address_etcCtrl.cover();
         System.runAs(brokerUser)
         {
             AddressBookController addressbook = new AddressBookController();
             addressbook.contactsSearch();
         }
         System.runAs(clientUser)
         {
             AddressBookController addressbook = new AddressBookController();
             addressbook.contactsSearch();
         }
    }
    Public static testmethod void login()
    {
        TestPortfolio_Address_etcCtrl.cover();
        System.runAs(brokerUser)
         {
             GardLoginCtrl loginctrl= new GardLoginCtrl();
             loginctrl.isRemember = true;   
             loginctrl.userName= 'test@xyz.com';
             loginctrl.password= '123456789';
             loginctrl.login();
             loginctrl.forgotPassword();
             loginctrl.forwardToCustomAuthPage();
             loginctrl.showMessage();
         }
    }
    Public static testmethod void forgotpassword()
    {
        TestPortfolio_Address_etcCtrl.cover();
         System.runAs(brokerUser)
         {
             GardForgotPasswordController forgot = new GardForgotPasswordController();
             forgot.username_1 = 'testbrokerUser@testorg.com';
             forgot.Submit();
         }
    }*/
 
}