@IsTest
private class MDMRoleProxyTests {

    private static List<Valid_Role_Combination__c> validRoles;
    
    private static void setupValidRoles() {
        ////For some reason this seems to be throwing an internal Salesforce error /
        ////System.UnexpectedException: Salesforce System Error: 1583762872-13077 (1716107030) (1716107030)
        //validRoles = (List<Valid_Role_Combination__c>)Test.loadData(Valid_Role_Combination__c.sObjectType, 'MDMValidRoles');
        ////END...
        validRoles = new List<Valid_Role_Combination__c>();
        validRoles.add(new Valid_Role_Combination__c(Role__c = 'Other', Sub_Role__c = 'Other Sub Role'));
        insert validRoles;
    }

    private static testmethod void UpsertRoleAsync_Success() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        setupValidRoles();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        MDM_Role__c m = new MDM_Role__c( Account__c = a.id,  Valid_Role_Combination__c = validRoles[0].id);
        insert m;
        id id = m.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, true);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMRoleProxy.MDMUpsertRoleAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        m = [SELECT id, Synchronisation_Status__c FROM MDM_Role__c WHERE id = :id];
        //System.assertEquals('Synchronised', m.Synchronisation_Status__c, 'Role Sync Status = Synchronised');
        
    }
    
    private static testmethod void UpsertRoleAsync_Failure() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        setupValidRoles();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        MDM_Role__c m = new MDM_Role__c( Account__c = a.id, Valid_Role_Combination__c = validRoles[0].id);
        insert m;
        id id = m.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, false);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMRoleProxy.MDMUpsertRoleAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        m = [SELECT id, Synchronisation_Status__c FROM MDM_Role__c WHERE id = :id];
        //System.assertEquals('Sync Failed', m.Synchronisation_Status__c, 'Role Sync Status = Sync Failed');
        
    }
    
    private static testmethod void UpsertRoleAsync_NullResponse() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        setupValidRoles();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        MDM_Role__c m = new MDM_Role__c( Account__c = a.id, Valid_Role_Combination__c = validRoles[0].id);
        insert m;
        id id = m.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = null;
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMRoleProxy.MDMUpsertRoleAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        m = [SELECT id, Synchronisation_Status__c FROM MDM_Role__c WHERE id = :id];
        //System.assertEquals('Sync Failed', m.Synchronisation_Status__c, 'Role Sync Status = Sync Failed');
        
    }

    private static testmethod void DeletetRoleAsync_Success() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        setupValidRoles();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        MDM_Role__c m = new MDM_Role__c( Account__c = a.id,  Valid_Role_Combination__c = validRoles[0].id);
        insert m;
        id id = m.id;
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, true);
                
        ///Act...
        Test.StartTest();
        // manually call the web service);
        MDMRoleProxy.MDMDeleteRoleAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        m = [SELECT id, Synchronisation_Status__c FROM MDM_Role__c WHERE id = :id];
        //System.assertEquals('Synchronised', m.Synchronisation_Status__c, 'ROLE Sync Status = Synchronised');
    }

    private static testmethod void DeleteRoleAsync_Failure() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        setupValidRoles();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        MDM_Role__c m = new MDM_Role__c( Account__c = a.id,  Valid_Role_Combination__c = validRoles[0].id);
        insert m;
        id id = m.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, false);
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMRoleProxy.MDMDeleteRoleAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        m = [SELECT id, Synchronisation_Status__c FROM MDM_Role__c WHERE id = :id];
        //System.assertEquals('Sync Failed', m.Synchronisation_Status__c, 'Account Sync Status = Sync Failed');
        
    }
    
    private static testmethod void DeleteRoleAsync_NullResponse() {
        ///Arrange...
        // ensure web services \ triggers are turned off (custom setting)
        // insert an account
        // set the web service mock (using the account id)
        MDMProxyTests.setupMDMConfigSettings();
        setupValidRoles();
        Account a = new Account (Name = 'Unit Test Account');
        insert a;
        MDM_Role__c m = new MDM_Role__c( Account__c = a.id,  Valid_Role_Combination__c = validRoles[0].id);
        insert m;
        id id = m.id;
        
        WebServiceUtilTests.Instance.MockWebServiceResponse = null;
                
        ///Act...
        Test.StartTest();
        // manually call the web service
        MDMRoleProxy.MDMDeleteRoleAsync(new List<id> { id });
        Test.StopTest();
        
        ///Assert...
        // ensure the sync status has been updated on the account
        m = [SELECT id, Synchronisation_Status__c FROM MDM_Role__c WHERE id = :id];
        //System.assertEquals('Sync Failed', m.Synchronisation_Status__c, 'Account Sync Status = Sync Failed');
        
    }

}