/**
 * Created by ohuuse on 22/10/2019.
 */

public with sharing class listBuilderModalController {

    // Input: Id from Case
    // Return: jsonstring with covers (Assets) marked with checked = true
    // Cover_and_Objects_for_case__c relation
    @AuraEnabled //(Cacheable = True)
    public static String getRelatedRelevantCovers(Id caseId) {
        // list that will be serialized and returned to the lwc
        List<jsonOut> jsonOuts = new List<jsonOut>();
        // loops through the map and adds them to new instances of the class jsonOut
        for (Cover_and_Objects_for_case__c jc : [SELECT Id, Name, Cover__c, Cover__r.Agreement__r.Agreement_Reference__c, Cover__r.Agreement__c, Cover__r.Agreement__r.Policy_Year__c, Cover__r.Account.Name, Cover__r.Name, Cover__r.Object__r.Name, Cover__r.On_risk_indicator__c, Cover__r.AccountId FROM Cover_and_Objects_for_case__c WHERE Case__c = :caseId AND Cover__c != null]) {
            jsonOut js = new jsonOut();
            js.recordId = jc.Cover__c;
            js.checked = True;
            js.agreementReference = jc.Cover__r.Agreement__r.Agreement_Reference__c;
            js.companyName = jc.Cover__r.Account.Name;
            js.coverName = jc.Cover__r.Name;
            js.objectName = jc.Cover__r.Object__r.Name;
            js.policyYear = jc.Cover__r.Agreement__r.Policy_Year__c;
            js.riskIndicator = jc.Cover__r.On_risk_indicator__c;
            js.accountId = jc.Cover__r.AccountId;
            jsonOuts.add(js);
        }
        return JSON.serialize(jsonOuts);
    }

    // Will SOQL account name based on a input string and return a json string
    @AuraEnabled (Cacheable = True)
    public static String getRelevantAccounts(String companyString) {
        // make the query open for symbols before and after the input string
        String searchString = '%' + companyString + '%';
        String searchStringCId = companyString + '%';

        // list that will be serialized and returned to the lwc
        List<jsonAccountOut> jsonAccountOuts = new List<jsonAccountOut>();
        // loops through the 10 first results from the soql query and adds
        // them to new instances of the class jsonAccountOut
        for (Account acc : [SELECT Id, Name, BillingCity, Company_ID__c, Company_Role__c FROM Account WHERE (Name LIKE :searchString OR Company_ID__c LIKE :searchStringCId) AND Active__c = true AND Company_Role__c IN ('broker','client') ORDER BY Name,Company_ID__c LIMIT 10]){
            jsonAccountOut jsacc = new jsonAccountOut();
            jsacc.recordId = acc.Id;
            jsacc.name = acc.Name;
            jsacc.billingCity = acc.BillingCity;
            jsacc.companyId = acc.Company_ID__c;
            jsacc.role = acc.Company_Role__c;
            jsonAccountOuts.add(jsacc);
        }

        if (jsonAccountOuts.size() > 0) {
            return JSON.serialize(jsonAccountOuts);

        } else {
            return '';
        }

    }

    // This method will take in the caseId and a json string with related covers that are to be junctioned to the the
    // case, or the junction is to be deleted
    @AuraEnabled
    public static void deleteCreateJunctionCovers(Id caseId, string jsonString) {
        System.debug(jsonString);
        // deserialize the jsonString to the class jsonsIn
        List<jsonIn> coversToModify = (List<jsonIn>) JSON.deserialize(jsonString, List<jsonIn>.class);

        // lists used to separate between deleting and deleting new junction records (Cover_and_Objects_for_case__c)
        List<Id> coverJunctionsToCreate = new List<Id>();
        List<Id> coverJunctionsToDelete = new List<Id>();

        // lists used to gather records for dml action at the end
        List<Cover_and_Objects_for_case__c> deleteCoverJunctionsList = new List<Cover_and_Objects_for_case__c>();
        List<Cover_and_Objects_for_case__c> newCoverJunctionsList = new List<Cover_and_Objects_for_case__c>();

        // sorts Cover_and_Objects_for_case__c into to lists based on their "dml tag"
        for (jsonIn coverToModify : coversToModify) {
            if (coverToModify.dml == 'delete') {
                coverJunctionsToDelete.add(coverToModify.recordId);
            } else if (coverToModify.dml == 'create') {
                coverJunctionsToCreate.add(coverToModify.recordId);
            }


        }

        // finds Cover_and_Objects_for_case__c to delete
        if (coverJunctionsToDelete.size() > 0) {
            deleteCoverJunctionsList.addAll([SELECT Id FROM Cover_and_Objects_for_case__c WHERE Cover__c IN :coverJunctionsToDelete AND Case__c = :caseId]);
        }

        // create (not commit) new Cover_and_Objects_for_case__c
        if (coverJunctionsToCreate.size() > 0) {
            for (Id coverId : coverJunctionsToCreate) {
                Cover_and_Objects_for_case__c newCoverJunction = new Cover_and_Objects_for_case__c(
                        Case__c = caseId,
                        Cover__c = coverId
                );
                newCoverJunctionsList.add(newCoverJunction);
            }
        }

        // delete old Cover_and_Objects_for_case__c
        if (deleteCoverJunctionsList.size() > 0) {
            try {
                System.debug('in delete try');
                delete deleteCoverJunctionsList;

            } catch (Exception e) {
                System.debug(e.getMessage());
                throw new AuraHandledException('Something went wrong deleting the old coverJunctions: '
                        + e.getMessage());
            }
        }

        // insert new Cover_and_Objects_for_case__c
        if (newCoverJunctionsList.size() > 0) {
            try {
                System.debug('in insert try');
                insert newCoverJunctionsList;

            } catch (Exception e) {
                System.debug(e.getMessage());
                throw new AuraHandledException('Something went wrong creating the new coverJunctions: '
                        + e.getMessage());
            }
        }
    }

    // used to send information to the lwc listBuilderModal
    class jsonOut {
        Id recordId;
        Boolean checked;
        String agreementReference;
        String companyName;
        String coverName;
        String objectName;
        String policyYear;
        Boolean riskIndicator;
        String accountId;
    }

    // used to retriev information from the lwc listBuilderModal
    public class jsonIn {
        public Id recordId;
        public String dml;

    }

    // used to send account information to the lwc listBuilderModal
    class jsonAccountOut {
        Id recordId;
        String name;
        String billingCity;
        String companyId;
        String role;
    }

    // Dynamic soql that supplies additional covers. a lwc can send in filters in order to reduce the stress on the front end tabel functionality
    @AuraEnabled
    public static String getMoreRelevantCovers(List<Id> accountIds, Integer lim, List<Id> notWantedCovers, Map<String, Object> filterValueMap, Map<String, Object> sortMap) {
        // filters
        Map<String, String> filterFieldMap = new Map<String, String>{
                'policyYear' => 'Agreement__r.Policy_Year__c',
                'agreementReference' => 'Agreement__r.Agreement_Reference__c',
                'riskIndicator' => 'On_risk_indicator__c',
                'coverName' => 'Name',
                'objectName' => 'Object__r.Name',
                'companyName' => 'Account.Name'
        };
        System.debug('in here');
        System.debug(filterValueMap);

        String filterString = 'SELECT Id, Agreement__r.Agreement_Reference__c, Agreement__c, Agreement__r.Policy_Year__c, Account.Name, Name, Object__r.Name, On_risk_indicator__c, AccountId FROM Asset WHERE AccountId IN :accountIds AND Id NOT IN :notWantedCovers ';

        if (filterValueMap.get('policyYear') != null) {
            filterString += ' AND ' + filterFieldMap.get('policyYear') + '=' + '\''+ String.valueOf(filterValueMap.get('policyYear')) +'\'';
        }

        if (filterValueMap.get('agreementReference') != null) {
            filterString += ' AND ' + filterFieldMap.get('agreementReference') + ' LIKE \'%' + filterValueMap.get('agreementReference') + '%\'';
        }

        if (filterValueMap.get('coverName') != null) {
            filterString += ' AND ' + filterFieldMap.get('coverName') + ' LIKE \'%' + filterValueMap.get('coverName') + '%\'';
        }

        if (filterValueMap.get('objectName') != null) {
            filterString += ' AND ' + filterFieldMap.get('objectName') + ' LIKE \'%' + filterValueMap.get('objectName') + '%\'';
        }

        filterString += ' AND ' + filterFieldMap.get('riskIndicator') + ' = ' + filterValueMap.get('riskIndicator');

        if (sortMap.get('by') != null) {
            filterString += ' ORDER BY ' + filterFieldMap.get(String.valueOf(sortMap.get('by'))) + ' ' + sortMap.get('direction') + ' LIMIT :lim';
        } else {
            filterString += ' ORDER BY Name DESC, Object__r.Name ASC LIMIT :lim';
        }
        System.debug(filterString);

        List<jsonOut> jsonOuts = new List<jsonOut>();

        List<Asset> additionalCovers = Database.query(filterString);
        for (Asset jc : additionalCovers) {
            jsonOut js = new jsonOut();
            js.recordId = jc.Id;
            js.checked = False;
            js.agreementReference = jc.Agreement__r.Agreement_Reference__c;
            js.companyName = jc.Account.Name;
            js.coverName = jc.Name;
            js.objectName = jc.Object__r.Name;
            js.policyYear = jc.Agreement__r.Policy_Year__c;
            js.riskIndicator = jc.On_risk_indicator__c;
            js.accountId = jc.AccountId;
            jsonOuts.add(js);
        }
        System.debug(jsonOuts.size());
        return JSON.serialize(jsonOuts);
    }


}