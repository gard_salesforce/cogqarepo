@isTest
private class TestContactHandler {
    ///////*******Variables for removing common literals********************//////////////
    public static string testStr = 'Test' ; 
    public static string lNameStr =  'Contact ';
    public static string normalStr =  'Normal';
    public static string typeClStr =  'client';
    /////**********************end**************************/////
    //Method to create custom settings value
    public static void createCustomSettingsValue(){
        List<ContactSubscriptionFields__c> conFieldsList = new List<ContactSubscriptionFields__c>();
        ContactSubscriptionFields__c conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        ContactSubscriptionFields__c conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        ContactSubscriptionFields__c conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList;
    }
    static testMethod void testUpdateContactsWithAdmin(){
        Test.startTest();
        createCustomSettingsValue();
        //build a list of record type ids
        List<Id> RecIds = new List<Id>();
        for(RecordType aRecType:ContactToolkit.ContactRecordTypes.values()){
            RecIds.add(aRecType.Id);
        }
        //Create contact
        Contact aContact = new Contact(  FirstName=testStr ,
                                       LastName =  lNameStr ,
                                       RecordTypeId = RecIds[0],                                       
                                       //If the record type is normal use the client type otherwise 
                                       //use the record type name which we expect will be Correspondent
                                       Type__c = ContactToolkit.ContactRecordTypes.get(RecIds[0]).Name ==  normalStr  ?  typeClStr  : ContactToolkit.ContactRecordTypes.get(RecIds[0]).Name);
        insert aContact;
        Contact fetchedContact = [SELECT ID FROM CONTACT WHERE FirstName =: testStr ];
        List<Id> allContactIds = new List<Id>();
        allContactIds.add(fetchedContact.Id);
        ContactHandler.updateContactsFuture(allContactIds);
        ContactHandler.updateContacts(allContactIds);
        Test.stopTest();             
        
        fetchedContact = [SELECT ID, FirstName FROM CONTACT WHERE FirstName =: testStr ];
        System.assertEquals(testStr ,fetchedContact.FirstName);          
    }
    
    static testMethod void testUpdateContactsWithNormal(){
        Test.startTest();
        createCustomSettingsValue();
        //build a list of record type ids
        List<Id> RecIds = new List<Id>();
        for(RecordType aRecType:ContactToolkit.ContactRecordTypes.values()){
            RecIds.add(aRecType.Id);
        }
        //Create contact
        Contact aContact = new Contact(  FirstName=testStr ,
                                       LastName =  lNameStr ,
                                       RecordTypeId = RecIds[0],                                       
                                       //If the record type is normal use the client type otherwise 
                                       //use the record type name which we expect will be Correspondent
                                       Type__c = ContactToolkit.ContactRecordTypes.get(RecIds[0]).Name ==  normalStr  ?  typeClStr  : ContactToolkit.ContactRecordTypes.get(RecIds[0]).Name);
        insert aContact;
        Contact fetchedContact = [SELECT ID FROM CONTACT WHERE FirstName=: testStr ];
        List<Id> allContactIds = new List<Id>();
        allContactIds.add(fetchedContact.Id);
        ContactHandler.updateContactsFuture(allContactIds);
        ContactHandler.updateContacts(allContactIds);
        Test.stopTest();             
        
        fetchedContact = [SELECT ID, MyGard_user_type__c FROM CONTACT WHERE FirstName=: testStr ];
        System.assertEquals(fetchedContact.MyGard_user_type__c , null);          
    }
    
    static testMethod void testUpdateContactRecordForNotifyAdminWithTrue(){
        Test.startTest();
        createCustomSettingsValue();
        //build a list of record type ids
        List<Id> RecIds = new List<Id>();
        for(RecordType aRecType:ContactToolkit.ContactRecordTypes.values()){
            RecIds.add(aRecType.Id);
        }
        //Create contact
        Contact aContact = new Contact(  FirstName=testStr ,
                                       LastName =  lNameStr ,
                                       RecordTypeId = RecIds[0],
                                       //If the record type is normal use the client type otherwise 
                                       //use the record type name which we expect will be Correspondent
                                       Type__c = ContactToolkit.ContactRecordTypes.get(RecIds[0]).Name ==  normalStr  ?  typeClStr  : ContactToolkit.ContactRecordTypes.get(RecIds[0]).Name);
        insert aContact;
        Contact fetchedContact = [SELECT ID FROM CONTACT WHERE FirstName=: testStr ];
        List<Id> allContactIds = new List<Id>();
        allContactIds.add(fetchedContact.Id);
        ContactHandler.updateContactRecordForNotifyAdminFuture(true,allContactIds);
        ContactHandler.updateContactRecordForNotifyAdmin(true,allContactIds);
        Test.stopTest();             
        
        fetchedContact = [SELECT ID, Notify_Admin__c FROM CONTACT WHERE FirstName=: testStr ];
        System.assertEquals(true,fetchedContact.Notify_Admin__c);          
    }
    
    static testMethod void testUpdateContactRecordForNotifyAdminWithFalse(){
        Test.startTest();
        createCustomSettingsValue();
        //build a list of record type ids
        List<Id> RecIds = new List<Id>();
        for(RecordType aRecType:ContactToolkit.ContactRecordTypes.values()){
            RecIds.add(aRecType.Id);
        }
        //Create contact
        Contact aContact = new Contact(  FirstName=testStr ,
                                       LastName =  lNameStr ,
                                       RecordTypeId = RecIds[0],
                                       //If the record type is normal use the client type otherwise 
                                       //use the record type name which we expect will be Correspondent
                                       Type__c = ContactToolkit.ContactRecordTypes.get(RecIds[0]).Name ==  normalStr  ?  typeClStr  : ContactToolkit.ContactRecordTypes.get(RecIds[0]).Name);
        insert aContact;
        Contact fetchedContact = [SELECT ID FROM CONTACT WHERE FirstName=: testStr ];
        List<Id> allContactIds = new List<Id>();
        allContactIds.add(fetchedContact.Id);
        ContactHandler.updateContactRecordForNotifyAdminFuture(false,allContactIds);
        ContactHandler.updateContactRecordForNotifyAdmin(false,allContactIds);
        Test.stopTest();             
        
        fetchedContact = [SELECT ID, Notify_Admin__c FROM CONTACT WHERE FirstName=: testStr ];
        System.assertEquals(false,fetchedContact.Notify_Admin__c);          
    }
    
    static testMethod void testUpdateContactsForInactiveUser(){
        Test.startTest();
        createCustomSettingsValue();
        //build a list of record type ids
        List<Id> RecIds = new List<Id>();
        for(RecordType aRecType:ContactToolkit.ContactRecordTypes.values()){
            RecIds.add(aRecType.Id);
        }
        //Create contact
        Contact aContact = new Contact(  FirstName=testStr ,
                                       LastName =  lNameStr ,
                                       RecordTypeId = RecIds[0],
                                       //If the record type is normal use the client type otherwise 
                                       //use the record type name which we expect will be Correspondent
                                       Type__c = ContactToolkit.ContactRecordTypes.get(RecIds[0]).Name ==  normalStr  ?  typeClStr  : ContactToolkit.ContactRecordTypes.get(RecIds[0]).Name);
        insert aContact;
        Contact fetchedContact = [SELECT ID FROM CONTACT WHERE FirstName=: testStr ];
        List<Id> allContactIds = new List<Id>();
        allContactIds.add(fetchedContact.Id);
        ContactHandler.updateContactsForInactiveUserFuture(allContactIds);
        ContactHandler.updateContactsForInactiveUser(allContactIds);
        Test.stopTest();             
        
        fetchedContact = [SELECT ID, MyGard_user_type__c FROM CONTACT WHERE FirstName=: testStr ];
        Boolean isNull = (fetchedContact.MyGard_user_type__c != null) ? false : true;
        System.assert(true,isNull);          
    }
}