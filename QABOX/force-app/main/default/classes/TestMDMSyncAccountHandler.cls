@isTest
private class TestMDMSyncAccountHandler {
    public static Account clientAcc;
    public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Market_Area__c Markt;
    public static User salesforceLicUser;
    
    private static void createUsers(){
        salesforceLicUser = new User(
                                Alias = 'standt', 
                                profileId = salesforceLicenseId ,
                                Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8',
                                CommunityNickname = 'test13',
                                LastName='Testing',
                                LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US',  
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName='test008@testorg.com'
                                     );
        
        insert salesforceLicUser; 
    }
    private static void createAccount(){
        Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt; 
        createUsers();        
        
        clientAcc = new Account( Name = 'Test_1', 
                                Site = '_www.test_1.se', 
                                Type = 'Client', 
                                ShippingStreet = 'Test Shipping Street',
                                BillingStreet = 'Gatan 1',
                                BillingCity = 'Stockholm', 
                                BillingCountry = 'SWE', 
                                BillingPostalCode = 'BS1 1AD',
                                recordTypeId=System.Label.Client_Contact_Record_Type,
                                Company_Role_Text__c='Client',
                                Market_Area__c = Markt.id,
                                Area_Manager__c = salesforceLicUser.id,
                                OwnerId = salesforceLicUser.id
                               );
        insert clientAcc;
    }
    
    static testMethod void testCheckMDMFieldsChanged(){
        Test.startTest();
        TestMDMSyncAccountHandler.createAccount();
        Account newAccount = clientAcc.clone(true, true, true, true);
        //insert newAccount;
        Map<Id,Account> oldAccounts = new Map<Id, Account>();
        oldAccounts.put(clientAcc.Id, clientAcc);
        List<Account> accounts = new List<Account>();
        accounts.add(newAccount);
        MDMSyncAccountHandler.CheckMDMFieldsChanged(accounts,oldAccounts);
        Test.stopTest();
    }
    
    static testMethod void testInsertAccountSyncStatusWebServiceEnabled(){
        Test.startTest();
        TestMDMSyncAccountHandler.createAccount();
        List<Account> accounts = new List<Account>();
        accounts.add(clientAcc);
        MDMSyncAccountHandler.InsertAccountSyncStatus(accounts,true);
        Test.stopTest();
    }
    
    static testMethod void testInsertAccountSyncStatusWebServiceDisabled(){
        Test.startTest();
        TestMDMSyncAccountHandler.createAccount();
        List<Account> accounts = new List<Account>();
        accounts.add(clientAcc);
        MDMSyncAccountHandler.InsertAccountSyncStatus(accounts,false);
        Test.stopTest();
    }
    
    static testMethod void testInsertAddressesSyncStatusWebServiceEnabled(){
        Test.startTest();
        TestMDMSyncAccountHandler.createAccount();
        List<Account> accounts = new List<Account>();
        accounts.add(clientAcc);
        MDMSyncAccountHandler.InsertAddressesSyncStatus(accounts,true);
        Test.stopTest();
    }
    
    static testMethod void testInsertAddressesSyncStatusWebServiceDisabled(){
        Test.startTest();
        TestMDMSyncAccountHandler.createAccount();
        List<Account> accounts = new List<Account>();
        accounts.add(clientAcc);
        MDMSyncAccountHandler.InsertAddressesSyncStatus(accounts,false);
        Test.stopTest();
    }
    
    static testMethod void testResyncCompaniesShippingAddress(){
        TestMDMSyncAccountHandler.createAccount();
        Account acc = [SELECT ID,Shipping_Address_Sync_Status__c FROM ACCOUNT WHERE ID =: clientAcc.ID];
        acc.Shipping_Address_Sync_Status__c = 'Sync Failed';
        update acc;
        
        Test.startTest();
        List<Account> accList = new List<Account>();
        accList.add(acc);
        MDMSyncAccountHandler.ResyncExistingAccounts(accList);
        Test.stopTest();
    }
    
    static testMethod void testResyncCompaniesBillingAddress(){
        TestMDMSyncAccountHandler.createAccount();
        Account acc = [SELECT ID,Billing_Address_Sync_Status__c FROM ACCOUNT WHERE ID =: clientAcc.ID];
        acc.Billing_Address_Sync_Status__c = 'Sync Failed';
        update acc;
        
        Test.startTest();
        List<Account> accList = new List<Account>();
        accList.add(acc);
        MDMSyncAccountHandler.ResyncExistingAccounts(accList);
        Test.stopTest();
    }
    
    static testMethod void testResyncCompaniesSync(){
        TestMDMSyncAccountHandler.createAccount();
        Account acc = [SELECT ID,Synchronisation_Status__c FROM ACCOUNT WHERE ID =: clientAcc.ID];
        acc.Synchronisation_Status__c = 'Sync Failed';
        update acc;
        
        Test.startTest();
        List<Account> accList = new List<Account>();
        accList.add(acc);
        System.assertEquals('Sync Failed',acc.Synchronisation_Status__c);
        MDMSyncAccountHandler.ResyncExistingAccounts(accList);
        Test.stopTest();
    }
}