/**************************************************************************
* Author  : Phil Spenceley
* Company : cDecisions
* Date    : 09/10/2013
***************************************************************************/
    
/// <summary>
///  This proxy class provides methods to access the MDM Partner Service
/// </summary>
public without sharing class MDMPartnerProxy extends MDMProxy {

	private Set<String> getAccountFields(String requestName) {
		Set<String> fields = new Set<String>();
		for (MDM_Service_Fields__c field : MDM_Service_Fields__c.getAll().values()) {
			if (requestName.equalsIgnoreCase(field.Request_Name__c) && 'Account'.equalsIgnoreCase(field.Source_Object__c) && 'Field'.equalsIgnoreCase(field.Type__c)) {
				fields.add(field.Field_Name__c.toLowerCase());
			}
		}
		return fields;
	}
	private Account getAccountById(id accountId, String requestName) {
		return (Account)Database.query(buildSoqlRequest(getAccountFields(requestName), 'Account', accountId));
    }
    private List<Account> getAccountsByIds(List<id> accountIds, String requestName) {
        return (List<Account>)Database.query(buildSoqlRequest(getAccountFields(requestName), 'Account', accountIds));
    }
	
	private MDMUpsertPartnerResponse MDMUpsertPartners(MDMUpsertPartnerRequest request) {
        DOM.Document responseDoc = sendRequest(request);
		MDMUpsertPartnerResponse response = new MDMUpsertPartnerResponse();
		try {
			response.Deserialize(
	            responseDoc
	                .getRootElement()
	                .getChildElement(SOAP_Body, SOAPNS)
	                .getChildElement(MDMChangeResponse_Label, MDMChangeResponse_Namespace)
	        );
		} catch (Exception ex) {
			throw new MDMDeserializationException(responseDoc, 'Failed to deserialize MDMPartnerProxy.MDMUpsertPartnerResponse', ex);
		}
		return response;
    }
	private MDMDeletePartnerResponse MDMDeletePartners(MDMDeletePartnerRequest request) {
        DOM.Document responseDoc = sendRequest(request);
		MDMDeletePartnerResponse response = new MDMDeletePartnerResponse();
		try {
			response.Deserialize(
	            responseDoc
	                .getRootElement()
	                .getChildElement(SOAP_Body, SOAPNS)
	                .getChildElement(MDMChangeResponse_Label, MDMChangeResponse_Namespace)
	        );
		} catch (Exception ex) {
			throw new MDMDeserializationException(responseDoc, 'Failed to deserialize MDMPartnerProxy.MDMDeletePartnerResponse', ex);
		}
		return response;
    }
	
	public void MDMUpsertPartner(Map<id, Account> upsertAccounts) {
		MDMUpsertPartnerRequest request = new MDMUpsertPartnerRequest();
		Map<id, Account> accounts = new Map<id, Account>(getAccountsByIds(new List<id> (upsertAccounts.keySet()), request.getName()));
		
        try {
			request.accounts = accounts.values();
            MDMUpsertPartnerResponse response = MDMUpsertPartners(request);
			for (MDMResponse mdmResponse : response.MDMResponses) {
				System.debug('*** MDMResponse:[ id: + ' + mdmResponse.id + ', success: ' + string.valueOf(mdmResponse.success) + ']');
				if (mdmResponse.Success) {
	                accounts.get(mdmResponse.Id).Synchronisation_Status__c = 'Synchronised';
	            } else {
	                accounts.get(mdmResponse.Id).Synchronisation_Status__c = 'Sync Failed';
	            }	
			}
			
			update accounts.values(); 
			
        } catch (MDMDeserializationException ex) {
			Logger.LogException('MDMPartnerProxy.MDMUpsertPartnerAsync (@Future, ex)', ex, ex.xmlString);
			if (accounts != null) {
				SetAccountsSyncFailed(accounts.values(), 'MDMPartnerProxy.MDMUpsertPartnerAsync (@Future, innerEx)');
			}
			
        } catch (Exception ex) {
            Logger.LogException('MDMPartnerProxy.MDMUpsertPartnerAsync (@Future, ex)', ex);
            if (accounts != null) {
				SetAccountsSyncFailed(accounts.values(), 'MDMPartnerProxy.MDMUpsertPartnerAsync (@Future, innerEx)');
			}
			
        }
	}
	
	public void MDMDeletePartner(Map<id, Account> deleteAccounts) {
		MDMDeletePartnerRequest request = new MDMDeletePartnerRequest();
        Map<id, Account> accounts = new Map<id, Account>(getAccountsByIds(new List<id> (deleteAccounts.keySet()), request.getName()));
		
        try {
			
			request.accounts = accounts.values();
            MDMDeletePartnerResponse response = MDMDeletePartners(request);
            //do something with the response...
            for (MDMResponse mdmResponse : response.MDMResponses) {
				System.debug('*** MDMResponse:[ id: + ' + mdmResponse.id + ', success: ' + string.valueOf(mdmResponse.success) + ']');
				if (mdmResponse.Success) {
	                accounts.get(mdmResponse.Id).Synchronisation_Status__c = 'Synchronised';
	            } else {
	                accounts.get(mdmResponse.Id).Synchronisation_Status__c = 'Sync Failed';
	            }	
			}
			
			update accounts.values(); 
		} catch (MDMDeserializationException ex) {
			Logger.LogException('MDMPartnerProxy.MDMDeletePartnerAsync (@Future, ex)', ex, ex.xmlString);
			if (accounts != null) {
				SetAccountsSyncFailed(accounts.values(), 'MDMPartnerProxy.MDMDeletePartnerAsync (@Future, innerEx)');
			}
			
        } catch (Exception ex) {
            Logger.LogException('MDMPartnerProxy.MDMDeletePartnerAsync (@Future, ex)', ex);
			if (accounts != null) {
				SetAccountsSyncFailed(accounts.values(), 'MDMPartnerProxy.MDMDeletePartnerAsync (@Future, innerEx)');
			}
			
        }
	}
	
	@future (callout = true)
    public static void MDMUpsertPartnerAsync(list<id> accountIds) {
		MDMPartnerProxy proxy = new MDMPartnerProxy();
		MDMUpsertPartnerRequest request = new MDMUpsertPartnerRequest();
        Map<id, Account> accounts = new Map<id, Account>(proxy.getAccountsByIds(accountIds, request.getName()));
		proxy.MDMUpsertPartner(accounts);
	}
	
	@future (callout = true)
    public static void MDMDeletePartnerAsync(list<id> accountIds) {
		MDMPartnerProxy proxy = new MDMPartnerProxy();
		MDMDeletePartnerRequest request = new MDMDeletePartnerRequest();
        Map<id, Account> accounts = new Map<id, Account>(proxy.getAccountsByIds(accountIds, request.getName()));
		proxy.MDMDeletePartner(accounts);
	}
	
	//Called from an exception to set all sync status to failed
	private static void SetAccountsSyncFailed(List<account> accounts, String source) {
		try {
			if (accounts != null) {
				for (Account a : accounts) {
					a.Synchronisation_Status__c = 'Sync Failed';
				}
				update accounts; 
			}
		} catch (Exception ex) {
			//Simple update, hopefully shouldn't get here.
			Logger.LogException(source, ex);
		}
	}
	
	/*
	//USED FOR Manual testing...
	public static String TestMDMUpsert(List<id> accountIds) {
		MDMPartnerProxy proxy = new MDMPartnerProxy();
		MDMUpsertPartnerRequest request = new MDMUpsertPartnerRequest();
        List<Account> accounts = proxy.getAccountsByIds(accountIds, request.getName());
		
        try {
			
			request.accounts = accounts;
			String output = proxy.buildRequest(request.getSoapBody().getRootElement());
			System.debug(output);
			return output;
		} catch (Exception ex) {
			return '';
		}
	}
	*/
	
/// ----------------------------------------------------------------------------
///  -- Sub classes for the request and response messages called above
/// ----------------------------------------------------------------------------
	public virtual class MDMPartnerRequest extends MDMRequest {
		//TODO: move to custom setting...
		protected String MDMChangePartnersRequest_Label {
			get {
				return 'changePartnersRequest';
			}
		}
		protected String MDMChangePartnersRequest_Namespace {
			get {
				return 'http://www.gard.no/mdm/v1_0/mdmmessage';
			}
		}
		protected String MDMChangePartnersRequest_Prefix {
			get {
				return 'mdm';
			}
		}
		
        public virtual override String getName() { return 'changePartnersRequest'; }
		public List<Account> Accounts { get; set; }
		        
        public virtual override DOM.Document getSoapBody() {
            return getSoapBody(this.getName(), MDMChangePartnersRequest_Namespace, MDMChangePartnersRequest_Prefix);
        }
		
        public DOM.Document getSoapBody(string method, String namespace, String prefix) {
			system.debug('*** method: '+method);
            DOM.Document body = new DOM.Document();
			DOM.XMLNode root = body.createRootElement(MDMChangePartnersRequest_Label, MDMChangePartnersRequest_Namespace, MDMChangePartnersRequest_Prefix);		

			//Get XML element list from Custom Setting
			List<MDM_Service_Fields__c> serviceFields = [
				SELECT Name, Request_Name__c, Sequence__c, Namespace_Prefix__c, Parent_Node__c, Type__c, XmlLabel__c, Source_Object__c, Field_Name__c, Address_Type__c, Omit_Node_If_Null__c, Strip_Special_Characters__c, Strip_Whitespaces__c
				FROM MDM_Service_Fields__c 
				WHERE Request_Name__c = :method
				ORDER BY Sequence__c];
			
            for(Account a : this.Accounts) {
				DOM.XmlNode partnerXml = root.addChildElement(method, namespace, prefix);
				appendSObjectXml(partnerXml, a, serviceFields);
            }
            return body;
        }
    }
    
	public class MDMUpsertPartnerRequest extends MDMPartnerRequest {
		public override String getName() { return 'upsertPartnerRequest'; }
    }
	
	public virtual class MDMChangeResponse { 
		public List<MDMResponse> mdmResponses { get; set; }
		
		public MDMChangeResponse() {
			mdmResponses = new List<MDMResponse>();
		}
		
		protected void Deserialize(Dom.XmlNode root, String parentNode) {
			try {
				for  (Dom.XmlNode node : root.getChildElements()) {
					try {
						XmlSerializer s = new XmlSerializer();
			            object o = s.Deserialize(node, 'MDMPartnerProxy.MDMResponse');
			            mdmResponses.add((MDMResponse)o);
					} catch (Exception ex) {
						//TODO: handle exceptions better...
						//Don't let one failure fail the whole batch...?
					}
				}
			} catch (Exception ex) {
				//TODO: handle exceptions better...
				throw ex;
			}
		}
	}
	
	public class MDMResponse implements XmlSerializable {
		public Boolean Success { get; set; }
		public String Id { get; set; }
		public String Error { get; set; }
		
		public object get(string fieldName) {
	        Map<string, object> thisobjectmap = new Map<string, object> 
	        { 
	            'id' => this.Id, 
	            'success' => this.Success,
				'error' => this.Error
	        };
	        return thisobjectmap.get(fieldname);
	    }
	    
	    public boolean put(string fieldName, object value) {
	        if (fieldName == 'id') {
	            this.Id = String.valueOf(value);
	        } else if (fieldName == 'success') {
	            this.Success = Boolean.valueOf(value);  
			} else if (fieldName == 'error') {
	            this.Error = String.valueOf(value);
	        } else {
	            return false;
	        }
	        return true;
	    }
	    
	    public Set<string> getFields() {
	        Set<string> fields = new Set<string> 
	        { 
	            'success',
				'id',
				'error'
	        };
	        
	        return fields;
	    }
	}
	
	public class MDMUpsertPartnerResponse extends MDMChangeResponse {
		public void Deserialize(Dom.XmlNode root) {
			Deserialize(root, 'MDMPartnerProxy.MDMChangeResponse');
		}
	}
	
	public class MDMDeletePartnerRequest extends MDMPartnerRequest {
		public override String getName() { return 'deletePartnerRequest'; }
    }
	
	public class MDMDeletePartnerResponse extends MDMChangeResponse {
		public void Deserialize(Dom.XmlNode root) {
			Deserialize(root, 'MDMPartnerProxy.MDMChangeResponse');
		}
	}

}