//A very bad test class by Pulkit
@isTest
public class TestUserManagementProcesses{
    private static GardTestData gtd;
    
    private static void createTestData(){
        gtd = new GardTestData();
        gtd.commonRecord();
    }
    
    @isTest private static void testUserUpdate(){
        createTestData();
        
        Test.startTest();
        UserManagementProcesses.letChain = false;
        System.enqueueJob(new UserManagementProcesses(GardTestData.brokerContact.Id,GardTestData.brokerAcc.id));
        Test.stopTest();
    }
	    
    @isTest private static void testAcmUpdate(){
        createTestData();
        
        Test.startTest();
        UserManagementProcesses ump = new UserManagementProcesses(GardTestData.brokerContact.Id,GardTestData.brokerAcc.id,2);
        System.enqueueJob(ump);
        Test.stopTest();
    }
    
}