@IsTest
private class MDMContactProxyTests {

	private static testmethod void UpsertContactAsync_Success() {
		///Arrange...
		// ensure web services \ triggers are turned off (custom setting)
		// insert an Contact
		// set the web service mock (using the Contact id)
		MDMProxyTests.setupMDMConfigSettings();
		Contact a = new Contact (FirstName = 'Test', LastName = 'Contact');
		insert a;
		id id = a.id;
		
		WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, true);
				
		///Act...
		Test.StartTest();
		// manually call the web service
		MDMContactProxy.MDMUpsertContactAsync(new List<id> { id });
		Test.StopTest();
		
		///Assert...
		// ensure the sync status has been updated on the Contact
		a = [SELECT id, Synchronisation_Status__c FROM Contact WHERE id = :id];
		System.assertEquals('Synchronised', a.Synchronisation_Status__c, 'Contact Sync Status = Synchronised');
		
	}
	
	private static testmethod void UpsertContactAsync_Failure() {
		///Arrange...
		// ensure web services \ triggers are turned off (custom setting)
		// insert an Contact
		// set the web service mock (using the Contact id)
		MDMProxyTests.setupMDMConfigSettings();
		Contact a = new Contact (FirstName = 'Test', LastName = 'Contact');
		insert a;
		id id = a.id;
		
		WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, false);
				
		///Act...
		Test.StartTest();
		// manually call the web service
		MDMContactProxy.MDMUpsertContactAsync(new List<id> { id });
		Test.StopTest();
		
		///Assert...
		// ensure the sync status has been updated on the Contact
		a = [SELECT id, Synchronisation_Status__c FROM Contact WHERE id = :id];
		System.assertEquals('Sync Failed', a.Synchronisation_Status__c, 'Contact Sync Status = Sync Failed');
		
	}
	
	private static testmethod void UpsertContactAsync_NullResponse() {
		///Arrange...
		// ensure web services \ triggers are turned off (custom setting)
		// insert an Contact
		// set the web service mock (using the Contact id)
		MDMProxyTests.setupMDMConfigSettings();
		Contact a = new Contact (FirstName = 'Test', LastName = 'Contact');
		insert a;
		id id = a.id;
		
		WebServiceUtilTests.Instance.MockWebServiceResponse = null;
				
		///Act...
		Test.StartTest();
		// manually call the web service
		MDMContactProxy.MDMUpsertContactAsync(new List<id> { id });
		Test.StopTest();
		
		///Assert...
		// ensure the sync status has been updated on the Contact
		a = [SELECT id, Synchronisation_Status__c FROM Contact WHERE id = :id];
		System.assertEquals('Sync Failed', a.Synchronisation_Status__c, 'Contact Sync Status = Sync Failed');
		
	}
	
	private static testmethod void DeletetContactAsync_Success() {
		///Arrange...
		// ensure web services \ triggers are turned off (custom setting)
		// insert an Contact
		// set the web service mock (using the Contact id)
		MDMProxyTests.setupMDMConfigSettings();
		Contact a = new Contact (FirstName = 'Test', LastName = 'Contact');
		insert a;
		id id = a.id;
		
		WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, true);
				
		///Act...
		Test.StartTest();
		// manually call the web service
		MDMContactProxy.MDMDeleteContactAsync(new List<id> { id });
		Test.StopTest();
		
		///Assert...
		// ensure the sync status has been updated on the Contact
		a = [SELECT id, Synchronisation_Status__c FROM Contact WHERE id = :id];
		System.assertEquals('Synchronised', a.Synchronisation_Status__c, 'Contact Sync Status = Synchronised');
		
	}
	
	private static testmethod void DeleteContactAsync_Failure() {
		///Arrange...
		// ensure web services \ triggers are turned off (custom setting)
		// insert an Contact
		// set the web service mock (using the Contact id)
		MDMProxyTests.setupMDMConfigSettings();
		Contact a = new Contact (FirstName = 'Test', LastName = 'Contact');
		insert a;
		id id = a.id;
		
		WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, false);
				
		///Act...
		Test.StartTest();
		// manually call the web service
		MDMContactProxy.MDMDeleteContactAsync(new List<id> { id });
		Test.StopTest();
		
		///Assert...
		// ensure the sync status has been updated on the Contact
		a = [SELECT id, Synchronisation_Status__c FROM Contact WHERE id = :id];
		System.assertEquals('Sync Failed', a.Synchronisation_Status__c, 'Contact Sync Status = Sync Failed');
		
	}
	
	private static testmethod void DeleteContactAsync_NullResponse() {
		///Arrange...
		// ensure web services \ triggers are turned off (custom setting)
		// insert an Contact
		// set the web service mock (using the Contact id)
		MDMProxyTests.setupMDMConfigSettings();
		Contact a = new Contact (FirstName = 'Test', LastName = 'Contact');
		insert a;
		id id = a.id;
		
		WebServiceUtilTests.Instance.MockWebServiceResponse = null;
				
		///Act...
		Test.StartTest();
		// manually call the web service
		MDMContactProxy.MDMDeleteContactAsync(new List<id> { id });
		Test.StopTest();
		
		///Assert...
		// ensure the sync status has been updated on the Contact
		a = [SELECT id, Synchronisation_Status__c FROM Contact WHERE id = :id];
		System.assertEquals('Sync Failed', a.Synchronisation_Status__c, 'Contact Sync Status = Sync Failed');
		
	}

}