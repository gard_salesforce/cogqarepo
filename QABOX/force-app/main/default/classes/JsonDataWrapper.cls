public class JsonDataWrapper{
    public class PemeInvoiceFile{
        Map<String,Object> peme;
        public PemeInvoiceFile(PEME_Invoice__c pemeInvoice,ContentVersion pemeFile){
            this.peme = new Map<String,Object>();
            this.peme.put('refNo',(pemeInvoice.PEME_reference_No__c != null) ? pemeInvoice.PEME_reference_No__c : '');
            String correctedFromDate = (pemeInvoice.Period_Covered_From__c+'').substring(0,(pemeInvoice.Period_Covered_From__c+'').indexOf(' '));
            String correctedToDate = (pemeInvoice.Period_Covered_To__c+'').substring(0,(pemeInvoice.Period_Covered_To__c+'').indexOf(' '));
            this.peme.put('periodFrom',(pemeInvoice.Period_Covered_From__c != null) ? correctedToDate : '');//to correct format Date field's value to Date
            this.peme.put('periodTo',(pemeInvoice.Period_Covered_To__c != null) ? correctedFromDate : '');//to correct format Date field's value to Date
            this.peme.put('invoiceNumber',(pemeInvoice.Clinic_Invoice_Number__c != null) ? pemeInvoice.Clinic_Invoice_Number__c : '');
            this.peme.put('clinicSalesforceId',(pemeInvoice.Clinic__c != null) ? pemeInvoice.Clinic__c : '');
            this.peme.put('clinicCompanyId',(pemeInvoice.clinic__r.company_Id__c != null) ? Integer.valueOf(pemeInvoice.clinic__r.company_Id__c) : 0);
            this.peme.put('totalAmount',(pemeInvoice.Total_Amount__c != null) ? pemeInvoice.Total_Amount__c : 0);
            this.peme.put('currency',(pemeInvoice.CurrencyIsoCode != null) ? pemeInvoice.CurrencyIsoCode : '');
            this.peme.put('bankAccountHolder',(pemeInvoice.Bank_Account_Holder__c != null) ? pemeInvoice.Bank_Account_Holder__c : '');
            this.peme.put('bankBranch',(pemeInvoice.Bank_Branch__c != null) ? pemeInvoice.Bank_Branch__c : '');
            this.peme.put('bankAccountNumber',(pemeInvoice.Bank_Account_Number__c != null) ? pemeInvoice.Bank_Account_Number__c : '');
            this.peme.put('bicCode',(pemeInvoice.Swift_Bic_Code__c != null) ? pemeInvoice.Swift_Bic_Code__c : '');
            this.peme.put('agentName',(pemeInvoice.Manning_agent_name__c != null) ? pemeInvoice.Manning_agent_name__c : '');
            this.peme.put('claimRefNo',(pemeInvoice.PEME_reference_No__c != null) ? pemeInvoice.PEME_reference_No__c : '');
            this.peme.put('file',new Map<String,String>{'name' => pemeFile.Title,'salesforceRef' => pemeFile.contentDocumentId});
            this.peme.put('verified',true);
        }
    }
}