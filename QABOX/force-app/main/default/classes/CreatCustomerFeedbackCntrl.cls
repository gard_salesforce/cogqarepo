public with sharing class CreatCustomerFeedbackCntrl {

    @AuraEnabled
    public static Event getEventRecord(String recordId) {
        return([select id, Subject, WhatId from EVENT Where id=:recordId LIMIT 1]);
    }
}