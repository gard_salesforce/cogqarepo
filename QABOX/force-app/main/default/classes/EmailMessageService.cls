/********************************************************************************************************

* Name         : EmailMessageService
* Author       : Deloitte,Cognizant Technology Solutions
* CreatedDate  : 26/09/2019
* Description  : Helper class for EmailMessageTrigger
* Test class   : EmailMessageServiceTest

================================================================
*  Version                        Developer                       Date                   Description
================================================================
*  1.0                               ohuuse                      26/09/2019       
*  1.1                               Anju G                      16/03/2021                   SF-6299
*  1.2                               Arpan Muhuri                17/06/2021                   SF-6563
*********************************************************************************************************/
public with sharing class EmailMessageService {
    
    public static void newIncomingEmail(List<SObject> emails) {
        List<String> debugStrings = new List<String>();
        Map<Id, EmailMessage> emailMessagesByCaseIds = new Map<Id, EmailMessage>();
        debugStrings.add('EmailMessagesMap - ');
        String csNum ='';
        String csRefId='';
        integer endIndex;
        integer startIndex;
        Map<ID,String>caseNumIDMap=new map<Id,String>();
         Map<String,id>caseRefIDMap=new map<String,Id>();
         //Handle Out Of Office emails : SF-5820
        for (EmailMessage emailMessage : (List<EmailMessage>) emails) {
           
            if(emailMessage.subject!= null && emailMessage.subject.contains('Automatic reply')){
           
             if(emailMessage.subject.contains('Case Number: ')){                // added SF-6299
               csNum = emailMessage.subject.substringAfter('Case Number: ').substringBefore(' ]');
               caseNumIDMap.put(emailMessage.parentId,csNum);
               }
            }
        }
       
        Map<String,Case>caseMap=new Map<String,Case>();
        if (!caseNumIDMap.values().isEmpty()   ){
            for(Case cases:[SELECT id,Case_Ref_Id__c,New_incoming_email__c,caseNumber FROM Case WHERE CaseNumber IN :caseNumIDMap.values() ]){
            
                caseMap.put(cases.caseNumber,cases);
            
            }
        }
         // system.debug('caseMap****'+caseMap);
          //system.debug('caseNumIDMap****'+caseNumIDMap);
           for(EmailMessage email:(List<EmailMessage>) emails){
         if(email.incoming ){
            if(!caseMap.isEmpty() && email.subject.contains('Automatic reply') ){
           
            if(caseMap.containsKey(caseNumIdMap.get(email.parentId))){
            csRefId=caseMap.get(caseNumIDMap.get(email.parentId)).Case_Ref_Id__c;
            email.parentId=caseMap.get(caseNumIDMap.get(email.parentId)).id;
            }
             if(email.TextBody != null)
            email.TextBody = email.TextBody+'\n'+csRefId.replace(' ]','').replace('[ ','');
        if(email.HtmlBody != null)
           email.HtmlBody = email.HtmlBody+'\n'+csRefId.replace(' ]','').replace('[ ','');
           
          
         }
         
          emailMessagesByCaseIds.put(email.ParentId, email);
            debugStrings.add('em.parentId -> emailMessage.id'+emailMessage.parentId+' -> '+email.id);
       }}
         if(!caseNumIDMap.keyset().isEmpty()){
             delete [SELECT Id FROM Case where Id IN: caseNumIDMap.keySet()];
         }
        // system.debug('emailMessagesByCaseIds*****'+emailMessagesByCaseIds);
        
     
        // Get relevant field from the CaseIds (both for IF filter and notification param data below)
        // system.debug('emailMessagesByCaseIds keyset*****'+emailMessagesByCaseIds.keySet());
         //system.debug('query*******'+[SELECT Id, Subject, OwnerId, Owner_Name__c, CaseNumber, RecordTypeId, RecordType.Name,Subject_description__c FROM Case WHERE Id IN :emailMessagesByCaseIds.keySet()]);
        Map<Id, Case> casesByIds = new Map<Id, Case>([SELECT Id, Subject, OwnerId, Owner.IsActive, Owner_Name__c, CaseNumber, RecordTypeId, RecordType.Name,Subject_description__c FROM Case WHERE Id IN :emailMessagesByCaseIds.keySet()]);
     
        for (Id caseId : emailMessagesByCaseIds.keySet()) {
            // Only create a notification for emails that are incoming and related to cases with record type = Customer Transaction
            
            if ( caseId!=null && emailMessagesByCaseIds.get(caseId) !=null && emailMessagesByCaseIds.get(caseId).Incoming && !casesByIds.isEmpty() && casesByIds.get(caseId).RecordType.Name == 'Customer Transactions') {
                debugStrings.add('Line 24 is true');
                Map<String, Object> params = new Map<String, Object>();
                List<Id> recipientIdsList = new List<Id>();
                system.debug('Owner active? '+casesByIds.get(caseId).Owner.IsActive);
                if (string.valueOf(casesByIds.get(caseId).OwnerId).startsWith('005')) {
                    debugStrings.add('Line 28 is true');
                    //System.debug((casesByIds.get(caseId).OwnerId));
                    recipientIdsList.add(casesByIds.get(caseId).OwnerId);
                    String subjectDescription=casesByIds.get(caseId).Subject_description__c !=null ?casesByIds.get(caseId).Subject_description__c:'';
                    params.put('Title', 'New Email on Case: ' + casesByIds.get(caseId).CaseNumber);
                    params.put('Body', 'From: ' + emailMessagesByCaseIds.get(caseId).FromName + '\n Subject: ' + emailMessagesByCaseIds.get(caseId).Subject +
                                '\n Subject Description: '+subjectDescription);
                    params.put('targetID', emailMessagesByCaseIds.get(caseId).ParentId);
                    params.put('RecipientIds', recipientIdsList);
                    if(casesByIds.get(caseId).Owner.IsActive){       //Added for SF-6563
                        Flow.Interview.NewEmailFlow emailFlow = new Flow.Interview.NewEmailFlow(params);
                        debugStrings.add('mapParams being sent to the flow : ');
                        for(String paramsKey : params.keySet()) debugStrings.add('-params '+paramsKey+' -> '+params.get(paramsKey));
                        debugStrings.add('About to run the Flow');
                      
                        emailFlow.start();
                    }
                }else{
                    debugStrings.add('Line 28 is false');
                }
            }else{
                debugStrings.add('Line 24 is false');
            }
        }
        Logger__c logger = new Logger__c();
        logger.Log_Description__c = String.join(debugStrings,'\n\n');
        System.debug('EmailMessageService incomingEmailMail logs');
        for(String debugString : debugStrings) System.debug('-'+debugString);
        //insert logger;
        
    }
 
    
    /*
     Created by : Anju G
     Ticket No:SF-5437
     Description : if the Email message size exceeds 25 MB ,throw the error message
    */
    public static void checkOutboundEmailSize(Map<id,EmailMessage>emailMessageMap){
        Map<id,integer>contentSizeMap=new Map<id,Integer>();
        Boolean isIncoming=false;
        for(EmailMessage email:emailMessageMap.values()){
            if(email.incoming){
               isIncoming=true;
            }
        }
        if(!emailMessageMap.isempty() && !isIncoming){
            for(ContentDocumentLink content:[select id,ContentDocument.contentSize,ContentDocumentId,LinkedEntityId from ContentDocumentLink where LinkedEntityId IN: emailMessageMap.keyset() ]){
               if(contentSizeMap.containsKey(Content.LinkedEntityId)){
                contentSizeMap.put(content.LinkedEntityId,(contentSizeMap.get(content.LinkedEntityId)+content.ContentDocument.ContentSize));
               }
               else{
                   contentSizeMap.put(content.LinkedEntityId,content.contentDocument.contentSize);
               }
               
            }
        
        //Email_Limit_Setting__mdt emailLimit= [select Max_Limit__c from Email_Limit_Setting__mdt where   DeveloperName='Email_Body' limit 1 ];
       for(emailMessage email:emailMessageMap.values()){
       double attSize=0.0;
            if(contentSizeMap.containsKey(email.id)){
                attSize=(contentSizeMap.get(email.id))/(1024 * 1024);
            }
             
            Integer totalSize=(integer)(attSize+(email.messageSize__c/(1024*1024)));
             
                   
       if(!email.incoming && email.parentid!=null && email.parentId.getSObjectType() == Case.sObjectType && ( attSize > 17.5 || totalSize>=25) ){
            email.addError(Label.MessageSizeTooLarge);
        }
       }
        
      }  
    }
    
}