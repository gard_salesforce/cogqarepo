//OBSOLETED by SF-4727
public with sharing class MDMSyncAccountRetryExtension{

    public final Account account{get;private set;} 
	
	public String redirectUrl {public get; private set;}
    public Boolean shouldRedirect {public get; private set;}
    
    public Boolean retryRequired{//added for SF-4727
        public get{
        	if(account.Billing_Address_Sync_Status__c == 'Sync Failed' 
               || account.Shipping_Address_Sync_Status__c == 'Sync Failed' 
               || account.Synchronisation_Status__c == 'Sync Failed'
              ){
                  System.debug('billing/shipping/sychronisation - '+account.Billing_Address_Sync_Status__c+'/'+account.Shipping_Address_Sync_Status__c+'/'+account.Synchronisation_Status__c);
                  return true;
              }
            System.debug('returning false');
            return false;
        }
        private set;
    }
    
	public Boolean isAdmin{
		get{
			MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
			return mdmSettings.Retry_Buttons_Enabled__c;
		}
	}
	
	public MDMSyncAccountRetryExtension (ApexPages.StandardController stdController){
        String thisAcountId = ApexPages.currentPage().getParameters().get('editedAccountId');
        
        if(thisAcountId == null){
            thisAcountId = stdController.getId();
        }
        
        this.account = [SELECT id,name,Synchronisation_Status__c,Shipping_Address_Sync_Status__c,Billing_Address_Sync_Status__c,Deleted__c,BillingPostalCode,ShippingPostalCode FROM Account WHERE id = :thisAcountId];
		
        //CodeBlock-1 STARTS, to be commented when merged button is tested
		shouldRedirect = false;
        redirectUrl = stdController.view().getUrl();
        //CodeBlock-1 ENDS, to be commented when merged button is tested
        
        System.debug('sfEdit editedAcountId - '+thisAcountId);
        System.debug('sfEdit retryRequired - '+retryRequired);
        System.debug('sfEdit Opening MDMSyncAccountRetryRibbon Account - '+this.account);
        System.debug('sfEdit Opening MDMSyncAccountRetryRibbon isAdmin - '+isAdmin);
    }
    
	//Added for SF-4727, replaces three buttons with one
    public void retryAccountSync(){
        /*
        //TempBlock-1 STARTS, Only for testing to be replaced with CommentBlock-1
        account.Billing_Address_Sync_Status__c = 'Sync In Progress';
        account.Shipping_Address_Sync_Status__c = 'Sync In Progress';
        account.Synchronisation_Status__c = 'Sync In Progress';
        //TempBlock-1 ENDS
        */
        
        //CommentBlock-1 STARTS
        if(account.Billing_Address_Sync_Status__c == 'Sync Failed') account.Billing_Address_Sync_Status__c = 'Sync In Progress';
        if(account.Shipping_Address_Sync_Status__c == 'Sync Failed') account.Shipping_Address_Sync_Status__c = 'Sync In Progress';
        if(account.Synchronisation_Status__c == 'Sync Failed') account.Synchronisation_Status__c = 'Sync In Progress';
		//CommentBlock-1 ENDS
        
        System.debug('sfEdit Updating Account - '+this.account);
		update account;
        
    }
    
    //added for SF-4727, required for classic to reload only the ribbon part
    public PageReference reloadRibbon(){
        PageReference retPageReference = Page.MDMSyncAccountRetryRibbon;
        System.debug('sfEdit Updating Account - '+this.account);
        retPageReference.getParameters().put('editedAccountId',(this.account.id+''));
        System.debug('sfEdit returning retPageReference - '+retPageReference);
        retPageReference.setRedirect(true);
        return retPageReference;
    }
    
    //OBSOLETE Methods left only for Deployment to be remomved after 19R2
    public ApexPages.PageReference RetryPartnerSync(){return null;}
    public ApexPages.PageReference RetryBillingAddressSync(){return null;}
    public ApexPages.PageReference RetryShippingAddressSync(){return null;}
    //Obsolete Methods(after SF-4727) STARTS
    /*
	public ApexPages.PageReference RetryPartnerSync() {
		shouldRedirect = true;
		//Hopefully a better way of doing this than below as needs no logic here and will lock the account until the future method is completed...
		account.Synchronisation_Status__c = 'Sync In Progress';
		update account;
		return null;
	}
	
	public ApexPages.PageReference RetryShippingAddressSync() {
		shouldRedirect = true;
		//Hopefully a better way of doing this than below as needs no logic here and will lock the account until the future method is completed...
		account.Shipping_Address_Sync_Status__c = 'Sync In Progress';
		update account;
		return null;
	}
	
	public ApexPages.PageReference RetryBillingAddressSync() {
		shouldRedirect = true;
		//Hopefully a better way of doing this than below as needs no logic here and will lock the account until the future method is completed...
		account.Billing_Address_Sync_Status__c = 'Sync In Progress';
		update account;
		return null;
	}
	*/
    //Obsolete Methods(after SF-4727) ENDS
}