public without sharing class MyEventsCtrl{
    public string loggedInContactId;
    public string loggedInAccountId;
    public String sSoqlQuery;
    public List<String> NAstatusList;
    public List<String> selectedEvents{get;set;}
    public List<String> selectedEventStatus{get;set;}
    public Set<String> eventOptionsList{get;set;}
    public Set<String> statusOptionsList{get;set;}
    public List<fluidoEventWrapper> listEventWrapper{get;set;}
    public List<List<fluidoEventWrapper>> lstAllEventsForExcelAndPrint { get ; set ;}
    public List<fluidoconnect__Invitation__c> eventList{get;set;}
    public Account_Contact_Mapping__c acm_junc_obj;
    Public Boolean Administrator{get;set;}
    public boolean isPageLoad;
    private Boolean isQueried = false;
    String strLimit;
    public Integer pageNum{get;set;}
    private Integer SMARTQUERY_PAGE_SIZE = 10;
    Public Integer intStartrecord{get;set;}
    Public Integer intEndrecord{get;set;}
    Public Integer size{get;set;}
    Public Integer noOfRecords{get;set;}
    private String nameOfMethod;
    private String sortDirection;
    private String sortExp = '';
    String strDSC;
    public Integer noOfData{get;set;}
    Date TODATE = Date.Today();
    public MyEventsCtrl(){
        NAstatusList = new List<String>();
        NAstatusList.add('Draft');
        NAstatusList.add('Cancelled');
        Administrator = false;
        sortExpression = 'fluidoconnect__Survey__r.Event_Start_Date__c';
    //calling helperclass method
        MyGardHelperCtrl.CreateCommonData();
        loggedInContactId = MyGardHelperCtrl.LOGGED_IN_CONTACT_ID;
        loggedInAccountId = MyGardHelperCtrl.LOGGED_IN_ACCOUNT_ID;
        acm_junc_obj = MyGardHelperCtrl.ACM_JUNC_OBJ;
        selectedEvents = new List<String>();
        selectedEventStatus = new List<String>();
        eventOptionsList = new Set<String>();
        statusOptionsList = new Set<String>();
        listEventWrapper = new List<fluidoEventWrapper>();
        eventList = new List<fluidoconnect__Invitation__c>();
        isPageLoad = true;
        pageNum = 1;
        strLimit = 'LIMIT 10000';
        strDSC = 'DESC';
        sortDirection = strDSC;
         if(acm_junc_obj.Administrator__c == 'Admin')
            Administrator = true;
    }
    //To Build Sort Expression
    public String sortExpression
    {
        get
        {
            return sortExp;
        }
        set
        {
            //if the column is clicked on then switch between Ascending and Descending modes
            if (value == sortExp)
                sortDirection = (sortDirection == strDSC)? 'ASC' : strDSC;
            else
                sortDirection = strDSC;
            sortExp = value;
        }
    }
    
    //To Get Sort Direction on Column Header Click
    public String getSortDirection(){
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
            return strDSC;
        else
            return sortDirection;
    }
    public void setSortDirection(String value){
         sortDirection = value;
    }
    /*
    public List<SelectOption> getEventOptions(){
        List<SelectOption> evtOptions = new List<SelectOption>();
        if(eventOptionsList != null && eventOptionsList.size() > 0){
            for(String str : eventOptionsList){
                if(str != null)
                    evtOptions.add(new SelectOption(str,str));
            }
            evtOptions.sort();
        }
        return evtOptions;
    }*/
    
    //--------------------------
    public List<SelectOption> getEventOptions(){
        List<SelectOption> evtOptions = new List<SelectOption>();
        if(eventList != null && eventList.size() > 0){
            for(fluidoconnect__Invitation__c str : eventList){
                if(str != null && str.fluidoconnect__Survey__r.fluidoconnect__Survey_Title_English__c != null)
                    evtOptions.add(new SelectOption(str.fluidoconnect__Survey__c,str.fluidoconnect__Survey__r.fluidoconnect__Survey_Title_English__c));
            }
            evtOptions.sort();
        }
        return evtOptions;
    }
    //-------------------------------------
    public List<SelectOption> getStatusOptions(){
        List<SelectOption> Options = new List<SelectOption>();
        if(statusOptionsList != null && statusOptionsList.size() > 0){
            for(String str : statusOptionsList){
                Options.add(new SelectOption(str,str));
            }
            Options.sort();
        }
        return Options;
    }
    public List<fluidoEventWrapper> getEvents()
    {
        return listEventWrapper;
    }
    public pageReference clearOptions()
    {
        eventOptionsList.Clear();
        statusOptionsList.clear();
        pageReference pg = page.MyEvents;
        pg.setRedirect(true);
        return pg;
    }
    //Page action to fetch events
    public PageReference viewData(){
        if(eventOptionsList != null && eventOptionsList.size() >0)
            eventOptionsList.clear();
        if(statusOptionsList != null && statusOptionsList.size() >0)
            statusOptionsList.clear();
        isQueried = true;
        listEventWrapper = new List<fluidoEventWrapper>();
        pageNum = 1;
        String queryStr = '';
        String strCondition ='';
        string sortFullExp = '';
        
        sortFullExp = sortFullExp + sortExpression  + ' ' + sortDirection;
        queryStr = 'SELECT id,fluidoconnect__Survey__r.Invitations_Locked__c,fluidoconnect__Survey__r.fluidoconnect__Survey_Title_English__c,fluidoconnect__Survey__r.fluidoconnect__Status__c,fluidoconnect__Status__c,fluidoconnect__Survey__r.Event_Start_Date__c,fluidoconnect__Contact__c,fluidoconnect__Survey__c,fluidoconnect__Survey__r.fluidoconnect__Event_Type__c,fluidoconnect__Survey__r.Link_to_program__c,fluidoconnect__Survey_Link_text__c FROM fluidoconnect__Invitation__c WHERE fluidoconnect__Survey__r.fluidoconnect__Event_Type__c = \'Registration\' AND fluidoconnect__Contact__c =: loggedInContactId AND ((fluidoconnect__Survey__r.Event_Start_Date__c = LAST_N_DAYS:365 AND fluidoconnect__Survey__r.Event_Start_Date__c != null) OR fluidoconnect__Survey__r.Event_Start_Date__c >=: TODATE) AND fluidoconnect__Survey__r.fluidoconnect__Status__c NOT IN: NAstatusList AND fluidoconnect__Survey__r.Accessible_via_MyGard__c = true AND fluidoconnect__Survey__r.Invitations_Locked__c = true';
        if(selectedEvents != null && selectedEvents.size() > 0){
            //strCondition = strCondition + ' AND fluidoconnect__Survey__r.fluidoconnect__Survey_Title_English__c IN: selectedEvents ';
            strCondition = strCondition + ' AND fluidoconnect__Survey__c IN: selectedEvents ';
        }
        if(selectedEventStatus != null && selectedEventStatus.size() > 0){
            strCondition = strCondition + ' AND fluidoconnect__Status__c IN: selectedEventStatus ';
        }
        if(sortFullExp != null && sortFullExp != '')
            strCondition = strCondition + ' ORDER BY ' + sortFullExp;
        queryStr = queryStr + strCondition ;
        sSoqlQuery = queryStr;
        system.debug('sortFullExp---> '+sortFullExp);
        system.debug('queryStr--> '+queryStr+' --strCondition--> '+strCondition+'--selectedEvents--'+selectedEvents );
        for(fluidoconnect__Invitation__c fluidoInv : database.query(queryStr)){
            eventOptionsList.add(fluidoInv.fluidoconnect__Survey__r.fluidoconnect__Survey_Title_English__c);
            statusOptionsList.add(fluidoInv.fluidoconnect__Status__c);
        }
        searchEvents();
        return null;
    }
    public void searchEvents(){
        listEventWrapper = new List<fluidoEventWrapper>();
        if(sSoqlQuery.length()>0){
            eventList = (List<fluidoconnect__Invitation__c>)setCon.getRecords();
             if(setCon.getPageNumber() == 1)
                intStartrecord = 1;
            else
                intStartrecord  = ((setCon.getPageNumber() - 1) * SMARTQUERY_PAGE_SIZE) + 1;
            intEndrecord = setCon.getPageNumber() * SMARTQUERY_PAGE_SIZE;
             if(intEndrecord > noOfRecords ){
                intEndrecord = noOfRecords ;
            }
        }
        for(fluidoconnect__Invitation__c fci : eventList){
            listEventWrapper.add(new fluidoEventWrapper(fci.fluidoconnect__Survey__r.fluidoconnect__Survey_Title_English__c,
                                                        fci.fluidoconnect__Survey__r.fluidoconnect__Status__c,
                                                        fci.fluidoconnect__Status__c,
                                                        fci.fluidoconnect__Survey__r.Event_Start_Date__c,
                                                        fci.fluidoconnect__Survey_Link_text__c,
                                                        String.ValueOf(fci.fluidoconnect__Survey__r.Link_to_program__c),
                                                        fci.fluidoconnect__Survey__r.Invitations_Locked__c
                                                        ));
        }
        system.debug('noOfRecords - '+noOfRecords+' Size - '+size+' setCon.getPageNumber() - '+setCon.getPageNumber());
    }
    public ApexPages.StandardSetController setCon 
    {
        get{
            if(setCon == null || isQueried){
                isQueried = false;
                size = SMARTQUERY_PAGE_SIZE;
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery+' '+strLimit));
                setCon.setPageSize(size);
                noOfRecords = setCon.getResultSize();
                if (noOfRecords == null || noOfRecords == 0){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No search results found.'));
                }else if (noOfRecords == 10000){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The search returned 10000 records (maximum allowable limit).'));
                }
            }
            return setCon;
        }set;
    }

    //Wrapper definition
    public class fluidoEventWrapper{
        public String eventName{get;set;}
        public String eventStatus{get;set;}
        public String eventMemberStatus{get;set;}
        public Date eventDate{get;set;}
        public String eventLink{get;set;}
        public string programLink{get;set;}
        public Boolean invitationSent{get;set;}
        public fluidoEventWrapper(String eventName, String eventStatus, String eventMemberStatus, Date eventDate,String eventLink,String programLink,Boolean invitationSent){
            this.eventName = eventName;
            this.eventStatus = eventStatus;
            this.eventMemberStatus = eventMemberStatus;
            this.eventDate = eventDate;
            this.eventLink = eventLink;
            this.programLink = programLink;
            this.invitationSent = invitationSent;
        }
    }
    //Pagination methods
     //to go to a specific page number
    public void setpageNumber(){
        setCon.setpageNumber(pageNum);
        searchEvents();
    }
    public Boolean hasNext{
        get{
            return setCon.getHasNext();
        }set;
    }
    public Boolean hasPrevious{
        get{
            return setCon.getHasPrevious();
        }set;
    }
    
    public Integer pageNumber{
        get{
            return setCon.getPageNumber();
        }set;
    }
    public void first(){
        nameOfMethod='first';
        navigate();
    }
    public void next(){
        nameOfMethod='next';
        pageNum = setCon.getPageNumber();
        navigate();
    }
    public void last(){
        nameOfMethod='last';
        navigate();
    }
  
    public void previous(){
        nameOfMethod='previous';
        pageNum = setCon.getPageNumber();
        navigate();
    }
    public void navigate(){
        if(nameOfMethod=='previous'){
             setCon.previous();
             pageNum=setCon.getpageNumber();
             searchEvents();
        }
        else if(nameOfMethod=='last'){
             setCon.last();
             pageNum=setCon.getpageNumber();
             searchEvents();
        }
        else if(nameOfMethod=='next'){
             setCon.next();
             pageNum=setCon.getpageNumber();
             searchEvents();
        }
        else if(nameOfMethod=='first'){
             setCon.first();
             pageNum=setCon.getpageNumber();
             searchEvents();
        }
    }
    //Export to excel and print methods - Starts
    public Static String getGenerateTimeStamp()
    {
        Datetime myDT = Datetime.now();
        String formatted = myDT.formatGMT('d/MMM/yy HH:mm a');
        return formatted ;
    }
    public Static String getGenerateTime()
    {
        Datetime myDT = Datetime.now();
        String formatted = myDT.formatGMT('EEE MMM d HH:mm:ss yyyy');
        return formatted ;
    }
    public Static String getUserName(){
        Contact loggedInCon = new contact();
        String loggedInConName = '';
        User usr = [Select Id, contactID from User Where Id =: UserInfo.getUserId()]  ;
        if(usr != null && usr.contactID != null){
            loggedInCon = [Select AccountId,Name from Contact Where id =: usr.contactID];
            loggedInConName = loggedInCon.Name;
        }
        return loggedInConName;
    }
    public void collectAllDataForPrintAndExcel(){
        lstAllEventsForExcelAndPrint = new List<List<fluidoEventWrapper>>();
        Integer count = 1;
        noOfData = 0;
        listEventWrapper = new List<fluidoEventWrapper>();
        system.debug('************sSoqlQuery'+sSoqlQuery);
        for(fluidoconnect__Invitation__c fci : Database.Query(sSoqlQuery))
        {
            if(count < 1000){
                listEventWrapper.add(new fluidoEventWrapper(fci.fluidoconnect__Survey__r.fluidoconnect__Survey_Title_English__c,
                                                            fci.fluidoconnect__Survey__r.fluidoconnect__Status__c,
                                                            fci.fluidoconnect__Status__c,
                                                            fci.fluidoconnect__Survey__r.Event_Start_Date__c,
                                                            fci.fluidoconnect__Survey_Link_text__c,
                                                            String.ValueOf(fci.fluidoconnect__Survey__r.Link_to_program__c),
                                                            fci.fluidoconnect__Survey__r.Invitations_Locked__c)); 
                count++;
            }
            else{
                lstAllEventsForExcelAndPrint.add(listEventWrapper);
                listEventWrapper.add(new fluidoEventWrapper(fci.fluidoconnect__Survey__r.fluidoconnect__Survey_Title_English__c,
                                                            fci.fluidoconnect__Survey__r.fluidoconnect__Status__c,
                                                            fci.fluidoconnect__Status__c,
                                                            fci.fluidoconnect__Survey__r.Event_Start_Date__c,
                                                            fci.fluidoconnect__Survey_Link_text__c,
                                                            fci.fluidoconnect__Survey__r.Link_to_program__c,
                                                            fci.fluidoconnect__Survey__r.Invitations_Locked__c));
                count = 1;
            }
            noOfData++;
        }
        lstAllEventsForExcelAndPrint.add(listEventWrapper);
    }
    //Generate excel method
    public PageReference exportToExcel(){
        collectAllDataForPrintAndExcel();
        return page.GenerateExcelForMyEvents;
    }
    //Print method
    public PageReference print(){
        collectAllDataForPrintAndExcel();
        return page.PrintforMyEvents;
    }
    //Export to excel an print methods - Ends
}