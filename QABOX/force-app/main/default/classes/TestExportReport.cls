@isTest
public class TestExportReport {
    public static PEME_Invoice__c pi;
    public static PEME_Enrollment_Form__c pefBroker;
    
    Static testMethod void reportTest()
    {
        GardTestData test_record = new GardTestData();
        test_record.commonRecord();
        test_record.customsettings_rec();
        Test.startTest();
            pefBroker=new PEME_Enrollment_Form__c();
            pefBroker.ClientName__c=GardTestData.brokerAcc.ID;
            pefBroker.Enrollment_Status__c='Draft';
            pefBroker.Submitter__c=GardTestData.brokerContact.Id;
            insert pefBroker;
         
         // PEMEClientInvoiceDetailsCtrl pcid=new PEMEClientInvoiceDetailsCtrl();
            pi=new PEME_Invoice__c ();
            pi.Client__c=GardTestData.clientAcc.id;
            pi.Clinic_Invoice_Number__c='qwerty123';
            pi.Clinic__c=GardTestData.clientAcc.id;
            pi.Crew_Examined__c=11223;
            pi.Invoice_Date__c=Date.valueOf('2016-01-05');
            pi.PEME_reference_No__c='aasd23';
            pi.Status__c='Approved';
            pi.ObjectList__c='qwerty for test purpose';
            pi.Total_Amount__c=112230;
            pi.Vessels__c= GardTestData.test_object_1st.id;
            //pi.GUID__c='guid1';
            pi.Period_Covered_From__c=Date.valueOf('2016-01-05');
            pi.Period_Covered_To__c =Date.valueOf('2016-01-28');
            pi.PEME_Enrollment_Detail__c = pefBroker.id;
            insert pi;
        String str=[select name from PEME_Invoice__c].name;
         system.runAs(GardTestData.brokerUser)
        {     
            
            ApexPages.currentPage().getParameters().put('fileName','testSearch$"*dolr');
            ApexPages.currentPage().getParameters().put('id','00OD0000006tXOv');//pi.name);
            exportReport report = new exportReport();
            //ApexPages.currentPage().getParameters().put('example',EncodingUtil.urlDecode(toDecode,'UTF-8'));
            report.getFilename();
            report.getReportData();
            
        }
        Test.stopTest();
    }

}