@isTest
//deactivate PB - Auto create the KYC review v11, Populate BvD fields b4 runtest
private class TestCustomerFeedbackTrigger {
    public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Market_Area__c Markt;
    public static User salesforceLicUser;
    public static User dummySalesforceLicUser;
    public static Account clientAcc;
    ///////*******Variables for removing common literals********************//////////////
    public static string feedbckTypStr =  'Suggestion'; 
    public static string enUsrStr =  'en_US';
    public static string sourceStr =  'E-mail';
    /////**********************end**************************/////
    private static void createUsers(){
        List<User> userList = new List<User>();
        
        salesforceLicUser = new User(
                                Alias = 'standt', 
                                profileId = salesforceLicenseId ,
                                Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8',
                                CommunityNickname = 'test13',
                                LastName='Testing',
                                LanguageLocaleKey= enUsrStr ,
                                LocaleSidKey= enUsrStr ,  
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName='test008@testorg.com'
                                     );
        userList.add(salesforceLicUser);
        dummySalesforceLicUser = new User(
                                Alias = 'dummy', 
                                profileId = salesforceLicenseId ,
                                Email='dummyuesr@testorg.com',
                                EmailEncodingKey='UTF-8',
                                CommunityNickname = 'dummy13',
                                LastName='Dummy',
                                LanguageLocaleKey= enUsrStr ,
                                LocaleSidKey= enUsrStr ,  
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName='dummy008@testorg.com'
                                     );
        userList.add(dummySalesforceLicUser);
        insert userList; 
    }
    
    private static void createClientCompany(){
        //
        Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt; 
        createUsers();      
        
        //create valid Role-SubRole Combination for Client only
        List<Valid_Role_Combination__c> vrcList = new List<Valid_Role_Combination__c>();
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
        vrcList.add(vrcClient);
        insert vrcList;
        
        clientAcc = new Account( Name = 'Test_1', 
                                Site = '_www.test_1.se', 
                                Type = 'Client', 
                                BillingStreet = 'Gatan 1',
                                BillingCity = 'Stockholm', 
                                BillingCountry = 'SWE', 
                                BillingPostalCode = 'BS1 1AD',
                                recordTypeId=System.Label.Client_Contact_Record_Type,
                                Market_Area__c = Markt.id,
                                Area_Manager__c = salesforceLicUser.id,
                                OwnerId = salesforceLicUser.id
                               );
        insert clientAcc;
    }
    
    static testMethod void clientCompanyToFeedbackWithResponsible(){
        TestCustomerFeedbackTrigger.createClientCompany();
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        Customer_Feedback__c companyToFeedback = new Customer_Feedback__c(
                                Title__c = 'Test Company To Feedback',
                                From_Company__c = clientAcc.id,
                                Type_of_feedback__c =  feedbckTypStr ,
                                Source__c =  sourceStr ,
                                Responsible__c = dummySalesforceLicUser.id
                               );
        insert companyToFeedback;
        Customer_Feedback__c fetchedCompanyToFeedback = [SELECT ID, Responsible__c FROM Customer_Feedback__c 
                                                            WHERE Title__c = 'Test Company To Feedback' LIMIT 1];
        System.assertEquals(dummySalesforceLicUser.Id,fetchedCompanyToFeedback.Responsible__c);
    }
    
    static testMethod void clientCompanyToFeedbackWithoutResponsible(){
        TestCustomerFeedbackTrigger.createClientCompany();
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        Customer_Feedback__c companyToFeedback = new Customer_Feedback__c(
                                Title__c = 'Test Company To Feedback',
                                From_Company__c = clientAcc.id,
                                Type_of_feedback__c =  feedbckTypStr ,
                                Source__c =  sourceStr 
                               );
        insert companyToFeedback;
        Customer_Feedback__c fetchedCompanyToFeedback = [SELECT ID, Responsible__c FROM Customer_Feedback__c 
                                                            WHERE Title__c = 'Test Company To Feedback' LIMIT 1];
        System.assertEquals(salesforceLicUser.Id,fetchedCompanyToFeedback.Responsible__c);
    }
    
    static testMethod void clientCompanyFromFeedbackWithResponsible(){
        TestCustomerFeedbackTrigger.createClientCompany();
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        Customer_Feedback__c companyFromFeedback = new Customer_Feedback__c(
                                Title__c = 'Test Company From Feedback',
                                Company_to__c = clientAcc.id,
                                Type_of_feedback__c =  feedbckTypStr ,
                                Source__c =  sourceStr ,
                                Responsible__c = dummySalesforceLicUser.id
                               );
        insert companyFromFeedback;
        Customer_Feedback__c fetchedCompanyFromFeedback = [SELECT ID, Responsible__c FROM Customer_Feedback__c 
                                                            WHERE Title__c = 'Test Company From Feedback' LIMIT 1];
        System.assertEquals(dummySalesforceLicUser.Id,fetchedCompanyFromFeedback.Responsible__c);
    }
    
    static testMethod void clientCompanyFromFeedbackWithoutResponsible(){
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        TestCustomerFeedbackTrigger.createClientCompany();
        Customer_Feedback__c companyFromFeedback = new Customer_Feedback__c(
                                Title__c = 'Test Company From Feedback',
                                Company_to__c = clientAcc.id,
                                Type_of_feedback__c =  feedbckTypStr ,
                                Source__c =  sourceStr 
                               );
        insert companyFromFeedback;
        Customer_Feedback__c fetchedCompanyFromFeedback = [SELECT ID, Responsible__c FROM Customer_Feedback__c 
                                                            WHERE Title__c = 'Test Company From Feedback' LIMIT 1];
        System.assertEquals(salesforceLicUser.Id,fetchedCompanyFromFeedback.Responsible__c);
    }
    //SF-4503 added by Abhirup
    static testMethod void createChatterFeed(){
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        date cfdate = date.parse('05/11/2012');
        TestCustomerFeedbackTrigger.createClientCompany();
        Customer_Feedback__c companyFromFeedback = new Customer_Feedback__c(
                                Customer_feedback_type__c = 'Open',
                                Title__c = 'Test Company From Feedback',
                                From_Company__c = clientAcc.id,
                                Type_of_feedback__c =  feedbckTypStr ,
                                Source__c =  sourceStr,
                                Date_feedback_received__c = cfdate
                               );
        insert companyFromFeedback;
        //Post created on Account feed, feedback creation
        FeedItem post = new FeedItem();
        post.ParentId= companyFromFeedback.From_Company__c;
        post.Body = 'New Customer feedback: '+companyFromFeedback.Title__c+'\n'+
                    'Type of Customer Feedback'+' : '+companyFromFeedback.Type_of_feedback__c+'\n'+
                    'Date feedback received: '+companyFromFeedback.Date_feedback_received__c.format();
        post.LinkURL =URL.getSalesforceBaseUrl().toExternalForm() + '/' +companyFromFeedback.Id;
        post.title ='View Feedback';
        insert post; 
        
        Customer_Feedback__c confidentialFeedback = new Customer_Feedback__c(
                                Customer_feedback_type__c = 'Confidential',
                                Title__c = 'Test Company From Feedback',
                                From_Company__c = clientAcc.id,
                                Type_of_feedback__c =  feedbckTypStr ,
                                Source__c =  sourceStr,
                                Date_feedback_received__c = cfdate
                               );
        insert confidentialFeedback;
        
        Customer_Feedback__c confidentialFeedback_updt = new Customer_Feedback__c(
                                Id= confidentialFeedback.Id,
                                Customer_feedback_type__c = 'Open',
                                Title__c = 'Test Company From Feedback',
                                From_Company__c = clientAcc.id,
                                Type_of_feedback__c =  feedbckTypStr ,
                                Source__c =  sourceStr,
                                Date_feedback_received__c = cfdate
                               );
        update confidentialFeedback_updt;
        //Post created on Account feed, feedback update- 'Confidential' to 'Open'
        FeedItem post_onupdt = new FeedItem();
        post_onupdt.ParentId= confidentialFeedback_updt.From_Company__c;
        post_onupdt.Body = 'New Customer feedback: '+confidentialFeedback_updt.Title__c+'\n'+
                    'Type of Customer Feedback'+' : '+confidentialFeedback_updt.Type_of_feedback__c+'\n'+
                    'Date feedback received: '+confidentialFeedback_updt.Date_feedback_received__c.format();
        post_onupdt.LinkURL =URL.getSalesforceBaseUrl().toExternalForm() + '/' +confidentialFeedback_updt.Id;
        post_onupdt.title ='View Feedback';
        insert post_onupdt;
    }
}