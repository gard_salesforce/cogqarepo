public class ClaimDocumentResponseWrapper {
    public Boolean Result{get;set;}
    public integer Count{get;set;}
    public Boolean MoreResultsAvailable{get;set;} 
    public integer ObjectType{get;set;}
    public List<String> ObjectTypeAlias{get;set;}
    public List<ResultObject> ResultObjects{get;set;}
    
    //Used for sorting
    public static String sortCol;
    public static Boolean sortOrder;//true : ASC, false : DESC
    public class ResultObject implements Comparable{
        public String ObjectID {get;set;}
        public String ExternalID {get;set;}
        public String Name {get;set;}       
        public ObjectProperty ObjectProperties{get;set;}
        
        public Integer compareTo(Object thatObj){
            ResultObject that = (ResultObject) thatObj;
            system.debug(' this.ObjectProperties.name******'+ this.ObjectProperties.name);
            system.debug(' that.ObjectProperties.name******'+ that.ObjectProperties.name);
            system.debug('ClaimDocumentResponseWrapper.sortOrder****'+ClaimDocumentResponseWrapper.sortOrder);
            switch on ClaimDocumentResponseWrapper.sortCol{
                when 'name'{return ClaimDocumentResponseWrapper.sortOrder ? this.ObjectProperties.name.compareTo(that.ObjectProperties.name) : that.ObjectProperties.name.compareTo(this.ObjectProperties.name) ;}
                when 'policyYr'{return ClaimDocumentResponseWrapper.sortOrder ? this.ObjectProperties.pd_years.compareTo(that.ObjectProperties.pd_years) : that.ObjectProperties.pd_years.compareTo(this.ObjectProperties.pd_years);}
               //when 'object'{return ClaimDocumentResponseWrapper.sortOrder ? this.ObjectProperties.pd_object.compareTo(that.ObjectProperties.pd_object) : that.ObjectProperties.pd_object.compareTo(this.ObjectProperties.pd_object);}
                when 'object'{return ClaimDocumentResponseWrapper.sortOrder ? this.ObjectProperties.objectName.compareTo(that.ObjectProperties.objectName) : that.ObjectProperties.objectName.compareTo(this.ObjectProperties.objectName);}
                when 'eventDate'{return sortOrder ? compareToDateTime(this.ObjectProperties.lastModifiedDT,that.ObjectProperties.lastModifiedDT) : compareToDateTime(that.ObjectProperties.lastModifiedDT,this.ObjectProperties.lastModifiedDT);}
                when 'docType'{ return ClaimDocumentResponseWrapper.sortOrder ? this.ObjectProperties.pd_payclddocumenttype.compareTo(that.ObjectProperties.pd_payclddocumenttype) : that.ObjectProperties.pd_payclddocumenttype.compareTo(this.ObjectProperties.pd_payclddocumenttype);}
                when 'claimRef' {return ClaimDocumentResponseWrapper.sortOrder ? this.ObjectProperties.gardClaimRef.compareTo(that.ObjectProperties.gardClaimRef) : that.ObjectProperties.gardClaimRef.compareTo(this.ObjectProperties.gardClaimRef);}
                when 'total' {return ClaimDocumentResponseWrapper.sortOrder ? compareToDecimal(this.ObjectProperties.total,that.ObjectProperties.total) : compareToDecimal(that.ObjectProperties.total,this.ObjectProperties.total);}
                when 'cover' {return ClaimDocumentResponseWrapper.sortOrder ? this.ObjectProperties.coverName.compareTo(that.ObjectProperties.coverName) : that.ObjectProperties.coverName.compareTo(this.ObjectProperties.coverName);}
                when 'claimType' {return ClaimDocumentResponseWrapper.sortOrder ? this.ObjectProperties.claimType.compareTo(that.ObjectProperties.claimType) : that.ObjectProperties.claimType.compareTo(this.ObjectProperties.claimType);}
                when 'reserve' {return ClaimDocumentResponseWrapper.sortOrder ? compareToDecimal(this.ObjectProperties.reserve,that.ObjectProperties.reserve) :compareToDecimal(that.ObjectProperties.reserve,this.ObjectProperties.reserve);}
                when 'paid' {return ClaimDocumentResponseWrapper.sortOrder ? compareToDecimal(this.ObjectProperties.paid,that.ObjectProperties.paid) : compareToDecimal(that.ObjectProperties.paid,this.ObjectProperties.paid);}
                when else {return ClaimDocumentResponseWrapper.sortOrder ? this.ObjectProperties.name.compareTo(that.ObjectProperties.name) : that.ObjectProperties.name.compareTo(this.ObjectProperties.name);}
            }
           }
            private Integer compareToDateTime(DateTime thisDT,DateTime thatDT){
                return (
                    thisDT.getTime() == thatDT.getTime() ? 0 : 
                    thisDT.getTime() < thatDT.getTime() ? 1 :
                    -1
                );
            }
             private Integer compareToDecimal(decimal thisDec,decimal thatDec){
            return (
                thisDec == thatDec ? 0 : thisDec < thatDec  ? 1 : -1
            );
        }
        
    }
    public class ObjectProperty{
        public String pd_years{get;set;}
        public String pd_claim{get;set;}
        public String pd_payclddocumenttype{get;set;}
        public String documentClass{get;set;}
        public String name{get;set;}
        public String pd_creator{get;set;}
        public String lastModified{get;set;}
        //Below fields are not available in response. These are populated in pagify(..) method from Claim Map
        public Decimal paid{get;set;}
        public Decimal reserve{get;set;}
        public Decimal total{get;set;}
        public String claimType{get;set;}
        public String coverName{get;set;}
        public String objectName{get;set;}
        public String gardClaimRef{get;set;}
        public DateTime lastModifiedDT{get;set;}
    }
    public List<List<ResultObject>> pagifiedResultObjects{get;set;}//each inner list represents a page of records
    public void pagify(List<String> selectedObjects,List<String> selectedDocTypes,List<String> selectedClaimType,List<String> selectedCovers){
        if(ResultObjects == null) return;
        List<String> objectsToFilter = new List<String>();
        List<String> docTypesToFilter = new List<String>();
        List<String> claimTypeToFilter = new List<String>();
        List<String> coversToFilter = new List<String>();
        //String claimQueryFilter = '';
        if(selectedObjects != null && selectedObjects.size() > 0){
            for(String selectedObj : selectedObjects){
                System.debug('selectedObj - '+selectedObj);
                objectsToFilter.add(selectedObj.trim());
            }
            //claimQueryFilter += 'AND Object__r.name IN: objectsToFilter';
        }
        if(selectedDocTypes != null && selectedDocTypes.size() > 0){
            for(String selectedDocType : selectedDocTypes){
                System.debug('selectedDocType - '+selectedDocType);
                docTypesToFilter.add(selectedDocType.trim());
            }
        }
        if(selectedClaimType != null && selectedClaimType.size() >0){
            for(String selectedClmType : selectedClaimType){
                System.debug('selectedClmType - '+selectedClmType);
                claimTypeToFilter.add(selectedClmType.trim());
            }
             //claimQueryFilter += ' AND Claim_Type__c IN: claimTypeToFilter';
        }
        if(selectedCovers != null && selectedCovers.size() > 0){
            for(String selectedCover : selectedCovers){
                System.debug('selectedCover - '+selectedCover);
                coversToFilter.add(selectedCover.trim());
            }
            //claimQueryFilter += ' AND Risk_Coverage__r.name IN: coversToFilter';
        }
        //Map the sfdc claim ref id with corresponding claim
        Map<String,Claim__c> claimRefClaimMap = new Map<String,Claim__c>();
        List<String> claimRefList = new List<String>();
        for(ResultObject resObj:ResultObjects){
            if(resObj.ObjectProperties.pd_claim != null && resObj.ObjectProperties.pd_claim.contains(' -'))
                claimRefList.add(resObj.ObjectProperties.pd_claim.substringBefore(' -'));   //Fetch all the sfdc claim ref id from the response
        }
        for(Claim__c claim:[SELECT Object__r.name,Risk_Coverage__r.name,Claim_Type__c,Reserve__c,Paid__c,Total__c,Claim_Reference_Number__c,SFDC_Claim_Ref_ID__c FROM Claim__c WHERE SFDC_Claim_Ref_ID__c IN: claimRefList]){
            claimRefClaimMap.put(claim.SFDC_Claim_Ref_ID__c,claim);
        }
        //Mapping ends
        Integer PAGE_SIZE = 10;
        pagifiedResultObjects = new List<List<ResultObject>>();
        Integer aggregateRecords = 0;
        Claim__c claimObj;
        while(aggregateRecords < ResultObjects.size()){
            List<ResultObject> innerROs = new List<ResultObject>();
            
            while(innerROs.size() < PAGE_SIZE && aggregateRecords < ResultObjects.size()){
                ClaimDocumentResponseWrapper.ResultObject docResWrap_ResObj = ResultObjects.get(aggregateRecords);
                
                if(claimRefClaimMap != null && docResWrap_ResObj.ObjectProperties.pd_claim != null && claimRefClaimMap.containsKey(docResWrap_ResObj.ObjectProperties.pd_claim.substringBefore(' -')))
                    claimObj = claimRefClaimMap.get(docResWrap_ResObj.ObjectProperties.pd_claim.substringBefore(' -'));
                if(claimObj != null){
                    docResWrap_ResObj.ObjectProperties.paid = claimObj.Paid__c;
                    docResWrap_ResObj.ObjectProperties.reserve = claimObj.Reserve__c;
                    docResWrap_ResObj.ObjectProperties.total = claimObj.Total__c;
                    docResWrap_ResObj.ObjectProperties.claimType = claimObj.Claim_Type__c;
                    docResWrap_ResObj.ObjectProperties.coverName = claimObj.Risk_Coverage__r.name;
                    docResWrap_ResObj.ObjectProperties.objectName = claimObj.Object__r.name;
                    docResWrap_ResObj.ObjectProperties.gardClaimRef = claimObj.Claim_Reference_Number__c;
                    docResWrap_ResObj.ObjectProperties.lastModifiedDT = createDateTime(docResWrap_ResObj.ObjectProperties.lastModified);
                }else{
                    docResWrap_ResObj.ObjectProperties.paid = 0.00;
                    docResWrap_ResObj.ObjectProperties.reserve = 0.00;
                    docResWrap_ResObj.ObjectProperties.total = 0.00;
                    docResWrap_ResObj.ObjectProperties.claimType = 'N/A';
                    docResWrap_ResObj.ObjectProperties.coverName = 'N/A';
                    docResWrap_ResObj.ObjectProperties.objectName = 'N/A';
                    docResWrap_ResObj.ObjectProperties.gardClaimRef = 'N/A';
                }
                  system.debug('docTypesToFilter******'+ docTypesToFilter);
                  system.debug('docResWrap_ResObj.ObjectProperties.pd_payclddocumenttype******'+(docResWrap_ResObj.ObjectProperties.pd_payclddocumenttype));
                if(
                    (objectsToFilter.isEmpty() || objectsToFilter.contains(docResWrap_ResObj.ObjectProperties.objectName))
                    &&(docTypesToFilter.isEmpty() || docTypesToFilter.contains(docResWrap_ResObj.ObjectProperties.pd_payclddocumenttype))
                    && (claimTypeToFilter.isEmpty() || claimTypeToFilter.contains(docResWrap_ResObj.ObjectProperties.claimType))
                    && (coversToFilter.isEmpty() || coversToFilter.contains(docResWrap_ResObj.ObjectProperties.coverName))
                ){
                    system.debug('inside if*****');
                    innerROs.add(docResWrap_ResObj);
                }
                aggregateRecords++;
            }
            system.debug('innerROs****'+innerROs);
            pagifiedResultObjects.add(innerROs);
        }
        system.debug('pagifiedResultObects--'+pagifiedResultObjects);
    }
    public Datetime createDateTime(String strDateTime){
        DateTime dt;
        //System.debug('String strDateTime : '+ strDateTime);
        if(strDateTime !=''){
            
            string str =strDateTime.replace('.', '/');
            //System.debug('String str : '+ str);
            List<String> strDateTimeArr = str.split('/');
            //System.debug('String strDateTimeArr : '+ strDateTimeArr);
            List<String> year = new List<String>();
            if(strDateTimeArr.size() >= 2)
               year = strDateTimeArr[2].split(' ');
            
            try{                
                //dt = DateTime.newInstance(integer.valueOf(year[0]) ,integer.valueOf(strDateTimeArr[0]),integer.valueOf(strDateTimeArr[1]), 0, 0, 0) ;  
                dt = DateTime.newInstance(integer.valueOf(year[0]), integer.valueOf(strDateTimeArr[1]),integer.valueOf(strDateTimeArr[0]), 1, 0, 0) ;
           
            }catch(Exception ex){
                System.debug('Exception while creating DateTime instance - '+ex.getMessage());
                return null;
            }
        }
        //system.debug('dt-------- --->' + dt);
        return dt;
    }
    public void sortIt(String sortCol,Boolean sortOrder){
        ClaimDocumentResponseWrapper.sortCol = sortCol;
        ClaimDocumentResponseWrapper.sortOrder = sortOrder;
        List<ResultObject> tempResObjs = new List<ResultObject>();
        for(List<ResultObject> resultObjectPage : this.pagifiedResultObjects) tempResObjs.addAll(resultObjectPage);
        tempResObjs.sort();//sorts the single document list
        this.pagifiedResultObjects = pagify(tempResObjs);
    }
    private static List<List<ResultObject>> pagify(List<ResultObject> linearResultObjects){
        List<List<ResultObject>> pagifiedResultObjects = new List<List<ResultObject>>();
        Integer PAGE_SIZE = 10;
        Integer aggregateRecords = 0;
        while(aggregateRecords < linearResultObjects.size()){
            List<ResultObject> resultObjectsPage = new List<ResultObject>();
            while(resultObjectsPage.size() < PAGE_SIZE && aggregateRecords < linearResultObjects.size()){
                resultObjectsPage.add(linearResultObjects.get(aggregateRecords));
                aggregateRecords++;
            }
            pagifiedResultObjects.add(resultObjectsPage);
        }
        
        
        return pagifiedResultObjects;
    }
}