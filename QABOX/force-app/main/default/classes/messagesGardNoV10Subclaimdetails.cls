//Generated by wsdl2apex

public class messagesGardNoV10Subclaimdetails {
    public class SubClaimDetailsBase {
        public messagesGardNoV10Personalclaimdetai.PersonalClaimDetailsBase PersonalClaimDetails;
        public messagesGardNoV10Pollutionclaimdeta.PollutionClaimDetailsBase PollutionClaimDetails;
        public messagesGardNoV10Stowawayclaimdetai.StowawayClaimDetailsBase StowawayClaimDetails;
        public messagesGardNoV10Thirdpartycollisio.ThirdPartyCollisionClaimDetailsBase ThirdPartyCollisionClaimDetails;
        public messagesGardNoV10Thirdpartycontactc.ThirdPartyContactClaimDetailsBase ThirdPartyContactClaimDetails;
        public messagesGardNoV10Peopleclaimdetails.PeopleClaimDetailsBase PeopleClaimDetails;
        public messagesGardNoV10Crewclaimdetails.CrewClaimDetailsBase CrewClaimDetails;
        public messagesGardNoV10Cargoclaimdetails.CargoClaimDetailsBase CargoClaimDetails;
        private String[] PersonalClaimDetails_type_info = new String[]{'PersonalClaimDetails','http://messages.gard.no/v1_0/SubClaimDetails',null,'0','1','false'};
        private String[] PollutionClaimDetails_type_info = new String[]{'PollutionClaimDetails','http://messages.gard.no/v1_0/SubClaimDetails',null,'0','1','false'};
        private String[] StowawayClaimDetails_type_info = new String[]{'StowawayClaimDetails','http://messages.gard.no/v1_0/SubClaimDetails',null,'0','1','false'};
        private String[] ThirdPartyCollisionClaimDetails_type_info = new String[]{'ThirdPartyCollisionClaimDetails','http://messages.gard.no/v1_0/SubClaimDetails',null,'0','1','false'};
        private String[] ThirdPartyContactClaimDetails_type_info = new String[]{'ThirdPartyContactClaimDetails','http://messages.gard.no/v1_0/SubClaimDetails',null,'0','1','false'};
        private String[] PeopleClaimDetails_type_info = new String[]{'PeopleClaimDetails','http://messages.gard.no/v1_0/SubClaimDetails',null,'0','1','false'};
        private String[] CrewClaimDetails_type_info = new String[]{'CrewClaimDetails','http://messages.gard.no/v1_0/SubClaimDetails',null,'0','1','false'};
        private String[] CargoClaimDetails_type_info = new String[]{'CargoClaimDetails','http://messages.gard.no/v1_0/SubClaimDetails',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://messages.gard.no/v1_0/SubClaimDetails','true','false'};
        private String[] field_order_type_info = new String[]{'PersonalClaimDetails','PollutionClaimDetails','StowawayClaimDetails','ThirdPartyCollisionClaimDetails','ThirdPartyContactClaimDetails','PeopleClaimDetails','CrewClaimDetails','CargoClaimDetails'};
    }
}