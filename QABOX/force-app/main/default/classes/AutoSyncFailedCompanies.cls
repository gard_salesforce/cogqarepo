public class AutoSyncFailedCompanies {
    private List<Account> allFailedCompanies = new List<Account>();
    private List<Account> newCompanies = new List<Account>();
    private List<Account> existingCompanies = new List<Account>();
    
    public AutoSyncFailedCompanies(){
        allFailedCompanies = [SELECT ID, GIC_Office_ID__c, Company_Role__c,Sub_Roles__c, Synchronisation_Status__c,
                              Roles_Sync_Status__c, Shipping_Address_Sync_Status__c, Billing_Address_Sync_Status__c 
                              FROM ACCOUNT WHERE Account_Sync_Status__c = 'Sync Failed'];
        System.Debug('Failed Companies Size: ' + allFailedCompanies.size());
    }
    /* This method will do the synchronization of the failed companies */
    public void syncFailedCompanies(){
        separateCompanies();
        
        if(newCompanies.size() > 0){
            Map<id, Account> newAccountsMap = new Map<Id, Account>();
            for(Account aNewAccount : newCompanies){
                newAccountsMap.put(aNewAccount.Id, aNewAccount);
            }
            System.debug('New Companies Size: ' + newCompanies.size());
            MDMSyncAccountHandler.ResyncNewAccounts(newCompanies, newAccountsMap);
        }
        
        if(existingCompanies.size() > 0){
            System.debug('Existing Companies Size: ' + existingCompanies.size());
            MDMSyncAccountHandler.ResyncExistingAccounts(existingCompanies);
        }
    }
    
    /* This method will separate the new and existing companies by -
	* If the GIC id is absent, then it is a new company,
	* otherwise it is an existing company
	*/
    private void separateCompanies(){
        for(Account anAccount : allFailedCompanies){
            if(anAccount.GIC_Office_ID__c == null || anAccount.GIC_Office_ID__c.equals('')){
                newCompanies.add(anAccount);
            }else{
                existingCompanies.add(anAccount);
            }
        }
    }
}