public without sharing class EmailQuoteController {
    
    /************************************************************
     * Author: Sam Wadhwani
     * Company: cDecisions Ltd
     * Date: 03/03/2011
     * Description: Controller class for EmailQuote VF page
     *            controller captures data from the quote record
     *            and retrieves the email service address from
     *            custom settings to construct and prepopulate
     *            the URL for automatic redirection to the email 
     *            composition page
     * Modifications:
     *    [ENTER DATE AND DESCRIPTION OF MODS HERE]
     *************************************************************/
    
    Public Quote quote;
    public EmailQuoteController(ApexPages.standardController controller){
        quote = [Select Id, ContactId, OpportunityId, Opportunity.AccountId from Quote where Id =:controller.getId()];
    }
    
    Public PageReference sendEmail(){
        List<QuoteDocument> quoteDocuments = [Select Id from QuoteDocument where quoteId = :quote.Id order by createddate desc limit 1];
        //if(quoteDocuments.size() > 0){
            Contact quoteContact;
            if(quote.ContactId != null)
                quoteContact = [Select Email from Contact where id=:quote.ContactId limit 1];
            String emailAuthorUrl = '/_ui/core/email/author/EmailAuthor?';
            if(quoteContact != null) emailAuthorUrl += 'p2_lkid=' + quoteContact.Id + '&';
            else emailAuthorUrl += 'p2_lkid=' + quote.Opportunity.AccountId + '&';
            emailAuthorUrl += 'rtype=003&';
            emailAuthorUrl += 'p3_lkid=' + quote.Id + '&';
            if(quoteDocuments.size()>0) emailAuthorUrl += 'doc_id=' + quoteDocuments[0].Id + '&';
            emailAuthorUrl += 'retURL=%2F' + quote.Id + '&';
            EmailSettings__c quotesettings = EmailSettings__c.getInstance('Quote');
            String emailServicesEmail = quotesettings.Attachment_BCC_Service__c;
            emailAuthorUrl += 'p5=' + EncodingUtil.urlEncode(emailServicesEmail, 'UTF-8');
            return new PageReference(emailAuthorUrl);
        //}
        //else{
        //    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'You need to create PDF first'));
        //    return null;
        //}
    }
    
    static TestMethod void testSendEmail() {
        String strRecId = [SELECT Id FROM RecordType WHERE Name='Marine' AND sObjectType='Opportunity' LIMIT 1].Id;
        EmailSettings__c settings = EmailSettings__c.getInstance('Quote');
        String svcaddress = settings.Attachment_BCC_Service__c;
        //create an account
        Account acc = TestDataGenerator.getClientAccount();//new Account(Name='APEXTESTACC001');
        //insert acc;
        String strAccId = acc.Id;
        //create an opportunity
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = strRecId;
        opp.Name = 'APEXTESTOPP001';
        opp.AccountId = strAccId;
        opp.StageName = 'Renewable Opportunity';
        opp.Type = 'New Business';
        opp.Sales_Channel__c = 'Direct';
        opp.CloseDate = Date.Today();
        insert opp;
        String strOppId = opp.Id;
        //create a quote
        Quote qt = new Quote(Name='APEXTESTQTE001', OpportunityId=strOppId);
        insert qt;
        String strQuoteId = qt.Id;
        //create a quote document
        QuoteDocument pdf = new QuoteDocument();
        pdf.QuoteId = qt.Id;
        pdf.Document = Blob.valueOf('APEX TEST QUOTE DOCUMENT CONTENT');
        insert pdf;
        String strDocId = pdf.Id;
        
        Test.startTest();
            PageReference pageRef = new PageReference('/apex/EmailQuote?id='+strQuoteId);
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController stdcontroller = new ApexPages.StandardController(qt);
            EmailQuoteController controller = new EmailQuoteController(stdcontroller);
            System.assertEquals(strQuoteId, controller.quote.Id);
            PageReference emailPage = controller.sendEmail();
        Test.stopTest();
        String testurl = '/_ui/core/email/author/EmailAuthor?';
        testurl+='doc_id=' + strDocId + '&p2_lkid=' + strAccId;
        testurl+='&p3_lkid=' + strQuoteId + '&p5=' + EncodingUtil.urlEncode(svcaddress, 'UTF-8');
        testurl+='&retURL=%2F' + strQuoteId + '&rtype=003';
        System.assertEquals(testurl, emailPage.getUrl());
    }
}