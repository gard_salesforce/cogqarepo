/*
Requirement : Enable the New Opportunity Checklist button in Opportunity Checklist related list of Opportunity in Lightning view
JIRA : SF-4484
Author : Abhirup Banik
Revision History
Intial version : 04/03/2019 : (Descp : used in New_OppChecklist_btn.cmp)
*/
public with sharing class New_OppChecklist_btn_Ctrl {
    @AuraEnabled
    public static List<Opportunity> getOpportunites(String recordId) {
        return [select id, Name, RecordTypeName__c,Checklist_Count__c,Type from Opportunity Where id=:recordId];
    }   
    @AuraEnabled
    public static List<Opportunity_Checklist__c> getOppChecklists(String oppId) {        
        System.debug('OppId'+oppId);
        List<Opportunity_Checklist__c> oppchk = [select id, Name, RecordType.Name,Checklist_Completed__c,CreatedBy.Name,LastModifiedBy.Name,Opportunity__r.Name from Opportunity_Checklist__c Where Opportunity__c = :oppId];
        System.debug('List--->'+oppchk[0].Opportunity__r.Name);
        return oppchk;
    }
    @AuraEnabled
    public static List<recordTypeWrapper.DisplayRecordTypes> getRecordTypes() {
        List<recordTypeWrapper.DisplayRecordTypes> lstRecordTypes =
            new List<recordTypeWrapper.DisplayRecordTypes>();
        List<Schema.RecordTypeInfo> recordtypes = Opportunity_Checklist__c.SObjectType.getDescribe().getRecordTypeInfos();
        for(RecordTypeInfo rt : recordtypes){
            if(rt.getName() != 'Master' && rt.getRecordTypeId() != '012D0000000QrrZIAS'
                                        && rt.getRecordTypeId() != '012D0000000V8dVIAS'
                                        && rt.getRecordTypeId() != '012D00000003HOfIAM'
                                        && rt.getRecordTypeId() != '012D0000000V8dWIAS'
                                        && rt.getRecordTypeId() !='012D0000000QrrYIAS')
                if(rt.isActive()==True){
                    recordTypeWrapper.DisplayRecordTypes recordType =
                        new recordTypeWrapper.DisplayRecordTypes();
                    recordType.recordTypeId =rt.getRecordTypeId();
                    recordType.recordTypeLabel = rt.getName()+' checklist';
                    recordType.isDefault =rt.isDefaultRecordTypeMapping();
                    lstRecordTypes.add(recordType);
                }
        }
        system.debug('lstRecordTypes' +lstRecordTypes);
        return lstRecordTypes;
        
    }    
    @AuraEnabled
    public static String fetchUserInfo(){
        String ProfileId;
        ProfileId = UserInfo.getProfileId();
        //User oUser = [select id,Name,ProfileId FROM User Where id =:UserInfo.getUserId()];
        system.debug('Profile-->'+ProfileId);
        return ProfileId;
    }
    @AuraEnabled
    public static Map<String,String> deleteRecord(String oppchkId){
        Map<String,String> mess = new Map<String,String>();
        //List<OpportunityPartner> partnerOrBrokersList = new List<OpportunityPartner>();         
        if(String.isEmpty(oppchkId)){
           mess.put('status','error'); 
           mess.put('message','Id is wrong!');
           return mess; 
        }        
        List<Opportunity_Checklist__c> oppChkList = [SELECT Id FROM Opportunity_Checklist__c where Id =:oppchkId];
        try{
           delete oppChkList;
           mess.put('status','success'); 
           mess.put('message','Deleted Successfully!');            
        }catch(Exception e){
           mess.put('status','error'); 
           mess.put('message','Id is wrong!');
        }
        return mess;
    }
}