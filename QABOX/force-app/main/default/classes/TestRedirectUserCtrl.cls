@istest

Public Class TestRedirectUserCtrl{
///////*******Variables for removing common literals********************//////////////
    public static string enUsrStr =  'en_US';
    public static string mailngStateStr =   'London';
/////**********************end**************************/////
public static testmethod void RedirectUser(){

    List<Account> AccountList=new List<Account>();
    List<Contact> ContactList=new List<Contact>();
    List<Contract> ContractList=new List<Contract>();
    List<User> UserList=new List<User>();
    List<Asset> AssetList=new List<Asset>();
    List<Case> caseList =new List<Case>();
    List<Market_Area__c> Market_AreaList=new List<Market_Area__c>();
    List<Object__c> ObjectList=new List<Object__c>();
    string partnerLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Partner Community' and name='Partner Community Login User Custom' limit 1].id;
    string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    Account clientAcc;
    Account brokerAcc;
    Contact brokerContact;
    Contact clientContact;
    User brokerUser, salesforceLicUser ;
    User clientUser;
    //correspondant contacts Custom Settings
        List<ContactSubscriptionFields__c> conFieldsList = new List<ContactSubscriptionFields__c>();
        ContactSubscriptionFields__c conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        ContactSubscriptionFields__c conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        ContactSubscriptionFields__c conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList;
        
          salesforceLicUser = new User(
                                            Alias = 'standt', 
                                            profileId = salesforceLicenseId ,
                                            Email='standarduser@testorg.com',
                                            EmailEncodingKey='UTF-8',
                                            CommunityNickname = 'test13',
                                            LastName='Testing',
                                            LanguageLocaleKey= enUsrStr ,
                                            LocaleSidKey= enUsrStr ,  
                                            TimeZoneSidKey='America/Los_Angeles',
                                            UserName='test008@testorg.com.mygard'
                                           );
        insert salesforceLicUser;
       //Accounts............
       brokerAcc = new Account(  Name='testre',
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',
                                    recordTypeId=System.Label.Broker_Contact_Record_Type,
                                    Site = '_www.cts.se',
                                    Type = 'Broker'
                                   // Market_Area__c = Markt.id,
                                   // Area_Manager__c = salesforceLicUser.id                                                                       
                                 );
       AccountList.add(brokerAcc);
    
      
       clientAcc = new Account( Name = 'Test_1', 
                                        Site = '_www.test_1.se', 
                                        Type = 'Client', 
                                        BillingStreet = 'Gatan 1',
                                        BillingCity = 'Stockholm', 
                                        BillingCountry = 'SWE', 
                                        BillingPostalCode = 'BS1 1AD',
                                        recordTypeId=System.Label.Client_Contact_Record_Type
                                       // Market_Area__c = Markt.id,
                                       // Area_Manager__c = salesforceLicUser.id 
                                       );
                                    
        AccountList.add(clientAcc); 
        insert AccountList; 
        
        Attachment att=new Attachment(Name='abc',
                                        ContentType='abcd',
                                        Body = blob.toPDF('pdfContent'),
                                        ParentId = brokerAcc.id
                                       );
          insert att;
        
        //Contacts.................
        brokerContact = new Contact( 
                                             FirstName='Raan',
                                             LastName='Baan',
                                             MailingCity =  mailngStateStr ,
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState =  mailngStateStr ,
                                             MailingStreet = '1 London Road',
                                             AccountId = brokerAcc.Id,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(brokerContact) ;

        clientContact= new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity =  mailngStateStr ,
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState =  mailngStateStr ,
                                              MailingStreet = '4 London Road',
                                              AccountId = clientAcc.Id,
                                              Email = 'test321@gmail.com'
                                          );
        ContactList.add(clientContact);
        insert ContactList;
        
        //Users..................
        brokerUser = new User(
                                    Alias = 'standt', 
                                    profileId = partnerLicenseId,
                                    Email='mdjawedm@gmail.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='estTing',
                                    LanguageLocaleKey= enUsrStr ,
                                    CommunityNickname = 'brokerUser',
                                    LocaleSidKey= enUsrStr ,  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testbrokerUser@testorg.com.mygard',
                                    ContactId = BrokerContact.Id
                                    //IsPortalEnabled = true
                                   // ContactId__c = grdobj.id
                                   );
        UserList.add(brokerUser);
                                   
        clientUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'clientUser',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey= enUsrStr ,
                                    LocaleSidKey= enUsrStr ,
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testclientUser@testorg.com.mygard',
                                    ContactId = clientContact.id
                                   // IsPortalEnabled = true
                                   // ContactId__c = grdobj.id
                                   );
                
         UserList.add(clientUser) ;
         insert UserList;
         
         System.runAs(brokerUser){
         
         RedirectUserCtrl test= new RedirectUserCtrl();
         test.RedirectUserToHome();
         System.assertEquals(test.isPortal , true);
         }
         }
         }