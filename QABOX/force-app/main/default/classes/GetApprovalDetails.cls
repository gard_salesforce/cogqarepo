public with sharing class GetApprovalDetails {

	public String objId
	{
		get;
		
		set
		{
			objId = value;
			
			if(approvalDetails == null && objId != null)
			{
				//First time requested, populate the list
				/*approvalDetails = new List<ProcessInstanceStep>([Select p.Comments, 
				p.ProcessInstance.TargetObjectId, p.Actor.FirstName, p.Actor.LastName, p.SystemModStamp from ProcessInstanceStep p  WHERE 
				p.ProcessInstance.TargetObjectID = :objId AND p.ProcessInstance.Status = 'Pending' AND (StepStatus = 'Approved' OR StepStatus='Rejected')]);*/
				
				List<ProcessInstance> approval = new List<ProcessInstance>();
				approval=[SELECT TargetObjectId, (SELECT Id, StepStatus, Comments, SystemModstamp, OriginalActor.FirstName, OriginalActor.LastName, Actor.FirstName, Actor.LastName FROM Steps WHERE (StepStatus = 'Approved' OR StepStatus = 'Rejected' OR StepStatus = 'Removed' OR StepStatus = 'Started') ORDER BY CreatedDate DESC) FROM ProcessInstance WHERE TargetObjectId =:objId ORDER BY CreatedDate DESC LIMIT 1];
				approvalDetails = approval[0].Steps;
				
				hasResults = true;		
			}
		}
		
	}
	public String msg{get;set;}
	public Boolean showComments{get;set;}
	
	public Boolean hasResults{
		
		get;
		/*{
			if(hasResults == null)
			{
				List<ProcessInstanceStep> steps = approvalDetails;
				if(!steps.isEmpty())
				{
					hasResults = true;
				}
				else
				{
					hasResults = false;
				}
			}
			
			return hasResults;
		}*/
	
		set;
	}
	
	public List<ProcessInstanceStep> approvalDetails{
		
		get;	
		/*{
			if(approvalDetails == null && objId != null)
			{
				//First time requested, populate the list
				approvalDetails = new List<ProcessInstanceStep>([Select p.Comments, 
				p.ProcessInstance.TargetObjectId, p.Actor.FirstName, p.Actor.LastName, p.SystemModStamp from ProcessInstanceStep p  WHERE 
				p.ProcessInstance.TargetObjectID = :objId AND p.ProcessInstance.Status = 'Pending' AND (StepStatus = 'Approved' OR StepStatus='Rejected')]);		
			}
			else
			{
				//return existing list
			}
			
			return approvalDetails;
		}*/
		set;}
	
	public GetApprovalDetails()
	{
		//System.Debug('Object Id:' + objId);
						
	}
}