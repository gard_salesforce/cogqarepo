global class Scheduler_BatchInsertMyCover implements Schedulable
{
    public static String sched = '0 00 01 * * ?';  //4 times a day 

    global static String scheduleMe() 
    {
        Scheduler_class SC = new Scheduler_class(); 
        return System.schedule('My batch Job', sched, SC);
    }

    global void execute(SchedulableContext sc) 
    {    
        if(!Test.isRunningTest())  
        {
            BatchInsertMyCover_bkp  prodBatch_false = new BatchInsertMyCover_bkp('false');     
            database.executebatch(prodBatch_false, 1);
            BatchInsertMyCover_bkp  prodBatch_true = new BatchInsertMyCover_bkp('true');
            database.executebatch(prodBatch_true, 20);
        }
    }
}