@isTest(seeAllData=false)
Public class TestRIDeclarationFormCtrl{ 
    void setUpTestData(){
        //GardTestData test_rec = new  GardTestData();
        //test_rec.commonrecord();
        //test_rec.customsettings_rec();
    }
    
    @isTest private static void testMethod1(){
        GardTestData test_rec = new  GardTestData();
        test_rec.commonrecord();
        test_rec.customsettings_rec();
        System.runAs(GardTestData.brokerUser){   
            Test.startTest();
            RIDeclarationFormCtrl rid=new RIDeclarationFormCtrl();
            RIDeclarationFormCtrl.InnerClass inn=new RIDeclarationFormCtrl.InnerClass();
            rid.changeSelectedClientIdFormsCS();
            rid.lstContact = new List<Contact>();
            rid.imoNo = 123;
            rid.noImoNo=true;
            rid.clientName = 'abc';
            rid.IdAsset=new Set<Id>();
            rid.selectedObj=GardTestData.test_object_1st.id;
            rid.clean='clean';
            rid.dirty='dirty';
            rid.lstAst=new List<Asset>();
            rid.MailId=GardTestData.uwf.id;
            rid.getDates();
            rid.getLoad();
            rid.getDischarge();
            rid.selectedglobalclient = new list<string>();
            rid.selectedglobalclient.add(GardTestData.clientAcc.id);
            List<SelectOption> clientOptions =  rid.getClientLst();
            rid.assetFinal = null;
            rid.selectedClient = GardTestData.clientAcc.id;
            rid.ObjName =string.valueof(GardTestData.test_object_1st.name) ;            
            rid.FindBy='objname';
            rid.searchByImoObjectName();
            rid.assetFinal = null;
            rid.selectedClient = GardTestData.clientAcc.id;
            rid.ObjName = string.valueof(GardTestData.test_object_1st.Imo_Lloyds_No__c);  
            rid.FindBy='imo';
            rid.searchByImoObjectName();
            rid.dateOutputForReview='05/02/2015';
            rid.FindBy='objname';
            rid.searchByImoObjectName();
            rid.FindBy='imo';
            rid.searchByImoObjectName();
            rid.setObjectSelection();
            rid.getCName();
            rid.addTextBox();
            rid.rowIndex=0;
            rid.removeTextBox();
            system.debug('--in 50--');
            rid.saveRecord();
            rid.clearRecord();
            rid.uwf.Clean_rated_vessel_carried_persistent__c=true;
            rid.uwf.Dirty_rated_vessel_carried_non_persist__c=true;
            rid.goNext();
            rid.goPrev(); 
            rid.sendToForm();
            rid.cancelRecord();
            rid.uwf.id=GardTestData.brokerCase.id; 
            //rid.saveRecord();     commented on 9th sept 2018
            rid.uwf.id=GardTestData.brokerCase.id; 
            rid.saveSubmit();
            rid.strSelected=GardTestData.clientAcc.id;
            rid.fetchSelectedClients();
            rid.strSelected=GardTestData.clientAcc.id+';'+GardTestData.clientAcc.id; 
            rid.fetchSelectedClients(); 
            rid.strGlobalClient=GardTestData.clientAcc.id+';'+GardTestData.clientAcc.id; 
            rid.fetchGlobalClients();
            Test.stopTest();
        }        
    }
    @isTest private static void testMethod2(){
        GardTestData test_rec = new  GardTestData();
        test_rec.commonrecord();
        test_rec.customsettings_rec();
        System.runAs(GardTestData.clientUser){
            Test.startTest();
            RIDeclarationFormCtrl rid=new RIDeclarationFormCtrl();
            rid.lstAccount = new List<Account>();
            rid.selectedClientfromPrev = GardTestData.clientAcc.id;
            rid.IdAsset=new Set<Id>();
            rid.selectedObj=GardTestData.test_object_1st.id;
            rid.clean='clean';
            rid.dirty='dirty';
            rid.lstAst=new List<Asset>();
            RIDeclarationFormCtrl.InnerClass inn=new RIDeclarationFormCtrl.InnerClass();
            rid.MailId=GardTestData.uwf.id;
            rid.getDates();
            rid.getLoad();
            rid.getDischarge();
            List<SelectOption> clientOptions =  rid.getClientLst();
            rid.assetFinal = null;
            rid.selectedClient = GardTestData.clientAcc.id;
            rid.ObjName = string.valueof(GardTestData.test_object_1st.name) ;       
            rid.FindBy='objname';
            rid.searchByImoObjectName();
            rid.assetFinal = null;
            rid.selectedClient = GardTestData.clientAcc.id;
            rid.ObjName = string.valueof(GardTestData.test_object_1st.Imo_Lloyds_No__c);
            rid.FindBy='imo';
            rid.searchByImoObjectName();
            rid.dateOutputForReview='05/02/2015';
            rid.setObjectSelection();
            rid.getCName();
            rid.addTextBox();
            rid.rowIndex=0;
            rid.removeTextBox();
            //rid.saveRecord();    commented on 9th sept 2018
            rid.clearRecord();
            rid.uwf.Clean_rated_vessel_carried_persistent__c=true;
            rid.uwf.Dirty_rated_vessel_carried_non_persist__c=true;
            rid.goNext();
            rid.goPrev();
            rid.sendToForm();
            rid.cancelRecord();
            rid.uwf.id=GardTestData.clientCase.id; 
            rid.saveRecord(); 
            rid.uwf.id=GardTestData.clientCase.id; 
            rid.chkAccess();
            Test.stopTest();
        }
    }
}