/********************************************************************************************************
**ClassName         : ViewClaimDocumentsCtrl
**Author            : Anju G & Arpan Muhuri
**Created Date      :09/03/2021
**Description       : Controller for claim Documents created as part of ticket SF-6151
=======================================================================================================
*  Version                        Developer                       Date                   Description
=========================================================================================================
*  1.0                             Anju & Arpan                   09/03/2021              SF-6151

*********************************************************************************************************/


public class ViewClaimDocumentsCtrl {
    String loggedInAccountId;
    String loggedInContactId;
    public String userType{get;set;}
    Boolean initialMode;
    public String loggedInAccCoId = '';
    public boolean enableSelectionMsg{get;set;}    //Used to display 'no records available' message in page
    public boolean isRecordFound{get;set;}
    public Integer noOfRecords{get; set;}
    public boolean portfolioDocumentTab{get;set;}
    public string selectedClient{get; set;}     //Holds the selectedValue from client dropdown
    public String selectedYear{get; set;}       //Holds the selectedValue from year dropdown
    public List<string> selectedClaimType{get; set;}  
    public List<string> selectedCover{get; set;}  
    public List<string> selectedObj{get; set;}  
    public Attachment att{get;set;}    //Used for document download
    public string currentDocumentId{get;set;}    //Used for document download
    public Boolean isDocDeteted{get;set;}   //Used for document download
    public boolean isFav{get;set;}
    public String shareClientName{get;set;}
    public String strGlobalClient{get;set;}//added For Global Client
    public List<String> selectedAllDocType{get; set;}   //Holds all the selected document types from doc type dropdown
    public List<SelectOption> docYearsList{get;set;}    //Holds options for Years dropdown
    public List<SelectOption> initialClaimDocTypes{get;set;}    //Holds options for Doc Types dropdown
    public List<SelectOption> docClaimTypeList{get;set;}
    public List<SelectOption> docCoverList{get;set;}
    public List<SelectOption> docObjList{get;set;}
    public List<SelectOption> clientOption{get; set;}   //Holds options for client dropdown
    public List<String> selectedGlobalClient {get;set;}     //added For Global Client
    public String newstrSelected{get;set;}      //added For Global Client
    public List<String> lstGlobalClientIds;     //added For Global Client
    List<String> globalClientIdLst;     //populated by MyGardHelperCtrl
    public Boolean hasMultipleClients{get;set;}     //4302 flag for checking if client has acquired Clients
    public Boolean hideSection{get;set;}    //Need to implement the functionality
    public ClaimDocumentResponseWrapper claimDocResponseWrapper{get;set;} 
    public String claimGUID;
    set<string> uniqCover = new set<string>();
    set<string> uniqClaimType=new set<string>();
    set<string> uniqObject = new set<string>();
    set<string> uniqueDocType=new Set<String>();
    
    set<string> documentType = new set<string>();
    set<string> objDocumentType = new set<string>();
    public ApexPages.StandardSetController submissionSetCon{
        get{ 
            return submissionSetCon;         
        }
        set; }
    public ApexPages.StandardSetController submissionSetConOpen{
        get{ 
            return submissionSetConOpen;         
        }
        set; }
    //public ViewClaimDocumentsCtrl(){}
    public ViewClaimDocumentsCtrl() {
        disabled=false;
        hideSection = false;
        initialMode = true;
        enableSelectionMsg = false;
        clientOption = new List<SelectOption>();
        selectedClient = '';
        claimGUID = '';
        pageNum = 1;
        size = 10;
        selectedYear = '';
        selectedAllDocType = new List<String>();
        selectedGlobalClient = new List<String>();//added For Global Client
        MyGardHelperCtrl.CreateCommonData();
        loggedInAccountId = MyGardHelperCtrl.LOGGED_IN_ACCOUNT_ID; 
        loggedInContactId = MyGardHelperCtrl.LOGGED_IN_CONTACT_ID;
        String currentUserId = UserInfo.getUserId();
        userType = MyGardHelperCtrl.USER_TYPE;
        hasMultipleClients = MyGardHelperCtrl.hasMultipleClients;
        lstGlobalClientIds = new List<String>();//added For Global Client
        globalClientIdLst = new List<String>();
        lstGlobalClientIds = MyGardHelperCtrl.fetchGlobalClientsHelper(loggedInAccountId,currentUserId);
        globalClientIdLst = MyGardHelperCtrl.globalClientIdLst;
        docClaimTypeList= new List<SelectOption>();
            docCoverList= new List<SelectOption>();
            docObjList= new List<SelectOption>();
        if(lstGlobalClientIds != null && lstGlobalClientIds.size() > 0){
            for(Account acc:[Select Id,Name from Account where Id IN:lstGlobalClientIds /*order by Name*/]){ //sfEdit commented ordering while retrieving
                selectedGlobalClient.add(acc.Name);
            }
            selectedGlobalClient.sort();
        }else{
            for(Account acc:[Select Id,Name from Account where Id IN:globalClientIdLst /*order by Name*/]){ //sfEdit commented ordering while retrieving
                selectedGlobalClient.add(acc.Name);
            }
            selectedGlobalClient.sort();
        }
        if(userType == 'Broker'){
            loggedInAccCoId = [SELECT Company_Id__c FROM Account WHERE Id =: loggedInAccountId].Company_Id__c;
            System.debug('loggedInAccCoId--'+loggedInAccCoId);
        }
        claimGUID = ApexPages.CurrentPage().getParameters().get('claimid');
        /*if(claimGUID != null && claimGUID != '')
            RestCallout();       
        */     
        populateFilterData();   //Populates the initial filter
    }
    //Methods for initial values in filter List STARTS
    private List<SelectOption> getInitialClients(){
        List<SelectOption> clients = new List<SelectOption>();
        if(lstGlobalClientIds != null && lstGlobalClientIds.size() > 0){
            System.debug('*******lstGlobalClientIds******* -->> '+lstGlobalClientIds);
            clients.add(new selectOption('', ''));
            for(Account acc:[Select Id,Name,company_Id__c from Account where Id IN:lstGlobalClientIds order by Name]){
                System.debug('*******Account******* -->> '+acc.Name);
                clients.add(new selectOption(acc.company_Id__c, acc.Name));
                System.debug('*******Account******* -->> '+clients);
            }
        }
       // selectedClient =clients[1].getValue();
        return clients;
    }
    //added For Global Client - start
    public pageReference fetchSelectedClients()
    {
        set<String> setClientIds = new set<String>();
        selectedGlobalClient = new List<String>();//added For Global Client
        if(newstrSelected != null && newstrSelected != '')
        {
            if(newstrSelected.contains(';'))
            {
                setClientIds.addAll(newstrSelected.split(';'));  
            }else
            {
                setClientIds.add(newstrSelected);    
            }
            if(setClientIds.size()>0)
            {
                for(Account acc:[Select Id,Name from Account where Id IN:setClientIds])
                {
                    selectedGlobalClient.add(acc.Name);//added For Global Client    
                }   
            }    
            selectedClient = '';
        }
        system.debug('selectedGlobalClient at fetchSelectedClients: '+selectedGlobalClient);
        pageReference pg = page.ViewDocument;
        pg.setRedirect(true);
        //Reload the page after globalclient go     
        return pg;
    }
    //get the list of claim DocumentType from Custom settings
    private List<SelectOption> getInitialClaimDocumentTypes(){
        List<SelectOption> allClaimDocumentTypes = new List<SelectOption>();
        System.debug('*******Document Type Option******* -->> ');
        for(MyGardDocumentType__c claimDocumentType :[Select Document_Name__c, Document_Type__c,Marine_Document__c,P_I_Document__c, Document_Class__c from MyGardDocumentType__c WHERE Claim_document__c = true order by Document_Name__c]){
            allClaimDocumentTypes.add(new selectOption(claimDocumentType.Document_Type__c, claimDocumentType.Document_Name__c));
        }
        return allClaimDocumentTypes;
    }
   // Populate the list of years
     private List<SelectOption> getYearList(){
        List<SelectOption> yearsList = new List<SelectOption>();
        for(integer i=0;i<integer.valueOf(Label.PolicyYears);i++){
       
       yearsList.add(new selectOption(String.valueOf(date.today().addYears(-i).year()),String.valueOf(date.today().addYears(-i).year())));
        
        }
        selectedYear = yearsList[0].getValue();
        return yearsList;
    }
    
    //This method is used to populate the values in filter
    public void populateFilterData(){  
    system.debug('in populate filter data *****');
    
        if(initialMode){
         system.debug('in initialMode *****');
            clientOption = getInitialClients();
            docYearsList = getYearList();
            initialClaimDocTypes = getInitialClaimDocumentTypes();
        }
        else{
            initialClaimDocTypes.clear();
            docClaimTypeList.clear();
            docCoverList.clear();
            docObjList.clear();
            uniqObject.clear();
            uniqCover.clear();
            uniqClaimType.clear();
            uniqueDocType.clear();
            if(claimDocResponseWrapper.ResultObjects != null){
                for(ClaimDocumentResponseWrapper.ResultObject result : claimDocResponseWrapper.ResultObjects){
                    ClaimDocumentResponseWrapper.objectProperty objProp=result.objectProperties;
                  if(!String.isEmpty(objProp.coverName) && !objProp.coverName.equalsIgnoreCase('N/A')){
                    if(!uniqCover.contains(objProp.coverName)){
                        uniqCover.add(objProp.coverName);
                    }
                  }
                  if(!String.isEmpty(objProp.objectName) && !objProp.objectName.equalsIgnoreCase('N/A')){
                    if(!uniqObject.contains(objProp.objectName)){
                        uniqObject.add(objProp.objectName);
                    }

                  }
                  if(!String.isEmpty(objProp.claimType) && !objProp.claimType.equalsIgnoreCase('N/A')){
                    if(!uniqClaimType.contains(objProp.claimType)){
                        uniqClaimType.add(objProp.claimType);
                    }

                  }
                  if(!String.isEmpty(objProp.pd_payclddocumenttype) && !objProp.pd_payclddocumenttype.equalsIgnoreCase('N/A')){
                    if(!uniqueDocType.contains(objProp.pd_payclddocumenttype)){
                        uniqueDocType.add(objProp.pd_payclddocumenttype);
                    }

                  }
                }
                if(!uniqObject.isEmpty()){
                   
                    for(String obj:uniqObject){
                        docObjList.add(new selectOption(obj, obj));
                    }
                    docObjList.sort();
                }
                if(!uniqCover.isEmpty()){
                   
                    for(String cover:uniqCover){
                        docCoverList.add(new selectOption(cover, cover));
                    }
                    docCoverList.sort();
                }
                if(!uniqClaimType.isEmpty()){
                   
                    for(String claim:uniqClaimType){
                        docClaimTypeList.add(new selectOption(claim, claim));
                    }
                    docClaimTypeList.sort();
                }
                if(!uniqueDocType.isEmpty()){
                   
                    for(String doc:uniqueDocType){
                        initialClaimDocTypes.add(new selectOption(doc, doc));
                    }
                    initialClaimDocTypes.sort();
                }
            }    

        }
    }
    // create the httprequest
    private httpRequest getHttpRequest(){
        string strRestServiceEndpoint='';
        string strContentType='';
        string strclientId='';
        string strclientSecret=''; 
        string authorization = '';   
        HttpRequest request = new HttpRequest();
        AccessTokenResponseWrapper.AccessTokenResponse authToken=AccessTokenGenerator.getAccessTokenResponse(Label.MyGardAccessToken);
        if(authToken!=null &&  authToken.access_token !=null && authToken.token_type!=null){
            authorization = authToken.token_type+' '+authToken.access_token; 
        }
        for(MyGardDocument_Rest_service_endpoint__c value : [SELECT Id, Name, Value__c FROM MyGardDocument_Rest_service_endpoint__c order by Name desc]){
            if(value.Name == 'Rest_service_endpoint')
                strRestServiceEndpoint=value.Value__c;
            if(value.Name == 'content-type')
                strContentType=value.Value__c;
        }
        request.setEndpoint(strRestServiceEndpoint);
        request.setMethod('POST');
        request.setHeader('content-type', strContentType);            
        request.setheader('Authorization',authorization);            
        request.setTimeout(120000);
        return request;
    }
     public Map <String,ClaimDocumentResponseWrapper.ResultObject> docIdObjectMap = new Map <String,ClaimDocumentResponseWrapper.ResultObject>();
   // this method used to sends the request to Mfile 
     public void RestCallout(){
        String jsonBody = '';
        //List<String> documentTypes = new List<String>{'4'};
        system.debug('selectedAllDocType--'+selectedAllDocType);
        system.debug('selectedClient--'+selectedClient);
        system.debug('selectedYear--'+selectedYear);
        system.debug('userType--'+userType);
        ClaimDocumentRequestWrapper requestWrapper;
        
         if(claimGUID != null && claimGUID != ''){
             String claimRefId = [SELECT Claim_Reference_Number__c FROM Claim__c WHERE GUID__c =: claimGUID LIMIT 1].Claim_Reference_Number__c;
             String refId = '';
             if(claimRefId != null && claimRefId != '' && claimRefId.StartsWith('0'))
                 claimRefId = claimRefId.right(claimRefId.length()-1);
             
             String[] parts;
             if(claimRefId != null && claimRefId != '' && claimRefId.contains('/')){
                 parts = claimRefId.split('/');
                 refId = parts[0]+'/'+parts[1];
             }
             else if(claimRefId != null && claimRefId != '' && !claimRefId.contains('/')){
                 refId = claimRefId.remove(' ');
             }
             system.debug('splittedRefId--'+refId);
             requestWrapper = new ClaimDocumentRequestWrapper(refId);
         }
             else
            requestWrapper = new ClaimDocumentRequestWrapper(userType,selectedClient,loggedInAccCoId,selectedYear,selectedAllDocType);//(userType,'26709','10398',2019,documentTypes);
        if(!requestWrapper.filter.isEmpty()){
            jsonBody = json.serialize(requestWrapper);  
        }
        system.debug('Request JSON body--'+jsonBody);
        Http http = new Http();
        HttpRequest request = getHttpRequest();
        if(jsonBody != null && jsonBody != ''){
            request.setBody(jsonBody);
            request.setHeader('Content-length', string.valueOf(jsonBody.length()));
        }
        HttpResponse  response = http.send(request);
        system.debug('Raw response--'+response.getBody());
        if (response.getStatusCode() == 200) {
            claimDocResponseWrapper  = (ClaimDocumentResponseWrapper) System.JSON.deserialize(response.getBody(), ClaimDocumentResponseWrapper.class);
            claimDocResponseWrapper.pagify(null,null,null,null);
            initializeColToOrder();
            setNavigationBooleans();
            if(claimDocResponseWrapper.ResultObjects != null){
                for(ClaimDocumentResponseWrapper.ResultObject resObj : claimDocResponseWrapper.ResultObjects){
                    docIdObjectMap.put(resObj.ExternalID,resObj);
                }  
            }
            if(claimDocResponseWrapper.Count > 0  && claimDocResponseWrapper.Count <= 1000){
                noOfRecords = claimDocResponseWrapper.Count;// record-total display  
                isRecordFound = true;
                enableSelectionMsg  = false;
                System.debug('claimDocResponseWrapper.ResultObjects : '+ claimDocResponseWrapper.ResultObjects);
            }else{enableSelectionMsg = true;}
        }
        
    }
    // Used to reset the values while click on clear button
    public PageReference reset(){
        system.debug('in Reset*********');
        selectedClient='';
        selectedYear = '';
        selectedClaimType.clear();
        selectedCover.clear();
        selectedAllDocType.clear();
        initialMode=true;
        disabled=false;
        populateFilterData();
      pageReference vd=page.ViewClaimDocuments;
        vd.setRedirect(true);
        return vd;
       
    } 
    public void customQuerySearch(){
        if(claimDocResponseWrapper == null || claimDocResponseWrapper.Count == 0) {
           RestCallout();
                if(claimDocResponseWrapper != null && claimDocResponseWrapper.Count > 0) {
                    initialMode = false; 
                    disabled=true;
                    populateFilterData();
                }
            }
        else{
            
            Set<String>selectedObjectSet=new Set<String>();
             set<String>selectedClaimTypeSet=new Set<String>();
             set<String>selectedcoverSet=new Set<String>();
             set<String>selectedDocTypeSet=new set<String>();
            
             if(docObjList.size() > 0 && selectedObj.size() > 0) {
                 selectedObjectSet.addAll(selectedObj);
             }
             if(docClaimTypeList.size() > 0 && selectedClaimType.size()>0) {
                 selectedClaimTypeSet.addAll(selectedClaimType);
             }
             if(docCoverList.size() > 0 && selectedCover.size()>0) {
                selectedcoverSet.addAll(selectedCover);
             }
             
             if(initialClaimDocTypes.size() > 0 &&  selectedAllDocType.size()>0) {
                selectedDocTypeSet.addAll(selectedAllDocType);
                
             }
            
         }
         List<String>selectedDocTypes=new List<string>();
         for(SelectOption eachTypeOption : initialClaimDocTypes){
            if(selectedAllDocType != null && selectedAllDocType.contains(eachTypeOption.getValue())){
                selectedDocTypes.add(eachTypeOption.getLabel());
            }
        }
         claimDocResponseWrapper.pagify(selectedObj,selectedDocTypes,selectedClaimType,selectedCover);
         if(claimDocResponseWrapper.pagifiedResultObjects != null && claimDocResponseWrapper.pagifiedResultObjects.size() > 0){
            claimDocResponseWrapper.Result = true;
            claimDocResponseWrapper.Count = (claimDocResponseWrapper.pagifiedResultObjects.size() - 1) * 10 + claimDocResponseWrapper.pagifiedResultObjects.get(claimDocResponseWrapper.pagifiedResultObjects.size()-1).size();
            claimDocResponseWrapper.ObjectType = 0;                         
            noOfRecords = claimDocResponseWrapper.Count;// record-total display
            enableSelectionMsg = false;
            isRecordFound=true;
            if(claimDocResponseWrapper.Count < 1000){
                claimDocResponseWrapper.MoreResultsAvailable = false;
            }
            else if(claimDocResponseWrapper.Count >= 1000){
                claimDocResponseWrapper.MoreResultsAvailable = true;
            }
        }
        System.debug('noOfRecords is: '+noOfRecords);
        if(noOfRecords == 0){
            enableSelectionMsg = true;
            isRecordFound = false;
            return;
        }
        first();
        //setNavigationBooleans();
    }
   
    
    // used for Pagination 
    public Integer currentPageNumber{get;set;}
    public Integer totalPageCount{get;set;}
    public Boolean hasPrevious{get;set;}
    public Boolean hasNext{get;set;}
    public Integer pageNum{get;set;}  
    public PageReference first(){
        currentPageNumber = 1;
        setNavigationBooleans();
        return null;
    }
    public PageReference previous(){
        currentPageNumber--;
        setNavigationBooleans();
        return null;
    }
    public PageReference next(){
        currentPageNumber++;
        setNavigationBooleans();
        return null;
    }
    public PageReference last(){
        currentPageNumber = totalPageCount;
        setNavigationBooleans();
        return null;
    }
    public String gotoPageNumber{get;set;}
    public PageReference goTo(){
        Integer gotoPageNumberInt;
        try{
            gotoPageNumberInt = Integer.valueOf(gotoPageNumber);
            currentPageNumber = (gotoPageNumberInt >= 1 && gotoPageNumberInt <= totalPageCount) ? gotoPageNumberInt : currentPageNumber;
        }catch(NullPointerException npe){
            gotoPageNumberInt = currentPageNumber;
        }catch(System.TypeException te){
            gotoPageNumberInt = currentPageNumber;
        }
        setNavigationBooleans();
        return null;
    }
    private void setNavigationBooleans(){
        currentPageNumber = (currentPageNumber == null) ? 1 : currentPageNumber;
        gotoPageNumber = (currentPageNumber+'');
        totalPageCount = (claimDocResponseWrapper.pagifiedResultObjects != null) ? claimDocResponseWrapper.pagifiedResultObjects.size() : 0;
        hasPrevious = currentPageNumber > 1;
        hasNext = currentPageNumber < totalPageCount;
    }
    
    public String sortCol{get;set;}
    public Boolean sortOrder{get;set;}
    public string newFilterName{get;set;}
    public boolean disabled{get;set;}
    public boolean chkExc{get;set;}
    public boolean isQueried1{get;set;}
    public string searchText{get;set;}
    public string callRest{get;set;}
    public Integer size{get;set;}
     public void createFavourites(){
        ClaimsDocumentUtil.createFavourites(newFilterName);
       
     }

     public Map<String,Boolean> colToOrderMap;
     public void initializeColToOrder(){
        if(colToOrderMap == null){
            colToOrderMap = new Map<String,Boolean>{
                'name'=>true,
                 'policyYr' => true,
                    'object' => true,
                    'docType' => true,
                    'cover' => true,
                    'claimType' => true,
                    'eventDate' => true,
                    'reserve' => true,
                    'paid' => true,
                    'total' => true,
                    'claimRef' => true,
                    'name'=>true
                    };  
                        }
        
    }
    
    
   public  PageReference sortTable(){

    system.debug('sort Col***'+sortCol);
    colToOrderMap= ClaimsDocumentUtil.sortTable(sortCol,sortOrder,colToOrderMap,claimDocResponseWrapper);
    return null;
     }
     public void deleteFavourites(){}
     public void fetchFavourites(){}
     
     //Document download
    public PageReference displayDoc()
    {   
        att=DocumentUtil.downloadDocument(currentDocumentId,loggedInAccountId);
        if(att!=null){
            system.debug('selectedClient1******'+selectedClient);
            Id selectedClientId;
            if(hasMultipleClients){
                selectedClientId=[Select id from Account where company_id__c=:selectedClient].id;
            }
            else{
                selectedClientId=loggedInAccountId;
            }
            ClaimsDocumentUtil.createClientActivity(currentDocumentId,loggedInAccountId,loggedInContactId,selectedClientId,hasMultipleClients,docIdObjectMap);  
            return page.DisplayClaimsDocument;
           
            
        }     
        
        else {
            return null;
        }
    }
    public void deleteDoc(){
        system.debug('selectedDocId---------'+att.id);        
        //delete att;
        processDeleteDocss(att.id);//call future method
        isDocDeteted = false;
    }
    @future
    public static void processDeleteDocss(Id recordId){
        /*sfEdit debugs*/Long tStart,tEnd;tStart = 0;tEnd = 0;tStart = System.currentTimeMillis();System.debug('sfEdit processDeleteDoc Starts @ - '+tStart);
        /*sfEdit debugs*/tEnd = System.currentTimeMillis();System.debug('sfEdit processDeleteDoc Ends @ - '+tEnd);System.debug('sfEdit tUtilised - '+(tEnd-tStart));
    }
     public PageReference exportToExcel(){ 
        
       
        return page.ExportToExcelClaimDocuments;
           }
}