@isTest

public class TestEmailSender1{

//Public list<EmailTemplate> Emaillist = new list<EmailTemplate>();
//Public EmailTemplate e1,e2,e3,e4,e5,e6;
    ///////*******Variables for removing common literals********************//////////////
    public static string mailStr = 't1@gmail.com';
    public static string ownerMailStr = 'ownergard@gmail.com';
    public static string cntrctNmStr = 'ABC';
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    /////**********************end**************************/////
    @future
    public static void inserttemp1(){
        list<EmailTemplate> Emaillist = new list<EmailTemplate>();
        
        EmailTemplate  e1 = new EmailTemplate(Name = 'MyGard-Extranet Feedback-Acknowledgement', body = 'HEADER] Thank you for contacting Gard. Your feedback has been received.   Below is a summary of the details submitted on MyGard:  Key feedback issue/area: [AREA] Comments: [COMMENTS]    Main reason for visiting MyGard today: [REASON] Overall site rating: [SITE_RATING]  Objective accomplished through MyGard: [OBJECTIVES_ACCOMPLISHED]    Any other comments: [OTHER_COMMENTS]    Preferred method of contact:    Phone: [PREF_PHONE] Email: [PREF_EMAIL] [SIGNATURE] [FOOTER]');
        
        Emaillist.add(e1); 
        
        EmailTemplate  e2 = new EmailTemplate(Name = 'MyGard-Contact Me-Acknowledgement', body = '[HEADER] Thank you for contacting Gard. Your request for Gard to contact you has been registered. Below is a summary of the details submitted on MyGard: Area: [AREA]    Client: [CLIENT]    Cover: [COVER]  Claim type: [CLAIMTYPE] Subject: [SUBJECT]  Preferred method of contact:    Phone: [PHONE]  Email: [EMAIL]  [SIGNATURE] [FOOTER]');
        Emaillist.add(e2 );
        
        EmailTemplate  e3 = new EmailTemplate(Name = 'MyGard-Start Sharing-Acknowledgement', body = '[HEADER] You are now sharing all your records with [CLIENTNAME]. All users of MyGard from [CLIENTNAME] are now able to see all records, e.g. claims, covers etc., related to agreements brokered by you. If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
        Emaillist.add(e3 );
        
        EmailTemplate  e4 = new EmailTemplate(Name = 'MyGard-Stop Sharing-Acknowledgement', body ='[HEADER] You are no longer sharing your records with [CLIENTNAME]. As a result, none of the users of MyGard from [CLIENTNAME] are able to see records, e.g. claims, covers etc., related to agreements brokered by you. If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
        Emaillist.add(e4 );
        
        EmailTemplate  e5 = new EmailTemplate(Name = 'MyGard-Request To Mark As Admin-Acknowledgement',body ='[HEADER] Thank you for submitting your request for a change of administrator user. As per your request, the administrator right will be transferred to following user:   First Name: [FIRSTNAME] Last Name: [LASTNAME]   If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
        Emaillist.add(e5 );
        
        EmailTemplate  e6 = new EmailTemplate(Name = 'MyGard-User access revokation-Acknowledgement',body = '[HEADER] Thank you for contacting Gard and we have registered your request. Based on your request, [USERNAME] does not have access to MyGard anymore. If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
        Emaillist.add(e6 );
        
        EmailTemplate  e7 = new EmailTemplate(Name = 'Acknowledgement - Change in Primary Contact',body ='[HEADER] Based on your request, [USERNAME] is now the new primary contact for your organisation. If you have any queries, please contact us at MyGard.support@gard.no. [SIGNATURE] [FOOTER]');
        Emaillist.add(e7 );
        
        EmailTemplate  e8 = new EmailTemplate(Name = 'MyGard-Request For Revoking People Claim Access-Acknowledgement', body ='[HEADER] Based on your request, [USERNAME] will now not be able to see the restricted claim description text in MyGard whenever the claim type falls within “Crew, Passenger, Person(s) other than crew or passenger”. If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
        Emaillist.add(e8);
        
        EmailTemplate  e9 = new EmailTemplate(Name = 'MyGard-Request For Granting People Claim Access-Acknowledgement', body = '[HEADER] Based on your request, [USERNAME] will now be able to see the restricted claim description text in MyGard whenever the claim type falls within “Crew, Passenger, Person(s) other than crew or passenger”. If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
        Emaillist.add(e9);
        
        EmailTemplate  e10 = new EmailTemplate(Name = 'MyGard-Request For New User Access-Acknowledgement', body = '[HEADER] Thank you for contacting Gard and we have registered your request. Below is a summary of the details submitted on MyGard:  First Name: [FIRSTNAME] Last Name: [LASTNAME]   Phone: [PHONE]  Email address: [EMAIL] Comments: [COMMENTS] If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
        Emaillist.add(e10);
        
        EmailTemplate  e11 = new EmailTemplate(Name = 'PEME Contact Me - Acknowledgement', body = '[HEADER] Thank you for contacting Gard and we have registered your request. Below is a summary of the details submitted on MyGard:  First Name: [FIRSTNAME] Last Name: [LASTNAME]   Phone: [PHONE]  Email address: [EMAIL] Comments: [COMMENTS] If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
        Emaillist.add(e11);
        
        
        EmailTemplate  e12 = new EmailTemplate(Name = 'MyGard-Request To Remove As Admin-Acknowledgement', body = '[HEADER] Thank you for contacting Gard and we have registered your request. Below is a summary of the details submitted on MyGard:  First Name: [FIRSTNAME] Last Name: [LASTNAME]   Phone: [PHONE]  Email address: [EMAIL] Comments: [COMMENTS] If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
        Emaillist.add(e12);
        
        
        EmailTemplate e13 = new EmailTemplate(Name = 'MyGard-Service Request File Upload/Case Comment', body = '[HEADER] A new [File/Comment] has been added to your Case. Case Reference Number: {!Case.CaseNumber} Comment added by MyGard: [caseComment] Please find the details of the case here - [Case URL] If you have any queries, please contact us at MyGard.support@gard.no. [SIGNATURE] [FOOTER]');
        Emaillist.add(e13);

        //EmailTemplate  e11 = new EmailTemplate(Name = 'MyGard-Request For New User Access-Acknowledgement');
        //Emaillist.add(e11);


        insert Emaillist;

        }

    public static testmethod void testemail(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        
        Gardtestdata test_rec = new gardtestdata();
        test_rec.commonrecord();
        
        CommonVariables__c cvar = new CommonVariables__c(name = 'ReplyToMailId', value__c ='test123@gmail.com');
        insert cvar;
        
        EmailServicesAddress testobj1= new EmailServicesAddress(LocalPart = 'ownergard',IsActive = true); 
        //insert testobj1;
        
        OrgWideEmailAddress owea = new OrgWideEmailAddress(DisplayName = 'no-reply@gard.no', Address = 'no-reply@gard.no');
        //insert owea;
        
         Customer_Feedback__c fdbck = new Customer_Feedback__c(Key_feedback_issue_area__c = 'Availability' ,
                                                               Did_you_accomplish_what_you_wanted_to__c = 'yes',
                                                               What_was_your_main_reason_for_visiting__c = 'testclass' ,
                                                               Overall_site_rating__c = 'Excellent' ,
                                                               Any_Other_Comments__c = 'testcomment',
                                                               Contact_preference_Phone__c = true,
                                                               Contact_preference_email__c = true );
                                                               
         insert fdbck;
         Customer_Feedback__c fdbck1 = new Customer_Feedback__c(Key_feedback_issue_area__c = 'MyGard bugs or problems' ,
                                                               Did_you_accomplish_what_you_wanted_to__c = 'yes',
                                                               What_was_your_main_reason_for_visiting__c = 'testclass' ,
                                                               Overall_site_rating__c = 'Excellent' ,
                                                               Any_Other_Comments__c = 'testcomment',
                                                               Contact_preference_Phone__c = true,
                                                               Contact_preference_email__c = true );
                                                               
         insert fdbck1;
         Customer_Feedback__c fdbck12 = new Customer_Feedback__c(Key_feedback_issue_area__c = 'MyGard experience' ,
                                                               Did_you_accomplish_what_you_wanted_to__c = 'yes',
                                                               What_was_your_main_reason_for_visiting__c = 'testclass' ,
                                                               Overall_site_rating__c = 'Excellent' ,
                                                               Any_Other_Comments__c = 'testcomment',
                                                               Contact_preference_Phone__c = true,
                                                               Contact_preference_email__c = true );
                                                               
         insert fdbck12;    
          Feedback_mail_id__c f1 = new Feedback_mail_id__c(name = 'Feedback notification',Email_address__c =  mailStr );
          insert f1;
          Feedback_mail_id__c f3 = new Feedback_mail_id__c(name = 'Feedback notification MyGard bugs',Email_address__c =  mailStr );
          insert f3;
           Feedback_mail_id__c fm= new Feedback_mail_id__c (name ='Feedback notification CC',Email_address__c = mailStr );
                insert fm;
           /* QueueSobject qs = new QueueSobject(queue.name = 'MyGard_Gard_Administrator',queue.email = 'abc@xyz.com',queue.doesSendEmailToMembers=true);
            insert qs;
            Group__c test_group1 = new Group__c(name = 'OPEN', Group_Id__c = '4');
             insert test_group1; */
            MyGard_support_email_address__c mg= new MyGard_support_email_address__c(name ='Support email address',Email_address__c = mailStr );
            insert mg;
           // Feedback_mail_id__c fm= new Feedback_mail_id__c (name ='Feedback notification CC',Email_address__c = mailStr );
          //  insert fm;
    System.runAs(gardtestdata.brokerUser)
    {
        test.startTest();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        EmailSender newobj = new emailsender();
        newobj.CaseId=gardtestdata.brokerCase.id;
        newobj.ownerEmail =  ownerMailStr ;
        newobj.contactMe_loggedInUser =  cntrctNmStr ;
        newobj.contactMe_loggedInUserOrganization = 'XYZ';
        newobj.clientBrokerAdminPhone = '123456';
        newobj.clientBrokerAdminMobile='4534534';
        newobj.clientBrokerAdminEmail= ownerMailStr ;
        newobj.contactMe_area='1qwr';
        newobj.contactMe_client='qwert';
        newobj.contactMe_subject='qwerty';
        newobj.contactMe_cover='qwerty';
        newobj.contactMe_claimType='qwerty';
        newobj.clientBrokerOrganization = 'testre';
        newobj.newUserInfoFirstName =  cntrctNmStr ;
        newobj.newUserInfoLastName =  cntrctNmStr ;
        newobj.newUserInfoPhone =  cntrctNmStr ;
        newobj.newUserInfoEmail =  ownerMailStr ;
        newobj.clientBrokerAdminPhone =  cntrctNmStr ;
        newobj.clientBrokerAdminMobile =  cntrctNmStr ;
        newobj.clientBrokerAdminEmail =  ownerMailStr ;
        newobj.addNewUsrComments =  cntrctNmStr ;
        newobj.contactMe_area = 'MyGard';
        newobj.caseLink=cntrctNmStr;
        newobj.removeUserInfoName =  cntrctNmStr ;
        newobj.clientBrokerOldAdminName =  cntrctNmStr ;
        newobj.clientBrokerNewAdminName =  cntrctNmStr ;
        newobj.clientOrganization = 'testre';
        newobj.clientBrokerAdminFullName =  cntrctNmStr ;
        newobj.revokedUserName =  cntrctNmStr ;
        newobj.feedbackId = fdbck.id;
        string[] toadress = new string[]{ ownerMailStr };
        toadress.add('test1');
        toadress.add('test2');
        toadress.add('test3');
        toadress.add('test4');
        toadress.add('test5');
        toadress.add('test6');
        toadress.add('test7');
        toadress.add('test8');
        toadress.add('test9');
        toadress.add('test10');
        newobj.caseInfo = 'File';
        //newobj.createAndSendEmail(Gardtestdata.brokerCase.id,'MyGard-Service Request File Upload/Case Comment','',toadress,Gardtestdata.brokerUser.id);
        newobj.caseInfo = 'Case Comment';
        newobj.caseComment ='Test comment';
        //newobj.createAndSendEmail(Gardtestdata.brokerCase.id,'MyGard-Service Request File Upload/Case Comment','',toadress,Gardtestdata.brokerUser.id);
        //newobj.createAndSendEmail(fdbck12.id, 'MyGard-Extranet Feedback-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
        newobj.createAndSendEmail(Gardtestdata.brokerCase.id, 'MyGard-Request For New User Access-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
        newobj.createAndSendEmail(Gardtestdata.brokerCase.id, 'PEME Contact Me - Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
        newobj.createAndSendEmail(Gardtestdata.brokerCase.id, 'MyGard-Request To Remove As Admin-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
        //newobj.createAndSendEmail(Gardtestdata.brokerCase.id, 'MyGard-Contact Me-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
        
        //newobj.createAndSendEmail(Gardtestdata.brokerCase.id, 'MyGard-Request To Revoke Admin Access-Notification', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
        //newobj.createAndSendEmail(Gardtestdata.broker_Contract.id, 'MyGard-Start Sharing-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
        //newobj.createAndSendEmail(Gardtestdata.broker_Contract.id, 'MyGard-Stop Sharing-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
       // newobj.createAndSendEmail(Gardtestdata.brokerCase.id, 'MyGard-Request To Mark As Admin-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
       // newobj.createAndSendEmail(Gardtestdata.brokerCase.id, 'MyGard-User access revokation-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
        newobj.createAndSendEmail(Gardtestdata.brokerContact.id, 'Acknowledgement - Change in Primary Contact', Gardtestdata.brokerContact.id, toadress , Gardtestdata.BrokerContact.Id);
        //newobj.createAndSendEmail(Gardtestdata.brokerContact.id, 'MyGard-Request For Revoking People Claim Access-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
        //newobj.createAndSendEmail(Gardtestdata.brokerContact.id, 'MyGard-Request For Granting People Claim Access-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
        newobj.send('contactme',Gardtestdata.clientAcc.id);
        newobj.contactMe_area = 'MyGard1';
        newobj.send('contactme',Gardtestdata.clientAcc.id);
        newobj.createBody(mail ,'adduser',Gardtestdata.clientAcc.id);
        newobj.send('registerclaim_copytoRequestor',Gardtestdata.clientAcc.id);
        newobj.send('editObject',Gardtestdata.clientAcc.id);
        newobj.feedbackArea='MyGard bugs or problems';
        newobj.send('userFeedback',Gardtestdata.clientAcc.id);
        //newobj.feedbackArea = 'PEME';
        //newobj.send('userFeedback',Gardtestdata.clientAcc.id);
       // newobj.send('userFeedback');
        newobj.AccountId=gardtestdata.brokerAcc.Id;
        newobj.send('startshare',Gardtestdata.clientAcc.id);
        //newobj.send('casecomment','');
        //newobj.send('fileupload','');
        //newobj.adduser
      //  newobj.createBody(mail ,'userFeedback');
        newobj.createBody(mail ,'removeuser',Gardtestdata.clientAcc.id);
        newobj.createBody(mail ,'makeadmin',Gardtestdata.clientAcc.id);
        newobj.createBody(mail ,'RemoveAdmin',Gardtestdata.clientAcc.id);
        newobj.createBody(mail ,'startshare',Gardtestdata.clientAcc.id);
        newobj.createBody(mail ,'stopshare',Gardtestdata.clientAcc.id);
        newobj.createBody(mail ,'revokeAccess',Gardtestdata.clientAcc.id);
        newobj.createBody(mail ,'PEMEcontactme',Gardtestdata.clientAcc.id);
        newobj.gardExtranetAdministrator =  cntrctNmStr ;
        newobj.newUserInfoMobile =  cntrctNmStr ;
        newobj.existingContactId =  cntrctNmStr ;
        newobj.logo =  cntrctNmStr ;
        newobj.contactMe_recipentName =  cntrctNmStr ;
        newobj.registerClaim_recipientName =  cntrctNmStr ;
        newobj.registerClaim_caseID =  cntrctNmStr ;
        newobj.registerClaim_myClaimId =  cntrctNmStr ;
        newobj.registerClaim_userType =  cntrctNmStr ;
        newobj.registerClaim_clientName =  cntrctNmStr ;
        newobj.registerClaim_referencenumber =  cntrctNmStr ;
        newobj.registerClaim_objectName =  cntrctNmStr ;
        newobj.registerClaim_eventDate =  cntrctNmStr ;
        newobj.registerClaim_place =  cntrctNmStr ;
        newobj.registerClaim_coverName =  cntrctNmStr ;
        newobj.registerClaim_claimType =  cntrctNmStr ;
        newobj.registerClaim_claimDescription =  cntrctNmStr ;
        newobj.registerClaim_name =  cntrctNmStr ;
        newobj.registerClaim_rank =  cntrctNmStr ;
        newobj.registerClaim_nationality =  cntrctNmStr ;
        newobj.registerClaim_claimDetail =  cntrctNmStr ;
        newobj.registerClaim_portOfEmbarkation =  cntrctNmStr ;
        newobj.registerClaim_dateOfEmbarkation =  cntrctNmStr ;
        newobj.registerClaim_numberofStowaways =  cntrctNmStr ;
        newobj.editObject_emailBody =  cntrctNmStr ;
        newobj.editObject_fromemail =  cntrctNmStr ;
        newobj.feedbackComments =  cntrctNmStr ;
        newobj.feedbackReason =  cntrctNmStr ;
        newobj.feedbackRating =  cntrctNmStr ;
        newobj.feedbackObjective =  cntrctNmStr ;
        System.assertEquals(newobj.contactMe_emailPreference , null);
        
        test.stopTest();
     }
 }
 }