/*
*
* Check that email template name is stored when id is changed
* on event.
*
*/
@isTest
private class testTrig_fluidoSurvey {

	static EmailTemplate et;
	static fluidoconnect__Survey__c[] events;
	
	static void setupTemplate()
	{
		//Folder f = new Folder(Name='Test Folder');
		//insert f;
		
		//f=[SELECT Id FROM Folder WHERE Name='Test Folder'];
		//The unified folder id is the org id
		string folderId = UserInfo.getOrganizationId();
		string templateid = Math.random().format().replace('.', '').replace(',','');
		
		et = new EmailTemplate(Name='Test Template', DeveloperName='Test_Template' + templateid, Body='Test Body', FolderId=folderId, TemplateType='Text', isActive=true);
		insert et;
		
	}
	
	static void setupEvents()
	{
		events = new fluidoconnect__Survey__c[]{ 
		new fluidoconnect__Survey__c(	Name='Test Event',
										Event_Start_Date__c = Date.Today(),
										fluidoconnect__Status__c='Open')
		};
		insert events;
	}
	
    static testMethod void unitTest() {
       
       setupTemplate();
       
       User u = [SELECT Id FROM User WHERE Id=:Userinfo.getUserId()];
       system.runAs(u)
       {
	       setupEvents();
	              
	       system.assertEquals(null, events[0].fluidoconnect__TemplateId__c);
	       et = [SELECT Id FROM EmailTemplate WHERE Name='Test Template' LIMIT 1];
	       events[0].fluidoconnect__TemplateId__c = et.Id;
	       update events;
	       
	       events = [SELECT Id, Name, Mass_Email_Template_Name__c FROM fluidoconnect__Survey__c];
	       
	       system.assertEquals('Test Template', events[0].Mass_Email_Template_Name__c);
       }
    }
}