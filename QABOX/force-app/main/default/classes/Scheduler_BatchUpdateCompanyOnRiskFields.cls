global class Scheduler_BatchUpdateCompanyOnRiskFields implements Schedulable {
    global void execute(SchedulableContext SC) {
        BatchUpdateCompanyOnRiskFields onRiskBatch = new BatchUpdateCompanyOnRiskFields();     
        database.executebatch(onRiskBatch , 10);
   }
}