@isTest
/*	This test retrieves all possible values in the Opportunity.Approva_Criteria__c, and checks that
	each value is included in the Approval_Criteria_Count__c formula. When we add or change criteria, 
	the test will break if we forget to update the formula. */
private class ApprovalCriteriaCountTest {
	
	static testMethod void myUnitTest() {

		Opportunity opp = TestDataGenerator.getOpportunity('P&I', 'Risk Evaluation', 'Small Craft');
		Schema.DescribeFieldResult fieldDescription = Opportunity.Approval_Criteria__c.getDescribe();
		List<Schema.PicklistEntry> entries = fieldDescription.getPicklistValues();
		
		for (Schema.PicklistEntry entry : entries) {			
			String value = entry.getValue();
			opp = TestDataGenerator.changeApprovalCriterion(opp, value);	
			Boolean isCounted = opp.Approval_Criteria_Count__c == 1;
			System.assert(isCounted, '"' + value + '" not included in Approval_Criteria_Count__c formula.');
		}
	}	
}