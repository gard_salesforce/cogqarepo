/*****************************************************************************************************
* Class Name: ClaimSurveyController
* Created By: Arpan Muhuri
* Date: 17-06-2021
* Description: SF-5859 - Used to automate sending claims survey to multiple contact at a time
* Test class : TestClaimSurveyController
*******************************************************************************************************
*  Version    Developer        Date          Description
=======================================================================================================
*  1.0        Arpan Muhuri     17-06-2021    Created
*/
public class ClaimSurveyController {
    
    public class JSONOutput{
        Id recordId;
        String name;
        String accountName;
        //String fileSource;
    }
    
    @AuraEnabled
    public static String fetchRecords(String searchString, String values) {//List<RecordsData>
        try {
            List<RecordsData> recordsDataList = new List<RecordsData>();
            //system.debug('Values--'+values);
            List<String> selectedvalues = (List<String>) JSON.deserialize(values, List<String>.class);
            String query = 'SELECT Id, Name, Account.name, Email FROM Contact ';
            if(selectedvalues != null && selectedvalues.size() > 0) {
                query += ' WHERE Id IN: selectedvalues LIMIT 49999';
            } else {
                //query += ' WHERE Email != null AND No_Longer_Employed_by_Company__c = false AND (FirstName LIKE \'%' + String.escapeSingleQuotes(searchString.trim()) + '%\' OR '+
                //            'LastName LIKE \'%'+ String.escapeSingleQuotes(searchString.trim()) + '%\' OR NAME LIKE \'%'+String.escapeSingleQuotes(searchString.trim()) + '%\') LIMIT 49999 ';
                query += ' WHERE Email != null AND No_Longer_Employed_by_Company__c = false AND Name LIKE \'%' + String.escapeSingleQuotes(searchString.trim()) + '%\'  LIMIT 15 ';
            }
            
            for(Contact con : (List<Contact>)Database.query(query)) {
                recordsDataList.add( new RecordsData(con.Name, con.id, con.Email, con.Account.name));
            }
            //return recordsDataList;
            return JSON.serialize(recordsDataList);
        } catch (Exception err) {
            if ( String.isNotBlank( err.getMessage() ) && err.getMessage().contains( 'error:' ) ) {
                throw new AuraHandledException(err.getMessage().split('error:')[1].split(':')[0] + '.');
            } else {
                throw new AuraHandledException(err.getMessage());
            }
        } 
    }
    
    public class RecordsData {
        @AuraEnabled public String label;
        @AuraEnabled public String value;
        @AuraEnabled public String email;
        @AuraEnabled public String companyName;
        public RecordsData(String label, String value, String email, String companyName) {
            this.label = label;
            this.value = value;
            if(email != null && email != '')
                this.email = email;
            else
                this.email = '-';
            this.companyName = companyName;
        }
    }
    //Insert claims survey(for tracking how many times survey is sent)
    @AuraEnabled
    public static void sendInvitation(Id claimId, List<Id> selectedContactIdsList){
        //system.debug('claim Id---'+claimId);
        Claims_survey__c claimsSurvey = new Claims_survey__c();
        claimsSurvey.Related_claim__c = claimId;
        claimsSurvey.Survey_ID__c = Label.Claims_survey_link_Salesforce;
        claimsSurvey.Email_template_used__c = Label.Claim_survey_email_template_link;
        insert claimsSurvey;
        //system.debug('claimsSurvey---: '+claimsSurvey);
        List<Claims_survey_contact__c> claimsSurveyConList = new List<Claims_survey_contact__c>();
        for(Id conId:selectedContactIdsList){
            Claims_survey_contact__c claimsSurveyCon = new Claims_survey_contact__c();
            claimsSurveyCon.Contact__c = conId;
            claimsSurveyCon.Claim__c = claimId;
            claimsSurveyCon.Claims_survey__c = claimsSurvey.id;
            claimsSurveyConList.add(claimsSurveyCon);
        }
        insert claimsSurveyConList;
        
        //system.debug('claimsSurveyConList---: '+claimsSurveyConList);
        Claim__c claimRecord = [SELECT Claim_Survey_Link__c,Claims_handler__c,Claim_Adjuster__c,Account__r.Key_Claims_Contact__c,Account__r.Claim_Adjuster_Marine_lk__c FROM Claim__c where id =: claimId];
        for(Claims_survey_contact__c csCon: claimsSurveyConList){
            //system.debug('survey link in claim:'+csCon.Claim__r.Claim_Survey_Link__c);
            csCon.Claim_Survey_Link__c = claimRecord.Claim_Survey_Link__c+csCon.Contact__c+'&ClaimContactId='+csCon.Id;
        }
        
        update claimsSurveyConList;
        
        Id roleId = [SELECT Id FROM UserRole WHERE DeveloperName = 'Customer_Relationship_Management'].Id;
        List<Claims_survey__Share> csShareList = new List<Claims_survey__Share>();
        
    }
    //Used for Testing LWC
     @AuraEnabled (Cacheable = True)
    public static String getRelevantContacts(String contactString){
        String searchString = '%' + contactString + '%';
        List<RecordsData> recordsDataList = new List<RecordsData>();
        for(Contact con:[SELECT Id,Name,Email,Account.name FROM Contact WHERE Email != null AND No_Longer_Employed_by_Company__c = false AND Name LIKE:searchString]){
            recordsDataList.add( new RecordsData(con.Name, con.id, con.Email, con.Account.name));
        }
        return JSON.serialize(recordsDataList);
    }
}