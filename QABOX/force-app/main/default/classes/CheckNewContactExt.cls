/*************************************************************************************************
    Author      : Martin Gardner
    Company     : cDecisions
    Date Created: 
    Description : This class provides support for overriding the new function in certain circumstances
    Modified    :   
***************************************************************************************************/
public with sharing class CheckNewContactExt {

    public contact con{get;set;}
    private ApexPages.StandardController stdCtrl {get; set;}

    public checkNewContactExt(ApexPages.StandardController std) {

        con = (Contact)std.getRecord();
    }
    
    public pagereference checkAndRedirect()
    {
        if(!ContactToolKit.checkForAccountRecordType(con))
            return null;
        else
        {
        	//MG cDecisions - Pass through any input parameters such as RecordType to support standard behavior
        	PageReference stdEdit = new PageReference('/003/e');//?id=' + con.Id + '&retURL=' + con.id);
            stdEdit.getParameters().putAll(ApexPages.currentPage().getParameters());
            //Check the parameters
            System.Debug('*** stdEdit.getParameters = ' + stdEdit.getParameters());
            return stdEdit;
        }   
    }
    public pagereference goBack()
    {
    	Map<String, String> params = ApexPages.currentPage().getParameters();
    	String retURL;
    	if(params.containsKey('retURL')){
    		retURL = params.get('retURL');
    	}else{
    		retURL = '/';
    	}
        pagereference stdDetail = new pageReference(retURL);
        return stdDetail;
    }
    

}